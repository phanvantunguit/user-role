import { Response } from 'express';
import { UserListRoleHandler } from 'src/usecases/role/user-list-role';
import { UserListRolePlanHandler } from 'src/usecases/role/user-list-role-plan';
export declare class UserRoleV2Controller {
    private userListRoleHandler;
    private userListRolePlanHandler;
    constructor(userListRoleHandler: UserListRoleHandler, userListRolePlanHandler: UserListRolePlanHandler);
    listRole(req: any, res: Response): Promise<void>;
    listPlan(req: any, res: Response): Promise<void>;
}
