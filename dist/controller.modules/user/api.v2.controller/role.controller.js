"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRoleV2Controller = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../const");
const user_list_role_1 = require("../../../usecases/role/user-list-role");
const user_list_role_plan_1 = require("../../../usecases/role/user-list-role-plan");
const response_util_1 = require("../../../utils/response.util");
const const_2 = require("../const");
let UserRoleV2Controller = class UserRoleV2Controller {
    constructor(userListRoleHandler, userListRolePlanHandler) {
        this.userListRoleHandler = userListRoleHandler;
        this.userListRolePlanHandler = userListRolePlanHandler;
    }
    async listRole(req, res) {
        var _a;
        const user_id = (_a = req.user) === null || _a === void 0 ? void 0 : _a.user_id;
        const result = await this.userListRoleHandler.execute(user_id, [const_1.ROLE_STATUS.OPEN, const_1.ROLE_STATUS.COMINGSOON]);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listPlan(req, res) {
        const user_id = req.user.user_id;
        const result = await this.userListRolePlanHandler.execute(user_id);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
};
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('/list'),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserRoleV2Controller.prototype, "listRole", null);
__decorate([
    common_1.Get('/plans'),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserRoleV2Controller.prototype, "listPlan", null);
UserRoleV2Controller = __decorate([
    swagger_1.ApiSecurity('Refresh-Token'),
    swagger_1.ApiBearerAuth(),
    common_1.UseGuards(const_2.UserAuthGuard),
    swagger_1.ApiTags('user/role/v2'),
    common_1.Controller('role'),
    __param(0, common_1.Inject(user_list_role_1.UserListRoleHandler)),
    __param(1, common_1.Inject(user_list_role_plan_1.UserListRolePlanHandler)),
    __metadata("design:paramtypes", [user_list_role_1.UserListRoleHandler,
        user_list_role_plan_1.UserListRolePlanHandler])
], UserRoleV2Controller);
exports.UserRoleV2Controller = UserRoleV2Controller;
//# sourceMappingURL=role.controller.js.map