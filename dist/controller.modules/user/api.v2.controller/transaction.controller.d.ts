import { Response } from 'express';
import { IPNTransactionV2Handler } from 'src/usecases/transaction/ipn-transaction-v2';
import { CreateTransactionV2Handler } from 'src/usecases/transaction/user-create-transaction-v2';
import { CreateTransactionV2Input } from 'src/usecases/transaction/user-create-transaction-v2/validate';
import { GetTransactionV2Handler } from 'src/usecases/transaction/user-get-transaction-v2';
import { ListTransactionV2Handler } from 'src/usecases/transaction/user-list-transaction-v2';
export declare class TransactionV2Controller {
    private createTransactionV2Handler;
    private getTransactionV2Handler;
    private listTransactionV2Handler;
    private ipnTransactionV2Handler;
    constructor(createTransactionV2Handler: CreateTransactionV2Handler, getTransactionV2Handler: GetTransactionV2Handler, listTransactionV2Handler: ListTransactionV2Handler, ipnTransactionV2Handler: IPNTransactionV2Handler);
    createTransaction(req: any, body: CreateTransactionV2Input, res: Response): Promise<void>;
    ipnTransaction(body: any, res: Response): Promise<void>;
    listTransaction(req: any, res: Response): Promise<void>;
    getTransaction(id: string, res: Response): Promise<void>;
}
