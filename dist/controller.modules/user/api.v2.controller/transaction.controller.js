"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionV2Controller = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../const");
const app_setting_1 = require("../../../const/app-setting");
const ipn_transaction_v2_1 = require("../../../usecases/transaction/ipn-transaction-v2");
const user_create_transaction_v2_1 = require("../../../usecases/transaction/user-create-transaction-v2");
const validate_1 = require("../../../usecases/transaction/user-create-transaction-v2/validate");
const user_get_transaction_v2_1 = require("../../../usecases/transaction/user-get-transaction-v2");
const validate_2 = require("../../../usecases/transaction/user-get-transaction-v2/validate");
const user_list_transaction_v2_1 = require("../../../usecases/transaction/user-list-transaction-v2");
const validate_3 = require("../../../usecases/transaction/user-list-transaction-v2/validate");
const response_util_1 = require("../../../utils/response.util");
const const_2 = require("../const");
let TransactionV2Controller = class TransactionV2Controller {
    constructor(createTransactionV2Handler, getTransactionV2Handler, listTransactionV2Handler, ipnTransactionV2Handler) {
        this.createTransactionV2Handler = createTransactionV2Handler;
        this.getTransactionV2Handler = getTransactionV2Handler;
        this.listTransactionV2Handler = listTransactionV2Handler;
        this.ipnTransactionV2Handler = ipnTransactionV2Handler;
    }
    async createTransaction(req, body, res) {
        const user_id = req.user.user_id;
        const email = req.user.email;
        let merchantCode = req.user.merchant_code || app_setting_1.APP_SETTING.MERCHANT_CODE_DEFAULT;
        const result = await this.createTransactionV2Handler.execute(Object.assign({}, body), user_id, email, merchantCode);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async ipnTransaction(body, res) {
        const result = await this.ipnTransactionV2Handler.execute(body);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listTransaction(req, res) {
        const user_id = req.user.user_id;
        const result = await this.listTransactionV2Handler.execute(user_id);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getTransaction(id, res) {
        const result = await this.getTransactionV2Handler.execute(id);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
};
__decorate([
    common_1.Post(''),
    swagger_1.ApiResponse({
        status: 200,
        type: class CreateTransactionOutputMap extends swagger_1.IntersectionType(validate_1.CreateTransactionV2PayloadOutput, const_2.BaseApiOutput) {
        },
        description: 'Create transaction',
    }),
    __param(0, common_1.Req()),
    __param(1, common_1.Body()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_1.CreateTransactionV2Input, Object]),
    __metadata("design:returntype", Promise)
], TransactionV2Controller.prototype, "createTransaction", null);
__decorate([
    common_1.Put('/ipn'),
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    __param(0, common_1.Body()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TransactionV2Controller.prototype, "ipnTransaction", null);
__decorate([
    common_1.Get('/list'),
    swagger_1.ApiResponse({
        status: 200,
        type: class ListTransactionV2PayloadOutputMap extends swagger_1.IntersectionType(validate_3.ListTransactionV2PayloadOutput, const_2.BaseApiOutput) {
        },
        description: 'List transaction',
    }),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TransactionV2Controller.prototype, "listTransaction", null);
__decorate([
    common_1.Get('/:id'),
    swagger_1.ApiResponse({
        status: 200,
        type: class DetailTransactionV2PayloadOutputMap extends swagger_1.IntersectionType(validate_2.DetailTransactionV2PayloadOutput, const_2.BaseApiOutput) {
        },
        description: 'Get detail transaction',
    }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], TransactionV2Controller.prototype, "getTransaction", null);
TransactionV2Controller = __decorate([
    swagger_1.ApiSecurity('Refresh-Token'),
    swagger_1.ApiBearerAuth(),
    common_1.UseGuards(const_2.UserAuthGuard),
    swagger_1.ApiTags('user/transaction/v2'),
    common_1.Controller('transaction'),
    __param(0, common_1.Inject(user_create_transaction_v2_1.CreateTransactionV2Handler)),
    __param(1, common_1.Inject(user_get_transaction_v2_1.GetTransactionV2Handler)),
    __param(2, common_1.Inject(user_list_transaction_v2_1.ListTransactionV2Handler)),
    __param(3, common_1.Inject(ipn_transaction_v2_1.IPNTransactionV2Handler)),
    __metadata("design:paramtypes", [user_create_transaction_v2_1.CreateTransactionV2Handler,
        user_get_transaction_v2_1.GetTransactionV2Handler,
        user_list_transaction_v2_1.ListTransactionV2Handler,
        ipn_transaction_v2_1.IPNTransactionV2Handler])
], TransactionV2Controller);
exports.TransactionV2Controller = TransactionV2Controller;
//# sourceMappingURL=transaction.controller.js.map