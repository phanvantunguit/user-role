import { ExecutionContext, CanActivate } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { SessionService } from 'src/services/session';
export declare const UserPermission: (permission: string) => import("@nestjs/common").CustomDecorator<string>;
export declare class UserAuthGuard implements CanActivate {
    private reflector;
    private sessionService;
    constructor(reflector: Reflector, sessionService: SessionService);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
export declare class BaseApiOutput {
    error_code?: string;
    message?: string;
}
