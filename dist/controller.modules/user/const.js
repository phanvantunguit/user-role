"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseApiOutput = exports.UserAuthGuard = exports.UserPermission = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const const_1 = require("../../const");
const handle_error_util_1 = require("../../utils/handle-error.util");
const hash_util_1 = require("../../utils/hash.util");
const common_2 = require("@nestjs/common");
const authorization_1 = require("../../const/authorization");
const config_1 = require("../../config");
const session_1 = require("../../services/session");
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const UserPermission = (permission) => common_2.SetMetadata('permission', permission);
exports.UserPermission = UserPermission;
let UserAuthGuard = class UserAuthGuard {
    constructor(reflector, sessionService) {
        this.reflector = reflector;
        this.sessionService = sessionService;
    }
    async canActivate(context) {
        const permission = this.reflector.get('permission', context.getHandler());
        const request = context.switchToHttp().getRequest();
        const authHeader = request.get('Authorization');
        if (authHeader) {
            const [type, token] = authHeader.split(' ');
            if (type.toLowerCase() === 'bearer' && token) {
                let decode;
                try {
                    decode = await hash_util_1.verifyToken(token, config_1.APP_CONFIG.TOKEN_PUBLIC_KEY);
                }
                catch (error) {
                    handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNAUTHORIZED }, const_1.ERROR_CODE.TOKEN_EXPIRED));
                }
                request.user = decode;
                await this.sessionService.verifySession(decode);
                return true;
            }
        }
        if (permission === const_1.WITHOUT_AUTHORIZATION) {
            return true;
        }
        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNAUTHORIZED }, const_1.ERROR_CODE.AUTHORIZATION_REQUIRED));
    }
};
UserAuthGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [core_1.Reflector,
        session_1.SessionService])
], UserAuthGuard);
exports.UserAuthGuard = UserAuthGuard;
class BaseApiOutput {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Error code of request',
        example: 'SUCCESS',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], BaseApiOutput.prototype, "error_code", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Message describe error code',
        example: 'Success',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], BaseApiOutput.prototype, "message", void 0);
exports.BaseApiOutput = BaseApiOutput;
//# sourceMappingURL=const.js.map