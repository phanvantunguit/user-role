"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiV1UserControllerModule = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const mail_1 = require("../../resources/mail");
const sendgrid_transport_1 = require("../../resources/mail/sendgrid.transport");
const user_1 = require("../../services/user");
const user_controller_1 = require("./api.v1.controller/user.controller");
const verify_token_1 = require("../../services/verify-token");
const user_role_1 = require("../../services/user-role");
const auth_user_role_1 = require("../../services/auth-user-role");
const role_controller_1 = require("./api.v1.controller/role.controller");
const role_1 = require("../../services/role");
const public_controller_1 = require("./api.v1.controller/public.controller");
const transaction_controller_1 = require("./api.v1.controller/transaction.controller");
const transaction_1 = require("../../services/transaction");
const coinpayment_transport_1 = require("../../resources/coinpayment/coinpayment.transport");
const coinpayment_1 = require("../../resources/coinpayment");
const app_setting_1 = require("../../services/app-setting");
const transaction_log_1 = require("../../services/transaction-log");
const transaction_event_1 = require("../../events/transaction.event");
const bot_setting_1 = require("../../services/bot-setting");
const event_emitter_1 = require("@nestjs/event-emitter");
const package_controller_1 = require("./api.v1.controller/package.controller");
const package_1 = require("../../services/package");
const storage_1 = require("../../resources/storage");
const google_transport_1 = require("../../resources/storage/google.transport");
const coinpayment_2 = require("../../services/payment/coinpayment");
const payment_1 = require("../../services/payment");
const resolution_1 = require("../../services/resolution");
const currency_1 = require("../../services/currency");
const session_1 = require("../../services/session");
const repositories_1 = require("../../repositories");
const general_setting_role_1 = require("../../services/general-setting-role");
const bot_controller_1 = require("./api.v1.controller/bot.controller");
const transaction_controller_2 = require("./api.v2.controller/transaction.controller");
const bot_1 = require("../../services/bot");
const user_create_transaction_v2_1 = require("../../usecases/transaction/user-create-transaction-v2");
const check_Items_1 = require("../../services/transaction-v2/check-Items");
const save_items_1 = require("../../services/transaction-v2/save-items");
const cmpayment_1 = require("../../resources/cmpayment");
const cmpayment_transport_1 = require("../../resources/cmpayment/cmpayment.transport");
const user_list_transaction_v2_1 = require("../../usecases/transaction/user-list-transaction-v2");
const user_get_transaction_v2_1 = require("../../usecases/transaction/user-get-transaction-v2");
const ipn_transaction_v2_1 = require("../../usecases/transaction/ipn-transaction-v2");
const user_list_bot_1 = require("../../usecases/bot/user-list-bot");
const role_controller_2 = require("./api.v2.controller/role.controller");
const user_list_role_1 = require("../../usecases/role/user-list-role");
const user_list_bot_signal_1 = require("../../usecases/bot/user-list-bot-signal");
const user_list_bot_paid_1 = require("../../usecases/bot/user-list-bot-paid");
const user_add_favorite_signal_1 = require("../../usecases/bot/user-add-favorite-signal");
const user_remove_favorite_signal_1 = require("../../usecases/bot/user-remove-favorite-signal");
const list_asset_log_1 = require("../../usecases/transaction/list-asset-log");
const user_list_bot_plan_1 = require("../../usecases/bot/user-list-bot-plan");
const user_list_role_plan_1 = require("../../usecases/role/user-list-role-plan");
const bot_trading_controller_1 = require("./api.v1.controller/bot-trading.controller");
const user_list_bot_trading_1 = require("../../usecases/bot/user-list-bot-trading");
const bot_trading_1 = require("../../services/bot-trading");
const user_list_bot_trading_plan_1 = require("../../usecases/bot/user-list-bot-trading-plan");
const connect_broker_1 = require("../../usecases/bot/connect-broker");
const forex_1 = require("../../resources/forex");
const logout_broker_1 = require("../../usecases/bot/logout-broker");
const user_update_bot_trading_status_1 = require("../../usecases/bot/user-update-bot-trading-status");
const bot_trading_event_1 = require("../../events/bot-trading.event");
const user_list_bot_trading_history_1 = require("../../usecases/bot/user-list-bot-trading-history");
const user_list_bot_trading_pnl_1 = require("../../usecases/bot/user-list-bot-trading-pnl");
const user_get_bot_trading_1 = require("../../usecases/bot/user-get-bot-trading");
const select_and_connect_broker_1 = require("../../usecases/bot/select-and-connect-broker");
const broker_1 = require("../../resources/broker");
const axi_1 = require("../../resources/broker/axi");
const xm_1 = require("../../resources/broker/xm");
const xm_transport_1 = require("../../resources/broker/xm/xm.transport");
const axi_transport_1 = require("../../resources/broker/axi/axi.transport");
const deactive_user_bot_trading_1 = require("../../usecases/bot/deactive-user-bot-trading");
const user_balance_and_fee_1 = require("../../usecases/bot/user-balance-and-fee");
const user_create_balance_and_fee_1 = require("../../usecases/bot/user-create-balance-and-fee");
const upload_broker_profile_1 = require("../../usecases/bot/upload-broker-profile");
const metaapi_1 = require("../../resources/broker/metaapi");
const metaapi_transport_1 = require("../../resources/broker/metaapi/metaapi.transport");
let APIV1Controller = class APIV1Controller {
};
APIV1Controller = __decorate([
    common_1.Module({
        imports: [repositories_1.DatabaseModule],
        controllers: [
            user_controller_1.UserController,
            role_controller_1.UserRoleController,
            transaction_controller_1.TransactionController,
            package_controller_1.UserPackageController,
            bot_controller_1.UserBotController,
            bot_trading_controller_1.UserBotTradingController,
        ],
        providers: [
            user_1.UserService,
            verify_token_1.VerifyTokenService,
            mail_1.MailResource,
            sendgrid_transport_1.SendGridTransport,
            user_role_1.UserRoleService,
            auth_user_role_1.AuthUserRoleService,
            role_1.RoleService,
            transaction_1.TransactionService,
            coinpayment_1.CoinpaymentResource,
            coinpayment_transport_1.CoinpaymentTransport,
            app_setting_1.AppSettingService,
            transaction_1.TransactionService,
            transaction_log_1.TransactionLogService,
            bot_setting_1.BotSettingService,
            transaction_event_1.TransactionEvent,
            package_1.PackageService,
            storage_1.StorageResource,
            google_transport_1.GCloudTransport,
            coinpayment_2.CoinpaymentService,
            payment_1.PaymentService,
            resolution_1.ResolutionService,
            currency_1.CurrencyService,
            session_1.SessionService,
            general_setting_role_1.GeneralSettingRoleService,
            user_list_bot_1.UserListBotHandler,
            bot_1.BotService,
            bot_trading_1.BotTradingService,
            user_list_bot_signal_1.UserListSignalHandler,
            user_list_bot_paid_1.UserListBotPaidHandler,
            user_add_favorite_signal_1.UserAddFavoriteSignalHandler,
            user_remove_favorite_signal_1.UserRemoveFavoriteSignalHandler,
            list_asset_log_1.ListAssetLogHandler,
            user_list_bot_plan_1.UserListBotPlanHandler,
            user_list_bot_trading_1.UserListBotTradingHandler,
            user_get_bot_trading_1.UserGetBotTradingHandler,
            user_list_bot_trading_plan_1.UserListBotTradingPlanHandler,
            connect_broker_1.ConnectBrokerServerHandler,
            forex_1.MetaapiResource,
            logout_broker_1.LogoutBrokerServerHandler,
            user_update_bot_trading_status_1.UserUpdateBotTradingStatusHandler,
            bot_trading_event_1.BotTradingEvent,
            user_list_bot_trading_history_1.UserListBotTradingHistoryHandler,
            user_list_bot_trading_pnl_1.UserListBotTradingPNLHandler,
            select_and_connect_broker_1.SelectAndConnectBrokerServerHandler,
            deactive_user_bot_trading_1.DeactivateUserBotTradingHandler,
            broker_1.BrokerResource,
            xm_1.XMResource,
            axi_1.AxiResource,
            xm_transport_1.XMTransport,
            axi_transport_1.AxiTransport,
            user_balance_and_fee_1.BalanceAndFeeDataHandler,
            user_create_balance_and_fee_1.CreateBalanceAndFeeDataHandler,
            upload_broker_profile_1.UploadBrokerProfileHandler,
            metaapi_1.BrokerDefaultResource,
            metaapi_transport_1.MetaapiTransport
        ],
    })
], APIV1Controller);
let APIV2Controller = class APIV2Controller {
};
APIV2Controller = __decorate([
    common_1.Module({
        imports: [repositories_1.DatabaseModule],
        controllers: [transaction_controller_2.TransactionV2Controller, role_controller_2.UserRoleV2Controller],
        providers: [
            session_1.SessionService,
            general_setting_role_1.GeneralSettingRoleService,
            role_1.RoleService,
            user_create_transaction_v2_1.CreateTransactionV2Handler,
            check_Items_1.CheckItemService,
            save_items_1.SaveItemService,
            cmpayment_1.CmpaymentResource,
            cmpayment_transport_1.CmpaymentTransport,
            user_get_transaction_v2_1.GetTransactionV2Handler,
            user_list_transaction_v2_1.ListTransactionV2Handler,
            ipn_transaction_v2_1.IPNTransactionV2Handler,
            user_list_role_1.UserListRoleHandler,
            user_list_role_plan_1.UserListRolePlanHandler,
            user_create_balance_and_fee_1.CreateBalanceAndFeeDataHandler,
        ],
    })
], APIV2Controller);
let APIPublicController = class APIPublicController {
};
APIPublicController = __decorate([
    common_1.Module({
        imports: [repositories_1.DatabaseModule],
        controllers: [public_controller_1.PublicController],
        providers: [
            session_1.SessionService,
            general_setting_role_1.GeneralSettingRoleService,
            role_1.RoleService,
            bot_setting_1.BotSettingService,
        ],
    })
], APIPublicController);
const routes = [
    { path: '', module: APIPublicController },
    { path: 'api/v1/user', module: APIV1Controller },
    { path: 'api/v2/user', module: APIV2Controller },
];
let ApiV1UserControllerModule = class ApiV1UserControllerModule {
};
ApiV1UserControllerModule = __decorate([
    common_1.Module({
        imports: [
            core_1.RouterModule.register(routes),
            APIV1Controller,
            APIV2Controller,
            APIPublicController,
            event_emitter_1.EventEmitterModule.forRoot(),
        ],
        providers: [],
    })
], ApiV1UserControllerModule);
exports.ApiV1UserControllerModule = ApiV1UserControllerModule;
//# sourceMappingURL=index.js.map