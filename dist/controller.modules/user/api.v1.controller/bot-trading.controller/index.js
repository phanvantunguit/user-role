"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserBotTradingController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const app_setting_1 = require("../../../../const/app-setting");
const bot_1 = require("../../../../const/bot");
const transaction_1 = require("../../../../const/transaction");
const bot_trading_repository_1 = require("../../../../repositories/bot-trading.repository");
const merchant_repository_1 = require("../../../../repositories/merchant.repository");
const app_setting_2 = require("../../../../services/app-setting");
const connect_broker_1 = require("../../../../usecases/bot/connect-broker");
const validate_1 = require("../../../../usecases/bot/connect-broker/validate");
const logout_broker_1 = require("../../../../usecases/bot/logout-broker");
const validate_2 = require("../../../../usecases/bot/logout-broker/validate");
const select_and_connect_broker_1 = require("../../../../usecases/bot/select-and-connect-broker");
const user_get_bot_trading_1 = require("../../../../usecases/bot/user-get-bot-trading");
const user_list_bot_trading_1 = require("../../../../usecases/bot/user-list-bot-trading");
const user_list_bot_trading_history_1 = require("../../../../usecases/bot/user-list-bot-trading-history");
const validate_3 = require("../../../../usecases/bot/user-list-bot-trading-history/validate");
const user_list_bot_trading_plan_1 = require("../../../../usecases/bot/user-list-bot-trading-plan");
const user_list_bot_trading_pnl_1 = require("../../../../usecases/bot/user-list-bot-trading-pnl");
const validate_4 = require("../../../../usecases/bot/user-list-bot-trading-pnl/validate");
const validate_5 = require("../../../../usecases/bot/user-list-bot-trading/validate");
const validate_6 = require("../../../../usecases/bot/user-list-bot/validate");
const user_update_bot_trading_status_1 = require("../../../../usecases/bot/user-update-bot-trading-status");
const validate_7 = require("../../../../usecases/bot/user-update-bot-trading-status/validate");
const handle_error_util_1 = require("../../../../utils/handle-error.util");
const response_util_1 = require("../../../../utils/response.util");
const const_2 = require("../../const");
const dto_1 = require("./dto");
const validate_8 = require("../../../../usecases/bot/user-balance-and-fee/validate");
const user_balance_and_fee_1 = require("../../../../usecases/bot/user-balance-and-fee");
const validate_9 = require("../../../../usecases/bot/user-create-balance-and-fee/validate");
const user_create_balance_and_fee_1 = require("../../../../usecases/bot/user-create-balance-and-fee");
const platform_express_1 = require("@nestjs/platform-express");
const upload_broker_profile_1 = require("../../../../usecases/bot/upload-broker-profile");
const validate_10 = require("../../../../usecases/bot/upload-broker-profile/validate");
let UserBotTradingController = class UserBotTradingController {
    constructor(userListBotTradingHandler, botTradingRepository, userListBotTradingPlanHandler, connectBrokerServerHandler, logoutBrokerServerHandler, userUpdateBotTradingStatusHandler, userListBotTradingHistoryHandler, userListBotTradingPNLHandler, userGetBotTradingHandler, merchantRepository, selectAndConnectBrokerServerHandler, appSettingService, balanceAndFeeDataHandler, createBalanceAndFeeDataHandler, uploadBrokerProfileHandler) {
        this.userListBotTradingHandler = userListBotTradingHandler;
        this.botTradingRepository = botTradingRepository;
        this.userListBotTradingPlanHandler = userListBotTradingPlanHandler;
        this.connectBrokerServerHandler = connectBrokerServerHandler;
        this.logoutBrokerServerHandler = logoutBrokerServerHandler;
        this.userUpdateBotTradingStatusHandler = userUpdateBotTradingStatusHandler;
        this.userListBotTradingHistoryHandler = userListBotTradingHistoryHandler;
        this.userListBotTradingPNLHandler = userListBotTradingPNLHandler;
        this.userGetBotTradingHandler = userGetBotTradingHandler;
        this.merchantRepository = merchantRepository;
        this.selectAndConnectBrokerServerHandler = selectAndConnectBrokerServerHandler;
        this.appSettingService = appSettingService;
        this.balanceAndFeeDataHandler = balanceAndFeeDataHandler;
        this.createBalanceAndFeeDataHandler = createBalanceAndFeeDataHandler;
        this.uploadBrokerProfileHandler = uploadBrokerProfileHandler;
    }
    async listBot(res, req) {
        var _a;
        const { m_affiliate } = req.headers;
        let merchantId;
        if (m_affiliate) {
            const merchant = await this.merchantRepository.findOne({
                code: m_affiliate,
                status: const_1.MERCHANT_STATUS.ACTIVE,
            });
            if (merchant) {
                merchantId = merchant.id;
            }
        }
        const user_id = (_a = req.user) === null || _a === void 0 ? void 0 : _a.user_id;
        const result = await this.userListBotTradingHandler.execute(user_id, merchantId);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async BalanceAndFee(res) {
        const result = await this.balanceAndFeeDataHandler.execute();
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listBotTradingHistory(res, req, query) {
        var _a;
        const user_id = (_a = req.user) === null || _a === void 0 ? void 0 : _a.user_id;
        const result = await this.userListBotTradingHistoryHandler.execute(query, user_id);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listBotPlan(res, req, query) {
        const { status } = query;
        const user_id = req.user.user_id;
        const result = await this.userListBotTradingPlanHandler.execute(user_id, status);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listBroker(res, req) {
        response_util_1.resSuccess({
            payload: bot_1.Brokers,
            res,
        });
    }
    async listBrokerServer(res) {
        const data = await this.appSettingService.getAppSetting({
            name: app_setting_1.APP_SETTING.BROKER_SERVER,
        });
        let result = bot_1.BrokerServers;
        if (data.value) {
            try {
                result = JSON.parse(data.value);
            }
            catch (error) {
                console.log('error', error);
            }
        }
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getRoleDetail(id, res, req) {
        var _a;
        const user_id = (_a = req.user) === null || _a === void 0 ? void 0 : _a.user_id;
        const result = await this.userGetBotTradingHandler.execute(id, user_id);
        if (result) {
            result['category'] = transaction_1.ORDER_CATEGORY.TBOT;
            response_util_1.resSuccess({
                payload: result,
                res,
            });
        }
        else {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
    }
    async getBotPNL(id, res, req, query) {
        var _a;
        const user_id = (_a = req.user) === null || _a === void 0 ? void 0 : _a.user_id;
        const { strategy_trade } = query;
        const result = await this.userListBotTradingPNLHandler.execute(user_id, id, strategy_trade);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async connectBroker(res, req, body) {
        const user_id = req.user.user_id;
        const merchant_code = req.user.merchant_code;
        const result = await this.connectBrokerServerHandler.execute(body, user_id, merchant_code);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async selectAndConnectBroker(res, req, body) {
        const user_id = req.user.user_id;
        const merchant_code = req.user.merchant_code;
        const result = await this.selectAndConnectBrokerServerHandler.execute(body, user_id, merchant_code);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async logoutBroker(res, req, body) {
        const user_id = req.user.user_id;
        const result = await this.logoutBrokerServerHandler.execute(body, user_id);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateStatus(res, req, body) {
        const user_id = req.user.user_id;
        const merchant_code = req.user.merchant_code;
        const result = await this.userUpdateBotTradingStatusHandler.execute(body, user_id, merchant_code);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async CreateBalanceAndFee(res, req, body) {
        const result = await this.createBalanceAndFeeDataHandler.execute(body);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async uploadFile(file, body, res, req) {
        const { platform, broker_code, account_id } = body;
        if (file) {
            const user_id = req.user.user_id;
            const merchant_code = req.user.merchant_code;
            const result = await this.uploadBrokerProfileHandler.execute(broker_code, file, platform, merchant_code, account_id, user_id);
            response_util_1.resSuccess({
                payload: result,
                res,
            });
        }
        else {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BAD_REQUEST));
        }
    }
};
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('/list'),
    swagger_1.ApiResponse({
        status: 200,
        type: class UserListBotTradingOutputMap extends swagger_1.IntersectionType(validate_5.UserListBotTradingOutput, const_2.BaseApiOutput) {
        },
        description: 'List bot trading for user',
    }),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "listBot", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('/balance-and-fee'),
    swagger_1.ApiResponse({
        status: 200,
        type: class BalanceAndFeeDataOutputMap extends swagger_1.IntersectionType(validate_8.BalanceAndFeeDataOutput, const_2.BaseApiOutput) {
        },
        description: 'Balance And Fee Name Default',
    }),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "BalanceAndFee", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('/trade-history'),
    swagger_1.ApiResponse({
        status: 200,
        type: class UserListTradeHistoryPayloadOutputMap extends swagger_1.IntersectionType(validate_3.UserListTradeHistoryPayloadOutput, const_2.BaseApiOutput) {
        },
        description: 'List bot trading history for user',
    }),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __param(2, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, validate_3.UserListTradeHistoryInput]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "listBotTradingHistory", null);
__decorate([
    common_1.Get('/plans'),
    swagger_1.ApiResponse({
        status: 200,
        type: class UserListBotOutputMap extends swagger_1.IntersectionType(validate_6.UserListBotOutput, const_2.BaseApiOutput) {
        },
        description: 'List bot trading for user',
    }),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __param(2, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "listBotPlan", null);
__decorate([
    common_1.Get('/brokers'),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "listBroker", null);
__decorate([
    common_1.Get('/broker-servers'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "listBrokerServer", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('/:id'),
    swagger_1.ApiResponse({
        status: 200,
        type: class DetailBotTradingPayloadDtoMap extends swagger_1.IntersectionType(dto_1.DetailBotTradingPayloadDto, const_2.BaseApiOutput) {
        },
        description: 'Get bot trading detail',
    }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "getRoleDetail", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('/:id/pnl'),
    swagger_1.ApiResponse({
        status: 200,
        type: class UserListBotTradingPnlPayloadOutputMap extends swagger_1.IntersectionType(validate_4.UserListBotTradingPnlPayloadOutput, const_2.BaseApiOutput) {
        },
        description: 'Get bot trading detail',
    }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __param(3, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "getBotPNL", null);
__decorate([
    common_1.Put('/connect'),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, validate_1.ConnectBrokerServerInput]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "connectBroker", null);
__decorate([
    common_1.Put('/select-and-connect'),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, validate_1.ConnectBrokerServerInput]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "selectAndConnectBroker", null);
__decorate([
    common_1.Put('/logout'),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, validate_2.LogoutBrokerServerInput]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "logoutBroker", null);
__decorate([
    common_1.Put('/update-status'),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, validate_7.UserUpdateBotTradingStatusInput]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "updateStatus", null);
__decorate([
    common_1.Post('/balance-and-fee'),
    swagger_1.ApiResponse({
        status: 200,
        type: class UserCreateBalanceAndFeeDataOutputMap extends swagger_1.IntersectionType(validate_9.CreateBalanceAndFeeDataOutput, const_2.BaseApiOutput) {
        },
        description: 'User create balance and fee',
    }),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, validate_9.CreateBalanceAndFeeDataInput]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "CreateBalanceAndFee", null);
__decorate([
    common_1.Post('/upload-profile'),
    swagger_1.ApiConsumes('multipart/form-data'),
    swagger_1.ApiBody({
        schema: {
            type: 'object',
            properties: {
                broker_code: { type: 'string' },
                platform: { type: 'string' },
                account_id: { type: 'string' },
                file: {
                    type: 'string',
                    format: 'binary',
                },
            },
        },
    }),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file')),
    swagger_1.ApiResponse({
        status: 200,
        type: class UploadBrokerProfileOutputMap extends swagger_1.IntersectionType(validate_10.UploadBrokerProfileOutput, const_2.BaseApiOutput) {
        },
        description: 'User create balance and fee',
    }),
    __param(0, common_1.UploadedFile()),
    __param(1, common_1.Body()),
    __param(2, common_1.Res()),
    __param(3, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], UserBotTradingController.prototype, "uploadFile", null);
UserBotTradingController = __decorate([
    swagger_1.ApiSecurity('Refresh-Token'),
    swagger_1.ApiBearerAuth(),
    common_1.UseGuards(const_2.UserAuthGuard),
    swagger_1.ApiTags('user/bot-trading'),
    common_1.Controller('bot-trading'),
    __param(0, common_1.Inject(user_list_bot_trading_1.UserListBotTradingHandler)),
    __param(1, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(2, common_1.Inject(user_list_bot_trading_plan_1.UserListBotTradingPlanHandler)),
    __param(3, common_1.Inject(connect_broker_1.ConnectBrokerServerHandler)),
    __param(4, common_1.Inject(logout_broker_1.LogoutBrokerServerHandler)),
    __param(5, common_1.Inject(user_update_bot_trading_status_1.UserUpdateBotTradingStatusHandler)),
    __param(6, common_1.Inject(user_list_bot_trading_history_1.UserListBotTradingHistoryHandler)),
    __param(7, common_1.Inject(user_list_bot_trading_pnl_1.UserListBotTradingPNLHandler)),
    __param(8, common_1.Inject(user_get_bot_trading_1.UserGetBotTradingHandler)),
    __param(9, common_1.Inject(merchant_repository_1.MerchantRepository)),
    __param(10, common_1.Inject(select_and_connect_broker_1.SelectAndConnectBrokerServerHandler)),
    __param(11, common_1.Inject(app_setting_2.AppSettingService)),
    __param(12, common_1.Inject(user_balance_and_fee_1.BalanceAndFeeDataHandler)),
    __param(13, common_1.Inject(user_create_balance_and_fee_1.CreateBalanceAndFeeDataHandler)),
    __param(14, common_1.Inject(upload_broker_profile_1.UploadBrokerProfileHandler)),
    __metadata("design:paramtypes", [user_list_bot_trading_1.UserListBotTradingHandler,
        bot_trading_repository_1.BotTradingRepository,
        user_list_bot_trading_plan_1.UserListBotTradingPlanHandler,
        connect_broker_1.ConnectBrokerServerHandler,
        logout_broker_1.LogoutBrokerServerHandler,
        user_update_bot_trading_status_1.UserUpdateBotTradingStatusHandler,
        user_list_bot_trading_history_1.UserListBotTradingHistoryHandler,
        user_list_bot_trading_pnl_1.UserListBotTradingPNLHandler,
        user_get_bot_trading_1.UserGetBotTradingHandler,
        merchant_repository_1.MerchantRepository,
        select_and_connect_broker_1.SelectAndConnectBrokerServerHandler,
        app_setting_2.AppSettingService,
        user_balance_and_fee_1.BalanceAndFeeDataHandler,
        user_create_balance_and_fee_1.CreateBalanceAndFeeDataHandler,
        upload_broker_profile_1.UploadBrokerProfileHandler])
], UserBotTradingController);
exports.UserBotTradingController = UserBotTradingController;
//# sourceMappingURL=index.js.map