import { BOT_STATUS } from 'src/const';
export declare class DetailBotTradingDto {
    id: string;
    name: string;
    type: string;
    description: string;
    status: BOT_STATUS;
    price: string;
    currency: string;
    pnl: string;
    max_drawdown: string;
    max_drawdown_change_percent: string;
    work_based_on: string[];
    image_url: string;
    created_at: number;
    updated_at: number;
    order: number;
    category: string;
    back_test: string;
    bought: number;
}
export declare class DetailBotTradingPayloadDto {
    payload: DetailBotTradingDto;
}
