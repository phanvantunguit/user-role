"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserBotController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const transaction_1 = require("../../../../const/transaction");
const bot_signal_repository_1 = require("../../../../repositories/bot-signal.repository");
const bot_repository_1 = require("../../../../repositories/bot.repository");
const user_add_favorite_signal_1 = require("../../../../usecases/bot/user-add-favorite-signal");
const user_list_bot_1 = require("../../../../usecases/bot/user-list-bot");
const user_list_bot_paid_1 = require("../../../../usecases/bot/user-list-bot-paid");
const user_list_bot_plan_1 = require("../../../../usecases/bot/user-list-bot-plan");
const user_list_bot_signal_1 = require("../../../../usecases/bot/user-list-bot-signal");
const validate_1 = require("../../../../usecases/bot/user-list-bot-signal/validate");
const validate_2 = require("../../../../usecases/bot/user-list-bot/validate");
const user_remove_favorite_signal_1 = require("../../../../usecases/bot/user-remove-favorite-signal");
const handle_error_util_1 = require("../../../../utils/handle-error.util");
const response_util_1 = require("../../../../utils/response.util");
const const_2 = require("../../const");
const dto_1 = require("./dto");
let UserBotController = class UserBotController {
    constructor(botRepository, userListBotHandler, userListSignalHandler, userListBotPaidHandler, userAddFavoriteSignalHandler, userRemoveFavoriteSignalHandler, userListBotPlanHandler) {
        this.botRepository = botRepository;
        this.userListBotHandler = userListBotHandler;
        this.userListSignalHandler = userListSignalHandler;
        this.userListBotPaidHandler = userListBotPaidHandler;
        this.userAddFavoriteSignalHandler = userAddFavoriteSignalHandler;
        this.userRemoveFavoriteSignalHandler = userRemoveFavoriteSignalHandler;
        this.userListBotPlanHandler = userListBotPlanHandler;
    }
    async listBot(res, req) {
        var _a;
        const user_id = (_a = req.user) === null || _a === void 0 ? void 0 : _a.user_id;
        const result = await this.userListBotHandler.execute(user_id);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listBotPaid(res, req) {
        const user_id = req.user.user_id;
        const result = await this.userListBotPaidHandler.execute(user_id);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listBotPlan(res, req) {
        const user_id = req.user.user_id;
        const result = await this.userListBotPlanHandler.execute(user_id);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listBotSignal(res, query, req) {
        const user_id = req.user.user_id;
        const result = await this.userListSignalHandler.execute(query, user_id);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getRoleDetail(id, res) {
        const result = await this.botRepository.findById(id);
        if (result) {
            result['category'] = transaction_1.ORDER_CATEGORY.SBOT;
            response_util_1.resSuccess({
                payload: result,
                res,
            });
        }
        else {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
    }
    async addFavoriteSignal(id, res, req) {
        const user_id = req.user.user_id;
        const result = await this.userAddFavoriteSignalHandler.execute(user_id, id);
        if (result) {
            response_util_1.resSuccess({
                payload: result,
                res,
            });
        }
        else {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
    }
    async removeFavoriteSignal(id, res, req) {
        const user_id = req.user.user_id;
        const result = await this.userRemoveFavoriteSignalHandler.execute(user_id, id);
        if (result) {
            response_util_1.resSuccess({
                payload: result,
                res,
            });
        }
        else {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
    }
};
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('/list'),
    swagger_1.ApiResponse({
        status: 200,
        type: class UserListBotOutputMap extends swagger_1.IntersectionType(validate_2.UserListBotOutput, const_2.BaseApiOutput) {
        },
        description: 'List bot for user',
    }),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserBotController.prototype, "listBot", null);
__decorate([
    common_1.Get('/paid'),
    swagger_1.ApiResponse({
        status: 200,
        type: class UserListBotOutputMap extends swagger_1.IntersectionType(validate_2.UserListBotOutput, const_2.BaseApiOutput) {
        },
        description: 'List bot for user',
    }),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserBotController.prototype, "listBotPaid", null);
__decorate([
    common_1.Get('/plans'),
    swagger_1.ApiResponse({
        status: 200,
        type: class UserListBotOutputMap extends swagger_1.IntersectionType(validate_2.UserListBotOutput, const_2.BaseApiOutput) {
        },
        description: 'List bot for user',
    }),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserBotController.prototype, "listBotPlan", null);
__decorate([
    common_1.Get('/signals'),
    swagger_1.ApiResponse({
        status: 200,
        type: class UserListBotSignalPayloadOutputMap extends swagger_1.IntersectionType(validate_1.UserListBotSignalPayloadOutput, const_2.BaseApiOutput) {
        },
        description: 'List bot signal for user',
    }),
    __param(0, common_1.Res()),
    __param(1, common_1.Query()),
    __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_1.UserListBotSignalInput, Object]),
    __metadata("design:returntype", Promise)
], UserBotController.prototype, "listBotSignal", null);
__decorate([
    common_1.Get('/:id'),
    swagger_1.ApiResponse({
        status: 200,
        type: class DetailBotPayloadDtoMap extends swagger_1.IntersectionType(dto_1.DetailBotPayloadDto, const_2.BaseApiOutput) {
        },
        description: 'Get bot detail',
    }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], UserBotController.prototype, "getRoleDetail", null);
__decorate([
    common_1.Post('/favorite-signal/:id'),
    swagger_1.ApiResponse({
        status: 200,
        description: 'Add favorite signal',
    }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], UserBotController.prototype, "addFavoriteSignal", null);
__decorate([
    common_1.Delete('/favorite-signal/:id'),
    swagger_1.ApiResponse({
        status: 200,
        description: 'Add remove signal',
    }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], UserBotController.prototype, "removeFavoriteSignal", null);
UserBotController = __decorate([
    swagger_1.ApiSecurity('Refresh-Token'),
    swagger_1.ApiBearerAuth(),
    common_1.UseGuards(const_2.UserAuthGuard),
    swagger_1.ApiTags('user/bot'),
    common_1.Controller('bot'),
    __param(0, common_1.Inject(bot_repository_1.BotRepository)),
    __param(1, common_1.Inject(user_list_bot_1.UserListBotHandler)),
    __param(2, common_1.Inject(user_list_bot_signal_1.UserListSignalHandler)),
    __param(3, common_1.Inject(user_list_bot_paid_1.UserListBotPaidHandler)),
    __param(4, common_1.Inject(user_add_favorite_signal_1.UserAddFavoriteSignalHandler)),
    __param(5, common_1.Inject(user_remove_favorite_signal_1.UserRemoveFavoriteSignalHandler)),
    __param(6, common_1.Inject(user_list_bot_plan_1.UserListBotPlanHandler)),
    __metadata("design:paramtypes", [bot_repository_1.BotRepository,
        user_list_bot_1.UserListBotHandler,
        user_list_bot_signal_1.UserListSignalHandler,
        user_list_bot_paid_1.UserListBotPaidHandler,
        user_add_favorite_signal_1.UserAddFavoriteSignalHandler,
        user_remove_favorite_signal_1.UserRemoveFavoriteSignalHandler,
        user_list_bot_plan_1.UserListBotPlanHandler])
], UserBotController);
exports.UserBotController = UserBotController;
//# sourceMappingURL=index.js.map