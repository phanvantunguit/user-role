import { BOT_STATUS } from 'src/const';
export declare class DetailBotDto {
    id: string;
    name: string;
    type: string;
    description: string;
    status: BOT_STATUS;
    price: string;
    currency: string;
    work_based_on: string[];
    image_url: string;
    created_at: number;
    updated_at: number;
    order: number;
    category: string;
}
export declare class DetailBotPayloadDto {
    payload: DetailBotDto;
}
