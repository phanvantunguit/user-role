"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DetailBotPayloadDto = exports.DetailBotDto = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const class_transformer_1 = require("class-transformer");
const transaction_1 = require("../../../../const/transaction");
class DetailBotDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailBotDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'MBC',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailBotDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'FUTURE',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailBotDto.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'Detect strong pin bar candlestick patterns with delta divergence',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailBotDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: Object.values(const_1.BOT_STATUS).join(','),
        example: const_1.BOT_STATUS.COMINGSOON,
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailBotDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '96',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailBotDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'USD',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailBotDto.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: [
            "Candle's wick length.",
            'Delta parameter.',
            'Stacked Imbalance parameter.',
            'Footprint parameter.',
        ],
        isArray: true,
    }),
    class_transformer_1.Type(() => String),
    __metadata("design:type", Array)
], DetailBotDto.prototype, "work_based_on", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'https://static-dev.cextrading.io/images/cm-user-roles/1672212992415.png',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailBotDto.prototype, "image_url", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], DetailBotDto.prototype, "created_at", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], DetailBotDto.prototype, "updated_at", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 1,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], DetailBotDto.prototype, "order", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: transaction_1.ORDER_CATEGORY.SBOT,
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailBotDto.prototype, "category", void 0);
exports.DetailBotDto = DetailBotDto;
class DetailBotPayloadDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Bot package info',
    }),
    class_validator_1.IsObject(),
    __metadata("design:type", DetailBotDto)
], DetailBotPayloadDto.prototype, "payload", void 0);
exports.DetailBotPayloadDto = DetailBotPayloadDto;
//# sourceMappingURL=dto.js.map