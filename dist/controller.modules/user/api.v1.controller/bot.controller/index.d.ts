import { Response } from 'express';
import { BotRepository } from 'src/repositories/bot.repository';
import { UserAddFavoriteSignalHandler } from 'src/usecases/bot/user-add-favorite-signal';
import { UserListBotHandler } from 'src/usecases/bot/user-list-bot';
import { UserListBotPaidHandler } from 'src/usecases/bot/user-list-bot-paid';
import { UserListBotPlanHandler } from 'src/usecases/bot/user-list-bot-plan';
import { UserListSignalHandler } from 'src/usecases/bot/user-list-bot-signal';
import { UserListBotSignalInput } from 'src/usecases/bot/user-list-bot-signal/validate';
import { UserRemoveFavoriteSignalHandler } from 'src/usecases/bot/user-remove-favorite-signal';
export declare class UserBotController {
    private botRepository;
    private userListBotHandler;
    private userListSignalHandler;
    private userListBotPaidHandler;
    private userAddFavoriteSignalHandler;
    private userRemoveFavoriteSignalHandler;
    private userListBotPlanHandler;
    constructor(botRepository: BotRepository, userListBotHandler: UserListBotHandler, userListSignalHandler: UserListSignalHandler, userListBotPaidHandler: UserListBotPaidHandler, userAddFavoriteSignalHandler: UserAddFavoriteSignalHandler, userRemoveFavoriteSignalHandler: UserRemoveFavoriteSignalHandler, userListBotPlanHandler: UserListBotPlanHandler);
    listBot(res: Response, req: any): Promise<void>;
    listBotPaid(res: Response, req: any): Promise<void>;
    listBotPlan(res: Response, req: any): Promise<void>;
    listBotSignal(res: Response, query: UserListBotSignalInput, req: any): Promise<void>;
    getRoleDetail(id: string, res: Response): Promise<void>;
    addFavoriteSignal(id: string, res: Response, req: any): Promise<void>;
    removeFavoriteSignal(id: string, res: Response, req: any): Promise<void>;
}
