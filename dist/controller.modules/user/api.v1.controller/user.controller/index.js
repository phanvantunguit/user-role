"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const config_1 = require("../../../../config");
const const_1 = require("../../../../const");
const app_setting_1 = require("../../../../const/app-setting");
const app_setting_2 = require("../../../../services/app-setting");
const bot_setting_1 = require("../../../../services/bot-setting");
const user_1 = require("../../../../services/user");
const handle_error_util_1 = require("../../../../utils/handle-error.util");
const response_util_1 = require("../../../../utils/response.util");
const const_2 = require("../../const");
const dto_1 = require("./dto");
const platform_express_1 = require("@nestjs/platform-express");
const storage_1 = require("../../../../resources/storage");
const resolution_1 = require("../../../../services/resolution");
const currency_1 = require("../../../../services/currency");
const setting_repository_1 = require("../../../../repositories/setting.repository");
const list_asset_log_1 = require("../../../../usecases/transaction/list-asset-log");
const validate_1 = require("../../../../usecases/transaction/list-asset-log/validate");
const dto_2 = require("../../../admin/api.v1.controller/setting.controller/dto");
const merchant_repository_1 = require("../../../../repositories/merchant.repository");
let UserController = class UserController {
    constructor(userService, appSettingService, botSettingService, storageResource, resolutionService, currencyService, settingRepo, listAssetLogHandler, merchantRepository) {
        this.userService = userService;
        this.appSettingService = appSettingService;
        this.botSettingService = botSettingService;
        this.storageResource = storageResource;
        this.resolutionService = resolutionService;
        this.currencyService = currencyService;
        this.settingRepo = settingRepo;
        this.listAssetLogHandler = listAssetLogHandler;
        this.merchantRepository = merchantRepository;
    }
    async register(body, res, req) {
        const { m_affiliate } = req.headers;
        const { email, first_name, last_name, password } = body;
        const settingRegister = await this.settingRepo.findAppSetting({
            name: app_setting_1.APP_SETTING.ON_OFF_REGISTER,
        });
        if (settingRegister.length === 0 ||
            settingRegister[0].value === app_setting_1.ON_OFF_REGISTER.ON) {
            const result = await this.userService.registerUser({
                email,
                first_name,
                last_name,
                password,
                merchant_code: m_affiliate,
            });
            response_util_1.resSuccess({
                payload: result,
                res,
            });
        }
        else {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.NO_PERMISSION));
        }
    }
    async resend(body, res, req) {
        const { m_affiliate } = req.headers;
        const result = await this.userService.resendEmailVerify({
            email: body.email,
            merchant_code: m_affiliate || app_setting_1.APP_SETTING.MERCHANT_CODE_DEFAULT,
        });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async login(req, body, res) {
        const { m_affiliate } = req.headers;
        const { email, password } = body;
        const userAgent = req.get('user-agent');
        const ipAddress = req.headers['x-forwarded-for'] || req.socket.remoteAddress || null;
        const result = await this.userService.loginUser({
            email,
            password,
            user_type: const_1.USER_TYPE.USER,
        }, userAgent, ipAddress, m_affiliate);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async verifyEmail(token, res) {
        try {
            const result = await this.userService.verifyEmailUser(token);
            let urlReplace = '';
            if (result.is_admin) {
                urlReplace = `${config_1.APP_CONFIG.DASHBOARD_ADMIN_BASE_URL}/login`;
            }
            else {
                const merchant = await this.merchantRepository.findOne({
                    code: result.merchant_code,
                });
                urlReplace = merchant
                    ? `${merchant.domain}/login`
                    : `${config_1.APP_CONFIG.DASHBOARD_CLIENT_BASE_URL}/login`;
            }
            res.redirect(urlReplace);
        }
        catch (error) {
            let message = 'Verify email failed';
            if (error && error.message) {
                message = error.message;
            }
            const htmlFailed = `
      <div class="heading"
      style="background: #942626;display:flex; align-items: center;justify-content: space-between;margin-left:-20px;margin-right: -20px;box-shadow: 0 5px 10px -5px green;padding-left: 30px;padding-right:30px;">
      <h1 style="margin: auto; line-height: 100px;padding-left: 10px;color:white">${message}</h1>
  </div>
      `;
            res.send(htmlFailed);
        }
    }
    async userConfirmEmail(body, res) {
        const result = await this.userService.userConfirmEmail(body.token);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async forgotPassword(body, res, req) {
        const { m_affiliate } = req.headers;
        const result = await this.userService.sendEmailForgotPassword({
            email: body.email,
            merchant_code: m_affiliate || app_setting_1.APP_SETTING.MERCHANT_CODE_DEFAULT
        });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async resetPassword(body, res) {
        const result = await this.userService.resetPassword(body);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateUserProfile(body, req, res) {
        const user_id = req.user.user_id;
        const dataUpdate = Object.assign(Object.assign({}, body), { id: user_id });
        const result = await this.userService.updateInfo(dataUpdate, false);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getUserProfile(req, res) {
        const { m_affiliate } = req.headers;
        const user_id = req.user.user_id;
        const result = await this.userService.getProfile({
            id: user_id,
        }, m_affiliate);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async checkUsername(req, username, res) {
        const user_id = req.user.user_id;
        const result = await this.userService.checkUsername(username, user_id);
        if (result) {
            res.status(common_1.HttpStatus.OK).send({
                error_code: const_1.ERROR_CODE.SUCCESS.error_code,
                message: 'Username is valid',
            });
        }
        else {
            res.status(common_1.HttpStatus.OK).send(Object.assign({}, const_1.ERROR_CODE.USERNAME_EXISTED));
        }
    }
    async createAppSetting(body, req, res) {
        const user_id = req.user.user_id;
        const result = await this.appSettingService.userCreateAppSetting(Object.assign(Object.assign({}, body), { owner_created: user_id }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listAppSetting(res, req) {
        var _a;
        const user_id = (_a = req.user) === null || _a === void 0 ? void 0 : _a.user_id;
        const result = await this.appSettingService.listAppSetting({ user_id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getAppSetting(name, res, req) {
        var _a;
        const user_id = (_a = req.user) === null || _a === void 0 ? void 0 : _a.user_id;
        const result = await this.appSettingService.getAppSetting({ name, user_id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listBotSetting(res, query) {
        const result = await this.botSettingService.list(Object.assign(Object.assign({}, query), { status: true }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getBotSetting(id, res) {
        const result = await this.botSettingService.get({ id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getResolution(res) {
        const result = await this.resolutionService.listResolution();
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getAssetList(res, req, query) {
        const user_id = req.user.user_id;
        const data = await this.listAssetLogHandler.execute(query, true, user_id);
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async getPaymentMethod(res, query) {
        const { all, category } = query;
        let data;
        if (all === true || all === 'true') {
            data = await this.currencyService.list({});
        }
        else {
            data = await this.currencyService.findCurrencies({ category });
        }
        const result = data.map((c) => {
            return Object.assign(Object.assign({}, c), { type: c.currency });
        });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async changeEmail(req, body, res) {
        const user_id = req.user.user_id;
        const result = await this.userService.changeEmail(Object.assign(Object.assign({}, body), { user_id }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async uploadFile(file, res) {
        if (file) {
            const file_url = await this.storageResource.upload(file);
            response_util_1.resSuccess({
                payload: { file_url },
                res,
            });
        }
        else {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BAD_REQUEST));
        }
    }
    async checkPassword(req, body, res) {
        const user_id = req.user.user_id;
        const result = await this.userService.checkPassword(user_id, body.password);
        if (result) {
            res.status(common_1.HttpStatus.OK).send({
                error_code: const_1.ERROR_CODE.SUCCESS.error_code,
                message: 'Password is correct',
            });
        }
        else {
            res.status(common_1.HttpStatus.OK).send(Object.assign({}, const_1.ERROR_CODE.PASSWORD_INCORRECT));
        }
    }
};
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Post('register'),
    __param(0, common_1.Body()),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostRegisterDto, Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "register", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Post('resend-email-verify'),
    __param(0, common_1.Body()),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostResendEmailVerifyDto, Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "resend", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Post('login'),
    __param(0, common_1.Req()),
    __param(1, common_1.Body()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.PostLoginDto, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "login", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('verify-email/:token'),
    __param(0, common_1.Param('token')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "verifyEmail", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Post('confirm-email'),
    __param(0, common_1.Body()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostEmailConfirmDto, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "userConfirmEmail", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Post('forgot-password'),
    __param(0, common_1.Body()),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostSendEmailForgotPasswordDto, Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "forgotPassword", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Post('reset-password'),
    __param(0, common_1.Body()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostResetPasswordDto, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "resetPassword", null);
__decorate([
    common_1.Put('/profile'),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutUserProfileDto, Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "updateUserProfile", null);
__decorate([
    common_1.Get('/profile'),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getUserProfile", null);
__decorate([
    common_1.Get('/check-username/:username'),
    __param(0, common_1.Req()),
    __param(1, common_1.Param('username')),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "checkUsername", null);
__decorate([
    common_1.Put('app-setting'),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_2.PostAppSettingDto, Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "createAppSetting", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('app-setting/list'),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "listAppSetting", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('app-setting/:name'),
    __param(0, common_1.Param('name')),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getAppSetting", null);
__decorate([
    common_1.Get('bot-setting/list'),
    __param(0, common_1.Res()),
    __param(1, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.GetBotSettingDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "listBotSetting", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('bot-setting/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getBotSetting", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('resolution/list'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getResolution", null);
__decorate([
    common_1.Get('asset/list'),
    swagger_1.ApiResponse({
        status: 200,
        type: class ListAssetLogPayloadOutputMap extends swagger_1.IntersectionType(validate_1.ListAssetLogPayloadOutput, const_2.BaseApiOutput) {
        },
        description: 'List asset for user',
    }),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __param(2, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, validate_1.ListAssetLogInput]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getAssetList", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('currency/list'),
    __param(0, common_1.Res()),
    __param(1, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getPaymentMethod", null);
__decorate([
    common_1.Post('change-email'),
    __param(0, common_1.Req()),
    __param(1, common_1.Body()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.PostChangeEmailDto, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "changeEmail", null);
__decorate([
    common_1.Post('/upload'),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file')),
    __param(0, common_1.UploadedFile()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "uploadFile", null);
__decorate([
    common_1.Put('/check-password'),
    __param(0, common_1.Req()),
    __param(1, common_1.Body()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.PutCheckPasswordDto, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "checkPassword", null);
UserController = __decorate([
    swagger_1.ApiSecurity('Refresh-Token'),
    swagger_1.ApiBearerAuth(),
    common_1.UseGuards(const_2.UserAuthGuard),
    swagger_1.ApiTags('user'),
    common_1.Controller(''),
    __param(6, common_1.Inject(setting_repository_1.SettingRepository)),
    __param(7, common_1.Inject(list_asset_log_1.ListAssetLogHandler)),
    __param(8, common_1.Inject(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [user_1.UserService,
        app_setting_2.AppSettingService,
        bot_setting_1.BotSettingService,
        storage_1.StorageResource,
        resolution_1.ResolutionService,
        currency_1.CurrencyService,
        setting_repository_1.SettingRepository,
        list_asset_log_1.ListAssetLogHandler,
        merchant_repository_1.MerchantRepository])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=index.js.map