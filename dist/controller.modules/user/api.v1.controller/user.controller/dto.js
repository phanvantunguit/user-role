"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PutCheckPasswordDto = exports.GetBotSettingDto = exports.PutUserProfileDto = exports.PostResetPasswordDto = exports.PostSendEmailForgotPasswordDto = exports.PostEmailConfirmDto = exports.PostChangeEmailDto = exports.PostResendEmailVerifyDto = exports.PostLoginDto = exports.PostRegisterDto = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
class PostRegisterDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Email to register account Coinmap',
        example: 'john@gmail.com',
    }),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], PostRegisterDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
        example: '123456Aa',
    }),
    class_validator_1.Matches(new RegExp(const_1.PasswordRegex), {
        message: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
    }),
    __metadata("design:type", String)
], PostRegisterDto.prototype, "password", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'First name',
        example: 'John',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostRegisterDto.prototype, "first_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Last name',
        example: 'Nguyen',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostRegisterDto.prototype, "last_name", void 0);
exports.PostRegisterDto = PostRegisterDto;
class PostLoginDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Email to login Coinmap',
        example: 'john@gmail.com',
    }),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], PostLoginDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'password must be at least 8 characters with upper case letter, lower case letter, and number',
        example: '123456Aa',
    }),
    class_validator_1.Matches(new RegExp(const_1.PasswordRegex), {
        message: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
    }),
    __metadata("design:type", String)
], PostLoginDto.prototype, "password", void 0);
exports.PostLoginDto = PostLoginDto;
class PostResendEmailVerifyDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Email to login Coinmap',
        example: 'john@gmail.com',
    }),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], PostResendEmailVerifyDto.prototype, "email", void 0);
exports.PostResendEmailVerifyDto = PostResendEmailVerifyDto;
class PostChangeEmailDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Email to login Coinmap',
        example: 'john@gmail.com',
    }),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], PostChangeEmailDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Url return',
        example: 'http://localhost:3000/confirm-change-email',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostChangeEmailDto.prototype, "callback_url", void 0);
exports.PostChangeEmailDto = PostChangeEmailDto;
class PostEmailConfirmDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Token to verify',
        example: '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostEmailConfirmDto.prototype, "token", void 0);
exports.PostEmailConfirmDto = PostEmailConfirmDto;
class PostSendEmailForgotPasswordDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Email to reset password',
        example: 'john@gmail.com',
    }),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], PostSendEmailForgotPasswordDto.prototype, "email", void 0);
exports.PostSendEmailForgotPasswordDto = PostSendEmailForgotPasswordDto;
class PostResetPasswordDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Token to reset password',
        example: '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostResetPasswordDto.prototype, "token", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'password must be at least 8 characters with upper case letter, lower case letter, and number',
        example: '123456Aa',
    }),
    class_validator_1.Matches(const_1.PasswordRegex),
    __metadata("design:type", String)
], PostResetPasswordDto.prototype, "password", void 0);
exports.PostResetPasswordDto = PostResetPasswordDto;
class PutUserProfileDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Phone number',
        example: '0987654321',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "phone", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'First name',
        example: 'John',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "first_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Last name',
        example: 'Nguyen',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "last_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Full address',
        example: '123 To Hien Thanh, phuong 10, quan 10, tp HCM',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "address", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Affiliate code',
        example: 'affiliate_code',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "affiliate_code", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Link image',
        example: 'https://static-dev.cextrading.io/1660898281635Untitled.png',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "profile_pic", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Link affiliate',
        example: '',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "link_affiliate", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Referral_code',
        example: '',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "referral_code", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Note updated',
        example: '',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "note_updated", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
        example: '123456Aa',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.Matches(new RegExp(const_1.PasswordRegex), {
        message: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
    }),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "password", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
        example: '123456Aa',
    }),
    class_validator_1.ValidateIf((o) => o.password),
    class_validator_1.Matches(new RegExp(const_1.PasswordRegex), {
        message: 'Old password must be at least 8 characters with upper case letter, lower case letter, and number',
    }),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "old_password", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Username is unique',
        example: 'John',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "username", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Country',
        example: 'Viet Nam',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "country", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'gender: male, female or other',
        example: 'male',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    class_validator_1.IsIn(Object.values(const_1.GENDER)),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "gender", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Year of birth',
        example: '2000',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "year_of_birth", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Phone code',
        example: '+84',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "phone_code", void 0);
exports.PutUserProfileDto = PutUserProfileDto;
class GetBotSettingDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 'Violency Pinbar',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], GetBotSettingDto.prototype, "name", void 0);
exports.GetBotSettingDto = GetBotSettingDto;
class PutCheckPasswordDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Check password',
        example: '143a4vAAA',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutCheckPasswordDto.prototype, "password", void 0);
exports.PutCheckPasswordDto = PutCheckPasswordDto;
function DoesMatch() {
    throw new Error('Function not implemented.');
}
//# sourceMappingURL=dto.js.map