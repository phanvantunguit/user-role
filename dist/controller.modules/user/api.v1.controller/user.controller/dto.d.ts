export declare class PostRegisterDto {
    email: string;
    password: string;
    first_name: string;
    last_name: string;
}
export declare class PostLoginDto {
    email: string;
    password: string;
}
export declare class PostResendEmailVerifyDto {
    email: string;
}
export declare class PostChangeEmailDto {
    email: string;
    callback_url: string;
}
export declare class PostEmailConfirmDto {
    token: string;
}
export declare class PostSendEmailForgotPasswordDto {
    email: string;
}
export declare class PostResetPasswordDto {
    token: string;
    password: string;
}
export declare class PutUserProfileDto {
    phone: string;
    first_name: string;
    last_name: string;
    address: string;
    affiliate_code: string;
    profile_pic: string;
    link_affiliate: string;
    referral_code: string;
    note_updated: string;
    password: string;
    old_password: string;
    username: string;
    country: string;
    gender: string;
    year_of_birth: string;
    phone_code: string;
}
export declare class GetBotSettingDto {
    name: string;
}
export declare class PutCheckPasswordDto {
    password: string;
}
