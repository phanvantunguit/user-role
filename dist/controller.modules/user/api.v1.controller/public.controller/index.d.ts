import { Response } from 'express';
import { AdditionalDataRepository } from 'src/repositories/additional-data.repository';
import { EventStoreRepository } from 'src/repositories/event-store.repository';
import { MerchantAdditionalDataRepository } from 'src/repositories/merchant-additional-data.repository';
import { MerchantRepository } from 'src/repositories/merchant.repository';
import { BotSettingService } from 'src/services/bot-setting';
import { SessionService } from 'src/services/session';
export declare class PublicController {
    private sessionService;
    private botSettingService;
    private merchantRepository;
    private eventStoreRepository;
    private merchantAdditionalDataRepository;
    private additionalDataRepository;
    constructor(sessionService: SessionService, botSettingService: BotSettingService, merchantRepository: MerchantRepository, eventStoreRepository: EventStoreRepository, merchantAdditionalDataRepository: MerchantAdditionalDataRepository, additionalDataRepository: AdditionalDataRepository);
    listRole(req: any, res: Response): Promise<void>;
    getMerchant(req: any, res: Response): Promise<void>;
    getMerchantAdditionalData(req: any, res: Response, query: any): Promise<void>;
    getAdditionalData(req: any, res: Response, query: any): Promise<void>;
    checkSession(req: any, res: Response): Promise<void>;
    listBotSetting(res: Response): Promise<void>;
    getBotSetting(id: string, res: Response): Promise<void>;
    getEventStores(res: Response, query: any): Promise<void>;
}
