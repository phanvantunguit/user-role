"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const config_1 = require("../../../../config");
const const_1 = require("../../../../const");
const app_setting_1 = require("../../../../const/app-setting");
const types_1 = require("../../../../domains/merchant/types");
const additional_data_repository_1 = require("../../../../repositories/additional-data.repository");
const event_store_repository_1 = require("../../../../repositories/event-store.repository");
const merchant_additional_data_repository_1 = require("../../../../repositories/merchant-additional-data.repository");
const merchant_repository_1 = require("../../../../repositories/merchant.repository");
const bot_setting_1 = require("../../../../services/bot-setting");
const session_1 = require("../../../../services/session");
const response_util_1 = require("../../../../utils/response.util");
const const_2 = require("../../const");
let PublicController = class PublicController {
    constructor(sessionService, botSettingService, merchantRepository, eventStoreRepository, merchantAdditionalDataRepository, additionalDataRepository) {
        this.sessionService = sessionService;
        this.botSettingService = botSettingService;
        this.merchantRepository = merchantRepository;
        this.eventStoreRepository = eventStoreRepository;
        this.merchantAdditionalDataRepository = merchantAdditionalDataRepository;
        this.additionalDataRepository = additionalDataRepository;
    }
    async listRole(req, res) {
        const data = {
            version: config_1.APP_CONFIG.VERSION,
        };
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async getMerchant(req, res) {
        var _a;
        const { m_affiliate, origin } = req.headers;
        console.log('merchant req.headers', req.headers);
        const queryMerchant = {
            status: const_1.MERCHANT_STATUS.ACTIVE,
        };
        if (m_affiliate) {
            queryMerchant['code'] = m_affiliate;
        }
        else if (origin) {
            queryMerchant['domain'] = origin;
        }
        const merchant = await this.merchantRepository.findOne(queryMerchant);
        let result;
        if (merchant) {
            delete merchant.config.wallet;
            delete merchant.config.wallet_active_history;
            const brokers = (_a = merchant.config.brokers) === null || _a === void 0 ? void 0 : _a.map((e) => {
                var _a;
                (_a = e === null || e === void 0 ? void 0 : e.referral_setting) === null || _a === void 0 ? true : delete _a.secret_key;
                return e;
            });
            merchant.config.brokers = brokers;
            const additional = await this.merchantAdditionalDataRepository.list({
                merchant_id: merchant.id,
                type: types_1.ADDITIONAL_DATA_TYPE.FAQ,
                status: types_1.MERCHANT_ADDITIONAL_DATA_STATUS.ON,
            });
            result = {
                name: merchant.name,
                code: merchant.code,
                domain: merchant.domain,
                config: merchant.config,
                faq: additional,
            };
        }
        else {
            result = {
                code: app_setting_1.APP_SETTING.MERCHANT_CODE_DEFAULT,
            };
        }
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getMerchantAdditionalData(req, res, query) {
        const { m_affiliate } = req.headers;
        console.log('req.headers', req.headers);
        const { type } = query;
        let result = [];
        if (m_affiliate) {
            const queryMerchant = {
                status: const_1.MERCHANT_STATUS.ACTIVE,
                code: m_affiliate,
            };
            const merchant = await this.merchantRepository.findOne(queryMerchant);
            console.log('merchant', merchant);
            if (merchant && m_affiliate) {
                result = await this.merchantAdditionalDataRepository.list({
                    merchant_id: merchant.id,
                    type,
                    status: types_1.MERCHANT_ADDITIONAL_DATA_STATUS.ON,
                });
            }
        }
        if (result.length === 0) {
            let name;
            if (types_1.STANDALONE_ADDITIONAL.includes(type)) {
                name = types_1.ADDITIONAL_DEFAULT_NAME;
            }
            result = await this.additionalDataRepository.list(name, type);
        }
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getAdditionalData(req, res, query) {
        const { type } = query;
        let name;
        if (types_1.STANDALONE_ADDITIONAL.includes(type)) {
            name = types_1.ADDITIONAL_DEFAULT_NAME;
        }
        let result = await this.additionalDataRepository.list(name, type);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async checkSession(req, res) {
        const result = await this.sessionService.verifySession(req.user);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listBotSetting(res) {
        const result = await this.botSettingService.list({});
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getBotSetting(id, res) {
        const result = await this.botSettingService.get({ id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getEventStores(res, query) {
        const result = await this.eventStoreRepository.findEventStore(query);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
};
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('/health_check'),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], PublicController.prototype, "listRole", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('/merchant'),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], PublicController.prototype, "getMerchant", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('/merchant/additional-data/list'),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __param(2, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], PublicController.prototype, "getMerchantAdditionalData", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('/additional-data/list'),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __param(2, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], PublicController.prototype, "getAdditionalData", null);
__decorate([
    common_1.Get('/check_session'),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], PublicController.prototype, "checkSession", null);
__decorate([
    common_1.Get('bot/list'),
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PublicController.prototype, "listBotSetting", null);
__decorate([
    common_1.Get('bot/:id'),
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], PublicController.prototype, "getBotSetting", null);
__decorate([
    common_1.Get('event-stores'),
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    __param(0, common_1.Res()),
    __param(1, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], PublicController.prototype, "getEventStores", null);
PublicController = __decorate([
    swagger_1.ApiSecurity('Refresh-Token'),
    swagger_1.ApiBearerAuth(),
    common_1.UseGuards(const_2.UserAuthGuard),
    common_1.Controller(''),
    __param(0, common_1.Inject(session_1.SessionService)),
    __param(2, common_1.Inject(merchant_repository_1.MerchantRepository)),
    __param(3, common_1.Inject(event_store_repository_1.EventStoreRepository)),
    __param(4, common_1.Inject(merchant_additional_data_repository_1.MerchantAdditionalDataRepository)),
    __param(5, common_1.Inject(additional_data_repository_1.AdditionalDataRepository)),
    __metadata("design:paramtypes", [session_1.SessionService,
        bot_setting_1.BotSettingService,
        merchant_repository_1.MerchantRepository,
        event_store_repository_1.EventStoreRepository,
        merchant_additional_data_repository_1.MerchantAdditionalDataRepository,
        additional_data_repository_1.AdditionalDataRepository])
], PublicController);
exports.PublicController = PublicController;
//# sourceMappingURL=index.js.map