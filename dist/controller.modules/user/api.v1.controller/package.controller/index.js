"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserPackageController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const authorization_1 = require("../../../../const/authorization");
const types_1 = require("../../../../domains/merchant/types");
const additional_data_repository_1 = require("../../../../repositories/additional-data.repository");
const merchant_additional_data_repository_1 = require("../../../../repositories/merchant-additional-data.repository");
const merchant_repository_1 = require("../../../../repositories/merchant.repository");
const package_1 = require("../../../../services/package");
const response_util_1 = require("../../../../utils/response.util");
const const_2 = require("../../const");
let UserPackageController = class UserPackageController {
    constructor(merchantAdditionalDataRepository, merchantRepository, additionalDataRepository, packageService) {
        this.merchantAdditionalDataRepository = merchantAdditionalDataRepository;
        this.merchantRepository = merchantRepository;
        this.additionalDataRepository = additionalDataRepository;
        this.packageService = packageService;
    }
    async listRole(req, res, query) {
        const { m_affiliate } = req.headers;
        let result = [];
        if (m_affiliate) {
            const merchant = await this.merchantRepository.findOne({
                code: m_affiliate,
            });
            if (merchant && merchant.config.domain_type === authorization_1.MERCHANT_TYPE.OTHERS) {
                if (query.type) {
                    const additional = await this.merchantAdditionalDataRepository.list({
                        merchant_id: merchant.id,
                        type: query.type,
                        status: types_1.MERCHANT_ADDITIONAL_DATA_STATUS.ON,
                    });
                    result = additional;
                }
                return response_util_1.resSuccess({
                    payload: result,
                    res,
                });
            }
        }
        result = await this.packageService.list({ status: true });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getRoleDetail(id, res) {
        const defaultPackage = await this.packageService.get({ id });
        if (defaultPackage) {
            response_util_1.resSuccess({
                payload: defaultPackage,
                res,
            });
        }
        else {
            const result = await this.additionalDataRepository.findById(id);
            response_util_1.resSuccess({
                payload: result,
                res,
            });
        }
    }
};
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('/list'),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __param(2, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], UserPackageController.prototype, "listRole", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Get('/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], UserPackageController.prototype, "getRoleDetail", null);
UserPackageController = __decorate([
    swagger_1.ApiSecurity('Refresh-Token'),
    swagger_1.ApiBearerAuth(),
    common_1.UseGuards(const_2.UserAuthGuard),
    swagger_1.ApiTags('user/package'),
    common_1.Controller('package'),
    __param(0, common_1.Inject(merchant_additional_data_repository_1.MerchantAdditionalDataRepository)),
    __param(1, common_1.Inject(merchant_repository_1.MerchantRepository)),
    __param(2, common_1.Inject(additional_data_repository_1.AdditionalDataRepository)),
    __param(3, common_1.Inject(package_1.PackageService)),
    __metadata("design:paramtypes", [merchant_additional_data_repository_1.MerchantAdditionalDataRepository,
        merchant_repository_1.MerchantRepository,
        additional_data_repository_1.AdditionalDataRepository,
        package_1.PackageService])
], UserPackageController);
exports.UserPackageController = UserPackageController;
//# sourceMappingURL=index.js.map