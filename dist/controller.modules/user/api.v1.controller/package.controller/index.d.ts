import { Response } from 'express';
import { AdditionalDataRepository } from 'src/repositories/additional-data.repository';
import { MerchantAdditionalDataRepository } from 'src/repositories/merchant-additional-data.repository';
import { MerchantRepository } from 'src/repositories/merchant.repository';
import { PackageService } from 'src/services/package';
export declare class UserPackageController {
    private merchantAdditionalDataRepository;
    private merchantRepository;
    private additionalDataRepository;
    private packageService;
    constructor(merchantAdditionalDataRepository: MerchantAdditionalDataRepository, merchantRepository: MerchantRepository, additionalDataRepository: AdditionalDataRepository, packageService: PackageService);
    listRole(req: any, res: Response, query: any): Promise<any>;
    getRoleDetail(id: string, res: Response): Promise<void>;
}
