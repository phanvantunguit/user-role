"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRoleController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const transaction_1 = require("../../../../const/transaction");
const role_1 = require("../../../../services/role");
const handle_error_util_1 = require("../../../../utils/handle-error.util");
const response_util_1 = require("../../../../utils/response.util");
const const_2 = require("../../const");
const dto_1 = require("./dto");
let UserRoleController = class UserRoleController {
    constructor(roleService) {
        this.roleService = roleService;
    }
    async listRole(query, req, res) {
        const user_id = req.user.user_id;
        const result = await this.roleService.listRoleForUser(user_id, query.package_id);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getRoleDetail(id, res) {
        const result = await this.roleService.getRoleDetail(id);
        if (result) {
            result['category'] = transaction_1.ORDER_CATEGORY.PKG;
            response_util_1.resSuccess({
                payload: result,
                res,
            });
        }
        else {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
    }
};
__decorate([
    common_1.Get('/list'),
    __param(0, common_1.Query()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.QueryUserRoleDto, Object, Object]),
    __metadata("design:returntype", Promise)
], UserRoleController.prototype, "listRole", null);
__decorate([
    common_1.Get('/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], UserRoleController.prototype, "getRoleDetail", null);
UserRoleController = __decorate([
    swagger_1.ApiSecurity('Refresh-Token'),
    swagger_1.ApiBearerAuth(),
    common_1.UseGuards(const_2.UserAuthGuard),
    swagger_1.ApiTags('user/role'),
    common_1.Controller('role'),
    __metadata("design:paramtypes", [role_1.RoleService])
], UserRoleController);
exports.UserRoleController = UserRoleController;
//# sourceMappingURL=index.js.map