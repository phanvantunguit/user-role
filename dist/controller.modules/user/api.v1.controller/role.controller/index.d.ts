import { Response } from 'express';
import { RoleService } from 'src/services/role';
import { QueryUserRoleDto } from './dto';
export declare class UserRoleController {
    private roleService;
    constructor(roleService: RoleService);
    listRole(query: QueryUserRoleDto, req: any, res: Response): Promise<void>;
    getRoleDetail(id: string, res: Response): Promise<void>;
}
