"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const transaction_1 = require("../../../../services/transaction");
const response_util_1 = require("../../../../utils/response.util");
const const_2 = require("../../const");
const dto_1 = require("./dto");
const transaction_2 = require("../../../../const/transaction");
let TransactionController = class TransactionController {
    constructor(transactionService) {
        this.transactionService = transactionService;
    }
    async createTransaction(req, body, res) {
        const user_id = req.user.user_id;
        const email = req.user.email;
        const transaction = await this.transactionService.checkCreateTransactionValid(user_id, body.role_id, body.package_id, body.buy_currency);
        if (transaction) {
            transaction['sell_amount'] = Number(transaction.sell_amount.toFixed(2));
            response_util_1.resSuccess({
                payload: transaction,
                res,
            });
        }
        else {
            const result = await this.transactionService.createTransaction(Object.assign(Object.assign({}, body), { user_id, buyer_email: email }));
            result['address'] = result.wallet_address;
            result['sell_amount'] = Number(result.sell_amount.toFixed(2));
            response_util_1.resSuccess({
                payload: result,
                res,
            });
        }
    }
    async upgradeFreeTrial(req, body, res) {
        const user_id = req.user.user_id;
        const userRole = await this.transactionService.upgradeFreeTrial(user_id, body.role_id);
        response_util_1.resSuccess({
            payload: userRole,
            res,
        });
    }
    async ipnTransaction(headers, body, res) {
        await this.transactionService.ipnTransaction(transaction_2.PAYMENT_METHOD.COIN_PAYMENT, body);
        response_util_1.resSuccess({
            payload: 'ok',
            res,
        });
    }
    async listTransaction(req, res) {
        const user_id = req.user.user_id;
        const data = await this.transactionService.listTransaction({ user_id });
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async checkTransaction(id, res) {
        const data = await this.transactionService.getTransaction({ id });
        data['sell_amount'] = Number(data.sell_amount.toFixed(2));
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
};
__decorate([
    common_1.Post(''),
    __param(0, common_1.Req()),
    __param(1, common_1.Body()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.PostTransactionDto, Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "createTransaction", null);
__decorate([
    common_1.Post('/upgrade-trial'),
    __param(0, common_1.Req()),
    __param(1, common_1.Body()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.PostUpgradeFreeTrialDto, Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "upgradeFreeTrial", null);
__decorate([
    const_2.UserPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Post('ipn/coinpayment'),
    __param(0, common_1.Headers()),
    __param(1, common_1.Body()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "ipnTransaction", null);
__decorate([
    common_1.Get('/list'),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "listTransaction", null);
__decorate([
    common_1.Get('/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "checkTransaction", null);
TransactionController = __decorate([
    swagger_1.ApiSecurity('Refresh-Token'),
    swagger_1.ApiBearerAuth(),
    common_1.UseGuards(const_2.UserAuthGuard),
    swagger_1.ApiTags('user/transaction'),
    common_1.Controller('transaction'),
    __param(0, common_1.Inject(transaction_1.TransactionService)),
    __metadata("design:paramtypes", [transaction_1.TransactionService])
], TransactionController);
exports.TransactionController = TransactionController;
//# sourceMappingURL=index.js.map