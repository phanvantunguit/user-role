import { Response } from 'express';
import { TransactionService } from 'src/services/transaction';
import { PostTransactionDto, PostUpgradeFreeTrialDto } from './dto';
export declare class TransactionController {
    private transactionService;
    constructor(transactionService: TransactionService);
    createTransaction(req: any, body: PostTransactionDto, res: Response): Promise<void>;
    upgradeFreeTrial(req: any, body: PostUpgradeFreeTrialDto, res: Response): Promise<void>;
    ipnTransaction(headers: any, body: any, res: Response): Promise<void>;
    listTransaction(req: any, res: Response): Promise<void>;
    checkTransaction(id: string, res: Response): Promise<void>;
}
