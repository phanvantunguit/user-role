import { PAYMENT_METHOD } from 'src/const/transaction';
export declare class PostTransactionDto {
    payment_method: PAYMENT_METHOD;
    role_id: string;
    package_id: string;
    buy_currency: string;
}
export declare class PostUpgradeFreeTrialDto {
    role_id: string;
}
