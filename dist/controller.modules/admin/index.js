"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiV1AdminControllerModule = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const repositories_1 = require("../../repositories");
const coinpayment_1 = require("../../resources/coinpayment");
const coinpayment_transport_1 = require("../../resources/coinpayment/coinpayment.transport");
const forex_1 = require("../../resources/forex");
const mail_1 = require("../../resources/mail");
const sendgrid_transport_1 = require("../../resources/mail/sendgrid.transport");
const storage_1 = require("../../resources/storage");
const google_transport_1 = require("../../resources/storage/google.transport");
const app_setting_1 = require("../../services/app-setting");
const auth_role_1 = require("../../services/auth-role");
const auth_user_role_1 = require("../../services/auth-user-role");
const bot_1 = require("../../services/bot");
const bot_setting_1 = require("../../services/bot-setting");
const bot_signal_1 = require("../../services/bot-signal");
const bot_trading_1 = require("../../services/bot-trading");
const bot_trading_history_1 = require("../../services/bot-trading-history");
const currency_1 = require("../../services/currency");
const exchange_1 = require("../../services/exchange");
const feature_1 = require("../../services/feature");
const feature_role_1 = require("../../services/feature-role");
const general_setting_1 = require("../../services/general-setting");
const general_setting_role_1 = require("../../services/general-setting-role");
const package_1 = require("../../services/package");
const payment_1 = require("../../services/payment");
const coinpayment_2 = require("../../services/payment/coinpayment");
const resolution_1 = require("../../services/resolution");
const role_1 = require("../../services/role");
const session_1 = require("../../services/session");
const symbol_1 = require("../../services/symbol");
const symbol_setting_role_1 = require("../../services/symbol-setting-role");
const transaction_1 = require("../../services/transaction");
const transaction_log_1 = require("../../services/transaction-log");
const user_1 = require("../../services/user");
const user_role_1 = require("../../services/user-role");
const verify_token_1 = require("../../services/verify-token");
const admin_delete_bot_trading_1 = require("../../usecases/bot/admin-delete-bot-trading");
const admin_import_trade_history_csv_1 = require("../../usecases/bot/admin-import-trade-history-csv");
const admin_list_trade_history_1 = require("../../usecases/bot/admin-list-trade-history");
const admin_update_bot_trading_status_1 = require("../../usecases/bot/admin-update-bot-trading-status");
const reset_subscriptions_1 = require("../../usecases/bot/reset-subscriptions");
const user_list_bot_1 = require("../../usecases/bot/user-list-bot");
const user_list_bot_trading_1 = require("../../usecases/bot/user-list-bot-trading");
const user_list_bot_trading_history_1 = require("../../usecases/bot/user-list-bot-trading-history");
const admin_list_merchant_1 = require("../../usecases/merchant/admin-list-merchant");
const user_list_role_1 = require("../../usecases/role/user-list-role");
const admin_get_detail_transaction_v2_1 = require("../../usecases/transaction/admin-get-detail-transaction-v2");
const admin_list_transaction_v2_1 = require("../../usecases/transaction/admin-list-transaction-v2");
const list_asset_log_1 = require("../../usecases/transaction/list-asset-log");
const admin_add_user_asset_1 = require("../../usecases/user-asset/admin-add-user-asset");
const admin_remove_user_asset_1 = require("../../usecases/user-asset/admin-remove-user-asset");
const admin_user_controller_1 = require("./api.v1.controller/admin-user.controller");
const auth_role_controller_1 = require("./api.v1.controller/auth-role.controller");
const bot_trading_history_controler_1 = require("./api.v1.controller/bot-trading-history.controler");
const bot_trading_controler_1 = require("./api.v1.controller/bot-trading.controler");
const bot_controller_1 = require("./api.v1.controller/bot.controller");
const permission_controller_1 = require("./api.v1.controller/permission.controller");
const role_controller_1 = require("./api.v1.controller/role.controller");
const setting_controller_1 = require("./api.v1.controller/setting.controller");
const transaction_controller_1 = require("./api.v1.controller/transaction.controller");
const merchant_controller_1 = require("./api.v2.controller/merchant.controller");
const transaction_controller_2 = require("./api.v2.controller/transaction.controller");
let APIV1Controller = class APIV1Controller {
};
APIV1Controller = __decorate([
    common_1.Module({
        imports: [repositories_1.DatabaseModule],
        controllers: [
            admin_user_controller_1.AdminUserController,
            permission_controller_1.AdminPermissionController,
            auth_role_controller_1.AuthRoleController,
            setting_controller_1.AdminSettingController,
            role_controller_1.RoleController,
            transaction_controller_1.AdminTransactionController,
            bot_controller_1.AdminBotController,
            bot_trading_controler_1.AdminBotTradingController,
            bot_trading_history_controler_1.AdminBotTradingHistoryController,
        ],
        providers: [
            auth_role_1.AuthRoleService,
            user_1.UserService,
            mail_1.MailResource,
            verify_token_1.VerifyTokenService,
            sendgrid_transport_1.SendGridTransport,
            auth_user_role_1.AuthUserRoleService,
            feature_1.FeatureService,
            exchange_1.ExchangeService,
            resolution_1.ResolutionService,
            symbol_1.SymbolService,
            general_setting_1.GeneralSettingService,
            user_role_1.UserRoleService,
            role_1.RoleService,
            feature_role_1.FeatureRoleService,
            general_setting_role_1.GeneralSettingRoleService,
            symbol_setting_role_1.SymbolSettingRoleService,
            app_setting_1.AppSettingService,
            transaction_1.TransactionService,
            coinpayment_1.CoinpaymentResource,
            transaction_log_1.TransactionLogService,
            coinpayment_transport_1.CoinpaymentTransport,
            bot_setting_1.BotSettingService,
            package_1.PackageService,
            payment_1.PaymentService,
            coinpayment_2.CoinpaymentService,
            currency_1.CurrencyService,
            google_transport_1.GCloudTransport,
            storage_1.StorageResource,
            session_1.SessionService,
            bot_1.BotService,
            bot_trading_1.BotTradingService,
            bot_signal_1.BotSignalService,
            list_asset_log_1.ListAssetLogHandler,
            admin_add_user_asset_1.AdminAddUserAssetHandler,
            admin_remove_user_asset_1.AdminRemoveUserAssetHandler,
            user_list_role_1.UserListRoleHandler,
            user_list_bot_1.UserListBotHandler,
            user_list_bot_trading_1.UserListBotTradingHandler,
            forex_1.MetaapiResource,
            bot_trading_history_1.BotTradingHistoryService,
            user_list_bot_trading_history_1.UserListBotTradingHistoryHandler,
            admin_import_trade_history_csv_1.AdminImportTradeHistoryCSVHandler,
            admin_list_trade_history_1.AdminListTradeHistoryHandler,
            admin_update_bot_trading_status_1.AdminUpdateBotTradingStatusHandler,
            reset_subscriptions_1.AdminResetSubscriptionHandler,
            admin_delete_bot_trading_1.AdminDeleteBotTradingHandler
        ],
    })
], APIV1Controller);
let APIV2Controller = class APIV2Controller {
};
APIV2Controller = __decorate([
    common_1.Module({
        imports: [repositories_1.DatabaseModule],
        controllers: [transaction_controller_2.AdminTransactionV2Controller, merchant_controller_1.AdminMerchantV2Controller],
        providers: [
            session_1.SessionService,
            general_setting_role_1.GeneralSettingRoleService,
            role_1.RoleService,
            admin_list_transaction_v2_1.AdminListTransactionV2Handler,
            admin_get_detail_transaction_v2_1.AdminGetTransactionV2Handler,
            admin_list_merchant_1.AdminListMerchantHandler,
        ],
    })
], APIV2Controller);
const routes = [
    { path: 'api/v1/admin', module: APIV1Controller },
    { path: 'api/v2/admin', module: APIV2Controller },
];
let ApiV1AdminControllerModule = class ApiV1AdminControllerModule {
};
ApiV1AdminControllerModule = __decorate([
    common_1.Module({
        imports: [core_1.RouterModule.register(routes), APIV1Controller, APIV2Controller],
    })
], ApiV1AdminControllerModule);
exports.ApiV1AdminControllerModule = ApiV1AdminControllerModule;
//# sourceMappingURL=index.js.map