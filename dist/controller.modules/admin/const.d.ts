import { ExecutionContext, CanActivate } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { PermissionRepository } from 'src/repositories/permission.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { SessionService } from 'src/services/session';
export declare const AdminPermission: (permission: string) => import("@nestjs/common").CustomDecorator<string>;
export declare class AdminAuthGuard implements CanActivate {
    private reflector;
    private permissionRepo;
    private userRepo;
    private sessionService;
    constructor(reflector: Reflector, permissionRepo: PermissionRepository, userRepo: UserRepository, sessionService: SessionService);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
export declare class AdminBaseApiOutput {
    error_code?: string;
    message?: string;
}
