import { Response } from 'express';
import { TransactionRepository } from 'src/repositories/transaction.repository';
import { TransactionService } from 'src/services/transaction';
import { TransactionLogService } from 'src/services/transaction-log';
import { GetTransactionDto } from './dto';
export declare class AdminTransactionController {
    private transactionService;
    private transactionLogService;
    private transactionRepository;
    constructor(transactionService: TransactionService, transactionLogService: TransactionLogService, transactionRepository: TransactionRepository);
    listTransaction(query: GetTransactionDto, res: Response): Promise<void>;
    checkOrigin(id: string, res: Response): Promise<void>;
    transactionLogs(id: string, res: Response): Promise<void>;
    getTransaction(id: string, res: Response): Promise<void>;
}
