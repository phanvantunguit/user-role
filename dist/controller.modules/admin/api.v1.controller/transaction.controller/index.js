"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminTransactionController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const transaction_repository_1 = require("../../../../repositories/transaction.repository");
const transaction_1 = require("../../../../services/transaction");
const transaction_log_1 = require("../../../../services/transaction-log");
const response_util_1 = require("../../../../utils/response.util");
const const_2 = require("../../const");
const dto_1 = require("./dto");
let AdminTransactionController = class AdminTransactionController {
    constructor(transactionService, transactionLogService, transactionRepository) {
        this.transactionService = transactionService;
        this.transactionLogService = transactionLogService;
        this.transactionRepository = transactionRepository;
    }
    async listTransaction(query, res) {
        const data = await this.transactionService.getTransactionPagination(query);
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async checkOrigin(id, res) {
        const transaction = await this.transactionRepository.findById(id);
        const data = await this.transactionService.checkPartnerTransaction(transaction);
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async transactionLogs(id, res) {
        const data = await this.transactionLogService.list({ transaction_id: id });
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async getTransaction(id, res) {
        const data = await this.transactionService.getTransaction({ id });
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
};
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_TRANSACTION.permission_id),
    common_1.Get('/list'),
    __param(0, common_1.Query()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.GetTransactionDto, Object]),
    __metadata("design:returntype", Promise)
], AdminTransactionController.prototype, "listTransaction", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_TRANSACTION.permission_id),
    common_1.Get('/:id/check'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminTransactionController.prototype, "checkOrigin", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_TRANSACTION.permission_id),
    common_1.Get('/:id/logs'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminTransactionController.prototype, "transactionLogs", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_TRANSACTION.permission_id),
    common_1.Get('/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminTransactionController.prototype, "getTransaction", null);
AdminTransactionController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiTags('admin/transaction'),
    common_1.Controller('transaction'),
    common_1.UseGuards(const_2.AdminAuthGuard),
    __param(0, common_1.Inject(transaction_1.TransactionService)),
    __param(1, common_1.Inject(transaction_log_1.TransactionLogService)),
    __param(2, common_1.Inject(transaction_repository_1.TransactionRepository)),
    __metadata("design:paramtypes", [transaction_1.TransactionService,
        transaction_log_1.TransactionLogService,
        transaction_repository_1.TransactionRepository])
], AdminTransactionController);
exports.AdminTransactionController = AdminTransactionController;
//# sourceMappingURL=index.js.map