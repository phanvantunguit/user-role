import { TRANSACTION_STATUS } from 'src/const/transaction';
export declare class GetTransactionDto {
    page: number;
    size: number;
    keyword: string;
    status: TRANSACTION_STATUS;
    from: number;
    to: number;
}
