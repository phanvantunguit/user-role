"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthRoleController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const auth_role_1 = require("../../../../services/auth-role");
const response_util_1 = require("../../../../utils/response.util");
const const_2 = require("../../const");
const dto_1 = require("./dto");
let AuthRoleController = class AuthRoleController {
    constructor(authRoleService) {
        this.authRoleService = authRoleService;
    }
    async listAuthRole(query, res) {
        const result = await this.authRoleService.getListAuthUserRole(query);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getAuthRole(id, res) {
        const result = await this.authRoleService.getAuthUserRole({ id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createAuthRole(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.authRoleService.createAuthRole(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateAuthRole(id, body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.authRoleService.updateAuthRole(Object.assign(Object.assign({}, body), { owner_created,
            id }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async deletAuthRole(id, res) {
        const result = await this.authRoleService.deleteAuthRole([id]);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
};
__decorate([
    common_1.Get('list'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_AUTH_ROLE.permission_id),
    __param(0, common_1.Query()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.QueryAuthRoleDto, Object]),
    __metadata("design:returntype", Promise)
], AuthRoleController.prototype, "listAuthRole", null);
__decorate([
    common_1.Get('/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_AUTH_ROLE.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AuthRoleController.prototype, "getAuthRole", null);
__decorate([
    common_1.Post(),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_AUTH_ROLE.permission_id),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostAuthRoleDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AuthRoleController.prototype, "createAuthRole", null);
__decorate([
    common_1.Put('/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_AUTH_ROLE.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __param(2, common_1.Req()),
    __param(3, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, dto_1.PutAuthRoleDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AuthRoleController.prototype, "updateAuthRole", null);
__decorate([
    common_1.Delete('/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.DELETE_AUTH_ROLE.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AuthRoleController.prototype, "deletAuthRole", null);
AuthRoleController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiTags('admin/auth-role'),
    common_1.Controller('auth-role'),
    common_1.UseGuards(const_2.AdminAuthGuard),
    __metadata("design:paramtypes", [auth_role_1.AuthRoleService])
], AuthRoleController);
exports.AuthRoleController = AuthRoleController;
//# sourceMappingURL=index.js.map