export declare class PostAuthRoleDto {
    permission_ids: string[];
    role_name: string;
    description: string;
}
export declare class PutAuthRoleDto {
    permission_ids: string[];
    role_name: string;
    description: string;
}
export declare class QueryAuthRoleDto {
    user_id: string;
}
