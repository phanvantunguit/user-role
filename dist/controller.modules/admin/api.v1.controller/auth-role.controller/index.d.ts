import { Response } from 'express';
import { AuthRoleService } from 'src/services/auth-role';
import { PostAuthRoleDto, PutAuthRoleDto, QueryAuthRoleDto } from './dto';
export declare class AuthRoleController {
    private authRoleService;
    constructor(authRoleService: AuthRoleService);
    listAuthRole(query: QueryAuthRoleDto, res: Response): Promise<void>;
    getAuthRole(id: string, res: Response): Promise<void>;
    createAuthRole(body: PostAuthRoleDto, req: any, res: Response): Promise<void>;
    updateAuthRole(id: string, body: PutAuthRoleDto, req: any, res: Response): Promise<void>;
    deletAuthRole(id: string, res: Response): Promise<void>;
}
