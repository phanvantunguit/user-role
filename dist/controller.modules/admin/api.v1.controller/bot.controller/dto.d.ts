import { BOT_STATUS } from 'src/const';
export declare class PostBotDto {
    name: string;
    bot_setting_id: string;
    type: string;
    status: BOT_STATUS;
    price: string;
    currency: string;
    description?: string;
    order?: number;
    work_based_on?: string[];
    image_url: string;
}
export declare class PutBotDto {
    name?: string;
    bot_setting_id?: string;
    type?: string;
    status?: BOT_STATUS;
    price?: string;
    currency?: string;
    description?: string;
    order?: number;
    work_based_on?: string[];
    image_url?: string;
}
export declare class QueryBotDto {
    user_id: string;
}
declare class BotOrderDto {
    id: string;
    order: number;
}
export declare class PutBotOrderDto {
    bots: BotOrderDto[];
}
export {};
