"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PutBotOrderDto = exports.QueryBotDto = exports.PutBotDto = exports.PostBotDto = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
class PostBotDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of bot',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Bot setting id',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotDto.prototype, "bot_setting_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Bot type',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotDto.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: `Status: ${Object.values(const_1.BOT_STATUS)}`,
        example: const_1.BOT_STATUS.OPEN,
    }),
    class_validator_1.IsString(),
    class_validator_1.IsIn(Object.values(const_1.BOT_STATUS)),
    __metadata("design:type", String)
], PostBotDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Price upgrade',
        example: '0.001',
    }),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PostBotDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'currency',
        example: 'TLCT',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotDto.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Describe detail about bot',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Sort role',
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PostBotDto.prototype, "order", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'List feature display for user',
        example: [
            'Crypto Data',
            'Footprint chat',
            'Volume profile',
            'Delta & cumulative delta',
            'Multiple panels',
        ],
    }),
    class_validator_1.IsArray(),
    class_validator_1.IsOptional(),
    class_validator_1.IsString({ each: true }),
    __metadata("design:type", Array)
], PostBotDto.prototype, "work_based_on", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Url image',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotDto.prototype, "image_url", void 0);
exports.PostBotDto = PostBotDto;
class PutBotDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Name of bot',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Bot setting id',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotDto.prototype, "bot_setting_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Bot type',
        example: 'FUTURE',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotDto.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: `Status: ${Object.values(const_1.BOT_STATUS)}`,
        example: const_1.BOT_STATUS.OPEN,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    class_validator_1.IsIn(Object.values(const_1.BOT_STATUS)),
    __metadata("design:type", String)
], PutBotDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Price upgrade',
        example: '0.001',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PutBotDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'currency',
        example: 'TLCT',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotDto.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Describe detail about bot',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Sort role',
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PutBotDto.prototype, "order", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'List feature display for user',
        example: [
            'Crypto Data',
            'Footprint chat',
            'Volume profile',
            'Delta & cumulative delta',
            'Multiple panels',
        ],
    }),
    class_validator_1.IsArray(),
    class_validator_1.IsString({ each: true }),
    __metadata("design:type", Array)
], PutBotDto.prototype, "work_based_on", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Url image',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotDto.prototype, "image_url", void 0);
exports.PutBotDto = PutBotDto;
class QueryBotDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'id of user',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], QueryBotDto.prototype, "user_id", void 0);
exports.QueryBotDto = QueryBotDto;
class BotOrderDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'id of bot',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], BotOrderDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'order of bot',
        example: 1,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], BotOrderDto.prototype, "order", void 0);
class PutBotOrderDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'list bot',
        example: [
            { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2', order: 1 },
            { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e3', order: 2 },
        ],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], PutBotOrderDto.prototype, "bots", void 0);
exports.PutBotOrderDto = PutBotOrderDto;
//# sourceMappingURL=dto.js.map