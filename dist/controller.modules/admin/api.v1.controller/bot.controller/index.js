"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminBotController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const bot_1 = require("../../../../services/bot");
const response_util_1 = require("../../../../utils/response.util");
const const_2 = require("../../const");
const dto_1 = require("./dto");
let AdminBotController = class AdminBotController {
    constructor(botService) {
        this.botService = botService;
    }
    async listBot(query, res) {
        const { user_id } = query;
        const result = await this.botService.listBot({ user_id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getBotDetail(id, res) {
        const result = await this.botService.get({ id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createBot(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.botService.createBot(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateBot(id, body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.botService.save(Object.assign(Object.assign({ id }, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateOrderBot(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.botService.updateOrderBot({
            bots: body.bots,
            owner_created,
        });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async deleteBot(id, req, res) {
        const result = await this.botService.handleBotWithoutUserId({
            bot_id: id,
        });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
};
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_BOT.permission_id),
    common_1.Get('/list'),
    __param(0, common_1.Query()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.QueryBotDto, Object]),
    __metadata("design:returntype", Promise)
], AdminBotController.prototype, "listBot", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_BOT.permission_id),
    common_1.Get('/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminBotController.prototype, "getBotDetail", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_BOT.permission_id),
    common_1.Post(''),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostBotDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminBotController.prototype, "createBot", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_BOT.permission_id),
    common_1.Put('/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __param(2, common_1.Req()),
    __param(3, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, dto_1.PutBotDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminBotController.prototype, "updateBot", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_BOT.permission_id),
    common_1.Put('/order'),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutBotOrderDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminBotController.prototype, "updateOrderBot", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.DELETE_BOT.permission_id),
    common_1.Delete('/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminBotController.prototype, "deleteBot", null);
AdminBotController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiTags('admin/bot'),
    common_1.Controller('bot'),
    common_1.UseGuards(const_2.AdminAuthGuard),
    __metadata("design:paramtypes", [bot_1.BotService])
], AdminBotController);
exports.AdminBotController = AdminBotController;
//# sourceMappingURL=index.js.map