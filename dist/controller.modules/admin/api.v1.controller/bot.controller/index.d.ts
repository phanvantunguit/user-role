import { Response } from 'express';
import { BotService } from 'src/services/bot';
import { PostBotDto, PutBotDto, PutBotOrderDto, QueryBotDto } from './dto';
export declare class AdminBotController {
    private botService;
    constructor(botService: BotService);
    listBot(query: QueryBotDto, res: Response): Promise<void>;
    getBotDetail(id: string, res: Response): Promise<void>;
    createBot(body: PostBotDto, req: any, res: Response): Promise<void>;
    updateBot(id: string, body: PutBotDto, req: any, res: Response): Promise<void>;
    updateOrderBot(body: PutBotOrderDto, req: any, res: Response): Promise<void>;
    deleteBot(id: string, req: any, res: Response): Promise<void>;
}
