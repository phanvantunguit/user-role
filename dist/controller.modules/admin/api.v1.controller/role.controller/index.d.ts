import { Response } from 'express';
import { FeatureRoleService } from 'src/services/feature-role';
import { GeneralSettingRoleService } from 'src/services/general-setting-role';
import { RoleService } from 'src/services/role';
import { SymbolSettingRoleService } from 'src/services/symbol-setting-role';
import { PostRoleDto, PutRoleDto, PutFeatureRoleDto, PutGeneralSettingRoleDto, PutSymbolSettingRoleDto, QueryRoleDto, PutRoleOrderDto } from './dto';
export declare class RoleController {
    private roleService;
    private featureRoleService;
    private generalSettingRoleService;
    private symbolSettingRoleService;
    constructor(roleService: RoleService, featureRoleService: FeatureRoleService, generalSettingRoleService: GeneralSettingRoleService, symbolSettingRoleService: SymbolSettingRoleService);
    listRole(query: QueryRoleDto, res: Response): Promise<void>;
    getRoleDetail(id: string, res: Response): Promise<void>;
    createRole(body: PostRoleDto, req: any, res: Response): Promise<void>;
    updateOrderRole(body: PutRoleOrderDto, req: any, res: Response): Promise<void>;
    updateRole(id: string, body: PutRoleDto, req: any, res: Response): Promise<void>;
    deleteRole(id: string, res: Response): Promise<void>;
    modifyFeatureRole(body: PutFeatureRoleDto, role_id: string, req: any, res: Response): Promise<void>;
    modifyGSRole(body: PutGeneralSettingRoleDto, role_id: string, req: any, res: Response): Promise<void>;
    createSSRole(body: PutSymbolSettingRoleDto, role_id: string, req: any, res: Response): Promise<void>;
}
