declare class GeneralSettingRole {
    general_setting_id: string;
    description: string;
    val_limit: number;
}
declare class FeatureRole {
    feature_id: string;
    description: string;
}
export declare class PutSymbolSettingRoleDto {
    list_symbol: string[];
    list_exchanged: string[];
    supported_resolutions: string[];
    description: string;
}
export declare class PostRoleDto {
    role_name: string;
    description: string;
    status: string;
    type: string;
    price: string;
    currency: string;
    parent_id: string;
    is_best_choice: boolean;
    order: number;
    description_features: string[];
    color: string;
}
export declare class PutRoleDto {
    role_name: string;
    description: string;
    status: string;
    type: string;
    price: string;
    currency: string;
    parent_id: string;
    is_best_choice: boolean;
    order: number;
    description_features: string[];
    color: string;
}
export declare class PutFeatureRoleDto {
    features: FeatureRole[];
}
export declare class PutGeneralSettingRoleDto {
    general_settings: GeneralSettingRole[];
}
export declare class QueryRoleDto {
    user_id: string;
}
declare class RoleOrderDto {
    id: string;
    order: number;
}
export declare class PutRoleOrderDto {
    roles: RoleOrderDto[];
}
export {};
