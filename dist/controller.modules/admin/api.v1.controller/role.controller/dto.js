"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PutRoleOrderDto = exports.QueryRoleDto = exports.PutGeneralSettingRoleDto = exports.PutFeatureRoleDto = exports.PutRoleDto = exports.PostRoleDto = exports.PutSymbolSettingRoleDto = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const const_1 = require("../../../../const");
class GeneralSettingRole {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Id of general setting',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], GeneralSettingRole.prototype, "general_setting_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: '',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], GeneralSettingRole.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 10,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], GeneralSettingRole.prototype, "val_limit", void 0);
class FeatureRole {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Id of feature',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], FeatureRole.prototype, "feature_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: '',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], FeatureRole.prototype, "description", void 0);
class PutSymbolSettingRoleDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'List of symbol',
        example: ['BTC', 'ETH'],
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsArray(),
    class_validator_1.IsString({ each: true }),
    __metadata("design:type", Array)
], PutSymbolSettingRoleDto.prototype, "list_symbol", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'List of exchange',
        example: ['exchange_1', 'exchange_2'],
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsArray(),
    class_validator_1.IsString({ each: true }),
    __metadata("design:type", Array)
], PutSymbolSettingRoleDto.prototype, "list_exchanged", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'List of resolution',
        example: ['resolution_1', 'resolution_2'],
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsArray(),
    class_validator_1.IsString({ each: true }),
    __metadata("design:type", Array)
], PutSymbolSettingRoleDto.prototype, "supported_resolutions", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: '',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutSymbolSettingRoleDto.prototype, "description", void 0);
exports.PutSymbolSettingRoleDto = PutSymbolSettingRoleDto;
class PostRoleDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of role',
        example: 'Customer Support',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostRoleDto.prototype, "role_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Describe detail about role',
        example: `Allow CS view feature and package of customer`,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostRoleDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: `Status: ${Object.values(const_1.ROLE_STATUS)}`,
        example: const_1.ROLE_STATUS.OPEN,
    }),
    class_validator_1.IsString(),
    class_validator_1.IsIn(Object.values(const_1.ROLE_STATUS)),
    __metadata("design:type", String)
], PostRoleDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Type of role Pack',
        example: const_1.ROLE_TYPE.PACKAGE,
    }),
    class_validator_1.IsString(),
    class_validator_1.IsIn(Object.values(const_1.ROLE_TYPE)),
    __metadata("design:type", String)
], PostRoleDto.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Price upgrade',
        example: '0.001',
    }),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PostRoleDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'currency',
        example: 'TLCT',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostRoleDto.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'parent role id',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostRoleDto.prototype, "parent_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Set highlight for role',
        example: true,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsBoolean(),
    __metadata("design:type", Boolean)
], PostRoleDto.prototype, "is_best_choice", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Sort role',
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", Number)
], PostRoleDto.prototype, "order", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'List feature display for user',
        example: [
            'Crypto Data',
            'Footprint chat',
            'Volume profile',
            'Delta & cumulative delta',
            'Multiple panels',
        ],
    }),
    class_validator_1.IsArray(),
    class_validator_1.IsString({ each: true }),
    __metadata("design:type", Array)
], PostRoleDto.prototype, "description_features", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Color role card on UI',
        example: '0000FF',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostRoleDto.prototype, "color", void 0);
exports.PostRoleDto = PostRoleDto;
class PutRoleDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of role',
        example: 'Customer Support',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutRoleDto.prototype, "role_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Describe detail about role',
        example: `Allow CS view feature and package of customer`,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutRoleDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: `Status: ${Object.values(const_1.ROLE_STATUS)}`,
        example: const_1.ROLE_STATUS.OPEN,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    class_validator_1.IsIn(Object.values(const_1.ROLE_STATUS)),
    __metadata("design:type", String)
], PutRoleDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Type of role Pack',
        example: const_1.ROLE_TYPE.PACKAGE,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    class_validator_1.IsIn(Object.values(const_1.ROLE_TYPE)),
    __metadata("design:type", String)
], PutRoleDto.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Price upgrade',
        example: '0.001',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PutRoleDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'currency',
        example: 'TLCT',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutRoleDto.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'parent role id',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutRoleDto.prototype, "parent_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Set highlight for role',
        example: true,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsBoolean(),
    __metadata("design:type", Boolean)
], PutRoleDto.prototype, "is_best_choice", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Sort role',
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PutRoleDto.prototype, "order", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'List feature display for user',
        example: [
            'Crypto Data',
            'Footprint chat',
            'Volume profile',
            'Delta & cumulative delta',
            'Multiple panels',
        ],
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsArray(),
    class_validator_1.IsString({ each: true }),
    __metadata("design:type", Array)
], PutRoleDto.prototype, "description_features", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Color role card on UI',
        example: '0000FF',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutRoleDto.prototype, "color", void 0);
exports.PutRoleDto = PutRoleDto;
class PutFeatureRoleDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'List of feature',
        example: [
            {
                feature_id: 'fd97c746-72c7-4eac-98b2-e5a9ea4278fb',
                description: '',
            },
        ],
    }),
    class_validator_1.IsOptional(),
    class_validator_1.ValidateNested({ each: true }),
    class_transformer_1.Type(() => FeatureRole),
    __metadata("design:type", Array)
], PutFeatureRoleDto.prototype, "features", void 0);
exports.PutFeatureRoleDto = PutFeatureRoleDto;
class PutGeneralSettingRoleDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'List of general setting',
        example: [
            {
                general_setting_id: 'fd97c746-72c7-4eac-98b2-e5a9ea4278fb',
                description: '',
                val_limit: 10,
            },
        ],
    }),
    class_validator_1.IsOptional(),
    class_validator_1.ValidateNested({ each: true }),
    class_transformer_1.Type(() => GeneralSettingRole),
    __metadata("design:type", Array)
], PutGeneralSettingRoleDto.prototype, "general_settings", void 0);
exports.PutGeneralSettingRoleDto = PutGeneralSettingRoleDto;
class QueryRoleDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'id of user',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], QueryRoleDto.prototype, "user_id", void 0);
exports.QueryRoleDto = QueryRoleDto;
class RoleOrderDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'id of role',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], RoleOrderDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'order of role',
        example: 1,
    }),
    class_validator_1.IsString(),
    __metadata("design:type", Number)
], RoleOrderDto.prototype, "order", void 0);
class PutRoleOrderDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'list roles',
        example: [
            { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2', order: 1 },
            { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e3', order: 2 },
        ],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], PutRoleOrderDto.prototype, "roles", void 0);
exports.PutRoleOrderDto = PutRoleOrderDto;
//# sourceMappingURL=dto.js.map