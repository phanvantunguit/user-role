"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const feature_role_1 = require("../../../../services/feature-role");
const general_setting_role_1 = require("../../../../services/general-setting-role");
const role_1 = require("../../../../services/role");
const symbol_setting_role_1 = require("../../../../services/symbol-setting-role");
const response_util_1 = require("../../../../utils/response.util");
const const_2 = require("../../const");
const dto_1 = require("./dto");
let RoleController = class RoleController {
    constructor(roleService, featureRoleService, generalSettingRoleService, symbolSettingRoleService) {
        this.roleService = roleService;
        this.featureRoleService = featureRoleService;
        this.generalSettingRoleService = generalSettingRoleService;
        this.symbolSettingRoleService = symbolSettingRoleService;
    }
    async listRole(query, res) {
        const { user_id } = query;
        const result = await this.roleService.listRole({ user_id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getRoleDetail(id, res) {
        const result = await this.roleService.getRoleDetail(id);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createRole(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.roleService.createRole(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateOrderRole(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.roleService.updateOrderRole({
            roles: body.roles,
            owner_created,
        });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateRole(id, body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.roleService.updateRole(Object.assign(Object.assign({}, body), { owner_created,
            id }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async deleteRole(id, res) {
        const result = await this.roleService.deleteRole(id);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async modifyFeatureRole(body, role_id, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.featureRoleService.modifyFeatureRole(Object.assign(Object.assign({}, body), { owner_created,
            role_id }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async modifyGSRole(body, role_id, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.generalSettingRoleService.modifyGeneralSettingRole(Object.assign(Object.assign({}, body), { owner_created,
            role_id }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createSSRole(body, role_id, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.symbolSettingRoleService.modifySymbolSettingRole(Object.assign(Object.assign({}, body), { owner_created,
            role_id }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
};
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_ROLE.permission_id),
    common_1.Get('/list'),
    __param(0, common_1.Query()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.QueryRoleDto, Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "listRole", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_ROLE.permission_id),
    common_1.Get('/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "getRoleDetail", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_ROLE.permission_id),
    common_1.Post(''),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostRoleDto, Object, Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "createRole", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_ROLE.permission_id),
    common_1.Put('/order'),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutRoleOrderDto, Object, Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "updateOrderRole", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_ROLE.permission_id),
    common_1.Put('/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __param(2, common_1.Req()),
    __param(3, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, dto_1.PutRoleDto, Object, Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "updateRole", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.DELETE_ROLE.permission_id),
    common_1.Delete('/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "deleteRole", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_ROLE.permission_id),
    common_1.Put('/feature/:role_id'),
    __param(0, common_1.Body()),
    __param(1, common_1.Param('role_id')),
    __param(2, common_1.Req()),
    __param(3, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutFeatureRoleDto, String, Object, Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "modifyFeatureRole", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_ROLE.permission_id),
    common_1.Put('/general-setting/:role_id'),
    __param(0, common_1.Body()),
    __param(1, common_1.Param('role_id')),
    __param(2, common_1.Req()),
    __param(3, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutGeneralSettingRoleDto, String, Object, Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "modifyGSRole", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_ROLE.permission_id),
    common_1.Put('/symbol-setting/:role_id'),
    __param(0, common_1.Body()),
    __param(1, common_1.Param('role_id')),
    __param(2, common_1.Req()),
    __param(3, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutSymbolSettingRoleDto, String, Object, Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "createSSRole", null);
RoleController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiTags('admin/role'),
    common_1.Controller('role'),
    common_1.UseGuards(const_2.AdminAuthGuard),
    __metadata("design:paramtypes", [role_1.RoleService,
        feature_role_1.FeatureRoleService,
        general_setting_role_1.GeneralSettingRoleService,
        symbol_setting_role_1.SymbolSettingRoleService])
], RoleController);
exports.RoleController = RoleController;
//# sourceMappingURL=index.js.map