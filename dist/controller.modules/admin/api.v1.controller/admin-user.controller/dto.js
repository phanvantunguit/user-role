"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAllUserDto = exports.GetUserDto = exports.RemoveMultipleUserRoleDto = exports.AddMultipleUserRoleDto = exports.PutUserRoleDto = exports.PutAuthUserRoleDto = exports.PostCreateUserAdminDto = exports.PutUserDto = exports.PutAdminUserProfileDto = exports.UserRoleDto = exports.AuthUserRoleDto = exports.PostLoginDto = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const class_transformer_1 = require("class-transformer");
class PostLoginDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Email to login Coinmap',
        example: 'john@gmail.com',
    }),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], PostLoginDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'password must be at least 8 characters with upper case letter, lower case letter, and number',
        example: '123456Aa',
    }),
    class_validator_1.Matches(const_1.PasswordRegex),
    __metadata("design:type", String)
], PostLoginDto.prototype, "password", void 0);
exports.PostLoginDto = PostLoginDto;
class AuthUserRoleDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Id of auth role',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AuthUserRoleDto.prototype, "auth_role_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        example: '',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AuthUserRoleDto.prototype, "description", void 0);
exports.AuthUserRoleDto = AuthUserRoleDto;
class UserRoleDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Id of role',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserRoleDto.prototype, "role_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        description: 'description',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserRoleDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        description: 'expires_at',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserRoleDto.prototype, "expires_at", void 0);
exports.UserRoleDto = UserRoleDto;
class PutAdminUserProfileDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Phone number',
        example: '0987654321',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutAdminUserProfileDto.prototype, "phone", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'First name',
        example: 'John',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutAdminUserProfileDto.prototype, "first_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Last name',
        example: 'Nguyen',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutAdminUserProfileDto.prototype, "last_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Full address',
        example: '123 To Hien Thanh, phuong 10, quan 10, tp HCM',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutAdminUserProfileDto.prototype, "address", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Affiliate code',
        example: 'affiliate_code',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutAdminUserProfileDto.prototype, "affiliate_code", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Link affiliate',
        example: '',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutAdminUserProfileDto.prototype, "link_affiliate", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Referral_code',
        example: '',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutAdminUserProfileDto.prototype, "referral_code", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Profile pic',
        example: '',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutAdminUserProfileDto.prototype, "profile_pic", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Note updated',
        example: '',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutAdminUserProfileDto.prototype, "note_updated", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
        example: '123456Aa',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.Matches(new RegExp(const_1.PasswordRegex), {
        message: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
    }),
    __metadata("design:type", String)
], PutAdminUserProfileDto.prototype, "password", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
        example: '123456Aa',
    }),
    class_validator_1.ValidateIf((o) => o.password),
    class_validator_1.Matches(new RegExp(const_1.PasswordRegex), {
        message: 'Old password must be at least 8 characters with upper case letter, lower case letter, and number',
    }),
    __metadata("design:type", String)
], PutAdminUserProfileDto.prototype, "old_password", void 0);
exports.PutAdminUserProfileDto = PutAdminUserProfileDto;
class PutUserDto extends PutAdminUserProfileDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Active of inactive',
        example: true,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsBoolean(),
    __metadata("design:type", Boolean)
], PutUserDto.prototype, "active", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Set user is admin or not',
        example: true,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsBoolean(),
    __metadata("design:type", Boolean)
], PutUserDto.prototype, "is_admin", void 0);
exports.PutUserDto = PutUserDto;
class PostCreateUserAdminDto extends PutUserDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Email to register account Coinmap',
        example: 'john@gmail.com',
    }),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], PostCreateUserAdminDto.prototype, "email", void 0);
exports.PostCreateUserAdminDto = PostCreateUserAdminDto;
class PutAuthUserRoleDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Id of user',
        example: '8c9e1891-b5b0-4949-bcfe-a2e25c1e53e6',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutAuthUserRoleDto.prototype, "user_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'List of auth role',
        example: [
            {
                auth_role_id: 'fd97c746-72c7-4eac-98b2-e5a9ea4278fb',
                description: 'Base role',
            },
            {
                auth_role_id: 'f4381e7d-a7df-45ac-b7b4-986e19919454',
                description: 'Plus role',
            },
        ],
    }),
    class_validator_1.ValidateNested({ each: true }),
    class_transformer_1.Type(() => AuthUserRoleDto),
    __metadata("design:type", Array)
], PutAuthUserRoleDto.prototype, "auth_roles", void 0);
exports.PutAuthUserRoleDto = PutAuthUserRoleDto;
class PutUserRoleDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Id of user',
        example: '8c9e1891-b5b0-4949-bcfe-a2e25c1e53e6',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutUserRoleDto.prototype, "user_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'List of auth role',
        example: [
            {
                role_id: 'fd97c746-72c7-4eac-98b2-e5a9ea4278fb',
                description: 'Base role',
            },
            {
                role_id: 'f4381e7d-a7df-45ac-b7b4-986e19919454',
                description: 'Plus role',
            },
        ],
    }),
    class_validator_1.ValidateNested({ each: true }),
    class_transformer_1.Type(() => UserRoleDto),
    __metadata("design:type", Array)
], PutUserRoleDto.prototype, "roles", void 0);
exports.PutUserRoleDto = PutUserRoleDto;
class AddMultipleUserRoleDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Id of user',
        example: ['8c9e1891-b5b0-4949-bcfe-a2e25c1e53e6'],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], AddMultipleUserRoleDto.prototype, "user_ids", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Id of roles',
        example: ['8c9e1891-b5b0-4949-bcfe-a2e25c1e53e6'],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], AddMultipleUserRoleDto.prototype, "role_ids", void 0);
__decorate([
    swagger_1.ApiProperty({
        description: 'quantity',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], AddMultipleUserRoleDto.prototype, "quantity", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Type of package MONTH or DAY',
        example: const_1.PACKAGE_TYPE.MONTH,
    }),
    class_validator_1.ValidateIf((o) => o.quantity),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AddMultipleUserRoleDto.prototype, "type", void 0);
exports.AddMultipleUserRoleDto = AddMultipleUserRoleDto;
class RemoveMultipleUserRoleDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Id of user',
        example: ['8c9e1891-b5b0-4949-bcfe-a2e25c1e53e6'],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], RemoveMultipleUserRoleDto.prototype, "user_ids", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Id of role',
        example: ['8c9e1891-b5b0-4949-bcfe-a2e25c1e53e6'],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], RemoveMultipleUserRoleDto.prototype, "role_ids", void 0);
exports.RemoveMultipleUserRoleDto = RemoveMultipleUserRoleDto;
class GetUserDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Page',
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], GetUserDto.prototype, "page", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Size',
        example: 50,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], GetUserDto.prototype, "size", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'The keyword to find user ex: phone number, name, email',
        example: 'john',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], GetUserDto.prototype, "keyword", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'List role filter',
        example: 'Role 1, role 2',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], GetUserDto.prototype, "roles", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'List bot filter',
        example: 'Bot 1, bot 2',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], GetUserDto.prototype, "bots", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Merchant code',
        example: 'CM',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], GetUserDto.prototype, "merchant_code", void 0);
exports.GetUserDto = GetUserDto;
class GetAllUserDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'The keyword to find user ex: phone number, name, email',
        example: 'john',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], GetAllUserDto.prototype, "keyword", void 0);
exports.GetAllUserDto = GetAllUserDto;
//# sourceMappingURL=dto.js.map