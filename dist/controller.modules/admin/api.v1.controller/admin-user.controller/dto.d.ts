import { PACKAGE_TYPE } from 'src/const';
export declare class PostLoginDto {
    email: string;
    password: string;
}
export declare class AuthUserRoleDto {
    auth_role_id: string;
    description?: string;
}
export declare class UserRoleDto {
    role_id: string;
    description?: string;
    expires_at?: number;
}
export declare class PutAdminUserProfileDto {
    phone: string;
    first_name: string;
    last_name: string;
    address: string;
    affiliate_code: string;
    link_affiliate: string;
    referral_code: string;
    profile_pic: string;
    note_updated: string;
    password: string;
    old_password: string;
}
export declare class PutUserDto extends PutAdminUserProfileDto {
    active: boolean;
    is_admin: boolean;
}
export declare class PostCreateUserAdminDto extends PutUserDto {
    email: string;
}
export declare class PutAuthUserRoleDto {
    user_id: string;
    auth_roles: AuthUserRoleDto[];
}
export declare class PutUserRoleDto {
    user_id: string;
    roles: UserRoleDto[];
}
export declare class AddMultipleUserRoleDto {
    user_ids: string[];
    role_ids: string[];
    quantity: number;
    type: PACKAGE_TYPE;
}
export declare class RemoveMultipleUserRoleDto {
    user_ids: string[];
    role_ids: string[];
}
export declare class GetUserDto {
    page: number;
    size: number;
    keyword: string;
    roles?: string;
    bots?: string;
    merchant_code?: string;
}
export declare class GetAllUserDto {
    keyword: string;
}
