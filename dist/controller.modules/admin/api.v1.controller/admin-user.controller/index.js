"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminUserController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const app_setting_1 = require("../../../../const/app-setting");
const const_2 = require("../../../user/const");
const merchant_repository_1 = require("../../../../repositories/merchant.repository");
const auth_user_role_1 = require("../../../../services/auth-user-role");
const user_1 = require("../../../../services/user");
const user_role_1 = require("../../../../services/user-role");
const admin_delete_bot_trading_1 = require("../../../../usecases/bot/admin-delete-bot-trading");
const admin_update_bot_trading_status_1 = require("../../../../usecases/bot/admin-update-bot-trading-status");
const validate_1 = require("../../../../usecases/bot/admin-update-bot-trading-status/validate");
const reset_subscriptions_1 = require("../../../../usecases/bot/reset-subscriptions");
const user_list_bot_1 = require("../../../../usecases/bot/user-list-bot");
const user_list_bot_trading_1 = require("../../../../usecases/bot/user-list-bot-trading");
const validate_2 = require("../../../../usecases/bot/user-list-bot-trading/validate");
const validate_3 = require("../../../../usecases/bot/user-list-bot/validate");
const user_list_role_1 = require("../../../../usecases/role/user-list-role");
const list_asset_log_1 = require("../../../../usecases/transaction/list-asset-log");
const validate_4 = require("../../../../usecases/transaction/list-asset-log/validate");
const admin_add_user_asset_1 = require("../../../../usecases/user-asset/admin-add-user-asset");
const validate_5 = require("../../../../usecases/user-asset/admin-add-user-asset/validate");
const admin_remove_user_asset_1 = require("../../../../usecases/user-asset/admin-remove-user-asset");
const validate_6 = require("../../../../usecases/user-asset/admin-remove-user-asset/validate");
const handle_error_util_1 = require("../../../../utils/handle-error.util");
const hash_util_1 = require("../../../../utils/hash.util");
const response_util_1 = require("../../../../utils/response.util");
const const_3 = require("../../const");
const dto_1 = require("./dto");
let AdminUserController = class AdminUserController {
    constructor(userService, authUserRoleService, userRoleService, listAssetLogHandler, userListRoleHandler, userListBotHandler, userListBotTradingHandler, adminAddUserAssetHandler, adminRemoveUserAssetHandler, adminUpdateBotTradingStatusHandler, merchantRepository, adminResetSubscriptionHandler, adminDeleteBotTradingHandler) {
        this.userService = userService;
        this.authUserRoleService = authUserRoleService;
        this.userRoleService = userRoleService;
        this.listAssetLogHandler = listAssetLogHandler;
        this.userListRoleHandler = userListRoleHandler;
        this.userListBotHandler = userListBotHandler;
        this.userListBotTradingHandler = userListBotTradingHandler;
        this.adminAddUserAssetHandler = adminAddUserAssetHandler;
        this.adminRemoveUserAssetHandler = adminRemoveUserAssetHandler;
        this.adminUpdateBotTradingStatusHandler = adminUpdateBotTradingStatusHandler;
        this.merchantRepository = merchantRepository;
        this.adminResetSubscriptionHandler = adminResetSubscriptionHandler;
        this.adminDeleteBotTradingHandler = adminDeleteBotTradingHandler;
    }
    async login(req, body, res) {
        const userAgent = req.get('user-agent');
        const ipAddress = req.headers['x-forwarded-for'] || req.socket.remoteAddress || null;
        const result = await this.userService.loginUser(Object.assign(Object.assign({}, body), { user_type: const_1.USER_TYPE.ADMIN }), userAgent, ipAddress);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createUser(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.userService.createUser(Object.assign(Object.assign({}, body), { owner_created, is_admin: false, merchant_code: app_setting_1.APP_SETTING.MERCHANT_CODE_DEFAULT }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createAdmin(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.userService.createUser(Object.assign(Object.assign({}, body), { owner_created, is_admin: true, merchant_code: app_setting_1.APP_SETTING.MERCHANT_CODE_DEFAULT }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateUserProfile(body, req, res) {
        const user_id = req.user.user_id;
        const result = await this.userService.updateInfo(Object.assign(Object.assign({}, body), { id: user_id }), true);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateUser(body, id, res) {
        const result = await this.userService.updateInfo(Object.assign(Object.assign({}, body), { id }), false);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateAdmin(body, id, res) {
        const result = await this.userService.updateInfo(Object.assign(Object.assign({}, body), { id }), true);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createAuthUserRole(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.authUserRoleService.modifyAuthUserRole(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createUserRole(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.userRoleService.modifyUserRole(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async addUserRole(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.userRoleService.addUserRoles({
            user_ids: body.user_ids,
            owner_created: owner_created,
            role_ids: body.role_ids,
            quantity: body.quantity,
            package_type: body.type,
        });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async removeUserRole(body, res, req) {
        const owner_created = req.user.user_id;
        const result = await this.userRoleService.removeUserRoles(owner_created, Object.assign({}, body));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async AddUserAsset(body, res, req) {
        const owner_created = req.user.user_id;
        const result = await this.adminAddUserAssetHandler.execute(body, owner_created);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async RemoveUserAsset(body, res, req) {
        const owner_created = req.user.user_id;
        const result = await this.adminRemoveUserAssetHandler.execute(body, owner_created);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getUser(query, res) {
        const result = await this.userService.getUserPagination(Object.assign({}, query));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getAllUser(query, res) {
        const result = await this.userService.listUser(Object.assign({}, query));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getAdmin(query, res) {
        const result = await this.userService.getUserPagination(Object.assign(Object.assign({}, query), { is_admin: true }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getUserProfile(req, res) {
        const user_id = req.user.user_id;
        const result = await this.userService.getUser({ id: user_id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getAssetList(res, query, id) {
        const data = await this.listAssetLogHandler.execute(query, false, id);
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async getRoleList(res, id) {
        const data = await this.userListRoleHandler.execute(id);
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async getBotList(res, id) {
        const data = await this.userListBotHandler.execute(id);
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async resetSubscriptions(res) {
        const data = await this.adminResetSubscriptionHandler.execute();
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async updateUserBotTradingStatus(req, res, id, body) {
        const { authorization } = req.headers;
        if (!authorization) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNAUTHORIZED }, const_1.ERROR_CODE.AUTHORIZATION_REQUIRED));
        }
        const [type, encode] = authorization.split(' ');
        if (type.toLowerCase() === 'basic' && encode) {
            const decode = hash_util_1.decodeBase64(encode);
            const [code, password] = decode.split(':');
            const merchant = await this.merchantRepository.findOne({ code });
            if (!merchant || password !== merchant.password) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.ACCOUNT_INVALID));
            }
        }
        else {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNAUTHORIZED }, const_1.ERROR_CODE.AUTHORIZATION_REQUIRED));
        }
        const data = await this.adminUpdateBotTradingStatusHandler.execute(body, id);
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async deleteBotTrading(req, res, id) {
        const { authorization } = req.headers;
        if (!authorization) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNAUTHORIZED }, const_1.ERROR_CODE.AUTHORIZATION_REQUIRED));
        }
        const [type, encode] = authorization.split(' ');
        if (type.toLowerCase() === 'basic' && encode) {
            const decode = hash_util_1.decodeBase64(encode);
            const [code, password] = decode.split(':');
            const merchant = await this.merchantRepository.findOne({ code });
            if (!merchant || password !== merchant.password) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.ACCOUNT_INVALID));
            }
        }
        else {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNAUTHORIZED }, const_1.ERROR_CODE.AUTHORIZATION_REQUIRED));
        }
        const data = await this.adminDeleteBotTradingHandler.execute(id);
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async getBotTradingList(res, id) {
        const data = await this.userListBotTradingHandler.execute(id);
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async getUserDetail(id, res) {
        const result = await this.userService.getUser({ id, is_admin: false });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getAdminDetail(id, res) {
        const result = await this.userService.getUser({ id, is_admin: true });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
};
__decorate([
    const_3.AdminPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Post('login'),
    __param(0, common_1.Req()),
    __param(1, common_1.Body()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.PostLoginDto, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "login", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_USER.permission_id),
    common_1.Post('/user'),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostCreateUserAdminDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "createUser", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_ADMIN.permission_id),
    common_1.Post('/admin'),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostCreateUserAdminDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "createAdmin", null);
__decorate([
    common_1.Put('/admin/profile'),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutAdminUserProfileDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "updateUserProfile", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_USER.permission_id),
    common_1.Put('/user/:id'),
    __param(0, common_1.Body()),
    __param(1, common_1.Param('id')),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutUserDto, String, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "updateUser", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_ADMIN.permission_id),
    common_1.Put('/admin/:id'),
    __param(0, common_1.Body()),
    __param(1, common_1.Param('id')),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutUserDto, String, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "updateAdmin", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_AUTH_ROLE.permission_id),
    common_1.Put('auth-user-role'),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutAuthUserRoleDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "createAuthUserRole", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_USER_ROLE.permission_id),
    common_1.Put('user-role'),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutUserRoleDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "createUserRole", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_USER_ROLE.permission_id),
    common_1.Put('add-multiple-user-role'),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.AddMultipleUserRoleDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "addUserRole", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_USER_ROLE.permission_id),
    common_1.Put('remove-multiple-user-role'),
    __param(0, common_1.Body()),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.RemoveMultipleUserRoleDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "removeUserRole", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_USER.permission_id),
    common_1.Put('add-user-asset'),
    __param(0, common_1.Body()),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_5.AdminAddUserAssetInput, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "AddUserAsset", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_USER.permission_id),
    common_1.Put('remove-user-asset'),
    __param(0, common_1.Body()),
    __param(1, common_1.Res()),
    __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_6.AdminRemoveUserAssetInput, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "RemoveUserAsset", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.GET_USER.permission_id),
    common_1.Get('/user/list'),
    __param(0, common_1.Query()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.GetUserDto, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "getUser", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.GET_USER.permission_id),
    common_1.Get('/user/all'),
    __param(0, common_1.Query()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.GetAllUserDto, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "getAllUser", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.GET_ADMIN.permission_id),
    common_1.Get('/admin/list'),
    __param(0, common_1.Query()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.GetUserDto, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "getAdmin", null);
__decorate([
    common_1.Get('/admin/profile'),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "getUserProfile", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.GET_USER.permission_id),
    common_1.Get('/user/:id/asset'),
    swagger_1.ApiResponse({
        status: 200,
        type: class ListAssetLogPayloadOutputMap extends swagger_1.IntersectionType(validate_4.ListAssetLogPayloadOutput, const_2.BaseApiOutput) {
        },
        description: 'List asset for user',
    }),
    __param(0, common_1.Res()),
    __param(1, common_1.Query()),
    __param(2, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_4.ListAssetLogInput, String]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "getAssetList", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.GET_USER.permission_id),
    common_1.Get('/user/:id/role'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "getRoleList", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.GET_USER.permission_id),
    common_1.Get('/user/:id/sbot'),
    swagger_1.ApiResponse({
        status: 200,
        type: class UserListBotOutputMap extends swagger_1.IntersectionType(validate_3.UserListBotOutput, const_2.BaseApiOutput) {
        },
        description: 'List asset for user',
    }),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "getBotList", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_USER.permission_id),
    common_1.Put('/user/tbot/reset-subscriptions'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "resetSubscriptions", null);
__decorate([
    const_3.AdminPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Put('/user/tbot/:id'),
    swagger_1.ApiResponse({
        status: 200,
        type: class AdminUpdateBotTradingStatusOutputMap extends swagger_1.IntersectionType(validate_1.AdminUpdateBotTradingStatusOutput, const_2.BaseApiOutput) {
        },
        description: 'Update User Bot Trading Status',
    }),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __param(2, common_1.Param('id')),
    __param(3, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, String, validate_1.AdminUpdateBotTradingStatusInput]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "updateUserBotTradingStatus", null);
__decorate([
    const_3.AdminPermission(const_1.WITHOUT_AUTHORIZATION),
    common_1.Delete('/user/tbot/:id'),
    __param(0, common_1.Req()),
    __param(1, common_1.Res()),
    __param(2, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, String]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "deleteBotTrading", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.GET_USER.permission_id),
    common_1.Get('/user/:id/tbot'),
    swagger_1.ApiResponse({
        status: 200,
        type: class UserListBotTradingOutputMap extends swagger_1.IntersectionType(validate_2.UserListBotTradingOutput, const_2.BaseApiOutput) {
        },
        description: 'List asset for user',
    }),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "getBotTradingList", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.GET_USER.permission_id),
    common_1.Get('/user/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "getUserDetail", null);
__decorate([
    const_3.AdminPermission(const_1.ADMIN_PERMISSION.GET_ADMIN.permission_id),
    common_1.Get('/admin/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminUserController.prototype, "getAdminDetail", null);
AdminUserController = __decorate([
    swagger_1.ApiBearerAuth(),
    common_1.UseGuards(const_3.AdminAuthGuard),
    swagger_1.ApiTags('admin'),
    common_1.Controller(''),
    __param(1, common_1.Inject(auth_user_role_1.AuthUserRoleService)),
    __param(2, common_1.Inject(user_role_1.UserRoleService)),
    __param(3, common_1.Inject(list_asset_log_1.ListAssetLogHandler)),
    __param(4, common_1.Inject(user_list_role_1.UserListRoleHandler)),
    __param(5, common_1.Inject(user_list_bot_1.UserListBotHandler)),
    __param(6, common_1.Inject(user_list_bot_trading_1.UserListBotTradingHandler)),
    __param(7, common_1.Inject(admin_add_user_asset_1.AdminAddUserAssetHandler)),
    __param(8, common_1.Inject(admin_remove_user_asset_1.AdminRemoveUserAssetHandler)),
    __param(9, common_1.Inject(admin_update_bot_trading_status_1.AdminUpdateBotTradingStatusHandler)),
    __param(10, common_1.Inject(merchant_repository_1.MerchantRepository)),
    __param(11, common_1.Inject(reset_subscriptions_1.AdminResetSubscriptionHandler)),
    __param(12, common_1.Inject(admin_delete_bot_trading_1.AdminDeleteBotTradingHandler)),
    __metadata("design:paramtypes", [user_1.UserService,
        auth_user_role_1.AuthUserRoleService,
        user_role_1.UserRoleService,
        list_asset_log_1.ListAssetLogHandler,
        user_list_role_1.UserListRoleHandler,
        user_list_bot_1.UserListBotHandler,
        user_list_bot_trading_1.UserListBotTradingHandler,
        admin_add_user_asset_1.AdminAddUserAssetHandler,
        admin_remove_user_asset_1.AdminRemoveUserAssetHandler,
        admin_update_bot_trading_status_1.AdminUpdateBotTradingStatusHandler,
        merchant_repository_1.MerchantRepository,
        reset_subscriptions_1.AdminResetSubscriptionHandler,
        admin_delete_bot_trading_1.AdminDeleteBotTradingHandler])
], AdminUserController);
exports.AdminUserController = AdminUserController;
//# sourceMappingURL=index.js.map