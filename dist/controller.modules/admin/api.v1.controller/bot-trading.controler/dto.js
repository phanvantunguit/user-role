"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PutBotTradingOrderDto = exports.PutBotTradingDto = exports.PostBotTradingDto = exports.QueryBotTradingDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const const_1 = require("../../../../const");
class QueryBotTradingDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'id of user',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], QueryBotTradingDto.prototype, "user_id", void 0);
exports.QueryBotTradingDto = QueryBotTradingDto;
class PostBotTradingDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of bot',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Clone Name of bot',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "clone_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Bot code',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "code", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Bot type',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: `Status: ${Object.values(const_1.BOT_STATUS)}`,
        example: const_1.BOT_STATUS.OPEN,
    }),
    class_validator_1.IsString(),
    class_validator_1.IsIn(Object.values(const_1.BOT_STATUS)),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Price upgrade',
        example: '0.001',
    }),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Display price',
        example: '0.001',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "display_price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Balance default',
        example: '500000',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "balance", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Pnl upgrade',
        example: '999.9',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "pnl", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Max Drawdown',
        example: '30.31',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "max_drawdown", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Max drawdown change percent',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "max_drawdown_change_percent", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'currency',
        example: 'TLCT',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Describe detail about bot',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Sort role',
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PostBotTradingDto.prototype, "order", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'List feature display for user',
        example: [
            'Crypto Data',
            'Footprint chat',
            'Volume profile',
            'Delta & cumulative delta',
            'Multiple panels',
        ],
    }),
    class_validator_1.IsArray(),
    class_validator_1.IsOptional(),
    class_validator_1.IsString({ each: true }),
    __metadata("design:type", Array)
], PostBotTradingDto.prototype, "work_based_on", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'translation',
    }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Object)
], PostBotTradingDto.prototype, "translation", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Url image',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "image_url", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Data back test',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotTradingDto.prototype, "back_test", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Number of user bought',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PostBotTradingDto.prototype, "bought", void 0);
exports.PostBotTradingDto = PostBotTradingDto;
class PutBotTradingDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Name of bot',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Clone Name of bot',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "clone_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Bot code',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "code", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Bot type',
        example: 'FUTURE',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: `Status: ${Object.values(const_1.BOT_STATUS)}`,
        example: const_1.BOT_STATUS.OPEN,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    class_validator_1.IsIn(Object.values(const_1.BOT_STATUS)),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Price upgrade',
        example: '0.001',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Display price',
        example: '0.001',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "display_price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Balance default',
        example: '500000',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "balance", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Pnl upgrade',
        example: '999.9',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "pnl", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Max Drawdown',
        example: '30.31',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "max_drawdown", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Max drawdown change percent',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "max_drawdown_change_percent", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'currency',
        example: 'TLCT',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Describe detail about bot',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Sort role',
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PutBotTradingDto.prototype, "order", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'List feature display for user',
        example: [
            'Crypto Data',
            'Footprint chat',
            'Volume profile',
            'Delta & cumulative delta',
            'Multiple panels',
        ],
    }),
    class_validator_1.IsArray(),
    class_validator_1.IsString({ each: true }),
    __metadata("design:type", Array)
], PutBotTradingDto.prototype, "work_based_on", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'translation',
    }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Object)
], PutBotTradingDto.prototype, "translation", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Url image',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "image_url", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Data back test',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotTradingDto.prototype, "back_test", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Number of user bought',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PutBotTradingDto.prototype, "bought", void 0);
exports.PutBotTradingDto = PutBotTradingDto;
class BotTradingOrderDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'id of bot',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], BotTradingOrderDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'order of bot',
        example: 1,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], BotTradingOrderDto.prototype, "order", void 0);
class PutBotTradingOrderDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'list bot',
        example: [
            { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2', order: 1 },
            { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e3', order: 2 },
        ],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], PutBotTradingOrderDto.prototype, "bots", void 0);
exports.PutBotTradingOrderDto = PutBotTradingOrderDto;
//# sourceMappingURL=dto.js.map