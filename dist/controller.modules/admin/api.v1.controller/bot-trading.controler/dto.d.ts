import { BOT_STATUS } from 'src/const';
export declare class QueryBotTradingDto {
    user_id: string;
}
export declare class PostBotTradingDto {
    name: string;
    clone_name: string;
    code: string;
    type: string;
    status: BOT_STATUS;
    price: string;
    display_price: string;
    balance: string;
    pnl: string;
    max_drawdown: string;
    max_drawdown_change_percent: string;
    currency: string;
    description?: string;
    order?: number;
    work_based_on?: string[];
    translation: any;
    image_url: string;
    back_test: string;
    bought: number;
}
export declare class PutBotTradingDto {
    name?: string;
    clone_name: string;
    code?: string;
    type?: string;
    status?: BOT_STATUS;
    price?: string;
    display_price: string;
    balance: string;
    pnl: string;
    max_drawdown: string;
    max_drawdown_change_percent: string;
    currency?: string;
    description?: string;
    order?: number;
    work_based_on?: string[];
    translation: any;
    image_url?: string;
    back_test: string;
    bought: number;
}
declare class BotTradingOrderDto {
    id: string;
    order: number;
}
export declare class PutBotTradingOrderDto {
    bots: BotTradingOrderDto[];
}
export {};
