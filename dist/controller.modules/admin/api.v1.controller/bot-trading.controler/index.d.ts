import { Response } from 'express';
import { BotTradingService } from 'src/services/bot-trading';
import { PostBotTradingDto, QueryBotTradingDto, PutBotTradingDto, PutBotTradingOrderDto } from './dto';
export declare class AdminBotTradingController {
    private botTradingService;
    constructor(botTradingService: BotTradingService);
    listBot(query: QueryBotTradingDto, res: Response): Promise<void>;
    getBotDetail(id: string, res: Response): Promise<void>;
    createBotTrading(body: PostBotTradingDto, req: any, res: Response): Promise<void>;
    updateBot(id: string, body: PutBotTradingDto, req: any, res: Response): Promise<void>;
    updateOrderBot(body: PutBotTradingOrderDto, req: any, res: Response): Promise<void>;
    deleteBot(id: string, req: any, res: Response): Promise<void>;
}
