import { PACKAGE_TYPE, SYMBOL_STATUS } from 'src/const';
export declare class PutFeatureDto {
    feature_id: string;
    feature_name: string;
    description: string;
    action: string;
}
export declare class PostFeatureDto {
    features: PutFeatureDto[];
}
export declare class PutExchangeDto {
    exchange_name: string;
    exchange_desc: string;
}
export declare class PostExchangeDto {
    exchanges: PutExchangeDto[];
}
export declare class PutGeneralSettingDto {
    general_setting_id: string;
    general_setting_name: string;
    description: string;
}
export declare class PostGeneralSettingDto {
    general_settings: PutGeneralSettingDto[];
}
export declare class PutResolutionDto {
    resolutions_name: string;
    display_name: string;
}
export declare class PostResolutionDto {
    resolutions: PutResolutionDto[];
}
export declare class PutSymbolDto {
    types: string;
    exchange_name: string;
    base_symbol: string;
    quote_symbol: string;
    description: string;
    ticks: object;
    status: SYMBOL_STATUS;
    timezone: string;
    minmov: number;
    minmov2: number;
    pointvalue: number;
    session: string;
    has_intraday: boolean;
    has_no_volume: boolean;
    pricescale: number;
}
declare class SymbolDto {
    symbol: string;
    types: string;
    exchange_name: string;
    base_symbol: string;
    quote_symbol: string;
    description: string;
    ticks: object;
    status: SYMBOL_STATUS;
    timezone: string;
    minmov: number;
    minmov2: number;
    pointvalue: number;
    session: string;
    has_intraday: boolean;
    has_no_volume: boolean;
    pricescale: number;
}
export declare class PostSymbolDto {
    symbols: SymbolDto[];
}
export declare class PostAppSettingDto {
    name: string;
    value: string;
    description: string;
}
export declare class PutAppSettingDto {
    name: string;
    value: string;
    description: string;
}
export declare class PostBotSettingDto {
    name: string;
    params: any;
    status: boolean;
}
export declare class PutBotSettingDto {
    name: string;
    params: any;
    status: boolean;
}
export declare class PostPackageDto {
    name: string;
    type: PACKAGE_TYPE;
    discount_rate: number;
    discount_amount: number;
    status: boolean;
    quantity: number;
    expires_at: number;
}
export declare class PutPackageDto {
    name: string;
    type: PACKAGE_TYPE;
    discount_rate: number;
    discount_amount: number;
    status: boolean;
    quantity: number;
    expires_at: number;
}
export declare class PostCurrencyDto {
    name: string;
    description: string;
    currency: string;
    image_url: string;
}
export declare class PutCurrencyDto {
    name: string;
    description: string;
    currency: string;
    image_url: string;
    status: boolean;
}
declare class CurrencyOrderDto {
    id: string;
    order: number;
}
export declare class PutCurrencyOrderDto {
    currencies: CurrencyOrderDto[];
}
export {};
