"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminSettingController = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const app_setting_1 = require("../../../../services/app-setting");
const bot_setting_1 = require("../../../../services/bot-setting");
const currency_1 = require("../../../../services/currency");
const exchange_1 = require("../../../../services/exchange");
const feature_1 = require("../../../../services/feature");
const general_setting_1 = require("../../../../services/general-setting");
const package_1 = require("../../../../services/package");
const resolution_1 = require("../../../../services/resolution");
const symbol_1 = require("../../../../services/symbol");
const storage_1 = require("../../../../resources/storage");
const response_util_1 = require("../../../../utils/response.util");
const const_2 = require("../../const");
const dto_1 = require("./dto");
const handle_error_util_1 = require("../../../../utils/handle-error.util");
let AdminSettingController = class AdminSettingController {
    constructor(featureService, exchangeService, generalSettingService, resolutionService, symbolService, appSettingService, botSettingService, packageService, currencyService, storageResource) {
        this.featureService = featureService;
        this.exchangeService = exchangeService;
        this.generalSettingService = generalSettingService;
        this.resolutionService = resolutionService;
        this.symbolService = symbolService;
        this.appSettingService = appSettingService;
        this.botSettingService = botSettingService;
        this.packageService = packageService;
        this.currencyService = currencyService;
        this.storageResource = storageResource;
    }
    async getFeatures(res) {
        const result = await this.featureService.getFeature();
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getFeatureDetail(id, res) {
        const result = await this.featureService.getFeatureDetail({
            feature_id: id,
        });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createFeatures(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.featureService.createFeature(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateFeature(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.featureService.updateFeature(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async deleteFeature(id, res) {
        const result = await this.featureService.deleteFeature([id]);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listExchange(res) {
        const result = await this.exchangeService.listExchange();
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getExchange(name, res) {
        const result = await this.exchangeService.getExchange({
            exchange_name: name,
        });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createExchanges(body, req, res) {
        const result = await this.exchangeService.createExchange(body.exchanges);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateExchange(body, req, res) {
        const result = await this.exchangeService.updateExchange(Object.assign({}, body));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async deleteExchange(name, res) {
        const result = await this.exchangeService.deleteExchange([name]);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listGeneralSetting(res) {
        const result = await this.generalSettingService.listGeneralSetting();
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getGeneralSetting(id, res) {
        const result = await this.generalSettingService.getGeneralSetting({
            general_setting_id: id,
        });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createGeneralSetting(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.generalSettingService.createGeneralSetting(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateGeneralSetting(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.generalSettingService.updateGeneralSetting(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async deleteGeneralSetting(id, res) {
        const result = await this.generalSettingService.deleteGeneralSetting([id]);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listResolution(res) {
        const result = await this.resolutionService.listResolution();
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getResolution(id, res) {
        const result = await this.resolutionService.getResolution({ id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createResolution(body, res) {
        const result = await this.resolutionService.createResolution(body.resolutions);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateResolution(id, body, res) {
        const result = await this.resolutionService.updateResolution(Object.assign(Object.assign({}, body), { id }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async deleteResolution(id, res) {
        const result = await this.resolutionService.deleteResolution([id]);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listSymbol(res) {
        const result = await this.symbolService.listSymbol();
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getSymbol(symbol, res) {
        const result = await this.symbolService.getSymbol({ symbol });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createSymbol(body, res) {
        const result = await this.symbolService.createSymbol(body.symbols);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateSymbol(symbol, body, res) {
        const result = await this.symbolService.updateSymbol(Object.assign(Object.assign({}, body), { symbol }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async deleteSymbol(symbol, res) {
        const result = await this.symbolService.deleteSymbol([symbol]);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listAppSetting(res) {
        const result = await this.appSettingService.listAppSetting({});
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getAppSetting(id, res) {
        const result = await this.appSettingService.getAppSetting({ id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createAppSetting(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.appSettingService.createAppSetting(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateAppSetting(id, body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.appSettingService.updateAppSetting(Object.assign(Object.assign({}, body), { id,
            owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async deleteAppSetting(id, res) {
        const result = await this.appSettingService.deleteAppSetting(id);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listBotSetting(res) {
        const result = await this.botSettingService.list({});
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getBotSetting(id, res) {
        const result = await this.botSettingService.get({ id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async creatrBotSetting(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.botSettingService.save(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateBotSetting(id, body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.botSettingService.save(Object.assign(Object.assign({}, body), { id,
            owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async deleteBotSetting(id, res) {
        const result = await this.botSettingService.delete({ id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getList(res) {
        const result = await this.packageService.list({});
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getPackage(id, res) {
        const result = await this.packageService.checkNotFoundAndThrow({ id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createPackage(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.packageService.save(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updatePackage(id, body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.packageService.save(Object.assign(Object.assign({}, body), { id,
            owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async deletePackage(id, res) {
        const result = await this.packageService.delete({ id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getCurrencyList(res) {
        const result = await this.currencyService.list({});
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateOrderRole(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.currencyService.updateOrderCurrency({
            currencies: body.currencies,
            owner_created,
        });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async getCurrency(id, res) {
        const result = await this.currencyService.get({ id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createCurrency(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.currencyService.save(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateCurrency(id, body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.currencyService.save(Object.assign(Object.assign({}, body), { id,
            owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async deleteCurrency(id, res) {
        const result = await this.currencyService.delete({ id });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async uploadFile(file, res) {
        if (file) {
            const file_url = await this.storageResource.upload(file);
            response_util_1.resSuccess({
                payload: { file_url },
                res,
            });
        }
        else {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BAD_REQUEST));
        }
    }
};
__decorate([
    common_1.Get('feature/list'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_FEATURE.permission_id),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "getFeatures", null);
__decorate([
    common_1.Get('feature/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_FEATURE.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "getFeatureDetail", null);
__decorate([
    common_1.Post('feature'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_FEATURE.permission_id),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostFeatureDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "createFeatures", null);
__decorate([
    common_1.Put('feature'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_FEATURE.permission_id),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutFeatureDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "updateFeature", null);
__decorate([
    common_1.Delete('feature/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.DELETE_FEATURE.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "deleteFeature", null);
__decorate([
    common_1.Get('exchange/list'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_EXCHANGE.permission_id),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "listExchange", null);
__decorate([
    common_1.Get('exchange/:name'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_EXCHANGE.permission_id),
    __param(0, common_1.Param('name')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "getExchange", null);
__decorate([
    common_1.Post('exchange'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_EXCHANGE.permission_id),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostExchangeDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "createExchanges", null);
__decorate([
    common_1.Put('exchange'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_EXCHANGE.permission_id),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutExchangeDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "updateExchange", null);
__decorate([
    common_1.Delete('exchange/:name'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.DELETE_EXCHANGE.permission_id),
    __param(0, common_1.Param('name')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "deleteExchange", null);
__decorate([
    common_1.Get('general-setting/list'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_GENERAL_SETTING.permission_id),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "listGeneralSetting", null);
__decorate([
    common_1.Get('general-setting/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_GENERAL_SETTING.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "getGeneralSetting", null);
__decorate([
    common_1.Post('general-setting'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_GENERAL_SETTING.permission_id),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostGeneralSettingDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "createGeneralSetting", null);
__decorate([
    common_1.Put('general-setting'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_EXCHANGE.permission_id),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutGeneralSettingDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "updateGeneralSetting", null);
__decorate([
    common_1.Delete('general-setting/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.DELETE_GENERAL_SETTING.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "deleteGeneralSetting", null);
__decorate([
    common_1.Get('resolution/list'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_RESOLUTION.permission_id),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "listResolution", null);
__decorate([
    common_1.Get('resolution/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_RESOLUTION.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "getResolution", null);
__decorate([
    common_1.Post('resolution'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_RESOLUTION.permission_id),
    __param(0, common_1.Body()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostResolutionDto, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "createResolution", null);
__decorate([
    common_1.Put('resolution/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_RESOLUTION.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, dto_1.PutResolutionDto, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "updateResolution", null);
__decorate([
    common_1.Delete('resolution/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.DELETE_RESOLUTION.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "deleteResolution", null);
__decorate([
    common_1.Get('symbol/list'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_SYMBOL.permission_id),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "listSymbol", null);
__decorate([
    common_1.Get('symbol/:symbol'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_SYMBOL.permission_id),
    __param(0, common_1.Param('symbol')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "getSymbol", null);
__decorate([
    common_1.Post('symbol'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_SYMBOL.permission_id),
    __param(0, common_1.Body()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostSymbolDto, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "createSymbol", null);
__decorate([
    common_1.Put('symbol/:symbol'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_SYMBOL.permission_id),
    __param(0, common_1.Param('symbol')),
    __param(1, common_1.Body()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, dto_1.PutSymbolDto, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "updateSymbol", null);
__decorate([
    common_1.Delete('symbol/:symbol'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.DELETE_SYMBOL.permission_id),
    __param(0, common_1.Param('symbol')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "deleteSymbol", null);
__decorate([
    common_1.Get('app-setting/list'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_APP_SETTING.permission_id),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "listAppSetting", null);
__decorate([
    common_1.Get('app-setting/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_APP_SETTING.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "getAppSetting", null);
__decorate([
    common_1.Post('app-setting'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_APP_SETTING.permission_id),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostAppSettingDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "createAppSetting", null);
__decorate([
    common_1.Put('app-setting/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_SYMBOL.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __param(2, common_1.Req()),
    __param(3, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, dto_1.PutAppSettingDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "updateAppSetting", null);
__decorate([
    common_1.Delete('app-setting/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.DELETE_SYMBOL.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "deleteAppSetting", null);
__decorate([
    common_1.Get('bot/list'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_BOT_SETTING.permission_id),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "listBotSetting", null);
__decorate([
    common_1.Get('bot/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_BOT_SETTING.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "getBotSetting", null);
__decorate([
    common_1.Post('bot'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_APP_SETTING.permission_id),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostBotSettingDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "creatrBotSetting", null);
__decorate([
    common_1.Put('bot/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_SYMBOL.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __param(2, common_1.Req()),
    __param(3, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, dto_1.PutBotSettingDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "updateBotSetting", null);
__decorate([
    common_1.Delete('bot/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.DELETE_SYMBOL.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "deleteBotSetting", null);
__decorate([
    common_1.Get('package/list'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_PACKAGE.permission_id),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "getList", null);
__decorate([
    common_1.Get('package/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_PACKAGE.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "getPackage", null);
__decorate([
    common_1.Post('package'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_PACKAGE.permission_id),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostPackageDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "createPackage", null);
__decorate([
    common_1.Put('package/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_PACKAGE.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __param(2, common_1.Req()),
    __param(3, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, dto_1.PutPackageDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "updatePackage", null);
__decorate([
    common_1.Delete('package/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.DELETE_PACKAGE.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "deletePackage", null);
__decorate([
    common_1.Get('currency/list'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_CURRENCY.permission_id),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "getCurrencyList", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_ROLE.permission_id),
    common_1.Put('currency/order'),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PutCurrencyOrderDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "updateOrderRole", null);
__decorate([
    common_1.Get('currency/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_CURRENCY.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "getCurrency", null);
__decorate([
    common_1.Post('currency'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_CURRENCY.permission_id),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostCurrencyDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "createCurrency", null);
__decorate([
    common_1.Put('currency/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_CURRENCY.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __param(2, common_1.Req()),
    __param(3, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, dto_1.PutCurrencyDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "updateCurrency", null);
__decorate([
    common_1.Delete('currency/:id'),
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.DELETE_CURRENCY.permission_id),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "deleteCurrency", null);
__decorate([
    common_1.Post('/upload'),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file')),
    __param(0, common_1.UploadedFile()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AdminSettingController.prototype, "uploadFile", null);
AdminSettingController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiTags('admin/setting'),
    common_1.Controller('setting'),
    common_1.UseGuards(const_2.AdminAuthGuard),
    __metadata("design:paramtypes", [feature_1.FeatureService,
        exchange_1.ExchangeService,
        general_setting_1.GeneralSettingService,
        resolution_1.ResolutionService,
        symbol_1.SymbolService,
        app_setting_1.AppSettingService,
        bot_setting_1.BotSettingService,
        package_1.PackageService,
        currency_1.CurrencyService,
        storage_1.StorageResource])
], AdminSettingController);
exports.AdminSettingController = AdminSettingController;
//# sourceMappingURL=index.js.map