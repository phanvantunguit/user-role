"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PutCurrencyOrderDto = exports.PutCurrencyDto = exports.PostCurrencyDto = exports.PutPackageDto = exports.PostPackageDto = exports.PutBotSettingDto = exports.PostBotSettingDto = exports.PutAppSettingDto = exports.PostAppSettingDto = exports.PostSymbolDto = exports.PutSymbolDto = exports.PostResolutionDto = exports.PutResolutionDto = exports.PostGeneralSettingDto = exports.PutGeneralSettingDto = exports.PostExchangeDto = exports.PutExchangeDto = exports.PostFeatureDto = exports.PutFeatureDto = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
class PutFeatureDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Id unique define specific feature',
        example: 'GET_CHART',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutFeatureDto.prototype, "feature_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of feature',
        example: 'View chart',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutFeatureDto.prototype, "feature_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Describe detail about feature',
        example: 'View chart 1',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutFeatureDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Action of feature (create, update, view)',
        example: 'view',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutFeatureDto.prototype, "action", void 0);
exports.PutFeatureDto = PutFeatureDto;
class PostFeatureDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Features',
        example: [
            {
                feature_id: 'GET_CHART',
                feature_name: 'View chart',
                description: 'View chart 1',
                action: 'view',
            },
        ],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], PostFeatureDto.prototype, "features", void 0);
exports.PostFeatureDto = PostFeatureDto;
class PutExchangeDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of exchange',
        example: 'exchange',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutExchangeDto.prototype, "exchange_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Describe detail about exchange',
        example: 'describe',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutExchangeDto.prototype, "exchange_desc", void 0);
exports.PutExchangeDto = PutExchangeDto;
class PostExchangeDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Exchanges',
        example: [
            {
                exchange_name: 'exchange',
                exchange_desc: 'describe',
            },
        ],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], PostExchangeDto.prototype, "exchanges", void 0);
exports.PostExchangeDto = PostExchangeDto;
class PutGeneralSettingDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Id of general setting',
        example: 'template_qty',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutGeneralSettingDto.prototype, "general_setting_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of general setting',
        example: 'Template quantity',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutGeneralSettingDto.prototype, "general_setting_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Describe detail about setting',
        example: "Number of customer's template",
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutGeneralSettingDto.prototype, "description", void 0);
exports.PutGeneralSettingDto = PutGeneralSettingDto;
class PostGeneralSettingDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'General settings',
        example: [
            {
                general_setting_id: 'template_qty',
                general_setting_name: 'Template quantity',
                description: "Number of customer's template",
            },
        ],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], PostGeneralSettingDto.prototype, "general_settings", void 0);
exports.PostGeneralSettingDto = PostGeneralSettingDto;
class PutResolutionDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'resolutions',
        example: '720',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutResolutionDto.prototype, "resolutions_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Display name of resolutions',
        example: '12h',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutResolutionDto.prototype, "display_name", void 0);
exports.PutResolutionDto = PutResolutionDto;
class PostResolutionDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'resolutions',
        example: [
            {
                resolutions_name: '720',
                display_name: '12h',
            },
        ],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], PostResolutionDto.prototype, "resolutions", void 0);
exports.PostResolutionDto = PostResolutionDto;
class PutSymbolDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Type of symbol',
        example: 'type 1',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutSymbolDto.prototype, "types", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Name of exchange',
        example: 'exchange',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutSymbolDto.prototype, "exchange_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'base symbol',
        example: 'base_symbol',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutSymbolDto.prototype, "base_symbol", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'quote symbol',
        example: 'quote_symbol',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutSymbolDto.prototype, "quote_symbol", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: '',
        example: 'Bitcoin / TetherUS',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutSymbolDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: '',
        example: {
            tick: 10,
            stepbase: 3,
            stepquote: 5,
            tickvalue: 10,
            muldecimal: 0,
            numdecimal: 1,
            ticksizemin: 0.000001,
        },
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsObject(),
    __metadata("design:type", Object)
], PutSymbolDto.prototype, "ticks", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Status of symbol',
        example: const_1.SYMBOL_STATUS.ON,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsIn(Object.values(const_1.SYMBOL_STATUS)),
    __metadata("design:type", String)
], PutSymbolDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 'UTC',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutSymbolDto.prototype, "timezone", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PutSymbolDto.prototype, "minmov", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 0,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PutSymbolDto.prototype, "minmov2", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PutSymbolDto.prototype, "pointvalue", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: '24x7',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutSymbolDto.prototype, "session", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: true,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsBoolean(),
    __metadata("design:type", Boolean)
], PutSymbolDto.prototype, "has_intraday", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: true,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsBoolean(),
    __metadata("design:type", Boolean)
], PutSymbolDto.prototype, "has_no_volume", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 10000,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PutSymbolDto.prototype, "pricescale", void 0);
exports.PutSymbolDto = PutSymbolDto;
class SymbolDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of symbol',
        example: 'BTC',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], SymbolDto.prototype, "symbol", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Type of symbol',
        example: 'type 1',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], SymbolDto.prototype, "types", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of exchange',
        example: 'exchange',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], SymbolDto.prototype, "exchange_name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'base symbol',
        example: 'base_symbol',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], SymbolDto.prototype, "base_symbol", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'quote symbol',
        example: 'quote_symbol',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], SymbolDto.prototype, "quote_symbol", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: '',
        example: 'Bitcoin / TetherUS',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], SymbolDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: '',
        example: {
            tickvalue: 10,
            tickvalueHeatmap: 10,
        },
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsObject(),
    __metadata("design:type", Object)
], SymbolDto.prototype, "ticks", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Status of symbol',
        example: const_1.SYMBOL_STATUS.ON,
    }),
    class_validator_1.IsIn(Object.values(const_1.SYMBOL_STATUS)),
    __metadata("design:type", String)
], SymbolDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 'UTC',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], SymbolDto.prototype, "timezone", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], SymbolDto.prototype, "minmov", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 0,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], SymbolDto.prototype, "minmov2", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], SymbolDto.prototype, "pointvalue", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: '24x7',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], SymbolDto.prototype, "session", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: true,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsBoolean(),
    __metadata("design:type", Boolean)
], SymbolDto.prototype, "has_intraday", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: true,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsBoolean(),
    __metadata("design:type", Boolean)
], SymbolDto.prototype, "has_no_volume", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 10000,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], SymbolDto.prototype, "pricescale", void 0);
class PostSymbolDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Symbols',
        example: [
            {
                symbol: 'BTC',
                types: 'type 1',
                exchange_name: 'exchange',
                base_symbol: 'base_symbol',
                quote_symbol: 'base_symbol',
                description: 'Bitcoin / TetherUS',
                ticks: {
                    tick: 10,
                    stepbase: 3,
                    stepquote: 5,
                    tickvalue: 10,
                    muldecimal: 0,
                    numdecimal: 1,
                    ticksizemin: 0.000001,
                },
            },
        ],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], PostSymbolDto.prototype, "symbols", void 0);
exports.PostSymbolDto = PostSymbolDto;
class PostAppSettingDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of app setting',
        example: 'ON_OFF_REGISTER',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostAppSettingDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Value of app setting',
        example: 'ON',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostAppSettingDto.prototype, "value", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Description of app setting',
        example: '',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostAppSettingDto.prototype, "description", void 0);
exports.PostAppSettingDto = PostAppSettingDto;
class PutAppSettingDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Name of app setting',
        example: 'ON_OFF_REGISTER',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutAppSettingDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Value of app setting',
        example: 'ON',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutAppSettingDto.prototype, "value", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Description of app setting',
        example: '',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutAppSettingDto.prototype, "description", void 0);
exports.PutAppSettingDto = PutAppSettingDto;
class PostBotSettingDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of bot setting',
        example: 'Violency Pinbar',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotSettingDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Params of bot setting',
        example: {},
    }),
    class_validator_1.IsObject(),
    __metadata("design:type", Object)
], PostBotSettingDto.prototype, "params", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Status of bot setting',
        example: true,
    }),
    class_validator_1.IsBoolean(),
    __metadata("design:type", Boolean)
], PostBotSettingDto.prototype, "status", void 0);
exports.PostBotSettingDto = PostBotSettingDto;
class PutBotSettingDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Name of bot setting',
        example: 'Violency Pinbar',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutBotSettingDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Params of bot setting',
        example: {},
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsObject(),
    __metadata("design:type", Object)
], PutBotSettingDto.prototype, "params", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Status of bot setting',
        example: true,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsBoolean(),
    __metadata("design:type", Boolean)
], PutBotSettingDto.prototype, "status", void 0);
exports.PutBotSettingDto = PutBotSettingDto;
class PostPackageDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of package',
        example: '3 months',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostPackageDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Type of package MONTH or DAY',
        example: const_1.PACKAGE_TYPE.MONTH,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostPackageDto.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Discount rate',
        example: 0.1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PostPackageDto.prototype, "discount_rate", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Discount amount',
        example: 10,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PostPackageDto.prototype, "discount_amount", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Status of package',
        example: true,
    }),
    class_validator_1.IsBoolean(),
    __metadata("design:type", Boolean)
], PostPackageDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Quantity of months',
        example: 1,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PostPackageDto.prototype, "quantity", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Expires for package',
        example: 1660186416132,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PostPackageDto.prototype, "expires_at", void 0);
exports.PostPackageDto = PostPackageDto;
class PutPackageDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of package',
        example: '3 months',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutPackageDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Type of package MONTH or DAY',
        example: const_1.PACKAGE_TYPE.MONTH,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutPackageDto.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Discount rate',
        example: 0.1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PutPackageDto.prototype, "discount_rate", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Discount amount',
        example: '10',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PutPackageDto.prototype, "discount_amount", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Status of package',
        example: true,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsBoolean(),
    __metadata("design:type", Boolean)
], PutPackageDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Quantity of months',
        example: '1',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PutPackageDto.prototype, "quantity", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Expires for package',
        example: 1660186416132,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PutPackageDto.prototype, "expires_at", void 0);
exports.PutPackageDto = PutPackageDto;
class PostCurrencyDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name of currency',
        example: 'Litecoin Testnet2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostCurrencyDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Code display',
        example: 'LTCT',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostCurrencyDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Currency',
        example: 'LTCT',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostCurrencyDto.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Link of image currency',
        example: 'link',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostCurrencyDto.prototype, "image_url", void 0);
exports.PostCurrencyDto = PostCurrencyDto;
class PutCurrencyDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Name of currency',
        example: 'Litecoin Testnet2',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutCurrencyDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Code display',
        example: 'LTCT',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutCurrencyDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Currency',
        example: 'LTCT',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutCurrencyDto.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Link of image currency',
        example: 'link',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutCurrencyDto.prototype, "image_url", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'On off currency',
        example: true,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsBoolean(),
    __metadata("design:type", Boolean)
], PutCurrencyDto.prototype, "status", void 0);
exports.PutCurrencyDto = PutCurrencyDto;
class CurrencyOrderDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'id of currency',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CurrencyOrderDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'order of role',
        example: 1,
    }),
    class_validator_1.IsString(),
    __metadata("design:type", Number)
], CurrencyOrderDto.prototype, "order", void 0);
class PutCurrencyOrderDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'list currency',
        example: [
            { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2', order: 1 },
            { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e3', order: 2 },
        ],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], PutCurrencyOrderDto.prototype, "currencies", void 0);
exports.PutCurrencyOrderDto = PutCurrencyOrderDto;
//# sourceMappingURL=dto.js.map