import { Response } from 'express';
import { BotTradingHistoryService } from 'src/services/bot-trading-history';
import { AdminImportTradeHistoryCSVHandler } from 'src/usecases/bot/admin-import-trade-history-csv';
import { AdminListTradeHistoryHandler } from 'src/usecases/bot/admin-list-trade-history';
import { AdminListTradeHistoryInput } from 'src/usecases/bot/admin-list-trade-history/validate';
import { PostBotTradingHistoryDto, QueryBotTradingHistoryDto } from './dto';
export declare class AdminBotTradingHistoryController {
    private botTradingHistoryService;
    private adminImportTradeHistoryCSVHandler;
    private adminListTradeHistoryHandler;
    constructor(botTradingHistoryService: BotTradingHistoryService, adminImportTradeHistoryCSVHandler: AdminImportTradeHistoryCSVHandler, adminListTradeHistoryHandler: AdminListTradeHistoryHandler);
    tradeHistory(query: AdminListTradeHistoryInput, res: Response): Promise<void>;
    listBot(query: QueryBotTradingHistoryDto, res: Response): Promise<void>;
    createBotTradingHistory(body: PostBotTradingHistoryDto, req: any, res: Response): Promise<void>;
    importBotTradingHistory(file: any, body: any, req: any, res: Response): Promise<void>;
    updateBot(id: string, body: PostBotTradingHistoryDto, req: any, res: Response): Promise<void>;
    deleteBot(id: string, req: any, res: Response): Promise<void>;
}
