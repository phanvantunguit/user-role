"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostBotTradingHistoryDto = exports.QueryBotTradingHistoryDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const bot_1 = require("../../../../const/bot");
class QueryBotTradingHistoryDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Page',
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], QueryBotTradingHistoryDto.prototype, "page", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Size',
        example: 50,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], QueryBotTradingHistoryDto.prototype, "size", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 'efebdafe-c359-4e88-bd17-82b2a5b1091b',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsUUID(),
    __metadata("design:type", String)
], QueryBotTradingHistoryDto.prototype, "bot_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: `${Object.keys(bot_1.TRADE_HISTORY_STATUS).join(' or ')}`,
        example: bot_1.TRADE_HISTORY_STATUS.OPEN,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], QueryBotTradingHistoryDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'id of bot trading history',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], QueryBotTradingHistoryDto.prototype, "user_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'From',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], QueryBotTradingHistoryDto.prototype, "from", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'To',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], QueryBotTradingHistoryDto.prototype, "to", void 0);
exports.QueryBotTradingHistoryDto = QueryBotTradingHistoryDto;
class PostBotTradingHistoryDto {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'efebdafe-c359-4e88-bd17-82b2a5b1091b',
    }),
    class_validator_1.IsUUID(),
    __metadata("design:type", String)
], PostBotTradingHistoryDto.prototype, "bot_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name token frist',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotTradingHistoryDto.prototype, "token_first", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Name token second',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotTradingHistoryDto.prototype, "token_second", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: `Status: ${Object.values(bot_1.TRADE_HISTORY_STATUS)}`,
        example: bot_1.TRADE_HISTORY_STATUS.OPEN,
    }),
    class_validator_1.IsString(),
    class_validator_1.IsIn(Object.values(bot_1.TRADE_HISTORY_STATUS)),
    __metadata("design:type", String)
], PostBotTradingHistoryDto.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Side',
        example: 'LONG',
    }),
    class_validator_1.IsString(),
    class_validator_1.IsIn(Object.keys(bot_1.TRADE_SIDE)),
    __metadata("design:type", String)
], PostBotTradingHistoryDto.prototype, "side", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Price entry',
        example: '0.009',
    }),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PostBotTradingHistoryDto.prototype, "entry_price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Price close',
        example: '0.009',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], PostBotTradingHistoryDto.prototype, "close_price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Profit/loss',
        example: '14',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotTradingHistoryDto.prototype, "profit", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Day start',
        example: '1676000403889',
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PostBotTradingHistoryDto.prototype, "day_started", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Day completed',
        example: '1676000403889',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], PostBotTradingHistoryDto.prototype, "day_completed", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'user of bot trading history',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PostBotTradingHistoryDto.prototype, "user_id", void 0);
exports.PostBotTradingHistoryDto = PostBotTradingHistoryDto;
//# sourceMappingURL=dto.js.map