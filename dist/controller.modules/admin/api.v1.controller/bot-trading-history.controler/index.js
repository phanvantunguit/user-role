"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminBotTradingHistoryController = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../../const");
const bot_trading_history_1 = require("../../../../services/bot-trading-history");
const admin_import_trade_history_csv_1 = require("../../../../usecases/bot/admin-import-trade-history-csv");
const admin_list_trade_history_1 = require("../../../../usecases/bot/admin-list-trade-history");
const validate_1 = require("../../../../usecases/bot/admin-list-trade-history/validate");
const response_util_1 = require("../../../../utils/response.util");
const const_2 = require("../../const");
const dto_1 = require("./dto");
let AdminBotTradingHistoryController = class AdminBotTradingHistoryController {
    constructor(botTradingHistoryService, adminImportTradeHistoryCSVHandler, adminListTradeHistoryHandler) {
        this.botTradingHistoryService = botTradingHistoryService;
        this.adminImportTradeHistoryCSVHandler = adminImportTradeHistoryCSVHandler;
        this.adminListTradeHistoryHandler = adminListTradeHistoryHandler;
    }
    async tradeHistory(query, res) {
        const result = await this.adminListTradeHistoryHandler.execute(query);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async listBot(query, res) {
        const result = await this.botTradingHistoryService.listBotTradingHistory(query);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async createBotTradingHistory(body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.botTradingHistoryService.createBot(Object.assign(Object.assign({}, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async importBotTradingHistory(file, body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.adminImportTradeHistoryCSVHandler.execute(file, body, owner_created);
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async updateBot(id, body, req, res) {
        const owner_created = req.user.user_id;
        const result = await this.botTradingHistoryService.save(Object.assign(Object.assign({ id }, body), { owner_created }));
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
    async deleteBot(id, req, res) {
        const result = await this.botTradingHistoryService.delete({
            id,
        });
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
};
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_SYSTEM_TRADE_HISTORY.permission_id),
    common_1.Get('/system-trade-history'),
    __param(0, common_1.Query()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_1.AdminListTradeHistoryInput, Object]),
    __metadata("design:returntype", Promise)
], AdminBotTradingHistoryController.prototype, "tradeHistory", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_BOT_TRADING_HISTORY.permission_id),
    common_1.Get('/list'),
    __param(0, common_1.Query()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.QueryBotTradingHistoryDto, Object]),
    __metadata("design:returntype", Promise)
], AdminBotTradingHistoryController.prototype, "listBot", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_BOT_TRADING_HISTORY.permission_id),
    common_1.Post(''),
    __param(0, common_1.Body()),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.PostBotTradingHistoryDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminBotTradingHistoryController.prototype, "createBotTradingHistory", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.CREATE_BOT_TRADING_HISTORY.permission_id),
    common_1.Post('/import-csv'),
    swagger_1.ApiConsumes('multipart/form-data'),
    swagger_1.ApiBody({
        schema: {
            type: 'object',
            properties: {
                bot_id: { type: 'string' },
                file: {
                    type: 'string',
                    format: 'binary',
                },
            },
        },
    }),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file')),
    __param(0, common_1.UploadedFile()),
    __param(1, common_1.Body()),
    __param(2, common_1.Req()),
    __param(3, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminBotTradingHistoryController.prototype, "importBotTradingHistory", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.UPDATE_BOT_TRADING_HISTORY.permission_id),
    common_1.Put('/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __param(2, common_1.Req()),
    __param(3, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, dto_1.PostBotTradingHistoryDto, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminBotTradingHistoryController.prototype, "updateBot", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.DELETE_BOT_TRADING_HISTORY.permission_id),
    common_1.Delete('/:id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Req()),
    __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminBotTradingHistoryController.prototype, "deleteBot", null);
AdminBotTradingHistoryController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiTags('admin/bot-trading-history'),
    common_1.Controller('bot-trading-history'),
    common_1.UseGuards(const_2.AdminAuthGuard),
    __param(1, common_1.Inject(admin_import_trade_history_csv_1.AdminImportTradeHistoryCSVHandler)),
    __param(2, common_1.Inject(admin_list_trade_history_1.AdminListTradeHistoryHandler)),
    __metadata("design:paramtypes", [bot_trading_history_1.BotTradingHistoryService,
        admin_import_trade_history_csv_1.AdminImportTradeHistoryCSVHandler,
        admin_list_trade_history_1.AdminListTradeHistoryHandler])
], AdminBotTradingHistoryController);
exports.AdminBotTradingHistoryController = AdminBotTradingHistoryController;
//# sourceMappingURL=index.js.map