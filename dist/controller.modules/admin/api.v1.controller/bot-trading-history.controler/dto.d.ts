import { TRADE_HISTORY_STATUS, TRADE_SIDE } from 'src/const/bot';
export declare class QueryBotTradingHistoryDto {
    page: number;
    size: number;
    bot_id: string;
    status: TRADE_HISTORY_STATUS;
    user_id: string;
    from: number;
    to: number;
}
export declare class PostBotTradingHistoryDto {
    bot_id: string;
    token_first: string;
    token_second: string;
    status: TRADE_HISTORY_STATUS;
    side: TRADE_SIDE;
    entry_price: string;
    close_price: string;
    profit: string;
    day_started: number;
    day_completed: number;
    user_id: string;
}
