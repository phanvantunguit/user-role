"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminPermissionController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const auth_role_1 = require("../../../../services/auth-role");
const response_util_1 = require("../../../../utils/response.util");
const const_1 = require("../../const");
let AdminPermissionController = class AdminPermissionController {
    constructor(authRoleService) {
        this.authRoleService = authRoleService;
    }
    getPermissions(res) {
        const result = this.authRoleService.getListPermission();
        response_util_1.resSuccess({
            payload: result,
            res,
        });
    }
};
__decorate([
    common_1.Get('/list'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AdminPermissionController.prototype, "getPermissions", null);
AdminPermissionController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiTags('admin/permission'),
    common_1.Controller('permission'),
    common_1.UseGuards(const_1.AdminAuthGuard),
    __metadata("design:paramtypes", [auth_role_1.AuthRoleService])
], AdminPermissionController);
exports.AdminPermissionController = AdminPermissionController;
//# sourceMappingURL=index.js.map