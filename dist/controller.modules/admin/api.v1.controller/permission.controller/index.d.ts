import { Response } from 'express';
import { AuthRoleService } from 'src/services/auth-role';
export declare class AdminPermissionController {
    private authRoleService;
    constructor(authRoleService: AuthRoleService);
    getPermissions(res: Response): void;
}
