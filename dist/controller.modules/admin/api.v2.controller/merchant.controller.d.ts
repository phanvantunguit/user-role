import { Response } from 'express';
import { AdminListMerchantHandler } from 'src/usecases/merchant/admin-list-merchant';
export declare class AdminMerchantV2Controller {
    private adminListMerchantHandler;
    constructor(adminListMerchantHandler: AdminListMerchantHandler);
    listTransaction(res: Response): Promise<void>;
}
