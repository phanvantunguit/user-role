"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminMerchantV2Controller = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../const");
const admin_list_merchant_1 = require("../../../usecases/merchant/admin-list-merchant");
const validate_1 = require("../../../usecases/merchant/admin-list-merchant/validate");
const response_util_1 = require("../../../utils/response.util");
const const_2 = require("../const");
let AdminMerchantV2Controller = class AdminMerchantV2Controller {
    constructor(adminListMerchantHandler) {
        this.adminListMerchantHandler = adminListMerchantHandler;
    }
    async listTransaction(res) {
        const data = await this.adminListMerchantHandler.execute();
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
};
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_TRANSACTION.permission_id),
    common_1.Get('/list'),
    swagger_1.ApiResponse({
        status: 200,
        type: class AdminListMerchantOutputMap extends swagger_1.IntersectionType(validate_1.AdminListMerchantOutput, const_2.AdminBaseApiOutput) {
        },
        description: 'List merchant',
    }),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminMerchantV2Controller.prototype, "listTransaction", null);
AdminMerchantV2Controller = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiTags('admin/merchant/v2'),
    common_1.Controller('merchant'),
    common_1.UseGuards(const_2.AdminAuthGuard),
    __param(0, common_1.Inject(admin_list_merchant_1.AdminListMerchantHandler)),
    __metadata("design:paramtypes", [admin_list_merchant_1.AdminListMerchantHandler])
], AdminMerchantV2Controller);
exports.AdminMerchantV2Controller = AdminMerchantV2Controller;
//# sourceMappingURL=merchant.controller.js.map