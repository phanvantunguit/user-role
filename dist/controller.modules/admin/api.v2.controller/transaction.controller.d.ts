import { Response } from 'express';
import { AdminGetTransactionV2Handler } from 'src/usecases/transaction/admin-get-detail-transaction-v2';
import { AdminListTransactionV2Handler } from 'src/usecases/transaction/admin-list-transaction-v2';
import { AdminListTransactionV2Input } from 'src/usecases/transaction/admin-list-transaction-v2/validate';
export declare class AdminTransactionV2Controller {
    private adminListTransactionV2Handler;
    private adminGetTransactionV2Handler;
    constructor(adminListTransactionV2Handler: AdminListTransactionV2Handler, adminGetTransactionV2Handler: AdminGetTransactionV2Handler);
    listTransaction(query: AdminListTransactionV2Input, res: Response): Promise<void>;
    getTransaction(id: string, res: Response): Promise<void>;
}
