"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminTransactionV2Controller = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../../const");
const admin_get_detail_transaction_v2_1 = require("../../../usecases/transaction/admin-get-detail-transaction-v2");
const validate_1 = require("../../../usecases/transaction/admin-get-detail-transaction-v2/validate");
const admin_list_transaction_v2_1 = require("../../../usecases/transaction/admin-list-transaction-v2");
const validate_2 = require("../../../usecases/transaction/admin-list-transaction-v2/validate");
const response_util_1 = require("../../../utils/response.util");
const const_2 = require("../const");
let AdminTransactionV2Controller = class AdminTransactionV2Controller {
    constructor(adminListTransactionV2Handler, adminGetTransactionV2Handler) {
        this.adminListTransactionV2Handler = adminListTransactionV2Handler;
        this.adminGetTransactionV2Handler = adminGetTransactionV2Handler;
    }
    async listTransaction(query, res) {
        const data = await this.adminListTransactionV2Handler.execute(query);
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
    async getTransaction(id, res) {
        const data = await this.adminGetTransactionV2Handler.execute({ id });
        response_util_1.resSuccess({
            payload: data,
            res,
        });
    }
};
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_TRANSACTION.permission_id),
    common_1.Get('/list'),
    swagger_1.ApiResponse({
        status: 200,
        type: class AdminListTransactionV2PayloadOutputMap extends swagger_1.IntersectionType(validate_2.AdminListTransactionV2PayloadOutput, const_2.AdminBaseApiOutput) {
        },
        description: 'List transaction',
    }),
    __param(0, common_1.Query()),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_2.AdminListTransactionV2Input, Object]),
    __metadata("design:returntype", Promise)
], AdminTransactionV2Controller.prototype, "listTransaction", null);
__decorate([
    const_2.AdminPermission(const_1.ADMIN_PERMISSION.GET_TRANSACTION.permission_id),
    common_1.Get('/:id'),
    swagger_1.ApiResponse({
        status: 200,
        type: class AdminGetTransactionV2PayloadOutputMap extends swagger_1.IntersectionType(validate_1.AdminGetTransactionV2PayloadOutput, const_2.AdminBaseApiOutput) {
        },
        description: 'Get detail transaction',
    }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminTransactionV2Controller.prototype, "getTransaction", null);
AdminTransactionV2Controller = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiTags('admin/transaction/v2'),
    common_1.Controller('transaction'),
    common_1.UseGuards(const_2.AdminAuthGuard),
    __param(0, common_1.Inject(admin_list_transaction_v2_1.AdminListTransactionV2Handler)),
    __param(1, common_1.Inject(admin_get_detail_transaction_v2_1.AdminGetTransactionV2Handler)),
    __metadata("design:paramtypes", [admin_list_transaction_v2_1.AdminListTransactionV2Handler,
        admin_get_detail_transaction_v2_1.AdminGetTransactionV2Handler])
], AdminTransactionV2Controller);
exports.AdminTransactionV2Controller = AdminTransactionV2Controller;
//# sourceMappingURL=transaction.controller.js.map