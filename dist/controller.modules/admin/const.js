"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminBaseApiOutput = exports.AdminAuthGuard = exports.AdminPermission = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const config_1 = require("../../config");
const const_1 = require("../../const");
const handle_error_util_1 = require("../../utils/handle-error.util");
const hash_util_1 = require("../../utils/hash.util");
const common_2 = require("@nestjs/common");
const permission_repository_1 = require("../../repositories/permission.repository");
const user_repository_1 = require("../../repositories/user.repository");
const session_1 = require("../../services/session");
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const AdminPermission = (permission) => common_2.SetMetadata('permission', permission);
exports.AdminPermission = AdminPermission;
let AdminAuthGuard = class AdminAuthGuard {
    constructor(reflector, permissionRepo, userRepo, sessionService) {
        this.reflector = reflector;
        this.permissionRepo = permissionRepo;
        this.userRepo = userRepo;
        this.sessionService = sessionService;
    }
    async canActivate(context) {
        const permission = this.reflector.get('permission', context.getHandler());
        if (permission === const_1.WITHOUT_AUTHORIZATION) {
            return true;
        }
        const request = context.switchToHttp().getRequest();
        const authHeader = request.get('Authorization');
        if (authHeader) {
            const [type, token] = authHeader.split(' ');
            if (type.toLowerCase() === 'bearer' && token) {
                let decode;
                try {
                    decode = await hash_util_1.verifyToken(token, config_1.APP_CONFIG.TOKEN_PUBLIC_KEY);
                }
                catch (error) {
                    handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNAUTHORIZED }, const_1.ERROR_CODE.TOKEN_EXPIRED));
                }
                await this.sessionService.verifySession(decode);
                if (decode.super_user || !permission) {
                    request.user = decode;
                    return true;
                }
                if (decode.user_id && decode.roles && decode.roles.length > 0) {
                    const userRoles = await this.userRepo.findAuthUserRole({
                        user_id: decode.user_id,
                    });
                    const roleIds = userRoles.map((e) => e.auth_role_id);
                    const roles = await this.permissionRepo.findAuthRoleByIds(roleIds);
                    const isValid = roles.some((r) => r.root.permissions.some((p) => p.permission_id === permission));
                    if (isValid) {
                        request.user = decode;
                        return true;
                    }
                }
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.NO_PERMISSION));
            }
        }
        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNAUTHORIZED }, const_1.ERROR_CODE.AUTHORIZATION_REQUIRED));
    }
};
AdminAuthGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [core_1.Reflector,
        permission_repository_1.PermissionRepository,
        user_repository_1.UserRepository,
        session_1.SessionService])
], AdminAuthGuard);
exports.AdminAuthGuard = AdminAuthGuard;
class AdminBaseApiOutput {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Error code of request',
        example: 'SUCCESS',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminBaseApiOutput.prototype, "error_code", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Message describe error code',
        example: 'Success',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminBaseApiOutput.prototype, "message", void 0);
exports.AdminBaseApiOutput = AdminBaseApiOutput;
//# sourceMappingURL=const.js.map