"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SymbolService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const setting_1 = require("../../domains/setting");
const setting_repository_1 = require("../../repositories/setting.repository");
const handle_error_util_1 = require("../../utils/handle-error.util");
let SymbolService = class SymbolService {
    constructor(settingRepo) {
        this.settingRepo = settingRepo;
    }
    async listSymbol() {
        const symbols = await this.settingRepo.findSymbol({});
        return symbols;
    }
    async getSymbol(params) {
        const symbols = await this.settingRepo.findSymbol(params);
        if (symbols.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.SYMBOL_NOT_FOUND));
        }
        return symbols[0];
    }
    async createSymbol(params) {
        const create = await this.settingRepo.saveSymbol(params);
        return create;
    }
    async updateSymbol(params) {
        const symbol = await this.getSymbol({ symbol: params.symbol });
        const updated = await this.settingRepo.saveSymbol([params]);
        return updated;
    }
    async deleteSymbol(symbol) {
        const deleted = await this.settingRepo.deleteSymbols(symbol);
        return deleted;
    }
};
SymbolService = __decorate([
    __param(0, common_1.Inject(setting_repository_1.SettingRepository)),
    __metadata("design:paramtypes", [setting_repository_1.SettingRepository])
], SymbolService);
exports.SymbolService = SymbolService;
//# sourceMappingURL=index.js.map