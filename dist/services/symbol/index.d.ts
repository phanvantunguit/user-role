import { CreateSymbol, QuerySymbol, RawSymbol, SymbolDomain, UpdateSymbol } from 'src/domains/setting';
import { SettingRepository } from 'src/repositories/setting.repository';
export declare class SymbolService implements SymbolDomain {
    private settingRepo;
    constructor(settingRepo: SettingRepository);
    listSymbol(): Promise<RawSymbol[]>;
    getSymbol(params: QuerySymbol): Promise<RawSymbol>;
    createSymbol(params: CreateSymbol[]): Promise<any>;
    updateSymbol(params: UpdateSymbol): Promise<any>;
    deleteSymbol(symbol: string[]): Promise<any>;
}
