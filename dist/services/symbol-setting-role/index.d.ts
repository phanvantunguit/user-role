import { PermissionRepository } from 'src/repositories/permission.repository';
import { SymbolSettingRoleDomain, SymbolSettingRoleCreate, SymbolSettingRoleUpdate, SymbolSettingRoleDelete } from 'src/domains/permission';
import { SettingRepository } from 'src/repositories/setting.repository';
import { RoleService } from '../role';
export declare class SymbolSettingRoleService implements SymbolSettingRoleDomain {
    private roleService;
    private settingRepo;
    private permissionRepo;
    constructor(roleService: RoleService, settingRepo: SettingRepository, permissionRepo: PermissionRepository);
    deleteSymbolSettingRole(params: SymbolSettingRoleDelete): Promise<any>;
    modifySymbolSettingRole(params: SymbolSettingRoleCreate): Promise<any>;
    createSymbolSettingRole(params: SymbolSettingRoleCreate): Promise<any>;
    updateSymbolSettingRole(params: SymbolSettingRoleUpdate): Promise<any>;
    checkSymbolSettingRolesNotFoundAndThrow(ids: string[]): Promise<import("../../repositories/permission.repository/symbol-setting-role.entity").SymbolSettingRoleEntity[]>;
    private getResolutionOrThrow;
    private getSymbolSettingOrThrow;
    private getExchangeSettingOrThrow;
}
