"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SymbolSettingRoleService = void 0;
const permission_repository_1 = require("../../repositories/permission.repository");
const common_1 = require("@nestjs/common");
const permission_1 = require("../../domains/permission");
const const_1 = require("../../const");
const handle_error_util_1 = require("../../utils/handle-error.util");
const setting_repository_1 = require("../../repositories/setting.repository");
const role_1 = require("../role");
let SymbolSettingRoleService = class SymbolSettingRoleService {
    constructor(roleService, settingRepo, permissionRepo) {
        this.roleService = roleService;
        this.settingRepo = settingRepo;
        this.permissionRepo = permissionRepo;
    }
    async deleteSymbolSettingRole(params) {
        const { id, role_id } = params;
        const role = await this.roleService.checkRoleNotFoundAndThrow({
            id: role_id,
        });
        await this.permissionRepo.deleteSymbolSettingRoleByIds([id]);
        const ssrIndex = role[0].root.symbol_settings_roles.findIndex((e) => e.id === id);
        if (ssrIndex > -1) {
            role[0].root.symbol_settings_roles.splice(ssrIndex, 1);
            const updateRole = await this.permissionRepo.saveRoles(role);
            return updateRole;
        }
        return role;
    }
    async modifySymbolSettingRole(params) {
        const { role_id } = params;
        await this.roleService.checkRoleNotFoundAndThrow({
            id: role_id,
        });
        const currentSSR = await this.permissionRepo.findSymbolSettingRole({
            role_id,
        });
        await Promise.all(currentSSR.map((e) => this.deleteSymbolSettingRole({ id: e.id, role_id })));
        await this.permissionRepo.saveSymbolSettingRole([params]);
        const roles = await this.permissionRepo.findRoles({ id: role_id });
        return roles[0];
    }
    async createSymbolSettingRole(params) {
        const { role_id, description, list_exchanged, list_symbol, supported_resolutions, } = params;
        const role = await this.roleService.checkRoleNotFoundAndThrow({
            id: role_id,
        });
        const [exchanges, symbols, resolutions] = await Promise.all([
            this.getExchangeSettingOrThrow(list_exchanged),
            this.getSymbolSettingOrThrow(list_symbol),
            this.getResolutionOrThrow(supported_resolutions),
        ]);
        const createSymbolSettingRole = await this.permissionRepo.saveSymbolSettingRole([params]);
        const dataSymbolSettingRole = {
            id: createSymbolSettingRole[0].id,
            description,
            exchanges,
            symbols,
            resolutions,
        };
        if (!role[0].root.symbol_settings_roles) {
            role[0].root.symbol_settings_roles = [];
        }
        role[0].root.symbol_settings_roles.push(dataSymbolSettingRole);
        const updateRole = await this.permissionRepo.saveRoles(role);
        return updateRole;
    }
    async updateSymbolSettingRole(params) {
        const { id, role_id, description, list_exchanged, list_symbol, supported_resolutions, } = params;
        const role = await this.roleService.checkRoleNotFoundAndThrow({
            id: role_id,
        });
        await this.checkSymbolSettingRolesNotFoundAndThrow([id]);
        const [exchanges, symbols, resolutions] = await Promise.all([
            this.getExchangeSettingOrThrow(list_exchanged),
            this.getSymbolSettingOrThrow(list_symbol),
            this.getResolutionOrThrow(supported_resolutions),
        ]);
        await this.permissionRepo.saveSymbolSettingRole([params]);
        const dataSymbolSettingRole = {
            id,
            description,
            exchanges,
            symbols,
            resolutions,
        };
        const ssrIndex = role[0].root.symbol_settings_roles.findIndex((e) => e.id === id);
        if (ssrIndex > -1) {
            role[0].root.symbol_settings_roles[ssrIndex] = dataSymbolSettingRole;
            const updateRole = await this.permissionRepo.saveRoles(role);
            return updateRole;
        }
        return role;
    }
    async checkSymbolSettingRolesNotFoundAndThrow(ids) {
        const roles = await this.permissionRepo.findSymbolSettingRoleByIds(ids);
        if (roles.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.SYMBOL_SETTING_ROLE_NOT_FOUND));
        }
        return roles;
    }
    async getResolutionOrThrow(ids) {
        const resolutions = await this.settingRepo.findResolutionByIds(ids);
        if (resolutions.length !== ids.length) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.RESOLUTION_NOT_FOUND));
        }
        return resolutions;
    }
    async getSymbolSettingOrThrow(symbols) {
        const listSymbols = await this.settingRepo.findSymbolBySymbols(symbols);
        if (listSymbols.length !== symbols.length) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.SYMBOL_NOT_FOUND));
        }
        return listSymbols;
    }
    async getExchangeSettingOrThrow(names) {
        const exchanges = await this.settingRepo.findExchangeByNames(names);
        if (exchanges.length !== names.length) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.EXCHANGE_NOT_FOUND));
        }
        return exchanges;
    }
};
SymbolSettingRoleService = __decorate([
    __param(1, common_1.Inject(setting_repository_1.SettingRepository)),
    __param(2, common_1.Inject(permission_repository_1.PermissionRepository)),
    __metadata("design:paramtypes", [role_1.RoleService,
        setting_repository_1.SettingRepository,
        permission_repository_1.PermissionRepository])
], SymbolSettingRoleService);
exports.SymbolSettingRoleService = SymbolSettingRoleService;
//# sourceMappingURL=index.js.map