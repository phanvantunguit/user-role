import { PermissionRepository } from 'src/repositories/permission.repository';
import { AuthRoleCreate, AuthRoleDomain, Permission, AuthRoleUpdate, RawAuthRole, QueryAuthRole, ListAuthRole } from 'src/domains/permission';
import { UserRepository } from 'src/repositories/user.repository';
export declare class AuthRoleService implements AuthRoleDomain {
    private permissionRepo;
    private userRepo;
    constructor(permissionRepo: PermissionRepository, userRepo: UserRepository);
    getAuthUserRole(params: QueryAuthRole): Promise<RawAuthRole>;
    getListAuthUserRole(params: ListAuthRole): Promise<RawAuthRole[]>;
    getListPermission(): Permission[];
    private checkRoleNameExistAndThrow;
    createAuthRole(params: AuthRoleCreate): Promise<RawAuthRole>;
    updateAuthRole(params: AuthRoleUpdate): Promise<RawAuthRole>;
    deleteAuthRole(ids: string[]): Promise<any>;
}
