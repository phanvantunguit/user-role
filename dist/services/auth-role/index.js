"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthRoleService = void 0;
const permission_repository_1 = require("../../repositories/permission.repository");
const common_1 = require("@nestjs/common");
const permission_1 = require("../../domains/permission");
const const_1 = require("../../const");
const handle_error_util_1 = require("../../utils/handle-error.util");
const user_repository_1 = require("../../repositories/user.repository");
let AuthRoleService = class AuthRoleService {
    constructor(permissionRepo, userRepo) {
        this.permissionRepo = permissionRepo;
        this.userRepo = userRepo;
    }
    async getAuthUserRole(params) {
        const authUserRoles = await this.permissionRepo.findAuthRole(params);
        return authUserRoles[0];
    }
    async getListAuthUserRole(params) {
        const { user_id } = params;
        if (user_id) {
            const userRoles = await this.userRepo.findAuthUserRole({ user_id });
            const roleIds = userRoles.map((e) => e.auth_role_id);
            return await this.permissionRepo.findAuthRoleByIds(roleIds);
        }
        return await this.permissionRepo.findAuthRole({});
    }
    getListPermission() {
        return const_1.ADMIN_PERMISSION_DATA;
    }
    async checkRoleNameExistAndThrow(params) {
        const authRoles = await this.permissionRepo.findAuthRole({
            role_name: params.role_name,
        });
        if ((authRoles.length > 0 && !params.id) ||
            (authRoles.length > 0 && params.id && params.id !== authRoles[0].id)) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ROLE_EXISTED));
        }
    }
    async createAuthRole(params) {
        const { permission_ids, role_name, owner_created, description } = params;
        await this.checkRoleNameExistAndThrow({ role_name });
        const permissions = permission_ids.map((id) => {
            return const_1.ADMIN_PERMISSION[id];
        });
        const dataCreate = {
            role_name,
            owner_created,
            root: {
                permissions,
            },
            description,
        };
        const create = await this.permissionRepo.saveAuthRoles([dataCreate]);
        return create[0];
    }
    async updateAuthRole(params) {
        const { id, permission_ids, role_name, owner_created, description } = params;
        await this.checkRoleNameExistAndThrow({ id, role_name });
        let root;
        if (permission_ids) {
            const permissions = permission_ids.map((id) => {
                return const_1.ADMIN_PERMISSION[id];
            });
            root = {
                permissions,
            };
        }
        const dataUpdate = {
            id,
            role_name,
            owner_created,
            description,
        };
        if (root) {
            dataUpdate.root = root;
        }
        const update = await this.permissionRepo.saveAuthRoles([dataUpdate]);
        return update[0];
    }
    deleteAuthRole(ids) {
        return this.permissionRepo.deleteAuthRoleByIds(ids);
    }
};
AuthRoleService = __decorate([
    __param(0, common_1.Inject(permission_repository_1.PermissionRepository)),
    __param(1, common_1.Inject(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [permission_repository_1.PermissionRepository,
        user_repository_1.UserRepository])
], AuthRoleService);
exports.AuthRoleService = AuthRoleService;
//# sourceMappingURL=index.js.map