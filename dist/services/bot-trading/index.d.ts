import { ITEM_STATUS } from 'src/const/transaction';
import { BotDomain } from 'src/domains/bot/bot.types';
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository';
import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { BotTradingEntity } from 'src/repositories/bot-trading.repository/bot-trading.entity';
import { CacheRepository } from 'src/repositories/cache.repository';
import { MerchantRepository } from 'src/repositories/merchant.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { MetaapiResource } from 'src/resources/forex';
import { MailResource } from 'src/resources/mail';
import { BaseService } from '../base';
import { EventStoreRepository } from 'src/repositories/event-store.repository';
import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository';
import { DBContext } from 'src/repositories/db-context';
export declare class BotTradingService extends BaseService<BotTradingEntity, BotTradingEntity> implements BotDomain {
    private botTradingRepository;
    private userRepository;
    private metaapiResource;
    private cacheRepository;
    private accountBalanceRepository;
    private mailResource;
    private merchantRepository;
    private eventStoreRepository;
    private dBContext;
    private botTradingHistoryRepository;
    constructor(botTradingRepository: BotTradingRepository, userRepository: UserRepository, metaapiResource: MetaapiResource, cacheRepository: CacheRepository, accountBalanceRepository: AccountBalanceRepository, mailResource: MailResource, merchantRepository: MerchantRepository, eventStoreRepository: EventStoreRepository, dBContext: DBContext, botTradingHistoryRepository: BotTradingHistoryRepository);
    listBot(params: {
        user_id?: string;
    }): Promise<any>;
    createBot(params: BotTradingEntity): Promise<BotTradingEntity>;
    updateBot(params: BotTradingEntity): Promise<BotTradingEntity>;
    updateOrderBot(params: {
        bots: {
            id: string;
            order: number;
        }[];
        owner_created: string;
    }): Promise<any[]>;
    handleBotWithoutUserId(params: {
        bot_id?: string;
    }): Promise<any>;
    connectBroker(params: {
        user_bot_id: string;
        user_id: string;
        bot_id: string;
    }): Promise<any>;
    emailBotStatus(params: {
        user_id: string;
        bot_id: string;
        status: ITEM_STATUS;
        trade?: {
            id: string;
            symbol: string;
            time: number;
            broker_account: string;
        };
        count_invalid?: number;
        stopout?: {
            balance_init: number;
            balance_current: number;
            max_drawdown: number;
            max_drawdown_change_percent: number;
            time: number;
            broker_account: string;
        };
    }): Promise<void>;
}
