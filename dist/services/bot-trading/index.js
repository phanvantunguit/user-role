"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotTradingService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const app_setting_1 = require("../../const/app-setting");
const bot_1 = require("../../const/bot");
const transaction_1 = require("../../const/transaction");
const bot_types_1 = require("../../domains/bot/bot.types");
const account_balance_repository_1 = require("../../repositories/account-balance.repository");
const bot_trading_repository_1 = require("../../repositories/bot-trading.repository");
const bot_trading_entity_1 = require("../../repositories/bot-trading.repository/bot-trading.entity");
const cache_repository_1 = require("../../repositories/cache.repository");
const merchant_repository_1 = require("../../repositories/merchant.repository");
const user_repository_1 = require("../../repositories/user.repository");
const forex_1 = require("../../resources/forex");
const mail_1 = require("../../resources/mail");
const handle_error_util_1 = require("../../utils/handle-error.util");
const base_1 = require("../base");
const date_fns_tz_1 = require("date-fns-tz");
const event_store_repository_1 = require("../../repositories/event-store.repository");
const bot_trading_history_repository_1 = require("../../repositories/bot-trading-history.repository");
const config_1 = require("../../config");
const db_context_1 = require("../../repositories/db-context");
let BotTradingService = class BotTradingService extends base_1.BaseService {
    constructor(botTradingRepository, userRepository, metaapiResource, cacheRepository, accountBalanceRepository, mailResource, merchantRepository, eventStoreRepository, dBContext, botTradingHistoryRepository) {
        super(botTradingRepository);
        this.botTradingRepository = botTradingRepository;
        this.userRepository = userRepository;
        this.metaapiResource = metaapiResource;
        this.cacheRepository = cacheRepository;
        this.accountBalanceRepository = accountBalanceRepository;
        this.mailResource = mailResource;
        this.merchantRepository = merchantRepository;
        this.eventStoreRepository = eventStoreRepository;
        this.dBContext = dBContext;
        this.botTradingHistoryRepository = botTradingHistoryRepository;
    }
    async listBot(params) {
        const { user_id } = params;
        if (user_id) {
            const userBots = await this.userRepository.findUserBot({ user_id });
            const botIds = userBots.map((e) => e.bot_id);
            if (botIds.length === 0) {
                return [];
            }
            return await this.botTradingRepository.findByIds(botIds);
        }
        return await this.list({});
    }
    async createBot(params) {
        if (Number(params.max_drawdown) <= Number(params.max_drawdown_change_percent)) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.MAX_DRAWDOWN_INVALID));
        }
        if (params.code) {
            this.cacheRepository.publisher.publish(app_setting_1.CHANELS.TBOT_CHANEL_UR, JSON.stringify({
                event: bot_1.TBOT_CHANEL_TS_EVENT.add_strategy,
                data: {
                    bot_code: params.code,
                },
            }));
        }
        return this.save(Object.assign({}, params));
    }
    async updateBot(params) {
        if (Number(params.max_drawdown) <= Number(params.max_drawdown_change_percent)) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.MAX_DRAWDOWN_INVALID));
        }
        if (params.code) {
            this.cacheRepository.publisher.publish(app_setting_1.CHANELS.TBOT_CHANEL_UR, JSON.stringify({
                event: bot_1.TBOT_CHANEL_TS_EVENT.add_strategy,
                data: {
                    bot_code: params.code,
                },
            }));
        }
        return this.save(Object.assign({}, params));
    }
    async updateOrderBot(params) {
        const { bots, owner_created } = params;
        const dataUpdate = bots.map((e) => {
            return Object.assign(Object.assign({}, e), { owner_created });
        });
        return this.botTradingRepository.saves(dataUpdate);
    }
    async handleBotWithoutUserId(params) {
        const { bot_id } = params;
        const resultFindUserId = await this.userRepository.findUserBotTrading({
            bot_id,
        });
        if (resultFindUserId.length > 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.USERS_HAVE_BOTS));
        }
        else {
            return await this.botTradingRepository.deleteById(bot_id);
        }
    }
    async connectBroker(params) {
        var _a, _b, _c;
        const { user_bot_id, user_id, bot_id } = params;
        try {
            await this.eventStoreRepository.save({
                event_id: user_bot_id,
                event_name: app_setting_1.EVENT_STORE_NAME.TBOT_CONNECTING,
                state: app_setting_1.EVENT_STORE_STATE.OPEN,
                user_id: user_id,
            });
        }
        catch (error) {
            return;
        }
        const [eventStopInactive, userBot, user, bot] = await Promise.all([
            this.eventStoreRepository.findOne({
                event_id: `${user_id}_${bot_id}`,
                state: app_setting_1.EVENT_STORE_STATE.OPEN,
                event_name: app_setting_1.EVENT_STORE_NAME.TBOT_STOP_INACTIVE_BY_SYSTEM,
            }),
            this.userRepository.findUserBotTrading({
                user_id,
                bot_id,
            }),
            this.userRepository.findOneUser({
                id: user_id,
            }),
            this.get({ id: bot_id }),
        ]);
        try {
            if (!userBot[0] || !user || !bot) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_INVALID));
            }
            if (((_a = userBot[0]) === null || _a === void 0 ? void 0 : _a.status) !== transaction_1.ITEM_STATUS.CONNECTING ||
                !((_b = userBot[0]) === null || _b === void 0 ? void 0 : _b.subscriber_id)) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.CONNECT_ACCOUNT_FAILED));
            }
            const subscriberId = userBot[0].subscriber_id;
            const account = await this.metaapiResource.getAccount({
                account_id: userBot[0].subscriber_id,
            });
            if (!account ||
                account.state !== 'DEPLOYED' ||
                account.connectionStatus !== 'CONNECTED') {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ACCOUNT_NOT_CONNECTED));
                return;
            }
            const connection = await account.getRPCConnection();
            await connection.connect();
            await connection.waitSynchronized();
            const positions = await connection.getPositions();
            if (positions.length > 0) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.POSITION_NOT_EMPTY));
            }
            const accountInfo = await connection.getAccountInformation();
            let initBalanceAmount = Number(accountInfo.balance);
            if (userBot[0].balance && Number(userBot[0].balance) < initBalanceAmount) {
                initBalanceAmount = Number(userBot[0].balance);
            }
            const startTime = new Date();
            let connectedTime = new Date();
            const botBalanceAmount = Number(bot.balance);
            const accountBalanceInit = await this.accountBalanceRepository.findOne({
                user_id,
                bot_id,
                change_id: 'init',
            });
            let isStopOut;
            try {
                const stopouts = await this.metaapiResource.copyFactory.tradingApi.getStopouts(subscriberId);
                isStopOut = stopouts.find((e) => e.strategy.id === bot.code);
            }
            catch (error) {
                console.log('getStopouts error', error);
            }
            if (eventStopInactive && !isStopOut) {
                connectedTime = userBot[0].connected_at
                    ? new Date(Number(userBot[0].connected_at))
                    : new Date(Number(userBot[0].updated_at));
            }
            if (botBalanceAmount > Number(accountInfo.balance)) {
                const [trade, accountBalance] = await Promise.all([
                    this.botTradingHistoryRepository.findOne({
                        broker_account: `${accountInfo.login}`,
                    }),
                    this.accountBalanceRepository.findOne({
                        user_id,
                        broker_account: `${accountInfo.login}`,
                    }),
                ]);
                if (!accountBalance ||
                    accountBalanceInit.broker_account !== `${accountInfo.login}` ||
                    ((accountBalanceInit === null || accountBalanceInit === void 0 ? void 0 : accountBalanceInit.balance) < botBalanceAmount && !trade)) {
                    try {
                        try {
                            await this.metaapiResource.removeSubscriber({
                                subscriber_id: userBot[0].subscriber_id,
                            });
                        }
                        catch (error) {
                            console.log('removeSubscriber error', error);
                        }
                        await this.metaapiResource.removeAccount({
                            subscriber_id: userBot[0].subscriber_id,
                        });
                        await this.accountBalanceRepository.delete({
                            broker_account: `${accountInfo.login}`,
                        });
                        const logConnect = await this.userRepository.findUserAssetLog({
                            status: transaction_1.ITEM_STATUS.NOT_CONNECT,
                            asset_id: bot_id,
                        });
                        if ((_c = logConnect[0]) === null || _c === void 0 ? void 0 : _c.order_id) {
                            const updateUserBot = {
                                id: user_bot_id,
                                broker: null,
                                broker_server: null,
                                broker_account: null,
                                subscriber_id: null,
                                status: transaction_1.ITEM_STATUS.NOT_CONNECT,
                            };
                            await this.userRepository.saveUserBotTrading([updateUserBot]);
                        }
                        else {
                            await this.userRepository.deleteUserBotTrading({
                                user_id,
                                bot_id,
                            });
                        }
                    }
                    catch (error) { }
                    this.cacheRepository.publisher.publish(app_setting_1.CHANELS.TBOT_CHANEL_TS, JSON.stringify({
                        event: bot_1.TBOT_CHANEL_TS_EVENT.account,
                        data: {
                            status: transaction_1.ITEM_STATUS.NOT_CONNECT,
                            user_id,
                            bot_id,
                            error: const_1.ERROR_CODE.ACCOUNT_NOT_ENOUGH_BALANCE,
                        },
                    }));
                    return;
                }
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ACCOUNT_NOT_ENOUGH_BALANCE));
            }
            const multiplier = Math.floor(initBalanceAmount / botBalanceAmount);
            const maxAbsoluteRisk = (Number(bot.max_drawdown) - Number(bot.max_drawdown_change_percent)) *
                multiplier *
                botBalanceAmount;
            return await this.dBContext.runInTransaction(async (queryRunner) => {
                const updateUserBot = {
                    id: userBot[0].id,
                    status: transaction_1.ITEM_STATUS.ACTIVE,
                    connected_at: connectedTime.valueOf(),
                };
                const dataLog = {
                    asset_id: bot.id,
                    user_id: user_id,
                    category: transaction_1.ORDER_CATEGORY.TBOT,
                    status: updateUserBot.status,
                    owner_created: user_id,
                    name: bot.name,
                    metadata: {
                        balance: initBalanceAmount,
                        broker: userBot[0].broker,
                        broker_account: userBot[0].broker_account,
                        broker_server: userBot[0].broker_server,
                        subscriber_id: userBot[0].subscriber_id,
                        expires_at: userBot[0].expires_at,
                        bot_name: bot.name
                    }
                };
                await Promise.all([
                    this.userRepository.saveUserBotTrading([updateUserBot], queryRunner),
                    this.userRepository.saveUserAssetLog([dataLog], queryRunner),
                    this.eventStoreRepository.delete({
                        event_id: `${userBot[0].user_id}_${userBot[0].bot_id}`,
                        state: app_setting_1.EVENT_STORE_STATE.OPEN,
                        event_name: app_setting_1.EVENT_STORE_NAME.TBOT_STOP_INACTIVE_BY_SYSTEM,
                    }),
                ]);
                if (!eventStopInactive) {
                    const dataInitBalance = {
                        user_id,
                        bot_id,
                        broker_account: `${accountInfo.login}`,
                        balance: accountInfo.balance,
                        change_by_user: true,
                        change_id: 'init',
                        created_at: updateUserBot.connected_at,
                    };
                    if (accountBalanceInit) {
                        dataInitBalance['id'] = accountBalanceInit.id;
                    }
                    await Promise.all([
                        this.accountBalanceRepository.save(dataInitBalance, queryRunner),
                        this.eventStoreRepository.save({
                            event_id: `${updateUserBot.connected_at}_${userBot[0].subscriber_id}`,
                            event_name: app_setting_1.EVENT_STORE_NAME.TBOT_CONNECTED,
                            state: app_setting_1.EVENT_STORE_STATE.OPEN,
                            user_id,
                            metadata: {
                                account_id: userBot[0].subscriber_id,
                                user_id: userBot[0].user_id,
                                connected_at: updateUserBot.connected_at,
                            },
                        }, queryRunner),
                    ]);
                }
                await this.metaapiResource.updateSubscriber({
                    subscriber_id: userBot[0].subscriber_id,
                    name: `${user.merchant_code}_${userBot[0].broker_account}_${user_id}`,
                    strategy_id: bot.code,
                    multiplier: multiplier,
                    max_absolute_risk: Math.ceil(maxAbsoluteRisk),
                    start_time: startTime,
                    broker: userBot[0].broker,
                });
                if (isStopOut) {
                    await this.metaapiResource.copyFactory.tradingApi.resetStopouts(subscriberId, bot.code, 'lifetime-balance-minus-equity');
                }
                this.cacheRepository.publisher.publish(app_setting_1.CHANELS.TBOT_CHANEL_TS, JSON.stringify({
                    event: bot_1.TBOT_CHANEL_TS_EVENT.account,
                    data: {
                        user_id,
                        bot_id,
                        balance: accountInfo.balance,
                        status: updateUserBot.status,
                        user_bot_id: updateUserBot.id,
                    },
                }));
                if (!eventStopInactive && !isStopOut) {
                    this.cacheRepository.publisher.publish(app_setting_1.CHANELS.TBOT_CHANEL_UR, JSON.stringify({
                        event: bot_1.TBOT_CHANEL_TS_EVENT.add_subscriber,
                        data: {
                            subscriber_id: userBot[0].subscriber_id,
                            user_id,
                        },
                    }));
                }
                console.log('connectBroker finish');
                return true;
            });
        }
        catch (error) {
            console.log('connectBroker error1:', error);
            try {
                const updateUserBot = {
                    id: user_bot_id,
                    status: eventStopInactive
                        ? transaction_1.ITEM_STATUS.INACTIVE_BY_SYSTEM
                        : transaction_1.ITEM_STATUS.INACTIVE,
                };
                await this.userRepository.saveUserBotTrading([updateUserBot]);
                const dataLog = {
                    asset_id: bot_id,
                    user_id: user_id,
                    category: transaction_1.ORDER_CATEGORY.TBOT,
                    status: updateUserBot.status,
                    owner_created: user_id,
                    name: bot.name,
                    metadata: {
                        event: bot_1.BOT_TRADING_EVENT.CONNECTING,
                        error,
                    },
                };
                await this.userRepository.saveUserAssetLog([dataLog]);
                this.cacheRepository.publisher.publish(app_setting_1.CHANELS.TBOT_CHANEL_TS, JSON.stringify({
                    event: bot_1.TBOT_CHANEL_TS_EVENT.account,
                    data: {
                        status: updateUserBot.status,
                        user_id,
                        bot_id,
                        error,
                    },
                }));
            }
            catch (error) {
                console.log('connectBroker error2:', error);
            }
        }
        finally {
            await this.eventStoreRepository.delete({ event_id: user_bot_id, event_name: app_setting_1.EVENT_STORE_NAME.TBOT_CONNECTING, state: app_setting_1.EVENT_STORE_STATE.OPEN });
        }
    }
    async emailBotStatus(params) {
        const { user_id, bot_id, status, trade, stopout, count_invalid } = params;
        const [user, bot] = await Promise.all([
            this.userRepository.findOneUser({
                id: user_id,
            }),
            this.botTradingRepository.findById(bot_id),
        ]);
        if (!user || !bot) {
            return;
        }
        const merchant = await this.merchantRepository.findOne({
            code: user.merchant_code,
        });
        let subject;
        let title;
        let from_email;
        let from_name;
        let logo_url;
        let header_url;
        let main_content = [];
        let twitter_url;
        let youtube_url;
        let facebook_url;
        let telegram_url;
        let company_name;
        if (merchant && merchant.config) {
            if (merchant.config['verified_sender'] &&
                merchant.config['email_sender']) {
                from_email = merchant.config['email_sender']['from_email'];
                from_name = merchant.config['email_sender']['from_name'];
            }
            if (merchant.config['social_media']) {
                twitter_url = merchant.config['social_media']['twitter_url'];
                youtube_url = merchant.config['social_media']['youtube_url'];
                facebook_url = merchant.config['social_media']['facebook_url'];
                telegram_url = merchant.config['social_media']['telegram_url'];
            }
            logo_url = merchant.config['email_logo_url'];
            header_url = merchant.config['email_banner_url'];
            company_name = merchant.name;
            from_email = from_email || config_1.APP_CONFIG.SENDGRID_SENDER_ALGO_EMAIL;
            from_name = from_name || config_1.APP_CONFIG.SENDGRID_SENDER_ALGO_NAME;
            logo_url = logo_url || config_1.APP_CONFIG.ALGO_LOGO_EMAIL;
            header_url = header_url || config_1.APP_CONFIG.ALGO_BANNER_EMAIL;
        }
        switch (status) {
            case transaction_1.ITEM_STATUS.INACTIVE_BY_SYSTEM:
                if (!trade) {
                    return;
                }
                subject = 'Vi Phạm Trading Bot';
                title = 'Vi Phạm Trading Bot';
                main_content.push(title);
                main_content.push('Bạn đã vi phạm chính sách tham gia. Chúng tôi đã phát hiện bạn có dấu hiệu vào lệnh bằng tay.');
                main_content.push(`Tên bot: ${bot.name}`);
                main_content.push(`Lần vi phạm thứ: ${count_invalid}`);
                main_content.push(`Tài khoản MT4: ${trade.broker_account}`);
                main_content.push(`Thời gian vào lệnh: ${date_fns_tz_1.formatInTimeZone(Number(trade.time), 'Asia/Ho_Chi_Minh', 'HH:mm dd/MM/yyyy')} (GMT+7)`);
                main_content.push(`Mã lệnh: ${trade.id}`);
                main_content.push(`Symbol: ${trade.symbol}`);
                main_content.push(`Quy định xử lý:`);
                main_content.push(`-  Lần 1: Tạm dừng trade.`);
                main_content.push(`-  Lần 2: Tạm dừng trade. Được phép kích hoạt lại sau 3 ngày.`);
                main_content.push(`-  Lần 3: Xoá Bot Trading.`);
                break;
            case transaction_1.ITEM_STATUS.EXPIRED:
                subject = 'Trading Bot Đã Hết Hạn';
                title = 'Trading Bot Đã Hết Hạn';
                main_content.push(`Bot của bạn đã hết hạn.`);
                main_content.push(`Tên bot: ${bot.name}`);
                break;
            case transaction_1.ITEM_STATUS.STOP_OUT:
                if (!stopout) {
                    return;
                }
                subject = 'Trading Bot Của Bạn Đã Bị Chạm Mốc Max Drawdown';
                title = 'Trading Bot của bạn đã bị chạm mốc Max Drawdown';
                main_content.push(title);
                main_content.push(`Bot Trading của bạn sẽ tạm dừng trong thời gian này, vui lòng kích hoạt lại Bot Trading để tiếp trade và đảm bảo tiền lớn hơn số dư tối thiểu.`);
                main_content.push(`Tên bot: ${bot.name}`);
                main_content.push(`Tài khoản MT4: ${stopout.broker_account}`);
                main_content.push(`Thời gian tạm dừng: ${date_fns_tz_1.formatInTimeZone(Number(stopout.time), 'Asia/Ho_Chi_Minh', 'HH:mm dd/MM/yyyy')} (GMT+7)`);
                break;
        }
        await this.mailResource.sendEmailStandard(subject, title, user.email, main_content, from_email, from_name, logo_url, header_url, twitter_url, youtube_url, facebook_url, telegram_url, company_name, `${user.first_name || ''} ${user.last_name || ''}`, config_1.APP_CONFIG.SENDGRID_CC_EMAIL);
    }
};
BotTradingService = __decorate([
    __param(0, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(1, common_1.Inject(user_repository_1.UserRepository)),
    __param(2, common_1.Inject(forex_1.MetaapiResource)),
    __param(3, common_1.Inject(cache_repository_1.CacheRepository)),
    __param(4, common_1.Inject(account_balance_repository_1.AccountBalanceRepository)),
    __param(5, common_1.Inject(mail_1.MailResource)),
    __param(6, common_1.Inject(merchant_repository_1.MerchantRepository)),
    __param(7, common_1.Inject(event_store_repository_1.EventStoreRepository)),
    __param(8, common_1.Inject(db_context_1.DBContext)),
    __param(9, common_1.Inject(bot_trading_history_repository_1.BotTradingHistoryRepository)),
    __metadata("design:paramtypes", [bot_trading_repository_1.BotTradingRepository,
        user_repository_1.UserRepository,
        forex_1.MetaapiResource,
        cache_repository_1.CacheRepository,
        account_balance_repository_1.AccountBalanceRepository,
        mail_1.MailResource,
        merchant_repository_1.MerchantRepository,
        event_store_repository_1.EventStoreRepository,
        db_context_1.DBContext,
        bot_trading_history_repository_1.BotTradingHistoryRepository])
], BotTradingService);
exports.BotTradingService = BotTradingService;
//# sourceMappingURL=index.js.map