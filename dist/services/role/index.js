"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const transaction_1 = require("../../const/transaction");
const permission_1 = require("../../domains/permission");
const user_1 = require("../../domains/user");
const permission_repository_1 = require("../../repositories/permission.repository");
const transaction_repository_1 = require("../../repositories/transaction.repository");
const user_repository_1 = require("../../repositories/user.repository");
const handle_error_util_1 = require("../../utils/handle-error.util");
let RoleService = class RoleService {
    constructor(permissionRepo, userRepo, transactionRepository) {
        this.permissionRepo = permissionRepo;
        this.userRepo = userRepo;
        this.transactionRepository = transactionRepository;
    }
    async updateOrderRole(params) {
        const { roles, owner_created } = params;
        const dataUpdate = roles.map((e) => {
            return Object.assign(Object.assign({}, e), { owner_created });
        });
        return this.permissionRepo.saveRoles(dataUpdate);
    }
    checkRoleDisable(parentId, roles) {
        if (!parentId) {
            return false;
        }
        const roleParent = roles.find((ur) => ur.id === parentId);
        if (!roleParent) {
            return false;
        }
        if (!roleParent.expires_at || Number(roleParent.expires_at) > Date.now()) {
            return true;
        }
        return this.checkRoleDisable(roleParent.parent_id, roles);
    }
    async listRoleForUser(user_id, package_id) {
        const rolesForUser = [];
        const [roles, user, trans] = await Promise.all([
            this.permissionRepo.findRoles({}),
            this.userRepo.findOneUserView({ id: user_id }),
            this.transactionRepository.viewFindByUserMultipleStatus(user_id, [
                transaction_1.TRANSACTION_STATUS.PROCESSING,
                transaction_1.TRANSACTION_STATUS.CREATED,
            ]),
        ]);
        roles
            .forEach((r) => {
            var _a, _b;
            if (package_id) {
                if (r.status === const_1.ROLE_STATUS.CLOSE) {
                    return;
                }
            }
            let userStatus = '';
            const roleUser = user.roles.find((ur) => ur.id === r.id);
            if (roleUser) {
                const description_features = ((_a = roleUser.description_features) === null || _a === void 0 ? void 0 : _a.features) || [];
                userStatus = const_1.USER_ROLE_STATUS.CURRENT_PLAN;
                if (!roleUser.expires_at ||
                    Number(roleUser.expires_at) > Date.now()) {
                    userStatus = const_1.USER_ROLE_STATUS.CURRENT_PLAN;
                    rolesForUser.push(Object.assign(Object.assign({}, roleUser), { user_status: userStatus, description_features }));
                    return;
                }
            }
            if (r.status === const_1.ROLE_STATUS.CLOSE) {
                return;
            }
            const description_features = ((_b = r.description_features) === null || _b === void 0 ? void 0 : _b.features) || [];
            r.description_features = description_features;
            const isDisable = this.checkRoleDisable(r.parent_id, user.roles);
            if (isDisable) {
                userStatus = const_1.USER_ROLE_STATUS.DISABLED;
                rolesForUser.push(Object.assign(Object.assign({}, r), { user_status: userStatus }));
                return;
            }
            let checkProcessing = false;
            for (const t of trans) {
                if (t.details.length > 0 && t.details[0].role_id === r.id) {
                    if (package_id) {
                        if (package_id === t.details[0].package_id) {
                            checkProcessing = true;
                            rolesForUser.push(Object.assign(Object.assign({}, r), { user_status: const_1.USER_ROLE_STATUS.PROCESSING, transaction_id: t.id }));
                            return;
                        }
                    }
                    else {
                        checkProcessing = true;
                        rolesForUser.push(Object.assign(Object.assign({}, r), { user_status: const_1.USER_ROLE_STATUS.PROCESSING, transaction_id: t.id }));
                    }
                }
            }
            if (!checkProcessing) {
                if (userStatus === const_1.USER_ROLE_STATUS.CURRENT_PLAN) {
                    const price = Number(roleUser.price) || 0;
                    if (price === 0 && package_id) {
                        return;
                    }
                    rolesForUser.push(Object.assign(Object.assign({}, roleUser), { user_status: userStatus, description_features }));
                }
                else {
                    rolesForUser.push(r);
                }
            }
        });
        return rolesForUser;
    }
    async listRole(params) {
        const { user_id } = params;
        if (user_id) {
            const userRoles = await this.userRepo.findUserRole({ user_id });
            const roleIds = userRoles.map((e) => e.role_id);
            if (roleIds.length === 0) {
                return [];
            }
            return await this.permissionRepo.findRoleByIds(roleIds);
        }
        return await this.permissionRepo.findRoles({});
    }
    async getRoleDetail(id) {
        const roles = await this.permissionRepo.findRoles({ id });
        if (roles.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.ROLE_NOT_FOUND));
        }
        return roles[0];
    }
    async createRole(params) {
        await this.checkRoleExistAndThrow({ role_name: params.role_name });
        const dataCreate = Object.assign(Object.assign({}, params), { description_features: {
                features: params.description_features,
            } });
        dataCreate.parent_id = dataCreate.parent_id ? dataCreate.parent_id : null;
        const create = await this.permissionRepo.saveRoles([dataCreate]);
        return create;
    }
    async updateRole(params) {
        await this.checkRoleNotFoundAndThrow({ id: params.id });
        const dataUpdate = Object.assign(Object.assign({}, params), { description_features: {
                features: params.description_features,
            } });
        if (!params.description_features) {
            delete dataUpdate.description_features;
        }
        if (!params.parent_id) {
            delete dataUpdate.parent_id;
        }
        const updated = await this.permissionRepo.saveRoles([dataUpdate]);
        return updated;
    }
    async deleteRole(id) {
        if (id === const_1.DEFAULT_ROLE.id) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.NO_PERMISSION));
        }
        const resultFindUserId = await this.userRepo.findUserRole({
            role_id: id,
        });
        if (resultFindUserId.length > 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.USERS_HAVE_ROLES));
        }
        const deleted = await this.permissionRepo.deleteRoleByIds([id]);
        return deleted;
    }
    async checkRoleNotFoundAndThrow(params) {
        const roles = await this.permissionRepo.findRoles(params);
        if (roles.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.ROLE_NOT_FOUND));
        }
        return roles;
    }
    async checkRoleExistAndThrow(params) {
        const roles = await this.permissionRepo.findRoles(params);
        if (roles.length > 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ROLE_EXISTED));
        }
    }
};
RoleService = __decorate([
    __param(0, common_1.Inject(permission_repository_1.PermissionRepository)),
    __param(1, common_1.Inject(user_repository_1.UserRepository)),
    __param(2, common_1.Inject(transaction_repository_1.TransactionRepository)),
    __metadata("design:paramtypes", [permission_repository_1.PermissionRepository,
        user_repository_1.UserRepository,
        transaction_repository_1.TransactionRepository])
], RoleService);
exports.RoleService = RoleService;
//# sourceMappingURL=index.js.map