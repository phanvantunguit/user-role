import { ListRole, QueryRole, RoleCreate, RoleDomain, RoleOrderUpdate, RoleUpdate } from 'src/domains/permission';
import { RawRoleView } from 'src/domains/user';
import { PermissionRepository } from 'src/repositories/permission.repository';
import { TransactionRepository } from 'src/repositories/transaction.repository';
import { UserRepository } from 'src/repositories/user.repository';
export declare class RoleService implements RoleDomain {
    private permissionRepo;
    private userRepo;
    private transactionRepository;
    constructor(permissionRepo: PermissionRepository, userRepo: UserRepository, transactionRepository: TransactionRepository);
    updateOrderRole(params: RoleOrderUpdate): Promise<any[]>;
    checkRoleDisable(parentId: string, roles: RawRoleView[]): any;
    listRoleForUser(user_id: string, package_id?: string): Promise<any>;
    listRole(params: ListRole): Promise<any>;
    getRoleDetail(id: string): Promise<any>;
    createRole(params: RoleCreate): Promise<any>;
    updateRole(params: RoleUpdate): Promise<any>;
    deleteRole(id: string): Promise<any>;
    checkRoleNotFoundAndThrow(params: QueryRole): Promise<import("../../repositories/permission.repository/role-view.entity").RoleViewEntity[]>;
    private checkRoleExistAndThrow;
}
