"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrencyService = void 0;
const common_1 = require("@nestjs/common");
const transaction_1 = require("../../const/transaction");
const currency_types_1 = require("../../domains/setting/currency.types");
const currency_repostitory_1 = require("../../repositories/currency.repostitory");
const currency_entity_1 = require("../../repositories/currency.repostitory/currency.entity");
const setting_repository_1 = require("../../repositories/setting.repository");
const handle_error_util_1 = require("../../utils/handle-error.util");
const base_1 = require("../base");
let CurrencyService = class CurrencyService extends base_1.BaseService {
    constructor(currencyRepository, settingRepository) {
        super(currencyRepository);
        this.currencyRepository = currencyRepository;
        this.settingRepository = settingRepository;
    }
    async updateOrderCurrency(params) {
        const { currencies, owner_created } = params;
        const dataUpdate = currencies.map((e) => {
            return Object.assign(Object.assign({}, e), { owner_created });
        });
        return this.currencyRepository.saves(dataUpdate);
    }
    async findCurrencies(params) {
        var _a;
        const { category } = params;
        const keyName = `CURRENCY_${category}`;
        const setting = await this.settingRepository.findOneAppSetting({
            name: keyName,
        });
        if (!((_a = setting === null || setting === void 0 ? void 0 : setting.value) === null || _a === void 0 ? void 0 : _a.trim())) {
            return [];
        }
        const ids = setting.value.split(',').filter(e => e.trim());
        return this.currencyRepository.find({ status: true, ids });
    }
};
CurrencyService = __decorate([
    __param(0, common_1.Inject(currency_repostitory_1.CurrencyRepository)),
    __param(1, common_1.Inject(setting_repository_1.SettingRepository)),
    __metadata("design:paramtypes", [currency_repostitory_1.CurrencyRepository,
        setting_repository_1.SettingRepository])
], CurrencyService);
exports.CurrencyService = CurrencyService;
//# sourceMappingURL=index.js.map