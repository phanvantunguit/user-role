import { ORDER_CATEGORY } from 'src/const/transaction';
import { CurrencyDomain, CurrencyOrderUpdate, QueryCurrency, RawCurrency } from 'src/domains/setting/currency.types';
import { CurrencyRepository } from 'src/repositories/currency.repostitory';
import { SettingRepository } from 'src/repositories/setting.repository';
import { BaseService } from '../base';
export declare class CurrencyService extends BaseService<QueryCurrency, RawCurrency> implements CurrencyDomain {
    private currencyRepository;
    private settingRepository;
    constructor(currencyRepository: CurrencyRepository, settingRepository: SettingRepository);
    updateOrderCurrency(params: CurrencyOrderUpdate): Promise<any[]>;
    findCurrencies(params: {
        category: ORDER_CATEGORY;
    }): Promise<RawCurrency[]>;
}
