import { QueryBotTradingHistoryDto } from 'src/controller.modules/admin/api.v1.controller/bot-trading-history.controler/dto';
import { BotDomain } from 'src/domains/bot/bot.types';
import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository';
import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { BaseService } from '../base';
export declare class BotTradingHistoryService extends BaseService<any, any> implements BotDomain {
    private botTradingHistoryRepository;
    private botTradingRepository;
    constructor(botTradingHistoryRepository: BotTradingHistoryRepository, botTradingRepository: BotTradingRepository);
    listBotTradingHistory(params: QueryBotTradingHistoryDto): Promise<any>;
    createBot(params: any): Promise<any>;
}
