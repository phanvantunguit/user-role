"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotTradingHistoryService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const transaction_1 = require("../../const/transaction");
const dto_1 = require("../../controller.modules/admin/api.v1.controller/bot-trading-history.controler/dto");
const bot_types_1 = require("../../domains/bot/bot.types");
const bot_trading_history_repository_1 = require("../../repositories/bot-trading-history.repository");
const bot_trading_repository_1 = require("../../repositories/bot-trading.repository");
const user_repository_1 = require("../../repositories/user.repository");
const forex_1 = require("../../resources/forex");
const handle_error_util_1 = require("../../utils/handle-error.util");
const base_1 = require("../base");
let BotTradingHistoryService = class BotTradingHistoryService extends base_1.BaseService {
    constructor(botTradingHistoryRepository, botTradingRepository) {
        super(botTradingHistoryRepository);
        this.botTradingHistoryRepository = botTradingHistoryRepository;
        this.botTradingRepository = botTradingRepository;
    }
    async listBotTradingHistory(params) {
        const botTrading = await this.botTradingRepository.findById(params.bot_id);
        delete params.bot_id;
        const trades = await this.botTradingHistoryRepository.getPaging(Object.assign(Object.assign({}, params), { bot_code: botTrading.code }));
        return trades;
    }
    async createBot(params) {
        return this.save(Object.assign({}, params));
    }
};
BotTradingHistoryService = __decorate([
    __param(0, common_1.Inject(bot_trading_history_repository_1.BotTradingHistoryRepository)),
    __param(1, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __metadata("design:paramtypes", [bot_trading_history_repository_1.BotTradingHistoryRepository,
        bot_trading_repository_1.BotTradingRepository])
], BotTradingHistoryService);
exports.BotTradingHistoryService = BotTradingHistoryService;
//# sourceMappingURL=index.js.map