"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRoleService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const transaction_1 = require("../../const/transaction");
const user_1 = require("../../domains/user");
const permission_repository_1 = require("../../repositories/permission.repository");
const user_repository_1 = require("../../repositories/user.repository");
const handle_error_util_1 = require("../../utils/handle-error.util");
let UserRoleService = class UserRoleService {
    constructor(userRepo, permissionRepo) {
        this.userRepo = userRepo;
        this.permissionRepo = permissionRepo;
    }
    async modifyUserRole(params) {
        const { user_id, owner_created, roles } = params;
        const roleIds = roles.map((r) => r.role_id);
        const dataRoles = await this.permissionRepo.findRoleByIds(roleIds);
        const updateUserAssetLogs = [];
        if (dataRoles.length !== roleIds.length) {
            const roleIdsNotFound = [];
            for (const dr of dataRoles) {
                if (!roleIds.includes(dr.id)) {
                    roleIdsNotFound.push(dr.id);
                }
            }
            handle_error_util_1.throwError({
                status: common_1.HttpStatus.NOT_FOUND,
                error_code: const_1.ERROR_CODE.ROLE_NOT_FOUND.error_code,
                message: roleIdsNotFound,
            });
        }
        const currentUserRoles = await this.userRepo.findUserRole({ user_id });
        const dataModify = [];
        for (const rReq of roles) {
            const urExistIndex = currentUserRoles.findIndex((ur) => ur.role_id === rReq.role_id);
            const dataRole = dataRoles.find((r) => r.id === rReq.role_id);
            updateUserAssetLogs.push({
                asset_id: rReq.role_id,
                user_id: user_id,
                category: transaction_1.ORDER_CATEGORY.PKG,
                status: transaction_1.ITEM_STATUS.ACTIVE,
                owner_created,
                expires_at: rReq.expires_at,
                name: dataRole.role_name,
                quantity: rReq.quantity,
                package_type: rReq.package_type,
            });
            if (urExistIndex > -1) {
                if (currentUserRoles[urExistIndex].description !== rReq.description) {
                    dataModify.push(Object.assign(Object.assign({}, currentUserRoles[urExistIndex]), { description: rReq.description, owner_created }));
                }
                currentUserRoles.splice(urExistIndex, 1);
            }
            else {
                dataModify.push(Object.assign(Object.assign({}, rReq), { user_id,
                    owner_created }));
            }
        }
        const deleteUserRoleIds = currentUserRoles.map((ur) => {
            return ur.id;
        });
        if (deleteUserRoleIds.length > 0) {
            const deleteUserRoles = await this.permissionRepo.findRoleByIds(deleteUserRoleIds);
            deleteUserRoles.forEach((dr) => {
                updateUserAssetLogs.push({
                    asset_id: dr.id,
                    user_id: user_id,
                    category: transaction_1.ORDER_CATEGORY.PKG,
                    status: transaction_1.ITEM_STATUS.DELETED,
                    owner_created,
                    name: dr.role_name,
                });
            });
            await this.userRepo.deleteUserRoleByIds(deleteUserRoleIds);
        }
        const modify = await this.userRepo.saveUserRole(dataModify);
        await this.userRepo.saveUserAssetLog(updateUserAssetLogs);
        return modify;
    }
    async addUserRoles(params) {
        const { user_ids, owner_created, role_ids, quantity, package_type } = params;
        let expiresAt = null;
        if (quantity) {
            const expiredTime = new Date();
            if (package_type === const_1.PACKAGE_TYPE.DAY) {
                expiredTime.setDate(expiredTime.getDate() + quantity);
            }
            else {
                expiredTime.setMonth(expiredTime.getMonth() + quantity);
            }
            expiresAt = expiredTime.valueOf();
        }
        const userRoles = await this.userRepo.findUserRoleByUserIdsAndRoleIds(user_ids, role_ids);
        const dataDoles = await this.permissionRepo.findRoleByIds(role_ids);
        const dataAdd = [];
        const updateUserAssetLogs = [];
        for (const user_id of user_ids) {
            for (let role of dataDoles) {
                if (role) {
                    const role_id = role.id;
                    const userRoleExisted = userRoles.find((u) => u.user_id === user_id && u.role_id === role_id);
                    updateUserAssetLogs.push({
                        asset_id: role_id,
                        user_id: user_id,
                        category: transaction_1.ORDER_CATEGORY.PKG,
                        status: transaction_1.ITEM_STATUS.ACTIVE,
                        owner_created,
                        expires_at: expiresAt,
                        name: role.role_name,
                        quantity,
                        package_type,
                    });
                    if (userRoleExisted) {
                        if (expiresAt &&
                            userRoleExisted.expires_at &&
                            userRoleExisted.expires_at > Date.now()) {
                            expiresAt =
                                Number(userRoleExisted.expires_at) + expiresAt - Date.now();
                        }
                        dataAdd.push({
                            id: userRoleExisted.id,
                            user_id,
                            role_id,
                            expires_at: expiresAt,
                            owner_created,
                        });
                    }
                    else {
                        dataAdd.push({
                            user_id,
                            role_id,
                            expires_at: expiresAt,
                            owner_created,
                        });
                    }
                }
            }
        }
        if (dataAdd.length > 0) {
            await this.userRepo.saveUserRole(dataAdd);
            await this.userRepo.saveUserAssetLog(updateUserAssetLogs);
        }
        return true;
    }
    async removeUserRoles(owner_created, params) {
        const { user_ids, role_ids } = params;
        await this.userRepo.deleteUserRoles(user_ids, role_ids);
        const dataDoles = await this.permissionRepo.findRoleByIds(role_ids);
        const updateUserAssetLogs = [];
        for (let userId of user_ids) {
            for (let role of dataDoles) {
                if (role) {
                    const roleId = role.id;
                    updateUserAssetLogs.push({
                        asset_id: roleId,
                        user_id: userId,
                        category: transaction_1.ORDER_CATEGORY.PKG,
                        status: transaction_1.ITEM_STATUS.DELETED,
                        owner_created,
                        name: role.role_name,
                    });
                }
            }
        }
        await this.userRepo.saveUserAssetLog(updateUserAssetLogs);
        return true;
    }
};
UserRoleService = __decorate([
    __param(1, common_1.Inject(permission_repository_1.PermissionRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        permission_repository_1.PermissionRepository])
], UserRoleService);
exports.UserRoleService = UserRoleService;
//# sourceMappingURL=index.js.map