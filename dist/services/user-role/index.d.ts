import { AddUserRoles, RemoveUserRoles, UserRoleDomain, UserRoleModify } from 'src/domains/user';
import { PermissionRepository } from 'src/repositories/permission.repository';
import { UserRepository } from 'src/repositories/user.repository';
export declare class UserRoleService implements UserRoleDomain {
    private userRepo;
    private permissionRepo;
    constructor(userRepo: UserRepository, permissionRepo: PermissionRepository);
    modifyUserRole(params: UserRoleModify): Promise<any>;
    addUserRoles(params: AddUserRoles): Promise<any>;
    removeUserRoles(owner_created: string, params: RemoveUserRoles): Promise<any>;
}
