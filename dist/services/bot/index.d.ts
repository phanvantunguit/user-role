import { QueryBot, RawBot, BotDomain } from 'src/domains/bot/bot.types';
import { BotSettingRepository } from 'src/repositories/bot-setting.repository';
import { BotRepository } from 'src/repositories/bot.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { BaseService } from '../base';
export declare class BotService extends BaseService<QueryBot, RawBot> implements BotDomain {
    private botRepository;
    private userRepository;
    private botSettingRepository;
    constructor(botRepository: BotRepository, userRepository: UserRepository, botSettingRepository: BotSettingRepository);
    listBot(params: {
        user_id?: string;
    }): Promise<any>;
    updateOrderBot(params: {
        bots: {
            id: string;
            order: number;
        }[];
        owner_created: string;
    }): Promise<any[]>;
    createBot(params: RawBot): Promise<RawBot>;
    handleBotWithoutUserId(params: {
        bot_id?: string;
    }): Promise<any>;
}
