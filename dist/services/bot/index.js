"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const bot_types_1 = require("../../domains/bot/bot.types");
const bot_setting_repository_1 = require("../../repositories/bot-setting.repository");
const bot_repository_1 = require("../../repositories/bot.repository");
const user_repository_1 = require("../../repositories/user.repository");
const handle_error_util_1 = require("../../utils/handle-error.util");
const base_1 = require("../base");
let BotService = class BotService extends base_1.BaseService {
    constructor(botRepository, userRepository, botSettingRepository) {
        super(botRepository);
        this.botRepository = botRepository;
        this.userRepository = userRepository;
        this.botSettingRepository = botSettingRepository;
    }
    async listBot(params) {
        const { user_id } = params;
        if (user_id) {
            const userBots = await this.userRepository.findUserBot({ user_id });
            const botIds = userBots.map((e) => e.bot_id);
            if (botIds.length === 0) {
                return [];
            }
            return await this.botRepository.findByIds(botIds);
        }
        return await this.list({});
    }
    async updateOrderBot(params) {
        const { bots, owner_created } = params;
        const dataUpdate = bots.map((e) => {
            return Object.assign(Object.assign({}, e), { owner_created });
        });
        return this.botRepository.saves(dataUpdate);
    }
    async createBot(params) {
        const botSeting = await this.botSettingRepository.findById(params.bot_setting_id);
        if (!botSeting) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
        const code = (botSeting.params &&
            botSeting.params.type &&
            botSeting.params.type.default) ||
            botSeting.name.replace(/ /g, '');
        return this.save(Object.assign(Object.assign({}, params), { code }));
    }
    async handleBotWithoutUserId(params) {
        const { bot_id } = params;
        const resultFindUserId = await this.userRepository.findUserBot({
            bot_id,
        });
        if (resultFindUserId.length > 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.USERS_HAVE_BOTS));
        }
        else {
            return await this.botRepository.deleteById(bot_id);
        }
    }
};
BotService = __decorate([
    __param(0, common_1.Inject(bot_repository_1.BotRepository)),
    __param(1, common_1.Inject(user_repository_1.UserRepository)),
    __param(2, common_1.Inject(bot_setting_repository_1.BotSettingRepository)),
    __metadata("design:paramtypes", [bot_repository_1.BotRepository,
        user_repository_1.UserRepository,
        bot_setting_repository_1.BotSettingRepository])
], BotService);
exports.BotService = BotService;
//# sourceMappingURL=index.js.map