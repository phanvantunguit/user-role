"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const transaction_1 = require("../../const/transaction");
const transaction_types_1 = require("../../domains/transaction/transaction.types");
const package_ropository_1 = require("../../repositories/package.ropository");
const permission_repository_1 = require("../../repositories/permission.repository");
const transaction_detail_repository_1 = require("../../repositories/transaction-detail.repository");
const transaction_repository_1 = require("../../repositories/transaction.repository");
const user_repository_1 = require("../../repositories/user.repository");
const handle_error_util_1 = require("../../utils/handle-error.util");
const transaction_log_1 = require("../transaction-log");
const payment_1 = require("../payment");
const user_1 = require("../../domains/user");
const event_emitter_1 = require("@nestjs/event-emitter");
const db_context_1 = require("../../repositories/db-context");
let TransactionService = class TransactionService {
    constructor(paymentService, transactionRepository, permissionRepository, transactionLogService, transactionDetailRepository, userRepository, packageRepository, eventEmitter, dBContext) {
        this.paymentService = paymentService;
        this.transactionRepository = transactionRepository;
        this.permissionRepository = permissionRepository;
        this.transactionLogService = transactionLogService;
        this.transactionDetailRepository = transactionDetailRepository;
        this.userRepository = userRepository;
        this.packageRepository = packageRepository;
        this.eventEmitter = eventEmitter;
        this.dBContext = dBContext;
    }
    async getTransactionPagination(params) {
        return await this.transactionRepository.viewFindTransactionPaging(params);
    }
    async ipnTransaction(method, pramas) {
        const { transaction, transaction_event, metadata } = await this.paymentService.getService(method).ipnTransaction(pramas);
        if (!transaction) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BAD_REQUEST));
        }
        let transaction_status = transaction_1.TRANSACTION_STATUS.PROCESSING;
        switch (transaction_event) {
            case transaction_1.TRANSACTION_EVENT.PAYMENT_FAILED:
                transaction_status = transaction_1.TRANSACTION_STATUS.FAILED;
                break;
            case transaction_1.TRANSACTION_EVENT.PAYMENT_CREATED:
                transaction_status = transaction_1.TRANSACTION_STATUS.CREATED;
                break;
            default:
                transaction_status = transaction_1.TRANSACTION_STATUS.PROCESSING;
        }
        const transLog = await this.transactionLogService.get({
            transaction_id: transaction.id,
            transaction_event,
        });
        if (!transLog) {
            await this.transactionLogService.save({
                transaction_id: transaction.id,
                transaction_event,
                transaction_status,
                metadata: metadata,
            });
        }
        else {
            this.eventEmitter.emit(transLog.transaction_event, transLog);
        }
    }
    async listTransaction(parmas) {
        const transaction = await this.transactionRepository.viewFind(parmas);
        return transaction;
    }
    async getTransaction(parmas) {
        const transaction = await this.transactionRepository.viewfindOne(parmas);
        if (!transaction) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.TRANSACTION_NOT_FOUND));
        }
        const createdInfo = await this.transactionLogService.get({
            transaction_id: parmas.id,
            transaction_event: transaction_1.TRANSACTION_EVENT.PAYMENT_CREATED,
        });
        let paymentInfo = {};
        if (transaction.payment_method === transaction_1.PAYMENT_METHOD.COIN_PAYMENT &&
            createdInfo.metadata.error === 'ok') {
            paymentInfo = createdInfo.metadata.result;
        }
        return Object.assign(Object.assign({}, paymentInfo), transaction);
    }
    async createTransaction(parmas) {
        const { user_id, payment_method, description, role_id, buy_currency, buyer_email, package_id, } = parmas;
        const roles = await this.permissionRepository.findRoleByIds([role_id]);
        const packageTime = await this.packageRepository.findById(package_id);
        if (roles.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.ROLE_NOT_FOUND));
        }
        if (roles[0].status !== const_1.ROLE_STATUS.OPEN) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ROLE_INVALID));
        }
        if (!packageTime) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.PACKAGE_NOT_FOUND));
        }
        const sellCurrency = buy_currency;
        const price = Number(roles[0].price) || 0;
        const quantity = packageTime.quantity || 0;
        const discountRate = packageTime.discount_rate || 0;
        const discountAmount = packageTime.discount_amount || 0;
        const sellAmount = price * quantity - price * quantity * discountRate - discountAmount;
        try {
            return await this.dBContext.runInTransaction(async (queryRunner) => {
                const newTrans = await this.transactionRepository.save({
                    user_id,
                    payment_method,
                    description,
                    status: transaction_1.TRANSACTION_STATUS.CREATED,
                    sell_amount: sellAmount,
                    sell_currency: sellCurrency,
                    buy_currency,
                }, queryRunner);
                const expiredTime = new Date();
                const transDetail = await this.transactionDetailRepository.save({
                    transaction_id: newTrans.id,
                    user_id: newTrans.user_id,
                    role_id: roles[0].id,
                    price: Number(roles[0].price),
                    currency: sellCurrency,
                    package_id,
                    package_type: packageTime.type,
                    package_name: packageTime.name,
                    discount_rate: Number(packageTime.discount_rate),
                    discount_amount: Number(packageTime.discount_amount),
                    quantity: Number(packageTime.quantity),
                    expires_at: expiredTime.setMonth(expiredTime.getMonth() + quantity),
                }, queryRunner);
                const paymentResult = await this.paymentService
                    .getService(payment_method)
                    .createTransaction(newTrans, buyer_email);
                await this.transactionRepository.save(Object.assign(Object.assign({}, newTrans), { payment_id: paymentResult.txn_id, buy_amount: paymentResult.buy_amount, wallet_address: paymentResult.wallet_address }), queryRunner);
                await this.transactionLogService.save({
                    transaction_id: newTrans.id,
                    transaction_event: transaction_1.TRANSACTION_EVENT.PAYMENT_CREATED,
                    transaction_status: transaction_1.TRANSACTION_STATUS.CREATED,
                    metadata: paymentResult.metadata,
                });
                delete paymentResult.metadata;
                return Object.assign(Object.assign({}, paymentResult), { transaction_id: newTrans.id, buy_currency, sell_amount: sellAmount, sell_currency: sellCurrency, details: [transDetail], created_at: newTrans.created_at });
            });
        }
        catch (error) {
            let message = const_1.ERROR_CODE.CREATE_TRANSACTION_FAILED.message;
            if (error && error.error && error.error.includes('Invalid currency')) {
                message = 'Currency is not support';
            }
            handle_error_util_1.throwError({
                status: common_1.HttpStatus.UNPROCESSABLE_ENTITY,
                error_code: const_1.ERROR_CODE.CREATE_TRANSACTION_FAILED.error_code,
                message,
            });
        }
    }
    async handlePaymentComplete(transaction) {
        const [transDetail] = await Promise.all([
            this.transactionDetailRepository.find({ transaction_id: transaction.id }),
        ]);
        const detail = transDetail[0];
        const userRole = await this.userRepository.findUserRole({
            role_id: detail.role_id,
            user_id: detail.user_id,
        });
        const dataUpdateRole = {
            role_id: detail.role_id,
            description: 'User upgrade',
            user_id: detail.user_id,
            owner_created: detail.user_id,
            quantity: detail.quantity,
            package_name: detail.package_name,
            expires_at: detail.expires_at,
            package_id: detail.package_id,
            package_type: detail.package_type,
        };
        if (userRole.length > 0) {
            if (userRole[0].expires_at > Date.now()) {
                dataUpdateRole.expires_at =
                    Number(userRole[0].expires_at) +
                        Number(dataUpdateRole.expires_at) -
                        Date.now();
            }
            dataUpdateRole['id'] = userRole[0].id;
        }
        await this.userRepository.saveUserRole([dataUpdateRole]);
        const dataUpdate = Object.assign(Object.assign({}, transaction), { status: transaction_1.TRANSACTION_STATUS.COMPLETE });
        await this.transactionRepository.save(dataUpdate);
        await this.transactionLogService.saveNotSendEvent({
            transaction_id: transaction.id,
            transaction_event: transaction_1.TRANSACTION_EVENT.DELIVERED,
            transaction_status: transaction_1.TRANSACTION_STATUS.COMPLETE,
            metadata: dataUpdateRole,
        });
    }
    async handlePaymentFailed(transaction) {
        const dataUpdate = Object.assign(Object.assign({}, transaction), { status: transaction_1.TRANSACTION_STATUS.FAILED });
        this.transactionRepository.save(dataUpdate);
    }
    async checkCreateTransactionValid(user_id, role_id, package_id, buy_currency) {
        const trans = await this.transactionRepository.viewFindByUserMultipleStatus(user_id, [transaction_1.TRANSACTION_STATUS.PROCESSING, transaction_1.TRANSACTION_STATUS.CREATED]);
        const transaction = trans.find((t) => t.details.length > 0 &&
            t.details[0].role_id === role_id &&
            t.details[0].package_id === package_id &&
            t.buy_currency === buy_currency);
        if (transaction && transaction.payment_id) {
            const createdInfo = await this.transactionLogService.get({
                transaction_id: transaction.id,
                transaction_event: transaction_1.TRANSACTION_EVENT.PAYMENT_CREATED,
            });
            let paymentInfo = {};
            if (transaction.payment_method === transaction_1.PAYMENT_METHOD.COIN_PAYMENT &&
                createdInfo.metadata.error === 'ok') {
                paymentInfo = createdInfo.metadata.result;
            }
            const transactionId = transaction.id;
            delete transaction.id;
            return Object.assign(Object.assign(Object.assign({}, transaction), paymentInfo), { transaction_id: transactionId });
        }
        return null;
    }
    async upgradeFreeTrial(user_id, role_id) {
        const roles = await this.permissionRepository.findRoleByIds([role_id]);
        if (roles.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.ROLE_NOT_FOUND));
        }
        const price = Number(roles[0].price) || 0;
        if (price !== 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_GATEWAY }, const_1.ERROR_CODE.ROLE_INVALID));
        }
        const userRole = await this.userRepository.findUserRole({
            role_id: role_id,
            user_id: user_id,
        });
        if (userRole.length > 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_GATEWAY }, const_1.ERROR_CODE.PACKAGE_UPGRADED));
        }
        const quantity = 14;
        const expiredTime = new Date();
        const dataUpdateRole = {
            role_id,
            description: 'User upgrade',
            user_id,
            owner_created: user_id,
            quantity,
            package_name: '14 Days',
            expires_at: expiredTime.setDate(expiredTime.getDate() + quantity),
            package_type: const_1.PACKAGE_TYPE.DAY,
        };
        const data = await this.userRepository.saveUserRole([dataUpdateRole]);
        return data[0];
    }
    async checkPartnerTransaction(transaction) {
        const { transaction_event, metadata } = await this.paymentService
            .getService(transaction.payment_method)
            .checkTransaction(transaction.payment_id);
        let transaction_status = transaction_1.TRANSACTION_STATUS.PROCESSING;
        switch (transaction_event) {
            case transaction_1.TRANSACTION_EVENT.PAYMENT_FAILED:
                transaction_status = transaction_1.TRANSACTION_STATUS.FAILED;
                break;
            case transaction_1.TRANSACTION_EVENT.PAYMENT_CREATED:
                transaction_status = transaction_1.TRANSACTION_STATUS.CREATED;
                break;
            default:
                transaction_status = transaction_1.TRANSACTION_STATUS.PROCESSING;
        }
        const transLog = await this.transactionLogService.get({
            transaction_id: transaction.id,
            transaction_event,
        });
        if (!transLog) {
            await this.transactionLogService.save({
                transaction_id: transaction.id,
                transaction_event,
                transaction_status,
                metadata: metadata,
            });
        }
        else {
            this.eventEmitter.emit(transLog.transaction_event, transLog);
        }
        return { transaction_status, transaction_event, metadata };
    }
};
TransactionService = __decorate([
    __param(0, common_1.Inject(payment_1.PaymentService)),
    __param(1, common_1.Inject(transaction_repository_1.TransactionRepository)),
    __param(2, common_1.Inject(permission_repository_1.PermissionRepository)),
    __param(3, common_1.Inject(transaction_log_1.TransactionLogService)),
    __param(4, common_1.Inject(transaction_detail_repository_1.TransactionDetailRepository)),
    __param(5, common_1.Inject(user_repository_1.UserRepository)),
    __param(6, common_1.Inject(package_ropository_1.PackageRepository)),
    __param(7, common_1.Inject(event_emitter_1.EventEmitter2)),
    __param(8, common_1.Inject(db_context_1.DBContext)),
    __metadata("design:paramtypes", [payment_1.PaymentService,
        transaction_repository_1.TransactionRepository,
        permission_repository_1.PermissionRepository,
        transaction_log_1.TransactionLogService,
        transaction_detail_repository_1.TransactionDetailRepository,
        user_repository_1.UserRepository,
        package_ropository_1.PackageRepository,
        event_emitter_1.EventEmitter2,
        db_context_1.DBContext])
], TransactionService);
exports.TransactionService = TransactionService;
//# sourceMappingURL=index.js.map