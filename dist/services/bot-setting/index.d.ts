import { BotSettingDomain, QueryBotSetting, RawBotSetting } from 'src/domains/setting/bot-setting.types';
import { BotSettingRepository } from 'src/repositories/bot-setting.repository';
import { BaseService } from '../base';
export declare class BotSettingService extends BaseService<QueryBotSetting, RawBotSetting> implements BotSettingDomain {
    private botSettingRepository;
    constructor(botSettingRepository: BotSettingRepository);
}
