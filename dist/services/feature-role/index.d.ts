import { PermissionRepository } from 'src/repositories/permission.repository';
import { FeatureRoleDomain, FeatureRoleCreate } from 'src/domains/permission';
import { SettingRepository } from 'src/repositories/setting.repository';
import { RoleService } from '../role';
export declare class FeatureRoleService implements FeatureRoleDomain {
    private roleService;
    private settingRepo;
    private permissionRepo;
    constructor(roleService: RoleService, settingRepo: SettingRepository, permissionRepo: PermissionRepository);
    modifyFeatureRole(params: FeatureRoleCreate): Promise<any>;
    private getFeatureOrThrow;
}
