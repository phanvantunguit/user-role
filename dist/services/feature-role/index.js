"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FeatureRoleService = void 0;
const permission_repository_1 = require("../../repositories/permission.repository");
const common_1 = require("@nestjs/common");
const permission_1 = require("../../domains/permission");
const const_1 = require("../../const");
const handle_error_util_1 = require("../../utils/handle-error.util");
const setting_repository_1 = require("../../repositories/setting.repository");
const role_1 = require("../role");
let FeatureRoleService = class FeatureRoleService {
    constructor(roleService, settingRepo, permissionRepo) {
        this.roleService = roleService;
        this.settingRepo = settingRepo;
        this.permissionRepo = permissionRepo;
    }
    async modifyFeatureRole(params) {
        const { owner_created, features, role_id } = params;
        await this.roleService.checkRoleNotFoundAndThrow({
            id: role_id,
        });
        const currentFeatureRoles = await this.permissionRepo.findFeatureRoles({
            role_id,
        });
        const dataModify = [];
        for (const fReq of features) {
            const frExistIndex = currentFeatureRoles.findIndex((cfr) => cfr.feature_id === fReq.feature_id);
            if (frExistIndex > -1) {
                if (currentFeatureRoles[frExistIndex].description !== fReq.description) {
                    dataModify.push(Object.assign(Object.assign({}, currentFeatureRoles[frExistIndex]), { description: fReq.description, owner_created }));
                }
                currentFeatureRoles.splice(frExistIndex, 1);
            }
            else {
                dataModify.push(Object.assign(Object.assign({}, fReq), { role_id,
                    owner_created }));
            }
        }
        const deleteFeatureRoles = currentFeatureRoles.map((aur) => aur.id);
        if (deleteFeatureRoles.length > 0) {
            await this.permissionRepo.deleteFeatureRolesByIds(deleteFeatureRoles);
        }
        await this.permissionRepo.saveFeatureRoles(dataModify);
        const roles = await this.permissionRepo.findRoles({ id: role_id });
        return roles[0];
    }
    async getFeatureOrThrow(feature_ids) {
        const features = await this.settingRepo.findFeatureByIds(feature_ids);
        if (features.length !== feature_ids.length) {
            const featureIdsNotFound = [];
            for (const f of features) {
                if (!feature_ids.includes(f.feature_id)) {
                    featureIdsNotFound.push(f.feature_id);
                }
            }
            handle_error_util_1.throwError({
                status: common_1.HttpStatus.NOT_FOUND,
                error_code: const_1.ERROR_CODE.FEATURE_NOT_FOUND.error_code,
                message: featureIdsNotFound,
            });
        }
        return features;
    }
};
FeatureRoleService = __decorate([
    __param(1, common_1.Inject(setting_repository_1.SettingRepository)),
    __param(2, common_1.Inject(permission_repository_1.PermissionRepository)),
    __metadata("design:paramtypes", [role_1.RoleService,
        setting_repository_1.SettingRepository,
        permission_repository_1.PermissionRepository])
], FeatureRoleService);
exports.FeatureRoleService = FeatureRoleService;
//# sourceMappingURL=index.js.map