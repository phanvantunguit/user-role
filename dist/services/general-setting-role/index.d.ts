import { PermissionRepository } from 'src/repositories/permission.repository';
import { GeneralSettingRoleDomain, GeneralSettingRoleCreate } from 'src/domains/permission';
import { SettingRepository } from 'src/repositories/setting.repository';
import { RoleService } from '../role';
export declare class GeneralSettingRoleService implements GeneralSettingRoleDomain {
    private roleService;
    private settingRepo;
    private permissionRepo;
    constructor(roleService: RoleService, settingRepo: SettingRepository, permissionRepo: PermissionRepository);
    getMaxLimitTab(roleIds: any): Promise<number>;
    modifyGeneralSettingRole(params: GeneralSettingRoleCreate): Promise<any>;
    private getGeneralSettingOrThrow;
}
