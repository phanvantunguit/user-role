"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GeneralSettingRoleService = void 0;
const permission_repository_1 = require("../../repositories/permission.repository");
const common_1 = require("@nestjs/common");
const permission_1 = require("../../domains/permission");
const const_1 = require("../../const");
const handle_error_util_1 = require("../../utils/handle-error.util");
const setting_repository_1 = require("../../repositories/setting.repository");
const role_1 = require("../role");
let GeneralSettingRoleService = class GeneralSettingRoleService {
    constructor(roleService, settingRepo, permissionRepo) {
        this.roleService = roleService;
        this.settingRepo = settingRepo;
        this.permissionRepo = permissionRepo;
    }
    async getMaxLimitTab(roleIds) {
        const currentGSRoles = await this.permissionRepo.findGeneralSettingMultipleRoles(roleIds, const_1.GENERAL_SETTING_LIMIT_TAB);
        if (currentGSRoles.length > 0) {
            return Math.max(...currentGSRoles.map((st) => st.val_limit));
        }
        return 1;
    }
    async modifyGeneralSettingRole(params) {
        const { owner_created, general_settings, role_id } = params;
        await this.roleService.checkRoleNotFoundAndThrow({
            id: role_id,
        });
        const currentGSRoles = await this.permissionRepo.findGeneralSettingRoles({
            role_id,
        });
        const dataModify = [];
        for (const gReq of general_settings) {
            if (gReq.val_limit) {
                const gExistIndex = currentGSRoles.findIndex((cgsr) => cgsr.general_setting_id === gReq.general_setting_id);
                if (gExistIndex > -1) {
                    if (currentGSRoles[gExistIndex].description !== gReq.description ||
                        Number(currentGSRoles[gExistIndex].val_limit) !==
                            Number(gReq.val_limit)) {
                        dataModify.push(Object.assign(Object.assign({}, currentGSRoles[gExistIndex]), { description: gReq.description, val_limit: gReq.val_limit, owner_created }));
                    }
                    currentGSRoles.splice(gExistIndex, 1);
                }
                else {
                    dataModify.push(Object.assign(Object.assign({}, gReq), { role_id,
                        owner_created }));
                }
            }
        }
        const deleteGSR = currentGSRoles.map((aur) => aur.id);
        if (deleteGSR.length > 0) {
            await this.permissionRepo.deleteGeneralSettingRolesByIds(deleteGSR);
        }
        await this.permissionRepo.saveGeneralSettingRoles(dataModify);
        const roles = await this.permissionRepo.findRoles({ id: role_id });
        return roles[0];
    }
    async getGeneralSettingOrThrow(general_setting_ids) {
        const generalSettings = await this.settingRepo.findGeneralSettingByIds(general_setting_ids);
        if (generalSettings.length !== general_setting_ids.length) {
            const gsIdsNotFound = [];
            for (const f of generalSettings) {
                if (!general_setting_ids.includes(f.general_setting_id)) {
                    gsIdsNotFound.push(f.general_setting_id);
                }
            }
            handle_error_util_1.throwError({
                status: common_1.HttpStatus.NOT_FOUND,
                error_code: const_1.ERROR_CODE.FEATURE_NOT_FOUND.error_code,
                message: gsIdsNotFound,
            });
        }
        return generalSettings;
    }
};
GeneralSettingRoleService = __decorate([
    __param(1, common_1.Inject(setting_repository_1.SettingRepository)),
    __param(2, common_1.Inject(permission_repository_1.PermissionRepository)),
    __metadata("design:paramtypes", [role_1.RoleService,
        setting_repository_1.SettingRepository,
        permission_repository_1.PermissionRepository])
], GeneralSettingRoleService);
exports.GeneralSettingRoleService = GeneralSettingRoleService;
//# sourceMappingURL=index.js.map