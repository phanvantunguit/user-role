import { PAYMENT_METHOD } from 'src/const/transaction';
import { PaymentDomain } from 'src/domains/payment/payment.types';
import { CoinpaymentService } from './coinpayment';
export declare class PaymentService {
    private coinpaymentService;
    constructor(coinpaymentService: CoinpaymentService);
    getService(method: PAYMENT_METHOD): PaymentDomain;
}
