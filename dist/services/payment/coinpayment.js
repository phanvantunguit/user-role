"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoinpaymentService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const transaction_1 = require("../../const/transaction");
const payment_types_1 = require("../../domains/payment/payment.types");
const transaction_2 = require("../../domains/transaction");
const transaction_repository_1 = require("../../repositories/transaction.repository");
const coinpayment_1 = require("../../resources/coinpayment");
const handle_error_util_1 = require("../../utils/handle-error.util");
const transaction_log_1 = require("../transaction-log");
let CoinpaymentService = class CoinpaymentService {
    constructor(coinpaymentResource, transactionRepository, transactionLogService) {
        this.coinpaymentResource = coinpaymentResource;
        this.transactionRepository = transactionRepository;
        this.transactionLogService = transactionLogService;
    }
    async ipnTransaction(ipnData) {
        let checktrans;
        let transaction;
        switch (ipnData.ipn_type) {
            case transaction_1.COIN_PAYMENT_IPN_TYPE.API: {
                checktrans = await this.coinpaymentResource.checkTransaction(ipnData.txn_id);
                transaction = await this.transactionRepository.findOne({
                    wallet_address: checktrans.result.payment_address,
                });
                break;
            }
            case transaction_1.COIN_PAYMENT_IPN_TYPE.WITHDRAWAL: {
                checktrans = await this.coinpaymentResource.checkWithdrawal(ipnData.id);
                checktrans.result['id'] = ipnData.id;
                transaction = await this.transactionRepository.findOne({
                    wallet_address: checktrans.result.send_address,
                });
                break;
            }
        }
        if (checktrans.error !== 'ok') {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BAD_REQUEST));
        }
        let transaction_event = transaction_1.TRANSACTION_EVENT.PAYMENT_PROCESSING;
        if (Number(checktrans.result.status) >= 100) {
            transaction_event = transaction_1.TRANSACTION_EVENT.PAYMENT_COMPLETE;
        }
        else if (Number(checktrans.result.status) < 0) {
            transaction_event = transaction_1.TRANSACTION_EVENT.PAYMENT_FAILED;
        }
        else if (checktrans.result.status === 0) {
            transaction_event = transaction_1.TRANSACTION_EVENT.PAYMENT_CREATED;
        }
        return {
            transaction,
            transaction_event,
            metadata: checktrans,
        };
    }
    async createTransaction(transaction, email) {
        const resTrans = await this.coinpaymentResource.createTransaction({
            sell_amount: Number(transaction.sell_amount),
            sell_currency: transaction.sell_currency,
            buy_currency: transaction.buy_currency,
            buyer_email: email,
        });
        if (resTrans.error !== 'ok') {
            throw resTrans;
        }
        return {
            buy_amount: resTrans.result.amount,
            txn_id: resTrans.result.txn_id,
            wallet_address: resTrans.result.address,
            timeout: resTrans.result.timeout,
            qrcode_url: resTrans.result.qrcode_url,
            status_url: resTrans.result.status_url,
            checkout_url: resTrans.result.checkout_url,
            metadata: resTrans,
        };
    }
    async extraDataPaymentInfo(payload) {
        const time = `${Number(payload.metadata.result.timeout) / 60} minutes`;
        return {
            wallet_address: payload.metadata.result.address,
            time,
            checkout_url: payload.metadata.result.checkout_url,
            qrcode_url: payload.metadata.result.qrcode_url,
            status_url: payload.metadata.result.status_url,
        };
    }
    async checkTransaction(payment_id) {
        const checktrans = await this.coinpaymentResource.checkTransaction(payment_id);
        let transaction_event = transaction_1.TRANSACTION_EVENT.PAYMENT_PROCESSING;
        if (checktrans.error !== 'ok') {
            transaction_event = transaction_1.TRANSACTION_EVENT.PAYMENT_FAILED;
            return {
                transaction_event,
                metadata: checktrans,
            };
        }
        if (Number(checktrans.result.status) >= 100) {
            transaction_event = transaction_1.TRANSACTION_EVENT.PAYMENT_COMPLETE;
        }
        else if (Number(checktrans.result.status) < 0) {
            transaction_event = transaction_1.TRANSACTION_EVENT.PAYMENT_FAILED;
        }
        else if (checktrans.result.status === 0) {
            transaction_event = transaction_1.TRANSACTION_EVENT.PAYMENT_CREATED;
        }
        return {
            transaction_event,
            metadata: checktrans,
        };
    }
};
CoinpaymentService = __decorate([
    __param(0, common_1.Inject(coinpayment_1.CoinpaymentResource)),
    __metadata("design:paramtypes", [coinpayment_1.CoinpaymentResource,
        transaction_repository_1.TransactionRepository,
        transaction_log_1.TransactionLogService])
], CoinpaymentService);
exports.CoinpaymentService = CoinpaymentService;
//# sourceMappingURL=coinpayment.js.map