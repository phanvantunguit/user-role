import { TRANSACTION_EVENT } from 'src/const/transaction';
import { PaymentDomain } from 'src/domains/payment/payment.types';
import { RawTransaction, RawTransactionLog } from 'src/domains/transaction';
import { TransactionRepository } from 'src/repositories/transaction.repository';
import { CoinpaymentResource } from 'src/resources/coinpayment';
import { TransactionLogService } from '../transaction-log';
export declare class CoinpaymentService implements PaymentDomain {
    private coinpaymentResource;
    private transactionRepository;
    private transactionLogService;
    constructor(coinpaymentResource: CoinpaymentResource, transactionRepository: TransactionRepository, transactionLogService: TransactionLogService);
    ipnTransaction(ipnData: any): Promise<{
        transaction: any;
        transaction_event: TRANSACTION_EVENT.PAYMENT_CREATED | TRANSACTION_EVENT.PAYMENT_PROCESSING | TRANSACTION_EVENT.PAYMENT_COMPLETE | TRANSACTION_EVENT.PAYMENT_FAILED;
        metadata: any;
    }>;
    createTransaction(transaction: RawTransaction, email: string): Promise<any>;
    extraDataPaymentInfo(payload: RawTransactionLog): Promise<{
        wallet_address: any;
        time: string;
        checkout_url: any;
        qrcode_url: any;
        status_url: any;
    }>;
    checkTransaction(payment_id: string): Promise<{
        transaction_event: TRANSACTION_EVENT.PAYMENT_CREATED | TRANSACTION_EVENT.PAYMENT_PROCESSING | TRANSACTION_EVENT.PAYMENT_COMPLETE | TRANSACTION_EVENT.PAYMENT_FAILED;
        metadata: any;
    }>;
}
