"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentService = void 0;
const common_1 = require("@nestjs/common");
const transaction_1 = require("../../const/transaction");
const payment_types_1 = require("../../domains/payment/payment.types");
const coinpayment_1 = require("./coinpayment");
let PaymentService = class PaymentService {
    constructor(coinpaymentService) {
        this.coinpaymentService = coinpaymentService;
    }
    getService(method) {
        switch (method) {
            case transaction_1.PAYMENT_METHOD.COIN_PAYMENT:
                return this.coinpaymentService;
            default:
                throw new Error(`Payment method ${method} not implemented`);
        }
    }
};
PaymentService = __decorate([
    __param(0, common_1.Inject(coinpayment_1.CoinpaymentService)),
    __metadata("design:paramtypes", [coinpayment_1.CoinpaymentService])
], PaymentService);
exports.PaymentService = PaymentService;
//# sourceMappingURL=index.js.map