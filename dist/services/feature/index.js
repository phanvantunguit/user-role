"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FeatureService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const setting_1 = require("../../domains/setting");
const setting_repository_1 = require("../../repositories/setting.repository");
const handle_error_util_1 = require("../../utils/handle-error.util");
let FeatureService = class FeatureService {
    constructor(settingRepo) {
        this.settingRepo = settingRepo;
    }
    async getFeatureDetail(params) {
        const features = await this.settingRepo.findFeature(params);
        return features[0];
    }
    async getFeature() {
        const features = await this.settingRepo.findFeature({});
        return features;
    }
    async createFeature(params) {
        const { owner_created, features } = params;
        const dataCreate = features.map((f) => {
            return Object.assign(Object.assign({}, f), { owner_created });
        });
        const create = await this.settingRepo.saveFeatures(dataCreate);
        return create;
    }
    async updateFeature(params) {
        const { feature_id, feature_name, description, action, owner_created } = params;
        await this.checkFeatureNotFoundAndThrow({ feature_id });
        await this.checkExistedAndThrow({ feature_id, feature_name });
        const dataUpdate = {
            feature_id,
            feature_name,
            description,
            action,
            owner_created,
        };
        const update = await this.settingRepo.saveFeatures([dataUpdate]);
        return update;
    }
    async deleteFeature(feature_ids) {
        const deleted = await this.settingRepo.deleteFeatureIds(feature_ids);
        return deleted;
    }
    async checkFeatureNotFoundAndThrow(params) {
        const features = await this.settingRepo.findFeature(params);
        if (features.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.FEATURE_NOT_FOUND));
        }
    }
    async checkExistedAndThrow(params) {
        const features = await this.settingRepo.findFeature({
            feature_name: params.feature_name,
        });
        if (features.length > 0 && features[0].feature_id !== params.feature_id) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.FEATURE_EXISTED));
        }
    }
};
FeatureService = __decorate([
    __param(0, common_1.Inject(setting_repository_1.SettingRepository)),
    __metadata("design:paramtypes", [setting_repository_1.SettingRepository])
], FeatureService);
exports.FeatureService = FeatureService;
//# sourceMappingURL=index.js.map