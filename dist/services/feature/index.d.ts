import { FeatureCreate, FeatureDomain, FeatureUpdate, QueryFeature, RawFeature } from 'src/domains/setting';
import { SettingRepository } from 'src/repositories/setting.repository';
export declare class FeatureService implements FeatureDomain {
    private settingRepo;
    constructor(settingRepo: SettingRepository);
    getFeatureDetail(params: QueryFeature): Promise<RawFeature>;
    getFeature(): Promise<RawFeature[]>;
    createFeature(params: FeatureCreate): Promise<any>;
    updateFeature(params: FeatureUpdate): Promise<any>;
    deleteFeature(feature_ids: string[]): Promise<any>;
    private checkFeatureNotFoundAndThrow;
    private checkExistedAndThrow;
}
