"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExchangeService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const setting_1 = require("../../domains/setting");
const setting_repository_1 = require("../../repositories/setting.repository");
const handle_error_util_1 = require("../../utils/handle-error.util");
let ExchangeService = class ExchangeService {
    constructor(settingRepo) {
        this.settingRepo = settingRepo;
    }
    async getExchange(params) {
        const exchanges = await this.settingRepo.findExchange(params);
        if (exchanges.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.EXCHANGE_NOT_FOUND));
        }
        return exchanges[0];
    }
    async listExchange() {
        const exchanges = await this.settingRepo.findExchange({});
        return exchanges;
    }
    async createExchange(params) {
        const create = await this.settingRepo.saveExchanges(params);
        return create;
    }
    async updateExchange(params) {
        const { exchange_name } = params;
        await this.checkExchangeNotFoundAndThrow({ exchange_name });
        const updated = await this.settingRepo.saveExchanges([params]);
        return updated;
    }
    async deleteExchange(names) {
        const deleted = await this.settingRepo.deleteExchangeNames(names);
        return deleted;
    }
    async checkExchangeNotFoundAndThrow(params) {
        const exchanges = await this.settingRepo.findExchange(params);
        if (exchanges.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.EXCHANGE_NOT_FOUND));
        }
    }
};
ExchangeService = __decorate([
    __param(0, common_1.Inject(setting_repository_1.SettingRepository)),
    __metadata("design:paramtypes", [setting_repository_1.SettingRepository])
], ExchangeService);
exports.ExchangeService = ExchangeService;
//# sourceMappingURL=index.js.map