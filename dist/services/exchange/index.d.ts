import { Exchange, ExchangeDomain, QueryExchange } from 'src/domains/setting';
import { SettingRepository } from 'src/repositories/setting.repository';
export declare class ExchangeService implements ExchangeDomain {
    private settingRepo;
    constructor(settingRepo: SettingRepository);
    getExchange(params: QueryExchange): Promise<Exchange>;
    listExchange(): Promise<Exchange[]>;
    createExchange(params: Exchange[]): Promise<any>;
    updateExchange(params: Exchange): Promise<any>;
    deleteExchange(names: string[]): Promise<any>;
    private checkExchangeNotFoundAndThrow;
}
