"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppSettingService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const app_setting_1 = require("../../const/app-setting");
const transaction_1 = require("../../const/transaction");
const app_setting_types_1 = require("../../domains/setting/app-setting.types");
const setting_repository_1 = require("../../repositories/setting.repository");
const user_repository_1 = require("../../repositories/user.repository");
const handle_error_util_1 = require("../../utils/handle-error.util");
let AppSettingService = class AppSettingService {
    constructor(settingRepo, userRepository) {
        this.settingRepo = settingRepo;
        this.userRepository = userRepository;
    }
    async listAppSetting(params) {
        const data = await this.settingRepo.findAppSetting(params);
        if (params.user_id) {
            const numberOfBotConnected = await this.userRepository.findUserBotTradingSQL({
                user_id: params.user_id,
                type: transaction_1.TBOT_TYPE.SELECT
            });
            data.push({
                id: null,
                name: app_setting_1.APP_SETTING.NUMBER_OF_TBOT_USED,
                value: numberOfBotConnected.length.toString(),
                description: null,
                owner_created: null,
                created_at: null,
                updated_at: null,
                user_id: params.user_id,
            });
        }
        return data;
    }
    async getAppSetting(params) {
        if (!Object.values(app_setting_1.APP_SETTING).includes(params.name)) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.APP_SETTING_NOT_FOUND));
        }
        switch (params.name) {
            case app_setting_1.APP_SETTING.NUMBER_OF_TBOT_USED:
                const numberOfBotConnected = await this.userRepository.findUserBotTradingSQL({
                    user_id: params.user_id,
                    type: transaction_1.TBOT_TYPE.SELECT
                });
                return {
                    id: null,
                    name: params.name,
                    value: numberOfBotConnected.length.toString(),
                    description: null,
                    owner_created: null,
                    created_at: null,
                    updated_at: null,
                    user_id: params.user_id,
                };
            default:
                const data = await this.settingRepo.findAppSetting(params);
                if (data.length === 0) {
                    return {
                        id: null,
                        name: params.name,
                        value: null,
                        description: null,
                        owner_created: null,
                        created_at: null,
                        updated_at: null,
                        user_id: null,
                    };
                }
                return data[0];
        }
    }
    async createAppSetting(params) {
        await this.checkAppSettingExistedAndThrow({
            id: undefined,
            name: params.name,
        });
        return this.settingRepo.saveAppSetting(params);
    }
    async userCreateAppSetting(params) {
        const setting = await this.settingRepo.findOneAppSetting({
            name: params.name,
            user_id: params.owner_created,
        });
        if (setting) {
            return this.settingRepo.saveAppSetting(Object.assign(Object.assign({}, params), { id: setting.id, user_id: params.owner_created }));
        }
        else {
            return this.settingRepo.saveAppSetting(Object.assign(Object.assign({}, params), { user_id: params.owner_created }));
        }
    }
    async updateAppSetting(params) {
        await this.checkAppSettingExistedAndThrow({
            id: params.id,
            name: params.name,
        });
        return this.settingRepo.saveAppSetting(params);
    }
    async deleteAppSetting(id) {
        await this.checkAppSettingNotFoundAndThrow({ id });
        return this.settingRepo.deleteAppSettingById(id);
    }
    async checkAppSettingNotFoundAndThrow(params) {
        const data = await this.settingRepo.findAppSetting(params);
        if (data.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.APP_SETTING_NOT_FOUND));
        }
    }
    async checkAppSettingExistedAndThrow(params) {
        const data = await this.settingRepo.findAppSetting({ name: params.name });
        if (data.length > 0 && data[0].id !== params.id) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.APP_SETTING_EXISTED));
        }
    }
};
AppSettingService = __decorate([
    __param(0, common_1.Inject(setting_repository_1.SettingRepository)),
    __param(1, common_1.Inject(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [setting_repository_1.SettingRepository,
        user_repository_1.UserRepository])
], AppSettingService);
exports.AppSettingService = AppSettingService;
//# sourceMappingURL=index.js.map