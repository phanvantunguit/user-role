import { AppSetting, AppSettingDomain, AppSettingUpdate, QueryAppSetting, RawAppSetting } from 'src/domains/setting/app-setting.types';
import { SettingRepository } from 'src/repositories/setting.repository';
import { UserRepository } from 'src/repositories/user.repository';
export declare class AppSettingService implements AppSettingDomain {
    private settingRepo;
    private userRepository;
    constructor(settingRepo: SettingRepository, userRepository: UserRepository);
    listAppSetting(params: {
        user_id?: string;
    }): Promise<RawAppSetting[]>;
    getAppSetting(params: QueryAppSetting): Promise<RawAppSetting>;
    createAppSetting(params: AppSetting): Promise<any>;
    userCreateAppSetting(params: {
        name: string;
        value: string;
        description?: string;
        owner_created: string;
    }): Promise<any>;
    updateAppSetting(params: AppSettingUpdate): Promise<any>;
    deleteAppSetting(id: string): Promise<any>;
    private checkAppSettingNotFoundAndThrow;
    private checkAppSettingExistedAndThrow;
}
