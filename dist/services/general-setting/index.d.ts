import { GeneralSettingCreate, GeneralSettingDomain, GeneralSettingUpdate, QueryGeneralSetting, RawGeneralSetting } from 'src/domains/setting';
import { SettingRepository } from 'src/repositories/setting.repository';
export declare class GeneralSettingService implements GeneralSettingDomain {
    private settingRepo;
    constructor(settingRepo: SettingRepository);
    getGeneralSetting(params: QueryGeneralSetting): Promise<RawGeneralSetting>;
    listGeneralSetting(): Promise<RawGeneralSetting[]>;
    createGeneralSetting(params: GeneralSettingCreate): Promise<any>;
    updateGeneralSetting(params: GeneralSettingUpdate): Promise<any>;
    deleteGeneralSetting(general_setting_ids: string[]): Promise<any>;
    private checkGSExistAndThrow;
}
