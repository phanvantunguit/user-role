"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GeneralSettingService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const setting_1 = require("../../domains/setting");
const setting_repository_1 = require("../../repositories/setting.repository");
const handle_error_util_1 = require("../../utils/handle-error.util");
let GeneralSettingService = class GeneralSettingService {
    constructor(settingRepo) {
        this.settingRepo = settingRepo;
    }
    async getGeneralSetting(params) {
        const generalSettings = await this.settingRepo.findGeneralSetting(params);
        if (generalSettings.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.GENERAL_SETTING_NOT_FOUND));
        }
        return generalSettings[0];
    }
    async listGeneralSetting() {
        const generalSettings = await this.settingRepo.findGeneralSetting({});
        return generalSettings;
    }
    async createGeneralSetting(params) {
        const { owner_created, general_settings } = params;
        const dataCreate = general_settings.map((gs) => {
            return Object.assign(Object.assign({}, gs), { owner_created });
        });
        const create = await this.settingRepo.saveGeneralSetting(dataCreate);
        return create;
    }
    async updateGeneralSetting(params) {
        const { general_setting_id, general_setting_name } = params;
        await this.getGeneralSetting({ general_setting_id });
        await this.checkGSExistAndThrow({
            general_setting_id,
            general_setting_name,
        });
        const update = await this.settingRepo.saveGeneralSetting([params]);
        return update;
    }
    async deleteGeneralSetting(general_setting_ids) {
        const deleted = await this.settingRepo.deleteGeneralSettingByIds(general_setting_ids);
        return deleted;
    }
    async checkGSExistAndThrow(params) {
        const resolutions = await this.settingRepo.findGeneralSetting(params);
        if (resolutions.length > 0 &&
            resolutions[0].general_setting_id !== params.general_setting_id) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.GENERAL_SETTING_EXISTED));
        }
    }
};
GeneralSettingService = __decorate([
    __param(0, common_1.Inject(setting_repository_1.SettingRepository)),
    __metadata("design:paramtypes", [setting_repository_1.SettingRepository])
], GeneralSettingService);
exports.GeneralSettingService = GeneralSettingService;
//# sourceMappingURL=index.js.map