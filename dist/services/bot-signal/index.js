"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotSignalService = void 0;
const common_1 = require("@nestjs/common");
const bot_signal_types_1 = require("../../domains/bot/bot-signal.types");
const bot_signal_repository_1 = require("../../repositories/bot-signal.repository");
const base_1 = require("../base");
let BotSignalService = class BotSignalService extends base_1.BaseService {
    constructor(botSignalRepository) {
        super(botSignalRepository);
        this.botSignalRepository = botSignalRepository;
    }
    async saveSignal(params) {
        const signal = await this.botSignalRepository.findOne({
            signal_id: params.signal_id,
        });
        if (signal) {
            return false;
        }
        const newSignal = {
            id: params.signal_id,
            exchange: params.exchange,
            symbol: params.symbol,
            name: params.name,
            resolution: params.resolution,
            signal_id: params.signal_id,
            type: params.type,
            time: params.time,
            image_url: params.image_url,
            metadata: params.metadata,
        };
        const saved = await this.botSignalRepository.save(newSignal);
        return saved;
    }
};
BotSignalService = __decorate([
    __param(0, common_1.Inject(bot_signal_repository_1.BotSignalRepository)),
    __metadata("design:paramtypes", [bot_signal_repository_1.BotSignalRepository])
], BotSignalService);
exports.BotSignalService = BotSignalService;
//# sourceMappingURL=index.js.map