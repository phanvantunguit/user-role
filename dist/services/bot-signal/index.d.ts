import { BotSignalDomain, QueryBotSignal, RawBotSignal } from 'src/domains/bot/bot-signal.types';
import { BotSignalRepository } from 'src/repositories/bot-signal.repository';
import { BaseService } from '../base';
export declare class BotSignalService extends BaseService<QueryBotSignal, RawBotSignal> implements BotSignalDomain {
    private botSignalRepository;
    constructor(botSignalRepository: BotSignalRepository);
    saveSignal(params: {
        exchange: 'BINANCE';
        symbol: 'LTCUSDT';
        name: 'botBoxCandle';
        resolution: '5m';
        signal_id: 'fad731ed_1673254427814';
        type: 'BUY';
        time: 1671527700000;
        image_url: 'https://static.cextrading.io/images/Bot_Box/Bot_Box_LTCUSDT_5m_1671527700000.png';
        metadata: {
            idSignal: 'fad731ed_1673254427814';
            positionSide: 'LONG';
            side: 'BUY';
        };
        created_at: 1673254434062;
    }): Promise<any>;
}
