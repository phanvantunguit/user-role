import { CreateTokenUUID, RawVerifyToken, VerifyTokenDomain } from 'src/domains/user';
import { UserRepository } from 'src/repositories/user.repository';
export declare class VerifyTokenService implements VerifyTokenDomain {
    private userRepo;
    constructor(userRepo: UserRepository);
    createTokenUUID(params: CreateTokenUUID, metadata?: any): Promise<RawVerifyToken>;
}
