"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const user_1 = require("../../domains/user");
const mail_1 = require("../../resources/mail");
const config_1 = require("../../config");
const user_repository_1 = require("../../repositories/user.repository");
const hashUtil = require("../../utils/hash.util");
const handle_error_util_1 = require("../../utils/handle-error.util");
const verify_token_1 = require("../verify-token");
const format_data_util_1 = require("../../utils/format-data.util");
const uaParser = require("ua-parser-js");
const session_1 = require("../session");
const uuid_1 = require("uuid");
const merchant_repository_1 = require("../../repositories/merchant.repository");
const authorization_1 = require("../../const/authorization");
const app_setting_1 = require("../../const/app-setting");
let UserService = class UserService {
    constructor(mailResource, verifyTokenService, userRepo, sessionService, merchantRepository) {
        this.mailResource = mailResource;
        this.verifyTokenService = verifyTokenService;
        this.userRepo = userRepo;
        this.sessionService = sessionService;
        this.merchantRepository = merchantRepository;
    }
    async getProfile(params, merchantCode) {
        const findUser = await this.userRepo.findOneUser({ id: params.id });
        if (merchantCode) {
            if (findUser.merchant_code !== merchantCode) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.ACCOUNT_INVALID));
            }
        }
        else {
            const merchant = await this.merchantRepository.findOne({
                code: findUser.merchant_code,
            });
            if (merchant && merchant.config.domain_type === authorization_1.MERCHANT_TYPE.OTHERS) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.ACCOUNT_INVALID));
            }
        }
        const user = await this.userRepo.findOneUserView(params);
        const userMerchant = await this.userRepo.findOneUser(params);
        const roles = user.roles.filter((r) => {
            if (r.expires_at) {
                if (Number(r.expires_at) <= Date.now()) {
                    return false;
                }
            }
            return true;
        });
        return Object.assign(Object.assign({}, user), { roles, merchant_code: userMerchant.merchant_code });
    }
    async getUserPagination(params) {
        const { page, size, keyword, is_admin, role_ids, roles, bots, merchant_code, } = params;
        if (is_admin) {
            return await this.userRepo.findUserViewPaging({
                page,
                size,
                keyword,
                is_admin,
            });
        }
        else {
            return await this.userRepo.findUserPaging({
                page,
                size,
                keyword,
                roles: roles ? roles.split(',') : [],
                bots: bots ? bots.split(',') : [],
                merchant_code,
            });
        }
    }
    async getUser(params) {
        const user = await this.userRepo.findOneUserView(params);
        return user;
    }
    async listUser(params) {
        const user = await this.userRepo.findUser(params);
        return user;
    }
    async updateInfo(params, isAdmin) {
        const findUser = await this.userRepo.findOneUser({ id: params.id });
        if (!findUser) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.USER_NOT_FOUND));
        }
        if (params.password) {
            const passIsCorrect = await hashUtil.comparePassword(params.old_password, findUser.password);
            if (!passIsCorrect) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.PASSWORD_INCORRECT));
            }
        }
        delete params.old_password;
        if (params.username) {
            const check = await this.checkUsername(params.username, findUser.id);
            if (!check) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNPROCESSABLE_ENTITY }, const_1.ERROR_CODE.USERNAME_EXISTED));
            }
        }
        const userUpdated = await this.userRepo.saveUser(params);
        delete userUpdated.password;
        return userUpdated;
    }
    async sendEmailForgotPassword(params) {
        const email = params.email.toLocaleLowerCase();
        const findUser = await this.userRepo.findOneUser({
            email,
            merchant_code: params.merchant_code,
        });
        if (!findUser) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.EMAIL_NOT_FOUND));
        }
        await this.userRepo.deleteToken({
            user_id: findUser.id,
            type: const_1.VERIFY_TOKEN_TYPE.RESET_PASSWORD,
        });
        const verifyToken = await this.verifyTokenService.createTokenUUID({
            user_id: findUser.id,
            expires_at: Date.now() + Number(config_1.APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * const_1.SECOND,
            type: const_1.VERIFY_TOKEN_TYPE.RESET_PASSWORD,
        });
        if (verifyToken) {
            const merchant = await this.merchantRepository.findOne({
                code: findUser.merchant_code,
            });
            let link = merchant ? merchant.domain : '';
            let from_email;
            let from_name;
            let logo_url;
            let header_url;
            let twitter_url;
            let youtube_url;
            let facebook_url;
            let telegram_url;
            let company_name;
            if (merchant && merchant.config) {
                if (merchant.config['verified_sender'] &&
                    merchant.config['email_sender']) {
                    from_email = merchant.config['email_sender']['from_email'];
                    from_name = merchant.config['email_sender']['from_name'];
                }
                if (merchant.config['social_media']) {
                    twitter_url = merchant.config['social_media']['twitter_url'];
                    youtube_url = merchant.config['social_media']['youtube_url'];
                    facebook_url = merchant.config['social_media']['facebook_url'];
                    telegram_url = merchant.config['social_media']['telegram_url'];
                }
                logo_url = merchant.config['email_logo_url'];
                header_url = merchant.config['email_banner_url'];
                if (merchant.config.domain_type === authorization_1.MERCHANT_TYPE.OTHERS) {
                    company_name = merchant.name;
                    from_email = from_email || config_1.APP_CONFIG.SENDGRID_SENDER_ALGO_EMAIL;
                    from_name = from_name || config_1.APP_CONFIG.SENDGRID_SENDER_ALGO_NAME;
                    logo_url = logo_url || config_1.APP_CONFIG.ALGO_LOGO_EMAIL;
                    header_url = header_url || config_1.APP_CONFIG.ALGO_BANNER_EMAIL;
                }
            }
            const resultSendMail = await this.mailResource.sendEmailForgotPassword(params.email, verifyToken.token, verifyToken.expires_at, link, from_email, from_name, logo_url, header_url, twitter_url, youtube_url, facebook_url, telegram_url, company_name, `${findUser.first_name || ''} ${findUser.last_name || ''}`);
            if (resultSendMail) {
                return resultSendMail;
            }
        }
        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNPROCESSABLE_ENTITY }, const_1.ERROR_CODE.SEND_EMAIL_FAILED));
    }
    async resetPassword(params) {
        const { token, password } = params;
        const verifyToken = await this.userRepo.findOneToken({
            token,
        });
        if (verifyToken) {
            if (verifyToken.expires_at < Date.now()) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.GONE }, const_1.ERROR_CODE.SESSION_VERIFY_EMAIL_EXPIRED));
            }
            const findUser = await this.userRepo.findOneUser({
                id: verifyToken.user_id,
            });
            if (!findUser) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.USER_NOT_FOUND));
            }
            await this.userRepo.saveUser({
                id: findUser.id,
                password,
            });
            await this.userRepo.deleteToken({
                token: verifyToken.token,
            });
            return 'Reset password successfully';
        }
        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNAUTHORIZED }, const_1.ERROR_CODE.SESSION_INVALID));
    }
    async loginUser(params, userAgent, ipAddress, merchantCode) {
        const email = params.email.toLocaleLowerCase();
        const queryUser = {
            email,
        };
        if (merchantCode) {
            queryUser['merchant_code'] = merchantCode;
        }
        const findUser = await this.userRepo.findOneUser(queryUser);
        if (!findUser) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.EMAIL_NOT_FOUND));
        }
        if (!findUser.email_confirmed) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.EMAIL_NOT_VERIFIED));
        }
        if (merchantCode) {
            if (findUser.merchant_code !== merchantCode) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.ACCOUNT_INVALID));
            }
        }
        else {
            const merchant = await this.merchantRepository.findOne({
                code: findUser.merchant_code,
            });
            if (merchant && merchant.config.domain_type === authorization_1.MERCHANT_TYPE.OTHERS) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.ACCOUNT_INVALID));
            }
        }
        const passIsCorrect = await hashUtil.comparePassword(params.password, findUser.password);
        if (!passIsCorrect) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNAUTHORIZED }, const_1.ERROR_CODE.LOGIN_FAILED));
        }
        if (!findUser.active) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.DEACTIVE_ACCOUNT));
        }
        let roles = [];
        if (params.user_type === const_1.USER_TYPE.USER) {
            const userRoles = await this.userRepo.findUserRole({
                user_id: findUser.id,
            });
            roles = userRoles.map((r) => r.role_id);
        }
        else {
            const authRoles = await this.userRepo.findAuthUserRole({
                user_id: findUser.id,
            });
            roles = authRoles.map((r) => r.auth_role_id);
        }
        if (params.user_type === const_1.USER_TYPE.ADMIN && !findUser.is_admin) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.NO_PERMISSION));
        }
        const expiresIn = params.user_type === const_1.USER_TYPE.USER
            ? config_1.APP_CONFIG.TIME_EXPIRED_LOGIN
            : 30 * 60;
        const expiresAt = Date.now() + expiresIn * 1000;
        const tokenId = uuid_1.v4();
        const token = hashUtil.generateToken({
            user_id: findUser.id,
            email: findUser.email,
            merchant_code: findUser.merchant_code,
            iss: 'cextrading',
            roles,
            super_user: findUser.super_user,
            token_id: tokenId,
        }, config_1.APP_CONFIG.TOKEN_PRIVATE_KEY, expiresIn);
        const dataUA = uaParser(userAgent);
        const dataSession = {
            user_id: findUser.id,
            token: token,
            name_device: dataUA.os.name,
            browser: dataUA.browser.name,
            ip_number: ipAddress,
            last_login: Date.now(),
            expires_at: expiresAt,
            enabled: true,
            token_id: tokenId,
        };
        await this.sessionService.save(dataSession);
        return {
            token,
        };
    }
    async verifyEmailUser(token) {
        const verifyToken = await this.userRepo.findOneToken({
            token,
        });
        if (verifyToken) {
            const findUser = await this.userRepo.findOneUser({
                id: verifyToken.user_id,
            });
            if (!findUser) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.USER_NOT_FOUND));
            }
            if (findUser.email_confirmed) {
                return findUser;
            }
            await this.userRepo.saveUser({
                id: findUser.id,
                active: true,
                email_confirmed: true,
            });
            return findUser;
        }
        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNAUTHORIZED }, const_1.ERROR_CODE.SESSION_INVALID));
    }
    async userConfirmEmail(token) {
        const verifyToken = await this.userRepo.findOneToken({
            token,
        });
        if (verifyToken) {
            if (verifyToken.expires_at < Date.now()) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.OK }, const_1.ERROR_CODE.SESSION_VERIFY_EMAIL_EXPIRED));
            }
            const findUser = await this.userRepo.findOneUser({
                id: verifyToken.user_id,
            });
            if (!findUser) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.USER_NOT_FOUND));
            }
            if (verifyToken.type === const_1.VERIFY_TOKEN_TYPE.CHANGE_EMAIL) {
                if (!verifyToken.metadata.verified) {
                    await this.userRepo.saveVerifyToken({
                        id: verifyToken.id,
                        metadata: Object.assign(Object.assign({}, verifyToken.metadata), { verified: true }),
                    });
                }
                const changeEmailTokens = await this.userRepo.findToken({
                    user_id: findUser.id,
                    type: const_1.VERIFY_TOKEN_TYPE.CHANGE_EMAIL,
                });
                const checkConfirm = changeEmailTokens.every((vt) => vt.metadata.verified);
                if (checkConfirm) {
                    await this.userRepo.saveUser({
                        id: findUser.id,
                        email: verifyToken.metadata.email,
                    });
                    return true;
                }
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.OK }, const_1.ERROR_CODE.EMAIL_VERIFIED_NOT_ENOUGH));
            }
        }
        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.OK }, const_1.ERROR_CODE.SESSION_INVALID));
    }
    async createUser(params) {
        const email = params.email.toLocaleLowerCase();
        const findUser = await this.userRepo.findOneUser({ email });
        if (findUser) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.EMAIL_EXISTED));
        }
        const password = format_data_util_1.generatePassword();
        const userCreated = await this.userRepo.saveUser(Object.assign(Object.assign({ created_at: Date.now() }, params), { password,
            email }));
        const verifyToken = await this.verifyTokenService.createTokenUUID({
            user_id: userCreated.id,
            expires_at: Date.now() + Number(config_1.APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * const_1.SECOND,
            type: const_1.VERIFY_TOKEN_TYPE.VERIFY_EMAIL,
        });
        if (verifyToken) {
            await this.userRepo.saveUserRole([
                {
                    user_id: userCreated.id,
                    role_id: const_1.DEFAULT_ROLE.id,
                    description: 'Role default',
                    owner_created: 'system',
                },
            ]);
            const merchant = await this.merchantRepository.findOne({
                code: params.merchant_code,
            });
            let from_email;
            let from_name;
            if (merchant &&
                merchant.config &&
                merchant.config['verified_sender'] &&
                merchant.config['email_sender']) {
                from_email = merchant.config['email_sender']['from_email'];
                from_name = merchant.config['email_sender']['from_name'];
            }
            const resultSendMail = await this.mailResource.sendEmailCreateAccount(params.email, verifyToken.token, password, from_email, from_name, `${userCreated.first_name || ''} ${userCreated.last_name || ''}`);
            if (resultSendMail) {
                await this.userRepo.saveUser({
                    id: userCreated.id,
                    note_updated: 'Send mail verify success',
                });
            }
            else {
                await this.userRepo.saveUser({
                    id: userCreated.id,
                    note_updated: 'Send mail verify failed',
                });
            }
            delete userCreated.password;
            return userCreated;
        }
        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNPROCESSABLE_ENTITY }, const_1.ERROR_CODE.SEND_EMAIL_FAILED));
    }
    async registerUser(params) {
        const email = params.email.toLocaleLowerCase();
        let merchantCode = app_setting_1.APP_SETTING.MERCHANT_CODE_DEFAULT;
        const queryUser = {
            email,
        };
        if (params.merchant_code) {
            const merchant = await this.merchantRepository.findOne({
                code: params.merchant_code,
                status: const_1.MERCHANT_STATUS.ACTIVE,
            });
            if (merchant) {
                if (merchant.config.domain_type === authorization_1.MERCHANT_TYPE.OTHERS) {
                    queryUser['merchant_code'] = [params.merchant_code];
                }
            }
            else {
                params.merchant_code = merchantCode;
            }
        }
        else {
            params.merchant_code = merchantCode;
        }
        if (!queryUser['merchant_code']) {
            const merchants = await this.merchantRepository.findMerchant({
                domain_type: authorization_1.MERCHANT_TYPE.COINMAP,
            });
            queryUser['merchant_code'] = merchants.map((e) => e.code);
        }
        if (queryUser['merchant_code'] && queryUser['merchant_code'].length > 0) {
            const findUser = await this.userRepo.findUser(queryUser);
            if (findUser.length > 0) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.EMAIL_EXISTED));
            }
        }
        const userCreated = await this.userRepo.saveUser(Object.assign(Object.assign({}, params), { created_at: Date.now(), email }));
        const verifyToken = await this.verifyTokenService.createTokenUUID({
            user_id: userCreated.id,
            expires_at: Date.now() + Number(config_1.APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * const_1.SECOND,
            type: const_1.VERIFY_TOKEN_TYPE.VERIFY_EMAIL,
        });
        if (verifyToken) {
            await this.userRepo.saveUserRole([
                {
                    user_id: userCreated.id,
                    role_id: const_1.DEFAULT_ROLE.id,
                    description: 'Role default',
                    owner_created: 'system',
                },
            ]);
            const merchant = await this.merchantRepository.findOne({
                code: params.merchant_code,
            });
            let from_email;
            let from_name;
            let logo_url;
            let header_url;
            let main_content;
            let twitter_url;
            let youtube_url;
            let facebook_url;
            let telegram_url;
            let company_name;
            if (merchant && merchant.config) {
                if (merchant.config['verified_sender'] &&
                    merchant.config['email_sender']) {
                    from_email = merchant.config['email_sender']['from_email'];
                    from_name = merchant.config['email_sender']['from_name'];
                }
                if (merchant.config['social_media']) {
                    twitter_url = merchant.config['social_media']['twitter_url'];
                    youtube_url = merchant.config['social_media']['youtube_url'];
                    facebook_url = merchant.config['social_media']['facebook_url'];
                    telegram_url = merchant.config['social_media']['telegram_url'];
                }
                logo_url = merchant.config['email_logo_url'];
                header_url = merchant.config['email_banner_url'];
                main_content = `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our Trading applications will help you trade safety and in comfort.`;
                if (merchant.config.domain_type === authorization_1.MERCHANT_TYPE.OTHERS) {
                    company_name = merchant.name;
                    from_email = from_email || config_1.APP_CONFIG.SENDGRID_SENDER_ALGO_EMAIL;
                    from_name = from_name || config_1.APP_CONFIG.SENDGRID_SENDER_ALGO_NAME;
                    logo_url = logo_url || config_1.APP_CONFIG.ALGO_LOGO_EMAIL;
                    header_url = header_url || config_1.APP_CONFIG.ALGO_BANNER_EMAIL;
                }
            }
            const resultSendMail = await this.mailResource.sendEmailConfirmAccount(params.email, verifyToken.token, from_email, from_name, logo_url, header_url, main_content, twitter_url, youtube_url, facebook_url, telegram_url, company_name, `${userCreated.first_name || ''} ${userCreated.last_name || ''}`);
            if (resultSendMail) {
                await this.userRepo.saveUser({
                    id: userCreated.id,
                    note_updated: 'Send mail verify success',
                });
            }
            else {
                await this.userRepo.saveUser({
                    id: userCreated.id,
                    note_updated: 'Send mail verify failed',
                });
            }
            return resultSendMail;
        }
        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNPROCESSABLE_ENTITY }, const_1.ERROR_CODE.SEND_EMAIL_FAILED));
    }
    async resendEmailVerify(params) {
        const email = params.email.toLocaleLowerCase();
        const findUser = await this.userRepo.findOneUser({
            email,
            merchant_code: params.merchant_code,
        });
        if (!findUser) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.EMAIL_NOT_FOUND));
        }
        if (findUser.email_confirmed) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.EMAIL_EXISTED));
        }
        const verifyToken = await this.verifyTokenService.createTokenUUID({
            user_id: findUser.id,
            expires_at: Date.now() + Number(config_1.APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * const_1.SECOND,
            type: const_1.VERIFY_TOKEN_TYPE.VERIFY_EMAIL,
        });
        if (verifyToken) {
            const merchant = await this.merchantRepository.findOne({
                code: findUser.merchant_code,
            });
            let from_email;
            let from_name;
            let logo_url;
            let header_url;
            let main_content;
            let twitter_url;
            let youtube_url;
            let facebook_url;
            let telegram_url;
            let company_name;
            if (merchant && merchant.config) {
                if (merchant.config['verified_sender'] &&
                    merchant.config['email_sender']) {
                    from_email = merchant.config['email_sender']['from_email'];
                    from_name = merchant.config['email_sender']['from_name'];
                }
                if (merchant.config['social_media']) {
                    twitter_url = merchant.config['social_media']['twitter_url'];
                    youtube_url = merchant.config['social_media']['youtube_url'];
                    facebook_url = merchant.config['social_media']['facebook_url'];
                    telegram_url = merchant.config['social_media']['telegram_url'];
                }
                logo_url = merchant.config['email_logo_url'];
                header_url = merchant.config['email_banner_url'];
                main_content = `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our Trading applications will help you trade safety and in comfort.`;
                if (merchant.config.domain_type === authorization_1.MERCHANT_TYPE.OTHERS) {
                    company_name = merchant.name;
                    from_email = from_email || config_1.APP_CONFIG.SENDGRID_SENDER_ALGO_EMAIL;
                    from_name = from_name || config_1.APP_CONFIG.SENDGRID_SENDER_ALGO_NAME;
                    logo_url = logo_url || config_1.APP_CONFIG.ALGO_LOGO_EMAIL;
                    header_url = header_url || config_1.APP_CONFIG.ALGO_BANNER_EMAIL;
                }
            }
            const resultSendMail = await this.mailResource.sendEmailConfirmAccount(params.email, verifyToken.token, from_email, from_name, logo_url, header_url, main_content, twitter_url, youtube_url, facebook_url, telegram_url, company_name, `${findUser.first_name || ''} ${findUser.last_name || ''}`);
            if (resultSendMail) {
                await this.userRepo.saveUser({
                    id: findUser.id,
                    note_updated: 'Send mail verify success',
                });
                return resultSendMail;
            }
            else {
                await this.userRepo.saveUser({
                    id: findUser.id,
                    note_updated: 'Send mail verify failed',
                });
            }
        }
        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNPROCESSABLE_ENTITY }, const_1.ERROR_CODE.SEND_EMAIL_FAILED));
    }
    async checkUsername(username, user_id) {
        const user = await this.userRepo.findOneUser({ username });
        if (!user || user.id === user_id) {
            return true;
        }
        return false;
    }
    async checkEmail(email) {
        const user = await this.userRepo.findOneUser({ email });
        if (user) {
            return false;
        }
        return true;
    }
    async changeEmail(params) {
        const { email, user_id, callback_url } = params;
        const [user, checkEmail] = await Promise.all([
            this.userRepo.findOneUser({ id: user_id }),
            this.userRepo.findOneUser({ email }),
        ]);
        if (!user) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.USER_NOT_FOUND));
        }
        if (checkEmail) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.EMAIL_EXISTED));
        }
        await this.userRepo.deleteToken({
            user_id: user.id,
            type: const_1.VERIFY_TOKEN_TYPE.CHANGE_EMAIL,
        });
        const verifyToken1 = await this.verifyTokenService.createTokenUUID({
            user_id: user.id,
            expires_at: Date.now() + Number(config_1.APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * const_1.SECOND,
            type: const_1.VERIFY_TOKEN_TYPE.CHANGE_EMAIL,
        }, { email, verified: false });
        const verifyToken2 = await this.verifyTokenService.createTokenUUID({
            user_id: user.id,
            expires_at: Date.now() + Number(config_1.APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * const_1.SECOND,
            type: const_1.VERIFY_TOKEN_TYPE.CHANGE_EMAIL,
        }, { email, verified: false });
        const merchant = await this.merchantRepository.findOne({
            code: user.merchant_code,
        });
        let from_email;
        let from_name;
        let logo_url;
        let header_url;
        let main_content;
        let twitter_url;
        let youtube_url;
        let facebook_url;
        let telegram_url;
        let company_name;
        if (merchant && merchant.config) {
            if (merchant.config['verified_sender'] &&
                merchant.config['email_sender']) {
                from_email = merchant.config['email_sender']['from_email'];
                from_name = merchant.config['email_sender']['from_name'];
            }
            if (merchant.config['social_media']) {
                twitter_url = merchant.config['social_media']['twitter_url'];
                youtube_url = merchant.config['social_media']['youtube_url'];
                facebook_url = merchant.config['social_media']['facebook_url'];
                telegram_url = merchant.config['social_media']['telegram_url'];
            }
            logo_url = merchant.config['email_logo_url'];
            header_url = merchant.config['email_banner_url'];
            main_content = `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our Trading applications will help you trade safety and in comfort.`;
            if (merchant.config.domain_type === authorization_1.MERCHANT_TYPE.OTHERS) {
                company_name = merchant.name;
                from_email = from_email || config_1.APP_CONFIG.SENDGRID_SENDER_ALGO_EMAIL;
                from_name = from_name || config_1.APP_CONFIG.SENDGRID_SENDER_ALGO_NAME;
                logo_url = logo_url || config_1.APP_CONFIG.ALGO_LOGO_EMAIL;
                header_url = header_url || config_1.APP_CONFIG.ALGO_BANNER_EMAIL;
            }
        }
        const [resultSendMail1, resultSendMail2] = await Promise.all([
            this.mailResource.sendEmailChange(user.email, verifyToken1.token, user.email, email, callback_url, from_email, from_name, logo_url, header_url, twitter_url, youtube_url, facebook_url, telegram_url, company_name, `${user.first_name || ''} ${user.last_name || ''}`),
            this.mailResource.sendEmailChange(email, verifyToken2.token, user.email, email, callback_url, from_email, from_name, logo_url, header_url, twitter_url, youtube_url, facebook_url, telegram_url, company_name, `${user.first_name || ''} ${user.last_name || ''}`),
        ]);
        if (resultSendMail1 && resultSendMail2) {
            await this.userRepo.saveUser({
                id: user.id,
                note_updated: 'Send mail change email success',
            });
            return true;
        }
        else {
            await this.userRepo.saveUser({
                id: user.id,
                note_updated: 'Send mail change email failed',
            });
        }
        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNPROCESSABLE_ENTITY }, const_1.ERROR_CODE.SEND_EMAIL_FAILED));
    }
    async changeEmailVerifyCode(params) {
        const { user_id, code } = params;
        const verifyToken = await this.userRepo.findOneToken({
            user_id,
            type: const_1.VERIFY_TOKEN_TYPE.CHANGE_EMAIL,
        });
        if (verifyToken.type === const_1.VERIFY_TOKEN_TYPE.CHANGE_EMAIL) {
            if (code === verifyToken.metadata.code) {
                await this.userRepo.saveUser({
                    id: user_id,
                    email: verifyToken.metadata.email,
                });
                return true;
            }
            else {
                return false;
            }
        }
        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNAUTHORIZED }, const_1.ERROR_CODE.SESSION_INVALID));
    }
    async checkPassword(user_id, password) {
        const user = await this.userRepo.findOneUser({ id: user_id });
        if (!user) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.USER_NOT_FOUND));
        }
        const passIsCorrect = await hashUtil.comparePassword(password, user.password);
        return passIsCorrect;
    }
};
UserService = __decorate([
    __param(0, common_1.Inject(mail_1.MailResource)),
    __param(4, common_1.Inject(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [mail_1.MailResource,
        verify_token_1.VerifyTokenService,
        user_repository_1.UserRepository,
        session_1.SessionService,
        merchant_repository_1.MerchantRepository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=index.js.map