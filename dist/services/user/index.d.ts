import { UserRegister, UserDomain, UserLogin, UserLoginResponse, EmailSend, UserResetPassword, UserUpdateInfo, UserCreate, RawUser, QueryUser, QueryUserPagination, RawUserView, ChangeEmail, ChangeEmailVerify } from 'src/domains/user';
import { MailResource } from 'src/resources/mail';
import { UserRepository } from 'src/repositories/user.repository';
import { VerifyTokenService } from 'src/services/verify-token';
import { SessionService } from '../session';
import { MerchantRepository } from 'src/repositories/merchant.repository';
export declare class UserService implements UserDomain {
    private mailResource;
    private verifyTokenService;
    private userRepo;
    private sessionService;
    private merchantRepository;
    constructor(mailResource: MailResource, verifyTokenService: VerifyTokenService, userRepo: UserRepository, sessionService: SessionService, merchantRepository: MerchantRepository);
    getProfile(params: QueryUser, merchantCode?: string): Promise<any>;
    getUserPagination(params: QueryUserPagination): Promise<any>;
    getUser(params: QueryUser): Promise<RawUserView>;
    listUser(params: {
        keyword?: string;
        is_admin?: boolean;
    }): Promise<RawUser[]>;
    updateInfo(params: UserUpdateInfo, isAdmin: boolean): Promise<any>;
    sendEmailForgotPassword(params: EmailSend): Promise<any>;
    resetPassword(params: UserResetPassword): Promise<any>;
    loginUser(params: UserLogin, userAgent: string, ipAddress: string, merchantCode?: string): Promise<UserLoginResponse>;
    verifyEmailUser(token: string): Promise<RawUser>;
    userConfirmEmail(token: string): Promise<boolean>;
    createUser(params: UserCreate): Promise<any>;
    registerUser(params: UserRegister): Promise<any>;
    resendEmailVerify(params: EmailSend): Promise<any>;
    checkUsername(username: string, user_id?: string): Promise<boolean>;
    checkEmail(email: string): Promise<boolean>;
    changeEmail(params: ChangeEmail): Promise<boolean>;
    changeEmailVerifyCode(params: ChangeEmailVerify): Promise<boolean>;
    checkPassword(user_id: string, password: string): Promise<boolean>;
}
