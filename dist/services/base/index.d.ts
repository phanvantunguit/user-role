import { BaseDomain } from 'src/domains/base/types';
import { IBaseRepository } from 'src/repositories/base.repository/types';
export declare class BaseService<Query, Raw> implements BaseDomain<Query, Raw> {
    private repository;
    constructor(repository: IBaseRepository<Query, Raw>);
    domain_name: string;
    get(params: Query): Promise<Raw>;
    list(params: Query): Promise<Raw[]>;
    save(params: Raw): Promise<Raw>;
    delete(params: Query): Promise<any>;
    checkNotFoundAndThrow(params: Query): Promise<Raw>;
    checkExistAndThrow(params: Query): Promise<void>;
}
