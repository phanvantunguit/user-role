"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const types_1 = require("../../domains/base/types");
const types_2 = require("../../repositories/base.repository/types");
const handle_error_util_1 = require("../../utils/handle-error.util");
class BaseService {
    constructor(repository) {
        this.domain_name = 'Data';
        this.repository = repository;
    }
    async get(params) {
        const data = await this.repository.findOne(params);
        return data;
    }
    async list(params) {
        return this.repository.find(params);
    }
    async save(params) {
        return this.repository.save(params);
    }
    async delete(params) {
        await this.checkNotFoundAndThrow(params);
        return this.repository.delete(params);
    }
    async checkNotFoundAndThrow(params) {
        const data = await this.repository.findOne(params);
        if (!data) {
            handle_error_util_1.throwError({
                status: common_1.HttpStatus.NOT_FOUND,
                message: `${this.domain_name} not found`,
                error_code: const_1.ERROR_CODE.NOT_FOUND.error_code,
            });
        }
        return data;
    }
    async checkExistAndThrow(params) {
        const data = await this.repository.findOne(params);
        if (data) {
            handle_error_util_1.throwError({
                status: common_1.HttpStatus.BAD_REQUEST,
                message: `${this.domain_name} was existed`,
                error_code: const_1.ERROR_CODE.RESOURCES_EXISTED.error_code,
            });
        }
    }
}
exports.BaseService = BaseService;
//# sourceMappingURL=index.js.map