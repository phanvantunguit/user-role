import { PackageDomain, QueryPackage, RawPackage } from 'src/domains/setting/package.types';
import { PackageRepository } from 'src/repositories/package.ropository';
import { BaseService } from '../base';
export declare class PackageService extends BaseService<QueryPackage, RawPackage> implements PackageDomain {
    private packageRepository;
    constructor(packageRepository: PackageRepository);
}
