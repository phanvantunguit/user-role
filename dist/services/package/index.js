"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PackageService = void 0;
const common_1 = require("@nestjs/common");
const package_types_1 = require("../../domains/setting/package.types");
const package_ropository_1 = require("../../repositories/package.ropository");
const base_1 = require("../base");
let PackageService = class PackageService extends base_1.BaseService {
    constructor(packageRepository) {
        super(packageRepository);
        this.packageRepository = packageRepository;
    }
};
PackageService = __decorate([
    __param(0, common_1.Inject(package_ropository_1.PackageRepository)),
    __metadata("design:paramtypes", [package_ropository_1.PackageRepository])
], PackageService);
exports.PackageService = PackageService;
//# sourceMappingURL=index.js.map