import { QueryResolution, RawResolution, Resolution, ResolutionDomain, ResolutionUpdate } from 'src/domains/setting';
import { SettingRepository } from 'src/repositories/setting.repository';
export declare class ResolutionService implements ResolutionDomain {
    private settingRepo;
    constructor(settingRepo: SettingRepository);
    updateResolution(params: ResolutionUpdate): Promise<RawResolution>;
    getResolution(params: QueryResolution): Promise<RawResolution>;
    listResolution(): Promise<RawResolution[]>;
    createResolution(params: Resolution[]): Promise<RawResolution[]>;
    deleteResolution(ids: string[]): Promise<any>;
}
