"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionLogService = void 0;
const common_1 = require("@nestjs/common");
const event_emitter_1 = require("@nestjs/event-emitter");
const transaction_log_types_1 = require("../../domains/transaction/transaction-log.types");
const transaction_log_repository_1 = require("../../repositories/transaction-log.repository");
const base_1 = require("../base");
let TransactionLogService = class TransactionLogService extends base_1.BaseService {
    constructor(transactionLogRepository, eventEmitter) {
        super(transactionLogRepository);
        this.transactionLogRepository = transactionLogRepository;
        this.eventEmitter = eventEmitter;
    }
    async save(params) {
        const transLog = await this.transactionLogRepository.save(params);
        this.eventEmitter.emit(transLog.transaction_event, transLog);
        return transLog;
    }
    async saveNotSendEvent(params) {
        const transLog = await this.transactionLogRepository.save(params);
        return transLog;
    }
};
TransactionLogService = __decorate([
    __param(0, common_1.Inject(transaction_log_repository_1.TransactionLogRepository)),
    __metadata("design:paramtypes", [transaction_log_repository_1.TransactionLogRepository,
        event_emitter_1.EventEmitter2])
], TransactionLogService);
exports.TransactionLogService = TransactionLogService;
//# sourceMappingURL=index.js.map