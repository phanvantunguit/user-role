import { EventEmitter2 } from '@nestjs/event-emitter';
import { QueryTransactionLog, RawTransactionLog, TransactionLogDomain } from 'src/domains/transaction/transaction-log.types';
import { TransactionLogRepository } from 'src/repositories/transaction-log.repository';
import { BaseService } from '../base';
export declare class TransactionLogService extends BaseService<QueryTransactionLog, RawTransactionLog> implements TransactionLogDomain {
    private transactionLogRepository;
    private eventEmitter;
    constructor(transactionLogRepository: TransactionLogRepository, eventEmitter: EventEmitter2);
    save(params: RawTransactionLog): Promise<RawTransactionLog>;
    saveNotSendEvent(params: RawTransactionLog): Promise<RawTransactionLog>;
}
