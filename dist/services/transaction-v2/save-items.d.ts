import { PACKAGE_TYPE } from 'src/const';
import { ORDER_CATEGORY } from 'src/const/transaction';
import { PermissionRepository } from 'src/repositories/permission.repository';
import { UserRepository } from 'src/repositories/user.repository';
export declare class SaveItemService {
    private permissionRepository;
    private userRepository;
    constructor(permissionRepository: PermissionRepository, userRepository: UserRepository);
    execute(param: {
        items: {
            id: string;
            name: string;
            price: string;
            quantity: number;
            type: PACKAGE_TYPE;
            category: ORDER_CATEGORY;
            discount_rate: number;
            discount_amount: number;
            balance?: number;
        }[];
        order_id: string;
        user_id: string;
        owner_created: string;
    }, queryRunner: any): Promise<import("../../repositories/user.repository/user-asset-log.entity").UserAssetLogEntity[]>;
}
