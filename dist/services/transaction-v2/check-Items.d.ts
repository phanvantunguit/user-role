import { PACKAGE_TYPE } from 'src/const';
import { ORDER_CATEGORY } from 'src/const/transaction';
import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { BotRepository } from 'src/repositories/bot.repository';
import { MerchantAdditionalDataRepository } from 'src/repositories/merchant-additional-data.repository';
import { MerchantCommissionRepository } from 'src/repositories/merchant-commission.repository';
import { MerchantRepository } from 'src/repositories/merchant.repository';
import { PackageRepository } from 'src/repositories/package.ropository';
import { PermissionRepository } from 'src/repositories/permission.repository';
import { CreateBalanceAndFeeDataHandler } from 'src/usecases/bot/user-create-balance-and-fee';
export declare class CheckItemService {
    private permissionRepository;
    private botRepository;
    private packageRepository;
    private botTradingRepository;
    private merchantCommissionRepository;
    private merchantRepository;
    private merchantAdditionalDataRepository;
    private createBalanceAndFeeDataHandler;
    constructor(permissionRepository: PermissionRepository, botRepository: BotRepository, packageRepository: PackageRepository, botTradingRepository: BotTradingRepository, merchantCommissionRepository: MerchantCommissionRepository, merchantRepository: MerchantRepository, merchantAdditionalDataRepository: MerchantAdditionalDataRepository, createBalanceAndFeeDataHandler: CreateBalanceAndFeeDataHandler);
    execute(param: {
        merchant_code: string;
        items: {
            id: string;
            quantity: number;
            type: PACKAGE_TYPE;
            category: ORDER_CATEGORY;
            balance?: number;
        }[];
        amount: string;
    }): Promise<{
        items: {
            id: string;
            name: string;
            price: string;
            quantity: number;
            type: PACKAGE_TYPE;
            category: ORDER_CATEGORY;
            discount_rate: number;
            discount_amount: number;
            commission_rate: number;
            commission_cash: number;
            balance?: number;
        }[];
        commission_cash: number;
    }>;
}
