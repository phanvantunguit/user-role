"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckItemService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const authorization_1 = require("../../const/authorization");
const transaction_1 = require("../../const/transaction");
const types_1 = require("../../domains/merchant/types");
const bot_trading_repository_1 = require("../../repositories/bot-trading.repository");
const bot_repository_1 = require("../../repositories/bot.repository");
const merchant_additional_data_repository_1 = require("../../repositories/merchant-additional-data.repository");
const merchant_commission_repository_1 = require("../../repositories/merchant-commission.repository");
const merchant_repository_1 = require("../../repositories/merchant.repository");
const package_ropository_1 = require("../../repositories/package.ropository");
const permission_repository_1 = require("../../repositories/permission.repository");
const user_create_balance_and_fee_1 = require("../../usecases/bot/user-create-balance-and-fee");
const handle_error_util_1 = require("../../utils/handle-error.util");
let CheckItemService = class CheckItemService {
    constructor(permissionRepository, botRepository, packageRepository, botTradingRepository, merchantCommissionRepository, merchantRepository, merchantAdditionalDataRepository, createBalanceAndFeeDataHandler) {
        this.permissionRepository = permissionRepository;
        this.botRepository = botRepository;
        this.packageRepository = packageRepository;
        this.botTradingRepository = botTradingRepository;
        this.merchantCommissionRepository = merchantCommissionRepository;
        this.merchantRepository = merchantRepository;
        this.merchantAdditionalDataRepository = merchantAdditionalDataRepository;
        this.createBalanceAndFeeDataHandler = createBalanceAndFeeDataHandler;
    }
    async execute(param) {
        const merchant = await this.merchantRepository.findOne({
            code: param.merchant_code,
        });
        const sbotIds = [];
        const roleIds = [];
        const tbotIds = [];
        const items = [];
        for (let item of param.items) {
            switch (item.category) {
                case transaction_1.ORDER_CATEGORY.SBOT:
                    sbotIds.push(item.id);
                    break;
                case transaction_1.ORDER_CATEGORY.PKG:
                    roleIds.push(item.id);
                    break;
                case transaction_1.ORDER_CATEGORY.TBOT:
                    tbotIds.push(item.id);
                    break;
            }
        }
        let totalCommission = 0;
        let totalAmount = 0;
        const roles = roleIds.length === 0
            ? []
            : await this.permissionRepository.findRoleByIds(roleIds);
        const sbots = sbotIds.length === 0 ? [] : await this.botRepository.findByIds(sbotIds);
        const tbots = tbotIds.length === 0
            ? []
            : await this.botTradingRepository.findByIds(tbotIds);
        let packages;
        if (merchant && merchant.config.domain_type === authorization_1.MERCHANT_TYPE.OTHERS) {
        }
        else {
            packages = await this.packageRepository.find({
                status: true,
            });
        }
        for (let r of roles) {
            if (merchant && merchant.config.domain_type === authorization_1.MERCHANT_TYPE.OTHERS) {
                const additional = await this.merchantAdditionalDataRepository.list({
                    merchant_id: merchant.id,
                    type: types_1.ADDITIONAL_DATA_TYPE.PKG_PERIOD,
                    status: types_1.MERCHANT_ADDITIONAL_DATA_STATUS.ON,
                });
                packages = additional.map((e) => {
                    var _a, _b;
                    if ((_b = (_a = e === null || e === void 0 ? void 0 : e.data) === null || _a === void 0 ? void 0 : _a.translation) === null || _b === void 0 ? void 0 : _b.en) {
                        return e.data.translation.en;
                    }
                    else {
                        return {};
                    }
                });
            }
            const roleOrder = param.items.find((i) => i.id === r.id);
            if (r.status === const_1.ROLE_STATUS.OPEN && roleOrder) {
                let commissionRate = 0;
                if (merchant) {
                    const merchantCommission = await this.merchantCommissionRepository.findOne({
                        merchant_id: merchant.id,
                        asset_id: r.id,
                    });
                    if (merchantCommission && merchantCommission.commission) {
                        commissionRate = Number(merchantCommission.commission);
                    }
                }
                const packageTime = packages.find((p) => p.type === roleOrder.type && p.quantity == roleOrder.quantity);
                let discountRate = 0;
                let discountAmount = 0;
                if (packageTime) {
                    discountRate = Number(packageTime.discount_rate) || 0;
                    discountAmount = Number(packageTime.discount_amount) || 0;
                }
                const price = Number(r.price) || 0;
                const quantity = roleOrder.quantity || 0;
                const sellAmount = price * quantity - price * quantity * discountRate - discountAmount;
                totalAmount += sellAmount;
                totalCommission += sellAmount * commissionRate;
                items.push({
                    id: r.id,
                    name: r.role_name,
                    price: r.price,
                    quantity: roleOrder.quantity,
                    type: roleOrder.type,
                    category: transaction_1.ORDER_CATEGORY.PKG,
                    discount_rate: discountRate,
                    discount_amount: discountAmount,
                    commission_rate: commissionRate,
                    commission_cash: sellAmount * commissionRate,
                });
            }
            else {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ROLE_INVALID));
            }
        }
        for (let b of sbots) {
            if (merchant && merchant.config.domain_type === authorization_1.MERCHANT_TYPE.OTHERS) {
                const additional = await this.merchantAdditionalDataRepository.list({
                    merchant_id: merchant.id,
                    type: types_1.ADDITIONAL_DATA_TYPE.SBOT_PERIOD,
                    status: types_1.MERCHANT_ADDITIONAL_DATA_STATUS.ON,
                });
                packages = additional.map((e) => {
                    var _a, _b;
                    if ((_b = (_a = e === null || e === void 0 ? void 0 : e.data) === null || _a === void 0 ? void 0 : _a.translation) === null || _b === void 0 ? void 0 : _b.en) {
                        return e.data.translation.en;
                    }
                    else {
                        return {};
                    }
                });
            }
            const botOrder = param.items.find((i) => i.id === b.id);
            if (b.status === const_1.BOT_STATUS.OPEN && botOrder) {
                let commissionRate = 0;
                if (merchant) {
                    const merchantCommission = await this.merchantCommissionRepository.findOne({
                        merchant_id: merchant.id,
                        asset_id: b.id,
                    });
                    if (merchantCommission && merchantCommission.commission) {
                        commissionRate = Number(merchantCommission.commission);
                    }
                }
                const packageTime = packages.find((p) => p.type === botOrder.type && p.quantity == botOrder.quantity);
                let discountRate = 0;
                let discountAmount = 0;
                if (packageTime) {
                    discountRate = Number(packageTime.discount_rate) || 0;
                    discountAmount = Number(packageTime.discount_amount) || 0;
                }
                const price = Number(b.price) || 0;
                const quantity = botOrder.quantity || 0;
                const sellAmount = price * quantity - price * quantity * discountRate - discountAmount;
                totalAmount += sellAmount;
                totalCommission += sellAmount * commissionRate;
                items.push({
                    id: b.id,
                    name: b.name,
                    price: b.price,
                    quantity: botOrder.quantity,
                    type: botOrder.type,
                    category: transaction_1.ORDER_CATEGORY.SBOT,
                    discount_rate: discountRate,
                    discount_amount: discountAmount,
                    commission_rate: commissionRate,
                    commission_cash: sellAmount * commissionRate,
                });
            }
            else {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_INVALID));
            }
        }
        for (let b of tbots) {
            if (merchant && merchant.config.domain_type === authorization_1.MERCHANT_TYPE.OTHERS) {
                const additional = await this.merchantAdditionalDataRepository.list({
                    merchant_id: merchant.id,
                    type: types_1.ADDITIONAL_DATA_TYPE.TBOT_PERIOD,
                    status: types_1.MERCHANT_ADDITIONAL_DATA_STATUS.ON,
                });
                packages = additional.map((e) => {
                    var _a, _b;
                    if ((_b = (_a = e === null || e === void 0 ? void 0 : e.data) === null || _a === void 0 ? void 0 : _a.translation) === null || _b === void 0 ? void 0 : _b.en) {
                        return e.data.translation.en;
                    }
                    else {
                        return {};
                    }
                });
            }
            const botOrder = param.items.find((i) => i.id === b.id);
            if (b.status === const_1.BOT_STATUS.OPEN && botOrder) {
                let commissionRate = 0;
                if (merchant) {
                    const merchantCommission = await this.merchantCommissionRepository.findOne({
                        merchant_id: merchant.id,
                        asset_id: b.id,
                    });
                    if (merchantCommission && merchantCommission.commission) {
                        commissionRate = Number(merchantCommission.commission);
                    }
                }
                const packageTime = packages.find((p) => p.type === botOrder.type && p.quantity == botOrder.quantity);
                let discountRate = 0;
                let discountAmount = 0;
                if (packageTime) {
                    discountRate = Number(packageTime.discount_rate) || 0;
                    discountAmount = Number(packageTime.discount_amount) || 0;
                }
                const getFee = await this.createBalanceAndFeeDataHandler.execute({ balance: Number(botOrder.balance), bot_id: b.id });
                const price = Number(getFee.total_price);
                const quantity = botOrder.quantity || 0;
                const sellAmount = price * quantity - price * quantity * discountRate - discountAmount;
                totalAmount += sellAmount;
                totalCommission += sellAmount * commissionRate;
                items.push({
                    id: b.id,
                    name: b.name,
                    price: price,
                    quantity: botOrder.quantity,
                    type: botOrder.type,
                    category: transaction_1.ORDER_CATEGORY.TBOT,
                    discount_rate: discountRate,
                    discount_amount: discountAmount,
                    commission_rate: commissionRate,
                    commission_cash: sellAmount * commissionRate,
                    balance: botOrder.balance
                });
            }
            else {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_INVALID));
            }
        }
        if (Number(param.amount) !== Number(totalAmount.toFixed(2))) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.AMOUNT_INVALID));
        }
        return { items, commission_cash: totalCommission };
    }
};
CheckItemService = __decorate([
    __param(0, common_1.Inject(permission_repository_1.PermissionRepository)),
    __param(1, common_1.Inject(bot_repository_1.BotRepository)),
    __param(2, common_1.Inject(package_ropository_1.PackageRepository)),
    __param(3, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(4, common_1.Inject(merchant_commission_repository_1.MerchantCommissionRepository)),
    __param(5, common_1.Inject(merchant_repository_1.MerchantRepository)),
    __param(6, common_1.Inject(merchant_additional_data_repository_1.MerchantAdditionalDataRepository)),
    __param(7, common_1.Inject(user_create_balance_and_fee_1.CreateBalanceAndFeeDataHandler)),
    __metadata("design:paramtypes", [permission_repository_1.PermissionRepository,
        bot_repository_1.BotRepository,
        package_ropository_1.PackageRepository,
        bot_trading_repository_1.BotTradingRepository,
        merchant_commission_repository_1.MerchantCommissionRepository,
        merchant_repository_1.MerchantRepository,
        merchant_additional_data_repository_1.MerchantAdditionalDataRepository,
        user_create_balance_and_fee_1.CreateBalanceAndFeeDataHandler])
], CheckItemService);
exports.CheckItemService = CheckItemService;
//# sourceMappingURL=check-Items.js.map