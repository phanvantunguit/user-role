"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaveItemService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const transaction_1 = require("../../const/transaction");
const bot_repository_1 = require("../../repositories/bot.repository");
const permission_repository_1 = require("../../repositories/permission.repository");
const user_repository_1 = require("../../repositories/user.repository");
const handle_error_util_1 = require("../../utils/handle-error.util");
let SaveItemService = class SaveItemService {
    constructor(permissionRepository, userRepository) {
        this.permissionRepository = permissionRepository;
        this.userRepository = userRepository;
    }
    async execute(param, queryRunner) {
        const userAssetLogs = param.items.map((item) => {
            const time = new Date();
            if (item.type === const_1.PACKAGE_TYPE.MONTH) {
                time.setMonth(time.getMonth() + item.quantity);
            }
            else {
                time.setDate(time.getDate() + item.quantity);
            }
            const data = {
                order_id: param.order_id,
                user_id: param.user_id,
                owner_created: param.owner_created,
                expires_at: time.valueOf(),
                status: transaction_1.ITEM_STATUS.PROCESSING,
                asset_id: item.id,
                category: item.category,
                package_type: item.type,
                quantity: item.quantity,
                name: item.name,
                price: item.price,
                discount_rate: item.discount_rate,
                discount_amount: item.discount_amount,
                type: transaction_1.TBOT_TYPE.BUY,
            };
            if (item.balance) {
                data['metadata'] = { balance: item.balance };
            }
            return data;
        });
        return this.userRepository.saveUserAssetLog(userAssetLogs, queryRunner);
    }
};
SaveItemService = __decorate([
    __param(0, common_1.Inject(permission_repository_1.PermissionRepository)),
    __param(1, common_1.Inject(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [permission_repository_1.PermissionRepository,
        user_repository_1.UserRepository])
], SaveItemService);
exports.SaveItemService = SaveItemService;
//# sourceMappingURL=save-items.js.map