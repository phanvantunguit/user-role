import { RawSession, Session, SessionDomain } from 'src/domains/user';
import { UserRepository } from 'src/repositories/user.repository';
import { IToken } from 'src/const/authorization';
import { CacheRepository } from 'src/repositories/cache.repository';
import { GeneralSettingRoleService } from '../general-setting-role';
export declare class SessionService implements SessionDomain {
    private userRepository;
    private cacheRepository;
    private generalSettingRoleService;
    constructor(userRepository: UserRepository, cacheRepository: CacheRepository, generalSettingRoleService: GeneralSettingRoleService);
    save(params: Session): Promise<RawSession>;
    verifySession(token: IToken): Promise<boolean>;
}
