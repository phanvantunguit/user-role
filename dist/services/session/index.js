"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SessionService = void 0;
const common_1 = require("@nestjs/common");
const user_1 = require("../../domains/user");
const user_repository_1 = require("../../repositories/user.repository");
const authorization_1 = require("../../const/authorization");
const cache_repository_1 = require("../../repositories/cache.repository");
const config_1 = require("../../config");
const handle_error_util_1 = require("../../utils/handle-error.util");
const const_1 = require("../../const");
const general_setting_role_1 = require("../general-setting-role");
const hash_util_1 = require("../../utils/hash.util");
const hashUtil = require("../../utils/hash.util");
let SessionService = class SessionService {
    constructor(userRepository, cacheRepository, generalSettingRoleService) {
        this.userRepository = userRepository;
        this.cacheRepository = cacheRepository;
        this.generalSettingRoleService = generalSettingRoleService;
    }
    async save(params) {
        const session = await this.userRepository.saveSession(params);
        const encodeData = params.token.split('.')[1];
        let limitTab = 1;
        if (encodeData) {
            const decode = JSON.parse(hash_util_1.decodeBase64(encodeData));
            limitTab = await this.generalSettingRoleService.getMaxLimitTab(decode.roles);
        }
        console.log("limitTab", limitTab);
        const sessions = await this.userRepository.findSessionLogin({ user_id: params.user_id, enabled: true }, limitTab);
        const activeTokens = sessions.map((s) => s.token_id);
        await this.cacheRepository.setKey(params.user_id, JSON.stringify(activeTokens), Number(config_1.APP_CONFIG.TIME_EXPIRED_LOGIN));
        return session;
    }
    async verifySession(token) {
        const cacheString = await this.cacheRepository.getKey(token.user_id);
        const cacheSessions = JSON.parse(cacheString);
        if (!cacheSessions || cacheSessions.length === 0) {
            const limitTab = await this.generalSettingRoleService.getMaxLimitTab(token.roles);
            const sessions = await this.userRepository.findSessionLogin({ user_id: token.user_id, enabled: true }, limitTab);
            const session = sessions.find((s) => s.token_id === token.token_id);
            if (session) {
                return true;
            }
        }
        else {
            const session = cacheSessions.find((t) => t === token.token_id);
            if (session) {
                return true;
            }
        }
        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNAUTHORIZED }, const_1.ERROR_CODE.TOKEN_INVALID));
    }
};
SessionService = __decorate([
    __param(0, common_1.Inject(user_repository_1.UserRepository)),
    __param(1, common_1.Inject(cache_repository_1.CacheRepository)),
    __param(2, common_1.Inject(general_setting_role_1.GeneralSettingRoleService)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        cache_repository_1.CacheRepository,
        general_setting_role_1.GeneralSettingRoleService])
], SessionService);
exports.SessionService = SessionService;
//# sourceMappingURL=index.js.map