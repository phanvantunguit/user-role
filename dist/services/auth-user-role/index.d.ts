import { AuthUserRoleDomain, AuthUserRoleModify } from 'src/domains/user';
import { PermissionRepository } from 'src/repositories/permission.repository';
import { UserRepository } from 'src/repositories/user.repository';
export declare class AuthUserRoleService implements AuthUserRoleDomain {
    private userRepo;
    private permissionRepo;
    constructor(userRepo: UserRepository, permissionRepo: PermissionRepository);
    modifyAuthUserRole(params: AuthUserRoleModify): Promise<any>;
}
