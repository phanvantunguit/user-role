"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthUserRoleService = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const user_1 = require("../../domains/user");
const permission_repository_1 = require("../../repositories/permission.repository");
const user_repository_1 = require("../../repositories/user.repository");
const handle_error_util_1 = require("../../utils/handle-error.util");
let AuthUserRoleService = class AuthUserRoleService {
    constructor(userRepo, permissionRepo) {
        this.userRepo = userRepo;
        this.permissionRepo = permissionRepo;
    }
    async modifyAuthUserRole(params) {
        const { user_id, owner_created, auth_roles } = params;
        const authRoleIds = auth_roles.map((ar) => ar.auth_role_id);
        const authRoles = await this.permissionRepo.findAuthRoleByIds(authRoleIds);
        if (authRoles.length !== authRoleIds.length) {
            const authRoleIdsNotFound = [];
            for (const ar of authRoles) {
                if (!authRoleIds.includes(ar.id)) {
                    authRoleIdsNotFound.push(ar.id);
                }
            }
            handle_error_util_1.throwError({
                status: common_1.HttpStatus.NOT_FOUND,
                error_code: const_1.ERROR_CODE.ROLE_NOT_FOUND.error_code,
                message: authRoleIdsNotFound,
            });
        }
        const currentAuthUserRoles = await this.userRepo.findAuthUserRole({
            user_id,
        });
        const dataModify = [];
        for (const arReq of auth_roles) {
            const aurExistIndex = currentAuthUserRoles.findIndex((aur) => aur.auth_role_id === arReq.auth_role_id);
            if (aurExistIndex > -1) {
                if (currentAuthUserRoles[aurExistIndex].description !== arReq.description) {
                    dataModify.push(Object.assign(Object.assign({}, currentAuthUserRoles[aurExistIndex]), { description: arReq.description, owner_created }));
                }
                currentAuthUserRoles.splice(aurExistIndex, 1);
            }
            else {
                dataModify.push(Object.assign(Object.assign({}, arReq), { user_id,
                    owner_created }));
            }
        }
        const deleteAuthUserRoles = currentAuthUserRoles.map((aur) => aur.id);
        if (deleteAuthUserRoles.length > 0) {
            await this.userRepo.deleteAuthUserRoleByIds(deleteAuthUserRoles);
        }
        const modify = await this.userRepo.saveAuthUserRole(dataModify);
        return modify;
    }
};
AuthUserRoleService = __decorate([
    __param(1, common_1.Inject(permission_repository_1.PermissionRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        permission_repository_1.PermissionRepository])
], AuthUserRoleService);
exports.AuthUserRoleService = AuthUserRoleService;
//# sourceMappingURL=index.js.map