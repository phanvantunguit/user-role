"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRolesAppModule = void 0;
const common_1 = require("@nestjs/common");
const controller_modules_1 = require("./controller.modules");
const repositories_1 = require("./repositories");
const user_get_bot_trading_1 = require("./usecases/bot/user-get-bot-trading");
const user_list_bot_trading_plan_1 = require("./usecases/bot/user-list-bot-trading-plan");
const websocketEvents = require("./websocket");
let UserRolesAppModule = class UserRolesAppModule {
};
UserRolesAppModule = __decorate([
    common_1.Module({
        imports: [
            repositories_1.DatabaseModule,
            controller_modules_1.ApiV1AdminControllerModule,
            controller_modules_1.ApiV1UserControllerModule,
        ],
        providers: [
            ...Object.values(websocketEvents),
            user_list_bot_trading_plan_1.UserListBotTradingPlanHandler,
            user_get_bot_trading_1.UserGetBotTradingHandler,
        ],
    })
], UserRolesAppModule);
exports.UserRolesAppModule = UserRolesAppModule;
//# sourceMappingURL=user-roles-app.module.js.map