"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.emailVerifyAccountCreate = void 0;
exports.emailVerifyAccountCreate = `<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <title>
  </title>
  <!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--<![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Archivo:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,500;0,700;1,400&display=swap" rel="stylesheet">
  <style type="text/css">
    #outlook a {
      padding: 0;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table,
    td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    p {
      display: block;
      margin: 13px 0;
    }

  </style>
  <!--[if mso]>
    <noscript>
    <xml>
    <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml>
    </noscript>
    <![endif]-->
  <!--[if lte mso 11]>
    <style type="text/css">
      .mj-outlook-group-fix { width:100% !important; }
    </style>
    <![endif]-->
  <style type="text/css">
    @media only screen and (min-width:480px) {
      .mj-column-per-100 {
        width: 100% !important;
        max-width: 100%;
      }

      .mj-column-per-30 {
        width: 30% !important;
        max-width: 30%;
      }

      .mj-column-per-70 {
        width: 70% !important;
        max-width: 70%;
      }
    }

  </style>
  <style media="screen and (min-width:480px)">
    .moz-text-html .mj-column-per-100 {
      width: 100% !important;
      max-width: 100%;
    }

    .moz-text-html .mj-column-per-30 {
      width: 30% !important;
      max-width: 30%;
    }

    .moz-text-html .mj-column-per-70 {
      width: 70% !important;
      max-width: 70%;
    }

  </style>
  <style type="text/css">
    @media only screen and (max-width:480px) {
      table.mj-full-width-mobile {
        width: 100% !important;
      }

      td.mj-full-width-mobile {
        width: auto !important;
      }
    }

  </style>
  <style type="text/css">
  </style>
</head>

<body style="word-spacing:normal;">
  <div style="">
    <!-- header.mjml -->
    <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#1D2842" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="background:#1D2842;background-color:#1D2842;margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#1D2842;background-color:#1D2842;width:100%;">
        <tbody>
          <tr>
            <td style="width:600px;">
              <img height="auto" src="https://static.cextrading.io/images/1/header-bg.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="600" />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;font-size:0px;padding:0 40px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><![endif]-->
              <!-- content.mjml -->
              <!--[if mso | IE]><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:520px;" width="520" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
              <div style="margin:0px auto;max-width:520px;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                  <tbody>
                      <tr>
                        <td align="left"
                          style="font-size:0px;padding:0;padding-bottom:20px;word-break:break-word; padding-top: 20px;">
                          <div
                            style="font-family:Archivo;font-size:16px;font-weight:700;line-height:1;text-align:left;color:#000000;">
                            Hello {{fullname}},</div>
                        </td>
                      </tr>
                      <tr>
                      <td align="left"
                        style="font-size:0px;padding:0;padding-bottom:20px;word-break:break-word;">
                        <div
                          style="font-family:Archivo;font-size:16px;font-weight:400;line-height:16px;text-align:left;color:#000000;">Your account has been created.</div>
                      </td>
                    </tr>
                      <tr>
                        <td align="left" style="font-size:0px;padding:0;padding-bottom:20px;word-break:break-word;">
                          <div style="font-family:Archivo;font-size:16px;font-weight:400;line-height:16px;text-align:left;color:#000000;">We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our CEX and DEX Trading applications will help you trade safety and in comfort.</div>
                        </td>
                      </tr>
                      <tr>
                        <td align="left" style="font-size:0px;padding:0;padding-bottom:20px;word-break:break-word;">
                          <div style="font-family:Archivo;font-size:16px;font-weight:400;line-height:16px;text-align:left;color:#000000;">{{password}}</div>
                        </td>
                      </tr>
                      <tr>
                        <td align="left" style="font-size:0px;padding:0;word-break:break-word;">
                          <div style="font-family:Archivo;font-size:16px;font-weight:400;line-height:16px;text-align:left;color:#000000;">To activate your Coinmap account: <a href={{linkVerify}}>click here</a></div>
                        </td>
                      </tr>
                      <tr>
                        <td align="left" style="font-size:0px;padding:0;padding-bottom:20px;padding-top:20px;word-break:break-word;">
                          <div style="font-family:Archivo;font-size:16px;font-weight:400;line-height:16px;text-align:left;color:#000000;">Happy trading with COINMAP,</div>
                        </td>
                      </tr>
                      <tr>
                        <td align="left" style="font-size:0px;padding:0;padding-bottom:20px;word-break:break-word;">
                          <div style="font-family:Archivo;font-size:16px;font-weight:700;line-height:16px;text-align:left;color:#000000;">COINMAP TEAM.</div>
                        </td>
                      </tr>
                  </tbody>
                </table>
              </div>
              <!--[if mso | IE]></td></tr></table></td></tr><![endif]-->
              <!-- footer.mjml -->
              <!--[if mso | IE]><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:520px;" width="520" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
              <div style="margin:0px auto;max-width:520px;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                  <tbody>
                    <tr>
                      <td style="border-top:1px solid #DDDDDD;direction:ltr;font-size:0px;padding:24px 0;text-align:left;">
                        <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="width:520px;" ><![endif]-->
                        <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                          <!--[if mso | IE]><table border="0" cellpadding="0" cellspacing="0" role="presentation" ><tr><td style="vertical-align:top;width:156px;" ><![endif]-->
                          <div class="mj-column-per-30 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:30%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                              <tbody>
                                <tr>
                                  <td style="border-right:1px solid #DDDDDD;vertical-align:top;padding:0;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                      <tbody>
                                        <tr>
                                          <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                              <tbody>
                                                <tr>
                                                  <td style="width:64px;">
                                                    <img height="auto" src="https://static.cextrading.io/images/1/logo.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="64" />
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td><td style="vertical-align:top;width:364px;" ><![endif]-->
                          <div class="mj-column-per-70 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:70%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                              <tbody>
                                <tr>
                                  <td style="vertical-align:top;padding-left:16px;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                      <tbody>
                                        <tr>
                                          <td align="left" style="font-size:0px;padding:0;padding-bottom:5px;word-break:break-word;">
                                            <div style="font-family:Archivo;font-size:11px;line-height:11px;text-align:left;color:#979797;">Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.</div>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="left" style="font-size:0px;padding:0;word-break:break-word;">
                                            <div style="font-family:Archivo;font-size:16px;line-height:1;text-align:left;color:#000000;"><a style="text-decoration: none; margin-right:8px;" href="https://www.facebook.com/CoinmapTrading" target="_blank">
                                                <img padding="0" src="https://static.cextrading.io/images/1/facebook.png"></img>
                                              </a>
                                              <a href="https://twitter.com/CoinmapTrading" style="text-decoration: none; margin-right:8px;" target="_blank">
                                                <img padding="0" src="https://static.cextrading.io/images/1/twitter.png"></img>
                                              </a>
                                              <a href="https://t.me/trading8x" style="text-decoration: none; margin-right:8px;" target="_blank">
                                                <img padding="0" src="https://static.cextrading.io/images/1/telegram.png"></img>
                                              </a>
                                              <a href="https://www.youtube.com/c/8xTRADING" style="text-decoration: none; margin-right:8px;" target="_blank">
                                                <img padding="0" src="https://static.cextrading.io/images/1/youtube.png"></img>
                                              </a>
                                            </div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td></tr></table><![endif]-->
                        </div>
                        <!--[if mso | IE]></td></tr></table><![endif]-->
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--[if mso | IE]></td></tr></table></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><![endif]-->
  </div>
</body>

</html>`;
//# sourceMappingURL=email-verify-account-create.js.map