"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const bodyParser = require("body-parser");
const user_roles_app_module_1 = require("./user-roles-app.module");
const exception_util_1 = require("./utils/exception.util");
const config_1 = require("./config");
const types_1 = require("./config/types");
const websocket_adapter_1 = require("./websocket/websocket.adapter");
const backgroud_app_module_1 = require("./backgroud-app.module");
const bot_trading_job_1 = require("./backgroud-jobs/bot-trading.job");
async function bootstrap() {
    const userRolesApp = await core_1.NestFactory.create(user_roles_app_module_1.UserRolesAppModule);
    userRolesApp.use(bodyParser.json({ limit: '50mb' }));
    userRolesApp.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
    userRolesApp.enableCors({
        exposedHeaders: ['Authorization', 'Refresh-Token']
    });
    userRolesApp.useGlobalPipes(new common_1.ValidationPipe({ whitelist: false, forbidNonWhitelisted: true }));
    userRolesApp.useGlobalFilters(new exception_util_1.ExceptionsUtil());
    if (config_1.APP_CONFIG.ENVIRONMENT !== types_1.ENVIRONMENT.PRODUCTION) {
        const merchantSwaggerConfig = new swagger_1.DocumentBuilder()
            .setTitle('User roles Document')
            .setDescription('This is user roles API document')
            .setVersion('1.0')
            .addBearerAuth()
            .addApiKey({ type: 'apiKey', name: 'Refresh-Token', in: 'header' }, 'Refresh-Token')
            .addServer(config_1.APP_CONFIG.BASE_URL)
            .build();
        const userRolesAppDocument = swagger_1.SwaggerModule.createDocument(userRolesApp, merchantSwaggerConfig);
        swagger_1.SwaggerModule.setup('api/v1/swagger', userRolesApp, userRolesAppDocument);
    }
    const wss = new websocket_adapter_1.WebSocketAdapter(userRolesApp);
    wss.init();
    await userRolesApp.listen(config_1.APP_CONFIG.PORT || 3000);
    const backgroudApp = await core_1.NestFactory.createApplicationContext(backgroud_app_module_1.BackgroudAppModule);
    const botTradingJob = backgroudApp.get(bot_trading_job_1.BotTradingJob);
    botTradingJob.run();
}
bootstrap();
//# sourceMappingURL=main.js.map