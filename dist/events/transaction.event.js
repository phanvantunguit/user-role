"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionEvent = void 0;
const common_1 = require("@nestjs/common");
const event_emitter_1 = require("@nestjs/event-emitter");
const transaction_1 = require("../const/transaction");
const transaction_2 = require("../domains/transaction");
const transaction_repository_1 = require("../repositories/transaction.repository");
const mail_1 = require("../resources/mail");
const transaction_3 = require("../services/transaction");
const payment_1 = require("../services/payment");
const transaction_log_1 = require("../services/transaction-log");
const email_1 = require("../const/email");
let TransactionEvent = class TransactionEvent {
    constructor(transactionRepository, mailResource, transactionService, paymentService, transactionLogService) {
        this.transactionRepository = transactionRepository;
        this.mailResource = mailResource;
        this.transactionService = transactionService;
        this.paymentService = paymentService;
        this.transactionLogService = transactionLogService;
    }
    async handleCreatedEvent(payload) {
        const transLog = await this.transactionLogService.get({
            transaction_id: payload.transaction_id,
            transaction_event: transaction_1.TRANSACTION_EVENT.SEND_EMAIL,
        });
        if (payload.transaction_status !== transaction_1.TRANSACTION_STATUS.FAILED && !transLog) {
            const transaction = await this.transactionRepository.viewfindOne({
                id: payload.transaction_id,
            });
            const extraDataPaymentInfo = await this.paymentService
                .getService(transaction.payment_method)
                .extraDataPaymentInfo(payload);
            const dataSend = Object.assign({ email: transaction.email, role_name: transaction.details[0].role_name, package_name: transaction.details[0].package_name, amount: transaction.sell_amount.toString(), currency: transaction.sell_currency, buy_amount: transaction.buy_amount, buy_currency: transaction.buy_currency, payment_id: transaction.payment_id }, extraDataPaymentInfo);
            const resEmail = await this.mailResource.sendEmailPaymentInfo(dataSend);
            this.transactionLogService.saveNotSendEvent({
                transaction_id: transaction.id,
                transaction_status: payload.transaction_status,
                transaction_event: transaction_1.TRANSACTION_EVENT.SEND_EMAIL,
                metadata: Object.assign({ is_send: resEmail, subject: email_1.EMAIL_SUBJECT.transaction_information }, dataSend),
            });
        }
        else {
            const transaction = await this.transactionRepository.findOne({
                id: payload.transaction_id,
            });
            await this.transactionRepository.save(Object.assign(Object.assign({}, transaction), { status: payload.transaction_status }));
        }
    }
    async handleProcessingEvent(payload) {
        const transaction = await this.transactionRepository.findOne({
            id: payload.transaction_id,
        });
        if (transaction.status === transaction_1.TRANSACTION_STATUS.CREATED) {
            await this.transactionRepository.save(Object.assign(Object.assign({}, transaction), { status: payload.transaction_status }));
        }
    }
    async handleCompleteEvent(payload) {
        const transaction = await this.transactionRepository.findOne({
            id: payload.transaction_id,
        });
        if (transaction.status === transaction_1.TRANSACTION_STATUS.PROCESSING ||
            transaction.status === transaction_1.TRANSACTION_STATUS.CREATED) {
            this.transactionService.handlePaymentComplete(transaction);
        }
    }
    async handleFailedEvent(payload) {
        const transaction = await this.transactionRepository.findOne({
            id: payload.transaction_id,
        });
        if (transaction.status === transaction_1.TRANSACTION_STATUS.PROCESSING ||
            transaction.status === transaction_1.TRANSACTION_STATUS.CREATED) {
            this.transactionService.handlePaymentFailed(transaction);
        }
    }
    async handleDeliveredEvent(payload) {
    }
};
__decorate([
    event_emitter_1.OnEvent(transaction_1.TRANSACTION_EVENT.PAYMENT_CREATED),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TransactionEvent.prototype, "handleCreatedEvent", null);
__decorate([
    event_emitter_1.OnEvent(transaction_1.TRANSACTION_EVENT.PAYMENT_PROCESSING),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TransactionEvent.prototype, "handleProcessingEvent", null);
__decorate([
    event_emitter_1.OnEvent(transaction_1.TRANSACTION_EVENT.PAYMENT_COMPLETE),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TransactionEvent.prototype, "handleCompleteEvent", null);
__decorate([
    event_emitter_1.OnEvent(transaction_1.TRANSACTION_EVENT.PAYMENT_FAILED),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TransactionEvent.prototype, "handleFailedEvent", null);
__decorate([
    event_emitter_1.OnEvent(transaction_1.TRANSACTION_EVENT.DELIVERED),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TransactionEvent.prototype, "handleDeliveredEvent", null);
TransactionEvent = __decorate([
    __param(0, common_1.Inject(transaction_repository_1.TransactionRepository)),
    __metadata("design:paramtypes", [transaction_repository_1.TransactionRepository,
        mail_1.MailResource,
        transaction_3.TransactionService,
        payment_1.PaymentService,
        transaction_log_1.TransactionLogService])
], TransactionEvent);
exports.TransactionEvent = TransactionEvent;
//# sourceMappingURL=transaction.event.js.map