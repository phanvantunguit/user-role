import { RawTransactionLog } from 'src/domains/transaction';
import { TransactionRepository } from 'src/repositories/transaction.repository';
import { MailResource } from 'src/resources/mail';
import { TransactionService } from 'src/services/transaction';
import { PaymentService } from 'src/services/payment';
import { TransactionLogService } from 'src/services/transaction-log';
export declare class TransactionEvent {
    private transactionRepository;
    private mailResource;
    private transactionService;
    private paymentService;
    private transactionLogService;
    constructor(transactionRepository: TransactionRepository, mailResource: MailResource, transactionService: TransactionService, paymentService: PaymentService, transactionLogService: TransactionLogService);
    handleCreatedEvent(payload: RawTransactionLog): Promise<void>;
    handleProcessingEvent(payload: RawTransactionLog): Promise<void>;
    handleCompleteEvent(payload: RawTransactionLog): Promise<void>;
    handleFailedEvent(payload: RawTransactionLog): Promise<void>;
    handleDeliveredEvent(payload: RawTransactionLog): Promise<void>;
}
