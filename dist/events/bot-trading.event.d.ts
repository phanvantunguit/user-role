import { BotTradingService } from 'src/services/bot-trading';
export declare class BotTradingEvent {
    private botTradingService;
    constructor(botTradingService: BotTradingService);
    handleConnectingEvent(payload: {
        user_bot_id: string;
        user_id: string;
        bot_id: string;
        bot_code: string;
    }): Promise<void>;
}
