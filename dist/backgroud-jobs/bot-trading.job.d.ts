import { EventStoreRepository } from 'src/repositories/event-store.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { MetaapiResource } from 'src/resources/forex';
import { BotTradingService } from 'src/services/bot-trading';
import { BackgroudJob } from '.';
export declare class BotTradingJob {
    private backgroudJob;
    private botTradingService;
    private userRepository;
    private metaapiResource;
    private eventStoreRepository;
    constructor(backgroudJob: BackgroudJob, botTradingService: BotTradingService, userRepository: UserRepository, metaapiResource: MetaapiResource, eventStoreRepository: EventStoreRepository);
    run(): void;
    checkNotConnected(): Promise<void>;
    checkExpired(): Promise<void>;
    checkBlockEvent(): Promise<void>;
}
