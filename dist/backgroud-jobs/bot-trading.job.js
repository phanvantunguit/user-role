"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotTradingJob = void 0;
const common_1 = require("@nestjs/common");
const schedule_1 = require("@nestjs/schedule");
const app_setting_1 = require("../const/app-setting");
const transaction_1 = require("../const/transaction");
const event_store_repository_1 = require("../repositories/event-store.repository");
const user_repository_1 = require("../repositories/user.repository");
const forex_1 = require("../resources/forex");
const bot_trading_1 = require("../services/bot-trading");
const _1 = require(".");
let BotTradingJob = class BotTradingJob {
    constructor(backgroudJob, botTradingService, userRepository, metaapiResource, eventStoreRepository) {
        this.backgroudJob = backgroudJob;
        this.botTradingService = botTradingService;
        this.userRepository = userRepository;
        this.metaapiResource = metaapiResource;
        this.eventStoreRepository = eventStoreRepository;
    }
    run() {
        this.backgroudJob.addNewJob(this.checkNotConnected.name, this.checkNotConnected.bind(this), schedule_1.CronExpression.EVERY_5_MINUTES, this.checkNotConnected.bind(this));
        this.backgroudJob.addNewJob(this.checkExpired.name, this.checkExpired.bind(this), schedule_1.CronExpression.EVERY_10_MINUTES, this.checkExpired.bind(this));
        this.backgroudJob.addNewJob(this.checkBlockEvent.name, this.checkBlockEvent.bind(this), schedule_1.CronExpression.EVERY_5_MINUTES, this.checkBlockEvent.bind(this));
    }
    async checkNotConnected() {
        const userBots = await this.userRepository.findUserBotTradingSQL({
            status: [transaction_1.ITEM_STATUS.CONNECTING],
            expires_at: Date.now(),
        });
        console.log('BotTradingJob checkNotConnected', userBots.length);
        for (let userBot of userBots) {
            try {
                await this.botTradingService.connectBroker({
                    user_bot_id: userBot.id,
                    user_id: userBot.user_id,
                    bot_id: userBot.bot_id,
                });
            }
            catch (error) {
                console.log('BotTradingJob checkNotConnected error', error);
            }
        }
    }
    async checkExpired() {
        const userBots = await this.userRepository.findUserBotTradingSQL({
            expired: Date.now(),
        });
        console.log('BotTradingJob checkExpired', userBots.length);
        for (let userBot of userBots) {
            let removed = true;
            try {
                if (userBot.subscriber_id) {
                    removed = await this.metaapiResource.removeAccount({
                        subscriber_id: userBot.subscriber_id,
                    });
                }
                if (userBot.profile_id) {
                    await this.metaapiResource.removeProvisioningProfile(userBot.profile_id);
                }
                if (removed) {
                    await this.userRepository.saveUserBotTrading([
                        {
                            id: userBot.id,
                            status: transaction_1.ITEM_STATUS.EXPIRED,
                            subscriber_id: null,
                            broker_account: null,
                        },
                    ]);
                    await this.botTradingService.emailBotStatus({
                        user_id: userBot.user_id,
                        bot_id: userBot.bot_id,
                        status: transaction_1.ITEM_STATUS.EXPIRED,
                    });
                }
            }
            catch (error) { }
        }
    }
    async checkBlockEvent() {
        var _a;
        const blockEvents = await this.eventStoreRepository.findEventStore({
            state: app_setting_1.EVENT_STORE_STATE.OPEN,
            event_name: [
                app_setting_1.EVENT_STORE_NAME.TBOT_STOP_INACTIVE_BY_SYSTEM,
                app_setting_1.EVENT_STORE_NAME.TBOT_CONNECTING,
            ],
        });
        for (let event of blockEvents) {
            try {
                if (event.event_name === app_setting_1.EVENT_STORE_NAME.TBOT_CONNECTING) {
                    const userBot = await this.userRepository.findUserBotTrading({
                        id: event.event_id,
                    });
                    if (userBot[0] && userBot[0].status !== transaction_1.ITEM_STATUS.CONNECTING) {
                        await this.eventStoreRepository.delete({
                            event_id: event.event_id,
                            event_name: app_setting_1.EVENT_STORE_NAME.TBOT_CONNECTING,
                            state: app_setting_1.EVENT_STORE_STATE.OPEN,
                        });
                    }
                }
                else if (event.event_name === app_setting_1.EVENT_STORE_NAME.TBOT_STOP_INACTIVE_BY_SYSTEM &&
                    ((_a = event.metadata) === null || _a === void 0 ? void 0 : _a.user_bot_id)) {
                    const userBot = await this.userRepository.findUserBotTrading({
                        id: event.metadata.user_bot_id,
                    });
                    if (userBot[0] && userBot[0].status === transaction_1.ITEM_STATUS.ACTIVE) {
                        await this.eventStoreRepository.delete({
                            event_id: event.event_id,
                            event_name: app_setting_1.EVENT_STORE_NAME.TBOT_STOP_INACTIVE_BY_SYSTEM,
                            state: app_setting_1.EVENT_STORE_STATE.OPEN,
                        });
                    }
                }
            }
            catch (error) { }
        }
    }
};
BotTradingJob = __decorate([
    common_1.Injectable(),
    __param(1, common_1.Inject(bot_trading_1.BotTradingService)),
    __param(2, common_1.Inject(user_repository_1.UserRepository)),
    __param(3, common_1.Inject(forex_1.MetaapiResource)),
    __param(4, common_1.Inject(event_store_repository_1.EventStoreRepository)),
    __metadata("design:paramtypes", [_1.BackgroudJob,
        bot_trading_1.BotTradingService,
        user_repository_1.UserRepository,
        forex_1.MetaapiResource,
        event_store_repository_1.EventStoreRepository])
], BotTradingJob);
exports.BotTradingJob = BotTradingJob;
//# sourceMappingURL=bot-trading.job.js.map