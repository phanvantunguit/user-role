import { CronExpression, SchedulerRegistry } from '@nestjs/schedule';
export declare class BackgroudJob {
    private schedulerRegistry;
    private readonly logger;
    constructor(schedulerRegistry: SchedulerRegistry);
    stopCronJob(name: string): void;
    startCronJob(name: string): void;
    deleteCronJob(name: string): void;
    addNewJob(name: string, taks: Function, time: CronExpression, preTask?: Function): void;
}
