import { TransactionRepository } from 'src/repositories/transaction.repository';
import { TransactionService } from 'src/services/transaction';
import { BackgroudJob } from '.';
export declare class TransactionJob {
    private backgroudJob;
    private transactionService;
    private transactionRepository;
    constructor(backgroudJob: BackgroudJob, transactionService: TransactionService, transactionRepository: TransactionRepository);
    run(): void;
    checkTransaction(): Promise<void>;
}
