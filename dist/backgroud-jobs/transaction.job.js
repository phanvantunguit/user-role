"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionJob = void 0;
const common_1 = require("@nestjs/common");
const schedule_1 = require("@nestjs/schedule");
const transaction_1 = require("../const/transaction");
const transaction_repository_1 = require("../repositories/transaction.repository");
const transaction_2 = require("../services/transaction");
const _1 = require(".");
let TransactionJob = class TransactionJob {
    constructor(backgroudJob, transactionService, transactionRepository) {
        this.backgroudJob = backgroudJob;
        this.transactionService = transactionService;
        this.transactionRepository = transactionRepository;
    }
    run() {
        this.backgroudJob.addNewJob(this.checkTransaction.name, this.checkTransaction.bind(this), schedule_1.CronExpression.EVERY_12_HOURS, this.checkTransaction.bind(this));
    }
    async checkTransaction() {
        const trans = await this.transactionRepository.findMultipleStatus({
            status: [transaction_1.TRANSACTION_STATUS.PROCESSING, transaction_1.TRANSACTION_STATUS.CREATED],
        });
        let time = 0;
        const _this = this;
        trans.forEach((tran) => {
            time += 5000;
            setTimeout(() => {
                _this.transactionService.checkPartnerTransaction(tran);
            }, time);
        });
    }
};
TransactionJob = __decorate([
    common_1.Injectable(),
    __param(2, common_1.Inject(transaction_repository_1.TransactionRepository)),
    __metadata("design:paramtypes", [_1.BackgroudJob,
        transaction_2.TransactionService,
        transaction_repository_1.TransactionRepository])
], TransactionJob);
exports.TransactionJob = TransactionJob;
//# sourceMappingURL=transaction.job.js.map