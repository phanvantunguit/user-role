"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventStoreRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const event_store_entity_1 = require("./event-store.entity");
class EventStoreRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(event_store_entity_1.EventStoreEntity, app_setting_1.ConnectionName.user_role);
    }
    findEventStore(param) {
        const { event_id, event_name, user_id, state, from, to } = param;
        const whereArray = [];
        if (event_id) {
            whereArray.push(`event_id = '${event_id}'`);
        }
        if (event_name) {
            if (Array.isArray(event_name) && event_name.length > 0) {
                whereArray.push(`event_name in (${event_name.map((e) => `'${e}'`)})`);
            }
            else {
                whereArray.push(`'event_name' = '${event_name}'`);
            }
        }
        if (user_id) {
            whereArray.push(`'user_id' = '${user_id}'`);
        }
        if (state) {
            whereArray.push(`'state' = '${state}'`);
        }
        if (from) {
            whereArray.push(`created_at >= ${from}`);
        }
        if (to) {
            whereArray.push(`created_at <= ${to}`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
    SELECT * FROM
    ${event_store_entity_1.EVENT_STORE_TABLE}
    ${where} 
    `;
        return this.repo().query(query);
    }
}
exports.EventStoreRepository = EventStoreRepository;
//# sourceMappingURL=index.js.map