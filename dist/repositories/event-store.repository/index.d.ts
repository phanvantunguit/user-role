import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { EventStoreEntity } from './event-store.entity';
export declare class EventStoreRepository extends BaseRepository<EventStoreEntity, EventStoreEntity> {
    repo(): Repository<EventStoreEntity>;
    findEventStore(param: {
        event_id?: string;
        event_name?: string | string[];
        user_id?: string;
        state?: string;
        from?: string;
        to?: string;
    }): Promise<EventStoreEntity[]>;
}
