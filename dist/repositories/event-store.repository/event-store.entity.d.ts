import { EntitySubscriberInterface, InsertEvent } from 'typeorm';
export declare const EVENT_STORE_TABLE = "event_stores";
export declare class EventStoreEntity {
    id?: string;
    event_id?: string;
    event_name?: string;
    user_id?: string;
    state?: string;
    metadata?: any;
    description?: string;
    created_at?: number;
}
export declare class EventStoreEntitySubscriber implements EntitySubscriberInterface<EventStoreEntity> {
    beforeInsert(event: InsertEvent<EventStoreEntity>): Promise<void>;
}
