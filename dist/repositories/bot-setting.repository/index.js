"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotSettingRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const bot_setting_types_1 = require("../../domains/setting/bot-setting.types");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const bot_setting_entity_1 = require("./bot-setting.entity");
class BotSettingRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(bot_setting_entity_1.BotSettingEntity, app_setting_1.ConnectionName.user_role);
    }
}
exports.BotSettingRepository = BotSettingRepository;
//# sourceMappingURL=index.js.map