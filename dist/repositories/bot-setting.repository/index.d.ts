import { QueryBotSetting, RawBotSetting } from 'src/domains/setting/bot-setting.types';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { BotSettingEntity } from './bot-setting.entity';
export declare class BotSettingRepository extends BaseRepository<QueryBotSetting, RawBotSetting> {
    repo(): Repository<BotSettingEntity>;
}
