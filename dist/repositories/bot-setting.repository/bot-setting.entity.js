"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotSettingEntitySubscriber = exports.BotSettingEntity = exports.BOT_SETTING_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.BOT_SETTING_TABLE = 'bot_settings';
let BotSettingEntity = class BotSettingEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], BotSettingEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], BotSettingEntity.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ type: 'jsonb', default: {} }),
    __metadata("design:type", Object)
], BotSettingEntity.prototype, "params", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], BotSettingEntity.prototype, "owner_created", void 0);
__decorate([
    typeorm_1.Column({ type: 'boolean' }),
    __metadata("design:type", Boolean)
], BotSettingEntity.prototype, "status", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], BotSettingEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], BotSettingEntity.prototype, "updated_at", void 0);
BotSettingEntity = __decorate([
    typeorm_1.Entity(exports.BOT_SETTING_TABLE)
], BotSettingEntity);
exports.BotSettingEntity = BotSettingEntity;
let BotSettingEntitySubscriber = class BotSettingEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
BotSettingEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], BotSettingEntitySubscriber);
exports.BotSettingEntitySubscriber = BotSettingEntitySubscriber;
//# sourceMappingURL=bot-setting.entity.js.map