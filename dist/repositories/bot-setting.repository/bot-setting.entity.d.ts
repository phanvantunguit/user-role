import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const BOT_SETTING_TABLE = "bot_settings";
export declare class BotSettingEntity {
    id: string;
    name: string;
    params: any;
    owner_created: string;
    status: boolean;
    created_at: number;
    updated_at: number;
}
export declare class BotSettingEntitySubscriber implements EntitySubscriberInterface<BotSettingEntity> {
    beforeInsert(event: InsertEvent<BotSettingEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<BotSettingEntity>): Promise<void>;
}
