"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FavoriteSignalEntitySubscriber = exports.FavoriteSignalEntity = exports.FAVORITE_SIGNAL_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.FAVORITE_SIGNAL_TABLE = 'favorite_signal';
let FavoriteSignalEntity = class FavoriteSignalEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], FavoriteSignalEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], FavoriteSignalEntity.prototype, "bot_signal_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    __metadata("design:type", String)
], FavoriteSignalEntity.prototype, "user_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], FavoriteSignalEntity.prototype, "created_at", void 0);
FavoriteSignalEntity = __decorate([
    typeorm_1.Entity(exports.FAVORITE_SIGNAL_TABLE),
    typeorm_1.Unique('bot_signal_id_user_id_un', ['bot_signal_id', 'user_id'])
], FavoriteSignalEntity);
exports.FavoriteSignalEntity = FavoriteSignalEntity;
let FavoriteSignalEntitySubscriber = class FavoriteSignalEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
    }
};
FavoriteSignalEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], FavoriteSignalEntitySubscriber);
exports.FavoriteSignalEntitySubscriber = FavoriteSignalEntitySubscriber;
//# sourceMappingURL=favorite-signal.entity.js.map