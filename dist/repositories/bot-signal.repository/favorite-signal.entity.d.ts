import { EntitySubscriberInterface, InsertEvent } from 'typeorm';
export declare const FAVORITE_SIGNAL_TABLE = "favorite_signal";
export declare class FavoriteSignalEntity {
    id?: string;
    bot_signal_id?: string;
    user_id?: string;
    created_at?: number;
}
export declare class FavoriteSignalEntitySubscriber implements EntitySubscriberInterface<FavoriteSignalEntity> {
    beforeInsert(event: InsertEvent<FavoriteSignalEntity>): Promise<void>;
}
