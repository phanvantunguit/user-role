"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotSignalRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const bot_signal_entity_1 = require("./bot-signal.entity");
const favorite_signal_entity_1 = require("./favorite-signal.entity");
class BotSignalRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(bot_signal_entity_1.BotSignalEntity, app_setting_1.ConnectionName.user_role);
    }
    favoriteSignalRepo() {
        return typeorm_1.getRepository(favorite_signal_entity_1.FavoriteSignalEntity, app_setting_1.ConnectionName.user_role);
    }
    async getPaging(params) {
        let { page, size } = params;
        const { exchanges, symbols, from, to, names, resolutions, types, user_id } = params;
        page = Number(page || 1);
        size = Number(size || 50);
        const whereArray = [];
        if (from) {
            whereArray.push(`time >= ${from}`);
        }
        if (to) {
            whereArray.push(`time <= ${to}`);
        }
        if (names && names.length > 0) {
            whereArray.push(`name in (${names.map((e) => `'${e}'`)})`);
        }
        if (exchanges && exchanges.length > 0) {
            whereArray.push(`exchange in (${exchanges.map((e) => `'${e}'`)})`);
        }
        if (symbols && symbols.length > 0) {
            whereArray.push(`symbol in (${symbols.map((e) => `'${e}'`)})`);
        }
        if (resolutions && resolutions.length > 0) {
            whereArray.push(`resolution in (${resolutions.map((e) => `'${e}'`)})`);
        }
        if (types && types.length > 0) {
            whereArray.push(`type in (${types.map((e) => `'${e}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryTransaction = `
    select ubs.*
    , ufs.favorite
    from (
      SELECT  *
      FROM ${bot_signal_entity_1.BOT_SIGNAL_TABLE}  
      ${where}  
      ORDER BY created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    ) as ubs
    left join (
    select bot_signal_id, true as favorite from favorite_signal fs2
    where fs2.user_id = '${user_id}'
    ) ufs on ufs.bot_signal_id = ubs.id 
    `;
        const queryCount = `
    SELECT COUNT(*)
    FROM ${bot_signal_entity_1.BOT_SIGNAL_TABLE}  
    ${where}
    `;
        const [rows, total] = await Promise.all([
            this.repo().query(queryTransaction),
            this.repo().query(queryCount),
        ]);
        return {
            rows,
            page,
            size,
            count: rows.length,
            total: Number(total[0].count),
        };
    }
    async getFavoritePaging(params) {
        let { page, size } = params;
        const { exchanges, symbols, from, to, names, resolutions, types, user_id } = params;
        page = Number(page || 1);
        size = Number(size || 50);
        const whereArray = [];
        if (from) {
            whereArray.push(`time >= ${from}`);
        }
        if (to) {
            whereArray.push(`time <= ${to}`);
        }
        if (names && names.length > 0) {
            whereArray.push(`name in (${names.map((e) => `'${e}'`)})`);
        }
        if (exchanges && exchanges.length > 0) {
            whereArray.push(`exchange in (${exchanges.map((e) => `'${e}'`)})`);
        }
        if (symbols && symbols.length > 0) {
            whereArray.push(`symbol in (${symbols.map((e) => `'${e}'`)})`);
        }
        if (resolutions && resolutions.length > 0) {
            whereArray.push(`resolution in (${resolutions.map((e) => `'${e}'`)})`);
        }
        if (types && types.length > 0) {
            whereArray.push(`type in (${types.map((e) => `'${e}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryTransaction = `
    select bs.*
	  ,ufs.favorite
    FROM ${bot_signal_entity_1.BOT_SIGNAL_TABLE} as bs
    right join (
    select bot_signal_id, true as favorite from favorite_signal fs2
    where fs2.user_id = '${user_id}'
    ) ufs on ufs.bot_signal_id = bs.id 
    ${where}  
      ORDER BY created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `;
        const queryCount = `
    SELECT COUNT(*)
    FROM ${bot_signal_entity_1.BOT_SIGNAL_TABLE} as bs
    right join (
    select bot_signal_id, true as favorite from favorite_signal fs2
    where fs2.user_id = '${user_id}'
    ) ufs on ufs.bot_signal_id = bs.id 
    ${where} 
    `;
        const [rows, total] = await Promise.all([
            this.repo().query(queryTransaction),
            this.repo().query(queryCount),
        ]);
        return {
            rows,
            page,
            size,
            count: rows.length,
            total: Number(total[0].count),
        };
    }
    async addFavoriteSignal(params) {
        return this.favoriteSignalRepo().save({
            user_id: params.user_id,
            bot_signal_id: params.bot_signal_id,
        });
    }
    async removeFavoriteSignal(params) {
        return this.favoriteSignalRepo().delete({
            user_id: params.user_id,
            bot_signal_id: params.bot_signal_id,
        });
    }
    async findFavoriteSignal(params) {
        return this.favoriteSignalRepo().findOne({
            user_id: params.user_id,
            bot_signal_id: params.bot_signal_id,
        });
    }
}
exports.BotSignalRepository = BotSignalRepository;
//# sourceMappingURL=index.js.map