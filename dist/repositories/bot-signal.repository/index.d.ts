import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { BotSignalEntity } from './bot-signal.entity';
import { FavoriteSignalEntity } from './favorite-signal.entity';
export declare class BotSignalRepository extends BaseRepository<BotSignalEntity, BotSignalEntity> {
    repo(): Repository<BotSignalEntity>;
    favoriteSignalRepo(): Repository<FavoriteSignalEntity>;
    getPaging(params: {
        user_id: string;
        page?: number;
        size?: number;
        exchanges?: string[];
        symbols?: string[];
        names?: string[];
        resolutions?: string[];
        types?: string[];
        from?: number;
        to?: number;
    }): Promise<{
        rows: any;
        page: number;
        size: number;
        count: number;
        total: number;
    }>;
    getFavoritePaging(params: {
        user_id: string;
        page?: number;
        size?: number;
        exchanges?: string[];
        symbols?: string[];
        names?: string[];
        resolutions?: string[];
        types?: string[];
        from?: number;
        to?: number;
    }): Promise<{
        rows: any;
        page: number;
        size: number;
        count: number;
        total: number;
    }>;
    addFavoriteSignal(params: {
        user_id: string;
        bot_signal_id: string;
    }): Promise<{
        user_id: string;
        bot_signal_id: string;
    } & FavoriteSignalEntity>;
    removeFavoriteSignal(params: {
        user_id: string;
        bot_signal_id: string;
    }): Promise<import("typeorm").DeleteResult>;
    findFavoriteSignal(params: {
        user_id: string;
        bot_signal_id: string;
    }): Promise<FavoriteSignalEntity>;
}
