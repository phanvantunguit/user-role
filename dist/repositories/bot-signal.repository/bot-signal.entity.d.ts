import { EntitySubscriberInterface, InsertEvent } from 'typeorm';
export declare const BOT_SIGNAL_TABLE = "bot_signal";
export declare class BotSignalEntity {
    id?: string;
    name?: string;
    resolution?: string;
    signal_id?: string;
    time?: number;
    type?: string;
    image_url?: string;
    exchange?: string;
    symbol?: string;
    metadata?: any;
    created_at?: number;
}
export declare class BotSignalEntitySubscriber implements EntitySubscriberInterface<BotSignalEntity> {
    beforeInsert(event: InsertEvent<BotSignalEntity>): Promise<void>;
}
