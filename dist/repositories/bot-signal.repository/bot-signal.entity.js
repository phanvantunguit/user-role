"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotSignalEntitySubscriber = exports.BotSignalEntity = exports.BOT_SIGNAL_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.BOT_SIGNAL_TABLE = 'bot_signal';
let BotSignalEntity = class BotSignalEntity {
};
__decorate([
    typeorm_1.PrimaryColumn('varchar'),
    __metadata("design:type", String)
], BotSignalEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotSignalEntity.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotSignalEntity.prototype, "resolution", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', unique: true }),
    __metadata("design:type", String)
], BotSignalEntity.prototype, "signal_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', nullable: true }),
    __metadata("design:type", Number)
], BotSignalEntity.prototype, "time", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotSignalEntity.prototype, "type", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotSignalEntity.prototype, "image_url", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotSignalEntity.prototype, "exchange", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotSignalEntity.prototype, "symbol", void 0);
__decorate([
    typeorm_1.Column({ type: 'jsonb', default: {} }),
    __metadata("design:type", Object)
], BotSignalEntity.prototype, "metadata", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], BotSignalEntity.prototype, "created_at", void 0);
BotSignalEntity = __decorate([
    typeorm_1.Entity(exports.BOT_SIGNAL_TABLE)
], BotSignalEntity);
exports.BotSignalEntity = BotSignalEntity;
let BotSignalEntitySubscriber = class BotSignalEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
    }
};
BotSignalEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], BotSignalEntitySubscriber);
exports.BotSignalEntitySubscriber = BotSignalEntitySubscriber;
//# sourceMappingURL=bot-signal.entity.js.map