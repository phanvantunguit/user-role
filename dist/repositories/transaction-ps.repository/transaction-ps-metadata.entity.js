"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionPSMetadataEntitySubscriber = exports.TransactionPSMetadataEntity = exports.TRANSACTION_PS_METADATA_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.TRANSACTION_PS_METADATA_TABLE = 'transaction_metadata';
let TransactionPSMetadataEntity = class TransactionPSMetadataEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], TransactionPSMetadataEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    typeorm_1.Index(),
    __metadata("design:type", String)
], TransactionPSMetadataEntity.prototype, "transaction_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionPSMetadataEntity.prototype, "attribute", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionPSMetadataEntity.prototype, "value", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], TransactionPSMetadataEntity.prototype, "created_at", void 0);
TransactionPSMetadataEntity = __decorate([
    typeorm_1.Entity(exports.TRANSACTION_PS_METADATA_TABLE)
], TransactionPSMetadataEntity);
exports.TransactionPSMetadataEntity = TransactionPSMetadataEntity;
let TransactionPSMetadataEntitySubscriber = class TransactionPSMetadataEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
    }
};
TransactionPSMetadataEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], TransactionPSMetadataEntitySubscriber);
exports.TransactionPSMetadataEntitySubscriber = TransactionPSMetadataEntitySubscriber;
//# sourceMappingURL=transaction-ps-metadata.entity.js.map