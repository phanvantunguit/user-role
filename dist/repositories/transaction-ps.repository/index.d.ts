import { TRANSACTION_STATUS } from 'src/const/transaction';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { TransactionPSLogEntity } from './transaction-ps-log.entity';
import { TransactionPSMetadataEntity } from './transaction-ps-metadata.entity';
import { TransactionPSEntity } from './transaction-ps.entity';
export declare class TransactionPSRepository extends BaseRepository<TransactionPSEntity, TransactionPSEntity> {
    repo(): Repository<TransactionPSEntity>;
    metadataRepo(): Repository<TransactionPSMetadataEntity>;
    logRepo(): Repository<TransactionPSLogEntity>;
    getPaging(params: {
        page?: number;
        size?: number;
        keyword?: string;
        status?: TRANSACTION_STATUS;
        from?: number;
        to?: number;
        merchant_code?: string;
        category?: string;
        name?: string;
    }): Promise<{
        rows: any;
        page: number;
        size: number;
        count: number;
        total: number;
    }>;
    getDetail(params: {
        id: string;
    }): Promise<{
        transaction: TransactionPSEntity;
        metadatas: TransactionPSMetadataEntity[];
        logs: TransactionPSLogEntity[];
    }>;
    getMetadata(params: {
        transaction_id: string;
    }): Promise<TransactionPSMetadataEntity[]>;
    getLog(params: {
        transaction_id: string;
    }): Promise<TransactionPSLogEntity[]>;
}
