import { PAYMENT_METHOD, TRANSACTION_STATUS } from 'src/const/transaction';
import { EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
export declare const TRANSACTION_PS_TABLE = "transactions";
export declare class TransactionPSEntity {
    id?: string;
    integrate_service?: string;
    order_id?: string;
    order_type?: string;
    user_id?: string;
    fullname?: string;
    email?: string;
    amount?: string;
    commission_cash?: string;
    currency?: string;
    payment_id?: string;
    payment_method?: PAYMENT_METHOD;
    status?: TRANSACTION_STATUS;
    ipn_url?: string;
    merchant_code?: string;
    created_at?: number;
    updated_at?: number;
}
export declare class TransactionPSEntitySubscriber implements EntitySubscriberInterface<TransactionPSEntity> {
    beforeInsert(event: InsertEvent<TransactionPSEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<TransactionPSEntity>): Promise<void>;
}
