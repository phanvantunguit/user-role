"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionPSRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const transaction_1 = require("../../const/transaction");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const transaction_ps_log_entity_1 = require("./transaction-ps-log.entity");
const transaction_ps_metadata_entity_1 = require("./transaction-ps-metadata.entity");
const transaction_ps_entity_1 = require("./transaction-ps.entity");
class TransactionPSRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(transaction_ps_entity_1.TransactionPSEntity, app_setting_1.ConnectionName.payment_service);
    }
    metadataRepo() {
        return typeorm_1.getRepository(transaction_ps_metadata_entity_1.TransactionPSMetadataEntity, app_setting_1.ConnectionName.payment_service);
    }
    logRepo() {
        return typeorm_1.getRepository(transaction_ps_log_entity_1.TransactionPSLogEntity, app_setting_1.ConnectionName.payment_service);
    }
    async getPaging(params) {
        let { page, size } = params;
        const { keyword, status, from, to, merchant_code, category, name } = params;
        page = Number(page || 1);
        size = Number(size || 50);
        const whereArray = [];
        if (from) {
            whereArray.push(`created_at >= ${from}`);
        }
        if (to) {
            whereArray.push(`created_at <= ${to}`);
        }
        if (status) {
            whereArray.push(`status = '${status}'`);
        }
        if (merchant_code) {
            whereArray.push(`merchant_code = '${merchant_code}'`);
        }
        if (keyword) {
            whereArray.push(`(email LIKE '%${keyword}%'
          OR fullname LIKE '%${keyword}%'
          OR order_id LIKE '%${keyword}%'
          OR payment_id LIKE '%${keyword}%')`);
        }
        if (category) {
            whereArray.push(`items like '%"category":"${category}"%'`);
        }
        if (name) {
            whereArray.push(`items like '%"name":"${name}"%'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryTransaction = `
      SELECT id,
      integrate_service,
      order_id,
      order_type,
      user_id,
      fullname,
      email,
      amount,
      currency,
      payment_id,
      payment_method,
      status,
      ipn_url,
      merchant_code,
      created_at,
      updated_at,
      CAST(items AS JSONB) AS items    
      FROM
      (SELECT t.*, tm.value as items
      FROM transactions t
      LEFT JOIN transaction_metadata tm on tm.transaction_id = t.id and tm.attribute = 'items') as tmix
      ${where}  
      ORDER BY created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `;
        const queryCount = `
    SELECT COUNT(*)
    FROM (SELECT t.*, tm.value as items
      FROM transactions t
      LEFT JOIN transaction_metadata tm on tm.transaction_id = t.id and tm.attribute = 'items') as tmix
    ${where}  
    `;
        const [rows, total] = await Promise.all([
            this.repo().query(queryTransaction),
            this.repo().query(queryCount),
        ]);
        return {
            rows,
            page,
            size,
            count: rows.length,
            total: Number(total[0].count),
        };
    }
    async getDetail(params) {
        const [transaction, metadatas, logs] = await Promise.all([
            this.repo().findOne({ id: params.id }),
            this.getMetadata({ transaction_id: params.id }),
            this.getLog({ transaction_id: params.id }),
        ]);
        return { transaction, metadatas, logs };
    }
    async getMetadata(params) {
        return this.metadataRepo().find({ transaction_id: params.transaction_id });
    }
    async getLog(params) {
        return this.logRepo().find({ transaction_id: params.transaction_id });
    }
}
exports.TransactionPSRepository = TransactionPSRepository;
//# sourceMappingURL=index.js.map