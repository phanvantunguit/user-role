import { EntitySubscriberInterface, InsertEvent } from 'typeorm';
export declare const TRANSACTION_PS_LOGS_TABLE = "transaction_logs";
export declare class TransactionPSLogEntity {
    id?: string;
    transaction_id?: string;
    transaction_event?: string;
    transaction_status?: string;
    metadata?: any;
    created_at?: number;
}
export declare class TransactionPSLogEntitySubscriber implements EntitySubscriberInterface<TransactionPSLogEntity> {
    beforeInsert(event: InsertEvent<TransactionPSLogEntity>): Promise<void>;
}
