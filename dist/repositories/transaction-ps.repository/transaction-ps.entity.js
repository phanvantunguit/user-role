"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionPSEntitySubscriber = exports.TransactionPSEntity = exports.TRANSACTION_PS_TABLE = void 0;
const transaction_1 = require("../../const/transaction");
const typeorm_1 = require("typeorm");
exports.TRANSACTION_PS_TABLE = 'transactions';
let TransactionPSEntity = class TransactionPSEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "integrate_service", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "order_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "order_type", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "user_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "fullname", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "email", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "amount", void 0);
__decorate([
    typeorm_1.Column({ type: 'float4', default: 0 }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "commission_cash", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "currency", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "payment_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "payment_method", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "status", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "ipn_url", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true, default: 'CM' }),
    __metadata("design:type", String)
], TransactionPSEntity.prototype, "merchant_code", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], TransactionPSEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], TransactionPSEntity.prototype, "updated_at", void 0);
TransactionPSEntity = __decorate([
    typeorm_1.Entity(exports.TRANSACTION_PS_TABLE),
    typeorm_1.Unique('service_order_un', ['integrate_service', 'order_id'])
], TransactionPSEntity);
exports.TransactionPSEntity = TransactionPSEntity;
let TransactionPSEntitySubscriber = class TransactionPSEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        if (!event.entity.deleted_at) {
            event.entity.updated_at = Date.now();
        }
    }
};
TransactionPSEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], TransactionPSEntitySubscriber);
exports.TransactionPSEntitySubscriber = TransactionPSEntitySubscriber;
//# sourceMappingURL=transaction-ps.entity.js.map