import { EntitySubscriberInterface, InsertEvent } from 'typeorm';
export declare const TRANSACTION_PS_METADATA_TABLE = "transaction_metadata";
export declare class TransactionPSMetadataEntity {
    id?: string;
    transaction_id?: string;
    attribute?: string;
    value?: string;
    created_at?: number;
}
export declare class TransactionPSMetadataEntitySubscriber implements EntitySubscriberInterface<TransactionPSMetadataEntity> {
    beforeInsert(event: InsertEvent<TransactionPSMetadataEntity>): Promise<void>;
}
