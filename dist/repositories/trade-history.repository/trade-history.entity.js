"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TradeHistoryEntitySubscriber = exports.TradeHistoryEntity = exports.TRADE_HISTORIES_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.TRADE_HISTORIES_TABLE = 'history_trade';
let TradeHistoryEntity = class TradeHistoryEntity {
};
__decorate([
    typeorm_1.PrimaryColumn({ type: 'varchar' }),
    __metadata("design:type", String)
], TradeHistoryEntity.prototype, "name_bot", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TradeHistoryEntity.prototype, "client_id", void 0);
__decorate([
    typeorm_1.PrimaryColumn({ type: 'bigint' }),
    __metadata("design:type", Number)
], TradeHistoryEntity.prototype, "order_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint' }),
    __metadata("design:type", typeof BigInt === "function" ? BigInt : Object)
], TradeHistoryEntity.prototype, "open_time", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', nullable: true }),
    __metadata("design:type", typeof BigInt === "function" ? BigInt : Object)
], TradeHistoryEntity.prototype, "close_time", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TradeHistoryEntity.prototype, "type", void 0);
__decorate([
    typeorm_1.Column({ type: 'numeric' }),
    __metadata("design:type", Number)
], TradeHistoryEntity.prototype, "size", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TradeHistoryEntity.prototype, "symbol", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TradeHistoryEntity.prototype, "resolution", void 0);
__decorate([
    typeorm_1.Column({ type: 'numeric' }),
    __metadata("design:type", Number)
], TradeHistoryEntity.prototype, "stop_lost", void 0);
__decorate([
    typeorm_1.Column({ type: 'numeric' }),
    __metadata("design:type", Number)
], TradeHistoryEntity.prototype, "take_profit", void 0);
__decorate([
    typeorm_1.Column({ type: 'numeric', nullable: true }),
    __metadata("design:type", Number)
], TradeHistoryEntity.prototype, "commission", void 0);
__decorate([
    typeorm_1.Column({ type: 'numeric', nullable: true }),
    __metadata("design:type", Number)
], TradeHistoryEntity.prototype, "swap", void 0);
__decorate([
    typeorm_1.Column({ type: 'numeric', nullable: true }),
    __metadata("design:type", Number)
], TradeHistoryEntity.prototype, "profit", void 0);
__decorate([
    typeorm_1.Column({ type: 'text', nullable: true }),
    __metadata("design:type", String)
], TradeHistoryEntity.prototype, "comment", void 0);
__decorate([
    typeorm_1.Column({ type: 'jsonb', default: {} }),
    __metadata("design:type", Object)
], TradeHistoryEntity.prototype, "extradata", void 0);
__decorate([
    typeorm_1.Column({ type: 'numeric', nullable: true }),
    __metadata("design:type", Number)
], TradeHistoryEntity.prototype, "open_price", void 0);
__decorate([
    typeorm_1.Column({ type: 'numeric', nullable: true }),
    __metadata("design:type", Number)
], TradeHistoryEntity.prototype, "close_price", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TradeHistoryEntity.prototype, "position_side", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], TradeHistoryEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], TradeHistoryEntity.prototype, "updated_at", void 0);
TradeHistoryEntity = __decorate([
    typeorm_1.Entity(exports.TRADE_HISTORIES_TABLE, { synchronize: false })
], TradeHistoryEntity);
exports.TradeHistoryEntity = TradeHistoryEntity;
let TradeHistoryEntitySubscriber = class TradeHistoryEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
TradeHistoryEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], TradeHistoryEntitySubscriber);
exports.TradeHistoryEntitySubscriber = TradeHistoryEntitySubscriber;
//# sourceMappingURL=trade-history.entity.js.map