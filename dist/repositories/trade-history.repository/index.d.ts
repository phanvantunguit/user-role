import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { TradeHistoryEntity } from './trade-history.entity';
export declare class TradeHistoryRepository extends BaseRepository<TradeHistoryEntity, TradeHistoryEntity> {
    repo(): Repository<TradeHistoryEntity>;
    getPaging(params: {
        page?: number;
        size?: number;
        from?: number;
        to?: number;
        keyword?: string;
        type?: string;
        symbols?: string[];
        name_bots?: string[];
        resolutions?: string[];
        position_side?: string;
        sort_by?: string;
        order_by?: 'desc' | 'asc';
    }): Promise<{
        rows: any;
        page: number;
        size: number;
        count: number;
        total: number;
    }>;
}
