"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TradeHistoryRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const trade_history_entity_1 = require("./trade-history.entity");
class TradeHistoryRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(trade_history_entity_1.TradeHistoryEntity, app_setting_1.ConnectionName.dmlcmghrkhmif);
    }
    async getPaging(params) {
        let { page, size, sort_by, order_by } = params;
        const { keyword, from, to, type, symbols, resolutions, position_side, name_bots, } = params;
        page = Number(page || 1);
        size = Number(size || 50);
        const whereArray = [];
        if (from) {
            whereArray.push(`open_time >= ${from}`);
        }
        if (to) {
            whereArray.push(`open_time <= ${to}`);
        }
        if (position_side) {
            whereArray.push(`position_side = '${position_side}'`);
        }
        if (type) {
            whereArray.push(`type = '${type}'`);
        }
        if (keyword) {
            whereArray.push(`(comment ILIKE '%${keyword}%'
      OR name_bot ILIKE '%${keyword}%'
      OR client_id ILIKE '%${keyword}%'
      OR symbol ILIKE '%${keyword}%'
      OR type ILIKE '%${keyword}%'
      OR position_side ILIKE '%${keyword}%'
      OR resolution ILIKE '%${keyword}%'
      OR order_id::varchar ILIKE '%${keyword}%')`);
        }
        if (name_bots && name_bots.length > 0) {
            whereArray.push(`name_bot in (${name_bots.map((e) => `'${e}'`)})`);
        }
        if (symbols && symbols.length > 0) {
            whereArray.push(`symbol in (${symbols.map((e) => `'${e}'`)})`);
        }
        if (resolutions && resolutions.length > 0) {
            whereArray.push(`resolution in (${resolutions.map((e) => `'${e}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryTransaction = `
      SELECT *
      FROM ${trade_history_entity_1.TRADE_HISTORIES_TABLE}
      ${where}  
      ORDER BY ${sort_by ? sort_by : 'open_time'} ${order_by ? order_by : 'DESC'} 
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `;
        const queryCount = `
    SELECT COUNT(*)
    FROM ${trade_history_entity_1.TRADE_HISTORIES_TABLE}
    ${where} 
    `;
        const [rows, total] = await Promise.all([
            this.repo().query(queryTransaction),
            this.repo().query(queryCount),
        ]);
        return {
            rows,
            page,
            size,
            count: rows.length,
            total: Number(total[0].count),
        };
    }
}
exports.TradeHistoryRepository = TradeHistoryRepository;
//# sourceMappingURL=index.js.map