import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const TRADE_HISTORIES_TABLE = "history_trade";
export declare class TradeHistoryEntity {
    name_bot: string;
    client_id: string;
    order_id: number;
    open_time: bigint;
    close_time: bigint;
    type: string;
    size: number;
    symbol: string;
    resolution: string;
    stop_lost: number;
    take_profit: number;
    commission: number;
    swap: number;
    profit: number;
    comment: string;
    extradata: any;
    open_price: number;
    close_price: number;
    position_side: string;
    created_at?: number;
    updated_at?: number;
}
export declare class TradeHistoryEntitySubscriber implements EntitySubscriberInterface<TradeHistoryEntity> {
    beforeInsert(event: InsertEvent<TradeHistoryEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<TradeHistoryEntity>): Promise<void>;
}
