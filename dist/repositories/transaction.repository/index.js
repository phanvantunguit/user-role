"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const transaction_types_1 = require("../../domains/transaction/transaction.types");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const transaction_view_entity_1 = require("./transaction-view.entity");
const transaction_entity_1 = require("./transaction.entity");
class TransactionRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(transaction_entity_1.TransactionEntity, app_setting_1.ConnectionName.user_role);
    }
    transactionViewRepo() {
        return typeorm_1.getRepository(transaction_view_entity_1.TransactionViewEntity, app_setting_1.ConnectionName.user_role);
    }
    async save(params, queryRunner) {
        if (queryRunner) {
            const transaction = await this.repo().create(params);
            return queryRunner.manager.save(transaction);
        }
        else {
            return this.repo().save(params);
        }
    }
    async viewfindOne(params) {
        return this.transactionViewRepo().findOne(params);
    }
    async viewFind(params) {
        return this.transactionViewRepo().find({
            where: params,
            order: { created_at: 'DESC' },
        });
    }
    async viewFindByUserMultipleStatus(user_id, status) {
        const whereArray = [];
        if (status.length > 0) {
            whereArray.push(`status in (${status.map((e) => `'${e}'`)})`);
        }
        whereArray.push(`user_id = '${user_id}'`);
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
      SELECT *
      FROM ${transaction_view_entity_1.TRANSACTIONS_VIEW}  
      ${where} 
      order by created_at DESC
    `;
        return this.transactionViewRepo().query(query);
    }
    async viewFindTransactionPaging(params) {
        let { page, size } = params;
        const { keyword, status, from, to } = params;
        page = Number(page || 1);
        size = Number(size || 50);
        const whereArray = [];
        whereArray.push(`created_at >= ${from}`);
        whereArray.push(`created_at <= ${to}`);
        if (status) {
            whereArray.push(`status = '${status}'`);
        }
        if (keyword) {
            whereArray.push(`(email LIKE '%${keyword}%'
        OR username LIKE '%${keyword}%'
        OR phone LIKE '%${keyword}%'
        OR first_name LIKE '%${keyword}%'
        OR last_name LIKE '%${keyword}%'
        OR payment_id LIKE '%${keyword}%')`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryTransaction = `
        SELECT  *
        FROM ${transaction_view_entity_1.TRANSACTIONS_VIEW}  
        ${where}  
        ORDER BY created_at DESC
        OFFSET ${(page - 1) * size}
        LIMIT ${size}
    `;
        const queryCount = `
    SELECT COUNT(*)
    FROM ${transaction_view_entity_1.TRANSACTIONS_VIEW}  
    ${where}
    `;
        const [rows, total] = await Promise.all([
            this.transactionViewRepo().query(queryTransaction),
            this.transactionViewRepo().query(queryCount),
        ]);
        return {
            rows,
            page,
            size,
            count: rows.length,
            total: Number(total[0].count),
        };
    }
    async findMultipleStatus(params) {
        const { status } = params;
        const whereArray = [];
        if (status.length > 0) {
            whereArray.push(`status in (${status.map((e) => `'${e}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
      SELECT *
      FROM ${transaction_entity_1.TRANSACTIONS_TABLE}  
      ${where} 
      order by created_at DESC
    `;
        return this.repo().query(query);
    }
}
exports.TransactionRepository = TransactionRepository;
//# sourceMappingURL=index.js.map