import { PAYMENT_METHOD, TRANSACTION_STATUS } from 'src/const/transaction';
import { EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
export declare const TRANSACTIONS_TABLE = "transactions";
export declare class TransactionEntity {
    id: string;
    user_id: string;
    payment_id: string;
    payment_method: PAYMENT_METHOD;
    description: string;
    status: TRANSACTION_STATUS;
    sell_amount: number;
    sell_currency: string;
    buy_currency: string;
    buy_amount: string;
    parent_id: string;
    wallet_address: string;
    created_at: number;
    updated_at: number;
}
export declare class TransactionEntitySubscriber implements EntitySubscriberInterface<TransactionEntity> {
    beforeInsert(event: InsertEvent<TransactionEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<TransactionEntity>): Promise<void>;
}
