"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionEntitySubscriber = exports.TransactionEntity = exports.TRANSACTIONS_TABLE = void 0;
const transaction_1 = require("../../const/transaction");
const typeorm_1 = require("typeorm");
exports.TRANSACTIONS_TABLE = 'transactions';
let TransactionEntity = class TransactionEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], TransactionEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    typeorm_1.Index(),
    __metadata("design:type", String)
], TransactionEntity.prototype, "user_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    typeorm_1.Index(),
    __metadata("design:type", String)
], TransactionEntity.prototype, "payment_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionEntity.prototype, "payment_method", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], TransactionEntity.prototype, "description", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionEntity.prototype, "status", void 0);
__decorate([
    typeorm_1.Column({ type: 'float4' }),
    __metadata("design:type", Number)
], TransactionEntity.prototype, "sell_amount", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionEntity.prototype, "sell_currency", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionEntity.prototype, "buy_currency", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], TransactionEntity.prototype, "buy_amount", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid', nullable: true }),
    __metadata("design:type", String)
], TransactionEntity.prototype, "parent_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], TransactionEntity.prototype, "wallet_address", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], TransactionEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], TransactionEntity.prototype, "updated_at", void 0);
TransactionEntity = __decorate([
    typeorm_1.Entity(exports.TRANSACTIONS_TABLE)
], TransactionEntity);
exports.TransactionEntity = TransactionEntity;
let TransactionEntitySubscriber = class TransactionEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
TransactionEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], TransactionEntitySubscriber);
exports.TransactionEntitySubscriber = TransactionEntitySubscriber;
//# sourceMappingURL=transaction.entity.js.map