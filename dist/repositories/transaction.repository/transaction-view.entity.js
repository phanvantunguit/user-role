"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionViewEntity = exports.TRANSACTIONS_VIEW = void 0;
const transaction_1 = require("../../const/transaction");
const transaction_2 = require("../../domains/transaction");
const typeorm_1 = require("typeorm");
exports.TRANSACTIONS_VIEW = 'transactions_view';
let TransactionViewEntity = class TransactionViewEntity {
};
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "user_id", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "payment_id", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "payment_method", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "description", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "status", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], TransactionViewEntity.prototype, "sell_amount", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "sell_currency", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "buy_currency", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "email", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "username", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "phone", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "first_name", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "last_name", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "buy_amount", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionViewEntity.prototype, "wallet_address", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Array)
], TransactionViewEntity.prototype, "details", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], TransactionViewEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], TransactionViewEntity.prototype, "updated_at", void 0);
TransactionViewEntity = __decorate([
    typeorm_1.ViewEntity(exports.TRANSACTIONS_VIEW, { expression: '', synchronize: false })
], TransactionViewEntity);
exports.TransactionViewEntity = TransactionViewEntity;
//# sourceMappingURL=transaction-view.entity.js.map