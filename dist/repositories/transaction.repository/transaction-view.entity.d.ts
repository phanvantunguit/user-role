import { PAYMENT_METHOD, TRANSACTION_STATUS } from 'src/const/transaction';
import { RawTransactionDetailView } from 'src/domains/transaction';
export declare const TRANSACTIONS_VIEW = "transactions_view";
export declare class TransactionViewEntity {
    id: string;
    user_id: string;
    payment_id: string;
    payment_method: PAYMENT_METHOD;
    description: string;
    status: TRANSACTION_STATUS;
    sell_amount: number;
    sell_currency: string;
    buy_currency: string;
    email: string;
    username: string;
    phone: string;
    first_name: string;
    last_name: string;
    buy_amount: string;
    wallet_address: string;
    details: RawTransactionDetailView[];
    created_at: number;
    updated_at: number;
}
