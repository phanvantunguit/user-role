import { QueryTransaction, QueryTransactionPagination, RawTransaction } from 'src/domains/transaction/transaction.types';
import { Repository, QueryRunner } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { TransactionViewEntity } from './transaction-view.entity';
import { TransactionEntity } from './transaction.entity';
export declare class TransactionRepository extends BaseRepository<QueryTransaction, RawTransaction> {
    repo(): Repository<TransactionEntity>;
    transactionViewRepo(): Repository<TransactionViewEntity>;
    save(params: RawTransaction, queryRunner?: QueryRunner): Promise<TransactionEntity>;
    viewfindOne(params: QueryTransaction): Promise<TransactionViewEntity>;
    viewFind(params: QueryTransaction): Promise<TransactionViewEntity[]>;
    viewFindByUserMultipleStatus(user_id: string, status: string[]): Promise<TransactionViewEntity[]>;
    viewFindTransactionPaging(params: QueryTransactionPagination): Promise<{
        rows: any;
        page: number;
        size: number;
        count: any;
        total: number;
    }>;
    findMultipleStatus(params: {
        status: string[];
    }): Promise<TransactionEntity[]>;
}
