import { Cache } from 'cache-manager';
export declare class CacheRepository {
    private cacheManager;
    client: any;
    publisher: any;
    constructor(cacheManager: Cache);
    setKey(key: string, item: string, expiresIn?: number): Promise<boolean>;
    getKey(key: string): Promise<any>;
    publish(channel: string, event: string, data: any, user_id?: string): Promise<void>;
}
