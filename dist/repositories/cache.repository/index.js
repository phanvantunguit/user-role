"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CacheRepository = void 0;
const common_1 = require("@nestjs/common");
const cache_manager_1 = require("cache-manager");
const app_setting_1 = require("../../const/app-setting");
const response_1 = require("../../const/response");
let CacheRepository = class CacheRepository {
    constructor(cacheManager) {
        this.cacheManager = cacheManager;
        this.client = cacheManager.store.getClient();
        this.publisher = this.client.duplicate();
        this.publisher.on('error', (error) => {
            console.log('CacheRepository error', error);
        });
    }
    async setKey(key, item, expiresIn = 3600) {
        if (!this.client.connected) {
            return false;
        }
        await this.cacheManager.set(`${app_setting_1.SERVICE_NAME}:${key}`, item, {
            ttl: expiresIn,
        });
    }
    async getKey(key) {
        if (!this.client.connected) {
            return false;
        }
        const value = await this.cacheManager.get(`${app_setting_1.SERVICE_NAME}:${key}`);
        return value;
    }
    async publish(channel, event, data, user_id) {
        this.publisher.publish(channel, JSON.stringify({
            event: response_1.WS_EVENT.app_message,
            data: {
                sub_event: event,
                sub_data: data,
                user_id,
            },
        }));
    }
};
CacheRepository = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject(common_1.CACHE_MANAGER)),
    __metadata("design:paramtypes", [typeof (_a = typeof cache_manager_1.Cache !== "undefined" && cache_manager_1.Cache) === "function" ? _a : Object])
], CacheRepository);
exports.CacheRepository = CacheRepository;
//# sourceMappingURL=index.js.map