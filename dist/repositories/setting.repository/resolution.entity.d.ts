import { EntitySubscriberInterface, InsertEvent } from 'typeorm';
export declare const RESOLUTIONS_TABLE = "resolutions";
export declare class ResolutionEntity {
    id: string;
    resolutions_name: string;
    display_name: string;
    created_at: number;
}
export declare class ResolutionEntitySubscriber implements EntitySubscriberInterface<ResolutionEntity> {
    beforeInsert(event: InsertEvent<ResolutionEntity>): Promise<void>;
}
