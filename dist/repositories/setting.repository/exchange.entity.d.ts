import { InsertEvent, EntitySubscriberInterface } from 'typeorm';
export declare const EXCHANGES_TABLE = "exchanges";
export declare class ExchangeEntity {
    exchange_name: string;
    exchange_desc: string;
    created_at: number;
}
export declare class ExchangeEntitySubscriber implements EntitySubscriberInterface<ExchangeEntity> {
    beforeInsert(event: InsertEvent<ExchangeEntity>): Promise<void>;
}
