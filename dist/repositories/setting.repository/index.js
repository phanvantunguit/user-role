"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SettingRepository = void 0;
const typeorm_1 = require("typeorm");
const format_data_util_1 = require("../../utils/format-data.util");
const feature_entity_1 = require("./feature.entity");
const setting_1 = require("../../domains/setting");
const exchange_entity_1 = require("./exchange.entity");
const resolution_entity_1 = require("./resolution.entity");
const symbol_entity_1 = require("./symbol.entity");
const general_setting_entity_1 = require("./general-setting.entity");
const app_setting_entity_1 = require("./app-setting.entity");
const app_setting_types_1 = require("../../domains/setting/app-setting.types");
const app_setting_1 = require("../../const/app-setting");
class SettingRepository {
    featureRepo() {
        return typeorm_1.getRepository(feature_entity_1.FeatureEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveFeatures(params) {
        return this.featureRepo().save(params);
    }
    async deleteFeatureIds(ids) {
        return this.featureRepo().delete(ids);
    }
    async findFeature(params) {
        const query = format_data_util_1.cleanObject(params);
        return this.featureRepo().find({
            where: query,
            order: { created_at: 'DESC' },
        });
    }
    async findFeatureByIds(ids) {
        return this.featureRepo().findByIds(ids);
    }
    exchangeRepo() {
        return typeorm_1.getRepository(exchange_entity_1.ExchangeEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveExchanges(params) {
        return this.exchangeRepo().save(params);
    }
    async deleteExchangeNames(names) {
        return this.exchangeRepo().delete(names);
    }
    async findExchange(params) {
        const query = format_data_util_1.cleanObject(params);
        return this.exchangeRepo().find({
            where: query,
            order: { created_at: 'DESC' },
        });
    }
    async findExchangeByNames(names) {
        return this.exchangeRepo().findByIds(names);
    }
    resolutionRepo() {
        return typeorm_1.getRepository(resolution_entity_1.ResolutionEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveResolution(params) {
        return this.resolutionRepo().save(params);
    }
    async deleteResolutionIds(ids) {
        return this.resolutionRepo().delete(ids);
    }
    async findResolution(params) {
        const query = format_data_util_1.cleanObject(params);
        return this.resolutionRepo().find({
            where: query,
            order: { created_at: 'DESC' },
        });
    }
    async findResolutionByIds(ids) {
        return this.resolutionRepo().findByIds(ids);
    }
    symbolRepo() {
        return typeorm_1.getRepository(symbol_entity_1.SymbolEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveSymbol(params) {
        return this.symbolRepo().save(params);
    }
    async deleteSymbols(symbols) {
        return this.symbolRepo().delete(symbols);
    }
    async findSymbol(params) {
        const query = format_data_util_1.cleanObject(params);
        return this.symbolRepo().find({
            where: query,
            order: { created_at: 'DESC' },
        });
    }
    async findSymbolBySymbols(symbols) {
        return this.symbolRepo().findByIds(symbols);
    }
    generalSettingRepo() {
        return typeorm_1.getRepository(general_setting_entity_1.GeneralSettingEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveGeneralSetting(params) {
        return this.generalSettingRepo().save(params);
    }
    async deleteGeneralSettingByIds(ids) {
        return this.generalSettingRepo().delete(ids);
    }
    async findGeneralSetting(params) {
        const query = format_data_util_1.cleanObject(params);
        return this.generalSettingRepo().find({
            where: query,
            order: { created_at: 'DESC' },
        });
    }
    async findGeneralSettingByIds(ids) {
        return this.generalSettingRepo().findByIds(ids);
    }
    appSettingRepo() {
        return typeorm_1.getRepository(app_setting_entity_1.AppSettingEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveAppSetting(params) {
        return this.appSettingRepo().save(params);
    }
    async findOneAppSetting(params) {
        return this.appSettingRepo().findOne(params);
    }
    async findAppSetting(params) {
        const { id, name, user_id } = params;
        const whereArray = [];
        if (user_id) {
            whereArray.push(`(user_id = '${user_id}' OR user_id = 'system')`);
        }
        else {
            whereArray.push(`user_id = 'system'`);
        }
        if (id) {
            whereArray.push(`id = '${id}'`);
        }
        if (name) {
            whereArray.push(`name = '${name}'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const querySetting = `
    SELECT  *
    FROM ${app_setting_entity_1.APP_SETTING_TABLE}  
    ${where}  
    ORDER BY updated_at DESC
  `;
        return this.appSettingRepo().query(querySetting);
    }
    async deleteAppSettingById(id) {
        return this.appSettingRepo().delete(id);
    }
}
exports.SettingRepository = SettingRepository;
//# sourceMappingURL=index.js.map