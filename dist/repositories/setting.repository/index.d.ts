import { QueryExchange, QueryFeature, QueryGeneralSetting, QueryResolution, QuerySymbol, RawExchange, RawFeature, RawGeneralSetting, RawResolution, RawSymbol } from 'src/domains/setting';
import { QueryAppSetting, RawAppSetting } from 'src/domains/setting/app-setting.types';
export declare class SettingRepository {
    private featureRepo;
    saveFeatures(params: RawFeature[]): Promise<RawFeature[]>;
    deleteFeatureIds(ids: string[]): Promise<import("typeorm").DeleteResult>;
    findFeature(params: QueryFeature): Promise<RawFeature[]>;
    findFeatureByIds(ids: any): Promise<RawFeature[]>;
    private exchangeRepo;
    saveExchanges(params: RawExchange[]): Promise<RawExchange[]>;
    deleteExchangeNames(names: string[]): Promise<import("typeorm").DeleteResult>;
    findExchange(params: QueryExchange): Promise<RawExchange[]>;
    findExchangeByNames(names: string[]): Promise<RawExchange[]>;
    private resolutionRepo;
    saveResolution(params: RawResolution[]): Promise<RawResolution[]>;
    deleteResolutionIds(ids: string[]): Promise<import("typeorm").DeleteResult>;
    findResolution(params: QueryResolution): Promise<RawResolution[]>;
    findResolutionByIds(ids: string[]): Promise<RawResolution[]>;
    private symbolRepo;
    saveSymbol(params: RawSymbol[]): Promise<RawSymbol[]>;
    deleteSymbols(symbols: string[]): Promise<import("typeorm").DeleteResult>;
    findSymbol(params: QuerySymbol): Promise<RawSymbol[]>;
    findSymbolBySymbols(symbols: string[]): Promise<RawSymbol[]>;
    private generalSettingRepo;
    saveGeneralSetting(params: RawGeneralSetting[]): Promise<RawGeneralSetting[]>;
    deleteGeneralSettingByIds(ids: string[]): Promise<import("typeorm").DeleteResult>;
    findGeneralSetting(params: QueryGeneralSetting): Promise<RawGeneralSetting[]>;
    findGeneralSettingByIds(ids: string[]): Promise<RawGeneralSetting[]>;
    private appSettingRepo;
    saveAppSetting(params: RawAppSetting): Promise<RawAppSetting>;
    findOneAppSetting(params: {
        name?: string;
        user_id?: string;
    }): Promise<RawAppSetting>;
    findAppSetting(params: QueryAppSetting): Promise<RawAppSetting[]>;
    deleteAppSettingById(id: string): Promise<import("typeorm").DeleteResult>;
}
