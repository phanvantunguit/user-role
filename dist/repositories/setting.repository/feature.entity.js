"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FeatureEntitySubscriber = exports.FeatureEntity = exports.FEATURES_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.FEATURES_TABLE = 'features';
let FeatureEntity = class FeatureEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    __metadata("design:type", String)
], FeatureEntity.prototype, "feature_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], FeatureEntity.prototype, "feature_name", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], FeatureEntity.prototype, "description", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], FeatureEntity.prototype, "action", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], FeatureEntity.prototype, "owner_created", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], FeatureEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], FeatureEntity.prototype, "updated_at", void 0);
FeatureEntity = __decorate([
    typeorm_1.Entity(exports.FEATURES_TABLE)
], FeatureEntity);
exports.FeatureEntity = FeatureEntity;
let FeatureEntitySubscriber = class FeatureEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
FeatureEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], FeatureEntitySubscriber);
exports.FeatureEntitySubscriber = FeatureEntitySubscriber;
//# sourceMappingURL=feature.entity.js.map