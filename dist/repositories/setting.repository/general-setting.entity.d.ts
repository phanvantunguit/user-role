import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const GENERAL_SETTING_TABLE = "general_settings";
export declare class GeneralSettingEntity {
    general_setting_id: string;
    general_setting_name: string;
    description: string;
    owner_created: string;
    created_at: number;
    updated_at: number;
}
export declare class GeneralSettingEntitySubscriber implements EntitySubscriberInterface<GeneralSettingEntity> {
    beforeInsert(event: InsertEvent<GeneralSettingEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<GeneralSettingEntity>): Promise<void>;
}
