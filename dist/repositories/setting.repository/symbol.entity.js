"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SymbolEntitySubscriber = exports.SymbolEntity = exports.SYMBOLS_TABLE = void 0;
const const_1 = require("../../const");
const typeorm_1 = require("typeorm");
exports.SYMBOLS_TABLE = 'symbols';
let SymbolEntity = class SymbolEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    __metadata("design:type", String)
], SymbolEntity.prototype, "symbol", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], SymbolEntity.prototype, "types", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], SymbolEntity.prototype, "exchange_name", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], SymbolEntity.prototype, "base_symbol", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], SymbolEntity.prototype, "quote_symbol", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], SymbolEntity.prototype, "description", void 0);
__decorate([
    typeorm_1.Column({ type: 'jsonb', default: {} }),
    __metadata("design:type", Object)
], SymbolEntity.prototype, "ticks", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', default: const_1.SYMBOL_STATUS.ON }),
    __metadata("design:type", String)
], SymbolEntity.prototype, "status", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], SymbolEntity.prototype, "timezone", void 0);
__decorate([
    typeorm_1.Column({ type: 'int4', nullable: true }),
    __metadata("design:type", Number)
], SymbolEntity.prototype, "minmov", void 0);
__decorate([
    typeorm_1.Column({ type: 'int4', nullable: true }),
    __metadata("design:type", Number)
], SymbolEntity.prototype, "minmov2", void 0);
__decorate([
    typeorm_1.Column({ type: 'int4', nullable: true }),
    __metadata("design:type", Number)
], SymbolEntity.prototype, "pointvalue", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], SymbolEntity.prototype, "session", void 0);
__decorate([
    typeorm_1.Column({ type: 'boolean', nullable: true }),
    __metadata("design:type", Boolean)
], SymbolEntity.prototype, "has_intraday", void 0);
__decorate([
    typeorm_1.Column({ type: 'boolean', nullable: true }),
    __metadata("design:type", Boolean)
], SymbolEntity.prototype, "has_no_volume", void 0);
__decorate([
    typeorm_1.Column({ type: 'int4', nullable: true }),
    __metadata("design:type", Number)
], SymbolEntity.prototype, "pricescale", void 0);
__decorate([
    typeorm_1.Column({ type: 'jsonb', nullable: true }),
    __metadata("design:type", Object)
], SymbolEntity.prototype, "config_heatmap", void 0);
__decorate([
    typeorm_1.Column({ type: 'int4', nullable: true }),
    __metadata("design:type", Number)
], SymbolEntity.prototype, "list_token", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], SymbolEntity.prototype, "created_at", void 0);
SymbolEntity = __decorate([
    typeorm_1.Entity(exports.SYMBOLS_TABLE)
], SymbolEntity);
exports.SymbolEntity = SymbolEntity;
let SymbolEntitySubscriber = class SymbolEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
    }
};
SymbolEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], SymbolEntitySubscriber);
exports.SymbolEntitySubscriber = SymbolEntitySubscriber;
//# sourceMappingURL=symbol.entity.js.map