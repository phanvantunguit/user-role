"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResolutionEntitySubscriber = exports.ResolutionEntity = exports.RESOLUTIONS_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.RESOLUTIONS_TABLE = 'resolutions';
let ResolutionEntity = class ResolutionEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], ResolutionEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], ResolutionEntity.prototype, "resolutions_name", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], ResolutionEntity.prototype, "display_name", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], ResolutionEntity.prototype, "created_at", void 0);
ResolutionEntity = __decorate([
    typeorm_1.Entity(exports.RESOLUTIONS_TABLE)
], ResolutionEntity);
exports.ResolutionEntity = ResolutionEntity;
let ResolutionEntitySubscriber = class ResolutionEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
    }
};
ResolutionEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], ResolutionEntitySubscriber);
exports.ResolutionEntitySubscriber = ResolutionEntitySubscriber;
//# sourceMappingURL=resolution.entity.js.map