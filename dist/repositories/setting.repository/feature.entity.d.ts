import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const FEATURES_TABLE = "features";
export declare class FeatureEntity {
    feature_id: string;
    feature_name: string;
    description: string;
    action: string;
    owner_created: string;
    created_at: number;
    updated_at: number;
}
export declare class FeatureEntitySubscriber implements EntitySubscriberInterface<FeatureEntity> {
    beforeInsert(event: InsertEvent<FeatureEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<FeatureEntity>): Promise<void>;
}
