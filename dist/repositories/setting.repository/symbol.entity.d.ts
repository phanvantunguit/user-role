import { SYMBOL_STATUS } from 'src/const';
import { InsertEvent, EntitySubscriberInterface } from 'typeorm';
export declare const SYMBOLS_TABLE = "symbols";
export declare class SymbolEntity {
    symbol: string;
    types: string;
    exchange_name: string;
    base_symbol: string;
    quote_symbol: string;
    description: string;
    ticks: any;
    status: SYMBOL_STATUS;
    timezone: string;
    minmov: number;
    minmov2: number;
    pointvalue: number;
    session: string;
    has_intraday: boolean;
    has_no_volume: boolean;
    pricescale: number;
    config_heatmap: any;
    list_token: number;
    created_at: number;
}
export declare class SymbolEntitySubscriber implements EntitySubscriberInterface<SymbolEntity> {
    beforeInsert(event: InsertEvent<SymbolEntity>): Promise<void>;
}
