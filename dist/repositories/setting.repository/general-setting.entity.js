"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GeneralSettingEntitySubscriber = exports.GeneralSettingEntity = exports.GENERAL_SETTING_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.GENERAL_SETTING_TABLE = 'general_settings';
let GeneralSettingEntity = class GeneralSettingEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    __metadata("design:type", String)
], GeneralSettingEntity.prototype, "general_setting_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], GeneralSettingEntity.prototype, "general_setting_name", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], GeneralSettingEntity.prototype, "description", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], GeneralSettingEntity.prototype, "owner_created", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], GeneralSettingEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], GeneralSettingEntity.prototype, "updated_at", void 0);
GeneralSettingEntity = __decorate([
    typeorm_1.Entity(exports.GENERAL_SETTING_TABLE)
], GeneralSettingEntity);
exports.GeneralSettingEntity = GeneralSettingEntity;
let GeneralSettingEntitySubscriber = class GeneralSettingEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
GeneralSettingEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], GeneralSettingEntitySubscriber);
exports.GeneralSettingEntitySubscriber = GeneralSettingEntitySubscriber;
//# sourceMappingURL=general-setting.entity.js.map