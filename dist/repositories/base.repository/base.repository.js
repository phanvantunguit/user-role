"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseRepository = void 0;
const typeorm_1 = require("typeorm");
class BaseRepository {
    repo() {
        return typeorm_1.getRepository(typeorm_1.BaseEntity);
    }
    async findOne(params) {
        return this.repo().findOne({ where: params, order: { created_at: 'DESC' } });
    }
    async find(params) {
        return this.repo().find({ where: params, order: { created_at: 'DESC' } });
    }
    async findById(id) {
        return this.repo().findOne(id);
    }
    async findByIds(ids) {
        return this.repo().findByIds(ids);
    }
    async save(params, queryRunner) {
        if (queryRunner) {
            const transaction = await this.repo().create(params);
            return queryRunner.manager.save(transaction);
        }
        else {
            return this.repo().save(params);
        }
    }
    async saves(params) {
        return this.repo().save(params);
    }
    async deleteById(id) {
        return this.repo().delete(id);
    }
    async deleteByIds(ids) {
        return this.repo().delete(ids);
    }
    async delete(params, queryRunner) {
        if (queryRunner) {
            return queryRunner.manager.delete(this.repo().target, params);
        }
        else {
            return this.repo().delete(params);
        }
    }
    async count(params) {
        return this.repo().count(params);
    }
}
exports.BaseRepository = BaseRepository;
//# sourceMappingURL=base.repository.js.map