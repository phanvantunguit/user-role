import { DeleteResult, Repository, QueryRunner } from 'typeorm';
import { EntityId } from 'typeorm/repository/EntityId';
import { IBaseRepository } from './types';
export declare class BaseRepository<Query, T> implements IBaseRepository<Query, T> {
    repo(): Repository<any>;
    findOne(params: Query): Promise<T>;
    find(params: Query): Promise<T[]>;
    findById(id: EntityId): Promise<T>;
    findByIds(ids: any): Promise<T[]>;
    save(params: any, queryRunner?: QueryRunner): Promise<T>;
    saves(params: T[]): Promise<T[]>;
    deleteById(id: EntityId): Promise<DeleteResult>;
    deleteByIds(ids: EntityId[]): Promise<DeleteResult>;
    delete(params: Query, queryRunner?: QueryRunner): Promise<DeleteResult>;
    count(params: T): Promise<number>;
}
