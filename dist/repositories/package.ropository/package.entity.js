"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PackageEntitySubscriber = exports.PackageEntity = exports.PACKAGES_TABLE = void 0;
const const_1 = require("../../const");
const typeorm_1 = require("typeorm");
exports.PACKAGES_TABLE = 'packages';
let PackageEntity = class PackageEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], PackageEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], PackageEntity.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ type: 'float4', nullable: true }),
    __metadata("design:type", Number)
], PackageEntity.prototype, "discount_rate", void 0);
__decorate([
    typeorm_1.Column({ type: 'float4', nullable: true }),
    __metadata("design:type", Number)
], PackageEntity.prototype, "discount_amount", void 0);
__decorate([
    typeorm_1.Column({ type: 'boolean', default: true }),
    __metadata("design:type", Boolean)
], PackageEntity.prototype, "status", void 0);
__decorate([
    typeorm_1.Column({ type: 'float4', nullable: true }),
    __metadata("design:type", Number)
], PackageEntity.prototype, "quantity", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], PackageEntity.prototype, "type", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    __metadata("design:type", String)
], PackageEntity.prototype, "owner_created", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], PackageEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], PackageEntity.prototype, "updated_at", void 0);
PackageEntity = __decorate([
    typeorm_1.Entity(exports.PACKAGES_TABLE)
], PackageEntity);
exports.PackageEntity = PackageEntity;
let PackageEntitySubscriber = class PackageEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
PackageEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], PackageEntitySubscriber);
exports.PackageEntitySubscriber = PackageEntitySubscriber;
//# sourceMappingURL=package.entity.js.map