"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PackageRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const package_types_1 = require("../../domains/setting/package.types");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const package_entity_1 = require("./package.entity");
class PackageRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(package_entity_1.PackageEntity, app_setting_1.ConnectionName.user_role);
    }
    find(params) {
        return this.repo().find({ where: params, order: { quantity: 1 } });
    }
}
exports.PackageRepository = PackageRepository;
//# sourceMappingURL=index.js.map