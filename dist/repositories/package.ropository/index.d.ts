import { QueryPackage, RawPackage } from 'src/domains/setting/package.types';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { PackageEntity } from './package.entity';
export declare class PackageRepository extends BaseRepository<QueryPackage, RawPackage> {
    repo(): Repository<PackageEntity>;
    find(params: QueryPackage): Promise<RawPackage[]>;
}
