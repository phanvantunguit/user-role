import { PACKAGE_TYPE } from 'src/const';
import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const PACKAGES_TABLE = "packages";
export declare class PackageEntity {
    id: string;
    name: string;
    discount_rate: number;
    discount_amount: number;
    status: boolean;
    quantity: number;
    type: PACKAGE_TYPE;
    owner_created: string;
    created_at: number;
    updated_at: number;
}
export declare class PackageEntitySubscriber implements EntitySubscriberInterface<PackageEntity> {
    beforeInsert(event: InsertEvent<PackageEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<PackageEntity>): Promise<void>;
}
