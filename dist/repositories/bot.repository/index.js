"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const bot_types_1 = require("../../domains/bot/bot.types");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const bot_entity_1 = require("./bot.entity");
class BotRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(bot_entity_1.BotEntity, app_setting_1.ConnectionName.user_role);
    }
    listConditionMultiple(params) {
        const { status } = params;
        const whereArray = [];
        if (status.length > 0) {
            whereArray.push(`status in (${status.map((e) => `'${e}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
      SELECT *
      FROM ${bot_entity_1.BOT_TABLE}  
      ${where} 
      order by "order" ASC
    `;
        return this.repo().query(query);
    }
}
exports.BotRepository = BotRepository;
//# sourceMappingURL=index.js.map