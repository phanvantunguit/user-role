import { QueryBot, RawBot } from 'src/domains/bot/bot.types';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { BotEntity } from './bot.entity';
export declare class BotRepository extends BaseRepository<QueryBot, RawBot> {
    repo(): Repository<BotEntity>;
    listConditionMultiple(params: {
        status?: string[];
    }): Promise<RawBot[]>;
}
