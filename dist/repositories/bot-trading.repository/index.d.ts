import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { BotTradingEntity } from './bot-trading.entity';
export declare class BotTradingRepository extends BaseRepository<BotTradingEntity, BotTradingEntity> {
    repo(): Repository<BotTradingEntity>;
    listConditionMultiple(params: {
        status?: string[];
        id?: string[];
    }): Promise<BotTradingEntity[]>;
}
