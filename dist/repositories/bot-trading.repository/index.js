"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotTradingRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const bot_trading_entity_1 = require("./bot-trading.entity");
class BotTradingRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(bot_trading_entity_1.BotTradingEntity, app_setting_1.ConnectionName.user_role);
    }
    listConditionMultiple(params) {
        const { status, id } = params;
        const whereArray = [];
        if (status.length > 0) {
            whereArray.push(`bt.status in (${status.map((e) => `'${e}'`)})`);
        }
        if (id && id.length > 0) {
            whereArray.push(`bt.id in (${id.map((e) => `'${e}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
      SELECT bt.*, count(ual.id) real_bought
      FROM ${bot_trading_entity_1.BOT_TRADING_TABLE} bt
      Left Join (
        select * from user_asset_logs ual
          where ual.status in ('ACTIVE', 'NOT_CONNECT')
        ) ual on ual.asset_id = bt.id
      ${where} 
      group by bt.id
      order by "order" ASC
    `;
        return this.repo().query(query);
    }
}
exports.BotTradingRepository = BotTradingRepository;
//# sourceMappingURL=index.js.map