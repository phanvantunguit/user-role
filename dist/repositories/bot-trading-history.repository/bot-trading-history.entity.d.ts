import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const BOT_TRADING_HISTORY_TABLE = "bot_tradings_history";
export declare class BotTradingHistoryEntity {
    id?: string;
    user_id?: string;
    subscriber_id?: string;
    broker_account?: string;
    bot_id?: string;
    bot_code?: string;
    owner_created?: string;
    token_first?: string;
    token_second?: string;
    status?: string;
    side?: string;
    entry_price?: string;
    close_price?: string;
    profit?: string;
    profit_cash?: number;
    commission?: number;
    swap?: number;
    volume?: number;
    day_started?: number;
    day_completed?: number;
    broker_comment?: string;
    created_at?: number;
    updated_at?: number;
}
export declare class BotTradingHistoryEntitySubscriber implements EntitySubscriberInterface<BotTradingHistoryEntity> {
    beforeInsert(event: InsertEvent<BotTradingHistoryEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<BotTradingHistoryEntity>): Promise<void>;
}
