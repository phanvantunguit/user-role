"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotTradingHistoryEntitySubscriber = exports.BotTradingHistoryEntity = exports.BOT_TRADING_HISTORY_TABLE = void 0;
const uuid_1 = require("uuid");
const typeorm_1 = require("typeorm");
exports.BOT_TRADING_HISTORY_TABLE = 'bot_tradings_history';
let BotTradingHistoryEntity = class BotTradingHistoryEntity {
};
__decorate([
    typeorm_1.PrimaryColumn({ type: 'varchar' }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid', nullable: true }),
    typeorm_1.Index(),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "user_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "subscriber_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "broker_account", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid', nullable: true }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "bot_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "bot_code", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid', nullable: true }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "owner_created", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "token_first", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "token_second", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "status", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "side", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "entry_price", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "close_price", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "profit", void 0);
__decorate([
    typeorm_1.Column({ type: 'float4', nullable: true }),
    __metadata("design:type", Number)
], BotTradingHistoryEntity.prototype, "profit_cash", void 0);
__decorate([
    typeorm_1.Column({ type: 'float4', nullable: true }),
    __metadata("design:type", Number)
], BotTradingHistoryEntity.prototype, "commission", void 0);
__decorate([
    typeorm_1.Column({ type: 'float4', nullable: true }),
    __metadata("design:type", Number)
], BotTradingHistoryEntity.prototype, "swap", void 0);
__decorate([
    typeorm_1.Column({ type: 'float4', nullable: true }),
    __metadata("design:type", Number)
], BotTradingHistoryEntity.prototype, "volume", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', nullable: true }),
    __metadata("design:type", Number)
], BotTradingHistoryEntity.prototype, "day_started", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', nullable: true }),
    __metadata("design:type", Number)
], BotTradingHistoryEntity.prototype, "day_completed", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], BotTradingHistoryEntity.prototype, "broker_comment", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], BotTradingHistoryEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], BotTradingHistoryEntity.prototype, "updated_at", void 0);
BotTradingHistoryEntity = __decorate([
    typeorm_1.Entity(exports.BOT_TRADING_HISTORY_TABLE)
], BotTradingHistoryEntity);
exports.BotTradingHistoryEntity = BotTradingHistoryEntity;
let BotTradingHistoryEntitySubscriber = class BotTradingHistoryEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
        if (!event.entity.id) {
            event.entity.id = uuid_1.v4();
        }
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
BotTradingHistoryEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], BotTradingHistoryEntitySubscriber);
exports.BotTradingHistoryEntitySubscriber = BotTradingHistoryEntitySubscriber;
//# sourceMappingURL=bot-trading-history.entity.js.map