import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { BotTradingHistoryEntity } from './bot-trading-history.entity';
export declare class BotTradingHistoryRepository extends BaseRepository<BotTradingHistoryEntity, BotTradingHistoryEntity> {
    repo(): Repository<BotTradingHistoryEntity>;
    listConditionMultiple(params: {
        status?: string[];
        connected_at?: number;
    }): Promise<BotTradingHistoryEntity[]>;
    getPaging(params: {
        from?: number;
        to?: number;
        page?: number;
        size?: number;
        status?: string;
        user_id?: string;
        bot_id?: string;
        subscriber_id?: string;
        connected_at?: number;
        bot_code?: string;
    }): Promise<{
        rows: any;
        page: number;
        size: number;
        count: any;
        total: number;
    }>;
    deleteDataWithoutUser(params: {
        bot_id: any;
    }): Promise<any>;
    chartPNL(param: {
        from?: number;
        to?: number;
        bot_id: string;
        user_id?: string;
        timezone: string;
        time_type: 'DAY' | 'MONTH';
        subscriber_id?: string;
        connected_at?: number;
    }): Promise<any>;
    getProfit(param: {
        from?: number;
        to?: number;
        bot_id: string;
        user_id: string;
        subscriber_id?: string;
        connected_at?: number;
    }): Promise<any>;
    deleteDataOfUserBot(param: {
        user_id: string;
        bot_id: string;
    }): Promise<any>;
    count(param: {
        user_id?: string;
        bot_id?: string;
        connected_at?: number;
    }): Promise<number>;
}
