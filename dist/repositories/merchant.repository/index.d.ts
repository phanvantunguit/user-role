import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { MerchantEntity } from './merchant.entity';
export declare class MerchantRepository extends BaseRepository<MerchantEntity, MerchantEntity> {
    repo(): Repository<MerchantEntity>;
    findMerchant(params: {
        status?: string;
        domain_type?: string;
    }): Promise<MerchantEntity[]>;
}
