import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { AdditionalDataEntity } from './additional-data.entity';
import { ADDITIONAL_DATA_TYPE } from 'src/domains/merchant/types';
export declare class AdditionalDataRepository extends BaseRepository<AdditionalDataEntity, AdditionalDataEntity> {
    repo(): Repository<AdditionalDataEntity>;
    list(name?: string, type?: ADDITIONAL_DATA_TYPE): Promise<any>;
}
