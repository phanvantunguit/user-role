"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdditionalDataRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const additional_data_entity_1 = require("./additional-data.entity");
const types_1 = require("../../domains/merchant/types");
class AdditionalDataRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(additional_data_entity_1.AdditionalDataEntity, app_setting_1.ConnectionName.user_role);
    }
    list(name, type) {
        const whereArray = [];
        if (name) {
            whereArray.push(`(name = '${name}')`);
        }
        if (type) {
            whereArray.push(`type = '${type}'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
    SELECT *
    FROM ${additional_data_entity_1.ADDITIONAL_DATA_TABLE}
    ${where} 
    order by created_at DESC
  `;
        return this.repo().query(query);
    }
}
exports.AdditionalDataRepository = AdditionalDataRepository;
//# sourceMappingURL=index.js.map