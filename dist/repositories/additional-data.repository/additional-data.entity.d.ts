import { EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
export declare const ADDITIONAL_DATA_TABLE = "additional_data";
export declare class AdditionalDataEntity {
    id?: string;
    type?: string;
    name?: string;
    data?: any;
    created_at?: number;
    updated_at?: number;
}
export declare class AdditionalDataEntitySubscriber implements EntitySubscriberInterface<AdditionalDataEntity> {
    beforeInsert(event: InsertEvent<AdditionalDataEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<AdditionalDataEntity>): Promise<void>;
}
