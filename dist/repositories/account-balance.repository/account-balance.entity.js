"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountBalanceEntitySubscriber = exports.AccountBalanceEntity = exports.ACCOUNT_BALANCES_TABLE = void 0;
const typeorm_1 = require("typeorm");
const date_fns_1 = require("date-fns");
const uuid_1 = require("uuid");
exports.ACCOUNT_BALANCES_TABLE = 'account_balances';
let AccountBalanceEntity = class AccountBalanceEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], AccountBalanceEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid', nullable: true }),
    typeorm_1.Index(),
    __metadata("design:type", String)
], AccountBalanceEntity.prototype, "user_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    typeorm_1.Index(),
    __metadata("design:type", String)
], AccountBalanceEntity.prototype, "bot_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], AccountBalanceEntity.prototype, "broker_account", void 0);
__decorate([
    typeorm_1.Column({ type: 'float' }),
    __metadata("design:type", Number)
], AccountBalanceEntity.prototype, "balance", void 0);
__decorate([
    typeorm_1.Column({ type: 'boolean', default: false }),
    __metadata("design:type", Boolean)
], AccountBalanceEntity.prototype, "change_by_user", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], AccountBalanceEntity.prototype, "change_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], AccountBalanceEntity.prototype, "date", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], AccountBalanceEntity.prototype, "created_at", void 0);
AccountBalanceEntity = __decorate([
    typeorm_1.Entity(exports.ACCOUNT_BALANCES_TABLE),
    typeorm_1.Unique('user_id_bot_id_change_id_ab_un', ['user_id', 'bot_id', 'change_id'])
], AccountBalanceEntity);
exports.AccountBalanceEntity = AccountBalanceEntity;
let AccountBalanceEntitySubscriber = class AccountBalanceEntitySubscriber {
    async beforeInsert(event) {
        if (!event.entity.created_at) {
            event.entity.created_at = Date.now();
        }
        if (!event.entity.change_id) {
            event.entity.change_id = uuid_1.v4();
        }
        event.entity.date = date_fns_1.format(event.entity.created_at, 'ddMMyy');
    }
};
AccountBalanceEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], AccountBalanceEntitySubscriber);
exports.AccountBalanceEntitySubscriber = AccountBalanceEntitySubscriber;
//# sourceMappingURL=account-balance.entity.js.map