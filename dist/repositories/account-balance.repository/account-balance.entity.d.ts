import { EntitySubscriberInterface, InsertEvent } from 'typeorm';
export declare const ACCOUNT_BALANCES_TABLE = "account_balances";
export declare class AccountBalanceEntity {
    id?: string;
    user_id?: string;
    bot_id?: string;
    broker_account?: string;
    balance?: number;
    change_by_user?: boolean;
    change_id?: string;
    date?: string;
    created_at?: number;
}
export declare class AccountBalanceEntitySubscriber implements EntitySubscriberInterface<AccountBalanceEntity> {
    beforeInsert(event: InsertEvent<AccountBalanceEntity>): Promise<void>;
}
