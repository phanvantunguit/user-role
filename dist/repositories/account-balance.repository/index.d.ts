import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { AccountBalanceEntity } from './account-balance.entity';
export declare class AccountBalanceRepository extends BaseRepository<AccountBalanceEntity, AccountBalanceEntity> {
    repo(): Repository<AccountBalanceEntity>;
    chartPNL(param: {
        bot_id: string;
        user_id: string;
    }): Promise<any>;
    getBalance(param: {
        bot_id: string;
        user_id: string;
        from?: number;
        order?: 'desc' | 'asc';
    }): Promise<any>;
    getBalanceByUser(param: {
        bot_id: string;
        user_id?: string;
        type?: 'cashout' | 'cashin';
        to?: number;
    }): Promise<any>;
    getAllOfAccounts(param: {
        user_id?: string;
    }): Promise<{
        user_id: string;
        broker_account: string;
    }[]>;
    deleteAccountBalance(param: {
        user_id: string;
        bot_id: string;
    }): Promise<any>;
}
