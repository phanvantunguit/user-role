"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionLogRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const transaction_log_types_1 = require("../../domains/transaction/transaction-log.types");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const transaction_log_entity_1 = require("./transaction-log.entity");
class TransactionLogRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(transaction_log_entity_1.TransactionLogEntity, app_setting_1.ConnectionName.user_role);
    }
}
exports.TransactionLogRepository = TransactionLogRepository;
//# sourceMappingURL=index.js.map