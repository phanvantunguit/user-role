import { QueryTransactionLog, RawTransactionLog } from 'src/domains/transaction/transaction-log.types';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { TransactionLogEntity } from './transaction-log.entity';
export declare class TransactionLogRepository extends BaseRepository<QueryTransactionLog, RawTransactionLog> {
    repo(): Repository<TransactionLogEntity>;
}
