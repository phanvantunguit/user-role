import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const AUTH_USER_ROLES_TABLE = "auth_user_roles";
export declare class AuthUserRoleEntity {
    id: string;
    auth_role_id: string;
    user_id: string;
    description: string;
    owner_created: string;
    created_at: number;
    updated_at: number;
}
export declare class AuthUserRoleEntitySubscriber implements EntitySubscriberInterface<AuthUserRoleEntity> {
    beforeInsert(event: InsertEvent<AuthUserRoleEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<AuthUserRoleEntity>): Promise<void>;
}
