import { EntitySubscriberInterface, InsertEvent } from 'typeorm';
export declare const USER_LOGS_TABLE = "user_logs";
export declare class UserLogEntity {
    id: string;
    user_id: string;
    ip_number: string;
    browser_type: string;
    created_at: number;
}
export declare class UserLogEntitySubscriber implements EntitySubscriberInterface<UserLogEntity> {
    beforeInsert(event: InsertEvent<UserLogEntity>): Promise<void>;
}
