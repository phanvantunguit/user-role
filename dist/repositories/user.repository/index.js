"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const typeorm_1 = require("typeorm");
const user_entity_1 = require("./user.entity");
const verify_token_entity_1 = require("./verify-token.entity");
const user_1 = require("../../domains/user");
const format_data_util_1 = require("../../utils/format-data.util");
const auth_user_role_entity_1 = require("./auth_user_role.entity");
const user_role_entity_1 = require("./user-role.entity");
const session_entity_1 = require("./session.entity");
const user_view_entity_1 = require("./user-view.entity");
const permission_1 = require("../../domains/permission");
const user_bot_entity_1 = require("./user-bot.entity");
const user_bot_types_1 = require("../../domains/user/user-bot.types");
const user_asset_log_types_1 = require("../../domains/user/user-asset-log.types");
const user_log_entity_1 = require("./user-log.entity");
const user_asset_log_entity_1 = require("./user-asset-log.entity");
const app_setting_1 = require("../../const/app-setting");
const transaction_1 = require("../../const/transaction");
const user_bot_trading_entity_1 = require("./user-bot-trading.entity");
const user_bot_trading_types_1 = require("../../domains/user/user-bot-trading.types");
class UserRepository {
    userRepo() {
        return typeorm_1.getRepository(user_entity_1.UserEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveUser(params) {
        if (params.email) {
            params.email = params.email.toLocaleLowerCase();
        }
        return this.userRepo().save(params);
    }
    async findOneUser(params) {
        const query = format_data_util_1.cleanObject(params);
        return this.userRepo().findOne(query);
    }
    async findUser(params) {
        let { keyword, is_admin, email, merchant_code } = params;
        const whereArray = [];
        if (is_admin) {
            whereArray.push(`is_admin = ${is_admin}`);
        }
        if (keyword) {
            keyword = keyword.toLowerCase();
            whereArray.push(`(email LIKE '%${keyword}%'
            OR lower(username) LIKE '%${keyword}%'
            OR phone LIKE '%${keyword}%'
            OR lower(first_name) LIKE '%${keyword}%'
            OR lower(last_name) LIKE '%${keyword}%')
            `);
        }
        if (email) {
            whereArray.push(`email = '${email}'`);
        }
        if (merchant_code && merchant_code.length > 0) {
            whereArray.push(`merchant_code in (${merchant_code.map((e) => `'${e}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryUser = `
    SELECT  *
    FROM ${user_entity_1.USERS_TABLE}  
    ${where}  
    ORDER BY created_at DESC`;
        return this.userRepo().query(queryUser);
    }
    userViewRepo() {
        return typeorm_1.getRepository(user_view_entity_1.UserViewEntity, app_setting_1.ConnectionName.user_role);
    }
    async findOneUserView(params) {
        const query = format_data_util_1.cleanObject(params);
        return this.userViewRepo().findOne(query);
    }
    async findUserViewPaging(params) {
        let { page, size, keyword } = params;
        const { is_admin, roles } = params;
        page = Number(page || 1);
        size = Number(size || 50);
        const whereArray = [];
        if (keyword) {
            keyword = keyword.toLowerCase();
            const keywords = keyword.split(' ');
            if (keywords.length > 1) {
                const last_name = keywords[keywords.length - 1];
                const first_name = keywords
                    .filter((e, i) => i < keywords.length - 1)
                    .join(' ');
                whereArray.push(`(email LIKE '%${keyword}%'
          OR lower(username) LIKE '%${keyword}%'
          OR phone LIKE '%${keyword}%'
          OR (lower(first_name) LIKE '%${first_name}%'
          AND lower(last_name) LIKE '%${last_name}%'))
          `);
            }
            else {
                whereArray.push(`(email LIKE '%${keyword}%'
        OR lower(username) LIKE '%${keyword}%'
        OR phone LIKE '%${keyword}%'
        OR lower(first_name) LIKE '%${keyword}%'
        OR lower(last_name) LIKE '%${keyword}%')
        `);
            }
        }
        if (is_admin) {
            whereArray.push(`is_admin = ${is_admin}`);
            const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
            const queryUser = `
    SELECT * 
    FROM (
      SELECT 
      u.*,
      COALESCE(json_agg(DISTINCT ar.role_name) FILTER (WHERE ar.role_name IS NOT NULL), '[]') AS auth_roles
      FROM ${user_entity_1.USERS_TABLE} u  
      left join auth_user_roles aur on aur.user_id = u.id 
      left join auth_roles ar on ar.id = aur.auth_role_id 
      group by u.id
    ) as utemp
    ${where}
    ORDER BY utemp.created_at DESC
    OFFSET ${(page - 1) * size}
    LIMIT ${size}
    `;
            const queryCount = `
    SELECT COUNT(*)
    FROM (
      SELECT 
      u.*,
      COALESCE(json_agg(DISTINCT r.role_name) FILTER (WHERE r.role_name IS NOT NULL), '[]') AS roles
      FROM ${user_entity_1.USERS_TABLE} u  
      left join user_roles ur on ur.user_id = u.id 
      left join roles r on r.id = ur.role_id 
      group by u.id
    ) as utemp
    ${where}
    `;
            const [rows, total] = await Promise.all([
                this.userRepo().query(queryUser),
                this.userRepo().query(queryCount),
            ]);
            return {
                rows,
                page,
                size,
                count: rows.length,
                total: Number(total[0].count),
            };
        }
        else {
            if (roles && roles.length > 0) {
                const whereRoles = roles.map((r) => `utemp.roles::jsonb ? '${r.role_name}'`);
                whereArray.push(`(${whereRoles.join(' OR ')})`);
            }
            const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
            const queryUser = `
      SELECT * 
      FROM (
        SELECT 
        u.*,
        COALESCE(json_agg(DISTINCT r.role_name) FILTER (WHERE r.role_name IS NOT NULL), '[]') AS roles
        FROM ${user_entity_1.USERS_TABLE} u  
        left join user_roles ur on ur.user_id = u.id 
        left join roles r on r.id = ur.role_id 
        group by u.id
      ) as utemp
      ${where}
      ORDER BY utemp.created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
      `;
            const queryCount = `
      SELECT COUNT(*)
      FROM (
        SELECT 
        u.*,
        COALESCE(json_agg(DISTINCT r.role_name) FILTER (WHERE r.role_name IS NOT NULL), '[]') AS roles
        FROM ${user_entity_1.USERS_TABLE} u  
        left join user_roles ur on ur.user_id = u.id 
        left join roles r on r.id = ur.role_id 
        group by u.id
      ) as utemp
      ${where}
      `;
            const [rows, total] = await Promise.all([
                this.userRepo().query(queryUser),
                this.userRepo().query(queryCount),
            ]);
            return {
                rows,
                page,
                size,
                count: rows.length,
                total: Number(total[0].count),
            };
        }
    }
    async findUserPaging(param) {
        let { page, size, keyword } = param;
        const { from, to, merchant_code, roles, bots } = param;
        page = Number(page || 1);
        size = Number(size || 50);
        const whereArray = [];
        if (from) {
            whereArray.push(`created_at >= ${from}`);
        }
        if (to) {
            whereArray.push(`created_at <= ${to}`);
        }
        if (merchant_code) {
            whereArray.push(`merchant_code = '${merchant_code}'`);
        }
        if (keyword) {
            keyword = keyword.toLowerCase();
            const keywords = keyword.split(' ');
            if (keywords.length > 1) {
                const last_name = keywords[keywords.length - 1];
                const first_name = keywords
                    .filter((e, i) => i < keywords.length - 1)
                    .join(' ');
                whereArray.push(`(email LIKE '%${keyword}%'
            OR lower(username) LIKE '%${keyword}%'
            OR phone LIKE '%${keyword}%'
            OR (lower(first_name) LIKE '%${first_name}%'
            AND lower(last_name) LIKE '%${last_name}%'))
            `);
            }
            else {
                whereArray.push(`(email LIKE '%${keyword}%'
          OR lower(username) LIKE '%${keyword}%'
          OR phone LIKE '%${keyword}%'
          OR lower(first_name) LIKE '%${keyword}%'
          OR lower(last_name) LIKE '%${keyword}%')
          `);
            }
        }
        if (roles.length > 0) {
            const whereRoles = roles.map((name) => `roles::jsonb ? '${name}'`);
            const whereLogRoles = roles.map((name) => `log_roles::jsonb ? '${name}'`);
            whereArray.push(`(${whereRoles.join(' OR ')} OR ${whereLogRoles.join(' OR ')})`);
        }
        if (bots.length > 0) {
            const whereBots = bots.map((name) => `bots::jsonb ? '${name}'`);
            whereArray.push(`(${whereBots.join(' OR ')})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryUser = `
      SELECT *
      FROM
      (SELECT u.id,
        u.email,
        u.username,
        u.phone,
        u.first_name,
        u.last_name,
        u.address,
        u.affiliate_code,
        u.link_affiliate,
        u.referral_code,
        u.profile_pic,
        u.active,
        u.email_confirmed,
        u.note_updated,
        u.date_registered,
        u.country,
        u.year_of_birth,
        u.gender,
        u.super_user,
        u.is_admin,
        u.phone_code,
        u.merchant_code,
        u.created_at,
        u.updated_at,
        COALESCE(json_agg(DISTINCT r.role_name) FILTER (WHERE r.role_name IS NOT NULL), '[]') AS roles,
        COALESCE(json_agg(DISTINCT ualr.name) FILTER (WHERE ualr.name IS NOT NULL), '[]') AS log_roles,
        COALESCE(json_agg(DISTINCT ualb.name) FILTER (WHERE ualb.name IS NOT NULL), '[]') AS bots
        FROM users as u
        LEFT JOIN user_roles ur on ur.user_id = u.id 
        LEFT JOIN roles r on r.id = ur.role_id
        LEFT JOIN user_asset_logs ualb on ualb.user_id = u.id and ualb.category = 'BOT'
        LEFT JOIN user_asset_logs ualr on ualr.user_id = u.id and ualr.category = 'PKG' and ualr.status != 'PROCESSING'
        GROUP BY u.id) as utemp
      ${where}  
      ORDER BY created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `;
        const queryCount = `
    SELECT COUNT(*)
    FROM (SELECT u.*,
      COALESCE(json_agg(DISTINCT r.role_name) FILTER (WHERE r.role_name IS NOT NULL), '[]') AS roles,
      COALESCE(json_agg(DISTINCT ualr.name) FILTER (WHERE ualr.name IS NOT NULL), '[]') AS log_roles,
      COALESCE(json_agg(DISTINCT ualb.name) FILTER (WHERE ualb.name IS NOT NULL), '[]') AS bots
      FROM users as u
      LEFT JOIN user_roles ur on ur.user_id = u.id 
      LEFT JOIN roles r on r.id = ur.role_id 
      LEFT JOIN user_asset_logs ualb on ualb.user_id = u.id and ualb.category = 'BOT'
      LEFT JOIN user_asset_logs ualr on ualr.user_id = u.id and ualr.category = 'PKG' and ualr.status != 'PROCESSING'
      GROUP BY u.id) as utemp
    ${where} 
    `;
        const [rows, total] = await Promise.all([
            this.userRepo().query(queryUser),
            this.userRepo().query(queryCount),
        ]);
        return {
            rows,
            page,
            size,
            count: rows.length,
            total: Number(total[0].count),
        };
    }
    verifyTokenRepo() {
        return typeorm_1.getRepository(verify_token_entity_1.VerifyTokenEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveVerifyToken(params) {
        return this.verifyTokenRepo().save(params);
    }
    async findOneToken(params) {
        const query = format_data_util_1.cleanObject(params);
        return this.verifyTokenRepo().findOne(query);
    }
    async findToken(params) {
        const query = format_data_util_1.cleanObject(params);
        return this.verifyTokenRepo().find(query);
    }
    async deleteToken(params) {
        const query = format_data_util_1.cleanObject(params);
        return this.verifyTokenRepo().delete(query);
    }
    authUserRoleRepo() {
        return typeorm_1.getRepository(auth_user_role_entity_1.AuthUserRoleEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveAuthUserRole(params) {
        return this.authUserRoleRepo().save(params);
    }
    async findAuthUserRole(params) {
        return this.authUserRoleRepo().find({
            where: params,
            order: { created_at: 'DESC' },
        });
    }
    async deleteAuthUserRoleByIds(ids) {
        return this.authUserRoleRepo().delete(ids);
    }
    userRoleRepo() {
        return typeorm_1.getRepository(user_role_entity_1.UserRoleEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveUserRole(params, queryRunner) {
        if (queryRunner) {
            const transaction = await this.userRoleRepo().create(params);
            return queryRunner.manager.save(transaction);
        }
        else {
            return this.userRoleRepo().save(params);
        }
    }
    async findUserRole(params) {
        return this.userRoleRepo().find({
            where: params,
            order: { created_at: 'DESC' },
        });
    }
    async findUserRoleByUserIdsAndRoleIds(userIds, roleIds) {
        const whereArray = [];
        if (userIds.length > 0) {
            whereArray.push(`user_id in (${userIds.map((user_id) => `'${user_id}'`)})`);
        }
        if (userIds.length > 0) {
            whereArray.push(`role_id in (${roleIds.map((role_id) => `'${role_id}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
      SELECT *
      FROM ${user_role_entity_1.USER_ROLE_TABLE}  
      ${where} 
      order by created_at DESC
    `;
        return this.userRoleRepo().query(query);
    }
    async deleteUserRoleByIds(ids) {
        return this.userRoleRepo().delete(ids);
    }
    async deleteUserRoles(userIds, roleIds) {
        const whereArray = [];
        if (userIds.length > 0) {
            whereArray.push(`user_id in (${userIds.map((user_id) => `'${user_id}'`)})`);
        }
        if (userIds.length > 0) {
            whereArray.push(`role_id in (${roleIds.map((role_id) => `'${role_id}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
      DELETE
      FROM ${user_role_entity_1.USER_ROLE_TABLE}  
      ${where}
    `;
        return this.userRoleRepo().query(query);
    }
    sessionRepo() {
        return typeorm_1.getRepository(session_entity_1.SessionEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveSession(params) {
        return this.sessionRepo().save(params);
    }
    async findSessionById(id) {
        return this.sessionRepo().findOne(id);
    }
    findLastSession(params) {
        return this.sessionRepo().findOne({
            where: params,
            order: { created_at: -1 },
        });
    }
    findSessionLogin(params, limit) {
        const paramsClear = format_data_util_1.cleanObject(params);
        const whereArray = format_data_util_1.formatQueryArray(paramsClear);
        whereArray.push('websocket_id is null');
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
      SELECT *
      FROM ${session_entity_1.SESSIONS_TABLE}  
      ${where} 
      order by created_at DESC
      limit ${limit}
    `;
        return this.sessionRepo().query(query);
    }
    findSessionWS(params, limit) {
        const paramsClear = format_data_util_1.cleanObject(params);
        const whereArray = format_data_util_1.formatQueryArray(paramsClear);
        whereArray.push('websocket_id is not null');
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
      SELECT *
      FROM ${session_entity_1.SESSIONS_TABLE}  
      ${where} 
      order by created_at DESC
      limit ${limit}
    `;
        return this.sessionRepo().query(query);
    }
    userLogRepo() {
        return typeorm_1.getRepository(user_log_entity_1.UserLogEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveUserLog(params) {
        return this.userLogRepo().save(params);
    }
    async findUserLog(params) {
        const query = format_data_util_1.cleanObject(params);
        return this.sessionRepo().find({ where: query, order: { created_at: -1 } });
    }
    userBotRepo() {
        return typeorm_1.getRepository(user_bot_entity_1.UserBotEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveUserBot(params, queryRunner) {
        if (queryRunner) {
            const transaction = await this.userBotRepo().create(params);
            return queryRunner.manager.save(transaction);
        }
        else {
            return this.userBotRepo().save(params);
        }
    }
    async deleteUserBot(params) {
        const { bot_id, user_id } = params;
        return this.userBotRepo().delete({ user_id, bot_id });
    }
    async findUserBot(params) {
        return this.userBotRepo().find({
            where: params,
            order: { created_at: 'DESC' },
        });
    }
    async findUserBotSQL(params) {
        const { user_id, bot_ids, status, expires_at } = params;
        const whereArray = [];
        if (user_id) {
            whereArray.push(`user_id = '${user_id}'`);
        }
        if (status) {
            whereArray.push(`status = '${status}'`);
        }
        if (expires_at) {
            whereArray.push(`expires_at > ${expires_at}`);
        }
        if (bot_ids) {
            whereArray.push(`bot_id in (${bot_ids.map((e) => `'${e}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
    SELECT *
    FROM ${user_bot_entity_1.USER_BOT_TABLE}  
    ${where} 
    order by created_at DESC
  `;
        return this.userBotRepo().query(query);
    }
    userAssetLogRepo() {
        return typeorm_1.getRepository(user_asset_log_entity_1.UserAssetLogEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveUserAssetLog(params, queryRunner) {
        if (queryRunner) {
            const transaction = await this.userAssetLogRepo().create(params);
            return queryRunner.manager.save(transaction);
        }
        else {
            return this.userAssetLogRepo().save(params);
        }
    }
    async findUserAssetLog(params) {
        return this.userAssetLogRepo().find({
            where: params,
            order: { created_at: 'DESC' },
        });
    }
    async findUserAssetLogPaging(params) {
        let { page, size, status, category, keyword, user_id, is_order } = params;
        page = Number(page || 1);
        size = Number(size || 50);
        const whereArray = [];
        if (user_id) {
            whereArray.push(`user_id = '${user_id}'`);
        }
        if (status && status.length > 0) {
            whereArray.push(`status in (${status.map((e) => `'${e}'`)})`);
        }
        if (category) {
            whereArray.push(`category = '${category}'`);
        }
        if (keyword) {
            whereArray.push(`(name LIKE '%${keyword}%'
        OR order_id LIKE '%${keyword}%')`);
        }
        if (is_order) {
            whereArray.push(`order_id is not null`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryAsset = `
      SELECT  *
      FROM ${user_asset_log_entity_1.USER_ASSET_LOG_TABLE}  
      ${where}  
      ORDER BY updated_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `;
        const queryCount = `
    SELECT COUNT(*)
    FROM ${user_asset_log_entity_1.USER_ASSET_LOG_TABLE}  
    ${where}
    `;
        const [rows, total] = await Promise.all([
            this.userAssetLogRepo().query(queryAsset),
            this.userAssetLogRepo().query(queryCount),
        ]);
        return {
            rows,
            page,
            size,
            count: rows.length,
            total: Number(total[0].count),
        };
    }
    async getAllOfUserTradingBots(param) {
        const { user_id } = param;
        const whereArray = [];
        if (user_id) {
            whereArray.push(`user_id = '${user_id}'`);
        }
        whereArray.push(`status = '${transaction_1.ITEM_STATUS.NOT_CONNECT}'`);
        whereArray.push(`category = '${transaction_1.ORDER_CATEGORY.TBOT}'`);
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
    SELECT user_id, asset_id
    FROM ${user_asset_log_entity_1.USER_ASSET_LOG_TABLE} 
    ${where}
    group by user_id, asset_id
    `;
        return this.userAssetLogRepo().query(query);
    }
    userBotTradingRepo() {
        return typeorm_1.getRepository(user_bot_trading_entity_1.UserBotTradingEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveUserBotTrading(params, queryRunner) {
        if (queryRunner) {
            const transaction = await this.userBotTradingRepo().create(params);
            return queryRunner.manager.save(transaction);
        }
        else {
            return this.userBotTradingRepo().save(params);
        }
    }
    async deleteUserBotTrading(params) {
        return this.userBotTradingRepo().delete(params);
    }
    async findUserBotTrading(params) {
        return this.userBotTradingRepo().find({
            where: params,
            order: { created_at: 'DESC' },
        });
    }
    async findUserBotTradingSQL(params) {
        const { user_id, bot_ids, status, expires_at, expired, type } = params;
        const whereArray = [];
        if (user_id) {
            whereArray.push(`user_id = '${user_id}'`);
        }
        if (status && status.length > 0) {
            whereArray.push(`status in (${status.map((e) => `'${e}'`)})`);
        }
        if (expires_at) {
            whereArray.push(`expires_at > ${expires_at}`);
        }
        if (expired) {
            whereArray.push(`expires_at < ${expired}`);
            whereArray.push(`status != '${transaction_1.ITEM_STATUS.EXPIRED}'`);
        }
        if (bot_ids && bot_ids.length > 0) {
            whereArray.push(`bot_id in (${bot_ids.map((e) => `'${e}'`)})`);
        }
        if (type) {
            whereArray.push(`type = '${type}'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
      SELECT *
      FROM ${user_bot_trading_entity_1.USER_BOT_TRADING_TABLE}  
      ${where} 
      order by created_at DESC
    `;
        return this.userBotTradingRepo().query(query);
    }
}
exports.UserRepository = UserRepository;
//# sourceMappingURL=index.js.map