"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SessionEntitySubscriber = exports.SessionEntity = exports.SESSIONS_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.SESSIONS_TABLE = 'sessions';
let SessionEntity = class SessionEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], SessionEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    typeorm_1.Index(),
    __metadata("design:type", String)
], SessionEntity.prototype, "user_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], SessionEntity.prototype, "token", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], SessionEntity.prototype, "name_device", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], SessionEntity.prototype, "browser", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], SessionEntity.prototype, "ip_number", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', nullable: true }),
    __metadata("design:type", Number)
], SessionEntity.prototype, "last_login", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', nullable: true }),
    __metadata("design:type", Number)
], SessionEntity.prototype, "expires_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'boolean', nullable: true }),
    __metadata("design:type", Boolean)
], SessionEntity.prototype, "enabled", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], SessionEntity.prototype, "websocket_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], SessionEntity.prototype, "token_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], SessionEntity.prototype, "created_at", void 0);
SessionEntity = __decorate([
    typeorm_1.Entity(exports.SESSIONS_TABLE)
], SessionEntity);
exports.SessionEntity = SessionEntity;
let SessionEntitySubscriber = class SessionEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
    }
};
SessionEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], SessionEntitySubscriber);
exports.SessionEntitySubscriber = SessionEntitySubscriber;
//# sourceMappingURL=session.entity.js.map