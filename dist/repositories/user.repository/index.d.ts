import { QueryRunner } from 'typeorm';
import { UserEntity } from './user.entity';
import { VerifyTokenEntity } from './verify-token.entity';
import { DeleteToken, QueryAuthUserRole, QuerySession, QueryToken, QueryUser, QueryUserLog, QueryUserRole, RawAuthUserRole, RawSession, RawUser, RawUserLog, RawUserRole, RawUserView, RawVerifyToken } from 'src/domains/user';
import { UserRoleEntity } from './user-role.entity';
import { SessionEntity } from './session.entity';
import { RawRole } from 'src/domains/permission';
import { UserBotEntity } from './user-bot.entity';
import { QueryUserBot, RawUserBot } from 'src/domains/user/user-bot.types';
import { QueryUserAssetLog, RawUserAssetLog } from 'src/domains/user/user-asset-log.types';
import { UserLogEntity } from './user-log.entity';
import { UserAssetLogEntity } from './user-asset-log.entity';
import { ITEM_STATUS, ORDER_CATEGORY, TBOT_TYPE } from 'src/const/transaction';
import { UserBotTradingEntity } from './user-bot-trading.entity';
import { RawUserBotTrading } from 'src/domains/user/user-bot-trading.types';
export declare class UserRepository {
    private userRepo;
    saveUser(params: RawUser): Promise<UserEntity>;
    findOneUser(params: QueryUser): Promise<UserEntity>;
    findUser(params: {
        email?: string;
        keyword?: string;
        is_admin?: boolean;
        merchant_code?: string[];
    }): Promise<UserEntity[]>;
    private userViewRepo;
    findOneUserView(params: QueryUser): Promise<RawUserView>;
    findUserViewPaging(params: {
        is_admin?: boolean;
        page?: number;
        size?: number;
        keyword?: string;
        roles?: RawRole[];
    }): Promise<{
        rows: any;
        page: number;
        size: number;
        count: any;
        total: number;
    }>;
    findUserPaging(param: {
        page?: number;
        size?: number;
        keyword?: string;
        from?: number;
        to?: number;
        merchant_code?: string;
        roles?: string[];
        bots?: string[];
    }): Promise<{
        rows: any;
        page: number;
        size: number;
        count: number;
        total: number;
    }>;
    private verifyTokenRepo;
    saveVerifyToken(params: RawVerifyToken): Promise<RawVerifyToken>;
    findOneToken(params: QueryToken): Promise<VerifyTokenEntity>;
    findToken(params: QueryToken): Promise<VerifyTokenEntity[]>;
    deleteToken(params: DeleteToken): Promise<import("typeorm").DeleteResult>;
    private authUserRoleRepo;
    saveAuthUserRole(params: RawAuthUserRole[]): Promise<RawAuthUserRole[]>;
    findAuthUserRole(params: QueryAuthUserRole): Promise<RawAuthUserRole[]>;
    deleteAuthUserRoleByIds(ids: string[]): Promise<import("typeorm").DeleteResult>;
    private userRoleRepo;
    saveUserRole(params: RawUserRole[], queryRunner?: QueryRunner): Promise<UserRoleEntity[]>;
    findUserRole(params: QueryUserRole): Promise<RawUserRole[]>;
    findUserRoleByUserIdsAndRoleIds(userIds: string[], roleIds: string[]): Promise<RawUserRole[]>;
    deleteUserRoleByIds(ids: string[]): Promise<import("typeorm").DeleteResult>;
    deleteUserRoles(userIds: string[], roleIds: string[]): Promise<any>;
    private sessionRepo;
    saveSession(params: RawSession): Promise<import("src/domains/user").Session & {
        id?: string;
        created_at?: number;
    } & SessionEntity>;
    findSessionById(id: string): Promise<SessionEntity>;
    findLastSession(params: QuerySession): Promise<SessionEntity>;
    findSessionLogin(params: QuerySession, limit: number): Promise<RawSession[]>;
    findSessionWS(params: QuerySession, limit: number): Promise<RawSession[]>;
    private userLogRepo;
    saveUserLog(params: RawUserLog): Promise<import("src/domains/user").UserLog & {
        id?: string;
        created_at?: number;
    } & UserLogEntity>;
    findUserLog(params: QueryUserLog): Promise<SessionEntity[]>;
    private userBotRepo;
    saveUserBot(params: RawUserBot[], queryRunner?: QueryRunner): Promise<UserBotEntity[]>;
    deleteUserBot(params: {
        user_id: string;
        bot_id: string;
    }): Promise<import("typeorm").DeleteResult>;
    findUserBot(params: QueryUserBot): Promise<RawUserBot[]>;
    findUserBotSQL(params: {
        user_id?: string;
        bot_ids?: string[];
        status?: ITEM_STATUS;
        expires_at?: number;
    }): Promise<RawUserBot[]>;
    private userAssetLogRepo;
    saveUserAssetLog(params: UserAssetLogEntity[], queryRunner?: QueryRunner): Promise<UserAssetLogEntity[]>;
    findUserAssetLog(params: QueryUserAssetLog): Promise<RawUserAssetLog[]>;
    findUserAssetLogPaging(params: {
        page?: number;
        size?: number;
        status?: string[];
        category?: ORDER_CATEGORY;
        keyword?: string;
        user_id?: string;
        is_order?: boolean;
    }): Promise<{
        rows: any;
        page: number;
        size: number;
        count: any;
        total: number;
    }>;
    getAllOfUserTradingBots(param: {
        user_id?: string;
    }): Promise<{
        user_id: string;
        asset_id: string;
    }[]>;
    private userBotTradingRepo;
    saveUserBotTrading(params: RawUserBotTrading[], queryRunner?: QueryRunner): Promise<UserBotTradingEntity[]>;
    deleteUserBotTrading(params: RawUserBotTrading): Promise<import("typeorm").DeleteResult>;
    findUserBotTrading(params: UserBotTradingEntity): Promise<UserBotTradingEntity[]>;
    findUserBotTradingSQL(params: {
        user_id?: string;
        bot_ids?: string[];
        status?: ITEM_STATUS[];
        expires_at?: number;
        expired?: number;
        type?: TBOT_TYPE;
    }): Promise<RawUserBotTrading[]>;
}
