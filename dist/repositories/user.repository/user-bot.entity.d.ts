import { ITEM_STATUS } from 'src/const/transaction';
import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const USER_BOT_TABLE = "user_bots";
export declare class UserBotEntity {
    id?: string;
    bot_id?: string;
    user_id?: string;
    owner_created?: string;
    expires_at: number;
    status?: ITEM_STATUS;
    created_at?: number;
    updated_at?: number;
}
export declare class UserBotEntitySubscriber implements EntitySubscriberInterface<UserBotEntity> {
    beforeInsert(event: InsertEvent<UserBotEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<UserBotEntity>): Promise<void>;
}
