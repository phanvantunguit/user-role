"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserBotEntitySubscriber = exports.UserBotEntity = exports.USER_BOT_TABLE = void 0;
const transaction_1 = require("../../const/transaction");
const typeorm_1 = require("typeorm");
exports.USER_BOT_TABLE = 'user_bots';
let UserBotEntity = class UserBotEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], UserBotEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    __metadata("design:type", String)
], UserBotEntity.prototype, "bot_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    typeorm_1.Index(),
    __metadata("design:type", String)
], UserBotEntity.prototype, "user_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    __metadata("design:type", String)
], UserBotEntity.prototype, "owner_created", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', nullable: true }),
    __metadata("design:type", Number)
], UserBotEntity.prototype, "expires_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], UserBotEntity.prototype, "status", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], UserBotEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], UserBotEntity.prototype, "updated_at", void 0);
UserBotEntity = __decorate([
    typeorm_1.Entity(exports.USER_BOT_TABLE),
    typeorm_1.Unique('sbot_id_user_id_un', ['bot_id', 'user_id'])
], UserBotEntity);
exports.UserBotEntity = UserBotEntity;
let UserBotEntitySubscriber = class UserBotEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
UserBotEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], UserBotEntitySubscriber);
exports.UserBotEntitySubscriber = UserBotEntitySubscriber;
//# sourceMappingURL=user-bot.entity.js.map