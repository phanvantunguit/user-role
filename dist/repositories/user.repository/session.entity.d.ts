import { EntitySubscriberInterface, InsertEvent } from 'typeorm';
export declare const SESSIONS_TABLE = "sessions";
export declare class SessionEntity {
    id: string;
    user_id: string;
    token: string;
    name_device: string;
    browser: string;
    ip_number: string;
    last_login: number;
    expires_at: number;
    enabled: boolean;
    websocket_id: string;
    token_id: string;
    created_at: number;
}
export declare class SessionEntitySubscriber implements EntitySubscriberInterface<SessionEntity> {
    beforeInsert(event: InsertEvent<SessionEntity>): Promise<void>;
}
