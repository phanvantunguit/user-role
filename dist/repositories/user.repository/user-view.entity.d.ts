export declare const USER_VIEW = "user_view";
export declare class UserViewEntity {
    id: string;
    email: string;
    username: string;
    phone: string;
    first_name: string;
    last_name: string;
    address: string;
    affiliate_code: string;
    link_affiliate: string;
    referral_code: string;
    profile_pic: string;
    active: boolean;
    email_confirmed: boolean;
    note_updated: string;
    date_registered: string;
    super_user: boolean;
    is_admin: boolean;
    roles: any;
    auth_roles: any;
    country: string;
    year_of_birth: string;
    gender: string;
    phone_code: string;
    created_at: number;
    updated_at: number;
}
