import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const USER_ROLE_TABLE = "user_roles";
export declare class UserRoleEntity {
    id: string;
    role_id: string;
    user_id: string;
    description: string;
    owner_created: string;
    package_id: string;
    package_type: string;
    quantity: number;
    package_name: string;
    expires_at: number;
    created_at: number;
    updated_at: number;
}
export declare class UserRoleEntitySubscriber implements EntitySubscriberInterface<UserRoleEntity> {
    beforeInsert(event: InsertEvent<UserRoleEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<UserRoleEntity>): Promise<void>;
}
