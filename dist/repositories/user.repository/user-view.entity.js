"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserViewEntity = exports.USER_VIEW = void 0;
const typeorm_1 = require("typeorm");
exports.USER_VIEW = 'user_view';
let UserViewEntity = class UserViewEntity {
};
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "email", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "username", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "phone", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "first_name", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "last_name", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "address", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "affiliate_code", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "link_affiliate", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "referral_code", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "profile_pic", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Boolean)
], UserViewEntity.prototype, "active", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Boolean)
], UserViewEntity.prototype, "email_confirmed", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "note_updated", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "date_registered", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Boolean)
], UserViewEntity.prototype, "super_user", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Boolean)
], UserViewEntity.prototype, "is_admin", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Object)
], UserViewEntity.prototype, "roles", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Object)
], UserViewEntity.prototype, "auth_roles", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "country", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "year_of_birth", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "gender", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], UserViewEntity.prototype, "phone_code", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], UserViewEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], UserViewEntity.prototype, "updated_at", void 0);
UserViewEntity = __decorate([
    typeorm_1.ViewEntity(exports.USER_VIEW, { expression: '', synchronize: false })
], UserViewEntity);
exports.UserViewEntity = UserViewEntity;
//# sourceMappingURL=user-view.entity.js.map