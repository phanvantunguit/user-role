"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserLogEntitySubscriber = exports.UserLogEntity = exports.USER_LOGS_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.USER_LOGS_TABLE = 'user_logs';
let UserLogEntity = class UserLogEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], UserLogEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    typeorm_1.Index(),
    __metadata("design:type", String)
], UserLogEntity.prototype, "user_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], UserLogEntity.prototype, "ip_number", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], UserLogEntity.prototype, "browser_type", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], UserLogEntity.prototype, "created_at", void 0);
UserLogEntity = __decorate([
    typeorm_1.Entity(exports.USER_LOGS_TABLE)
], UserLogEntity);
exports.UserLogEntity = UserLogEntity;
let UserLogEntitySubscriber = class UserLogEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
    }
};
UserLogEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], UserLogEntitySubscriber);
exports.UserLogEntitySubscriber = UserLogEntitySubscriber;
//# sourceMappingURL=user-log.entity.js.map