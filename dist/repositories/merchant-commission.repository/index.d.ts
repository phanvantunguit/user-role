import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { MerchantCommissionEntity } from './merchant-commission.entity';
export declare class MerchantCommissionRepository extends BaseRepository<MerchantCommissionEntity, MerchantCommissionEntity> {
    repo(): Repository<MerchantCommissionEntity>;
}
