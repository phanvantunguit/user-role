"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantCommissionRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const merchant_commission_entity_1 = require("./merchant-commission.entity");
class MerchantCommissionRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(merchant_commission_entity_1.MerchantCommissionEntity, app_setting_1.ConnectionName.user_role);
    }
}
exports.MerchantCommissionRepository = MerchantCommissionRepository;
//# sourceMappingURL=index.js.map