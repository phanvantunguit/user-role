"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrencyEntitySubscriber = exports.CurrencyEntity = exports.CURRENCIES_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.CURRENCIES_TABLE = 'currencies';
let CurrencyEntity = class CurrencyEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], CurrencyEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], CurrencyEntity.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], CurrencyEntity.prototype, "description", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], CurrencyEntity.prototype, "currency", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], CurrencyEntity.prototype, "image_url", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    __metadata("design:type", String)
], CurrencyEntity.prototype, "owner_created", void 0);
__decorate([
    typeorm_1.Column({ type: 'boolean', default: true }),
    __metadata("design:type", Boolean)
], CurrencyEntity.prototype, "status", void 0);
__decorate([
    typeorm_1.Column({ type: 'int4', nullable: true }),
    __metadata("design:type", Number)
], CurrencyEntity.prototype, "order", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], CurrencyEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], CurrencyEntity.prototype, "updated_at", void 0);
CurrencyEntity = __decorate([
    typeorm_1.Entity(exports.CURRENCIES_TABLE)
], CurrencyEntity);
exports.CurrencyEntity = CurrencyEntity;
let CurrencyEntitySubscriber = class CurrencyEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
CurrencyEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], CurrencyEntitySubscriber);
exports.CurrencyEntitySubscriber = CurrencyEntitySubscriber;
//# sourceMappingURL=currency.entity.js.map