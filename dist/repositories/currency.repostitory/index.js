"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrencyRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const currency_types_1 = require("../../domains/setting/currency.types");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const currency_entity_1 = require("./currency.entity");
class CurrencyRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(currency_entity_1.CurrencyEntity, app_setting_1.ConnectionName.user_role);
    }
    async find(params) {
        const { id, name, currency, status, ids } = params;
        const whereArray = [];
        if (status) {
            whereArray.push(`status = ${status}`);
        }
        if (name) {
            whereArray.push(`name = '${name}'`);
        }
        if (id) {
            whereArray.push(`id = '${id}'`);
        }
        if (currency) {
            whereArray.push(`currency = '${currency}'`);
        }
        if (ids && ids.length > 0) {
            whereArray.push(`id in (${ids.map((e) => `'${e}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const querySetting = `
    SELECT  *
    FROM ${currency_entity_1.CURRENCIES_TABLE}  
    ${where}  
    ORDER BY "order" ASC
    `;
        return this.repo().query(querySetting);
    }
}
exports.CurrencyRepository = CurrencyRepository;
//# sourceMappingURL=index.js.map