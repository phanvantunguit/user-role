import { QueryCurrency, RawCurrency } from 'src/domains/setting/currency.types';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { CurrencyEntity } from './currency.entity';
export declare class CurrencyRepository extends BaseRepository<QueryCurrency, RawCurrency> {
    repo(): Repository<CurrencyEntity>;
    find(params: QueryCurrency & {
        ids: string[];
    }): Promise<CurrencyEntity[]>;
}
