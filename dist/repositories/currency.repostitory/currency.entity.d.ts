import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const CURRENCIES_TABLE = "currencies";
export declare class CurrencyEntity {
    id: string;
    name: string;
    description: string;
    currency: string;
    image_url: string;
    owner_created: string;
    status: boolean;
    order: number;
    created_at: number;
    updated_at: number;
}
export declare class CurrencyEntitySubscriber implements EntitySubscriberInterface<CurrencyEntity> {
    beforeInsert(event: InsertEvent<CurrencyEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<CurrencyEntity>): Promise<void>;
}
