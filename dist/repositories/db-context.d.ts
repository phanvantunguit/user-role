import { Connection, QueryRunner } from 'typeorm';
export declare class DBContext {
    private connection;
    constructor(connection: Connection);
    runInTransaction(runInTransaction: (queryRunner: QueryRunner) => Promise<any>): Promise<any>;
}
