import { EntitySubscriberInterface, InsertEvent } from 'typeorm';
export declare const TRANSACTION_DETAILS_TABLE = "transaction_details";
export declare class TransactionDetailEntity {
    id: string;
    user_id: string;
    transaction_id: string;
    role_id: string;
    price: number;
    currency: string;
    package_id: string;
    package_type: string;
    package_name: string;
    discount_rate: number;
    discount_amount: number;
    quantity: number;
    expires_at: number;
    created_at: number;
}
export declare class TransactionDetailEntitySubscriber implements EntitySubscriberInterface<TransactionDetailEntity> {
    beforeInsert(event: InsertEvent<TransactionDetailEntity>): Promise<void>;
}
