import { QueryTransactionDetail, RawTransactionDetail } from 'src/domains/transaction';
import { Repository, QueryRunner } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { TransactionDetailViewEntity } from './transaction-detail-view.entity';
import { TransactionDetailEntity } from './transaction-detail.entity';
export declare class TransactionDetailRepository extends BaseRepository<QueryTransactionDetail, RawTransactionDetail> {
    repo(): Repository<TransactionDetailEntity>;
    transactionDetailViewRepo(): Repository<TransactionDetailViewEntity>;
    save(params: RawTransactionDetail, queryRunner?: QueryRunner): Promise<TransactionDetailEntity>;
    findOne(params: QueryTransactionDetail): Promise<TransactionDetailViewEntity>;
    find(params: QueryTransactionDetail): Promise<TransactionDetailViewEntity[]>;
}
