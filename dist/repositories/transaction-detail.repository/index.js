"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionDetailRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const transaction_1 = require("../../domains/transaction");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const transaction_detail_view_entity_1 = require("./transaction-detail-view.entity");
const transaction_detail_entity_1 = require("./transaction-detail.entity");
class TransactionDetailRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(transaction_detail_entity_1.TransactionDetailEntity, app_setting_1.ConnectionName.user_role);
    }
    transactionDetailViewRepo() {
        return typeorm_1.getRepository(transaction_detail_view_entity_1.TransactionDetailViewEntity, app_setting_1.ConnectionName.user_role);
    }
    async save(params, queryRunner) {
        if (queryRunner) {
            const transactionDetail = await this.repo().create(params);
            return queryRunner.manager.save(transactionDetail);
        }
        else {
            return this.repo().save(params);
        }
    }
    async findOne(params) {
        return this.transactionDetailViewRepo().findOne(params);
    }
    async find(params) {
        return this.transactionDetailViewRepo().find({
            where: params,
            order: { created_at: 'DESC' },
        });
    }
}
exports.TransactionDetailRepository = TransactionDetailRepository;
//# sourceMappingURL=index.js.map