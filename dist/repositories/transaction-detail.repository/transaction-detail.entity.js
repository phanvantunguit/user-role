"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionDetailEntitySubscriber = exports.TransactionDetailEntity = exports.TRANSACTION_DETAILS_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.TRANSACTION_DETAILS_TABLE = 'transaction_details';
let TransactionDetailEntity = class TransactionDetailEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], TransactionDetailEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    typeorm_1.Index(),
    __metadata("design:type", String)
], TransactionDetailEntity.prototype, "user_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    typeorm_1.Index(),
    __metadata("design:type", String)
], TransactionDetailEntity.prototype, "transaction_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    __metadata("design:type", String)
], TransactionDetailEntity.prototype, "role_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'float4' }),
    __metadata("design:type", Number)
], TransactionDetailEntity.prototype, "price", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionDetailEntity.prototype, "currency", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    __metadata("design:type", String)
], TransactionDetailEntity.prototype, "package_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionDetailEntity.prototype, "package_type", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionDetailEntity.prototype, "package_name", void 0);
__decorate([
    typeorm_1.Column({ type: 'float4' }),
    __metadata("design:type", Number)
], TransactionDetailEntity.prototype, "discount_rate", void 0);
__decorate([
    typeorm_1.Column({ type: 'float4' }),
    __metadata("design:type", Number)
], TransactionDetailEntity.prototype, "discount_amount", void 0);
__decorate([
    typeorm_1.Column({ type: 'float4' }),
    __metadata("design:type", Number)
], TransactionDetailEntity.prototype, "quantity", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", Number)
], TransactionDetailEntity.prototype, "expires_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], TransactionDetailEntity.prototype, "created_at", void 0);
TransactionDetailEntity = __decorate([
    typeorm_1.Entity(exports.TRANSACTION_DETAILS_TABLE)
], TransactionDetailEntity);
exports.TransactionDetailEntity = TransactionDetailEntity;
let TransactionDetailEntitySubscriber = class TransactionDetailEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
    }
};
TransactionDetailEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], TransactionDetailEntitySubscriber);
exports.TransactionDetailEntitySubscriber = TransactionDetailEntitySubscriber;
//# sourceMappingURL=transaction-detail.entity.js.map