"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionDetailViewEntity = exports.TRANSACTION_DETAILS_VIEW = void 0;
const typeorm_1 = require("typeorm");
exports.TRANSACTION_DETAILS_VIEW = 'transaction_details_view';
let TransactionDetailViewEntity = class TransactionDetailViewEntity {
};
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionDetailViewEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionDetailViewEntity.prototype, "user_id", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionDetailViewEntity.prototype, "transaction_id", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionDetailViewEntity.prototype, "role_id", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionDetailViewEntity.prototype, "role_name", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], TransactionDetailViewEntity.prototype, "price", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionDetailViewEntity.prototype, "currency", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionDetailViewEntity.prototype, "package_id", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionDetailViewEntity.prototype, "package_type", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], TransactionDetailViewEntity.prototype, "package_name", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], TransactionDetailViewEntity.prototype, "discount_rate", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], TransactionDetailViewEntity.prototype, "discount_amount", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], TransactionDetailViewEntity.prototype, "quantity", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], TransactionDetailViewEntity.prototype, "expires_at", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], TransactionDetailViewEntity.prototype, "created_at", void 0);
TransactionDetailViewEntity = __decorate([
    typeorm_1.ViewEntity(exports.TRANSACTION_DETAILS_VIEW, { expression: '', synchronize: false })
], TransactionDetailViewEntity);
exports.TransactionDetailViewEntity = TransactionDetailViewEntity;
//# sourceMappingURL=transaction-detail-view.entity.js.map