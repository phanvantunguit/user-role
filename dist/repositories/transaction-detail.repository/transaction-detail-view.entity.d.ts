export declare const TRANSACTION_DETAILS_VIEW = "transaction_details_view";
export declare class TransactionDetailViewEntity {
    id: string;
    user_id: string;
    transaction_id: string;
    role_id: string;
    role_name: string;
    price: number;
    currency: string;
    package_id: string;
    package_type: string;
    package_name: string;
    discount_rate: number;
    discount_amount: number;
    quantity: number;
    expires_at: number;
    created_at: number;
}
