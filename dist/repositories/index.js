"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const config_1 = require("../config");
const auth_role_entity_1 = require("./permission.repository/auth-role.entity");
const feature_role_entity_1 = require("./permission.repository/feature-role.entity");
const general_setting_role_entity_1 = require("./permission.repository/general-setting-role.entity");
const role_entity_1 = require("./permission.repository/role.entity");
const symbol_setting_role_entity_1 = require("./permission.repository/symbol-setting-role.entity");
const exchange_entity_1 = require("./setting.repository/exchange.entity");
const feature_entity_1 = require("./setting.repository/feature.entity");
const general_setting_entity_1 = require("./setting.repository/general-setting.entity");
const resolution_entity_1 = require("./setting.repository/resolution.entity");
const symbol_entity_1 = require("./setting.repository/symbol.entity");
const user_view_entity_1 = require("./user.repository/user-view.entity");
const auth_user_role_entity_1 = require("./user.repository/auth_user_role.entity");
const session_entity_1 = require("./user.repository/session.entity");
const user_log_entity_1 = require("./user.repository/user-log.entity");
const user_role_entity_1 = require("./user.repository/user-role.entity");
const user_entity_1 = require("./user.repository/user.entity");
const verify_token_entity_1 = require("./user.repository/verify-token.entity");
const role_view_entity_1 = require("./permission.repository/role-view.entity");
const app_setting_entity_1 = require("./setting.repository/app-setting.entity");
const transaction_entity_1 = require("./transaction.repository/transaction.entity");
const transaction_log_entity_1 = require("./transaction-log.repository/transaction-log.entity");
const transaction_detail_entity_1 = require("./transaction-detail.repository/transaction-detail.entity");
const transaction_view_entity_1 = require("./transaction.repository/transaction-view.entity");
const transaction_detail_view_entity_1 = require("./transaction-detail.repository/transaction-detail-view.entity");
const bot_setting_entity_1 = require("./bot-setting.repository/bot-setting.entity");
const package_entity_1 = require("./package.ropository/package.entity");
const currency_entity_1 = require("./currency.repostitory/currency.entity");
const redisStore = require("cache-manager-redis-store");
const cache_repository_1 = require("./cache.repository");
const currency_repostitory_1 = require("./currency.repostitory");
const bot_setting_repository_1 = require("./bot-setting.repository");
const package_ropository_1 = require("./package.ropository");
const permission_repository_1 = require("./permission.repository");
const setting_repository_1 = require("./setting.repository");
const transaction_detail_repository_1 = require("./transaction-detail.repository");
const transaction_log_repository_1 = require("./transaction-log.repository");
const user_repository_1 = require("./user.repository");
const transaction_repository_1 = require("./transaction.repository");
const base_repository_1 = require("./base.repository/base.repository");
const db_context_1 = require("./db-context");
const bot_repository_1 = require("./bot.repository");
const bot_entity_1 = require("./bot.repository/bot.entity");
const user_bot_entity_1 = require("./user.repository/user-bot.entity");
const user_asset_log_entity_1 = require("./user.repository/user-asset-log.entity");
const transaction_ps_log_entity_1 = require("./transaction-ps.repository/transaction-ps-log.entity");
const transaction_ps_metadata_entity_1 = require("./transaction-ps.repository/transaction-ps-metadata.entity");
const transaction_ps_entity_1 = require("./transaction-ps.repository/transaction-ps.entity");
const transaction_ps_repository_1 = require("./transaction-ps.repository");
const app_setting_1 = require("../const/app-setting");
const bot_signal_repository_1 = require("./bot-signal.repository");
const bot_signal_entity_1 = require("./bot-signal.repository/bot-signal.entity");
const favorite_signal_entity_1 = require("./bot-signal.repository/favorite-signal.entity");
const merchant_repository_1 = require("./merchant.repository");
const merchant_entity_1 = require("./merchant.repository/merchant.entity");
const bot_trading_repository_1 = require("./bot-trading.repository");
const bot_trading_entity_1 = require("./bot-trading.repository/bot-trading.entity");
const user_bot_trading_entity_1 = require("./user.repository/user-bot-trading.entity");
const bot_trading_history_entity_1 = require("./bot-trading-history.repository/bot-trading-history.entity");
const bot_trading_history_repository_1 = require("./bot-trading-history.repository");
const account_balance_repository_1 = require("./account-balance.repository");
const account_balance_entity_1 = require("./account-balance.repository/account-balance.entity");
const merchant_commission_repository_1 = require("./merchant-commission.repository");
const merchant_commission_entity_1 = require("./merchant-commission.repository/merchant-commission.entity");
const merchant_invoice_repository_1 = require("./merchant-invoice.repository");
const merchant_invoice_entity_1 = require("./merchant-invoice.repository/merchant-invoice.entity");
const event_store_repository_1 = require("./event-store.repository");
const event_store_entity_1 = require("./event-store.repository/event-store.entity");
const additional_data_repository_1 = require("./additional-data.repository");
const merchant_additional_data_repository_1 = require("./merchant-additional-data.repository");
const additional_data_entity_1 = require("./additional-data.repository/additional-data.entity");
const merchant_additional_data_entity_1 = require("./merchant-additional-data.repository/merchant-additional-data.entity");
const repo = [
    base_repository_1.BaseRepository,
    user_repository_1.UserRepository,
    permission_repository_1.PermissionRepository,
    setting_repository_1.SettingRepository,
    transaction_repository_1.TransactionRepository,
    transaction_detail_repository_1.TransactionDetailRepository,
    transaction_log_repository_1.TransactionLogRepository,
    bot_setting_repository_1.BotSettingRepository,
    package_ropository_1.PackageRepository,
    currency_repostitory_1.CurrencyRepository,
    cache_repository_1.CacheRepository,
    db_context_1.DBContext,
    bot_repository_1.BotRepository,
    transaction_ps_repository_1.TransactionPSRepository,
    bot_signal_repository_1.BotSignalRepository,
    merchant_repository_1.MerchantRepository,
    bot_trading_repository_1.BotTradingRepository,
    bot_trading_history_repository_1.BotTradingHistoryRepository,
    account_balance_repository_1.AccountBalanceRepository,
    merchant_commission_repository_1.MerchantCommissionRepository,
    merchant_invoice_repository_1.MerchantInvoiceRepository,
    event_store_repository_1.EventStoreRepository,
    additional_data_repository_1.AdditionalDataRepository,
    merchant_additional_data_repository_1.MerchantAdditionalDataRepository,
];
let DatabaseModule = class DatabaseModule {
    constructor(cacheManager) {
        const client = cacheManager.store.getClient();
        client.on('error', (error) => {
            console.error(error);
        });
    }
};
DatabaseModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forRoot({
                type: 'postgres',
                name: app_setting_1.ConnectionName.user_role,
                host: config_1.APP_CONFIG.POSTGRES_HOST,
                port: config_1.APP_CONFIG.POSTGRES_PORT,
                username: config_1.APP_CONFIG.POSTGRES_USER,
                password: config_1.APP_CONFIG.POSTGRES_PASS,
                database: config_1.APP_CONFIG.POSTGRES_DB,
                entities: [
                    auth_role_entity_1.AuthRoleEntity,
                    feature_role_entity_1.FeatureRoleEntity,
                    general_setting_entity_1.GeneralSettingEntity,
                    role_entity_1.RoleEntity,
                    symbol_setting_role_entity_1.SymbolSettingRoleEntity,
                    exchange_entity_1.ExchangeEntity,
                    feature_entity_1.FeatureEntity,
                    resolution_entity_1.ResolutionEntity,
                    symbol_entity_1.SymbolEntity,
                    auth_user_role_entity_1.AuthUserRoleEntity,
                    user_entity_1.UserEntity,
                    verify_token_entity_1.VerifyTokenEntity,
                    session_entity_1.SessionEntity,
                    user_log_entity_1.UserLogEntity,
                    user_role_entity_1.UserRoleEntity,
                    user_view_entity_1.UserViewEntity,
                    general_setting_role_entity_1.GeneralSettingRoleEntity,
                    role_view_entity_1.RoleViewEntity,
                    app_setting_entity_1.AppSettingEntity,
                    transaction_entity_1.TransactionEntity,
                    transaction_detail_entity_1.TransactionDetailEntity,
                    transaction_log_entity_1.TransactionLogEntity,
                    transaction_view_entity_1.TransactionViewEntity,
                    transaction_detail_view_entity_1.TransactionDetailViewEntity,
                    bot_setting_entity_1.BotSettingEntity,
                    package_entity_1.PackageEntity,
                    currency_entity_1.CurrencyEntity,
                    bot_entity_1.BotEntity,
                    user_bot_entity_1.UserBotEntity,
                    user_asset_log_entity_1.UserAssetLogEntity,
                    bot_signal_entity_1.BotSignalEntity,
                    favorite_signal_entity_1.FavoriteSignalEntity,
                    merchant_entity_1.MerchantEntity,
                    bot_trading_entity_1.BotTradingEntity,
                    user_bot_trading_entity_1.UserBotTradingEntity,
                    bot_trading_history_entity_1.BotTradingHistoryEntity,
                    account_balance_entity_1.AccountBalanceEntity,
                    merchant_commission_entity_1.MerchantCommissionEntity,
                    merchant_invoice_entity_1.MerchantInvoiceEntity,
                    event_store_entity_1.EventStoreEntity,
                    additional_data_entity_1.AdditionalDataEntity,
                    merchant_additional_data_entity_1.MerchantAdditionalDataEntity,
                ],
                subscribers: [
                    user_entity_1.UserEntitySubscriber,
                    auth_user_role_entity_1.AuthUserRoleEntitySubscriber,
                    auth_role_entity_1.AuthRoleEntitySubscriber,
                    feature_role_entity_1.FeatureRoleEntitySubscriber,
                    general_setting_entity_1.GeneralSettingEntitySubscriber,
                    role_entity_1.RoleEntitySubscriber,
                    symbol_setting_role_entity_1.SymbolSettingRoleEntitySubscriber,
                    exchange_entity_1.ExchangeEntitySubscriber,
                    feature_entity_1.FeatureEntitySubscriber,
                    resolution_entity_1.ResolutionEntitySubscriber,
                    symbol_entity_1.SymbolEntitySubscriber,
                    session_entity_1.SessionEntitySubscriber,
                    user_log_entity_1.UserLogEntitySubscriber,
                    user_role_entity_1.UserRoleEntitySubscriber,
                    general_setting_role_entity_1.GeneralSettingRoleEntitySubscriber,
                    app_setting_entity_1.AppSettingEntitySubscriber,
                    transaction_entity_1.TransactionEntitySubscriber,
                    transaction_detail_entity_1.TransactionDetailEntitySubscriber,
                    transaction_log_entity_1.TransactionLogEntitySubscriber,
                    bot_setting_entity_1.BotSettingEntitySubscriber,
                    package_entity_1.PackageEntitySubscriber,
                    currency_entity_1.CurrencyEntitySubscriber,
                    bot_entity_1.BotEntitySubscriber,
                    user_bot_entity_1.UserBotEntitySubscriber,
                    user_asset_log_entity_1.UserAssetLogEntitySubscriber,
                    bot_signal_entity_1.BotSignalEntitySubscriber,
                    favorite_signal_entity_1.FavoriteSignalEntitySubscriber,
                    merchant_entity_1.MerchantEntitySubscriber,
                    bot_trading_entity_1.BotTradingEntitySubscriber,
                    user_bot_trading_entity_1.UserBotTradingEntitySubscriber,
                    bot_trading_history_entity_1.BotTradingHistoryEntitySubscriber,
                    account_balance_entity_1.AccountBalanceEntitySubscriber,
                    merchant_commission_entity_1.MerchantCommissionEntitySubscriber,
                    merchant_entity_1.MerchantEntitySubscriber,
                    event_store_entity_1.EventStoreEntitySubscriber,
                    additional_data_entity_1.AdditionalDataEntitySubscriber,
                    merchant_additional_data_entity_1.MerchantAdditionalDataEntitySubscriber,
                ],
                synchronize: true,
                migrations: ['dist/migrations/*{.ts,.js}'],
                migrationsTableName: 'migrations',
                migrationsRun: true,
                keepConnectionAlive: true,
            }),
            common_1.CacheModule.register({
                store: redisStore,
                host: config_1.APP_CONFIG.REDIS_HOST,
                port: config_1.APP_CONFIG.REDIS_PORT,
                password: config_1.APP_CONFIG.REDIS_PASS,
                ttl: 3600,
            }),
            typeorm_1.TypeOrmModule.forRoot({
                type: 'postgres',
                name: app_setting_1.ConnectionName.payment_service,
                host: config_1.APP_CONFIG.PAYMENT_POSTGRES_HOST,
                port: config_1.APP_CONFIG.PAYMENT_POSTGRES_PORT,
                username: config_1.APP_CONFIG.PAYMENT_POSTGRES_USER,
                password: config_1.APP_CONFIG.PAYMENT_POSTGRES_PASS,
                database: config_1.APP_CONFIG.PAYMENT_POSTGRES_DB,
                entities: [
                    transaction_ps_log_entity_1.TransactionPSLogEntity,
                    transaction_ps_metadata_entity_1.TransactionPSMetadataEntity,
                    transaction_ps_entity_1.TransactionPSEntity,
                ],
                subscribers: [
                    transaction_ps_log_entity_1.TransactionPSLogEntitySubscriber,
                    transaction_ps_entity_1.TransactionPSEntitySubscriber,
                    transaction_ps_metadata_entity_1.TransactionPSMetadataEntitySubscriber,
                ],
                synchronize: false,
                migrationsRun: false,
                keepConnectionAlive: true,
            }),
        ],
        providers: repo,
        exports: repo,
    }),
    __param(0, common_1.Inject(common_1.CACHE_MANAGER)),
    __metadata("design:paramtypes", [Object])
], DatabaseModule);
exports.DatabaseModule = DatabaseModule;
//# sourceMappingURL=index.js.map