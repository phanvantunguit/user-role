import { EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
export declare const MERCHANT_INVOICES_TABLE = "merchant_invoices";
export declare class MerchantInvoiceEntity {
    id?: string;
    merchant_id?: string;
    amount_commission_complete?: number;
    status?: string;
    transaction_id?: string;
    wallet_address?: string;
    wallet_network?: string;
    description?: string;
    created_at?: number;
    updated_at?: number;
    updated_by?: string;
    metadata?: any;
}
export declare class MerchantInvoiceEntitySubscriber implements EntitySubscriberInterface<MerchantInvoiceEntity> {
    beforeInsert(event: InsertEvent<MerchantInvoiceEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<MerchantInvoiceEntity>): Promise<void>;
}
