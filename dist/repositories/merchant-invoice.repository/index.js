"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantInvoiceRepository = void 0;
const app_setting_1 = require("../../const/app-setting");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository/base.repository");
const merchant_invoice_entity_1 = require("./merchant-invoice.entity");
class MerchantInvoiceRepository extends base_repository_1.BaseRepository {
    repo() {
        return typeorm_1.getRepository(merchant_invoice_entity_1.MerchantInvoiceEntity, app_setting_1.ConnectionName.user_role);
    }
}
exports.MerchantInvoiceRepository = MerchantInvoiceRepository;
//# sourceMappingURL=index.js.map