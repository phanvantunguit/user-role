import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { MerchantInvoiceEntity } from './merchant-invoice.entity';
export declare class MerchantInvoiceRepository extends BaseRepository<MerchantInvoiceEntity, MerchantInvoiceEntity> {
    repo(): Repository<MerchantInvoiceEntity>;
}
