import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const AUTH_ROLES_TABLE = "auth_roles";
export declare class AuthRoleEntity {
    id: string;
    role_name: string;
    root: any;
    description: string;
    owner_created: string;
    created_at: number;
    updated_at: number;
}
export declare class AuthRoleEntitySubscriber implements EntitySubscriberInterface<AuthRoleEntity> {
    beforeInsert(event: InsertEvent<AuthRoleEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<AuthRoleEntity>): Promise<void>;
}
