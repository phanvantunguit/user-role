import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const SYMBOL_SETTINGS_ROLES_TABLE = "symbol_settings_roles";
export declare class SymbolSettingRoleEntity {
    id: string;
    role_id: string;
    description: string;
    list_exchanged: any;
    list_symbol: any;
    supported_resolutions: any;
    owner_created: string;
    created_at: number;
    updated_at: number;
}
export declare class SymbolSettingRoleEntitySubscriber implements EntitySubscriberInterface<SymbolSettingRoleEntity> {
    beforeInsert(event: InsertEvent<SymbolSettingRoleEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<SymbolSettingRoleEntity>): Promise<void>;
}
