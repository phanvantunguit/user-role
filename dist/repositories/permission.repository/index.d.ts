import { RoleEntity } from './role.entity';
import { QueryAuthRole, QueryFeatureRole, QueryGeneralSettingRole, QueryRole, QuerySymbolSettingRole, RawAuthRole, RawFeatureRole, RawGeneralSettingRole, RawRole, RawSymbolSettingRole } from 'src/domains/permission';
import { GeneralSettingRoleEntity } from './general-setting-role.entity';
import { SymbolSettingRoleEntity } from './symbol-setting-role.entity';
import { RoleViewEntity } from './role-view.entity';
export declare class PermissionRepository {
    private authRoleRepo;
    saveAuthRoles(params: RawAuthRole[]): Promise<RawAuthRole[]>;
    findAuthRole(params: QueryAuthRole): Promise<RawAuthRole[]>;
    findAuthRoleByIds(auth_role_ids: string[]): Promise<RawAuthRole[]>;
    deleteAuthRoleByIds(auth_role_ids: string[]): Promise<any>;
    private featureRoleRepo;
    saveFeatureRoles(params: RawFeatureRole[]): Promise<RawFeatureRole[]>;
    findFeatureRoles(params: QueryFeatureRole): Promise<RawFeatureRole[]>;
    deleteFeatureRolesByIds(ids: string[]): Promise<any>;
    private generalSettingRoleRepo;
    saveGeneralSettingRoles(params: RawGeneralSettingRole[]): Promise<GeneralSettingRoleEntity[]>;
    findGeneralSettingRoles(params: QueryGeneralSettingRole): Promise<RawGeneralSettingRole[]>;
    findGeneralSettingMultipleRoles(roleIds: string[], generalSettingId: string): Promise<RawGeneralSettingRole[]>;
    deleteGeneralSettingRolesByIds(ids: string[]): Promise<any>;
    private roleRepo;
    saveRoles(params: RawRole[]): Promise<RoleEntity[]>;
    deleteRoleByIds(ids: string[]): Promise<any>;
    findOneRoleTable(params: QueryRole): Promise<any>;
    private roleViewRepo;
    findRoles(params: QueryRole): Promise<RoleViewEntity[]>;
    findRolesConditionMultiple(params: {
        status?: string[];
    }): Promise<RoleViewEntity[]>;
    findRoleByIds(ids: string[]): Promise<RawRole[]>;
    private symbolSettingRoleRepo;
    saveSymbolSettingRole(params: RawSymbolSettingRole[]): Promise<SymbolSettingRoleEntity[]>;
    findSymbolSettingRoleByIds(ids: string[]): Promise<SymbolSettingRoleEntity[]>;
    deleteSymbolSettingRoleByIds(ids: string[]): Promise<any>;
    findSymbolSettingRole(params: QuerySymbolSettingRole): Promise<SymbolSettingRoleEntity[]>;
}
