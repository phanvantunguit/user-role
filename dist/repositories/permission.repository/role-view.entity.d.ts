export declare const ROLES_VIEW = "roles_view";
export declare class RoleViewEntity {
    id: string;
    role_name: string;
    root: any;
    description: string;
    status: string;
    type: string;
    price: string;
    currency: string;
    owner_created: string;
    parent_id: string;
    is_best_choice: boolean;
    order: number;
    description_features: any;
    color: string;
    created_at: number;
    updated_at: number;
}
