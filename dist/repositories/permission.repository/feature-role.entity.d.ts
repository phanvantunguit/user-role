import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const FEATURE_ROLES_TABLE = "feature_roles";
export declare class FeatureRoleEntity {
    id: string;
    role_id: string;
    feature_id: string;
    description: string;
    owner_created: string;
    created_at: number;
    updated_at: number;
}
export declare class FeatureRoleEntitySubscriber implements EntitySubscriberInterface<FeatureRoleEntity> {
    beforeInsert(event: InsertEvent<FeatureRoleEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<FeatureRoleEntity>): Promise<void>;
}
