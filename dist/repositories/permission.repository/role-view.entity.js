"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleViewEntity = exports.ROLES_VIEW = void 0;
const typeorm_1 = require("typeorm");
exports.ROLES_VIEW = 'roles_view';
let RoleViewEntity = class RoleViewEntity {
};
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], RoleViewEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], RoleViewEntity.prototype, "role_name", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Object)
], RoleViewEntity.prototype, "root", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], RoleViewEntity.prototype, "description", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], RoleViewEntity.prototype, "status", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], RoleViewEntity.prototype, "type", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], RoleViewEntity.prototype, "price", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], RoleViewEntity.prototype, "currency", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], RoleViewEntity.prototype, "owner_created", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], RoleViewEntity.prototype, "parent_id", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Boolean)
], RoleViewEntity.prototype, "is_best_choice", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], RoleViewEntity.prototype, "order", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Object)
], RoleViewEntity.prototype, "description_features", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", String)
], RoleViewEntity.prototype, "color", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], RoleViewEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.ViewColumn(),
    __metadata("design:type", Number)
], RoleViewEntity.prototype, "updated_at", void 0);
RoleViewEntity = __decorate([
    typeorm_1.ViewEntity(exports.ROLES_VIEW, { expression: '', synchronize: false })
], RoleViewEntity);
exports.RoleViewEntity = RoleViewEntity;
//# sourceMappingURL=role-view.entity.js.map