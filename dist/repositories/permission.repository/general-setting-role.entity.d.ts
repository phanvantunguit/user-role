import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const GENERAL_SETTINGS_ROLES_TABLE = "general_settings_roles";
export declare class GeneralSettingRoleEntity {
    id: string;
    role_id: string;
    general_setting_id: string;
    description: string;
    val_limit: number;
    owner_created: string;
    created_at: number;
    updated_at: number;
}
export declare class GeneralSettingRoleEntitySubscriber implements EntitySubscriberInterface<GeneralSettingRoleEntity> {
    beforeInsert(event: InsertEvent<GeneralSettingRoleEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<GeneralSettingRoleEntity>): Promise<void>;
}
