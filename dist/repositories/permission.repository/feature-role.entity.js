"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FeatureRoleEntitySubscriber = exports.FeatureRoleEntity = exports.FEATURE_ROLES_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.FEATURE_ROLES_TABLE = 'feature_roles';
let FeatureRoleEntity = class FeatureRoleEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], FeatureRoleEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    __metadata("design:type", String)
], FeatureRoleEntity.prototype, "role_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], FeatureRoleEntity.prototype, "feature_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], FeatureRoleEntity.prototype, "description", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], FeatureRoleEntity.prototype, "owner_created", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], FeatureRoleEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], FeatureRoleEntity.prototype, "updated_at", void 0);
FeatureRoleEntity = __decorate([
    typeorm_1.Entity(exports.FEATURE_ROLES_TABLE)
], FeatureRoleEntity);
exports.FeatureRoleEntity = FeatureRoleEntity;
let FeatureRoleEntitySubscriber = class FeatureRoleEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
FeatureRoleEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], FeatureRoleEntitySubscriber);
exports.FeatureRoleEntitySubscriber = FeatureRoleEntitySubscriber;
//# sourceMappingURL=feature-role.entity.js.map