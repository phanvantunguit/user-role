"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SymbolSettingRoleEntitySubscriber = exports.SymbolSettingRoleEntity = exports.SYMBOL_SETTINGS_ROLES_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.SYMBOL_SETTINGS_ROLES_TABLE = 'symbol_settings_roles';
let SymbolSettingRoleEntity = class SymbolSettingRoleEntity {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    typeorm_1.Generated('uuid'),
    __metadata("design:type", String)
], SymbolSettingRoleEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid' }),
    __metadata("design:type", String)
], SymbolSettingRoleEntity.prototype, "role_id", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], SymbolSettingRoleEntity.prototype, "description", void 0);
__decorate([
    typeorm_1.Column({ type: 'jsonb', nullable: true }),
    __metadata("design:type", Object)
], SymbolSettingRoleEntity.prototype, "list_exchanged", void 0);
__decorate([
    typeorm_1.Column({ type: 'jsonb', nullable: true }),
    __metadata("design:type", Object)
], SymbolSettingRoleEntity.prototype, "list_symbol", void 0);
__decorate([
    typeorm_1.Column({ type: 'jsonb', nullable: true }),
    __metadata("design:type", Object)
], SymbolSettingRoleEntity.prototype, "supported_resolutions", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], SymbolSettingRoleEntity.prototype, "owner_created", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], SymbolSettingRoleEntity.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], SymbolSettingRoleEntity.prototype, "updated_at", void 0);
SymbolSettingRoleEntity = __decorate([
    typeorm_1.Entity(exports.SYMBOL_SETTINGS_ROLES_TABLE)
], SymbolSettingRoleEntity);
exports.SymbolSettingRoleEntity = SymbolSettingRoleEntity;
let SymbolSettingRoleEntitySubscriber = class SymbolSettingRoleEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
SymbolSettingRoleEntitySubscriber = __decorate([
    typeorm_1.EventSubscriber()
], SymbolSettingRoleEntitySubscriber);
exports.SymbolSettingRoleEntitySubscriber = SymbolSettingRoleEntitySubscriber;
//# sourceMappingURL=symbol-setting-role.entity.js.map