"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionRepository = void 0;
const typeorm_1 = require("typeorm");
const auth_role_entity_1 = require("./auth-role.entity");
const role_entity_1 = require("./role.entity");
const permission_1 = require("../../domains/permission");
const general_setting_role_entity_1 = require("./general-setting-role.entity");
const format_data_util_1 = require("../../utils/format-data.util");
const symbol_setting_role_entity_1 = require("./symbol-setting-role.entity");
const feature_role_entity_1 = require("./feature-role.entity");
const role_view_entity_1 = require("./role-view.entity");
const app_setting_1 = require("../../const/app-setting");
class PermissionRepository {
    authRoleRepo() {
        return typeorm_1.getRepository(auth_role_entity_1.AuthRoleEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveAuthRoles(params) {
        return this.authRoleRepo().save(params);
    }
    async findAuthRole(params) {
        const query = format_data_util_1.cleanObject(params);
        return this.authRoleRepo().find({
            where: query,
            order: { created_at: 'DESC' },
        });
    }
    async findAuthRoleByIds(auth_role_ids) {
        return this.authRoleRepo().findByIds(auth_role_ids);
    }
    async deleteAuthRoleByIds(auth_role_ids) {
        return this.authRoleRepo().delete(auth_role_ids);
    }
    featureRoleRepo() {
        return typeorm_1.getRepository(feature_role_entity_1.FeatureRoleEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveFeatureRoles(params) {
        return this.featureRoleRepo().save(params);
    }
    async findFeatureRoles(params) {
        return this.featureRoleRepo().find(params);
    }
    async deleteFeatureRolesByIds(ids) {
        return this.featureRoleRepo().delete(ids);
    }
    generalSettingRoleRepo() {
        return typeorm_1.getRepository(general_setting_role_entity_1.GeneralSettingRoleEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveGeneralSettingRoles(params) {
        return this.generalSettingRoleRepo().save(params);
    }
    async findGeneralSettingRoles(params) {
        return this.generalSettingRoleRepo().find(params);
    }
    async findGeneralSettingMultipleRoles(roleIds, generalSettingId) {
        const query = `
      SELECT *
      FROM ${general_setting_role_entity_1.GENERAL_SETTINGS_ROLES_TABLE}  
      WHERE 
      ${roleIds.length > 0
            ? `role_id in (${roleIds.map((id) => `'${id}'`)}) and`
            : ''} 
	    general_setting_id = '${generalSettingId}'  
    `;
        return this.generalSettingRoleRepo().query(query);
    }
    async deleteGeneralSettingRolesByIds(ids) {
        return this.generalSettingRoleRepo().delete(ids);
    }
    roleRepo() {
        return typeorm_1.getRepository(role_entity_1.RoleEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveRoles(params) {
        return this.roleRepo().save(params);
    }
    async deleteRoleByIds(ids) {
        return this.roleRepo().delete(ids);
    }
    async findOneRoleTable(params) {
        return this.roleRepo().findOne(params);
    }
    roleViewRepo() {
        return typeorm_1.getRepository(role_view_entity_1.RoleViewEntity, app_setting_1.ConnectionName.user_role);
    }
    async findRoles(params) {
        const query = format_data_util_1.cleanObject(params);
        return this.roleViewRepo().find({
            where: query,
            order: { order: 'ASC' },
        });
    }
    async findRolesConditionMultiple(params) {
        const { status } = params;
        const whereArray = [];
        if (status && status.length > 0) {
            whereArray.push(`status in (${status.map((e) => `'${e}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
      SELECT *
      FROM ${role_entity_1.ROLES_TABLE}  
      ${where} 
      order by "order" ASC
    `;
        return this.roleRepo().query(query);
    }
    async findRoleByIds(ids) {
        const queryRoles = `
        SELECT *
        FROM ${role_view_entity_1.ROLES_VIEW} rv 
        WHERE id in (${ids.map((e) => `'${e}'`)})
        `;
        const data = await this.roleViewRepo().query(queryRoles);
        return data;
    }
    symbolSettingRoleRepo() {
        return typeorm_1.getRepository(symbol_setting_role_entity_1.SymbolSettingRoleEntity, app_setting_1.ConnectionName.user_role);
    }
    async saveSymbolSettingRole(params) {
        return this.symbolSettingRoleRepo().save(params);
    }
    async findSymbolSettingRoleByIds(ids) {
        return this.symbolSettingRoleRepo().findByIds(ids);
    }
    async deleteSymbolSettingRoleByIds(ids) {
        return this.symbolSettingRoleRepo().delete(ids);
    }
    async findSymbolSettingRole(params) {
        return this.symbolSettingRoleRepo().find(params);
    }
}
exports.PermissionRepository = PermissionRepository;
//# sourceMappingURL=index.js.map