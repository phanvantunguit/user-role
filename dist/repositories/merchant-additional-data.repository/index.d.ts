import { ADDITIONAL_DATA_TYPE, MERCHANT_ADDITIONAL_DATA_STATUS } from 'src/domains/merchant/types';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository/base.repository';
import { MerchantAdditionalDataEntity } from './merchant-additional-data.entity';
export declare class MerchantAdditionalDataRepository extends BaseRepository<MerchantAdditionalDataEntity, MerchantAdditionalDataEntity> {
    repo(): Repository<MerchantAdditionalDataEntity>;
    list(param: {
        merchant_id?: string;
        type?: ADDITIONAL_DATA_TYPE;
        status?: MERCHANT_ADDITIONAL_DATA_STATUS;
    }): Promise<any>;
}
