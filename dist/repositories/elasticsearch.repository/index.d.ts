import { ElasticsearchService } from '@nestjs/elasticsearch';
export declare class ElasticSearchRepository {
    private readonly elasticsearchService;
    constructor(elasticsearchService: ElasticsearchService);
    createDocument(index: string, id: string, data: any): Promise<string>;
    updateDocument(index: string, id: string, data: any): Promise<import("@elastic/elasticsearch/lib/api/types").UpdateResponse<unknown>>;
    deleteDocument(index: string, id: string): Promise<import("@elastic/elasticsearch/lib/api/types").WriteResponseBase>;
    deleteQueryDocument(index: string, query?: any): Promise<import("@elastic/elasticsearch/lib/api/types").DeleteByQueryResponse>;
    getDocument(index: string, id?: string, query?: any, limit?: number, offset?: number): Promise<import("@elastic/elasticsearch/lib/api/types").SearchHit<unknown>[]>;
    isExist(index: string, id: string): Promise<boolean>;
}
