"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ElasticSearchRepository = void 0;
const common_1 = require("@nestjs/common");
const elasticsearch_1 = require("@nestjs/elasticsearch");
const format_data_util_1 = require("../../utils/format-data.util");
let ElasticSearchRepository = class ElasticSearchRepository {
    constructor(elasticsearchService) {
        this.elasticsearchService = elasticsearchService;
    }
    async createDocument(index, id, data) {
        const result = await this.elasticsearchService.create({
            id,
            index,
            body: data,
        });
        return result._id;
    }
    async updateDocument(index, id, data) {
        const result = await this.elasticsearchService.update({
            id,
            index,
            body: {
                doc: data,
            },
        });
        return result;
    }
    async deleteDocument(index, id) {
        const result = await this.elasticsearchService.delete({
            id,
            index,
        });
        return result;
    }
    async deleteQueryDocument(index, query) {
        const clearQuery = format_data_util_1.cleanObject(query);
        const must = Object.keys(clearQuery).map((key) => {
            const match = {};
            match[key] = clearQuery[key];
            return { match };
        });
        const result = await this.elasticsearchService.deleteByQuery({
            index,
            query: {
                bool: {
                    must: must,
                },
            },
        });
        return result;
    }
    async getDocument(index, id, query, limit, offset) {
        if (!limit)
            limit = 50;
        if (!offset)
            offset = 0;
        if (id) {
            const result = await this.elasticsearchService.search({
                index,
                from: offset,
                size: limit,
                query: {
                    ids: {
                        values: id,
                    },
                },
            });
            return result.hits.hits;
        }
        else if (query) {
            const clearQuery = format_data_util_1.cleanObject(query);
            const must = Object.keys(clearQuery).map((key) => {
                const match = {};
                match[key] = clearQuery[key];
                return { match };
            });
            const result = await this.elasticsearchService.search({
                index,
                from: offset,
                size: limit,
                query: {
                    bool: {
                        must: must,
                    },
                },
            });
            return result.hits.hits;
        }
        const result = await this.elasticsearchService.search({
            index,
        });
        return result.hits.hits;
    }
    async isExist(index, id) {
        const result = await this.elasticsearchService.exists({
            id,
            index,
        });
        return result;
    }
};
ElasticSearchRepository = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [elasticsearch_1.ElasticsearchService])
], ElasticSearchRepository);
exports.ElasticSearchRepository = ElasticSearchRepository;
//# sourceMappingURL=index.js.map