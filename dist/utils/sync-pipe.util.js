"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SyncPipeRoleModify = exports.SyncPipe = void 0;
const events_1 = require("events");
const uuid_1 = require("uuid");
const eventEmitter = new events_1.EventEmitter();
class SyncPipe {
    constructor() {
        this.pipe = [];
    }
    async promise(callback, ...input) {
        const id = uuid_1.v4();
        return new Promise(async (resolve) => {
            if (this.pipe.length === 0) {
                this.pipe.push(id);
                const result = await callback(...input);
                this.pipe.splice(0, 1);
                eventEmitter.emit(id);
                return resolve(result);
            }
            else {
                const previousId = this.pipe[this.pipe.length - 1];
                this.pipe.push(id);
                eventEmitter.on(previousId, async () => {
                    const result = await callback(...input);
                    this.pipe.splice(0, 1);
                    eventEmitter.emit(id, result);
                    return resolve(result);
                });
            }
        });
    }
}
exports.SyncPipe = SyncPipe;
exports.SyncPipeRoleModify = new SyncPipe();
//# sourceMappingURL=sync-pipe.util.js.map