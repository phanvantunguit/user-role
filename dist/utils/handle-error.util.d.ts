import { IReason } from 'src/const/response';
export declare function throwError(reason: IReason): void;
