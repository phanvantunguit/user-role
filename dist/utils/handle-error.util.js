"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.throwError = void 0;
const response_1 = require("../const/response");
function throwError(reason) {
    throw reason;
}
exports.throwError = throwError;
//# sourceMappingURL=handle-error.util.js.map