import { ArgumentsHost, ExceptionFilter } from '@nestjs/common';
export declare class ExceptionsUtil implements ExceptionFilter {
    private logger;
    catch(exception: any, host: ArgumentsHost): void;
}
