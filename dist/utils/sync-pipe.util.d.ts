export declare class SyncPipe {
    private pipe;
    promise(callback: any, ...input: any[]): Promise<unknown>;
}
export declare const SyncPipeRoleModify: SyncPipe;
