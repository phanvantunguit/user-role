export declare const EMAIL_SUBJECT: {
    registration: string;
    reset_password: string;
    verify_email: string;
    transaction_information: string;
    payment_changes: string;
    payment_refund: string;
    upgrade_account_success: string;
    change_email: string;
};
