"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WS_EVENT = exports.ERROR_CODE = void 0;
exports.ERROR_CODE = {
    SUCCESS: {
        error_code: 'SUCCESS',
        message: 'Success',
    },
    UNKNOWN: {
        error_code: 'UNKNOWN',
        message: 'unknown',
    },
    BAD_REQUEST: {
        error_code: 'BAD_REQUEST',
        message: 'Bad request',
    },
    UNPROCESSABLE: {
        error_code: 'UNPROCESSABLE',
        message: 'Try it again',
    },
    NOT_FOUND: {
        error_code: 'NOT_FOUND',
        message: 'Not found',
    },
    RESOURCES_EXISTED: {
        error_code: 'RESOURCES_EXISTED',
        message: 'Resources was existed',
    },
    USER_NOT_FOUND: {
        error_code: 'USER_NOT_FOUND',
        message: 'User not found',
    },
    EMAIL_NOT_FOUND: {
        error_code: 'EMAIL_NOT_FOUND',
        message: 'Email not found',
    },
    EMAIL_EXISTED: {
        error_code: 'EMAIL_EXISTED',
        message: 'Email address was existed',
    },
    EMAIL_NOT_VERIFIED: {
        error_code: 'EMAIL_NOT_VERIFIED',
        message: 'Email address is not verified',
    },
    EMAIL_VERIFIED_NOT_ENOUGH: {
        error_code: 'EMAIL_VERIFIED_NOT_ENOUGH',
        message: 'You have confirmed 1 of 2 email addresses',
    },
    LOGIN_FAILED: {
        error_code: 'LOGIN_FAILED',
        message: 'Email address or password is incorrect',
    },
    SESSION_VERIFY_EMAIL_EXPIRED: {
        error_code: 'SESSION_VERIFY_EMAIL_EXPIRED',
        message: 'Session verify email address was expired',
    },
    SEND_EMAIL_FAILED: {
        error_code: 'SEND_EMAIL_FAILED',
        message: 'Send Email is failed',
    },
    SESSION_LOGION_EXPIRED: {
        error_code: 'SESSION_LOGION_EXPIRED',
        message: 'Session login was expired',
    },
    SESSION_INVALID: {
        error_code: 'SESSION_INVALID',
        message: 'Session is invalid',
    },
    AUTHORIZATION_REQUIRED: {
        error_code: 'AUTHORIZATION_REQUIRED',
        message: 'Authorization is required',
    },
    SIGNATURE_INVALID: {
        error_code: 'SIGNATURE_INVALID',
        message: 'Signature is invalid',
    },
    CHECKSUM_INVALID: {
        error_code: 'CHECKSUM_INVALID',
        message: 'Checksum is invalid',
    },
    TOKEN_EXPIRED: {
        error_code: 'TOKEN_EXPIRED',
        message: 'Token was expired',
    },
    TOKEN_INVALID: {
        error_code: 'TOKEN_INVALID',
        message: 'Token is invalid',
    },
    DEACTIVE_ACCOUNT: {
        error_code: 'DEACTIVE_ACCOUNT',
        message: 'Account was deactivated, contact administrator to support',
    },
    ROLE_EXISTED: {
        error_code: 'ROLE_EXISTED',
        message: 'Role was existed',
    },
    ROLE_NOT_FOUND: {
        error_code: 'ROLE_NOT_FOUND',
        message: 'Role is not found',
    },
    ROLE_INVALID: {
        error_code: 'ROLE_INVALID',
        message: 'Role is invalid',
    },
    NO_PERMISSION: {
        error_code: 'NO_PERMISSION',
        message: 'You do not have permission',
    },
    FEATURE_EXISTED: {
        error_code: 'FEATURE_EXISTED',
        message: 'Feature was existed',
    },
    FEATURE_NOT_FOUND: {
        error_code: 'FEATURE_NOT_FOUND',
        message: 'Feature not found',
    },
    EXCHANGE_NOT_FOUND: {
        error_code: 'EXCHANGE_NOT_FOUND',
        message: 'Exchange not found',
    },
    RESOLUTION_NOT_FOUND: {
        error_code: 'RESOLUTION_NOT_FOUND',
        message: 'Resolution not found',
    },
    RESOLUTION_EXISTED: {
        error_code: 'RESOLUTION_EXISTED',
        message: 'Resolution was existed',
    },
    GENERAL_SETTING_NOT_FOUND: {
        error_code: 'GENERAL_SETTING_NOT_FOUND',
        message: 'General setting not found',
    },
    GENERAL_SETTING_EXISTED: {
        error_code: 'GENERAL_SETTING_EXISTED',
        message: 'General setting was existed',
    },
    SYMBOL_NOT_FOUND: {
        error_code: 'SYMBOL_NOT_FOUND',
        message: 'Symbol not found',
    },
    SYMBOL_SETTING_ROLE_NOT_FOUND: {
        error_code: 'SYMBOL_SETTING_ROLE_NOT_FOUND',
        message: 'Symbol setting role not found',
    },
    APP_SETTING_NOT_FOUND: {
        error_code: 'APP_SETTING_NOT_FOUND',
        message: 'App setting not found',
    },
    APP_SETTING_EXISTED: {
        error_code: 'APP_SETTING_EXISTED',
        message: 'App setting was existed',
    },
    CREATE_TRANSACTION_FAILED: {
        error_code: 'CREATE_TRANSACTION_FAILED',
        message: 'Create transaction failed',
    },
    TRANSACTION_NOT_FOUND: {
        error_code: 'TRANSACTION_NOT_FOUND',
        message: 'Transaction not found',
    },
    TRANSACTION_PROCESSING: {
        error_code: 'TRANSACTION_PROCESSING',
        message: 'Transaction processing',
    },
    TRANSACTION_PARENT_NOT_FOUND: {
        error_code: 'TRANSACTION_PARENT_NOT_FOUND',
        message: 'Transaction parent not found',
    },
    PACKAGE_NOT_FOUND: {
        error_code: 'PACKAGE_NOT_FOUND',
        message: 'Package is not found',
    },
    PACKAGE_UPGRADED: {
        error_code: 'PACKAGE_UPGRADED',
        message: 'Package was upgraded',
    },
    PACKAGE_UPGRADE_PROCESSING: {
        error_code: 'PACKAGE_UPGRADE_PROCESSING',
        message: 'Upgrade package is processing',
    },
    USERNAME_EXISTED: {
        error_code: 'USERNAME_EXISTED',
        message: 'Username was existed',
    },
    VERIFY_CODE_INVALID: {
        error_code: 'VERIFY_CODE_INVALID',
        message: 'Verify code invalid',
    },
    PASSWORD_INCORRECT: {
        error_code: 'PASSWORD_INCORRECT',
        message: 'Password is incorrect',
    },
    PACKAGE_UPGRADE_SUCCESS: {
        error_code: 'PACKAGE_UPGRADE_SUCCESS',
        message: 'Upgrade package is success',
    },
    BOT_NOT_FOUND: {
        error_code: 'BOT_NOT_FOUND',
        message: 'Bot is not found',
    },
    BOT_INVALID: {
        error_code: 'BOT_INVALID',
        message: 'Bot is invalid',
    },
    AMOUNT_INVALID: {
        error_code: 'AMOUNT_INVALID',
        message: 'Amount is invalid',
    },
    SIGNAL_NOT_FOUND: {
        error_code: 'SIGNAL_NOT_FOUND',
        message: 'Signal is not found',
    },
    FAVORITE_SIGNAL_EXISTED: {
        error_code: 'FAVORITE_SIGNAL_EXISTED',
        message: 'Favorite signal existed',
    },
    FAVORITE_SIGNAL_NOT_FOUND: {
        error_code: 'FAVORITE_SIGNAL_NOT_FOUND',
        message: 'Favorite signal is not found',
    },
    ACCOUNT_INVALID: {
        error_code: 'ACCOUNT_INVALID',
        message: 'Account is invalid',
    },
    ACCOUNT_EXISTED: {
        error_code: 'ACCOUNT_EXISTED',
        message: 'Account was existed',
    },
    NUMBER_OF_ACCOUNT_LIMITED: {
        error_code: 'NUMBER_OF_ACCOUNT_LIMITED',
        message: 'Number of account was limited',
    },
    ACCOUNT_CONNECTED: {
        error_code: 'ACCOUNT_CONNECTED',
        message: 'Account was connected',
    },
    ACCOUNT_NOT_CONNECTED: {
        error_code: 'ACCOUNT_NOT_CONNECTED',
        message: 'Account not connected',
    },
    ACCOUNT_NOT_ACTIVE: {
        error_code: 'ACCOUNT_NOT_ACTIVE',
        message: 'Account not active',
    },
    ACCOUNT_INACTIVE_BY_SYSTEM: {
        error_code: 'ACCOUNT_INACTIVE_BY_SYSTEM',
        message: 'Please contact the admin for support.',
    },
    ACCOUNT_NOT_ENOUGH_BALANCE: {
        error_code: 'ACCOUNT_NOT_ENOUGH_BALANCE',
        message: 'Account not enough balance',
    },
    ACCOUNT_NOT_REFERRED: {
        error_code: 'ACCOUNT_NOT_REFERRED',
        message: 'Account must be referred by us',
    },
    POSITION_NOT_EMPTY: {
        error_code: 'POSITION_NOT_EMPTY',
        message: 'Position must be empty',
    },
    STATUS_INVALID: {
        error_code: 'STATUS_INVALID',
        message: 'Status is invalid',
    },
    CONNECT_ACCOUNT_FAILED: {
        error_code: 'CONNECT_ACCOUNT_FAILED',
        message: 'Connect account failed',
    },
    PROFILE_REQUIRED: {
        error_code: 'PROFILE_REQUIRED',
        message: 'Profile is required',
    },
    ACCOUNT_INFO_INCORRECT: {
        error_code: 'ACCOUNT_INFO_INCORRECT',
        message: 'Account info is not correct',
    },
    USERS_HAVE_BOTS: {
        error_code: 'DELETE_BOT_FAILED',
        message: 'Users have Bots',
    },
    USERS_HAVE_ROLES: {
        error_code: 'USERS_HAVE_ROLES',
        message: 'Users have Roles',
    },
    RESOURCES_NOT_EXISTED: {
        error_code: 'RESOURCES_NOT_EXISTED',
        message: 'Resources not existed',
    },
    TBOT_BLOCKED: {
        error_code: 'TBOT_BLOCKED',
        message: 'Trading Bot was blocked',
    },
    MAX_DRAWDOWN_INVALID: {
        error_code: 'MAX_DRAWDOWN_INVALID',
        message: 'max_drawdown must be greater than max_drawdown_change_percent',
    },
};
var StatusCodeSuccess;
(function (StatusCodeSuccess) {
    StatusCodeSuccess[StatusCodeSuccess["OK"] = 200] = "OK";
    StatusCodeSuccess[StatusCodeSuccess["CREATED"] = 201] = "CREATED";
    StatusCodeSuccess[StatusCodeSuccess["ACCEPTED"] = 202] = "ACCEPTED";
    StatusCodeSuccess[StatusCodeSuccess["NON_AUTHORITATIVE_INFORMATION"] = 203] = "NON_AUTHORITATIVE_INFORMATION";
    StatusCodeSuccess[StatusCodeSuccess["NO_CONTENT"] = 204] = "NO_CONTENT";
    StatusCodeSuccess[StatusCodeSuccess["RESET_CONTENT"] = 205] = "RESET_CONTENT";
    StatusCodeSuccess[StatusCodeSuccess["PARTIAL_CONTENT"] = 206] = "PARTIAL_CONTENT";
})(StatusCodeSuccess || (StatusCodeSuccess = {}));
var WS_EVENT;
(function (WS_EVENT) {
    WS_EVENT["connect"] = "connect";
    WS_EVENT["disconnect"] = "disconnect";
    WS_EVENT["logout"] = "logout";
    WS_EVENT["session_invalid"] = "session_invalid";
    WS_EVENT["verify"] = "verify";
    WS_EVENT["deactivate"] = "deactivate";
    WS_EVENT["check_device"] = "check_device";
    WS_EVENT["transaction_update"] = "transaction_update";
    WS_EVENT["bot_signal"] = "bot_signal";
    WS_EVENT["app_message"] = "app_message";
    WS_EVENT["tbot_init"] = "tbot_init";
    WS_EVENT["tbot_close"] = "tbot_close";
    WS_EVENT["tbot_update"] = "tbot_update";
    WS_EVENT["tbot_trade_update"] = "tbot_trade_update";
    WS_EVENT["tbot_chart_pnl_update"] = "tbot_chart_pnl_update";
    WS_EVENT["tbot_strategy_init"] = "tbot_strategy_init";
    WS_EVENT["tbot_strategy_close"] = "tbot_strategy_close";
    WS_EVENT["tbot_strategy_trade_update"] = "tbot_strategy_trade_update";
    WS_EVENT["tbot_strategy_chart_pnl_update"] = "tbot_strategy_chart_pnl_update";
})(WS_EVENT = exports.WS_EVENT || (exports.WS_EVENT = {}));
//# sourceMappingURL=response.js.map