import { HttpStatus } from '@nestjs/common';
import { Response } from 'express';
export declare const ERROR_CODE: {
    SUCCESS: {
        error_code: string;
        message: string;
    };
    UNKNOWN: {
        error_code: string;
        message: string;
    };
    BAD_REQUEST: {
        error_code: string;
        message: string;
    };
    UNPROCESSABLE: {
        error_code: string;
        message: string;
    };
    NOT_FOUND: {
        error_code: string;
        message: string;
    };
    RESOURCES_EXISTED: {
        error_code: string;
        message: string;
    };
    USER_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    EMAIL_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    EMAIL_EXISTED: {
        error_code: string;
        message: string;
    };
    EMAIL_NOT_VERIFIED: {
        error_code: string;
        message: string;
    };
    EMAIL_VERIFIED_NOT_ENOUGH: {
        error_code: string;
        message: string;
    };
    LOGIN_FAILED: {
        error_code: string;
        message: string;
    };
    SESSION_VERIFY_EMAIL_EXPIRED: {
        error_code: string;
        message: string;
    };
    SEND_EMAIL_FAILED: {
        error_code: string;
        message: string;
    };
    SESSION_LOGION_EXPIRED: {
        error_code: string;
        message: string;
    };
    SESSION_INVALID: {
        error_code: string;
        message: string;
    };
    AUTHORIZATION_REQUIRED: {
        error_code: string;
        message: string;
    };
    SIGNATURE_INVALID: {
        error_code: string;
        message: string;
    };
    CHECKSUM_INVALID: {
        error_code: string;
        message: string;
    };
    TOKEN_EXPIRED: {
        error_code: string;
        message: string;
    };
    TOKEN_INVALID: {
        error_code: string;
        message: string;
    };
    DEACTIVE_ACCOUNT: {
        error_code: string;
        message: string;
    };
    ROLE_EXISTED: {
        error_code: string;
        message: string;
    };
    ROLE_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    ROLE_INVALID: {
        error_code: string;
        message: string;
    };
    NO_PERMISSION: {
        error_code: string;
        message: string;
    };
    FEATURE_EXISTED: {
        error_code: string;
        message: string;
    };
    FEATURE_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    EXCHANGE_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    RESOLUTION_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    RESOLUTION_EXISTED: {
        error_code: string;
        message: string;
    };
    GENERAL_SETTING_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    GENERAL_SETTING_EXISTED: {
        error_code: string;
        message: string;
    };
    SYMBOL_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    SYMBOL_SETTING_ROLE_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    APP_SETTING_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    APP_SETTING_EXISTED: {
        error_code: string;
        message: string;
    };
    CREATE_TRANSACTION_FAILED: {
        error_code: string;
        message: string;
    };
    TRANSACTION_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    TRANSACTION_PROCESSING: {
        error_code: string;
        message: string;
    };
    TRANSACTION_PARENT_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    PACKAGE_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    PACKAGE_UPGRADED: {
        error_code: string;
        message: string;
    };
    PACKAGE_UPGRADE_PROCESSING: {
        error_code: string;
        message: string;
    };
    USERNAME_EXISTED: {
        error_code: string;
        message: string;
    };
    VERIFY_CODE_INVALID: {
        error_code: string;
        message: string;
    };
    PASSWORD_INCORRECT: {
        error_code: string;
        message: string;
    };
    PACKAGE_UPGRADE_SUCCESS: {
        error_code: string;
        message: string;
    };
    BOT_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    BOT_INVALID: {
        error_code: string;
        message: string;
    };
    AMOUNT_INVALID: {
        error_code: string;
        message: string;
    };
    SIGNAL_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    FAVORITE_SIGNAL_EXISTED: {
        error_code: string;
        message: string;
    };
    FAVORITE_SIGNAL_NOT_FOUND: {
        error_code: string;
        message: string;
    };
    ACCOUNT_INVALID: {
        error_code: string;
        message: string;
    };
    ACCOUNT_EXISTED: {
        error_code: string;
        message: string;
    };
    NUMBER_OF_ACCOUNT_LIMITED: {
        error_code: string;
        message: string;
    };
    ACCOUNT_CONNECTED: {
        error_code: string;
        message: string;
    };
    ACCOUNT_NOT_CONNECTED: {
        error_code: string;
        message: string;
    };
    ACCOUNT_NOT_ACTIVE: {
        error_code: string;
        message: string;
    };
    ACCOUNT_INACTIVE_BY_SYSTEM: {
        error_code: string;
        message: string;
    };
    ACCOUNT_NOT_ENOUGH_BALANCE: {
        error_code: string;
        message: string;
    };
    ACCOUNT_NOT_REFERRED: {
        error_code: string;
        message: string;
    };
    POSITION_NOT_EMPTY: {
        error_code: string;
        message: string;
    };
    STATUS_INVALID: {
        error_code: string;
        message: string;
    };
    CONNECT_ACCOUNT_FAILED: {
        error_code: string;
        message: string;
    };
    PROFILE_REQUIRED: {
        error_code: string;
        message: string;
    };
    ACCOUNT_INFO_INCORRECT: {
        error_code: string;
        message: string;
    };
    USERS_HAVE_BOTS: {
        error_code: string;
        message: string;
    };
    USERS_HAVE_ROLES: {
        error_code: string;
        message: string;
    };
    RESOURCES_NOT_EXISTED: {
        error_code: string;
        message: string;
    };
    TBOT_BLOCKED: {
        error_code: string;
        message: string;
    };
    MAX_DRAWDOWN_INVALID: {
        error_code: string;
        message: string;
    };
};
declare enum StatusCodeSuccess {
    OK = 200,
    CREATED = 201,
    ACCEPTED = 202,
    NON_AUTHORITATIVE_INFORMATION = 203,
    NO_CONTENT = 204,
    RESET_CONTENT = 205,
    PARTIAL_CONTENT = 206
}
export interface ISuccess {
    payload: any;
    res: Response;
    message?: string;
    status?: StatusCodeSuccess;
}
export interface IReason {
    status: HttpStatus;
    error_code: string;
    message: string | string[];
}
export interface IFailed {
    payload?: any;
    res: Response;
    reason: IReason;
}
export declare enum WS_EVENT {
    connect = "connect",
    disconnect = "disconnect",
    logout = "logout",
    session_invalid = "session_invalid",
    verify = "verify",
    deactivate = "deactivate",
    check_device = "check_device",
    transaction_update = "transaction_update",
    bot_signal = "bot_signal",
    app_message = "app_message",
    tbot_init = "tbot_init",
    tbot_close = "tbot_close",
    tbot_update = "tbot_update",
    tbot_trade_update = "tbot_trade_update",
    tbot_chart_pnl_update = "tbot_chart_pnl_update",
    tbot_strategy_init = "tbot_strategy_init",
    tbot_strategy_close = "tbot_strategy_close",
    tbot_strategy_trade_update = "tbot_strategy_trade_update",
    tbot_strategy_chart_pnl_update = "tbot_strategy_chart_pnl_update"
}
export {};
