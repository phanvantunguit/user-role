export declare type IToken = {
    user_id: string;
    email: string;
    merchant_code: string;
    iss: string;
    roles: string[];
    super_user: boolean;
    token_id: string;
};
export declare enum MERCHANT_TYPE {
    COINMAP = "COINMAP",
    OTHERS = "OTHERS"
}
