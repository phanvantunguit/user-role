export declare const Brokers: {
    code: string;
    name: string;
}[];
export declare const BrokerServers: {
    xm: string[];
    axi: string[];
};
export declare enum BROKER_CODE {
    axi = "axi",
    xm = "xm",
    fxlink = "fxlink"
}
export declare enum BOT_TRADING_EVENT {
    CONNECTING = "CONNECTING",
    CONNECTED = "CONNECTED",
    INACTIVE_BY_SYSTEM = "INACTIVE_BY_SYSTEM",
    DISCONNECTED = "DISCONNECTED",
    EXPIRED = "EXPIRED"
}
export declare enum TRADE_HISTORY_STATUS {
    OPEN = "OPEN",
    CLOSED = "CLOSED"
}
export declare enum TRADE_SIDE {
    LONG = "LONG",
    SHORT = "SHORT"
}
export declare enum TBOT_CHANEL_TS_EVENT {
    trades = "trades",
    account = "account",
    connect_stream = "connect_stream",
    disconnect_stream = "disconnect_stream",
    strategy_trades = "strategy_trades",
    add_strategy = "add_strategy",
    add_subscriber = "add_subscriber"
}
export declare enum TBOT_PLATFORM {
    mt4 = "mt4",
    mt5 = "mt5"
}
