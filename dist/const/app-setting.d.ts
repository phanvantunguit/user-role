export declare enum ON_OFF_REGISTER {
    ON = "ON",
    OFF = "OFF"
}
export declare const APP_SETTING: {
    ON_OFF_REGISTER: string;
    SIGNAL_PLATFORM: string;
    MERCHANT_CODE_DEFAULT: string;
    ON_OFF_LOCALSTORAGE: string;
    NUMBER_OF_TBOT: string;
    NUMBER_OF_TBOT_USED: string;
    TBOT_INACTIVE_BY_SYSTEM: string;
    BROKER_SERVER: string;
    CURRENCY_PKG: string;
    CURRENCY_SBOT: string;
    CURRENCY_TBOT: string;
    BROKER_SETTING: string;
};
export declare const SERVICE_NAME = "cm-user-roles";
export declare const CHANELS: {
    WS_CHANEL: string;
    SIGNAL_BOT_CHANEL: string;
    TBOT_CHANEL_TS: string;
    TBOT_CHANEL_UR: string;
};
export declare const ConnectionName: {
    user_role: string;
    payment_service: string;
    dmlcmghrkhmif: string;
};
export declare type SettingSignalPlatfrom = {
    ON_OFF_PUSH_NOTIFICATION: 'ON' | 'OFF';
    ON_OFF_SOUND_NOTIFICATION: 'ON' | 'OFF';
    OFF_SIGNALS_BOT: string;
};
export declare enum EVENT_STORE_STATE {
    OPEN = "OPEN",
    PROCESSING = "PROCESSING",
    ERROR = "ERROR",
    CLOSED = "CLOSED"
}
export declare enum EVENT_STORE_NAME {
    TBOT_INACTIVE_BY_SYSTEM = "TBOT_INACTIVE_BY_SYSTEM",
    TBOT_STOP_INACTIVE_BY_SYSTEM = "TBOT_STOP_INACTIVE_BY_SYSTEM",
    TBOT_CONNECTED = "TBOT_CONNECTED",
    INP_PAYMENT_TBOT = "INP_PAYMENT_TBOT",
    TBOT_SYNC = "TBOT_SYNC",
    TBOT_STOP_OUT = "TBOT_STOP_OUT",
    TBOT_ACTIVE_PROCESSING = "TBOT_ACTIVE_PROCESSING",
    TBOT_CONNECTING = "TBOT_CONNECTING"
}
