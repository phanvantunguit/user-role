"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TBOT_PLATFORM = exports.TBOT_CHANEL_TS_EVENT = exports.TRADE_SIDE = exports.TRADE_HISTORY_STATUS = exports.BOT_TRADING_EVENT = exports.BROKER_CODE = exports.BrokerServers = exports.Brokers = void 0;
exports.Brokers = [
    {
        code: 'axi',
        name: 'AxiCorp Financial Services Pty Ltd',
    },
    {
        code: 'xm',
        name: 'XmCorp Financial Services Pty Ltd',
    },
];
exports.BrokerServers = {
    xm: ['XMGlobal-Demo'],
    axi: ['Axi.SVG-US10-Live', 'Axi.SVG-US03-Demo', 'Axi.SVG-US888-Demo'],
};
var BROKER_CODE;
(function (BROKER_CODE) {
    BROKER_CODE["axi"] = "axi";
    BROKER_CODE["xm"] = "xm";
    BROKER_CODE["fxlink"] = "fxlink";
})(BROKER_CODE = exports.BROKER_CODE || (exports.BROKER_CODE = {}));
var BOT_TRADING_EVENT;
(function (BOT_TRADING_EVENT) {
    BOT_TRADING_EVENT["CONNECTING"] = "CONNECTING";
    BOT_TRADING_EVENT["CONNECTED"] = "CONNECTED";
    BOT_TRADING_EVENT["INACTIVE_BY_SYSTEM"] = "INACTIVE_BY_SYSTEM";
    BOT_TRADING_EVENT["DISCONNECTED"] = "DISCONNECTED";
    BOT_TRADING_EVENT["EXPIRED"] = "EXPIRED";
})(BOT_TRADING_EVENT = exports.BOT_TRADING_EVENT || (exports.BOT_TRADING_EVENT = {}));
var TRADE_HISTORY_STATUS;
(function (TRADE_HISTORY_STATUS) {
    TRADE_HISTORY_STATUS["OPEN"] = "OPEN";
    TRADE_HISTORY_STATUS["CLOSED"] = "CLOSED";
})(TRADE_HISTORY_STATUS = exports.TRADE_HISTORY_STATUS || (exports.TRADE_HISTORY_STATUS = {}));
var TRADE_SIDE;
(function (TRADE_SIDE) {
    TRADE_SIDE["LONG"] = "LONG";
    TRADE_SIDE["SHORT"] = "SHORT";
})(TRADE_SIDE = exports.TRADE_SIDE || (exports.TRADE_SIDE = {}));
var TBOT_CHANEL_TS_EVENT;
(function (TBOT_CHANEL_TS_EVENT) {
    TBOT_CHANEL_TS_EVENT["trades"] = "trades";
    TBOT_CHANEL_TS_EVENT["account"] = "account";
    TBOT_CHANEL_TS_EVENT["connect_stream"] = "connect_stream";
    TBOT_CHANEL_TS_EVENT["disconnect_stream"] = "disconnect_stream";
    TBOT_CHANEL_TS_EVENT["strategy_trades"] = "strategy_trades";
    TBOT_CHANEL_TS_EVENT["add_strategy"] = "add_strategy";
    TBOT_CHANEL_TS_EVENT["add_subscriber"] = "add_subscriber";
})(TBOT_CHANEL_TS_EVENT = exports.TBOT_CHANEL_TS_EVENT || (exports.TBOT_CHANEL_TS_EVENT = {}));
var TBOT_PLATFORM;
(function (TBOT_PLATFORM) {
    TBOT_PLATFORM["mt4"] = "mt4";
    TBOT_PLATFORM["mt5"] = "mt5";
})(TBOT_PLATFORM = exports.TBOT_PLATFORM || (exports.TBOT_PLATFORM = {}));
//# sourceMappingURL=bot.js.map