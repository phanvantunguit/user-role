"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.USER_EVENT = exports.GENDER = exports.USER_TYPE = exports.PasswordRegex = exports.VERIFY_TOKEN_TYPE = void 0;
var VERIFY_TOKEN_TYPE;
(function (VERIFY_TOKEN_TYPE) {
    VERIFY_TOKEN_TYPE["RESET_PASSWORD"] = "RESET_PASSWORD";
    VERIFY_TOKEN_TYPE["VERIFY_EMAIL"] = "VERIFY_EMAIL";
    VERIFY_TOKEN_TYPE["CHANGE_EMAIL"] = "CHANGE_EMAIL";
})(VERIFY_TOKEN_TYPE = exports.VERIFY_TOKEN_TYPE || (exports.VERIFY_TOKEN_TYPE = {}));
exports.PasswordRegex = `^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9!@#$%^&*(),.~/?=|;:'"{}<>]{8,}$`;
var USER_TYPE;
(function (USER_TYPE) {
    USER_TYPE["USER"] = "USER";
    USER_TYPE["ADMIN"] = "ADMIN";
})(USER_TYPE = exports.USER_TYPE || (exports.USER_TYPE = {}));
var GENDER;
(function (GENDER) {
    GENDER["MALE"] = "male";
    GENDER["FEMALE"] = "female";
    GENDER["OTHER"] = "other";
})(GENDER = exports.GENDER || (exports.GENDER = {}));
var USER_EVENT;
(function (USER_EVENT) {
    USER_EVENT["LOGIN"] = "LOGIN";
})(USER_EVENT = exports.USER_EVENT || (exports.USER_EVENT = {}));
//# sourceMappingURL=user.js.map