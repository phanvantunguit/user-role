import { PermissionRepository } from 'src/repositories/permission.repository';
import { UserRepository } from 'src/repositories/user.repository';
export declare class UserListRolePlanHandler {
    private userRepository;
    private permissionRepo;
    constructor(userRepository: UserRepository, permissionRepo: PermissionRepository);
    execute(user_id: string): Promise<any[]>;
}
