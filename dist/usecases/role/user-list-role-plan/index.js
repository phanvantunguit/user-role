"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListRolePlanHandler = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../../const");
const permission_repository_1 = require("../../../repositories/permission.repository");
const user_repository_1 = require("../../../repositories/user.repository");
let UserListRolePlanHandler = class UserListRolePlanHandler {
    constructor(userRepository, permissionRepo) {
        this.userRepository = userRepository;
        this.permissionRepo = permissionRepo;
    }
    async execute(user_id) {
        const userRoles = await this.userRepository.findUserRole({
            user_id,
        });
        const userRolesFilter = [];
        const userIds = [];
        userRoles.forEach((ur) => {
            ur.role_id;
            const indexUR = userRolesFilter.findIndex((urf) => urf.role_id === ur.role_id);
            if (indexUR > -1) {
                if (ur.expires_at === null ||
                    Number(ur.expires_at) > Number(userRolesFilter[indexUR].expires_at)) {
                    userRolesFilter.slice(indexUR, 1);
                    userRolesFilter.push(ur);
                    userIds.push(ur.role_id);
                }
            }
            else {
                userRolesFilter.push(ur);
                userIds.push(ur.role_id);
            }
        });
        const roles = await this.permissionRepo.findRoleByIds(userIds);
        const mapRoles = roles.map((r) => {
            const userRoleExit = userRolesFilter.find((ub) => ub.role_id === r.id);
            if (userRoleExit) {
                return Object.assign(Object.assign({}, r), { expires_at: userRoleExit.expires_at });
            }
            else {
                return r;
            }
        });
        return mapRoles;
    }
};
UserListRolePlanHandler = __decorate([
    __param(0, common_1.Inject(user_repository_1.UserRepository)),
    __param(1, common_1.Inject(permission_repository_1.PermissionRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        permission_repository_1.PermissionRepository])
], UserListRolePlanHandler);
exports.UserListRolePlanHandler = UserListRolePlanHandler;
//# sourceMappingURL=index.js.map