import { ORDER_CATEGORY } from 'src/const/transaction';
export declare class AdminRemoveUserAssetInput {
    user_id: string;
    category: ORDER_CATEGORY;
    asset_id: string;
}
