"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRemoveUserAssetHandler = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../../const");
const transaction_1 = require("../../../const/transaction");
const bot_trading_repository_1 = require("../../../repositories/bot-trading.repository");
const bot_repository_1 = require("../../../repositories/bot.repository");
const permission_repository_1 = require("../../../repositories/permission.repository");
const user_repository_1 = require("../../../repositories/user.repository");
const handle_error_util_1 = require("../../../utils/handle-error.util");
let AdminRemoveUserAssetHandler = class AdminRemoveUserAssetHandler {
    constructor(permissionRepository, botRepository, botTradingRepository, userRepository) {
        this.permissionRepository = permissionRepository;
        this.botRepository = botRepository;
        this.botTradingRepository = botTradingRepository;
        this.userRepository = userRepository;
    }
    async execute(params, ownerCreated) {
        const { user_id, category, asset_id } = params;
        const updateUserAssetLog = {
            user_id: user_id,
            category,
            status: transaction_1.ITEM_STATUS.DELETED,
            owner_created: ownerCreated,
        };
        let asset;
        switch (category) {
            case transaction_1.ORDER_CATEGORY.PKG:
                asset = await this.permissionRepository.findOneRoleTable({
                    id: asset_id,
                });
                if (asset) {
                    await this.userRepository.deleteUserRoles([user_id], [asset_id]);
                    updateUserAssetLog['name'] = asset.role_name;
                }
                break;
            case transaction_1.ORDER_CATEGORY.SBOT:
                asset = await this.botRepository.findById(asset_id);
                if (asset) {
                    await this.userRepository.deleteUserBot({
                        user_id,
                        bot_id: asset_id,
                    });
                    updateUserAssetLog['name'] = asset.name;
                }
                break;
            case transaction_1.ORDER_CATEGORY.TBOT:
                asset = await this.botTradingRepository.findById(asset_id);
                if (asset) {
                    const userBot = await this.userRepository.findUserBotTrading({
                        user_id,
                        bot_id: asset_id,
                    });
                    if (userBot.length > 0 && userBot[0].subscriber_id) {
                        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ACCOUNT_CONNECTED));
                    }
                    await this.userRepository.deleteUserBotTrading({
                        user_id,
                        bot_id: asset_id,
                    });
                    updateUserAssetLog['name'] = asset.name;
                }
                break;
            default:
                break;
        }
        if (!asset) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.NOT_FOUND));
        }
        updateUserAssetLog['asset_id'] = asset.id;
        await this.userRepository.saveUserAssetLog([updateUserAssetLog]);
    }
};
AdminRemoveUserAssetHandler = __decorate([
    __param(0, common_1.Inject(permission_repository_1.PermissionRepository)),
    __param(1, common_1.Inject(bot_repository_1.BotRepository)),
    __param(2, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(3, common_1.Inject(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [permission_repository_1.PermissionRepository,
        bot_repository_1.BotRepository,
        bot_trading_repository_1.BotTradingRepository,
        user_repository_1.UserRepository])
], AdminRemoveUserAssetHandler);
exports.AdminRemoveUserAssetHandler = AdminRemoveUserAssetHandler;
//# sourceMappingURL=index.js.map