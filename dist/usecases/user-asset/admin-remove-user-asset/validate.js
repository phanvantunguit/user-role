"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRemoveUserAssetInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const transaction_1 = require("../../../const/transaction");
class AdminRemoveUserAssetInput {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminRemoveUserAssetInput.prototype, "user_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: `category of item: ${Object.values(transaction_1.ORDER_CATEGORY)}`,
        example: transaction_1.ORDER_CATEGORY.SBOT,
    }),
    class_validator_1.IsIn(Object.values(transaction_1.ORDER_CATEGORY)),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminRemoveUserAssetInput.prototype, "category", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'item id',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminRemoveUserAssetInput.prototype, "asset_id", void 0);
exports.AdminRemoveUserAssetInput = AdminRemoveUserAssetInput;
//# sourceMappingURL=validate.js.map