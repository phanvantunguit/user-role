import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { BotRepository } from 'src/repositories/bot.repository';
import { PermissionRepository } from 'src/repositories/permission.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { AdminRemoveUserAssetInput } from './validate';
export declare class AdminRemoveUserAssetHandler {
    private permissionRepository;
    private botRepository;
    private botTradingRepository;
    private userRepository;
    constructor(permissionRepository: PermissionRepository, botRepository: BotRepository, botTradingRepository: BotTradingRepository, userRepository: UserRepository);
    execute(params: AdminRemoveUserAssetInput, ownerCreated: string): Promise<any>;
}
