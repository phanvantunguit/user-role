import { PACKAGE_TYPE } from 'src/const';
import { ORDER_CATEGORY } from 'src/const/transaction';
export declare class AdminAddUserAssetInput {
    user_id: string;
    category: ORDER_CATEGORY;
    asset_id: string;
    quantity: number;
    type: PACKAGE_TYPE;
}
