import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { BotRepository } from 'src/repositories/bot.repository';
import { PermissionRepository } from 'src/repositories/permission.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { AdminAddUserAssetInput } from './validate';
export declare class AdminAddUserAssetHandler {
    private permissionRepository;
    private botRepository;
    private botTradingRepository;
    private userRepository;
    constructor(permissionRepository: PermissionRepository, botRepository: BotRepository, botTradingRepository: BotTradingRepository, userRepository: UserRepository);
    execute(params: AdminAddUserAssetInput, ownerCreated: string): Promise<any>;
}
