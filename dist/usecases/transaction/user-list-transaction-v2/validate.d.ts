import { UserListTransactionV2Validate } from '../validate';
export declare class ListTransactionV2PayloadOutput {
    payload: UserListTransactionV2Validate[];
}
