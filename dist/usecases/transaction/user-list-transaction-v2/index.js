"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListTransactionV2Handler = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../../const");
const cmpayment_1 = require("../../../resources/cmpayment");
let ListTransactionV2Handler = class ListTransactionV2Handler {
    constructor(cmpaymentResource) {
        this.cmpaymentResource = cmpaymentResource;
    }
    async execute(user_id) {
        const dataRes = await this.cmpaymentResource.listTransaction({
            user_id,
        });
        if (dataRes.error_code === const_1.ERROR_CODE.SUCCESS.error_code) {
            const result = dataRes.payload.map((paymentResult) => ({
                transaction_id: paymentResult.id,
                order_id: paymentResult.order_id,
                amount: paymentResult.amount,
                currency: paymentResult.currency,
                payment_method: paymentResult.payment_method,
                status: paymentResult.status,
                payment_id: paymentResult.payment_id,
                updated_at: paymentResult.updated_at,
                created_at: paymentResult.created_at,
            }));
            return result;
        }
        return dataRes;
    }
};
ListTransactionV2Handler = __decorate([
    __param(0, common_1.Inject(cmpayment_1.CmpaymentResource)),
    __metadata("design:paramtypes", [cmpayment_1.CmpaymentResource])
], ListTransactionV2Handler);
exports.ListTransactionV2Handler = ListTransactionV2Handler;
//# sourceMappingURL=index.js.map