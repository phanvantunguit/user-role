import { CmpaymentResource } from 'src/resources/cmpayment';
import { ListTransactionV2Validate } from '../validate';
export declare class ListTransactionV2Handler {
    private cmpaymentResource;
    constructor(cmpaymentResource: CmpaymentResource);
    execute(user_id: string): Promise<ListTransactionV2Validate[]>;
}
