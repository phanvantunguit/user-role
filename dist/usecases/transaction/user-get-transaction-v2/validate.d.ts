import { PAYMENT_METHOD, TRANSACTION_STATUS } from 'src/const/transaction';
import { ItemDataValidate } from '../user-create-transaction-v2/validate';
export declare class DetailTransactionV2Output {
    transaction_id: string;
    order_id: string;
    amount: string;
    currency: string;
    payment_method: PAYMENT_METHOD;
    status: TRANSACTION_STATUS;
    payment_id: string;
    wallet_address: string;
    timeout: string;
    qrcode_url: string;
    status_url: string;
    checkout_url: string;
    items: ItemDataValidate[];
    created_at: number;
    updated_at: number;
}
export declare class DetailTransactionV2PayloadOutput {
    payload: DetailTransactionV2Output;
}
