"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetTransactionV2Handler = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../../const");
const cmpayment_1 = require("../../../resources/cmpayment");
let GetTransactionV2Handler = class GetTransactionV2Handler {
    constructor(cmpaymentResource) {
        this.cmpaymentResource = cmpaymentResource;
    }
    async execute(id) {
        var _a;
        const dataRes = await this.cmpaymentResource.getTransaction(id);
        if (dataRes.error_code === const_1.ERROR_CODE.SUCCESS.error_code) {
            const paymentResult = dataRes.payload;
            return {
                transaction_id: paymentResult.id,
                order_id: paymentResult.order_id,
                amount: paymentResult.amount,
                currency: paymentResult.currency,
                payment_method: paymentResult.payment_method,
                status: paymentResult.status,
                payment_id: paymentResult.payment_id,
                items: paymentResult.items,
                wallet_address: paymentResult.wallet_address,
                timeout: (_a = paymentResult.timeout) === null || _a === void 0 ? void 0 : _a.toString(),
                qrcode_url: paymentResult.qrcode_url,
                status_url: paymentResult.status_url,
                checkout_url: paymentResult.checkout_url,
                updated_at: paymentResult.updated_at,
                created_at: paymentResult.created_at,
            };
        }
        return dataRes;
    }
};
GetTransactionV2Handler = __decorate([
    __param(0, common_1.Inject(cmpayment_1.CmpaymentResource)),
    __metadata("design:paramtypes", [cmpayment_1.CmpaymentResource])
], GetTransactionV2Handler);
exports.GetTransactionV2Handler = GetTransactionV2Handler;
//# sourceMappingURL=index.js.map