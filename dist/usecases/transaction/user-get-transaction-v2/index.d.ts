import { CmpaymentResource } from 'src/resources/cmpayment';
import { DetailTransactionV2Output } from './validate';
export declare class GetTransactionV2Handler {
    private cmpaymentResource;
    constructor(cmpaymentResource: CmpaymentResource);
    execute(id: string): Promise<DetailTransactionV2Output>;
}
