"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DetailTransactionV2PayloadOutput = exports.DetailTransactionV2Output = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const transaction_1 = require("../../../const/transaction");
const validate_1 = require("../user-create-transaction-v2/validate");
class DetailTransactionV2Output {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailTransactionV2Output.prototype, "transaction_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'BOT1802220001',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailTransactionV2Output.prototype, "order_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailTransactionV2Output.prototype, "amount", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Currency for user payment',
        example: 'LTCT',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailTransactionV2Output.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Payment method',
        example: transaction_1.PAYMENT_METHOD.COIN_PAYMENT,
    }),
    class_validator_1.IsString(),
    class_validator_1.IsIn(Object.values(transaction_1.PAYMENT_METHOD)),
    __metadata("design:type", String)
], DetailTransactionV2Output.prototype, "payment_method", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: Object.values(transaction_1.TRANSACTION_STATUS).join(','),
        example: transaction_1.TRANSACTION_STATUS.CREATED,
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailTransactionV2Output.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Payment id of coinpayment',
        example: 'OAINONOFASF1203412',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailTransactionV2Output.prototype, "payment_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'wdfghjkloiu1yt2r367893',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailTransactionV2Output.prototype, "wallet_address", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'timeout second',
        example: '5000',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailTransactionV2Output.prototype, "timeout", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'link qrcode',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailTransactionV2Output.prototype, "qrcode_url", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'link status',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailTransactionV2Output.prototype, "status_url", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'link checkout',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], DetailTransactionV2Output.prototype, "checkout_url", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        type: validate_1.ItemDataValidate,
        isArray: true,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsArray(),
    class_validator_1.ValidateNested({ each: true }),
    class_transformer_1.Type(() => validate_1.ItemDataValidate),
    __metadata("design:type", Array)
], DetailTransactionV2Output.prototype, "items", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsString(),
    __metadata("design:type", Number)
], DetailTransactionV2Output.prototype, "created_at", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsString(),
    __metadata("design:type", Number)
], DetailTransactionV2Output.prototype, "updated_at", void 0);
exports.DetailTransactionV2Output = DetailTransactionV2Output;
class DetailTransactionV2PayloadOutput {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Transaction info',
    }),
    class_validator_1.IsObject(),
    __metadata("design:type", DetailTransactionV2Output)
], DetailTransactionV2PayloadOutput.prototype, "payload", void 0);
exports.DetailTransactionV2PayloadOutput = DetailTransactionV2PayloadOutput;
//# sourceMappingURL=validate.js.map