import { ITEM_STATUS, ORDER_CATEGORY, PAYMENT_METHOD, TRANSACTION_STATUS } from 'src/const/transaction';
export declare class ListTransactionV2Validate {
    order_id?: string;
    amount?: string;
    currency?: string;
    payment_method?: PAYMENT_METHOD;
    status?: TRANSACTION_STATUS;
    payment_id?: string;
    created_at?: number;
    updated_at?: number;
}
export declare class UserListTransactionV2Validate extends ListTransactionV2Validate {
    transaction_id: string;
}
export declare class AdminListTransactionV2Validate extends ListTransactionV2Validate {
    id?: string;
    user_id?: string;
    fullname?: string;
    email?: string;
}
export declare class TransactionV2MetadataValidate {
    id?: string;
    transaction_id?: string;
    attribute?: string;
    value?: string;
    created_at?: number;
}
export declare class TransactionV2LogValidate {
    id?: string;
    transaction_id?: string;
    transaction_event?: string;
    transaction_status?: string;
    metadata?: any;
    created_at?: number;
}
export declare class UserAssetLogValidate {
    id: string;
    order_id: string;
    asset_id: string;
    package_type: string;
    category: ORDER_CATEGORY;
    quantity: number;
    expires_at: number;
    status: ITEM_STATUS;
    name: string;
    price: string;
    discount_rate: number;
    discount_amount: number;
    created_at: number;
    updated_at: number;
}
