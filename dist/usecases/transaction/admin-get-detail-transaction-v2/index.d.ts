import { TransactionPSRepository } from 'src/repositories/transaction-ps.repository';
import { AdminGetTransactionV2Output } from './validate';
export declare class AdminGetTransactionV2Handler {
    private transactionPSRepository;
    constructor(transactionPSRepository: TransactionPSRepository);
    execute(params: {
        id: string;
    }): Promise<AdminGetTransactionV2Output>;
}
