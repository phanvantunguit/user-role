import { AdminListTransactionV2Validate, TransactionV2LogValidate, TransactionV2MetadataValidate } from '../validate';
export declare class AdminGetTransactionV2Output {
    transaction: AdminListTransactionV2Validate;
    metadatas: TransactionV2MetadataValidate[];
    logs: TransactionV2LogValidate[];
}
export declare class AdminGetTransactionV2PayloadOutput {
    payload: AdminGetTransactionV2Output;
}
