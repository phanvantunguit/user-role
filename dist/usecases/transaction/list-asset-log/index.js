"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListAssetLogHandler = void 0;
const common_1 = require("@nestjs/common");
const transaction_1 = require("../../../const/transaction");
const user_repository_1 = require("../../../repositories/user.repository");
let ListAssetLogHandler = class ListAssetLogHandler {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    async execute(params, forUser, user_id) {
        const { page, keyword, size, category } = params;
        let defaultStatus;
        let isOrder;
        if (forUser) {
            defaultStatus = [transaction_1.ITEM_STATUS.ACTIVE, transaction_1.ITEM_STATUS.PROCESSING, transaction_1.ITEM_STATUS.NOT_CONNECT, transaction_1.ITEM_STATUS.FAILED];
            isOrder = true;
        }
        const status = params.status ? [params.status] : defaultStatus;
        const dataRes = await this.userRepository.findUserAssetLogPaging({
            page,
            size,
            keyword,
            status,
            user_id,
            is_order: isOrder,
            category,
        });
        return dataRes;
    }
};
ListAssetLogHandler = __decorate([
    __param(0, common_1.Inject(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository])
], ListAssetLogHandler);
exports.ListAssetLogHandler = ListAssetLogHandler;
//# sourceMappingURL=index.js.map