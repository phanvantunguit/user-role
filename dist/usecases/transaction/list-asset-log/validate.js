"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListAssetLogPayloadOutput = exports.ListAssetLogOutput = exports.ListAssetLogInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const transaction_1 = require("../../../const/transaction");
const validate_1 = require("../validate");
class ListAssetLogInput {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Page',
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], ListAssetLogInput.prototype, "page", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Size',
        example: 50,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], ListAssetLogInput.prototype, "size", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'The keyword name, order id',
        example: 'john',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], ListAssetLogInput.prototype, "keyword", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: `${Object.keys(transaction_1.ITEM_STATUS).join(',')}`,
        example: transaction_1.ITEM_STATUS.ACTIVE,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], ListAssetLogInput.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: `${Object.keys(transaction_1.ORDER_CATEGORY).join(',')}`,
        example: transaction_1.ORDER_CATEGORY.SBOT,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], ListAssetLogInput.prototype, "category", void 0);
exports.ListAssetLogInput = ListAssetLogInput;
class ListAssetLogOutput {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Page',
        example: 1,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], ListAssetLogOutput.prototype, "page", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Size',
        example: 50,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], ListAssetLogOutput.prototype, "size", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], ListAssetLogOutput.prototype, "count", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], ListAssetLogOutput.prototype, "total", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        isArray: true,
        type: validate_1.UserAssetLogValidate,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsArray(),
    class_transformer_1.Type(() => validate_1.UserAssetLogValidate),
    __metadata("design:type", Array)
], ListAssetLogOutput.prototype, "rows", void 0);
exports.ListAssetLogOutput = ListAssetLogOutput;
class ListAssetLogPayloadOutput {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        type: ListAssetLogOutput,
    }),
    class_validator_1.IsObject(),
    __metadata("design:type", ListAssetLogOutput)
], ListAssetLogPayloadOutput.prototype, "payload", void 0);
exports.ListAssetLogPayloadOutput = ListAssetLogPayloadOutput;
//# sourceMappingURL=validate.js.map