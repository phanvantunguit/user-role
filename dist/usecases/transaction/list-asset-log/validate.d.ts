import { ITEM_STATUS, ORDER_CATEGORY } from 'src/const/transaction';
import { UserAssetLogValidate } from '../validate';
export declare class ListAssetLogInput {
    page: number;
    size: number;
    keyword: string;
    status: ITEM_STATUS;
    category: ORDER_CATEGORY;
}
export declare class ListAssetLogOutput {
    page: number;
    size: number;
    count: number;
    total: number;
    rows: UserAssetLogValidate[];
}
export declare class ListAssetLogPayloadOutput {
    payload: ListAssetLogOutput;
}
