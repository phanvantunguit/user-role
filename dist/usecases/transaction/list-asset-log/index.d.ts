import { UserRepository } from 'src/repositories/user.repository';
import { ListAssetLogInput, ListAssetLogOutput } from './validate';
export declare class ListAssetLogHandler {
    private userRepository;
    constructor(userRepository: UserRepository);
    execute(params: ListAssetLogInput, forUser: boolean, user_id?: string): Promise<ListAssetLogOutput>;
}
