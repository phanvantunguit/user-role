"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserAssetLogValidate = exports.TransactionV2LogValidate = exports.TransactionV2MetadataValidate = exports.AdminListTransactionV2Validate = exports.UserListTransactionV2Validate = exports.ListTransactionV2Validate = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const const_1 = require("../../const");
const transaction_1 = require("../../const/transaction");
class ListTransactionV2Validate {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'BOT1802220001',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], ListTransactionV2Validate.prototype, "order_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], ListTransactionV2Validate.prototype, "amount", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Currency for user payment',
        example: 'LTCT',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], ListTransactionV2Validate.prototype, "currency", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Payment method',
        example: transaction_1.PAYMENT_METHOD.COIN_PAYMENT,
    }),
    class_validator_1.IsString(),
    class_validator_1.IsIn(Object.values(transaction_1.PAYMENT_METHOD)),
    __metadata("design:type", String)
], ListTransactionV2Validate.prototype, "payment_method", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: Object.values(transaction_1.TRANSACTION_STATUS).join(','),
        example: transaction_1.TRANSACTION_STATUS.CREATED,
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], ListTransactionV2Validate.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Payment id of coinpayment',
        example: 'OAINONOFASF1203412',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], ListTransactionV2Validate.prototype, "payment_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsString(),
    __metadata("design:type", Number)
], ListTransactionV2Validate.prototype, "created_at", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsString(),
    __metadata("design:type", Number)
], ListTransactionV2Validate.prototype, "updated_at", void 0);
exports.ListTransactionV2Validate = ListTransactionV2Validate;
class UserListTransactionV2Validate extends ListTransactionV2Validate {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserListTransactionV2Validate.prototype, "transaction_id", void 0);
exports.UserListTransactionV2Validate = UserListTransactionV2Validate;
class AdminListTransactionV2Validate extends ListTransactionV2Validate {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTransactionV2Validate.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e1',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTransactionV2Validate.prototype, "user_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 'Nguyen Van A',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTransactionV2Validate.prototype, "fullname", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 'nguyenvana@gmail.com',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTransactionV2Validate.prototype, "email", void 0);
exports.AdminListTransactionV2Validate = AdminListTransactionV2Validate;
class TransactionV2MetadataValidate {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TransactionV2MetadataValidate.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TransactionV2MetadataValidate.prototype, "transaction_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'wallet_address',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TransactionV2MetadataValidate.prototype, "attribute", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'moBpkWoiMmJJKhBjDXeXPWPaTJFRCdmM1g',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TransactionV2MetadataValidate.prototype, "value", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsString(),
    __metadata("design:type", Number)
], TransactionV2MetadataValidate.prototype, "created_at", void 0);
exports.TransactionV2MetadataValidate = TransactionV2MetadataValidate;
class TransactionV2LogValidate {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TransactionV2LogValidate.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TransactionV2LogValidate.prototype, "transaction_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'PAYMENT_COMPLETE',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TransactionV2LogValidate.prototype, "transaction_event", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'PROCESSING',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TransactionV2LogValidate.prototype, "transaction_status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: {},
    }),
    class_validator_1.IsObject(),
    __metadata("design:type", Object)
], TransactionV2LogValidate.prototype, "metadata", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsString(),
    __metadata("design:type", Number)
], TransactionV2LogValidate.prototype, "created_at", void 0);
exports.TransactionV2LogValidate = TransactionV2LogValidate;
class UserAssetLogValidate {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserAssetLogValidate.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'BOT010120230001',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserAssetLogValidate.prototype, "order_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserAssetLogValidate.prototype, "asset_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: const_1.PACKAGE_TYPE.MONTH,
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserAssetLogValidate.prototype, "package_type", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: transaction_1.ORDER_CATEGORY.SBOT,
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserAssetLogValidate.prototype, "category", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 1,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserAssetLogValidate.prototype, "quantity", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserAssetLogValidate.prototype, "expires_at", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: transaction_1.ITEM_STATUS.ACTIVE,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", String)
], UserAssetLogValidate.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: transaction_1.ITEM_STATUS.ACTIVE,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", String)
], UserAssetLogValidate.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '3',
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", String)
], UserAssetLogValidate.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 0.2,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserAssetLogValidate.prototype, "discount_rate", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 10,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserAssetLogValidate.prototype, "discount_amount", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserAssetLogValidate.prototype, "created_at", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserAssetLogValidate.prototype, "updated_at", void 0);
exports.UserAssetLogValidate = UserAssetLogValidate;
//# sourceMappingURL=validate.js.map