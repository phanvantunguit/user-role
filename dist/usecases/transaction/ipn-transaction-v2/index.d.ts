import { TRANSACTION_STATUS } from 'src/const/transaction';
import { CacheRepository } from 'src/repositories/cache.repository';
import { DBContext } from 'src/repositories/db-context';
import { EventStoreRepository } from 'src/repositories/event-store.repository';
import { UserRepository } from 'src/repositories/user.repository';
export declare class IPNTransactionV2Handler {
    private userRepository;
    private dBContext;
    private cacheRepository;
    private eventStoreRepository;
    constructor(userRepository: UserRepository, dBContext: DBContext, cacheRepository: CacheRepository, eventStoreRepository: EventStoreRepository);
    execute(param: {
        order_id: string;
        status: TRANSACTION_STATUS;
        order_type: string;
        transaction_id: string;
        checksum: string;
    }): Promise<any>;
}
