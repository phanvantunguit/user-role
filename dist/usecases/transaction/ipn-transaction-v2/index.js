"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IPNTransactionV2Handler = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../../config");
const const_1 = require("../../../const");
const app_setting_1 = require("../../../const/app-setting");
const response_1 = require("../../../const/response");
const transaction_1 = require("../../../const/transaction");
const user_asset_log_types_1 = require("../../../domains/user/user-asset-log.types");
const cache_repository_1 = require("../../../repositories/cache.repository");
const db_context_1 = require("../../../repositories/db-context");
const event_store_repository_1 = require("../../../repositories/event-store.repository");
const user_repository_1 = require("../../../repositories/user.repository");
const format_data_util_1 = require("../../../utils/format-data.util");
const handle_error_util_1 = require("../../../utils/handle-error.util");
const hash_util_1 = require("../../../utils/hash.util");
const app_setting_2 = require("../../../const/app-setting");
let IPNTransactionV2Handler = class IPNTransactionV2Handler {
    constructor(userRepository, dBContext, cacheRepository, eventStoreRepository) {
        this.userRepository = userRepository;
        this.dBContext = dBContext;
        this.cacheRepository = cacheRepository;
        this.eventStoreRepository = eventStoreRepository;
    }
    async execute(param) {
        const checksum = param.checksum;
        if (!checksum) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.CHECKSUM_INVALID));
        }
        delete param.checksum;
        const dataString = format_data_util_1.formatRequestDataToString(param);
        const check = hash_util_1.validateChecksum(dataString, checksum, config_1.APP_CONFIG.SECRET_KEY);
        if (!check) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.CHECKSUM_INVALID));
        }
        const userAssetLogs = await this.userRepository.findUserAssetLog({
            order_id: param.order_id,
        });
        if (userAssetLogs.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.NOT_FOUND));
        }
        try {
            const openIpnPayment = await this.eventStoreRepository.save({
                event_id: param.order_id,
                event_name: app_setting_2.EVENT_STORE_NAME.INP_PAYMENT_TBOT,
                state: app_setting_2.EVENT_STORE_STATE.OPEN,
                user_id: userAssetLogs[0].user_id,
                metadata: {
                    order_id: param.order_id,
                    time: Date.now()
                },
            });
            try {
                return await this.dBContext.runInTransaction(async (queryRunner) => {
                    const itemStatus = param.status === transaction_1.TRANSACTION_STATUS.COMPLETE
                        ? transaction_1.ITEM_STATUS.ACTIVE
                        : transaction_1.ITEM_STATUS.FAILED;
                    const userId = userAssetLogs[0].user_id;
                    const userAssetBotIds = [];
                    const userAssetBots = [];
                    const userAssetRoleIds = [];
                    const userAssetRoles = [];
                    const userAssetBotTradingIds = [];
                    const userAssetBotTradings = [];
                    const updateUserAssetLogs = userAssetLogs.map((ual) => {
                        if (ual.category === transaction_1.ORDER_CATEGORY.SBOT) {
                            userAssetBotIds.push(ual.asset_id);
                            userAssetBots.push(ual);
                        }
                        if (ual.category === transaction_1.ORDER_CATEGORY.PKG) {
                            userAssetRoleIds.push(ual.asset_id);
                            userAssetRoles.push(ual);
                        }
                        if (ual.category === transaction_1.ORDER_CATEGORY.TBOT) {
                            userAssetBotTradingIds.push(ual.asset_id);
                            userAssetBotTradings.push(ual);
                            return Object.assign(Object.assign({}, ual), { status: itemStatus === transaction_1.ITEM_STATUS.ACTIVE
                                    ? transaction_1.ITEM_STATUS.NOT_CONNECT
                                    : transaction_1.ITEM_STATUS.FAILED });
                        }
                        return Object.assign(Object.assign({}, ual), { status: itemStatus });
                    });
                    const promises = [
                        this.userRepository.saveUserAssetLog(updateUserAssetLogs, queryRunner),
                    ];
                    if (itemStatus === transaction_1.ITEM_STATUS.ACTIVE) {
                        console.log('buy userAssetBotIds', userAssetBotIds);
                        if (userAssetBotIds.length > 0) {
                            const userBots = await this.userRepository.findUserBotSQL({
                                user_id: userId,
                                bot_ids: userAssetBotIds,
                            });
                            console.log('buy userBots', userBots);
                            const updateUserBots = userAssetBots.map((asset) => {
                                console.log('buy userBots asset', asset);
                                const assetExited = userBots.find((b) => b.bot_id === asset.asset_id);
                                const newAsset = {
                                    expires_at: asset.expires_at,
                                    bot_id: asset.asset_id,
                                    user_id: asset.user_id,
                                    owner_created: asset.owner_created,
                                    status: itemStatus,
                                };
                                if (assetExited) {
                                    if (assetExited.expires_at > Date.now()) {
                                        newAsset.expires_at =
                                            Number(assetExited.expires_at) +
                                                Number(newAsset.expires_at) -
                                                Date.now();
                                    }
                                    newAsset['id'] = assetExited.id;
                                }
                                console.log('buy userBots newAsset', newAsset);
                                return newAsset;
                            });
                            console.log('buy updateUserBots', updateUserBots);
                            promises.push(this.userRepository.saveUserBot(updateUserBots, queryRunner));
                        }
                        if (userAssetRoleIds.length > 0) {
                            const userRoles = await this.userRepository.findUserRoleByUserIdsAndRoleIds([userId], userAssetRoleIds);
                            console.log('buy userRoles', userRoles);
                            const updateUserRoles = userAssetRoles.map((asset) => {
                                console.log('buy userRoles asset', asset);
                                const assetExited = userRoles.find((b) => b.role_id === asset.asset_id);
                                const newAsset = {
                                    role_id: asset.asset_id,
                                    user_id: asset.user_id,
                                    owner_created: asset.owner_created,
                                    quantity: asset.quantity,
                                    expires_at: asset.expires_at,
                                };
                                if (assetExited) {
                                    if (assetExited.expires_at > Date.now()) {
                                        newAsset.expires_at =
                                            Number(assetExited.expires_at) +
                                                Number(newAsset.expires_at) -
                                                Date.now();
                                    }
                                    newAsset['id'] = assetExited.id;
                                }
                                console.log('buy userRoles newAsset', newAsset);
                                return newAsset;
                            });
                            console.log('buy updateUserRoles', updateUserRoles);
                            promises.push(this.userRepository.saveUserRole(updateUserRoles, queryRunner));
                        }
                        if (userAssetBotTradingIds.length > 0) {
                            const userBots = await this.userRepository.findUserBotTradingSQL({
                                user_id: userId,
                                bot_ids: userAssetBotTradingIds,
                            });
                            console.log('buy userBots', userBots);
                            const updateUserBots = userAssetBotTradings.map((asset) => {
                                var _a;
                                console.log('buy userBots asset', asset);
                                const assetExited = userBots.find((b) => b.bot_id === asset.asset_id);
                                const newAsset = {
                                    expires_at: asset.expires_at,
                                    bot_id: asset.asset_id,
                                    user_id: asset.user_id,
                                    owner_created: asset.owner_created,
                                    status: transaction_1.ITEM_STATUS.NOT_CONNECT,
                                    type: transaction_1.TBOT_TYPE.BUY,
                                    balance: (_a = asset.metadata) === null || _a === void 0 ? void 0 : _a.balance
                                };
                                if (assetExited) {
                                    if (assetExited.expires_at > Date.now()) {
                                        newAsset.expires_at =
                                            Number(assetExited.expires_at) +
                                                Number(newAsset.expires_at) -
                                                Date.now();
                                    }
                                    newAsset['id'] = assetExited.id;
                                }
                                console.log('buy userBots newAsset', newAsset);
                                return newAsset;
                            });
                            console.log('buy updateUserBots', updateUserBots);
                            promises.push(this.userRepository.saveUserBotTrading(updateUserBots, queryRunner));
                        }
                    }
                    const result = await Promise.all(promises);
                    await this.eventStoreRepository.save({
                        event_id: param.order_id,
                        event_name: app_setting_2.EVENT_STORE_NAME.INP_PAYMENT_TBOT,
                        state: app_setting_2.EVENT_STORE_STATE.CLOSED,
                        user_id: userAssetLogs[0].user_id,
                        metadata: {
                            order_id: param.order_id,
                            time: Date.now()
                        },
                    });
                    this.cacheRepository.publish(app_setting_1.CHANELS.WS_CHANEL, response_1.WS_EVENT.transaction_update, param, userId);
                    return result;
                });
            }
            catch (error) {
                await this.eventStoreRepository.deleteById(openIpnPayment.id);
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNPROCESSABLE_ENTITY }, const_1.ERROR_CODE.UNPROCESSABLE));
            }
        }
        catch (error) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.TRANSACTION_PROCESSING));
        }
    }
};
IPNTransactionV2Handler = __decorate([
    __param(0, common_1.Inject(user_repository_1.UserRepository)),
    __param(1, common_1.Inject(db_context_1.DBContext)),
    __param(2, common_1.Inject(cache_repository_1.CacheRepository)),
    __param(3, common_1.Inject(event_store_repository_1.EventStoreRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        db_context_1.DBContext,
        cache_repository_1.CacheRepository,
        event_store_repository_1.EventStoreRepository])
], IPNTransactionV2Handler);
exports.IPNTransactionV2Handler = IPNTransactionV2Handler;
//# sourceMappingURL=index.js.map