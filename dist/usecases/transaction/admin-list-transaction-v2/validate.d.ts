import { TRANSACTION_STATUS } from 'src/const/transaction';
import { AdminListTransactionV2Validate } from '../validate';
export declare class AdminListTransactionV2Input {
    page: number;
    size: number;
    keyword: string;
    status: TRANSACTION_STATUS;
    from: number;
    to: number;
    category?: string;
    name?: string;
    merchant_code?: string;
}
export declare class AdminListTransactionV2Output {
    page: number;
    size: number;
    count: number;
    total: number;
    rows: AdminListTransactionV2Validate[];
}
export declare class AdminListTransactionV2PayloadOutput {
    payload: AdminListTransactionV2Output;
}
