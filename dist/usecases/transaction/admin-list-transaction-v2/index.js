"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminListTransactionV2Handler = void 0;
const common_1 = require("@nestjs/common");
const transaction_ps_repository_1 = require("../../../repositories/transaction-ps.repository");
let AdminListTransactionV2Handler = class AdminListTransactionV2Handler {
    constructor(transactionPSRepository) {
        this.transactionPSRepository = transactionPSRepository;
    }
    async execute(params) {
        const dataRes = await this.transactionPSRepository.getPaging(params);
        return dataRes;
    }
};
AdminListTransactionV2Handler = __decorate([
    __param(0, common_1.Inject(transaction_ps_repository_1.TransactionPSRepository)),
    __metadata("design:paramtypes", [transaction_ps_repository_1.TransactionPSRepository])
], AdminListTransactionV2Handler);
exports.AdminListTransactionV2Handler = AdminListTransactionV2Handler;
//# sourceMappingURL=index.js.map