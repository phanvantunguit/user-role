"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminListTransactionV2PayloadOutput = exports.AdminListTransactionV2Output = exports.AdminListTransactionV2Input = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const transaction_1 = require("../../../const/transaction");
const validate_1 = require("../validate");
class AdminListTransactionV2Input {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Page',
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], AdminListTransactionV2Input.prototype, "page", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Size',
        example: 50,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], AdminListTransactionV2Input.prototype, "size", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'The keyword to find user ex: phone number, name, email',
        example: 'john',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTransactionV2Input.prototype, "keyword", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Status of transaction',
        example: 'COMPLETE',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTransactionV2Input.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'From',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], AdminListTransactionV2Input.prototype, "from", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'To',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], AdminListTransactionV2Input.prototype, "to", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Category',
        example: 'BOT',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTransactionV2Input.prototype, "category", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Item name',
        example: 'Bot abc',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTransactionV2Input.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Merchant code',
        example: 'CM',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTransactionV2Input.prototype, "merchant_code", void 0);
exports.AdminListTransactionV2Input = AdminListTransactionV2Input;
class AdminListTransactionV2Output {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Page',
        example: 1,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], AdminListTransactionV2Output.prototype, "page", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Size',
        example: 50,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], AdminListTransactionV2Output.prototype, "size", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], AdminListTransactionV2Output.prototype, "count", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], AdminListTransactionV2Output.prototype, "total", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        isArray: true,
        type: validate_1.AdminListTransactionV2Validate,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsArray(),
    class_transformer_1.Type(() => validate_1.AdminListTransactionV2Validate),
    __metadata("design:type", Array)
], AdminListTransactionV2Output.prototype, "rows", void 0);
exports.AdminListTransactionV2Output = AdminListTransactionV2Output;
class AdminListTransactionV2PayloadOutput {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        type: AdminListTransactionV2Output,
    }),
    class_validator_1.IsObject(),
    __metadata("design:type", AdminListTransactionV2Output)
], AdminListTransactionV2PayloadOutput.prototype, "payload", void 0);
exports.AdminListTransactionV2PayloadOutput = AdminListTransactionV2PayloadOutput;
//# sourceMappingURL=validate.js.map