import { TransactionPSRepository } from 'src/repositories/transaction-ps.repository';
import { AdminListTransactionV2Input, AdminListTransactionV2Output } from './validate';
export declare class AdminListTransactionV2Handler {
    private transactionPSRepository;
    constructor(transactionPSRepository: TransactionPSRepository);
    execute(params: AdminListTransactionV2Input): Promise<AdminListTransactionV2Output>;
}
