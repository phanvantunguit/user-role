"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateTransactionV2Handler = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../../config");
const const_1 = require("../../../const");
const app_setting_1 = require("../../../const/app-setting");
const db_context_1 = require("../../../repositories/db-context");
const user_repository_1 = require("../../../repositories/user.repository");
const cmpayment_1 = require("../../../resources/cmpayment");
const check_Items_1 = require("../../../services/transaction-v2/check-Items");
const save_items_1 = require("../../../services/transaction-v2/save-items");
const handle_error_util_1 = require("../../../utils/handle-error.util");
let CreateTransactionV2Handler = class CreateTransactionV2Handler {
    constructor(checkItemService, saveItemService, dBContext, cmpaymentResource, userRepository) {
        this.checkItemService = checkItemService;
        this.saveItemService = saveItemService;
        this.dBContext = dBContext;
        this.cmpaymentResource = cmpaymentResource;
        this.userRepository = userRepository;
    }
    async execute(param, user_id, email, merchant_code) {
        const user = await this.userRepository.findOneUser({ id: user_id });
        if (!user) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.USER_NOT_FOUND));
        }
        const { items, commission_cash } = await this.checkItemService.execute({
            merchant_code,
            items: param.items,
            amount: param.amount,
        });
        const orderTypes = [];
        for (let item of param.items) {
            if (!orderTypes.includes(item.category)) {
                orderTypes.push(item.category);
            }
        }
        const orderType = orderTypes.join('-');
        return await this.dBContext.runInTransaction(async (queryRunner) => {
            var _a;
            const data = {
                merchant_code,
                order_type: orderType,
                amount: Number(param.amount),
                currency: param.currency,
                payment_method: param.payment_method,
                integrate_service: app_setting_1.SERVICE_NAME,
                user: {
                    id: user_id,
                    email,
                    fullname: `${user.first_name || ''} ${user.last_name || ''}`.trim(),
                },
                items,
                ipn_url: `${config_1.APP_CONFIG.BASE_URL}/api/v2/user/transaction/ipn`,
                commission_cash,
            };
            const dataRes = await this.cmpaymentResource.createTransaction(data);
            if (dataRes.error_code === const_1.ERROR_CODE.SUCCESS.error_code) {
                const paymentResult = dataRes.payload;
                if (paymentResult.order_id) {
                    const userAssetLogs = await this.userRepository.findUserAssetLog({
                        order_id: paymentResult.order_id,
                    });
                    console.log('userAssetLogs', userAssetLogs);
                    if (userAssetLogs.length === 0) {
                        await this.saveItemService.execute({
                            items: items,
                            user_id,
                            owner_created: user_id,
                            order_id: paymentResult.order_id,
                        }, queryRunner);
                    }
                }
                return {
                    transaction_id: paymentResult.id,
                    order_id: paymentResult.order_id,
                    amount: paymentResult.amount,
                    currency: paymentResult.currency,
                    payment_method: paymentResult.payment_method,
                    status: paymentResult.status,
                    payment_id: paymentResult.payment_id,
                    wallet_address: paymentResult.wallet_address,
                    timeout: (_a = paymentResult.timeout) === null || _a === void 0 ? void 0 : _a.toString(),
                    qrcode_url: paymentResult.qrcode_url,
                    status_url: paymentResult.status_url,
                    checkout_url: paymentResult.checkout_url,
                    updated_at: paymentResult.updated_at,
                    created_at: paymentResult.created_at,
                };
            }
            return dataRes;
        });
    }
};
CreateTransactionV2Handler = __decorate([
    __param(0, common_1.Inject(check_Items_1.CheckItemService)),
    __param(1, common_1.Inject(save_items_1.SaveItemService)),
    __param(2, common_1.Inject(db_context_1.DBContext)),
    __param(3, common_1.Inject(cmpayment_1.CmpaymentResource)),
    __param(4, common_1.Inject(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [check_Items_1.CheckItemService,
        save_items_1.SaveItemService,
        db_context_1.DBContext,
        cmpayment_1.CmpaymentResource,
        user_repository_1.UserRepository])
], CreateTransactionV2Handler);
exports.CreateTransactionV2Handler = CreateTransactionV2Handler;
//# sourceMappingURL=index.js.map