import { PACKAGE_TYPE } from 'src/const';
import { ORDER_CATEGORY, PAYMENT_METHOD, TRANSACTION_STATUS } from 'src/const/transaction';
export declare class ItemDataValidate {
    id: string;
    quantity: number;
    type: PACKAGE_TYPE;
    category: ORDER_CATEGORY;
    balance?: number;
}
export declare class CreateTransactionV2Input {
    payment_method: PAYMENT_METHOD;
    items: ItemDataValidate[];
    amount: string;
    currency: string;
}
export declare class CreateTransactionV2Output {
    transaction_id: string;
    order_id: string;
    amount: string;
    currency: string;
    payment_method: PAYMENT_METHOD;
    status: TRANSACTION_STATUS;
    payment_id: string;
    wallet_address: string;
    timeout: string;
    qrcode_url: string;
    status_url: string;
    checkout_url: string;
    created_at: number;
    updated_at: number;
}
export declare class CreateTransactionV2PayloadOutput {
    payload: CreateTransactionV2Output;
}
