import { DBContext } from 'src/repositories/db-context';
import { UserRepository } from 'src/repositories/user.repository';
import { CmpaymentResource } from 'src/resources/cmpayment';
import { CheckItemService } from 'src/services/transaction-v2/check-Items';
import { SaveItemService } from 'src/services/transaction-v2/save-items';
import { CreateTransactionV2Input, CreateTransactionV2Output } from './validate';
export declare class CreateTransactionV2Handler {
    private checkItemService;
    private saveItemService;
    private dBContext;
    private cmpaymentResource;
    private userRepository;
    constructor(checkItemService: CheckItemService, saveItemService: SaveItemService, dBContext: DBContext, cmpaymentResource: CmpaymentResource, userRepository: UserRepository);
    execute(param: CreateTransactionV2Input, user_id: string, email: string, merchant_code: string): Promise<CreateTransactionV2Output>;
}
