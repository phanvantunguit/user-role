import { MerchantRepository } from 'src/repositories/merchant.repository';
import { MerchantValidate } from '../validate';
export declare class AdminListMerchantHandler {
    private merchantRepository;
    constructor(merchantRepository: MerchantRepository);
    execute(): Promise<MerchantValidate[]>;
}
