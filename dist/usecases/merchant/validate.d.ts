import { MERCHANT_STATUS } from 'src/const';
export declare class MerchantValidate {
    name?: string;
    code?: string;
    email?: string;
    status?: MERCHANT_STATUS;
    description?: string;
    domain?: string;
}
