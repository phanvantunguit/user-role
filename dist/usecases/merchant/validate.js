"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantValidate = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const const_1 = require("../../const");
class MerchantValidate {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'name',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], MerchantValidate.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'code',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], MerchantValidate.prototype, "code", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'email',
    }),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], MerchantValidate.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: `${Object.keys(const_1.MERCHANT_STATUS).join(',')}`,
        example: const_1.MERCHANT_STATUS.ACTIVE,
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], MerchantValidate.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'code',
    }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], MerchantValidate.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'domain',
    }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], MerchantValidate.prototype, "domain", void 0);
exports.MerchantValidate = MerchantValidate;
//# sourceMappingURL=validate.js.map