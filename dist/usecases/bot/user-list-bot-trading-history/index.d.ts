import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { UserListTradeHistoryOutput, UserListTradeHistoryInput } from './validate';
export declare class UserListBotTradingHistoryHandler {
    private botTradingHistoryRepository;
    private userRepository;
    constructor(botTradingHistoryRepository: BotTradingHistoryRepository, userRepository: UserRepository);
    execute(params: UserListTradeHistoryInput, user_id?: string): Promise<UserListTradeHistoryOutput>;
}
