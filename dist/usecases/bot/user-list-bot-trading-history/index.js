"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListBotTradingHistoryHandler = void 0;
const common_1 = require("@nestjs/common");
const transaction_1 = require("../../../const/transaction");
const bot_trading_history_repository_1 = require("../../../repositories/bot-trading-history.repository");
const user_repository_1 = require("../../../repositories/user.repository");
const user_bot_trading_entity_1 = require("../../../repositories/user.repository/user-bot-trading.entity");
let UserListBotTradingHistoryHandler = class UserListBotTradingHistoryHandler {
    constructor(botTradingHistoryRepository, userRepository) {
        this.botTradingHistoryRepository = botTradingHistoryRepository;
        this.userRepository = userRepository;
    }
    async execute(params, user_id) {
        let userBot;
        let userId;
        let connected_at;
        if (user_id && params.bot_id) {
            userBot = await this.userRepository.findUserBotTrading({
                user_id,
                bot_id: params.bot_id,
            });
            if (userBot[0]) {
                connected_at = userBot[0].connected_at;
                if (params.strategy_trade !== 'true') {
                    userId = user_id;
                    if (userBot[0].status === transaction_1.ITEM_STATUS.NOT_CONNECT || userBot[0].status === transaction_1.ITEM_STATUS.CONNECTING) {
                        connected_at = Date.now();
                    }
                }
            }
        }
        const trades = await this.botTradingHistoryRepository.getPaging({
            page: params.page,
            size: params.size,
            status: params.status,
            from: params.from,
            to: params.to,
            bot_id: params.bot_id,
            user_id: userId,
            connected_at
        });
        return trades;
    }
};
UserListBotTradingHistoryHandler = __decorate([
    __param(0, common_1.Inject(bot_trading_history_repository_1.BotTradingHistoryRepository)),
    __param(1, common_1.Inject(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [bot_trading_history_repository_1.BotTradingHistoryRepository,
        user_repository_1.UserRepository])
], UserListBotTradingHistoryHandler);
exports.UserListBotTradingHistoryHandler = UserListBotTradingHistoryHandler;
//# sourceMappingURL=index.js.map