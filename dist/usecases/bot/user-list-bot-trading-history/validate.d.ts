import { TRADE_HISTORY_STATUS } from 'src/const/bot';
import { ITEM_STATUS } from 'src/const/transaction';
export declare class UserListTradeHistoryInput {
    page: number;
    size: number;
    bot_id: string;
    status: TRADE_HISTORY_STATUS;
    strategy_trade?: string;
    from: number;
    to: number;
}
export declare class TradeHistoryDataValidate {
    id?: string;
    token_first?: string;
    token_second?: string;
    status?: string;
    side?: string;
    entry_price?: string;
    close_price?: string;
    profit?: string;
    day_started?: number;
    day_completed?: number;
    created_at?: number;
    updated_at?: number;
    user_status?: ITEM_STATUS;
}
export declare class UserListTradeHistoryOutput {
    page: number;
    size: number;
    count: number;
    total: number;
    rows: TradeHistoryDataValidate[];
}
export declare class UserListTradeHistoryPayloadOutput {
    payload: UserListTradeHistoryOutput;
}
