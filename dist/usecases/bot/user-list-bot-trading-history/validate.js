"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListTradeHistoryPayloadOutput = exports.UserListTradeHistoryOutput = exports.TradeHistoryDataValidate = exports.UserListTradeHistoryInput = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const const_1 = require("../../../const");
const bot_1 = require("../../../const/bot");
const transaction_1 = require("../../../const/transaction");
class UserListTradeHistoryInput {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Page',
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], UserListTradeHistoryInput.prototype, "page", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Size',
        example: 50,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], UserListTradeHistoryInput.prototype, "size", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        example: 'efebdafe-c359-4e88-bd17-82b2a5b1091b',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsUUID(),
    __metadata("design:type", String)
], UserListTradeHistoryInput.prototype, "bot_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: `${Object.keys(bot_1.TRADE_HISTORY_STATUS).join(' or ')}`,
        example: bot_1.TRADE_HISTORY_STATUS.OPEN,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserListTradeHistoryInput.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'true or false',
        example: 'true',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsBooleanString(),
    __metadata("design:type", String)
], UserListTradeHistoryInput.prototype, "strategy_trade", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'From',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], UserListTradeHistoryInput.prototype, "from", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'To',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], UserListTradeHistoryInput.prototype, "to", void 0);
exports.UserListTradeHistoryInput = UserListTradeHistoryInput;
class TradeHistoryDataValidate {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TradeHistoryDataValidate.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        description: 'Name token frist',
    }),
    common_1.Optional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TradeHistoryDataValidate.prototype, "token_first", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Name token second',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TradeHistoryDataValidate.prototype, "token_second", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: Object.values(const_1.BOT_STATUS).join(','),
        example: const_1.BOT_STATUS.OPEN,
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TradeHistoryDataValidate.prototype, "status", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Side',
        example: 'LONG',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TradeHistoryDataValidate.prototype, "side", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Price entry',
        example: '0.009',
    }),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], TradeHistoryDataValidate.prototype, "entry_price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Price close',
        example: '0.009',
    }),
    class_validator_1.IsNumberString(),
    __metadata("design:type", String)
], TradeHistoryDataValidate.prototype, "close_price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Profit/loss',
        example: '14',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], TradeHistoryDataValidate.prototype, "profit", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Day start',
        example: '1676000403889',
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], TradeHistoryDataValidate.prototype, "day_started", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Day completed',
        example: '1676000403889',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], TradeHistoryDataValidate.prototype, "day_completed", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], TradeHistoryDataValidate.prototype, "created_at", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], TradeHistoryDataValidate.prototype, "updated_at", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: `${Object.keys(transaction_1.ITEM_STATUS)}`,
        example: transaction_1.ITEM_STATUS.ACTIVE,
    }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], TradeHistoryDataValidate.prototype, "user_status", void 0);
exports.TradeHistoryDataValidate = TradeHistoryDataValidate;
class UserListTradeHistoryOutput {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Page',
        example: 1,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserListTradeHistoryOutput.prototype, "page", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Size',
        example: 50,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserListTradeHistoryOutput.prototype, "size", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserListTradeHistoryOutput.prototype, "count", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserListTradeHistoryOutput.prototype, "total", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        isArray: true,
        type: TradeHistoryDataValidate,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsArray(),
    class_transformer_1.Type(() => TradeHistoryDataValidate),
    __metadata("design:type", Array)
], UserListTradeHistoryOutput.prototype, "rows", void 0);
exports.UserListTradeHistoryOutput = UserListTradeHistoryOutput;
class UserListTradeHistoryPayloadOutput {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        type: UserListTradeHistoryOutput,
    }),
    class_validator_1.IsObject(),
    __metadata("design:type", UserListTradeHistoryOutput)
], UserListTradeHistoryPayloadOutput.prototype, "payload", void 0);
exports.UserListTradeHistoryPayloadOutput = UserListTradeHistoryPayloadOutput;
//# sourceMappingURL=validate.js.map