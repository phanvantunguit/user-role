export declare class BotSignalValidate {
    id?: string;
    name?: string;
    resolution?: string;
    signal_id?: string;
    time?: number;
    type?: string;
    image_url?: string;
    exchange?: string;
    symbol?: string;
    created_at?: number;
}
