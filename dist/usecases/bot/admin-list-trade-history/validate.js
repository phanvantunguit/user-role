"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminListTradeHistoryInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class AdminListTradeHistoryInput {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Page',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], AdminListTradeHistoryInput.prototype, "page", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Size',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], AdminListTradeHistoryInput.prototype, "size", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTradeHistoryInput.prototype, "keyword", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'type',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTradeHistoryInput.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'symbols',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTradeHistoryInput.prototype, "symbols", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'name_bots',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", Object)
], AdminListTradeHistoryInput.prototype, "name_bots", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'resolutions',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTradeHistoryInput.prototype, "resolutions", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'position_side',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTradeHistoryInput.prototype, "position_side", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminListTradeHistoryInput.prototype, "sort_by", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'desc or asc',
        example: 'desc',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsIn(['desc', 'asc']),
    __metadata("design:type", String)
], AdminListTradeHistoryInput.prototype, "order_by", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'From',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], AdminListTradeHistoryInput.prototype, "from", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'To',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], AdminListTradeHistoryInput.prototype, "to", void 0);
exports.AdminListTradeHistoryInput = AdminListTradeHistoryInput;
//# sourceMappingURL=validate.js.map