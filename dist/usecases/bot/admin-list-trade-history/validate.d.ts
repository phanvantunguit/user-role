export declare class AdminListTradeHistoryInput {
    page: number;
    size: number;
    keyword: string;
    type?: string;
    symbols?: string;
    name_bots: any;
    resolutions?: string;
    position_side?: string;
    sort_by: string;
    order_by: 'desc' | 'asc';
    from: number;
    to: number;
}
