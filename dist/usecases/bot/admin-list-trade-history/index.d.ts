import { AdminListTradeHistoryInput } from './validate';
export declare class AdminListTradeHistoryHandler {
    constructor();
    execute(params: AdminListTradeHistoryInput): Promise<any>;
}
