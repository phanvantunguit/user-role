"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminListTradeHistoryHandler = void 0;
class AdminListTradeHistoryHandler {
    constructor() { }
    async execute(params) {
        return {
            rows: [],
            page: 1,
            size: 1,
            count: 1,
            total: 1,
        };
    }
}
exports.AdminListTradeHistoryHandler = AdminListTradeHistoryHandler;
//# sourceMappingURL=index.js.map