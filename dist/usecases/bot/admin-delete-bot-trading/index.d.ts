import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { CacheRepository } from 'src/repositories/cache.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { MetaapiResource } from 'src/resources/forex';
export declare class AdminDeleteBotTradingHandler {
    private userRepository;
    private metaapiResource;
    private botTradingRepository;
    private cacheRepository;
    constructor(userRepository: UserRepository, metaapiResource: MetaapiResource, botTradingRepository: BotTradingRepository, cacheRepository: CacheRepository);
    execute(id: string): Promise<boolean>;
}
