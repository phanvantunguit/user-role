"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminDeleteBotTradingHandler = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../../const");
const app_setting_1 = require("../../../const/app-setting");
const bot_1 = require("../../../const/bot");
const transaction_1 = require("../../../const/transaction");
const bot_trading_repository_1 = require("../../../repositories/bot-trading.repository");
const cache_repository_1 = require("../../../repositories/cache.repository");
const user_repository_1 = require("../../../repositories/user.repository");
const forex_1 = require("../../../resources/forex");
const handle_error_util_1 = require("../../../utils/handle-error.util");
let AdminDeleteBotTradingHandler = class AdminDeleteBotTradingHandler {
    constructor(userRepository, metaapiResource, botTradingRepository, cacheRepository) {
        this.userRepository = userRepository;
        this.metaapiResource = metaapiResource;
        this.botTradingRepository = botTradingRepository;
        this.cacheRepository = cacheRepository;
    }
    async execute(id) {
        const userBot = await this.userRepository.findUserBotTrading({ id });
        if (userBot.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
        const bot = await this.botTradingRepository.findById(userBot[0].bot_id);
        if (!bot) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
        if (userBot[0].subscriber_id) {
            try {
                await this.metaapiResource.removeSubscriber({
                    subscriber_id: userBot[0].subscriber_id,
                });
            }
            catch (error) {
                console.log('removeSubscriber error', error);
            }
            await this.metaapiResource.removeAccount({
                subscriber_id: userBot[0].subscriber_id,
            });
        }
        if (userBot[0].profile_id) {
            await this.metaapiResource.removeProvisioningProfile(userBot[0].profile_id);
        }
        await this.userRepository.deleteUserBotTrading({ id });
        this.cacheRepository.publisher.publish(app_setting_1.CHANELS.TBOT_CHANEL_TS, JSON.stringify({
            event: bot_1.TBOT_CHANEL_TS_EVENT.account,
            data: {
                bot_id: bot.id,
                user_id: userBot[0].user_id,
                user_bot_id: userBot[0].id,
                status: transaction_1.ITEM_STATUS.DELETED,
            },
        }));
        return true;
    }
};
AdminDeleteBotTradingHandler = __decorate([
    __param(0, common_1.Inject(user_repository_1.UserRepository)),
    __param(1, common_1.Inject(forex_1.MetaapiResource)),
    __param(2, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(3, common_1.Inject(cache_repository_1.CacheRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        forex_1.MetaapiResource,
        bot_trading_repository_1.BotTradingRepository,
        cache_repository_1.CacheRepository])
], AdminDeleteBotTradingHandler);
exports.AdminDeleteBotTradingHandler = AdminDeleteBotTradingHandler;
//# sourceMappingURL=index.js.map