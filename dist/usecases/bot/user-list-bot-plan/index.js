"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListBotPlanHandler = void 0;
const common_1 = require("@nestjs/common");
const transaction_1 = require("../../../const/transaction");
const bot_repository_1 = require("../../../repositories/bot.repository");
const user_repository_1 = require("../../../repositories/user.repository");
let UserListBotPlanHandler = class UserListBotPlanHandler {
    constructor(botRepository, userRepository) {
        this.botRepository = botRepository;
        this.userRepository = userRepository;
    }
    async execute(user_id) {
        let userBots = await this.userRepository.findUserBotSQL({
            user_id,
        });
        const userBotIdDuplicate = [];
        userBots = userBots.filter((ub) => {
            if (!userBotIdDuplicate.includes(ub.bot_id)) {
                userBotIdDuplicate.push(ub.bot_id);
                return true;
            }
            return false;
        });
        const bots = await this.botRepository.findByIds(userBotIdDuplicate);
        const mapBots = bots.map((b) => {
            const userBotExit = userBots.find((ub) => ub.bot_id === b.id);
            if (userBotExit) {
                return Object.assign(Object.assign({}, b), { expires_at: userBotExit.expires_at, user_status: userBotExit.status });
            }
            else {
                return b;
            }
        });
        return mapBots;
    }
};
UserListBotPlanHandler = __decorate([
    __param(0, common_1.Inject(bot_repository_1.BotRepository)),
    __param(1, common_1.Inject(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [bot_repository_1.BotRepository,
        user_repository_1.UserRepository])
], UserListBotPlanHandler);
exports.UserListBotPlanHandler = UserListBotPlanHandler;
//# sourceMappingURL=index.js.map