"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadBrokerProfileHandler = void 0;
const common_1 = require("@nestjs/common");
const bot_1 = require("../../../const/bot");
const forex_1 = require("../../../resources/forex");
const setting_repository_1 = require("../../../repositories/setting.repository");
const app_setting_1 = require("../../../const/app-setting");
const app_setting_types_1 = require("../../../domains/setting/app-setting.types");
let UploadBrokerProfileHandler = class UploadBrokerProfileHandler {
    constructor(metaapiResource, settingRepository) {
        this.metaapiResource = metaapiResource;
        this.settingRepository = settingRepository;
    }
    async execute(broker_code, file, platform, merchant_code, account_id, user_id) {
        var _a;
        const brokerSettings = await this.settingRepository.findAppSetting({
            name: app_setting_1.APP_SETTING.BROKER_SETTING,
        });
        let brokerSetting;
        try {
            if ((_a = brokerSettings[0]) === null || _a === void 0 ? void 0 : _a.value) {
                const dataPare = JSON.parse(brokerSettings[0].value) || {};
                brokerSetting = dataPare[broker_code];
            }
        }
        catch (error) {
            console.log('brokerSetting error', error);
        }
        const { fieldname, originalname, encoding, mimetype, buffer, size } = file;
        const version = platform === bot_1.TBOT_PLATFORM.mt4 ? 4 : 5;
        const profile = await this.metaapiResource.createProvisioningProfile({
            name: originalname,
            version,
            brokerTimezone: brokerSetting === null || brokerSetting === void 0 ? void 0 : brokerSetting.broker_timezone,
            brokerDSTSwitchTimezone: brokerSetting === null || brokerSetting === void 0 ? void 0 : brokerSetting.broker_dst_switch_timezone,
            file: buffer,
            merchant_code,
            account_id,
            user_id,
        });
        return { profile_id: profile.id };
    }
};
UploadBrokerProfileHandler = __decorate([
    __param(0, common_1.Inject(forex_1.MetaapiResource)),
    __param(1, common_1.Inject(setting_repository_1.SettingRepository)),
    __metadata("design:paramtypes", [forex_1.MetaapiResource,
        setting_repository_1.SettingRepository])
], UploadBrokerProfileHandler);
exports.UploadBrokerProfileHandler = UploadBrokerProfileHandler;
//# sourceMappingURL=index.js.map