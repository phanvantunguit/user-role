import { TBOT_PLATFORM } from 'src/const/bot';
import { MetaapiResource } from 'src/resources/forex';
import { UploadBrokerProfileOutput } from './validate';
import { SettingRepository } from 'src/repositories/setting.repository';
export declare class UploadBrokerProfileHandler {
    private metaapiResource;
    private settingRepository;
    constructor(metaapiResource: MetaapiResource, settingRepository: SettingRepository);
    execute(broker_code: string, file: any, platform: TBOT_PLATFORM, merchant_code: any, account_id: any, user_id: any): Promise<UploadBrokerProfileOutput>;
}
