import { BotRepository } from 'src/repositories/bot.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { BotDataValidate } from '../user-list-bot/validate';
export declare class UserListBotPaidHandler {
    private botRepository;
    private userRepository;
    constructor(botRepository: BotRepository, userRepository: UserRepository);
    execute(user_id: string): Promise<BotDataValidate[]>;
}
