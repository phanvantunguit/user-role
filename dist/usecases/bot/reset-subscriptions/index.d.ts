import { AccountBalanceRepository } from 'src/repositories/account-balance.repository';
import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { MetaapiResource } from 'src/resources/forex';
export declare class AdminResetSubscriptionHandler {
    private userRepository;
    private metaapiResource;
    private botTradingRepository;
    private accountBalanceRepository;
    constructor(userRepository: UserRepository, metaapiResource: MetaapiResource, botTradingRepository: BotTradingRepository, accountBalanceRepository: AccountBalanceRepository);
    execute(): Promise<any>;
    asyncExecute(userBots: any, bots: any): Promise<void>;
}
