"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminResetSubscriptionHandler = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../../const");
const transaction_1 = require("../../../const/transaction");
const account_balance_repository_1 = require("../../../repositories/account-balance.repository");
const bot_trading_repository_1 = require("../../../repositories/bot-trading.repository");
const user_repository_1 = require("../../../repositories/user.repository");
const forex_1 = require("../../../resources/forex");
const handle_error_util_1 = require("../../../utils/handle-error.util");
let AdminResetSubscriptionHandler = class AdminResetSubscriptionHandler {
    constructor(userRepository, metaapiResource, botTradingRepository, accountBalanceRepository) {
        this.userRepository = userRepository;
        this.metaapiResource = metaapiResource;
        this.botTradingRepository = botTradingRepository;
        this.accountBalanceRepository = accountBalanceRepository;
    }
    async execute() {
        const [userBots, bots] = await Promise.all([
            this.userRepository.findUserBotTrading({
                status: transaction_1.ITEM_STATUS.ACTIVE,
            }),
            this.botTradingRepository.find({}),
        ]);
        this.asyncExecute(userBots, bots);
        return userBots.length;
    }
    async asyncExecute(userBots, bots) {
        for (let ub of userBots) {
            try {
                const bot = bots.find((b) => b.id === ub.bot_id);
                let startTime = ub.connected_at
                    ? new Date(Number(ub.connected_at))
                    : new Date(Number(ub.updated_at));
                if (startTime.toString() === 'Invalid Date') {
                    startTime = new Date();
                }
                const account = await this.metaapiResource.getAccount({
                    account_id: ub.subscriber_id,
                });
                if (account &&
                    account.state === 'DEPLOYED' &&
                    account.connectionStatus === 'CONNECTED') {
                    const connection = await account.getRPCConnection();
                    await connection.connect();
                    await connection.waitSynchronized();
                    const accountInfo = await connection.getAccountInformation();
                    const accountBalance = await this.accountBalanceRepository.findOne({
                        user_id: ub.user_id,
                        bot_id: ub.bot_id,
                        change_id: 'init',
                    });
                    const initBalance = accountBalance
                        ? accountBalance.balance
                        : accountInfo.balance;
                    if (Number(accountInfo.balance) < Number(bot.balance)) {
                        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ACCOUNT_NOT_ENOUGH_BALANCE));
                    }
                    const multiplier = Math.floor(Number(initBalance) / Number(bot.balance));
                    console.log('startTime', startTime);
                    const maxAbsoluteRisk = (Number(bot.max_drawdown) - Number(bot.max_drawdown_change_percent)) *
                        multiplier *
                        Number(bot.balance);
                    const user = await this.userRepository.findOneUser({ id: ub.user_id });
                    if (user) {
                        await this.metaapiResource.updateSubscriber({
                            subscriber_id: ub.subscriber_id,
                            name: `${user.merchant_code}_${ub.broker_account}_${user.id}`,
                            strategy_id: bot.code,
                            multiplier,
                            max_absolute_risk: Math.ceil(maxAbsoluteRisk),
                            start_time: startTime,
                            broker: ub.broker
                        });
                    }
                }
            }
            catch (error) {
            }
        }
    }
};
AdminResetSubscriptionHandler = __decorate([
    __param(0, common_1.Inject(user_repository_1.UserRepository)),
    __param(1, common_1.Inject(forex_1.MetaapiResource)),
    __param(2, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(3, common_1.Inject(account_balance_repository_1.AccountBalanceRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        forex_1.MetaapiResource,
        bot_trading_repository_1.BotTradingRepository,
        account_balance_repository_1.AccountBalanceRepository])
], AdminResetSubscriptionHandler);
exports.AdminResetSubscriptionHandler = AdminResetSubscriptionHandler;
//# sourceMappingURL=index.js.map