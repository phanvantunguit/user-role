"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeactivateUserBotTradingHandler = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../../config");
const app_setting_1 = require("../../../const/app-setting");
const transaction_1 = require("../../../const/transaction");
const event_store_repository_1 = require("../../../repositories/event-store.repository");
const setting_repository_1 = require("../../../repositories/setting.repository");
const user_repository_1 = require("../../../repositories/user.repository");
const forex_1 = require("../../../resources/forex");
const bot_trading_1 = require("../../../services/bot-trading");
let DeactivateUserBotTradingHandler = class DeactivateUserBotTradingHandler {
    constructor(eventStoreRepository, botTradingService, metaapiResource, userRepository, settingRepository) {
        this.eventStoreRepository = eventStoreRepository;
        this.botTradingService = botTradingService;
        this.metaapiResource = metaapiResource;
        this.userRepository = userRepository;
        this.settingRepository = settingRepository;
    }
    async execute(user_bot_id, user_id, bot_id, trade) {
        var _a, _b;
        const sendEmailProcessing = await this.eventStoreRepository.save({
            event_id: `${trade.id}`,
            user_id,
            event_name: app_setting_1.EVENT_STORE_NAME.TBOT_INACTIVE_BY_SYSTEM,
            state: app_setting_1.EVENT_STORE_STATE.PROCESSING,
        });
        try {
            const events = await this.eventStoreRepository.find({
                user_id: user_id,
                event_name: app_setting_1.APP_SETTING.TBOT_INACTIVE_BY_SYSTEM,
            });
            const deleteEventIds = [];
            let countInactive = 0;
            events.forEach((e) => {
                var _a;
                if (((_a = e.metadata) === null || _a === void 0 ? void 0 : _a.bot_id) === bot_id) {
                    deleteEventIds.push(e.id);
                    if (e.state === app_setting_1.EVENT_STORE_STATE.OPEN) {
                        countInactive += 1;
                    }
                }
            });
            if (countInactive > config_1.APP_CONFIG.NUMBER_OF_TIMES_ALLOW_USER_ACTIVATE_TBOT) {
                const userBot = await this.userRepository.findUserBotTrading({
                    id: user_bot_id,
                });
                if ((_a = userBot[0]) === null || _a === void 0 ? void 0 : _a.subscriber_id) {
                    try {
                        await this.metaapiResource.removeSubscriber({
                            subscriber_id: userBot[0].subscriber_id,
                        });
                    }
                    catch (error) {
                        console.log('removeSubscriber error', error);
                    }
                    await this.metaapiResource.removeAccount({
                        subscriber_id: userBot[0].subscriber_id,
                    });
                    await this.userRepository.deleteUserBotTrading({
                        user_id,
                        bot_id,
                    });
                    await this.eventStoreRepository.deleteByIds(deleteEventIds);
                    const numberOfBot = await this.settingRepository.findAppSetting({
                        user_id,
                        name: app_setting_1.APP_SETTING.NUMBER_OF_TBOT,
                    });
                    await this.eventStoreRepository.delete({
                        event_id: `${userBot[0].user_id}_${userBot[0].bot_id}`,
                        state: app_setting_1.EVENT_STORE_STATE.OPEN,
                        event_name: app_setting_1.EVENT_STORE_NAME.TBOT_STOP_INACTIVE_BY_SYSTEM,
                    });
                    if ((_b = numberOfBot[0]) === null || _b === void 0 ? void 0 : _b.value) {
                        const newNumberOfBot = Number(numberOfBot[0].value) > 0
                            ? Number(numberOfBot[0].value) - 1
                            : 0;
                        await this.settingRepository.saveAppSetting({
                            id: numberOfBot[0].id,
                            value: newNumberOfBot.toString(),
                        });
                    }
                }
            }
            else {
                try {
                    await this.eventStoreRepository.save({
                        event_id: `${trade.id}`,
                        user_id,
                        event_name: app_setting_1.EVENT_STORE_NAME.TBOT_INACTIVE_BY_SYSTEM,
                        state: app_setting_1.EVENT_STORE_STATE.CLOSED,
                    });
                }
                catch (error) {
                    console.log('error', error);
                }
            }
            await this.botTradingService.emailBotStatus({
                user_id,
                bot_id,
                status: transaction_1.ITEM_STATUS.INACTIVE_BY_SYSTEM,
                trade,
                count_invalid: countInactive,
            });
        }
        catch (error) {
            console.log('error', error);
            await this.eventStoreRepository.deleteById(sendEmailProcessing.id);
        }
    }
};
DeactivateUserBotTradingHandler = __decorate([
    __param(0, common_1.Inject(event_store_repository_1.EventStoreRepository)),
    __param(1, common_1.Inject(bot_trading_1.BotTradingService)),
    __param(2, common_1.Inject(forex_1.MetaapiResource)),
    __param(3, common_1.Inject(user_repository_1.UserRepository)),
    __param(4, common_1.Inject(setting_repository_1.SettingRepository)),
    __metadata("design:paramtypes", [event_store_repository_1.EventStoreRepository,
        bot_trading_1.BotTradingService,
        forex_1.MetaapiResource,
        user_repository_1.UserRepository,
        setting_repository_1.SettingRepository])
], DeactivateUserBotTradingHandler);
exports.DeactivateUserBotTradingHandler = DeactivateUserBotTradingHandler;
//# sourceMappingURL=index.js.map