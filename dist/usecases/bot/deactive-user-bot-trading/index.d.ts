import { EventStoreRepository } from 'src/repositories/event-store.repository';
import { SettingRepository } from 'src/repositories/setting.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { MetaapiResource } from 'src/resources/forex';
import { BotTradingService } from 'src/services/bot-trading';
export declare class DeactivateUserBotTradingHandler {
    private eventStoreRepository;
    private botTradingService;
    private metaapiResource;
    private userRepository;
    private settingRepository;
    constructor(eventStoreRepository: EventStoreRepository, botTradingService: BotTradingService, metaapiResource: MetaapiResource, userRepository: UserRepository, settingRepository: SettingRepository);
    execute(user_bot_id: string, user_id: string, bot_id: string, trade: {
        id: string;
        symbol: string;
        time: number;
        broker_account: string;
    }): Promise<any>;
}
