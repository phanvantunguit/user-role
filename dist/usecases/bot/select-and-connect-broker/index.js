"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SelectAndConnectBrokerServerHandler = void 0;
const common_1 = require("@nestjs/common");
const event_emitter_1 = require("@nestjs/event-emitter");
const config_1 = require("../../../config");
const const_1 = require("../../../const");
const app_setting_1 = require("../../../const/app-setting");
const bot_1 = require("../../../const/bot");
const transaction_1 = require("../../../const/transaction");
const app_setting_types_1 = require("../../../domains/setting/app-setting.types");
const bot_trading_repository_1 = require("../../../repositories/bot-trading.repository");
const event_store_repository_1 = require("../../../repositories/event-store.repository");
const merchant_repository_1 = require("../../../repositories/merchant.repository");
const setting_repository_1 = require("../../../repositories/setting.repository");
const user_repository_1 = require("../../../repositories/user.repository");
const broker_1 = require("../../../resources/broker");
const forex_1 = require("../../../resources/forex");
const app_setting_2 = require("../../../services/app-setting");
const handle_error_util_1 = require("../../../utils/handle-error.util");
let SelectAndConnectBrokerServerHandler = class SelectAndConnectBrokerServerHandler {
    constructor(userRepository, metaapiResource, botTradingRepository, eventEmitter, appSettingService, eventStoreRepository, merchantRepository, brokerResource, settingRepository) {
        this.userRepository = userRepository;
        this.metaapiResource = metaapiResource;
        this.botTradingRepository = botTradingRepository;
        this.eventEmitter = eventEmitter;
        this.appSettingService = appSettingService;
        this.eventStoreRepository = eventStoreRepository;
        this.merchantRepository = merchantRepository;
        this.brokerResource = brokerResource;
        this.settingRepository = settingRepository;
    }
    async execute(params, userId, merchantCode) {
        var _a, _b;
        const { bot_id, account_id, password, broker_code, broker_server, platform, profile_id, } = params;
        try {
            const [asNumberOfBot, numberOfBotConnected, brokerSettings] = await Promise.all([
                this.appSettingService.getAppSetting({
                    user_id: userId,
                    name: app_setting_1.APP_SETTING.NUMBER_OF_TBOT,
                }),
                this.userRepository.findUserBotTradingSQL({
                    user_id: userId,
                    type: transaction_1.TBOT_TYPE.SELECT,
                }),
                this.settingRepository.findAppSetting({
                    name: app_setting_1.APP_SETTING.BROKER_SETTING,
                }),
            ]);
            let brokerSetting;
            try {
                if ((_a = brokerSettings[0]) === null || _a === void 0 ? void 0 : _a.value) {
                    const dataPare = JSON.parse(brokerSettings[0].value) || {};
                    brokerSetting = dataPare[broker_code];
                }
            }
            catch (error) {
                console.log('brokerSetting error', error);
            }
            if ((brokerSetting === null || brokerSetting === void 0 ? void 0 : brokerSetting.required_profile) && !profile_id) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.PROFILE_REQUIRED));
            }
            const numberOfBotOfUser = Number(asNumberOfBot === null || asNumberOfBot === void 0 ? void 0 : asNumberOfBot.value) || 0;
            const numberOfBotUsed = (Number(numberOfBotConnected.length) || 0) + 1;
            if (numberOfBotOfUser < numberOfBotUsed) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.RESOURCES_NOT_EXISTED));
            }
            const userBotByAccountId = await this.userRepository.findUserBotTrading({
                broker_account: account_id,
            });
            if (userBotByAccountId.length > 0 &&
                (userBotByAccountId[0].user_id !== userId ||
                    userBotByAccountId[0].bot_id !== bot_id)) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ACCOUNT_EXISTED));
            }
            const events = await this.eventStoreRepository.find({
                user_id: userId,
                event_name: app_setting_1.EVENT_STORE_NAME.TBOT_INACTIVE_BY_SYSTEM,
                state: app_setting_1.EVENT_STORE_STATE.OPEN,
            });
            const numberOfTimeInactive = events.filter((e) => { var _a; return ((_a = e.metadata) === null || _a === void 0 ? void 0 : _a.bot_id) === params.bot_id; }).length;
            if (numberOfTimeInactive >
                config_1.APP_CONFIG.NUMBER_OF_TIMES_ALLOW_USER_ACTIVATE_TBOT) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.TBOT_BLOCKED));
            }
            const [checkUserBot, bot] = await Promise.all([
                this.userRepository.findUserBotTrading({
                    bot_id,
                    user_id: userId,
                }),
                this.botTradingRepository.findById(bot_id),
            ]);
            const merchant = await this.merchantRepository.findOne({
                code: merchantCode,
            });
            if (merchant && (brokerSetting === null || brokerSetting === void 0 ? void 0 : brokerSetting.check_referral_broker)) {
                let broker = {};
                if ((_b = merchant.config) === null || _b === void 0 ? void 0 : _b.brokers) {
                    broker = merchant.config.brokers.find((br) => br.code === broker_code);
                }
                const checkReferral = await this.brokerResource.checkAccountReferral(Object.assign({ account: account_id, broker_code,
                    platform }, broker === null || broker === void 0 ? void 0 : broker.referral_setting));
                if (!checkReferral) {
                    handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ACCOUNT_NOT_REFERRED));
                }
            }
            let subscriber = {
                id: userBotByAccountId[0] ? userBotByAccountId[0].subscriber_id : null,
            };
            let account;
            if (subscriber.id) {
                account = await this.metaapiResource.getAccount({
                    account_id: subscriber.id,
                });
                if (account) {
                    try {
                        await account.update({
                            name: `${merchantCode}_${account_id}_${userId}`,
                            password: password,
                            server: broker_server,
                        });
                    }
                    catch (error) {
                        console.log('error', error);
                        if (error.status === common_1.HttpStatus.BAD_REQUEST) {
                            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ACCOUNT_INFO_INCORRECT));
                        }
                        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.CONNECT_ACCOUNT_FAILED));
                    }
                }
            }
            if (!account) {
                try {
                    subscriber = await this.metaapiResource.createSubscriber({
                        login: account_id,
                        password: password,
                        name: `${merchantCode}_${account_id}_${userId}`,
                        server: broker_server,
                        platform,
                        profile_id,
                    });
                }
                catch (error) {
                    console.log('error', error);
                    if (error.status === common_1.HttpStatus.BAD_REQUEST) {
                        handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ACCOUNT_INFO_INCORRECT));
                    }
                    handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.CONNECT_ACCOUNT_FAILED));
                }
            }
            if (subscriber && subscriber.id) {
                const expiredTime = new Date();
                const quantity = 12;
                expiredTime.setMonth(expiredTime.getMonth() + quantity);
                const expiresAt = expiredTime.valueOf();
                const updateUserBot = {
                    broker_server: broker_server,
                    broker: broker_code,
                    broker_account: account_id,
                    expires_at: expiresAt,
                    bot_id,
                    user_id: userId,
                    subscriber_id: subscriber.id,
                    status: transaction_1.ITEM_STATUS.CONNECTING,
                    owner_created: userId,
                    connected_at: Date.now(),
                    type: transaction_1.TBOT_TYPE.SELECT,
                };
                if (profile_id) {
                    updateUserBot['profile_id'] = profile_id;
                }
                if (checkUserBot[0]) {
                    updateUserBot['id'] = checkUserBot[0].id;
                }
                const updateBot = await this.userRepository.saveUserBotTrading([
                    updateUserBot,
                ]);
                const dataLog = {
                    asset_id: bot.id,
                    user_id: userId,
                    category: transaction_1.ORDER_CATEGORY.TBOT,
                    status: transaction_1.ITEM_STATUS.NOT_CONNECT,
                    owner_created: userId,
                    name: bot.name,
                    quantity,
                    package_type: const_1.PACKAGE_TYPE.MONTH,
                    type: transaction_1.TBOT_TYPE.SELECT,
                };
                await this.userRepository.saveUserAssetLog([dataLog]);
                delete updateBot[0].expires_at;
                if (bot.code) {
                    setTimeout(async () => {
                        this.eventEmitter.emit(bot_1.BOT_TRADING_EVENT.CONNECTING, {
                            user_bot_id: updateBot[0].id,
                            user_id: userId,
                            bot_id,
                            bot_code: bot.code,
                        });
                    }, 60 * 1000);
                }
                return updateBot[0];
            }
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.CONNECT_ACCOUNT_FAILED));
        }
        catch (error) {
            if (profile_id) {
                await this.metaapiResource.removeProvisioningProfile(profile_id);
            }
            throw error;
        }
    }
};
SelectAndConnectBrokerServerHandler = __decorate([
    __param(0, common_1.Inject(user_repository_1.UserRepository)),
    __param(1, common_1.Inject(forex_1.MetaapiResource)),
    __param(2, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(3, common_1.Inject(event_emitter_1.EventEmitter2)),
    __param(4, common_1.Inject(app_setting_2.AppSettingService)),
    __param(5, common_1.Inject(event_store_repository_1.EventStoreRepository)),
    __param(6, common_1.Inject(merchant_repository_1.MerchantRepository)),
    __param(7, common_1.Inject(broker_1.BrokerResource)),
    __param(8, common_1.Inject(setting_repository_1.SettingRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        forex_1.MetaapiResource,
        bot_trading_repository_1.BotTradingRepository,
        event_emitter_1.EventEmitter2,
        app_setting_2.AppSettingService,
        event_store_repository_1.EventStoreRepository,
        merchant_repository_1.MerchantRepository,
        broker_1.BrokerResource,
        setting_repository_1.SettingRepository])
], SelectAndConnectBrokerServerHandler);
exports.SelectAndConnectBrokerServerHandler = SelectAndConnectBrokerServerHandler;
//# sourceMappingURL=index.js.map