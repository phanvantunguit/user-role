import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { BotTradingDataValidate } from './validate';
import { MerchantCommissionRepository } from 'src/repositories/merchant-commission.repository';
import { EventStoreRepository } from 'src/repositories/event-store.repository';
export declare class UserListBotTradingHandler {
    private botTradingRepository;
    private userRepository;
    private merchantCommissionRepository;
    private eventStoreRepository;
    constructor(botTradingRepository: BotTradingRepository, userRepository: UserRepository, merchantCommissionRepository: MerchantCommissionRepository, eventStoreRepository: EventStoreRepository);
    execute(user_id?: string, merchant_id?: string): Promise<BotTradingDataValidate[]>;
}
