"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListBotTradingHandler = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../../const");
const transaction_1 = require("../../../const/transaction");
const bot_trading_repository_1 = require("../../../repositories/bot-trading.repository");
const user_repository_1 = require("../../../repositories/user.repository");
const merchant_commission_repository_1 = require("../../../repositories/merchant-commission.repository");
const event_store_repository_1 = require("../../../repositories/event-store.repository");
const app_setting_1 = require("../../../const/app-setting");
let UserListBotTradingHandler = class UserListBotTradingHandler {
    constructor(botTradingRepository, userRepository, merchantCommissionRepository, eventStoreRepository) {
        this.botTradingRepository = botTradingRepository;
        this.userRepository = userRepository;
        this.merchantCommissionRepository = merchantCommissionRepository;
        this.eventStoreRepository = eventStoreRepository;
    }
    async execute(user_id, merchant_id) {
        let ids;
        if (merchant_id) {
            const merchantBot = await this.merchantCommissionRepository.find({
                merchant_id,
                status: const_1.MERCHANT_COMMISSION_STATUS.ACTIVE,
                category: transaction_1.ORDER_CATEGORY.TBOT,
            });
            ids = merchantBot.map((e) => e.asset_id);
        }
        if (ids && ids.length === 0) {
            return [];
        }
        const bots = await this.botTradingRepository.listConditionMultiple({
            status: [const_1.BOT_STATUS.OPEN, const_1.BOT_STATUS.COMINGSOON],
            id: ids,
        });
        if (!user_id) {
            return bots;
        }
        let userBots = await this.userRepository.findUserBotTradingSQL({
            user_id,
        });
        const userBotIdDuplicate = [];
        userBots = userBots.filter((ub) => {
            if (!userBotIdDuplicate.includes(ub.bot_id)) {
                userBotIdDuplicate.push(ub.bot_id);
                return true;
            }
            return false;
        });
        const events = await this.eventStoreRepository.find({
            user_id: user_id,
            event_name: app_setting_1.APP_SETTING.TBOT_INACTIVE_BY_SYSTEM,
            state: app_setting_1.EVENT_STORE_STATE.OPEN,
        });
        const eventByBot = {};
        events.forEach((e) => {
            var _a;
            if ((_a = e.metadata) === null || _a === void 0 ? void 0 : _a.bot_id) {
                if (!eventByBot[e.metadata.bot_id]) {
                    eventByBot[e.metadata.bot_id] = {
                        count: 1,
                        created_at: e.created_at,
                    };
                }
                else {
                    eventByBot[e.metadata.bot_id].count += 1;
                    if (eventByBot[e.metadata.bot_id].created_at < e.created_at) {
                        eventByBot[e.metadata.bot_id].created_at = e.created_at;
                    }
                }
            }
        });
        const mapBots = bots.map((b) => {
            var _a, _b;
            const userBotExit = userBots.find((ub) => ub.bot_id === b.id);
            if (userBotExit) {
                return Object.assign(Object.assign({}, b), { expires_at: userBotExit.expires_at, count_inactive_by_system: ((_a = eventByBot[b.id]) === null || _a === void 0 ? void 0 : _a.count) || 0, inactive_by_system_at: (_b = eventByBot[b.id]) === null || _b === void 0 ? void 0 : _b.created_at });
            }
            else {
                return b;
            }
        });
        return mapBots;
    }
};
UserListBotTradingHandler = __decorate([
    __param(0, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(1, common_1.Inject(user_repository_1.UserRepository)),
    __param(2, common_1.Inject(merchant_commission_repository_1.MerchantCommissionRepository)),
    __param(3, common_1.Inject(event_store_repository_1.EventStoreRepository)),
    __metadata("design:paramtypes", [bot_trading_repository_1.BotTradingRepository,
        user_repository_1.UserRepository,
        merchant_commission_repository_1.MerchantCommissionRepository,
        event_store_repository_1.EventStoreRepository])
], UserListBotTradingHandler);
exports.UserListBotTradingHandler = UserListBotTradingHandler;
//# sourceMappingURL=index.js.map