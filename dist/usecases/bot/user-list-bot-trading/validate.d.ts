import { BOT_STATUS } from 'src/const';
import { ITEM_STATUS } from 'src/const/transaction';
export declare class BotTradingDataValidate {
    id?: string;
    name?: string;
    type?: string;
    description?: string;
    status?: BOT_STATUS;
    price?: string;
    pnl?: string;
    max_drawdown?: string;
    max_drawdown_change_percent?: string;
    currency?: string;
    work_based_on?: string[];
    image_url?: string;
    created_at?: number;
    updated_at?: number;
    order?: number;
    expires_at?: number;
    user_status?: ITEM_STATUS;
    back_test?: string;
    bought?: number;
}
export declare class UserListBotTradingOutput {
    payload: BotTradingDataValidate[];
}
