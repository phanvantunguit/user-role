"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserAddFavoriteSignalHandler = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../../const");
const bot_signal_repository_1 = require("../../../repositories/bot-signal.repository");
const handle_error_util_1 = require("../../../utils/handle-error.util");
let UserAddFavoriteSignalHandler = class UserAddFavoriteSignalHandler {
    constructor(botSignalRepository) {
        this.botSignalRepository = botSignalRepository;
    }
    async execute(user_id, bot_signal_id) {
        const [signal, favoriteSignal] = await Promise.all([
            this.botSignalRepository.findById(bot_signal_id),
            this.botSignalRepository.findFavoriteSignal({
                user_id,
                bot_signal_id,
            }),
        ]);
        if (!signal) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.SIGNAL_NOT_FOUND));
        }
        if (favoriteSignal) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.FAVORITE_SIGNAL_EXISTED));
        }
        return this.botSignalRepository.addFavoriteSignal({
            user_id,
            bot_signal_id,
        });
    }
};
UserAddFavoriteSignalHandler = __decorate([
    __param(0, common_1.Inject(bot_signal_repository_1.BotSignalRepository)),
    __metadata("design:paramtypes", [bot_signal_repository_1.BotSignalRepository])
], UserAddFavoriteSignalHandler);
exports.UserAddFavoriteSignalHandler = UserAddFavoriteSignalHandler;
//# sourceMappingURL=index.js.map