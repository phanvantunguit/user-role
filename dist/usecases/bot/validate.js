"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotSignalValidate = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class BotSignalValidate {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], BotSignalValidate.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'BotGifDo',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], BotSignalValidate.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '5m',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], BotSignalValidate.prototype, "resolution", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'fad731ed_1673254427814',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], BotSignalValidate.prototype, "signal_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsString(),
    __metadata("design:type", Number)
], BotSignalValidate.prototype, "time", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'BUY',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], BotSignalValidate.prototype, "type", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'https://static.cextrading.io/images/Bot_Box/Bot_Box_LTCUSDT_5m_1671527700000.png',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], BotSignalValidate.prototype, "image_url", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'BINANCE',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], BotSignalValidate.prototype, "exchange", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'BTC',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], BotSignalValidate.prototype, "symbol", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsString(),
    __metadata("design:type", Number)
], BotSignalValidate.prototype, "created_at", void 0);
exports.BotSignalValidate = BotSignalValidate;
//# sourceMappingURL=validate.js.map