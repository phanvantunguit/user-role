import { EventEmitter2 } from '@nestjs/event-emitter';
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository';
import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { EventStoreRepository } from 'src/repositories/event-store.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { MetaapiResource } from 'src/resources/forex';
import { UserUpdateBotTradingStatusInput } from './validate';
export declare class UserUpdateBotTradingStatusHandler {
    private userRepository;
    private metaapiResource;
    private botTradingRepository;
    private eventStoreRepository;
    private accountBalanceRepository;
    private eventEmitter;
    constructor(userRepository: UserRepository, metaapiResource: MetaapiResource, botTradingRepository: BotTradingRepository, eventStoreRepository: EventStoreRepository, accountBalanceRepository: AccountBalanceRepository, eventEmitter: EventEmitter2);
    execute(params: UserUpdateBotTradingStatusInput, userId: string, merchantCode: string): Promise<any>;
}
