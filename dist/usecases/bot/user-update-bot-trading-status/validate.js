"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUpdateBotTradingStatusInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const transaction_1 = require("../../../const/transaction");
class UserUpdateBotTradingStatusInput {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'efebdafe-c359-4e88-bd17-82b2a5b1091b',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserUpdateBotTradingStatusInput.prototype, "bot_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: `Update status: ${transaction_1.ITEM_STATUS.ACTIVE}`,
        example: transaction_1.ITEM_STATUS.ACTIVE,
    }),
    class_validator_1.IsString(),
    class_validator_1.IsIn([transaction_1.ITEM_STATUS.ACTIVE]),
    __metadata("design:type", String)
], UserUpdateBotTradingStatusInput.prototype, "status", void 0);
exports.UserUpdateBotTradingStatusInput = UserUpdateBotTradingStatusInput;
//# sourceMappingURL=validate.js.map