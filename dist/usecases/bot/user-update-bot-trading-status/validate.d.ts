import { ITEM_STATUS } from 'src/const/transaction';
export declare class UserUpdateBotTradingStatusInput {
    bot_id: string;
    status: ITEM_STATUS;
}
