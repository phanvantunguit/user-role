"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUpdateBotTradingStatusHandler = void 0;
const common_1 = require("@nestjs/common");
const event_emitter_1 = require("@nestjs/event-emitter");
const config_1 = require("../../../config");
const const_1 = require("../../../const");
const app_setting_1 = require("../../../const/app-setting");
const bot_1 = require("../../../const/bot");
const transaction_1 = require("../../../const/transaction");
const account_balance_repository_1 = require("../../../repositories/account-balance.repository");
const bot_trading_repository_1 = require("../../../repositories/bot-trading.repository");
const event_store_repository_1 = require("../../../repositories/event-store.repository");
const user_repository_1 = require("../../../repositories/user.repository");
const forex_1 = require("../../../resources/forex");
const handle_error_util_1 = require("../../../utils/handle-error.util");
let UserUpdateBotTradingStatusHandler = class UserUpdateBotTradingStatusHandler {
    constructor(userRepository, metaapiResource, botTradingRepository, eventStoreRepository, accountBalanceRepository, eventEmitter) {
        this.userRepository = userRepository;
        this.metaapiResource = metaapiResource;
        this.botTradingRepository = botTradingRepository;
        this.eventStoreRepository = eventStoreRepository;
        this.accountBalanceRepository = accountBalanceRepository;
        this.eventEmitter = eventEmitter;
    }
    async execute(params, userId, merchantCode) {
        const { bot_id, status } = params;
        const [checkUserBot, bot] = await Promise.all([
            this.userRepository.findUserBotTrading({
                bot_id,
                user_id: userId,
            }),
            this.botTradingRepository.findById(bot_id),
        ]);
        if (checkUserBot.length === 0 || !bot) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
        if (!checkUserBot[0].subscriber_id ||
            ![
                transaction_1.ITEM_STATUS.INACTIVE,
                transaction_1.ITEM_STATUS.STOP_OUT,
                transaction_1.ITEM_STATUS.INACTIVE_BY_SYSTEM,
            ].includes(checkUserBot[0].status)) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ACCOUNT_NOT_CONNECTED));
        }
        if (checkUserBot[0].status === status) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.STATUS_INVALID));
        }
        if (checkUserBot[0].status === transaction_1.ITEM_STATUS.INACTIVE_BY_SYSTEM) {
            const events = await this.eventStoreRepository.find({
                user_id: userId,
                event_name: app_setting_1.APP_SETTING.TBOT_INACTIVE_BY_SYSTEM,
                state: app_setting_1.EVENT_STORE_STATE.OPEN,
            });
            let inactiveBySystemAt = 0;
            let countInactive = 0;
            events.forEach((e) => {
                var _a;
                if (((_a = e.metadata) === null || _a === void 0 ? void 0 : _a.bot_id) === bot_id) {
                    countInactive += 1;
                    if (inactiveBySystemAt < e.created_at) {
                        inactiveBySystemAt = e.created_at;
                    }
                }
            });
            if (countInactive > 1 &&
                Number(inactiveBySystemAt) + config_1.APP_CONFIG.TIME_BLOCKED_TBOT > Date.now()) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.TBOT_BLOCKED));
            }
        }
        const events = await this.eventStoreRepository.find({
            user_id: userId,
            event_name: app_setting_1.EVENT_STORE_NAME.TBOT_INACTIVE_BY_SYSTEM,
            state: app_setting_1.EVENT_STORE_STATE.OPEN,
        });
        const numberOfTimeInactive = events.filter((e) => { var _a; return ((_a = e.metadata) === null || _a === void 0 ? void 0 : _a.bot_id) === params.bot_id; }).length;
        if (numberOfTimeInactive > config_1.APP_CONFIG.NUMBER_OF_TIMES_ALLOW_USER_ACTIVATE_TBOT) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.TBOT_BLOCKED));
        }
        let startTime = checkUserBot[0].connected_at
            ? new Date(Number(checkUserBot[0].connected_at))
            : new Date(Number(checkUserBot[0].updated_at));
        const account = await this.metaapiResource.getAccount({
            account_id: checkUserBot[0].subscriber_id,
        });
        if (account &&
            account.state === 'DEPLOYED' &&
            account.connectionStatus === 'CONNECTED') {
            const connection = await account.getRPCConnection();
            await connection.connect();
            await connection.waitSynchronized();
            const accountInfo = await connection.getAccountInformation();
            const positions = await connection.getPositions();
            if (positions.length > 0) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.POSITION_NOT_EMPTY));
            }
            let isStopOut;
            try {
                const stopouts = await this.metaapiResource.copyFactory.tradingApi.getStopouts(checkUserBot[0].subscriber_id);
                isStopOut = stopouts.find((e) => e.strategy.id === bot.code);
            }
            catch (error) {
                console.log("getStopouts error", error);
            }
            if (Number(accountInfo.balance) < Number(bot.balance)) {
                handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ACCOUNT_NOT_ENOUGH_BALANCE));
            }
            const updateUserBot = {
                id: checkUserBot[0].id,
                status: transaction_1.ITEM_STATUS.CONNECTING,
            };
            const updateBot = await this.userRepository.saveUserBotTrading([
                updateUserBot,
            ]);
            this.eventEmitter.emit(bot_1.BOT_TRADING_EVENT.CONNECTING, {
                user_bot_id: updateBot[0].id,
                user_id: userId,
                bot_id,
                bot_code: bot.code,
            });
            delete updateBot[0].expires_at;
            return updateBot[0];
        }
        else {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ACCOUNT_NOT_CONNECTED));
        }
    }
};
UserUpdateBotTradingStatusHandler = __decorate([
    __param(0, common_1.Inject(user_repository_1.UserRepository)),
    __param(1, common_1.Inject(forex_1.MetaapiResource)),
    __param(2, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(3, common_1.Inject(event_store_repository_1.EventStoreRepository)),
    __param(4, common_1.Inject(account_balance_repository_1.AccountBalanceRepository)),
    __param(5, common_1.Inject(event_emitter_1.EventEmitter2)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        forex_1.MetaapiResource,
        bot_trading_repository_1.BotTradingRepository,
        event_store_repository_1.EventStoreRepository,
        account_balance_repository_1.AccountBalanceRepository,
        event_emitter_1.EventEmitter2])
], UserUpdateBotTradingStatusHandler);
exports.UserUpdateBotTradingStatusHandler = UserUpdateBotTradingStatusHandler;
//# sourceMappingURL=index.js.map