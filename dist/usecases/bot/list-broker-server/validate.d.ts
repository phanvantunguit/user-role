export declare class ListBrokerServerInput {
    broker_code: string;
}
export declare class ListBrokerServerOutput {
    payload: string[];
}
