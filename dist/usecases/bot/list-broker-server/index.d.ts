import { ListBrokerServerInput } from './validate';
export declare class ListBrokerServerHandler {
    constructor();
    execute(params: ListBrokerServerInput): Promise<string[]>;
}
