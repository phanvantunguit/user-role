import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { MetaapiResource } from 'src/resources/forex';
import { AppSettingService } from 'src/services/app-setting';
import { LogoutBrokerServerInput } from './validate';
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository';
export declare class LogoutBrokerServerHandler {
    private userRepository;
    private metaapiResource;
    private botTradingRepository;
    private appSettingService;
    private accountBalanceRepository;
    constructor(userRepository: UserRepository, metaapiResource: MetaapiResource, botTradingRepository: BotTradingRepository, appSettingService: AppSettingService, accountBalanceRepository: AccountBalanceRepository);
    execute(params: LogoutBrokerServerInput, userId: string): Promise<any>;
}
