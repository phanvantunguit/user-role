"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogoutBrokerServerHandler = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../../const");
const app_setting_1 = require("../../../const/app-setting");
const transaction_1 = require("../../../const/transaction");
const bot_trading_repository_1 = require("../../../repositories/bot-trading.repository");
const user_repository_1 = require("../../../repositories/user.repository");
const forex_1 = require("../../../resources/forex");
const app_setting_2 = require("../../../services/app-setting");
const handle_error_util_1 = require("../../../utils/handle-error.util");
const account_balance_repository_1 = require("../../../repositories/account-balance.repository");
let LogoutBrokerServerHandler = class LogoutBrokerServerHandler {
    constructor(userRepository, metaapiResource, botTradingRepository, appSettingService, accountBalanceRepository) {
        this.userRepository = userRepository;
        this.metaapiResource = metaapiResource;
        this.botTradingRepository = botTradingRepository;
        this.appSettingService = appSettingService;
        this.accountBalanceRepository = accountBalanceRepository;
    }
    async execute(params, userId) {
        const { bot_id } = params;
        const [checkUserBot, bot] = await Promise.all([
            this.userRepository.findUserBotTrading({
                bot_id,
                user_id: userId,
            }),
            this.botTradingRepository.findById(bot_id),
        ]);
        if (checkUserBot.length === 0 || !bot) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
        if (!checkUserBot[0].subscriber_id ||
            (checkUserBot[0].status !== transaction_1.ITEM_STATUS.ACTIVE &&
                checkUserBot[0].status !== transaction_1.ITEM_STATUS.INACTIVE &&
                checkUserBot[0].status !== transaction_1.ITEM_STATUS.INACTIVE_BY_SYSTEM &&
                checkUserBot[0].status !== transaction_1.ITEM_STATUS.STOP_OUT)) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.ACCOUNT_NOT_CONNECTED));
        }
        const dataLog = {
            asset_id: bot.id,
            user_id: userId,
            category: transaction_1.ORDER_CATEGORY.TBOT,
            status: transaction_1.ITEM_STATUS.LOG_OUT,
            owner_created: userId,
            name: bot.name,
            metadata: {},
        };
        const dataLogSaved = await this.userRepository.saveUserAssetLog([dataLog]);
        try {
            await this.metaapiResource.removeSubscriber({
                subscriber_id: checkUserBot[0].subscriber_id,
            });
        }
        catch (error) {
            dataLog.metadata['errorRemoveSubscriber'] = error;
            await this.userRepository.saveUserAssetLog([
                {
                    id: dataLogSaved[0].id,
                    metadata: dataLog.metadata,
                },
            ]);
        }
        try {
            await this.metaapiResource.removeAccount({
                subscriber_id: checkUserBot[0].subscriber_id,
            });
        }
        catch (error) {
            dataLog.metadata['errorRemoveAccount'] = error;
            await this.userRepository.saveUserAssetLog([
                {
                    id: dataLogSaved[0].id,
                    metadata: dataLog.metadata,
                },
            ]);
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNPROCESSABLE_ENTITY }, const_1.ERROR_CODE.UNPROCESSABLE));
        }
        if (checkUserBot[0].profile_id) {
            await this.metaapiResource.removeProvisioningProfile(checkUserBot[0].profile_id);
        }
        const asNumberOfBot = await this.appSettingService.getAppSetting({
            user_id: userId,
            name: app_setting_1.APP_SETTING.NUMBER_OF_TBOT,
        });
        const numberOfBotOfUser = Number(asNumberOfBot === null || asNumberOfBot === void 0 ? void 0 : asNumberOfBot.value) || 0;
        if (numberOfBotOfUser > 0 || checkUserBot[0].type === transaction_1.TBOT_TYPE.SELECT) {
            const userBotTradingDeleted = await this.userRepository.deleteUserBotTrading({
                user_id: userId,
                bot_id,
            });
            dataLog.metadata['userBotTradingDeleted'] = userBotTradingDeleted;
        }
        else {
            const updateUserBot = {
                id: checkUserBot[0].id,
                status: transaction_1.ITEM_STATUS.NOT_CONNECT,
                subscriber_id: null,
                broker_account: null,
            };
            const userBotTradingSaved = await this.userRepository.saveUserBotTrading([
                updateUserBot,
            ]);
            dataLog.metadata['userBotTradingSaved'] = userBotTradingSaved;
        }
        await this.accountBalanceRepository.deleteAccountBalance({
            user_id: userId,
            bot_id,
        });
        await this.userRepository.saveUserAssetLog([
            {
                id: dataLogSaved[0].id,
                metadata: dataLog.metadata,
            },
        ]);
        delete checkUserBot[0].expires_at;
        return checkUserBot[0];
    }
};
LogoutBrokerServerHandler = __decorate([
    __param(0, common_1.Inject(user_repository_1.UserRepository)),
    __param(1, common_1.Inject(forex_1.MetaapiResource)),
    __param(2, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(3, common_1.Inject(app_setting_2.AppSettingService)),
    __param(4, common_1.Inject(account_balance_repository_1.AccountBalanceRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        forex_1.MetaapiResource,
        bot_trading_repository_1.BotTradingRepository,
        app_setting_2.AppSettingService,
        account_balance_repository_1.AccountBalanceRepository])
], LogoutBrokerServerHandler);
exports.LogoutBrokerServerHandler = LogoutBrokerServerHandler;
//# sourceMappingURL=index.js.map