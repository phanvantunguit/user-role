import { EventEmitter2 } from '@nestjs/event-emitter';
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository';
import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { EventStoreRepository } from 'src/repositories/event-store.repository';
import { MerchantRepository } from 'src/repositories/merchant.repository';
import { SettingRepository } from 'src/repositories/setting.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { BrokerResource } from 'src/resources/broker';
import { MetaapiResource } from 'src/resources/forex';
import { ConnectBrokerServerInput } from './validate';
export declare class ConnectBrokerServerHandler {
    private userRepository;
    private metaapiResource;
    private botTradingRepository;
    private eventEmitter;
    private accountBalanceRepository;
    private eventStoreRepository;
    private merchantRepository;
    private brokerResource;
    private settingRepository;
    constructor(userRepository: UserRepository, metaapiResource: MetaapiResource, botTradingRepository: BotTradingRepository, eventEmitter: EventEmitter2, accountBalanceRepository: AccountBalanceRepository, eventStoreRepository: EventStoreRepository, merchantRepository: MerchantRepository, brokerResource: BrokerResource, settingRepository: SettingRepository);
    execute(params: ConnectBrokerServerInput, userId: string, merchantCode: string): Promise<any>;
}
