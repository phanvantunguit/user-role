import { BROKER_CODE, TBOT_PLATFORM } from 'src/const/bot';
export declare class ConnectBrokerServerInput {
    bot_id: string;
    broker_code: BROKER_CODE;
    broker_server: string;
    account_id: string;
    password: string;
    platform: TBOT_PLATFORM;
    profile_id: string;
}
