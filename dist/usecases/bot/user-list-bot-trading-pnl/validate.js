"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListBotTradingPnlPayloadOutput = exports.BotPnlValidate = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
class BotPnlValidate {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], BotPnlValidate.prototype, "bot_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 20,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], BotPnlValidate.prototype, "pnl_day_percent", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 100,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], BotPnlValidate.prototype, "pnl_day_init", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 120,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], BotPnlValidate.prototype, "pnl_day_current", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], BotPnlValidate.prototype, "created_at", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: Date.now(),
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], BotPnlValidate.prototype, "updated_at", void 0);
exports.BotPnlValidate = BotPnlValidate;
class UserListBotTradingPnlPayloadOutput {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        isArray: true,
        type: BotPnlValidate,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsArray(),
    class_transformer_1.Type(() => BotPnlValidate),
    __metadata("design:type", Array)
], UserListBotTradingPnlPayloadOutput.prototype, "payload", void 0);
exports.UserListBotTradingPnlPayloadOutput = UserListBotTradingPnlPayloadOutput;
//# sourceMappingURL=validate.js.map