import { AccountBalanceRepository } from 'src/repositories/account-balance.repository';
import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository';
import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { BotPnlValidate } from './validate';
export declare class UserListBotTradingPNLHandler {
    private accountBalanceRepository;
    private botTradingHistoryRepository;
    private botTradingRepository;
    private userRepository;
    constructor(accountBalanceRepository: AccountBalanceRepository, botTradingHistoryRepository: BotTradingHistoryRepository, botTradingRepository: BotTradingRepository, userRepository: UserRepository);
    execute(user_id: string, bot_id: string, strategy_trade?: string): Promise<BotPnlValidate[]>;
}
