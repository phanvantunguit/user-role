export declare class BotPnlValidate {
    bot_id: string;
    pnl_day_percent: number;
    pnl_day_init: number;
    pnl_day_current: number;
    created_at: number;
    updated_at: number;
}
export declare class UserListBotTradingPnlPayloadOutput {
    payload: BotPnlValidate[];
}
