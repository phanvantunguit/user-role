"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListBotTradingPNLHandler = void 0;
const common_1 = require("@nestjs/common");
const account_balance_repository_1 = require("../../../repositories/account-balance.repository");
const bot_trading_history_repository_1 = require("../../../repositories/bot-trading-history.repository");
const bot_trading_repository_1 = require("../../../repositories/bot-trading.repository");
const user_repository_1 = require("../../../repositories/user.repository");
const handle_error_util_1 = require("../../../utils/handle-error.util");
const const_1 = require("../../../const");
const transaction_1 = require("../../../const/transaction");
let UserListBotTradingPNLHandler = class UserListBotTradingPNLHandler {
    constructor(accountBalanceRepository, botTradingHistoryRepository, botTradingRepository, userRepository) {
        this.accountBalanceRepository = accountBalanceRepository;
        this.botTradingHistoryRepository = botTradingHistoryRepository;
        this.botTradingRepository = botTradingRepository;
        this.userRepository = userRepository;
    }
    async execute(user_id, bot_id, strategy_trade) {
        let userId;
        let userBot;
        let connected_at;
        if (user_id && bot_id) {
            userBot = await this.userRepository.findUserBotTrading({
                user_id,
                bot_id,
            });
            if (userBot[0]) {
                connected_at = userBot[0].connected_at;
                if (strategy_trade !== 'true') {
                    userId = user_id;
                    if (userBot[0].status === transaction_1.ITEM_STATUS.NOT_CONNECT || userBot[0].status === transaction_1.ITEM_STATUS.CONNECTING) {
                        connected_at = Date.now();
                    }
                }
            }
        }
        const data = await this.botTradingHistoryRepository.chartPNL({
            bot_id,
            user_id: userId,
            timezone: `UTC-7`,
            time_type: 'DAY',
            connected_at
        });
        const bot = await this.botTradingRepository.findById(bot_id);
        if (!bot) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
        let initBalance = 0;
        const balance = await this.accountBalanceRepository.getBalanceByUser({
            user_id: userId,
            bot_id,
        });
        if (balance[0]) {
            initBalance = balance[0].total;
        }
        else {
            initBalance = Number(bot.balance) || 10000;
        }
        const balanceLatest = await this.accountBalanceRepository.getBalance({
            user_id: userId,
            bot_id,
            order: 'desc',
        });
        const realBalance = balanceLatest[0] ? balanceLatest[0].balance : initBalance;
        const result = [];
        let currentBalance = initBalance;
        let cumulativePNL = 0;
        for (let i = 0; i < data.length; i++) {
            const e = data[i];
            if (i > 0) {
                const startDate = new Date(data[i - 1].time);
                const secondDate = new Date(data[i - 1].time);
                secondDate.setDate(secondDate.getDate() + 1);
                const endDate = new Date(e.time);
                const timeDiff = Math.abs(endDate.valueOf() - startDate.valueOf());
                const diffDays = Math.floor(timeDiff / (1000 * 3600 * 24));
                if (diffDays > 1) {
                    for (let d = secondDate; d < endDate; d.setDate(d.getDate() + 1)) {
                        result.push(Object.assign(Object.assign({}, result[result.length - 1]), { created_at: d.valueOf(), updated_at: d.valueOf() }));
                    }
                }
            }
            currentBalance += e.profit_cash;
            cumulativePNL += e.profit_cash;
            result.push({
                bot_id,
                pnl_day_cash: cumulativePNL,
                current_balance: currentBalance,
                balance: realBalance,
                pnl_day_percent: (cumulativePNL / initBalance) * 100,
                created_at: new Date(e.time).valueOf(),
                updated_at: new Date(e.time).valueOf(),
            });
        }
        return result;
    }
};
UserListBotTradingPNLHandler = __decorate([
    __param(0, common_1.Inject(account_balance_repository_1.AccountBalanceRepository)),
    __param(1, common_1.Inject(bot_trading_history_repository_1.BotTradingHistoryRepository)),
    __param(2, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(3, common_1.Inject(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [account_balance_repository_1.AccountBalanceRepository,
        bot_trading_history_repository_1.BotTradingHistoryRepository,
        bot_trading_repository_1.BotTradingRepository,
        user_repository_1.UserRepository])
], UserListBotTradingPNLHandler);
exports.UserListBotTradingPNLHandler = UserListBotTradingPNLHandler;
//# sourceMappingURL=index.js.map