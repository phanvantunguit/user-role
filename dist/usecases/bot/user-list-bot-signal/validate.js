"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListBotSignalPayloadOutput = exports.UserListSignalOutput = exports.UserListBotSignalInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const validate_1 = require("../validate");
class UserListBotSignalInput {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Page',
        example: 1,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], UserListBotSignalInput.prototype, "page", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Size',
        example: 50,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], UserListBotSignalInput.prototype, "size", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'bot_ids',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2,1c9e1891-b5b0-4949-bcfe-a2e25c1e53e1',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserListBotSignalInput.prototype, "bot_ids", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'exchange name',
        example: 'BINANCE,FTX',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserListBotSignalInput.prototype, "exchanges", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'resolution',
        example: '5m,7m',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserListBotSignalInput.prototype, "resolutions", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'type',
        example: 'BUY,SELL',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserListBotSignalInput.prototype, "types", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'symbol',
        example: 'BTC,USD',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserListBotSignalInput.prototype, "symbols", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'favorite',
        example: 'true',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsBooleanString(),
    __metadata("design:type", String)
], UserListBotSignalInput.prototype, "favorite", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'From',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], UserListBotSignalInput.prototype, "from", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'To',
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsNumberString(),
    __metadata("design:type", Number)
], UserListBotSignalInput.prototype, "to", void 0);
exports.UserListBotSignalInput = UserListBotSignalInput;
class UserListSignalOutput {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Page',
        example: 1,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserListSignalOutput.prototype, "page", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'Size',
        example: 50,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserListSignalOutput.prototype, "size", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserListSignalOutput.prototype, "count", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], UserListSignalOutput.prototype, "total", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        isArray: true,
        type: validate_1.BotSignalValidate,
    }),
    class_validator_1.IsOptional(),
    class_validator_1.IsArray(),
    class_transformer_1.Type(() => validate_1.BotSignalValidate),
    __metadata("design:type", Array)
], UserListSignalOutput.prototype, "rows", void 0);
exports.UserListSignalOutput = UserListSignalOutput;
class UserListBotSignalPayloadOutput {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        type: UserListSignalOutput,
    }),
    class_validator_1.IsObject(),
    __metadata("design:type", UserListSignalOutput)
], UserListBotSignalPayloadOutput.prototype, "payload", void 0);
exports.UserListBotSignalPayloadOutput = UserListBotSignalPayloadOutput;
//# sourceMappingURL=validate.js.map