import { BotSignalRepository } from 'src/repositories/bot-signal.repository';
import { BotRepository } from 'src/repositories/bot.repository';
import { SettingRepository } from 'src/repositories/setting.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { UserListBotSignalInput, UserListSignalOutput } from './validate';
export declare class UserListSignalHandler {
    private botSignalRepository;
    private userRepository;
    private botRepository;
    private settingRepository;
    constructor(botSignalRepository: BotSignalRepository, userRepository: UserRepository, botRepository: BotRepository, settingRepository: SettingRepository);
    execute(params: UserListBotSignalInput, user_id: string): Promise<UserListSignalOutput>;
}
