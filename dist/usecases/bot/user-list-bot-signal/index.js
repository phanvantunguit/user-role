"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListSignalHandler = void 0;
const common_1 = require("@nestjs/common");
const app_setting_1 = require("../../../const/app-setting");
const transaction_1 = require("../../../const/transaction");
const bot_signal_repository_1 = require("../../../repositories/bot-signal.repository");
const bot_repository_1 = require("../../../repositories/bot.repository");
const setting_repository_1 = require("../../../repositories/setting.repository");
const user_repository_1 = require("../../../repositories/user.repository");
let UserListSignalHandler = class UserListSignalHandler {
    constructor(botSignalRepository, userRepository, botRepository, settingRepository) {
        this.botSignalRepository = botSignalRepository;
        this.userRepository = userRepository;
        this.botRepository = botRepository;
        this.settingRepository = settingRepository;
    }
    async execute(params, user_id) {
        var _a, _b, _c, _d, _e;
        const favorite = params.favorite === 'true' ? true : false;
        const [userBots, userSettingSignalPlatfrom] = await Promise.all([
            this.userRepository.findUserBotSQL({
                user_id,
                status: transaction_1.ITEM_STATUS.ACTIVE,
                expires_at: Date.now(),
            }),
            this.settingRepository.findOneAppSetting({
                name: app_setting_1.APP_SETTING.SIGNAL_PLATFORM,
                user_id,
            }),
        ]);
        let offBotCodes = [];
        if (userSettingSignalPlatfrom && userSettingSignalPlatfrom.value) {
            try {
                const config = JSON.parse(userSettingSignalPlatfrom.value);
                if (config && config.OFF_SIGNALS_BOT) {
                    offBotCodes = config.OFF_SIGNALS_BOT.toString().split(',');
                }
            }
            catch (error) {
                console.log('error', error);
            }
        }
        const queryBotIds = ((_a = params.bot_ids) === null || _a === void 0 ? void 0 : _a.split(',')) || [];
        let botIds = userBots.map((b) => b.bot_id);
        if (queryBotIds.length > 0) {
            botIds = botIds.filter((bs) => queryBotIds.includes(bs));
        }
        const bots = await this.botRepository.findByIds(botIds);
        const botCodes = [];
        for (const b of bots) {
            if (b.code && !offBotCodes.includes(b.code)) {
                botCodes.push(b.code);
            }
        }
        if (botCodes.length === 0) {
            return {
                rows: [],
                page: 1,
                size: Number(params.size) || 50,
                count: 0,
                total: 0,
            };
        }
        const query = {
            page: params.page,
            size: params.size,
            exchanges: (_b = params.exchanges) === null || _b === void 0 ? void 0 : _b.split(','),
            symbols: (_c = params.symbols) === null || _c === void 0 ? void 0 : _c.split(','),
            names: botCodes,
            resolutions: (_d = params.resolutions) === null || _d === void 0 ? void 0 : _d.split(','),
            types: (_e = params.types) === null || _e === void 0 ? void 0 : _e.split(','),
            from: params.from,
            to: params.to,
            user_id,
        };
        if (favorite) {
            return this.botSignalRepository.getFavoritePaging(query);
        }
        else {
            return this.botSignalRepository.getPaging(query);
        }
    }
};
UserListSignalHandler = __decorate([
    __param(0, common_1.Inject(bot_signal_repository_1.BotSignalRepository)),
    __param(1, common_1.Inject(user_repository_1.UserRepository)),
    __param(2, common_1.Inject(bot_repository_1.BotRepository)),
    __param(3, common_1.Inject(setting_repository_1.SettingRepository)),
    __metadata("design:paramtypes", [bot_signal_repository_1.BotSignalRepository,
        user_repository_1.UserRepository,
        bot_repository_1.BotRepository,
        setting_repository_1.SettingRepository])
], UserListSignalHandler);
exports.UserListSignalHandler = UserListSignalHandler;
//# sourceMappingURL=index.js.map