import { BotSignalValidate } from '../validate';
export declare class UserListBotSignalInput {
    page: number;
    size: number;
    bot_ids?: string;
    exchanges?: string;
    resolutions?: string;
    types: string;
    symbols?: string;
    favorite?: string;
    from: number;
    to: number;
}
export declare class UserListSignalOutput {
    page: number;
    size: number;
    count: number;
    total: number;
    rows: BotSignalValidate[];
}
export declare class UserListBotSignalPayloadOutput {
    payload: UserListSignalOutput;
}
