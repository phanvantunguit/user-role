"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateBalanceAndFeeDataOutput = exports.CreatelDataValidate = exports.CreateBalanceAndFeeDataInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
class CreateBalanceAndFeeDataInput {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Bot_id',
    }),
    class_validator_1.IsUUID(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CreateBalanceAndFeeDataInput.prototype, "bot_id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'Balance Bot',
    }),
    class_validator_1.IsNumber(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], CreateBalanceAndFeeDataInput.prototype, "balance", void 0);
exports.CreateBalanceAndFeeDataInput = CreateBalanceAndFeeDataInput;
class CreatelDataValidate {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'price',
    }),
    class_validator_1.IsNumber(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], CreatelDataValidate.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: false,
        description: 'total_price',
    }),
    class_validator_1.IsNumber(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], CreatelDataValidate.prototype, "total_price", void 0);
exports.CreatelDataValidate = CreatelDataValidate;
class CreateBalanceAndFeeDataOutput {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        type: CreatelDataValidate,
    }),
    class_validator_1.IsOptional(),
    class_transformer_1.Type(() => CreatelDataValidate),
    __metadata("design:type", CreatelDataValidate)
], CreateBalanceAndFeeDataOutput.prototype, "payload", void 0);
exports.CreateBalanceAndFeeDataOutput = CreateBalanceAndFeeDataOutput;
//# sourceMappingURL=validate.js.map