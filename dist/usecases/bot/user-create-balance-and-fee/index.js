"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateBalanceAndFeeDataHandler = void 0;
const common_1 = require("@nestjs/common");
const additional_data_repository_1 = require("../../../repositories/additional-data.repository");
const types_1 = require("../../../domains/merchant/types");
const bot_trading_repository_1 = require("../../../repositories/bot-trading.repository");
const handle_error_util_1 = require("../../../utils/handle-error.util");
const const_1 = require("../../../const");
let CreateBalanceAndFeeDataHandler = class CreateBalanceAndFeeDataHandler {
    constructor(additionalDataRepository, botTradingRepository) {
        this.additionalDataRepository = additionalDataRepository;
        this.botTradingRepository = botTradingRepository;
    }
    async execute(params) {
        var _a, _b, _c;
        const data = await this.additionalDataRepository.list('DEFAULT', types_1.ADDITIONAL_DATA_TYPE.TBOT_FEE);
        const GetTbot = await this.botTradingRepository.findById(params.bot_id);
        let totalPrice;
        let baseFee = Number(GetTbot.price);
        if (!GetTbot) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.NOT_FOUND }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
        (_c = (_b = (_a = data[0]) === null || _a === void 0 ? void 0 : _a.data) === null || _b === void 0 ? void 0 : _b.ranges) === null || _c === void 0 ? void 0 : _c.map((item, i, array) => {
            var _a, _b;
            baseFee =
                baseFee +
                    (Number(item === null || item === void 0 ? void 0 : item.from) - Number((_a = array[i - 1]) === null || _a === void 0 ? void 0 : _a.from)) *
                        Number((_b = array[i - 1]) === null || _b === void 0 ? void 0 : _b.percent);
            if (!baseFee) {
                baseFee = Number(GetTbot.price);
            }
            if (params.balance >= (item === null || item === void 0 ? void 0 : item.from) && params.balance <= (item === null || item === void 0 ? void 0 : item.to)) {
                totalPrice =
                    Number(baseFee) +
                        (Number(params.balance) - Number(item === null || item === void 0 ? void 0 : item.from)) * Number(item === null || item === void 0 ? void 0 : item.percent);
            }
            else if (params.balance >= (item === null || item === void 0 ? void 0 : item.from) && !(item === null || item === void 0 ? void 0 : item.to)) {
                totalPrice =
                    Number(baseFee) +
                        (Number(params.balance) - Number(item === null || item === void 0 ? void 0 : item.from)) * Number(item === null || item === void 0 ? void 0 : item.percent);
            }
        });
        return {
            total_price: totalPrice,
            price: Number(GetTbot.price),
        };
    }
};
CreateBalanceAndFeeDataHandler = __decorate([
    __param(0, common_1.Inject(additional_data_repository_1.AdditionalDataRepository)),
    __param(1, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __metadata("design:paramtypes", [additional_data_repository_1.AdditionalDataRepository,
        bot_trading_repository_1.BotTradingRepository])
], CreateBalanceAndFeeDataHandler);
exports.CreateBalanceAndFeeDataHandler = CreateBalanceAndFeeDataHandler;
//# sourceMappingURL=index.js.map