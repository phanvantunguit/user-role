export declare class CreateBalanceAndFeeDataInput {
    bot_id?: string;
    balance?: number;
}
export declare class CreatelDataValidate {
    price?: Number;
    total_price?: Number;
}
export declare class CreateBalanceAndFeeDataOutput {
    payload: CreatelDataValidate;
}
