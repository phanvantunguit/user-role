import { AdditionalDataRepository } from 'src/repositories/additional-data.repository';
import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { CreateBalanceAndFeeDataInput, CreatelDataValidate } from './validate';
export declare class CreateBalanceAndFeeDataHandler {
    private additionalDataRepository;
    private botTradingRepository;
    constructor(additionalDataRepository: AdditionalDataRepository, botTradingRepository: BotTradingRepository);
    execute(params: CreateBalanceAndFeeDataInput): Promise<CreatelDataValidate>;
}
