import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository';
import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
export declare class AdminImportTradeHistoryCSVHandler {
    private botTradingRepository;
    private botTradingHistoryRepository;
    constructor(botTradingRepository: BotTradingRepository, botTradingHistoryRepository: BotTradingHistoryRepository);
    execute(file: any, param: any, admin_id: string): Promise<any>;
}
