"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminImportTradeHistoryCSVHandler = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../../const");
const bot_1 = require("../../../const/bot");
const bot_trading_history_repository_1 = require("../../../repositories/bot-trading-history.repository");
const bot_trading_repository_1 = require("../../../repositories/bot-trading.repository");
const handle_error_util_1 = require("../../../utils/handle-error.util");
const XLSX = require("xlsx");
let AdminImportTradeHistoryCSVHandler = class AdminImportTradeHistoryCSVHandler {
    constructor(botTradingRepository, botTradingHistoryRepository) {
        this.botTradingRepository = botTradingRepository;
        this.botTradingHistoryRepository = botTradingHistoryRepository;
    }
    async execute(file, param, admin_id) {
        const { bot_id } = param;
        const bot = await this.botTradingRepository.findById(bot_id);
        if (!bot) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
        const workbook = XLSX.read(file.buffer, { type: 'buffer' });
        const rowObjects = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);
        const dataSaves = [];
        try {
            for (let row of rowObjects) {
                if (row) {
                    const direction = row.side === bot_1.TRADE_SIDE.LONG ? 1 : -1;
                    const profitCash = Number(row.close) && Number(row.entry)
                        ? direction *
                            (Number(row.close) - Number(row.entry)) *
                            Number(row.volume)
                        : null;
                    const profit = Number(row.close) && Number(row.entry)
                        ? ((direction * (Number(row.close) - Number(row.entry))) /
                            Number(row.entry)) *
                            100
                        : null;
                    dataSaves.push({
                        token_first: bot.token_first,
                        token_second: bot.token_second,
                        bot_id,
                        status: row.status,
                        side: row.side,
                        entry_price: row.entry,
                        close_price: row.close,
                        volume: Number(row.volume),
                        day_started: row.started
                            ? new Date(row.started.replace('_', ' ')).valueOf()
                            : null,
                        day_completed: row.completed
                            ? new Date(row.completed.replace('_', ' ')).valueOf()
                            : null,
                        owner_created: admin_id,
                        profit,
                        profit_cash: profitCash,
                    });
                }
            }
        }
        catch (error) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.UNPROCESSABLE));
        }
        if (dataSaves.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.TRANSACTION_NOT_FOUND));
        }
        await this.botTradingHistoryRepository.deleteDataWithoutUser({ bot_id });
        const saved = await this.botTradingHistoryRepository.saves(dataSaves);
        return saved;
    }
};
AdminImportTradeHistoryCSVHandler = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(1, common_1.Inject(bot_trading_history_repository_1.BotTradingHistoryRepository)),
    __metadata("design:paramtypes", [bot_trading_repository_1.BotTradingRepository,
        bot_trading_history_repository_1.BotTradingHistoryRepository])
], AdminImportTradeHistoryCSVHandler);
exports.AdminImportTradeHistoryCSVHandler = AdminImportTradeHistoryCSVHandler;
//# sourceMappingURL=index.js.map