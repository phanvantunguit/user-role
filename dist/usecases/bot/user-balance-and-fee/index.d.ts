import { BalanceAndFeeDataOutput } from './validate';
import { AdditionalDataRepository } from 'src/repositories/additional-data.repository';
export declare class BalanceAndFeeDataHandler {
    private additionalDataRepository;
    constructor(additionalDataRepository: AdditionalDataRepository);
    execute(): Promise<BalanceAndFeeDataOutput>;
}
