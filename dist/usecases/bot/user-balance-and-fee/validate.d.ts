import { ADDITIONAL_DATA_TYPE } from 'src/domains/merchant/types';
export declare class BalanceAndFeeDataInput {
    name?: string;
}
export declare class AdditionalDataValidate {
    type?: ADDITIONAL_DATA_TYPE;
    name?: string;
    data?: any;
}
export declare class BalanceAndFeeDataOutput {
    payload: AdditionalDataValidate;
}
