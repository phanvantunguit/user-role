import { ITEM_STATUS } from 'src/const/transaction';
export declare class AdminUpdateBotTradingStatusInput {
    status: ITEM_STATUS;
}
export declare class AdminUpdateBotTradingStatusValidate {
    id: string;
    status: ITEM_STATUS;
}
export declare class AdminUpdateBotTradingStatusOutput {
    payload: AdminUpdateBotTradingStatusValidate;
}
