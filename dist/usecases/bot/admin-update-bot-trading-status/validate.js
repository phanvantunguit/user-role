"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminUpdateBotTradingStatusOutput = exports.AdminUpdateBotTradingStatusValidate = exports.AdminUpdateBotTradingStatusInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const transaction_1 = require("../../../const/transaction");
class AdminUpdateBotTradingStatusInput {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: `Update status: ${transaction_1.ITEM_STATUS.INACTIVE}`,
        example: transaction_1.ITEM_STATUS.INACTIVE,
    }),
    class_validator_1.IsString(),
    class_validator_1.IsIn([transaction_1.ITEM_STATUS.INACTIVE]),
    __metadata("design:type", String)
], AdminUpdateBotTradingStatusInput.prototype, "status", void 0);
exports.AdminUpdateBotTradingStatusInput = AdminUpdateBotTradingStatusInput;
class AdminUpdateBotTradingStatusValidate {
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        example: 'efebdafe-c359-4e88-bd17-82b2a5b1091b',
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminUpdateBotTradingStatusValidate.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        description: `Update status: ${Object.values(transaction_1.ITEM_STATUS)}`,
        example: transaction_1.ITEM_STATUS.INACTIVE,
    }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], AdminUpdateBotTradingStatusValidate.prototype, "status", void 0);
exports.AdminUpdateBotTradingStatusValidate = AdminUpdateBotTradingStatusValidate;
class AdminUpdateBotTradingStatusOutput {
}
__decorate([
    swagger_1.ApiProperty({
        required: false,
        type: AdminUpdateBotTradingStatusValidate,
    }),
    class_validator_1.IsObject(),
    __metadata("design:type", AdminUpdateBotTradingStatusValidate)
], AdminUpdateBotTradingStatusOutput.prototype, "payload", void 0);
exports.AdminUpdateBotTradingStatusOutput = AdminUpdateBotTradingStatusOutput;
//# sourceMappingURL=validate.js.map