"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminUpdateBotTradingStatusHandler = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../../const");
const app_setting_1 = require("../../../const/app-setting");
const bot_1 = require("../../../const/bot");
const transaction_1 = require("../../../const/transaction");
const bot_trading_repository_1 = require("../../../repositories/bot-trading.repository");
const cache_repository_1 = require("../../../repositories/cache.repository");
const user_repository_1 = require("../../../repositories/user.repository");
const forex_1 = require("../../../resources/forex");
const handle_error_util_1 = require("../../../utils/handle-error.util");
let AdminUpdateBotTradingStatusHandler = class AdminUpdateBotTradingStatusHandler {
    constructor(userRepository, metaapiResource, botTradingRepository, cacheRepository) {
        this.userRepository = userRepository;
        this.metaapiResource = metaapiResource;
        this.botTradingRepository = botTradingRepository;
        this.cacheRepository = cacheRepository;
    }
    async execute(params, id) {
        const { status } = params;
        const userBot = await this.userRepository.findUserBotTrading({ id });
        if (userBot.length === 0) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
        const bot = await this.botTradingRepository.findById(userBot[0].bot_id);
        if (!bot) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.BOT_NOT_FOUND));
        }
        if (!userBot[0].subscriber_id ||
            ![transaction_1.ITEM_STATUS.INACTIVE_BY_SYSTEM].includes(userBot[0].status)) {
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.BAD_REQUEST }, const_1.ERROR_CODE.STATUS_INVALID));
        }
        const updateUserBot = {
            id: userBot[0].id,
            status,
        };
        try {
            const stopouts = await this.metaapiResource.copyFactory.tradingApi.getStopouts(userBot[0].subscriber_id);
            const isStopOut = stopouts.find((e) => e.strategy.id === bot.code);
            if (isStopOut) {
                updateUserBot.status = transaction_1.ITEM_STATUS.STOP_OUT;
            }
        }
        catch (error) {
        }
        await this.userRepository.saveUserBotTrading([updateUserBot]);
        this.cacheRepository.publisher.publish(app_setting_1.CHANELS.TBOT_CHANEL_TS, JSON.stringify({
            event: bot_1.TBOT_CHANEL_TS_EVENT.account,
            data: {
                bot_id: bot.id,
                user_id: userBot[0].user_id,
                user_bot_id: updateUserBot.id,
                status: updateUserBot.status,
            },
        }));
        return {
            id: updateUserBot.id,
            status,
        };
    }
};
AdminUpdateBotTradingStatusHandler = __decorate([
    __param(0, common_1.Inject(user_repository_1.UserRepository)),
    __param(1, common_1.Inject(forex_1.MetaapiResource)),
    __param(2, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(3, common_1.Inject(cache_repository_1.CacheRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        forex_1.MetaapiResource,
        bot_trading_repository_1.BotTradingRepository,
        cache_repository_1.CacheRepository])
], AdminUpdateBotTradingStatusHandler);
exports.AdminUpdateBotTradingStatusHandler = AdminUpdateBotTradingStatusHandler;
//# sourceMappingURL=index.js.map