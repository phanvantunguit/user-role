import { BOT_STATUS } from 'src/const';
import { ITEM_STATUS } from 'src/const/transaction';
export declare class BotDataValidate {
    id?: string;
    name?: string;
    type?: string;
    description?: string;
    status?: BOT_STATUS;
    price?: string;
    currency?: string;
    work_based_on?: string[];
    image_url?: string;
    created_at?: number;
    updated_at?: number;
    order?: number;
    expires_at?: number;
    user_status?: ITEM_STATUS;
}
export declare class UserListBotOutput {
    payload: BotDataValidate[];
}
