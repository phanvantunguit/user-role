"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListBotHandler = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../../const");
const transaction_1 = require("../../../const/transaction");
const bot_repository_1 = require("../../../repositories/bot.repository");
const user_repository_1 = require("../../../repositories/user.repository");
let UserListBotHandler = class UserListBotHandler {
    constructor(botRepository, userRepository) {
        this.botRepository = botRepository;
        this.userRepository = userRepository;
    }
    async execute(user_id) {
        const bots = await this.botRepository.listConditionMultiple({
            status: [const_1.BOT_STATUS.OPEN, const_1.BOT_STATUS.COMINGSOON],
        });
        if (!user_id) {
            return bots;
        }
        let userBots = await this.userRepository.findUserBot({
            user_id,
            status: transaction_1.ITEM_STATUS.ACTIVE,
        });
        const userBotIdDuplicate = [];
        userBots = userBots.filter((ub) => {
            if (!userBotIdDuplicate.includes(ub.bot_id)) {
                userBotIdDuplicate.push(ub.bot_id);
                return true;
            }
            return false;
        });
        const mapBots = bots.map((b) => {
            const userBotExit = userBots.find((ub) => ub.bot_id === b.id);
            if (userBotExit) {
                return Object.assign(Object.assign({}, b), { expires_at: userBotExit.expires_at });
            }
            else {
                return b;
            }
        });
        return mapBots;
    }
};
UserListBotHandler = __decorate([
    __param(0, common_1.Inject(bot_repository_1.BotRepository)),
    __param(1, common_1.Inject(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [bot_repository_1.BotRepository,
        user_repository_1.UserRepository])
], UserListBotHandler);
exports.UserListBotHandler = UserListBotHandler;
//# sourceMappingURL=index.js.map