"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserGetBotTradingHandler = void 0;
const common_1 = require("@nestjs/common");
const transaction_1 = require("../../../const/transaction");
const account_balance_repository_1 = require("../../../repositories/account-balance.repository");
const bot_trading_history_repository_1 = require("../../../repositories/bot-trading-history.repository");
const bot_trading_repository_1 = require("../../../repositories/bot-trading.repository");
const user_repository_1 = require("../../../repositories/user.repository");
let UserGetBotTradingHandler = class UserGetBotTradingHandler {
    constructor(botTradingRepository, accountBalanceRepository, botTradingHistoryRepository, userRepository) {
        this.botTradingRepository = botTradingRepository;
        this.accountBalanceRepository = accountBalanceRepository;
        this.botTradingHistoryRepository = botTradingHistoryRepository;
        this.userRepository = userRepository;
    }
    async execute(bot_id, user_id) {
        var _a, _b, _c, _d;
        const bot = await this.botTradingRepository.findById(bot_id);
        if (!user_id) {
            return bot;
        }
        const userBot = await this.userRepository.findUserBotTrading({
            user_id,
            bot_id,
        });
        if (userBot.length === 0) {
            return bot;
        }
        const userBotExit = userBot[0];
        const minBalance = bot.balance;
        if (!userBotExit.subscriber_id || userBotExit.status === transaction_1.ITEM_STATUS.CONNECTING || userBotExit.status === transaction_1.ITEM_STATUS.NOT_CONNECT) {
            return Object.assign(Object.assign({}, bot), { expires_at: userBotExit.expires_at, user_status: userBotExit.status, broker: userBotExit.broker, broker_server: userBotExit.broker_server, broker_account: userBotExit.broker_account, min_balance: minBalance, balance: 0 });
        }
        const time7Day = new Date();
        time7Day.setDate(time7Day.getDate() - 7);
        time7Day.setHours(0, 0, 0, 0);
        const time30Day = new Date();
        time30Day.setDate(time30Day.getDate() - 30);
        time30Day.setHours(0, 0, 0, 0);
        const timeToDay = new Date();
        timeToDay.setHours(0, 0, 0, 0);
        const [balanceInit, profit7Day, profit30Day, profitToday, totalProfit, totalTrade, balanceLastet,] = await Promise.all([
            this.accountBalanceRepository.getBalanceByUser({
                user_id,
                bot_id,
            }),
            this.botTradingHistoryRepository.getProfit({
                user_id,
                bot_id,
                to: time7Day.valueOf(),
                connected_at: userBotExit.connected_at,
            }),
            this.botTradingHistoryRepository.getProfit({
                user_id,
                bot_id,
                to: time30Day.valueOf(),
                connected_at: userBotExit.connected_at,
            }),
            this.botTradingHistoryRepository.getProfit({
                user_id,
                bot_id,
                to: timeToDay.valueOf(),
                connected_at: userBotExit.connected_at,
            }),
            this.botTradingHistoryRepository.getProfit({
                user_id,
                bot_id,
                connected_at: userBotExit.connected_at,
            }),
            this.botTradingHistoryRepository.count({
                user_id,
                bot_id,
                connected_at: userBotExit.connected_at,
            }),
            this.accountBalanceRepository.getBalance({
                user_id,
                bot_id,
                order: 'desc',
            }),
        ]);
        const balance_init = balanceInit[0] ? balanceInit[0].total : 0;
        const pnl_30_day_init = (Number((_a = profit30Day[0]) === null || _a === void 0 ? void 0 : _a.total) || 0) + balance_init;
        const pnl_7_day_init = (Number((_b = profit7Day[0]) === null || _b === void 0 ? void 0 : _b.total) || 0) + balance_init;
        const pnl_day_init = (Number((_c = profitToday[0]) === null || _c === void 0 ? void 0 : _c.total) || 0) + balance_init;
        const balance_current = (Number((_d = totalProfit[0]) === null || _d === void 0 ? void 0 : _d.total) || 0) + balance_init;
        bot.balance = balanceLastet[0] ? balanceLastet[0].balance : 0;
        return Object.assign(Object.assign({}, bot), { pnl_day_current: balance_current, pnl_day_init, pnl_7_day_current: balance_current, pnl_7_day_init, pnl_30_day_current: balance_current, pnl_30_day_init,
            balance_init,
            balance_current, total_trade: totalTrade, expires_at: userBotExit.expires_at, user_status: userBotExit.status, broker: userBotExit.broker, broker_server: userBotExit.broker_server, broker_account: userBotExit.broker_account, min_balance: minBalance });
    }
};
UserGetBotTradingHandler = __decorate([
    __param(0, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(1, common_1.Inject(account_balance_repository_1.AccountBalanceRepository)),
    __param(2, common_1.Inject(bot_trading_history_repository_1.BotTradingHistoryRepository)),
    __param(3, common_1.Inject(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [bot_trading_repository_1.BotTradingRepository,
        account_balance_repository_1.AccountBalanceRepository,
        bot_trading_history_repository_1.BotTradingHistoryRepository,
        user_repository_1.UserRepository])
], UserGetBotTradingHandler);
exports.UserGetBotTradingHandler = UserGetBotTradingHandler;
//# sourceMappingURL=index.js.map