import { AccountBalanceRepository } from 'src/repositories/account-balance.repository';
import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository';
import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { UserRepository } from 'src/repositories/user.repository';
export declare class UserGetBotTradingHandler {
    private botTradingRepository;
    private accountBalanceRepository;
    private botTradingHistoryRepository;
    private userRepository;
    constructor(botTradingRepository: BotTradingRepository, accountBalanceRepository: AccountBalanceRepository, botTradingHistoryRepository: BotTradingHistoryRepository, userRepository: UserRepository);
    execute(bot_id: string, user_id: string): Promise<any>;
}
