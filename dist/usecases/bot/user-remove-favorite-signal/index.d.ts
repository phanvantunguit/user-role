import { BotSignalRepository } from 'src/repositories/bot-signal.repository';
export declare class UserRemoveFavoriteSignalHandler {
    private botSignalRepository;
    constructor(botSignalRepository: BotSignalRepository);
    execute(user_id: string, bot_signal_id: string): Promise<any>;
}
