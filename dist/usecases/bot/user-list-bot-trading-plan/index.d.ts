import { BotTradingRepository } from 'src/repositories/bot-trading.repository';
import { EventStoreRepository } from 'src/repositories/event-store.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { UserGetBotTradingHandler } from '../user-get-bot-trading';
import { BotDataValidate } from '../user-list-bot/validate';
export declare class UserListBotTradingPlanHandler {
    private botTradingRepository;
    private userRepository;
    private userGetBotTradingHandler;
    private eventStoreRepository;
    constructor(botTradingRepository: BotTradingRepository, userRepository: UserRepository, userGetBotTradingHandler: UserGetBotTradingHandler, eventStoreRepository: EventStoreRepository);
    execute(user_id: string, status?: string): Promise<BotDataValidate[]>;
}
