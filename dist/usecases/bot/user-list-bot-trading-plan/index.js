"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListBotTradingPlanHandler = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("../../../const");
const app_setting_1 = require("../../../const/app-setting");
const transaction_1 = require("../../../const/transaction");
const account_balance_repository_1 = require("../../../repositories/account-balance.repository");
const bot_trading_repository_1 = require("../../../repositories/bot-trading.repository");
const event_store_repository_1 = require("../../../repositories/event-store.repository");
const user_repository_1 = require("../../../repositories/user.repository");
const user_get_bot_trading_1 = require("../user-get-bot-trading");
let UserListBotTradingPlanHandler = class UserListBotTradingPlanHandler {
    constructor(botTradingRepository, userRepository, userGetBotTradingHandler, eventStoreRepository) {
        this.botTradingRepository = botTradingRepository;
        this.userRepository = userRepository;
        this.userGetBotTradingHandler = userGetBotTradingHandler;
        this.eventStoreRepository = eventStoreRepository;
    }
    async execute(user_id, status) {
        var _a, _b;
        let userBots = await this.userRepository.findUserBotTradingSQL({
            user_id,
            status: status ? status.split(',') : null,
        });
        const userBotIdDuplicate = [];
        userBots = userBots.filter((ub) => {
            if (!userBotIdDuplicate.includes(ub.bot_id)) {
                userBotIdDuplicate.push(ub.bot_id);
                return true;
            }
            return false;
        });
        const bots = await this.botTradingRepository.findByIds(userBotIdDuplicate);
        const events = await this.eventStoreRepository.find({
            user_id: user_id,
            event_name: app_setting_1.APP_SETTING.TBOT_INACTIVE_BY_SYSTEM,
            state: app_setting_1.EVENT_STORE_STATE.OPEN,
        });
        const eventByBot = {};
        events.forEach((e) => {
            var _a;
            if ((_a = e.metadata) === null || _a === void 0 ? void 0 : _a.bot_id) {
                if (!eventByBot[e.metadata.bot_id]) {
                    eventByBot[e.metadata.bot_id] = {
                        count: 1,
                        created_at: e.created_at,
                    };
                }
                else {
                    eventByBot[e.metadata.bot_id].count += 1;
                    if (eventByBot[e.metadata.bot_id].created_at < e.created_at) {
                        eventByBot[e.metadata.bot_id].created_at = e.created_at;
                    }
                }
            }
        });
        const mapBots = [];
        for (let b of bots) {
            const userBotExit = userBots.find((ub) => ub.bot_id === b.id);
            if (userBotExit) {
                const ubData = await this.userGetBotTradingHandler.execute(userBotExit.bot_id, userBotExit.user_id);
                mapBots.push(Object.assign(Object.assign({}, ubData), { count_inactive_by_system: ((_a = eventByBot[b.id]) === null || _a === void 0 ? void 0 : _a.count) || 0, inactive_by_system_at: (_b = eventByBot[b.id]) === null || _b === void 0 ? void 0 : _b.created_at }));
            }
            else {
                mapBots.push(b);
            }
        }
        return mapBots;
    }
};
UserListBotTradingPlanHandler = __decorate([
    __param(0, common_1.Inject(bot_trading_repository_1.BotTradingRepository)),
    __param(1, common_1.Inject(user_repository_1.UserRepository)),
    __param(2, common_1.Inject(user_get_bot_trading_1.UserGetBotTradingHandler)),
    __param(3, common_1.Inject(event_store_repository_1.EventStoreRepository)),
    __metadata("design:paramtypes", [bot_trading_repository_1.BotTradingRepository,
        user_repository_1.UserRepository,
        user_get_bot_trading_1.UserGetBotTradingHandler,
        event_store_repository_1.EventStoreRepository])
], UserListBotTradingPlanHandler);
exports.UserListBotTradingPlanHandler = UserListBotTradingPlanHandler;
//# sourceMappingURL=index.js.map