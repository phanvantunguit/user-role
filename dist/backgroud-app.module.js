"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BackgroudAppModule = void 0;
const common_1 = require("@nestjs/common");
const event_emitter_1 = require("@nestjs/event-emitter");
const schedule_1 = require("@nestjs/schedule");
const backgroud_jobs_1 = require("./backgroud-jobs");
const bot_trading_job_1 = require("./backgroud-jobs/bot-trading.job");
const transaction_job_1 = require("./backgroud-jobs/transaction.job");
const transaction_event_1 = require("./events/transaction.event");
const repositories_1 = require("./repositories");
const coinpayment_1 = require("./resources/coinpayment");
const coinpayment_transport_1 = require("./resources/coinpayment/coinpayment.transport");
const forex_1 = require("./resources/forex");
const mail_1 = require("./resources/mail");
const sendgrid_transport_1 = require("./resources/mail/sendgrid.transport");
const bot_trading_1 = require("./services/bot-trading");
const bot_trading_history_1 = require("./services/bot-trading-history");
const payment_1 = require("./services/payment");
const coinpayment_2 = require("./services/payment/coinpayment");
const transaction_1 = require("./services/transaction");
const transaction_log_1 = require("./services/transaction-log");
let BackgroudAppModule = class BackgroudAppModule {
};
BackgroudAppModule = __decorate([
    common_1.Module({
        imports: [repositories_1.DatabaseModule, event_emitter_1.EventEmitterModule.forRoot()],
        providers: [
            backgroud_jobs_1.BackgroudJob,
            transaction_job_1.TransactionJob,
            schedule_1.SchedulerRegistry,
            transaction_1.TransactionService,
            payment_1.PaymentService,
            transaction_log_1.TransactionLogService,
            coinpayment_2.CoinpaymentService,
            coinpayment_1.CoinpaymentResource,
            coinpayment_transport_1.CoinpaymentTransport,
            transaction_event_1.TransactionEvent,
            mail_1.MailResource,
            sendgrid_transport_1.SendGridTransport,
            bot_trading_job_1.BotTradingJob,
            forex_1.MetaapiResource,
            bot_trading_1.BotTradingService,
            bot_trading_history_1.BotTradingHistoryService,
        ],
    })
], BackgroudAppModule);
exports.BackgroudAppModule = BackgroudAppModule;
//# sourceMappingURL=backgroud-app.module.js.map