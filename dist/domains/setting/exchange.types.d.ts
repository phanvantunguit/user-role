export interface ExchangeDomain {
    listExchange(): Promise<Exchange[]>;
    getExchange(params: QueryExchange): Promise<Exchange>;
    createExchange(params: Exchange[]): Promise<any>;
    updateExchange(params: Exchange): Promise<any>;
    deleteExchange(ids: string[]): Promise<any>;
}
export declare type Exchange = {
    exchange_name: string;
    exchange_desc: string;
};
export declare type QueryExchange = {
    exchange_name?: string;
};
export declare type RawExchange = Exchange & {
    created_at?: number;
};
