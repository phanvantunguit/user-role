import { BaseDomain } from '../base/types';
export declare type BotSettingDomain = BaseDomain<QueryBotSetting, RawBotSetting>;
export declare type QueryBotSetting = {
    id?: string;
    name?: string;
    status?: boolean;
};
export declare type RawBotSetting = {
    id?: string;
    name?: string;
    params?: any;
    owner_created?: string;
    status?: boolean;
    created_at?: number;
    updated_at?: number;
};
