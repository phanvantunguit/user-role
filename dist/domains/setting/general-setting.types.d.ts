export interface GeneralSettingDomain {
    listGeneralSetting(): Promise<RawGeneralSetting[]>;
    getGeneralSetting(params: QueryGeneralSetting): Promise<RawGeneralSetting>;
    createGeneralSetting(params: GeneralSettingCreate): Promise<any>;
    updateGeneralSetting(params: GeneralSettingUpdate): Promise<any>;
    deleteGeneralSetting(general_setting_ids: string[]): Promise<any>;
}
export declare type GeneralSetting = {
    general_setting_id: string;
    general_setting_name: string;
    description?: string;
};
export declare type GeneralSettingCreate = {
    general_settings: GeneralSetting[];
    owner_created: string;
};
export declare type GeneralSettingUpdate = GeneralSetting & {
    owner_created: string;
};
export declare type QueryGeneralSetting = {
    general_setting_id?: string;
    general_setting_name?: string;
};
export declare type RawGeneralSetting = GeneralSetting & {
    owner_created: string;
    created_at?: number;
    updated_at?: number;
};
