export interface FeatureDomain {
    getFeature(): Promise<RawFeature[]>;
    getFeatureDetail(params: QueryFeature): Promise<RawFeature>;
    createFeature(params: FeatureCreate): Promise<any>;
    updateFeature(params: FeatureUpdate): Promise<any>;
    deleteFeature(feature_ids: string[]): Promise<any>;
}
export declare type Feature = {
    feature_id: string;
    feature_name: string;
    description?: string;
    action?: string;
};
export declare type FeatureCreate = {
    owner_created: string;
    features: Feature[];
};
export declare type FeatureUpdate = Feature & {
    owner_created: string;
};
export declare type RawFeature = Feature & {
    owner_created: string;
    created_at?: number;
    updated_at?: number;
};
export declare type QueryFeature = {
    feature_id?: string;
    feature_name?: string;
};
