export interface AppSettingDomain {
    listAppSetting(params: QueryAppSetting): Promise<RawAppSetting[]>;
    getAppSetting(params: QueryAppSetting): Promise<RawAppSetting>;
    createAppSetting(params: AppSetting): Promise<any>;
    updateAppSetting(params: AppSettingUpdate): Promise<any>;
    deleteAppSetting(id: string): Promise<any>;
}
export declare type AppSetting = {
    name?: string;
    value?: string;
    description?: string;
    owner_created?: string;
};
export declare type AppSettingUpdate = AppSetting & {
    id: string;
};
export declare type QueryAppSetting = {
    id?: string;
    name?: string;
    user_id?: string;
};
export declare type RawAppSetting = AppSetting & {
    id?: string;
    user_id?: string;
    created_at?: number;
    updated_at?: number;
};
export declare type BrokerSettings = {
    [code: string]: BrokerSetting;
};
export declare type BrokerSetting = {
    name: string;
    code: string;
    broker_timezone: string;
    broker_dst_switch_timezone: string;
    check_referral_broker: boolean;
    required_profile: boolean;
    servers: string[];
    referral_setting: {
        key: string;
        name: string;
        type: string;
    }[];
};
