export interface ResolutionDomain {
    listResolution(): Promise<RawResolution[]>;
    getResolution(params: QueryResolution): Promise<RawResolution>;
    createResolution(params: Resolution[]): Promise<RawResolution[]>;
    deleteResolution(ids: string[]): Promise<any>;
    updateResolution(params: ResolutionUpdate): Promise<RawResolution>;
}
export declare type Resolution = {
    resolutions_name: string;
    display_name: string;
};
export declare type ResolutionUpdate = Resolution & {
    id: any;
};
export declare type QueryResolution = {
    id?: string;
    resolutions_name?: string;
    display_name?: string;
};
export declare type RawResolution = Resolution & {
    id?: string;
    created_at?: number;
};
