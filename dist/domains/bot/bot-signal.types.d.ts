import { BaseDomain } from '../base/types';
export declare type BotSignalDomain = BaseDomain<QueryBotSignal, RawBotSignal>;
export declare type QueryBotSignal = {
    id?: string;
    name?: string;
    signal_id?: string;
    time?: number;
    type?: string;
    exchange?: string;
    symbol?: string;
};
export declare type RawBotSignal = {
    id?: string;
    name?: string;
    resolution?: string;
    signal_id?: string;
    time?: number;
    type?: string;
    image_url?: string;
    exchange?: string;
    symbol?: string;
    metadata?: any;
    created_at?: number;
};
