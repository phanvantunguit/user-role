export interface AuthRoleDomain {
    createAuthRole(params: AuthRoleCreate): Promise<RawAuthRole>;
    updateAuthRole(params: AuthRoleUpdate): Promise<RawAuthRole>;
    deleteAuthRole(ids: string[]): Promise<any>;
    getListPermission(): Permission[];
    getListAuthUserRole(params: ListAuthRole): Promise<RawAuthRole[]>;
    getAuthUserRole(params: QueryAuthRole): Promise<RawAuthRole>;
}
export declare type Permission = {
    permission_id: string;
    permission_name: string;
    description: string;
};
declare type Root = {
    permissions: Permission[];
};
export declare type AuthRole = {
    role_name?: string;
    root?: Root;
    description?: string;
    owner_created?: string;
};
export declare type AuthRoleUpdate = {
    id: string;
    permission_ids: string[];
    role_name: string;
    owner_created: string;
    description?: string;
};
export declare type AuthRoleCreate = {
    permission_ids: string[];
    role_name: string;
    owner_created: string;
    description?: string;
};
export declare type QueryAuthRole = {
    id?: string;
    role_name?: string;
};
export declare type ListAuthRole = {
    user_id?: string;
};
export declare type RawAuthRole = AuthRole & {
    id?: string;
    created_at?: number;
    updated_at?: number;
};
export {};
