export interface FeatureRoleDomain {
    modifyFeatureRole(params: FeatureRoleCreate): Promise<any>;
}
declare type Feature = {
    feature_id: string;
    description?: string;
};
export declare type FeatureRoleCreate = {
    role_id: string;
    features: Feature[];
    owner_created: string;
};
export declare type FeatureRole = {
    role_id: string;
    feature_id: string;
    description?: string;
    owner_created: string;
};
export declare type FeatureRoleUpdate = FeatureRole & {
    id: string;
};
export declare type RawFeatureRole = FeatureRole & {
    id?: string;
    created_at?: number;
    updated_at?: number;
};
export declare type QueryFeatureRole = {
    role_id?: string;
};
export {};
