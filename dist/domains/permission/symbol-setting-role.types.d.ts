export interface SymbolSettingRoleDomain {
    modifySymbolSettingRole(params: SymbolSettingRoleCreate): Promise<any>;
    createSymbolSettingRole(params: SymbolSettingRoleCreate): Promise<any>;
    updateSymbolSettingRole(params: SymbolSettingRoleUpdate): Promise<any>;
    deleteSymbolSettingRole(params: SymbolSettingRoleDelete): Promise<any>;
}
export declare type SymbolSettingRole = {
    description?: string;
    list_exchanged?: string[];
    list_symbol?: string[];
    supported_resolutions?: string[];
    role_id: string;
};
export declare type SymbolSettingRoleCreate = SymbolSettingRole & {
    owner_created: string;
};
export declare type SymbolSettingRoleUpdate = SymbolSettingRole & {
    id: string;
    owner_created: string;
};
export declare type SymbolSettingRoleDelete = {
    id: string;
    role_id: string;
};
export declare type QuerySymbolSettingRole = {
    id?: string;
    role_id?: string;
};
export declare type RawSymbolSettingRole = SymbolSettingRole & {
    id?: string;
    created_at?: number;
    updated_at?: number;
};
