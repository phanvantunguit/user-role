export interface GeneralSettingRoleDomain {
    modifyGeneralSettingRole(params: GeneralSettingRoleCreate): Promise<any>;
    getMaxLimitTab(roleIds: string[]): Promise<number>;
}
export declare type GeneralSettingRole = {
    general_setting_id: string;
    description?: string;
    val_limit: number;
};
export declare type GeneralSettingRoleCreate = {
    role_id: string;
    owner_created: string;
    general_settings: GeneralSettingRole[];
};
export declare type GeneralSettingRoleUpdate = GeneralSettingRole & {
    id: string;
    role_id: string;
    owner_created: string;
};
export declare type RawGeneralSettingRole = GeneralSettingRole & {
    id?: string;
    role_id: string;
    owner_created: string;
    created_at?: number;
    updated_at?: number;
};
export declare type QueryGeneralSettingRole = {
    role_id?: string;
};
