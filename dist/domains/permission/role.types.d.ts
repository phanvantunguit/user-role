import { RawFeature, RawGeneralSetting, RawSymbol, RawExchange, RawResolution } from '../setting/';
export interface RoleDomain {
    createRole(params: RoleCreate): Promise<any>;
    updateRole(params: RoleUpdate): Promise<any>;
    deleteRole(id: string): Promise<any>;
    updateOrderRole(params: RoleOrderUpdate): Promise<any[]>;
    listRoleForUser(userId: string, package_id?: string): Promise<any>;
    listRole(params: ListRole): Promise<any>;
    getRoleDetail(id: string): Promise<any>;
}
declare type SymbolSettingRole = {
    id: string;
    description?: string;
    symbols?: RawSymbol[];
    exchanges?: RawExchange[];
    resolutions?: RawResolution[];
};
declare type Root = {
    features?: RawFeature[];
    general_settings?: RawGeneralSetting[];
    symbol_settings_roles?: SymbolSettingRole[];
};
export declare type Role = {
    role_name?: string;
    root?: Root;
    description?: string;
    status?: string;
    type?: string;
    price?: string;
    currency?: string;
    parent_id?: string;
    is_best_choice?: boolean;
    order?: number;
    description_features?: {
        features: string[];
    };
    color?: string;
};
export declare type RoleCreate = {
    role_name: string;
    owner_created: string;
    description?: string;
    status: string;
    type: string;
    price: string;
    currency: string;
    parent_id?: string;
    is_best_choice?: boolean;
    order?: number;
    description_features?: string[];
    color?: string;
};
export declare type RoleUpdate = RoleCreate & {
    id: string;
};
export declare type RoleOrderUpdate = {
    roles: {
        id: string;
        order: number;
    }[];
    owner_created: string;
};
export declare type QueryRole = {
    id?: string;
    role_name?: string;
};
export declare type ListRole = {
    user_id?: string;
};
export declare type RawRole = Role & {
    id?: string;
    owner_created: string;
    created_at?: number;
    updated_at?: number;
};
export {};
