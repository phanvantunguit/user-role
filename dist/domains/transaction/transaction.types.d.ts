import { PAYMENT_METHOD, TRANSACTION_STATUS } from 'src/const/transaction';
import { RawTransactionDetail } from '.';
import { PartnerResCreateTransaction } from '../payment/payment.types';
import { RawUserRole } from '../user';
export interface TransactionDomain {
    createTransaction(parmas: CreateTransaction): Promise<ResCreateTransaction>;
    ipnTransaction(method: PAYMENT_METHOD, params: any): any;
    listTransaction(parmas: QueryTransaction): Promise<RawTransactionView[]>;
    getTransaction(parmas: QueryTransaction): Promise<RawTransactionView>;
    getTransactionPagination(params: QueryTransactionPagination): Promise<any>;
    handlePaymentComplete(transaction: RawTransaction): any;
    handlePaymentFailed(transaction: RawTransaction): any;
    checkCreateTransactionValid(user_id: string, role_id: string, package_id: string, buy_currency: string): Promise<any>;
    upgradeFreeTrial(user_id: string, role_id: string): Promise<RawUserRole>;
    checkPartnerTransaction(transaction: RawTransaction): Promise<ResCheckTransaction>;
}
export interface ResCheckTransaction {
    transaction_status: string;
    transaction_event: string;
    metadata: any;
}
export declare type CreateTransaction = {
    user_id: string;
    payment_method: PAYMENT_METHOD;
    description?: string;
    role_id: string;
    package_id: string;
    buy_currency: string;
    buyer_email: string;
};
export declare type CreateTransactionChild = {
    parent_id: string;
    amount?: number;
    description: string;
    owner_created: string;
};
export declare type ResCreateTransaction = PartnerResCreateTransaction & {
    transaction_id: string;
    buy_currency: string;
    sell_amount: number;
    sell_currency: string;
    details: RawTransactionDetail[];
    created_at: number;
};
export declare type QueryTransactionPagination = {
    page?: number;
    size?: number;
    keyword?: string;
    status?: TRANSACTION_STATUS;
    from?: number;
    to?: number;
};
export declare type QueryTransaction = {
    payment_id?: string;
    user_id?: string;
    id?: string;
    status?: TRANSACTION_STATUS;
    wallet_address?: string;
};
export declare type RawTransaction = {
    id?: string;
    user_id: string;
    payment_id?: string;
    payment_method: PAYMENT_METHOD;
    description?: string;
    status: TRANSACTION_STATUS;
    sell_amount: number;
    sell_currency: string;
    buy_currency: string;
    buy_amount?: string;
    parent_id?: string;
    wallet_address?: string;
    created_at?: number;
    updated_at?: number;
};
export declare type RawTransactionView = {
    id: string;
    user_id: string;
    payment_id: string;
    payment_method: PAYMENT_METHOD;
    description: string;
    status: TRANSACTION_STATUS;
    sell_amount: number;
    sell_currency: string;
    buy_currency: string;
    email: string;
    username: string;
    phone: string;
    first_name: string;
    last_name: string;
    buy_amount: string;
    wallet_address: string;
    details: any;
    created_at: number;
    updated_at: number;
};
