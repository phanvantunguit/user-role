import { USER_TYPE } from 'src/const';
import { RawAuthRole, RawRole } from '../permission';
export interface UserDomain {
    registerUser(params: UserRegister): Promise<any>;
    verifyEmailUser(token: string): Promise<any>;
    loginUser(params: UserLogin, userAgent: string, ipAddress: string): Promise<UserLoginResponse>;
    resendEmailVerify(params: EmailSend): Promise<any>;
    sendEmailForgotPassword(params: EmailSend): Promise<any>;
    resetPassword(params: UserResetPassword): Promise<any>;
    updateInfo(params: UserUpdateInfo, isAdmin: boolean): Promise<any>;
    createUser(params: UserCreate): Promise<any>;
    getUserPagination(params: QueryUserPagination): Promise<any>;
    getUser(params: QueryUser): Promise<any>;
    getProfile(params: QueryUser): Promise<any>;
}
export declare type UserRegister = {
    email: string;
    password: string;
    first_name: string;
    last_name: string;
    merchant_code: string;
};
export declare type UserCreate = {
    email: string;
    username?: string;
    phone?: string;
    first_name?: string;
    last_name?: string;
    address?: string;
    affiliate_code?: string;
    link_affiliate?: string;
    referral_code?: string;
    profile_pic?: string;
    note_updated?: string;
    owner_created: string;
    is_admin?: boolean;
    country?: string;
    year_of_birth?: string;
    gender?: string;
    merchant_code: string;
};
export declare type ChangeEmail = {
    user_id: string;
    email: string;
    callback_url: string;
};
export declare type ChangeEmailVerify = {
    user_id: string;
    code: string;
};
export declare type EmailSend = {
    email: string;
    merchant_code: string;
};
export declare type UserLogin = {
    user_type: USER_TYPE;
    email: string;
    password: string;
};
export declare type UserLoginResponse = {
    token: string;
};
export declare type UserResetPassword = {
    token: string;
    password: string;
};
export declare type UserUpdateInfo = {
    id: string;
    username?: string;
    phone?: string;
    first_name?: string;
    last_name?: string;
    address?: string;
    affiliate_code?: string;
    link_affiliate?: string;
    referral_code?: string;
    profile_pic?: string;
    note_updated?: string;
    active?: boolean;
    is_admin?: boolean;
    password?: string;
    old_password?: string;
    country?: string;
    year_of_birth?: string;
    gender?: string;
    phone_code?: string;
    merchant_code?: string;
};
export declare type QueryUser = {
    id?: string;
    email?: string;
    is_admin?: boolean;
    username?: string;
    country?: string;
    year_of_birth?: string;
    gender?: string;
    merchant_code?: string;
};
export declare type RawRoleView = RawRole & {
    expires_at: number;
    quantity: number;
    package_name: string;
    package_type: string;
    package_id: string;
};
export declare type RawUserView = {
    id: string;
    email?: string;
    username?: string;
    phone?: string;
    first_name?: string;
    last_name?: string;
    address?: string;
    affiliate_code?: string;
    link_affiliate?: string;
    referral_code?: string;
    profile_pic?: string;
    active?: boolean;
    email_confirmed?: boolean;
    note_updated?: string;
    date_registered?: string;
    super_user?: boolean;
    is_admin?: boolean;
    roles?: RawRoleView[];
    auth_roles?: RawAuthRole[];
    country?: string;
    year_of_birth?: string;
    gender?: string;
    phone_code?: string;
    created_at?: number;
    updated_at?: number;
};
export declare type User = {
    email?: string;
    username?: string;
    password?: string;
    phone?: string;
    first_name?: string;
    last_name?: string;
    address?: string;
    affiliate_code?: string;
    link_affiliate?: string;
    referral_code?: string;
    profile_pic?: string;
    active?: boolean;
    email_confirmed?: boolean;
    note_updated?: string;
    date_registered?: string;
    super_user?: boolean;
    is_admin?: boolean;
    country?: string;
    year_of_birth?: string;
    gender?: string;
    phone_code?: string;
    merchant_code?: string;
};
export declare type RawUser = User & {
    id?: string;
    created_at?: number;
    updated_at?: number;
};
export declare type QueryUserPagination = {
    is_admin?: boolean;
    page?: number;
    size?: number;
    keyword?: string;
    role_ids?: string;
    roles?: string;
    bots?: string;
    merchant_code?: string;
};
