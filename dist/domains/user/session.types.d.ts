import { IToken } from 'src/const/authorization';
export interface SessionDomain {
    save(params: RawSession): Promise<RawSession>;
    verifySession(token: IToken): Promise<boolean>;
}
export declare type Session = {
    user_id?: string;
    token?: string;
    name_device?: string;
    browser?: string;
    ip_number?: string;
    last_login?: number;
    expires_at?: number;
    enabled?: boolean;
    websocket_id?: string;
    token_id?: string;
};
export declare type QuerySession = {
    user_id?: string;
    enabled?: boolean;
    token?: string;
    token_id?: string;
    websocket_id?: string;
    id?: any;
};
export declare type RawSession = Session & {
    id?: string;
    created_at?: number;
};
