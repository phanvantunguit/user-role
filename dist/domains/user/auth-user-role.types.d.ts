export interface AuthUserRoleDomain {
    modifyAuthUserRole(params: AuthUserRoleModify): Promise<any>;
}
export declare type AuthUserRole = {
    auth_role_id?: string;
    description?: string;
};
export declare type AuthUserRoleModify = {
    user_id: string;
    owner_created: string;
    auth_roles: AuthUserRole[];
};
export declare type QueryAuthUserRole = {
    user_id?: string;
};
export declare type RawAuthUserRole = AuthUserRole & {
    id?: string;
    owner_created?: string;
    created_at?: number;
    updated_at?: number;
};
