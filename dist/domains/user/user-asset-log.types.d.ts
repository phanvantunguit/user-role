import { ITEM_STATUS, ORDER_CATEGORY } from 'src/const/transaction';
export declare type QueryUserAssetLog = {
    id?: string;
    order_id?: string;
    asset_id?: string;
    user_id?: string;
    status?: ITEM_STATUS;
    category?: ORDER_CATEGORY;
    name?: string;
};
export declare type RawUserAssetLog = {
    id?: string;
    asset_id?: string;
    user_id?: string;
    order_id?: string;
    category?: ORDER_CATEGORY;
    expires_at?: number;
    status?: ITEM_STATUS;
    package_type?: string;
    quantity?: number;
    name?: string;
    price?: string;
    discount_amount?: number;
    discount_rate?: number;
    owner_created?: string;
    created_at?: number;
    updated_at?: number;
    metadata?: any;
};
