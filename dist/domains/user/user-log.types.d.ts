export declare type UserLog = {
    user_id: string;
    ip_number: string;
    browser_type: string;
};
export declare type QueryUserLog = {
    user_id?: string;
};
export declare type RawUserLog = UserLog & {
    id?: string;
    created_at?: number;
};
