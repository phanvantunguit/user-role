export declare enum ENVIRONMENT {
    DEVELOPMENT = "DEVELOPMENT",
    STAGING = "STAGING",
    PRODUCTION = "PRODUCTION"
}
export interface Iconfig {
    PORT: number;
    BASE_URL: string;
    ENVIRONMENT: ENVIRONMENT;
    SECRET_KEY: string;
    TIME_EXPIRED_VERIFY_EMAIL: number;
    TIME_EXPIRED_LOGIN: number;
    TOKEN_PRIVATE_KEY_PATH: string;
    TOKEN_PUBLIC_KEY_PATH: string;
    POSTGRES_HOST: string;
    POSTGRES_PORT: number;
    POSTGRES_USER: string;
    POSTGRES_PASS: string;
    POSTGRES_DB: string;
    PAYMENT_POSTGRES_HOST: string;
    PAYMENT_POSTGRES_PORT: number;
    PAYMENT_POSTGRES_USER: string;
    PAYMENT_POSTGRES_PASS: string;
    PAYMENT_POSTGRES_DB: string;
    SENDGRID_BASE_URL: string;
    SENDGRID_API_TOKEN: string;
    SENDGRID_SENDER_EMAIL: string;
    SENDGRID_SENDER_NAME: string;
    SENDGRID_CC_EMAIL: string;
    SENDGRID_SENDER_ALGO_EMAIL: string;
    SENDGRID_SENDER_ALGO_NAME: string;
    DASHBOARD_CLIENT_BASE_URL: string;
    DASHBOARD_ADMIN_BASE_URL: string;
    COINPAYMENT_BASE_URL: string;
    COINPAYMENT_PUBLIC_KEY: string;
    COINPAYMENT_PRIVATE_KEY: string;
    CMPAYMENT_BASE_URL: string;
    GOOGLE_CLOUD_KEYFILE: string;
    GOOGLE_CLOUD_PROJECT_ID: string;
    GOOGLE_CLOUD_STORAGE_BUCKET: string;
    REDIS_HOST: string;
    REDIS_PORT: number;
    REDIS_PASS: string;
    MT_TOKEN: string;
    MT_URL: string;
    VERSION: string;
    AXI_BASE_URL: string;
    NUMBER_OF_USER_ACTIVE_TBOT: number;
    ALGO_LOGO_EMAIL: string;
    ALGO_BANNER_EMAIL: string;
    TIME_BLOCKED_TBOT: number;
    NUMBER_OF_TIMES_ALLOW_USER_ACTIVATE_TBOT: number;
}
