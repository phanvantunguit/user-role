"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.APP_CONFIG = void 0;
const dotenv = require("dotenv");
const fs = require("fs");
const const_1 = require("../const");
const types_1 = require("./types");
dotenv.config();
const envConfig = process.env;
let gwEnv = '';
let clientEnv = '';
let ccEmail = 'tuongnguyen@coinmap.tech';
let timeBlockedTBot = 3 * const_1.DAY;
switch (process.env.ENVIRONMENT) {
    case types_1.ENVIRONMENT.DEVELOPMENT:
        gwEnv = '-dev';
        clientEnv = 'dev.';
        ccEmail = 'trongnguyen@coinmap.tech';
        timeBlockedTBot = 1 * const_1.MINUTE;
        break;
    case types_1.ENVIRONMENT.STAGING:
        gwEnv = '-staging';
        clientEnv = 'staging.';
        ccEmail = 'trongnguyen@coinmap.tech';
        timeBlockedTBot = 1 * const_1.MINUTE;
        break;
}
exports.APP_CONFIG = Object.assign(Object.assign({ PAYMENT_POSTGRES_HOST: 'postgresql-ha-pgpool.db', PAYMENT_POSTGRES_PORT: 5432, PAYMENT_POSTGRES_USER: 'cm_postgresql_dev', PAYMENT_POSTGRES_PASS: 'D3GaosGQPoZp1lk1EaS0OwQTj', PAYMENT_POSTGRES_DB: 'cm_payment_service', AXI_BASE_URL: 'https://api3.axi.com', XM_BASE_URL: 'https://mypartners.xm.com', MT_URL: 'https://mt-manager-api-v1.new-york.agiliumtrade.ai', SENDGRID_SENDER_ALGO_EMAIL: 'hello@algotradingmarket.com', SENDGRID_SENDER_ALGO_NAME: 'ATM', ALGO_LOGO_EMAIL: 'https://static-dev.cextrading.io/images/cm-user-roles/1683703900434.png', ALGO_BANNER_EMAIL: 'https://static-dev.cextrading.io/images/cm-user-roles/1683703868125.jpg', SENDGRID_CC_EMAIL: ccEmail }, envConfig), { CMPAYMENT_BASE_URL: `https://gw${gwEnv}.cextrading.io/cm-payment-service`, DASHBOARD_CLIENT_BASE_URL: `https://${clientEnv}coinmap.tech`, TOKEN_PRIVATE_KEY: fs.readFileSync(envConfig.TOKEN_PRIVATE_KEY_PATH, 'utf8'), TOKEN_PUBLIC_KEY: fs.readFileSync(envConfig.TOKEN_PUBLIC_KEY_PATH, 'utf8'), TIME_EXPIRED_LOGIN: 2 * 24 * 60 * 60, NUMBER_OF_TIMES_ALLOW_USER_ACTIVATE_TBOT: 2, TIME_BLOCKED_TBOT: timeBlockedTBot });
//# sourceMappingURL=index.js.map