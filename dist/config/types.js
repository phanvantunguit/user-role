"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ENVIRONMENT = void 0;
var ENVIRONMENT;
(function (ENVIRONMENT) {
    ENVIRONMENT["DEVELOPMENT"] = "DEVELOPMENT";
    ENVIRONMENT["STAGING"] = "STAGING";
    ENVIRONMENT["PRODUCTION"] = "PRODUCTION";
})(ENVIRONMENT = exports.ENVIRONMENT || (exports.ENVIRONMENT = {}));
//# sourceMappingURL=types.js.map