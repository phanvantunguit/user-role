import { UserListBotTradingPlanHandler } from 'src/usecases/bot/user-list-bot-trading-plan';
import { WebSocketMessage } from '../types';
export declare class BotTradingInitEvent {
    private userListBotTradingPlanHandler;
    constructor(userListBotTradingPlanHandler: UserListBotTradingPlanHandler);
    execute(param: {
        user_id: string;
        status: string;
        cb: (data: WebSocketMessage, options?: any) => void;
    }): Promise<void>;
}
