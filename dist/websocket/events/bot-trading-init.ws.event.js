"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotTradingInitEvent = void 0;
const common_1 = require("@nestjs/common");
const crypto_1 = require("crypto");
const const_1 = require("../../const");
const bot_1 = require("../../const/bot");
const user_list_bot_trading_plan_1 = require("../../usecases/bot/user-list-bot-trading-plan");
const uuid_1 = require("uuid");
const transaction_1 = require("../../const/transaction");
let BotTradingInitEvent = class BotTradingInitEvent {
    constructor(userListBotTradingPlanHandler) {
        this.userListBotTradingPlanHandler = userListBotTradingPlanHandler;
    }
    async execute(param) {
        const { user_id, status, cb } = param;
        if (user_id) {
            let userBot = await this.userListBotTradingPlanHandler.execute(user_id, status);
            userBot.map((e) => {
                if (e.user_status === transaction_1.ITEM_STATUS.ACTIVE) {
                    setInterval(() => {
                        const rate = crypto_1.randomInt(2) === 1 ? 1 : -1;
                        cb({
                            event: const_1.WS_EVENT.tbot_update,
                            data: {
                                id: e.id,
                                pnl_day_init: 100,
                                pnl_day_current: crypto_1.randomInt(10000),
                                pnl_7_day_init: 500,
                                pnl_7_day_current: crypto_1.randomInt(10000),
                                pnl_30_day_init: 1000,
                                pnl_30_day_current: crypto_1.randomInt(10000),
                                total_trade: crypto_1.randomInt(10000),
                                profit: crypto_1.randomInt(1000) * rate,
                                gain: crypto_1.randomInt(100) * rate,
                                balance_init: 2000,
                                balance_current: crypto_1.randomInt(10000),
                            },
                        });
                        cb({
                            event: const_1.WS_EVENT.tbot_trade_update,
                            data: {
                                id: uuid_1.v4(),
                                user_id: user_id,
                                bot_id: e.id,
                                token_first: 'EURUSD',
                                status: bot_1.TRADE_HISTORY_STATUS.OPEN,
                                side: rate > 0 ? bot_1.TRADE_SIDE.LONG : bot_1.TRADE_SIDE.SHORT,
                                entry_price: crypto_1.randomInt(1000),
                                profit: crypto_1.randomInt(100),
                                day_started: Date.now(),
                                created_at: Date.now(),
                                updated_at: Date.now(),
                            },
                        });
                        const pnl_day_current = crypto_1.randomInt(10000);
                        cb({
                            event: const_1.WS_EVENT.tbot_chart_pnl_update,
                            data: {
                                bot_id: e.id,
                                pnl_day_init: 1000,
                                pnl_day_current,
                                pnl_day_percent: ((pnl_day_current - 1000) / 1000) * 100,
                                created_at: Date.now(),
                                updated_at: Date.now(),
                            },
                        });
                    }, 1000);
                }
            });
        }
    }
};
BotTradingInitEvent = __decorate([
    __param(0, common_1.Inject(user_list_bot_trading_plan_1.UserListBotTradingPlanHandler)),
    __metadata("design:paramtypes", [user_list_bot_trading_plan_1.UserListBotTradingPlanHandler])
], BotTradingInitEvent);
exports.BotTradingInitEvent = BotTradingInitEvent;
//# sourceMappingURL=bot-trading-init.ws.event.js.map