"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebSocketEvent = void 0;
const common_1 = require("@nestjs/common");
const bot_1 = require("../const/bot");
const response_1 = require("../const/response");
const bot_trading_init_ws_event_1 = require("./events/bot-trading-init.ws.event");
let WebSocketEvent = class WebSocketEvent {
    constructor(botTradingInitEvent) {
        this.botTradingInitEvent = botTradingInitEvent;
    }
    request(ws, message) {
        const _this = this;
        const sendMessage = (message, options) => {
            if (ws) {
                if (typeof message === 'object') {
                    ws.send(JSON.stringify(message), options);
                }
                else {
                    ws.send(message, options);
                }
            }
        };
        switch (message.event) {
            case response_1.WS_EVENT.tbot_init:
                console.log('ws.user_id', ws.user_id);
                this.botTradingInitEvent.execute(Object.assign(Object.assign({ user_id: ws.user_id }, message.data), { cb: sendMessage }));
                break;
            default:
                break;
        }
    }
    subscribeWSChanel(message) {
        const _this = this;
        switch (message.event) {
            case response_1.WS_EVENT.app_message:
                break;
            default:
                break;
        }
    }
    subscribeTradingBotChanel(message) {
        switch (message.event) {
            case bot_1.TBOT_CHANEL_TS_EVENT.account:
                break;
            case bot_1.TBOT_CHANEL_TS_EVENT.trades:
                break;
            default:
                break;
        }
    }
};
WebSocketEvent = __decorate([
    __param(0, common_1.Inject(bot_trading_init_ws_event_1.BotTradingInitEvent)),
    __metadata("design:paramtypes", [bot_trading_init_ws_event_1.BotTradingInitEvent])
], WebSocketEvent);
exports.WebSocketEvent = WebSocketEvent;
//# sourceMappingURL=websocket.event.js.map