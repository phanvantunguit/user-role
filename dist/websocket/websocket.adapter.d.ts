import { INestApplication } from '@nestjs/common';
import { SessionService } from 'src/services/session';
import { UserRepository } from 'src/repositories/user.repository';
import { CacheRepository } from 'src/repositories/cache.repository';
import { GeneralSettingRoleService } from 'src/services/general-setting-role';
import { BotSignalService } from 'src/services/bot-signal';
import { BotRepository } from 'src/repositories/bot.repository';
import { SettingRepository } from 'src/repositories/setting.repository';
import { WebSocketEvent } from './websocket.event';
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository';
import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository';
import { EventStoreRepository } from 'src/repositories/event-store.repository';
import { BotTradingService } from 'src/services/bot-trading';
import { UserGetBotTradingHandler } from 'src/usecases/bot/user-get-bot-trading';
import { DeactivateUserBotTradingHandler } from 'src/usecases/bot/deactive-user-bot-trading';
interface IVerify {
    active_client_ids: string[];
    user_id: string;
    token_id: string;
    client_id: string;
    verify_id: string;
}
interface ICheckBrowser {
    client_id: string;
    user_id: string;
    token_id: string;
    verify_id: string;
}
interface IAppSendMessage {
    user_id?: string;
    event: string;
    data: any;
}
interface IBotSignalMessage {
    e: 'signal';
    exchange: 'BINANCE';
    symbol: 'LTCUSDT';
    name: 'botBoxCandle';
    resolution: '5m';
    signal_id: 'fad731ed_1673254427814';
    type: 'BUY';
    time: 1671527700000;
    image_url: 'https://static.cextrading.io/images/Bot_Box/Bot_Box_LTCUSDT_5m_1671527700000.png';
    metadata: {
        idSignal: 'fad731ed_1673254427814';
        positionSide: 'LONG';
        side: 'BUY';
    };
    created_at: 1673254434062;
}
export declare class WebSocketAdapter {
    private app;
    private logger;
    clientsUserId: Map<any, any>;
    clientsBot: Map<any, any>;
    clientsBotTrading: Map<any, any>;
    wss: any;
    sessionService: SessionService;
    userRepository: UserRepository;
    cacheRepository: CacheRepository;
    generalSettingRoleService: GeneralSettingRoleService;
    botSignalService: BotSignalService;
    botRepository: BotRepository;
    settingRepository: SettingRepository;
    accountBalanceRepository: AccountBalanceRepository;
    botTradingHistoryRepository: BotTradingHistoryRepository;
    userGetBotTradingHandler: UserGetBotTradingHandler;
    subscriber: any;
    publisher: any;
    isAlive: boolean;
    webSocketEvent: WebSocketEvent;
    eventStoreRepository: EventStoreRepository;
    botTradingService: BotTradingService;
    deactivateUserBotTradingHandler: DeactivateUserBotTradingHandler;
    constructor(app: INestApplication);
    forceLogoutClient(params: IVerify, wsServer: any): Promise<void>;
    checkBrowser(params: ICheckBrowser, wsServer: any): Promise<void>;
    appSendMessage(params: IAppSendMessage): Promise<void>;
    handleStreamBotTrading(params: IAppSendMessage): Promise<void>;
    handleStreamBotStrategyTrading(params: IAppSendMessage): Promise<void>;
    handleVerify(client: any, token: string): Promise<IVerify>;
    handleDiconnect(client: any): Promise<void>;
    handleBotSignal(params: IBotSignalMessage): Promise<void>;
    heartbeat(): void;
    init(): void;
}
export {};
