import { TBOT_CHANEL_TS_EVENT } from 'src/const/bot';
import { BotTradingInitEvent } from './events/bot-trading-init.ws.event';
import { WebSocketMessage, WSClient } from './types';
export declare class WebSocketEvent {
    private botTradingInitEvent;
    constructor(botTradingInitEvent: BotTradingInitEvent);
    request(ws: WSClient, message: WebSocketMessage): void;
    subscribeWSChanel(message: WebSocketMessage): void;
    subscribeTradingBotChanel(message: {
        data: any;
        event: TBOT_CHANEL_TS_EVENT;
    }): void;
}
