"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebSocketAdapter = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../config");
const authorization_1 = require("../const/authorization");
const session_1 = require("../services/session");
const hash_util_1 = require("../utils/hash.util");
const ws_1 = require("ws");
const uuid_1 = require("uuid");
const redis = require("redis");
const app_setting_1 = require("../const/app-setting");
const user_repository_1 = require("../repositories/user.repository");
const cache_repository_1 = require("../repositories/cache.repository");
const general_setting_role_1 = require("../services/general-setting-role");
const bot_signal_1 = require("../services/bot-signal");
const const_1 = require("../const");
const transaction_1 = require("../const/transaction");
const bot_repository_1 = require("../repositories/bot.repository");
const setting_repository_1 = require("../repositories/setting.repository");
const websocket_event_1 = require("./websocket.event");
const bot_1 = require("../const/bot");
const account_balance_repository_1 = require("../repositories/account-balance.repository");
const bot_trading_history_repository_1 = require("../repositories/bot-trading-history.repository");
const event_store_repository_1 = require("../repositories/event-store.repository");
const bot_trading_1 = require("../services/bot-trading");
const user_get_bot_trading_1 = require("../usecases/bot/user-get-bot-trading");
const deactive_user_bot_trading_1 = require("../usecases/bot/deactive-user-bot-trading");
class WebSocketAdapter {
    constructor(app) {
        this.app = app;
        this.logger = new common_1.Logger('WSGateway');
        this.clientsUserId = new Map();
        this.clientsBot = new Map();
        this.clientsBotTrading = new Map();
        this.isAlive = true;
        this.wss = new ws_1.WebSocketServer({ server: app.getHttpServer() });
        this.sessionService = app.get(session_1.SessionService);
        this.userRepository = app.get(user_repository_1.UserRepository);
        this.cacheRepository = app.get(cache_repository_1.CacheRepository);
        this.generalSettingRoleService = app.get(general_setting_role_1.GeneralSettingRoleService);
        this.botSignalService = app.get(bot_signal_1.BotSignalService);
        this.botRepository = app.get(bot_repository_1.BotRepository);
        this.settingRepository = app.get(setting_repository_1.SettingRepository);
        this.webSocketEvent = app.get(websocket_event_1.WebSocketEvent);
        this.accountBalanceRepository = app.get(account_balance_repository_1.AccountBalanceRepository);
        this.botTradingHistoryRepository = app.get(bot_trading_history_repository_1.BotTradingHistoryRepository);
        this.botTradingHistoryRepository = app.get(bot_trading_history_repository_1.BotTradingHistoryRepository);
        this.eventStoreRepository = app.get(event_store_repository_1.EventStoreRepository);
        this.botTradingService = app.get(bot_trading_1.BotTradingService);
        this.userGetBotTradingHandler = app.get(user_get_bot_trading_1.UserGetBotTradingHandler);
        this.deactivateUserBotTradingHandler = app.get(deactive_user_bot_trading_1.DeactivateUserBotTradingHandler);
        this.subscriber = redis.createClient({
            host: config_1.APP_CONFIG.REDIS_HOST,
            port: config_1.APP_CONFIG.REDIS_PORT,
            password: config_1.APP_CONFIG.REDIS_PASS,
        });
        this.subscriber.on('error', (error) => {
            console.log('WSocket error', error);
        });
        this.subscriber.on('connect', (error) => {
            console.log('WSocket connect');
            this.publisher = this.subscriber.duplicate();
        });
    }
    async forceLogoutClient(params, wsServer) {
        const _this = this;
        const clients = this.clientsUserId.get(params.user_id) || [];
        if (clients.length > 0) {
            clients.forEach(function each(ws) {
                if (ws.user_id === params.user_id &&
                    !params.active_client_ids.includes(ws.id)) {
                    if (params.token_id === ws.token_id) {
                        ws.send(JSON.stringify({
                            event: const_1.WS_EVENT.deactivate,
                        }));
                        _this.logger.log(`id: ${ws.id} user: ${ws.user_id} Deactivate`);
                    }
                    else {
                        ws.send(JSON.stringify({
                            event: const_1.WS_EVENT.logout,
                        }));
                        _this.logger.log(`id: ${ws.id} user: ${ws.user_id} Logout`);
                    }
                }
            });
        }
    }
    async checkBrowser(params, wsServer) {
        const clients = this.clientsUserId.get(params.user_id) || [];
        if (clients.length > 0) {
            clients.forEach(function each(ws) {
                if (ws.token_id === params.token_id && ws.id !== params.client_id) {
                    ws.send(JSON.stringify({
                        event: const_1.WS_EVENT.check_device,
                        data: params.verify_id,
                    }));
                }
                else {
                }
            });
        }
    }
    async appSendMessage(params) {
        const clients = this.clientsUserId.get(params.user_id) || [];
        if (clients.length > 0) {
            clients.forEach(function each(ws) {
                if (ws.user_id === params.user_id) {
                    ws.send(JSON.stringify({
                        event: params.event,
                        data: params.data,
                    }));
                }
            });
        }
    }
    async handleStreamBotTrading(params) {
        const clients = this.clientsUserId.get(params.user_id) || [];
        if (clients.length > 0) {
            clients.forEach(function each(ws) {
                ws.send(JSON.stringify({
                    event: params.event,
                    data: params.data,
                }));
            });
        }
    }
    async handleStreamBotStrategyTrading(params) {
        for (let ws of this.clientsBotTrading.values()) {
            ws.send(JSON.stringify({
                event: params.event,
                data: params.data,
            }));
        }
    }
    async handleVerify(client, token) {
        try {
            const decode = await hash_util_1.verifyToken(token, config_1.APP_CONFIG.TOKEN_PUBLIC_KEY);
            await this.sessionService.verifySession(decode);
            client.send(JSON.stringify({
                event: const_1.WS_EVENT.verify,
                data: client.verify_id,
            }));
            client.user_id = decode.user_id;
            client.token_id = decode.token_id;
            this.logger.log(`id: ${client.id} user: ${client.user_id} Connection`);
            const userBots = await this.userRepository.findUserBotSQL({
                user_id: decode.user_id,
                status: transaction_1.ITEM_STATUS.ACTIVE,
                expires_at: Date.now(),
            });
            const botIds = userBots.map((b) => b.bot_id);
            const [bots, userSettingSignalPlatfrom] = await Promise.all([
                this.botRepository.findByIds(botIds),
                this.settingRepository.findOneAppSetting({
                    name: app_setting_1.APP_SETTING.SIGNAL_PLATFORM,
                    user_id: decode.user_id,
                }),
            ]);
            let offBotCodes = [];
            if (userSettingSignalPlatfrom && userSettingSignalPlatfrom.value) {
                try {
                    const config = JSON.parse(userSettingSignalPlatfrom.value);
                    if (config && config.OFF_SIGNALS_BOT) {
                        offBotCodes = config.OFF_SIGNALS_BOT.toString().split(',');
                    }
                }
                catch (error) { }
            }
            const botCodes = [];
            bots.forEach((b) => {
                if (b.code && !offBotCodes.includes(b.code)) {
                    const userClientBots = this.clientsBot.get(b.code) || [];
                    botCodes.push(b.code);
                    if (!userClientBots.some((ucb) => ucb.id === client.id)) {
                        userClientBots.push(client);
                        this.clientsBot.set(b.code, userClientBots);
                        this.logger.log(`clientsBot size: ${this.clientsBot.size}`);
                    }
                }
            });
            client.bot_codes = botCodes;
            const userClients = this.clientsUserId.get(decode.user_id) || [];
            if (!userClients.some((ucl) => ucl.id === client.id)) {
                userClients.push(client);
                this.clientsUserId.set(decode.user_id, userClients);
                this.logger.log(`clientsUserId size: ${this.clientsUserId.size}`);
                const session = await this.userRepository.findLastSession({
                    token_id: decode.token_id,
                    enabled: true,
                });
                const dataSession = {
                    user_id: session.user_id,
                    token: session.token,
                    name_device: session.name_device,
                    browser: session.browser,
                    ip_number: session.ip_number,
                    last_login: session.last_login,
                    expires_at: session.expires_at,
                    enabled: true,
                    websocket_id: client.id,
                    token_id: session.token_id,
                };
                await this.sessionService.save(dataSession);
                const encodeData = dataSession.token.split('.')[1];
                let limitTab = 1;
                if (encodeData) {
                    const decode = JSON.parse(hash_util_1.decodeBase64(encodeData));
                    limitTab = await this.generalSettingRoleService.getMaxLimitTab(decode.roles);
                }
                const sessions = await this.userRepository.findSessionWS({ token_id: decode.token_id, enabled: true }, limitTab);
                const activeClientIds = sessions
                    .map((s) => s.websocket_id)
                    .filter((id) => id);
                await this.forceLogoutClient({
                    active_client_ids: activeClientIds,
                    user_id: decode.user_id,
                    token_id: decode.token_id,
                    verify_id: client.verify_id,
                    client_id: client.id,
                }, this.wss);
                return {
                    active_client_ids: activeClientIds,
                    user_id: client.user_id,
                    token_id: client.token_id,
                    verify_id: client.verify_id,
                    client_id: client.id,
                };
            }
            return null;
        }
        catch (error) {
            console.log('error', error);
            this.logger.log(`id: ${client.id} user: ${client.user_id} Session invalid`);
            client.send(JSON.stringify({
                event: const_1.WS_EVENT.session_invalid,
            }));
            return null;
        }
    }
    async handleDiconnect(client) {
        if (client.bot_codes && client.bot_codes.length > 0) {
            client.bot_codes.forEach((b) => {
                const userClientBots = this.clientsBot.get(b.code) || [];
                if (userClientBots.length > 0) {
                    const newUserBots = userClientBots.filter((ucb) => ucb.id !== client.id);
                    this.clientsBot.set(client.user_id, newUserBots);
                }
            });
        }
        if (client.user_id) {
            const userClients = this.clientsUserId.get(client.user_id) || [];
            if (userClients.length > 0) {
                const newUserClients = userClients.filter((ucl) => ucl.id !== client.id);
                this.clientsUserId.set(client.user_id, newUserClients);
                const session = await this.userRepository.findLastSession({
                    websocket_id: client.id,
                    enabled: true,
                });
                if (session) {
                    await this.userRepository.saveSession({
                        id: session.id,
                        enabled: false,
                    });
                }
            }
        }
    }
    async handleBotSignal(params) {
        try {
            const userBots = this.clientsBot.get(params.name) || [];
            try {
                await this.botSignalService.saveSignal(params);
            }
            catch (error) { }
            if (userBots.length > 0) {
                userBots.forEach((client) => {
                    client.send(JSON.stringify({
                        event: const_1.WS_EVENT.bot_signal,
                        data: {
                            exchange: params.exchange,
                            symbol: params.symbol,
                            name: params.name,
                            resolution: params.resolution,
                            signal_id: params.signal_id,
                            type: params.type,
                            time: params.time,
                            image_url: params.image_url,
                        },
                    }));
                });
            }
        }
        catch (error) {
            console.log('handleBotSignal error', error);
        }
    }
    heartbeat() {
        this.isAlive = true;
    }
    init() {
        const _this = this;
        this.wss.on('connection', (ws, req) => {
            _this.logger.log(`Connection size: ${this.wss.clients.size}`);
            ws.id = uuid_1.v4();
            ws.isAlive = true;
            ws.on('pong', this.heartbeat);
            ws.on('message', async (data, isBinary) => {
                try {
                    console.log('isBinary', isBinary);
                    const message = JSON.parse(data);
                    _this.logger.log(`${ws.id} Event: ${message.event}`);
                    switch (message.event) {
                        case const_1.WS_EVENT.verify:
                            ws.verify_id = uuid_1.v4();
                            const user = await _this.handleVerify(ws, message.data);
                            if (user) {
                                _this.publisher.publish(app_setting_1.CHANELS.WS_CHANEL, JSON.stringify({ event: const_1.WS_EVENT.verify, data: user }));
                            }
                            break;
                        case const_1.WS_EVENT.check_device:
                            if (ws.verify_id) {
                                _this.checkBrowser({
                                    client_id: ws.id,
                                    user_id: ws.user_id,
                                    token_id: ws.token_id,
                                    verify_id: ws.verify_id,
                                }, _this.wss);
                                _this.publisher.publish(app_setting_1.CHANELS.WS_CHANEL, JSON.stringify({
                                    event: const_1.WS_EVENT.check_device,
                                    data: {
                                        client_id: ws.id,
                                        user_id: ws.user_id,
                                        token_id: ws.token_id,
                                        verify_id: ws.verify_id,
                                    },
                                }));
                            }
                            break;
                        case const_1.WS_EVENT.tbot_init:
                            _this.logger.log(`${ws.id} verify_id: ${ws.verify_id}`);
                            if (_this.publisher) {
                                _this.logger.log(`${ws.id} publisher`);
                            }
                            if (ws.verify_id) {
                                ws.tbot_init = true;
                                _this.publisher.publish(app_setting_1.CHANELS.TBOT_CHANEL_UR, JSON.stringify({
                                    event: bot_1.TBOT_CHANEL_TS_EVENT.connect_stream,
                                    data: { user_id: ws.user_id },
                                }));
                            }
                            break;
                        case const_1.WS_EVENT.tbot_close:
                            if (ws.verify_id) {
                                ws.tbot_init = false;
                                _this.publisher.publish(app_setting_1.CHANELS.TBOT_CHANEL_UR, JSON.stringify({
                                    event: bot_1.TBOT_CHANEL_TS_EVENT.disconnect_stream,
                                    data: { user_id: ws.user_id },
                                }));
                            }
                            break;
                        case const_1.WS_EVENT.tbot_strategy_init:
                            ws.tbot_strategy_init = true;
                            this.clientsBotTrading.set(ws.id, ws);
                            break;
                        case const_1.WS_EVENT.tbot_strategy_close:
                            ws.tbot_strategy_init = false;
                            this.clientsBotTrading.delete(ws.id);
                            break;
                        default:
                            break;
                    }
                }
                catch (error) {
                    console.log('error', error);
                }
            });
            ws.on('close', async (e) => {
                this.logger.log(`id: ${ws.id} user: ${ws.user_id} Disconnect`);
                await this.handleDiconnect(ws);
            });
            ws.onerror = function () {
                console.log('Some Error occurred');
            };
        });
        const interval = setInterval(function ping() {
            _this.wss.clients.forEach(function each(ws) {
                if (ws.isAlive === false)
                    return ws.terminate();
                ws.isAlive = false;
                ws.ping();
            });
        }, 30000);
        this.wss.on('close', function close() {
            clearInterval(interval);
        });
        this.subscriber.on('message', async (chanel, data) => {
            this.logger.log(`Channel: ${chanel} data: ${data}`);
            if (chanel === app_setting_1.CHANELS.WS_CHANEL) {
                try {
                    const message = JSON.parse(data);
                    if (message.event === const_1.WS_EVENT.verify) {
                        this.forceLogoutClient(message.data, this.wss);
                    }
                    if (message.event === const_1.WS_EVENT.check_device) {
                        this.checkBrowser(message.data, this.wss);
                    }
                    if (message.event === const_1.WS_EVENT.app_message) {
                        this.appSendMessage({
                            event: message.data.sub_event,
                            user_id: message.data.user_id,
                            data: message.data.sub_data,
                        });
                    }
                }
                catch (error) {
                    console.log('WS_CHANEL error', error);
                }
            }
            if (chanel === app_setting_1.CHANELS.SIGNAL_BOT_CHANEL) {
                try {
                    const message = JSON.parse(data);
                    this.handleBotSignal(message);
                }
                catch (error) {
                    console.log('SIGNAL_BOT_CHANEL error', error);
                }
            }
            if (chanel === app_setting_1.CHANELS.TBOT_CHANEL_TS) {
                try {
                    const message = JSON.parse(data);
                    if (message.event === bot_1.TBOT_CHANEL_TS_EVENT.account) {
                        const { user_bot_id, user_id, bot_id, subscriber_id, balance, time, status, trade, stopout, error, } = message.data;
                        const userBot = await this.userGetBotTradingHandler.execute(bot_id, user_id);
                        if (userBot) {
                            const events = await this.eventStoreRepository.find({
                                user_id,
                                event_name: app_setting_1.APP_SETTING.TBOT_INACTIVE_BY_SYSTEM,
                                state: app_setting_1.EVENT_STORE_STATE.OPEN,
                            });
                            let inactiveBySystemAt = 0;
                            let countInactive = 0;
                            events.forEach((e) => {
                                var _a;
                                if (((_a = e.metadata) === null || _a === void 0 ? void 0 : _a.bot_id) === bot_id) {
                                    countInactive += 1;
                                    if (inactiveBySystemAt < e.created_at) {
                                        inactiveBySystemAt = e.created_at;
                                    }
                                }
                            });
                            this.handleStreamBotTrading({
                                event: const_1.WS_EVENT.tbot_update,
                                user_id,
                                data: {
                                    user_bot_id,
                                    id: bot_id,
                                    balance_init: userBot.balance_init,
                                    pnl_day_init: userBot.pnl_day_init,
                                    pnl_day_current: userBot.pnl_day_current,
                                    pnl_7_day_current: userBot.pnl_7_day_current,
                                    pnl_7_day_init: userBot.pnl_7_day_init,
                                    pnl_30_day_current: userBot.pnl_30_day_current,
                                    pnl_30_day_init: userBot.pnl_30_day_init,
                                    total_trade: userBot.total_trade,
                                    balance_current: userBot.balance_current,
                                    user_status: status,
                                    balance: userBot.balance,
                                    count_inactive_by_system: countInactive,
                                    inactive_by_system_at: inactiveBySystemAt,
                                    error,
                                },
                            });
                        }
                        if (status === transaction_1.ITEM_STATUS.INACTIVE_BY_SYSTEM) {
                            if (!trade) {
                                return;
                            }
                            this.deactivateUserBotTradingHandler.execute(user_bot_id, user_id, bot_id, trade);
                        }
                        if (status === transaction_1.ITEM_STATUS.STOP_OUT) {
                            if (!stopout) {
                                return;
                            }
                            try {
                                const sendEmailProcessing = await this.eventStoreRepository.save({
                                    event_id: `${stopout.time}`,
                                    user_id,
                                    event_name: app_setting_1.EVENT_STORE_NAME.TBOT_STOP_OUT,
                                    state: app_setting_1.EVENT_STORE_STATE.PROCESSING,
                                });
                                try {
                                    await this.botTradingService.emailBotStatus({
                                        user_id,
                                        bot_id,
                                        status: transaction_1.ITEM_STATUS.STOP_OUT,
                                        stopout,
                                    });
                                    try {
                                        await this.eventStoreRepository.save({
                                            event_id: `${stopout.time}`,
                                            user_id,
                                            event_name: app_setting_1.EVENT_STORE_NAME.TBOT_STOP_OUT,
                                            state: app_setting_1.EVENT_STORE_STATE.CLOSED,
                                        });
                                    }
                                    catch (error) {
                                        console.log('error', error);
                                    }
                                }
                                catch (error) {
                                    console.log('error', error);
                                    await this.eventStoreRepository.deleteById(sendEmailProcessing.id);
                                }
                            }
                            catch (error) {
                                console.log('error', error);
                            }
                        }
                    }
                    if (message.event === bot_1.TBOT_CHANEL_TS_EVENT.trades) {
                        const { user_bot_id, user_id, bot_id, subscriber_id, trades } = message.data;
                        const userBot = await this.userGetBotTradingHandler.execute(bot_id, user_id);
                        if (userBot) {
                            const events = await this.eventStoreRepository.find({
                                user_id,
                                event_name: app_setting_1.APP_SETTING.TBOT_INACTIVE_BY_SYSTEM,
                                state: app_setting_1.EVENT_STORE_STATE.OPEN,
                            });
                            let inactiveBySystemAt = 0;
                            let countInactive = 0;
                            events.forEach((e) => {
                                var _a;
                                if (((_a = e.metadata) === null || _a === void 0 ? void 0 : _a.bot_id) === bot_id) {
                                    countInactive += 1;
                                    if (inactiveBySystemAt < e.created_at) {
                                        inactiveBySystemAt = e.created_at;
                                    }
                                }
                            });
                            this.handleStreamBotTrading({
                                event: const_1.WS_EVENT.tbot_update,
                                user_id,
                                data: {
                                    user_bot_id,
                                    id: bot_id,
                                    balance_init: userBot.balance_init,
                                    pnl_day_init: userBot.pnl_day_init,
                                    pnl_day_current: userBot.pnl_day_current,
                                    pnl_7_day_current: userBot.pnl_7_day_current,
                                    pnl_7_day_init: userBot.pnl_7_day_init,
                                    pnl_30_day_current: userBot.pnl_30_day_current,
                                    pnl_30_day_init: userBot.pnl_30_day_init,
                                    total_trade: userBot.total_trade,
                                    balance_current: userBot.balance_current,
                                    balance: userBot.balance,
                                    count_inactive_by_system: countInactive,
                                    inactive_by_system_at: inactiveBySystemAt,
                                },
                            });
                        }
                        if (trades && trades.length > 0) {
                            this.handleStreamBotTrading({
                                event: const_1.WS_EVENT.tbot_trade_update,
                                user_id,
                                data: {
                                    id: user_bot_id,
                                    bot_id,
                                    trades,
                                },
                            });
                            this.handleStreamBotTrading({
                                event: const_1.WS_EVENT.tbot_chart_pnl_update,
                                user_id,
                                data: {
                                    bot_id,
                                },
                            });
                        }
                    }
                    if (message.event === bot_1.TBOT_CHANEL_TS_EVENT.strategy_trades) {
                        const { bot_id, trades } = message.data;
                        if (trades && trades.length > 0) {
                            this.handleStreamBotStrategyTrading({
                                event: const_1.WS_EVENT.tbot_strategy_trade_update,
                                data: {
                                    bot_id,
                                    trades,
                                },
                            });
                            this.handleStreamBotStrategyTrading({
                                event: const_1.WS_EVENT.tbot_strategy_chart_pnl_update,
                                data: {
                                    bot_id,
                                },
                            });
                        }
                    }
                }
                catch (error) {
                    console.log('TBOT_CHANEL_TS error', error);
                }
            }
        });
        this.subscriber.subscribe(app_setting_1.CHANELS.WS_CHANEL);
        this.subscriber.subscribe(app_setting_1.CHANELS.SIGNAL_BOT_CHANEL);
        this.subscriber.subscribe(app_setting_1.CHANELS.TBOT_CHANEL_TS);
    }
}
exports.WebSocketAdapter = WebSocketAdapter;
//# sourceMappingURL=websocket.adapter.js.map