import { CoinpaymentTransport } from 'src/resources/coinpayment/coinpayment.transport';
import { IConvertCoin, ICreateTransaction } from './types';
export declare class CoinpaymentResource {
    private coinpaymentTransport;
    constructor(coinpaymentTransport: CoinpaymentTransport);
    createTransaction(params: ICreateTransaction): Promise<any>;
    checkTransaction(payment_id: string): Promise<any>;
    checkWithdrawal(withdrawal_id: string): Promise<any>;
    convertCoin(params: IConvertCoin): Promise<any>;
}
