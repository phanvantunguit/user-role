"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoinpaymentTransport = void 0;
const http_transport_util_1 = require("../../utils/http-transport.util");
const config_1 = require("../../config");
const hash_util_1 = require("../../utils/hash.util");
const format_data_util_1 = require("../../utils/format-data.util");
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const handle_error_util_1 = require("../../utils/handle-error.util");
class CoinpaymentTransport {
    async requestPOST(params) {
        const url = config_1.APP_CONFIG.COINPAYMENT_BASE_URL || 'https://www.coinpayments.net/api.php';
        const dataClear = Object.assign(Object.assign({}, format_data_util_1.cleanObject(params)), { version: '1', key: config_1.APP_CONFIG.COINPAYMENT_PUBLIC_KEY, format: 'json' });
        const dataString = format_data_util_1.formatRequestDataToString(dataClear);
        const checksum = hash_util_1.createChecksumSHA512HMAC(dataString, config_1.APP_CONFIG.COINPAYMENT_PRIVATE_KEY, 'hex');
        const headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            HMAC: checksum,
        };
        try {
            const result = await http_transport_util_1.requestPOST(url, dataString, headers);
            return result;
        }
        catch (error) {
            console.log('error', error);
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNPROCESSABLE_ENTITY }, const_1.ERROR_CODE.UNPROCESSABLE));
        }
    }
}
exports.CoinpaymentTransport = CoinpaymentTransport;
//# sourceMappingURL=coinpayment.transport.js.map