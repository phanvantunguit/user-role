"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoinpaymentResource = void 0;
const common_1 = require("@nestjs/common");
const coinpayment_transport_1 = require("./coinpayment.transport");
let CoinpaymentResource = class CoinpaymentResource {
    constructor(coinpaymentTransport) {
        this.coinpaymentTransport = coinpaymentTransport;
    }
    async createTransaction(params) {
        const data = {
            cmd: 'create_transaction',
            amount: params.sell_amount,
            currency1: params.sell_currency,
            currency2: params.buy_currency,
            buyer_email: params.buyer_email,
        };
        return this.coinpaymentTransport.requestPOST(data);
    }
    async checkTransaction(payment_id) {
        const data = {
            cmd: 'get_tx_info',
            txid: payment_id,
        };
        return this.coinpaymentTransport.requestPOST(data);
    }
    async checkWithdrawal(withdrawal_id) {
        const data = {
            cmd: 'get_withdrawal_info',
            id: withdrawal_id,
        };
        return this.coinpaymentTransport.requestPOST(data);
    }
    async convertCoin(params) {
        const data = {
            cmd: 'convert',
            amount: params.amount,
            from: params.from,
            to: params.to,
        };
        return this.coinpaymentTransport.requestPOST(data);
    }
};
CoinpaymentResource = __decorate([
    __param(0, common_1.Inject(coinpayment_transport_1.CoinpaymentTransport)),
    __metadata("design:paramtypes", [coinpayment_transport_1.CoinpaymentTransport])
], CoinpaymentResource);
exports.CoinpaymentResource = CoinpaymentResource;
//# sourceMappingURL=index.js.map