import { CmpaymentTransport } from 'src/resources/cmpayment/cmpayment.transport';
export declare class CmpaymentResource {
    private cmpaymentTransport;
    constructor(cmpaymentTransport: CmpaymentTransport);
    createTransaction(params: any): Promise<any>;
    getTransaction(id: string): Promise<any>;
    listTransaction(param: {
        user_id?: string;
        email?: string;
    }): Promise<any>;
}
