"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CmpaymentTransport = void 0;
const http_transport_util_1 = require("../../utils/http-transport.util");
const config_1 = require("../../config");
const hash_util_1 = require("../../utils/hash.util");
const format_data_util_1 = require("../../utils/format-data.util");
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const handle_error_util_1 = require("../../utils/handle-error.util");
class CmpaymentTransport {
    async requestPOST(params, url) {
        const dataClear = format_data_util_1.cleanObject(params);
        const dataString = format_data_util_1.formatRequestDataToStringV2(dataClear);
        const checksum = hash_util_1.createChecksum(dataString, config_1.APP_CONFIG.SECRET_KEY);
        dataClear['checksum'] = checksum;
        const headers = {
            'Content-Type': 'application/json',
        };
        try {
            const result = await http_transport_util_1.requestPOST(url, dataClear, headers);
            return result;
        }
        catch (error) {
            console.log('error', error);
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNPROCESSABLE_ENTITY }, const_1.ERROR_CODE.UNPROCESSABLE));
        }
    }
    async requestGET(params, url) {
        const dataClear = format_data_util_1.cleanObject(params);
        const dataString = format_data_util_1.formatRequestDataToStringV2(dataClear);
        const checksum = hash_util_1.createChecksum(dataString, config_1.APP_CONFIG.SECRET_KEY);
        dataClear['checksum'] = checksum;
        const headers = {
            'Content-Type': 'application/json',
        };
        try {
            url = `${url}?${dataString}`;
            const result = await http_transport_util_1.requestGET(url, headers);
            return result;
        }
        catch (error) {
            console.log('error', error);
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNPROCESSABLE_ENTITY }, const_1.ERROR_CODE.UNPROCESSABLE));
        }
    }
}
exports.CmpaymentTransport = CmpaymentTransport;
//# sourceMappingURL=cmpayment.transport.js.map