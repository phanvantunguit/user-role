"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CmpaymentResource = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../config");
const app_setting_1 = require("../../const/app-setting");
const cmpayment_transport_1 = require("./cmpayment.transport");
let CmpaymentResource = class CmpaymentResource {
    constructor(cmpaymentTransport) {
        this.cmpaymentTransport = cmpaymentTransport;
    }
    async createTransaction(params) {
        const url = `${config_1.APP_CONFIG.CMPAYMENT_BASE_URL}/api/v1/transaction`;
        return this.cmpaymentTransport.requestPOST(params, url);
    }
    async getTransaction(id) {
        const url = `${config_1.APP_CONFIG.CMPAYMENT_BASE_URL}/api/v1/transaction/${id}`;
        return this.cmpaymentTransport.requestGET({}, url);
    }
    async listTransaction(param) {
        const url = `${config_1.APP_CONFIG.CMPAYMENT_BASE_URL}/api/v1/transaction/list`;
        return this.cmpaymentTransport.requestGET(Object.assign({ integrate_service: app_setting_1.SERVICE_NAME }, param), url);
    }
};
CmpaymentResource = __decorate([
    __param(0, common_1.Inject(cmpayment_transport_1.CmpaymentTransport)),
    __metadata("design:paramtypes", [cmpayment_transport_1.CmpaymentTransport])
], CmpaymentResource);
exports.CmpaymentResource = CmpaymentResource;
//# sourceMappingURL=index.js.map