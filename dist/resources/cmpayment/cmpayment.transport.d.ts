export declare class CmpaymentTransport {
    requestPOST(params: any, url: string): Promise<any>;
    requestGET(params: any, url: string): Promise<any>;
}
