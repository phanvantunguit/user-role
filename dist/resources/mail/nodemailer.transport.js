"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodeMailerTransport = void 0;
const nodemailer = require("nodemailer");
const option = {
    service: 'gmail',
    auth: {
        user: 'sender.coinmap@gmail.com',
        pass: 'Sender@coinmap',
    },
};
class NodeMailerTransport {
    connect() {
        this.transporter = nodemailer.createTransport(option);
    }
    async nodemailerSendEmail(params) {
        return new Promise((resolve) => {
            const _this = this;
            if (!this.transporter) {
                this.connect();
            }
            this.transporter.verify(function (error) {
                if (error) {
                    throw error;
                }
                else {
                    const mail = {
                        from: option.auth.user,
                        to: params.to,
                        subject: params.subject,
                        html: params.html,
                    };
                    _this.transporter.sendMail(mail, function (error, info) {
                        if (error) {
                            console.log(error);
                            return resolve(false);
                        }
                        else {
                            console.log('Email sent: ' + info.response);
                            return resolve(true);
                        }
                    });
                }
            });
        });
    }
}
exports.NodeMailerTransport = NodeMailerTransport;
//# sourceMappingURL=nodemailer.transport.js.map