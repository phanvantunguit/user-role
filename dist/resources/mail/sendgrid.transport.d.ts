import { ISendEmail } from './types';
export declare class SendGridTransport {
    sendGridSendEmail(params: ISendEmail, email?: string, name?: string, ccEmail?: string): Promise<any>;
}
