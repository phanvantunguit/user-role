import { ISendEmail } from './types';
export declare class NodeMailerTransport {
    transporter: any;
    connect(): void;
    nodemailerSendEmail(params: ISendEmail): Promise<boolean>;
}
