export interface ISendEmail {
    to: string;
    subject: string;
    html: string;
}
export interface IEmailPaymentInfo {
    email: string;
    role_name: string;
    package_name: string;
    amount: string;
    currency: string;
    buy_amount: string;
    buy_currency: string;
    wallet_address?: string;
    time?: string;
    checkout_url?: string;
    qrcode_url?: string;
    status_url?: string;
    sell_amount_received?: string;
    payment_id: string;
}
export interface IEmailPaymentWarning {
    email: string;
    amount: string;
    role_name: string;
    package_name: string;
    currency: string;
    transaction_id: string;
    transaction_status: string;
    wallet_address?: string;
    sell_amount_received: string;
    change_amount: any;
}
export interface IEmailPaymentRefund {
    email: string;
    sell_amount: string;
    sell_currency: string;
    sell_amount_received: string;
    amount: string;
    fee: string;
    total: string;
    role_name: string;
    package_name: string;
    currency: string;
    transaction_id: string;
    transaction_status: string;
    wallet_address?: string;
}
export interface IEmailUpgradeSuccess {
    email: string;
    role_name: string;
    package_name: string;
    amount: string;
    currency: string;
}
