import { SendGridTransport } from './sendgrid.transport';
import { IEmailPaymentInfo } from './types';
export declare class MailResource {
    private sendGridTransport;
    constructor(sendGridTransport: SendGridTransport);
    sendEmailConfirmAccount(email: string, token: string, from_email?: string, from_name?: string, logo_url?: string, header_url?: string, main_content?: string, twitter_url?: string, youtube_url?: string, facebook_url?: string, telegram_url?: string, company_name?: string, fullname?: string): Promise<any>;
    sendEmailForgotPassword(email: string, token: string, expires_at: number, resetPasswordLink?: string, from_email?: string, from_name?: string, logo_url?: string, header_url?: string, twitter_url?: string, youtube_url?: string, facebook_url?: string, telegram_url?: string, company_name?: string, fullname?: string): Promise<any>;
    sendEmailCreateAccount(email: string, token: string, password: string, from_email?: string, from_name?: string, fullname?: string): Promise<any>;
    sendEmailChange(email: string, token: string, originEmail: string, newEmail: string, callbackUrl: string, from_email?: string, from_name?: string, logo_url?: string, header_url?: string, twitter_url?: string, youtube_url?: string, facebook_url?: string, telegram_url?: string, company_name?: string, fullname?: string): Promise<any>;
    sendEmailPaymentInfo(parmas: IEmailPaymentInfo, from_email?: string, from_name?: string): Promise<any>;
    sendEmailInactiveBySystem(email: string, token: string, from_email?: string, from_name?: string, logo_url?: string, header_url?: string, main_content?: string, twitter_url?: string, youtube_url?: string, facebook_url?: string, telegram_url?: string, company_name?: string, fullname?: string): Promise<any>;
    sendEmailStandard(subject: string, title: string, email: string, main_content: string[], from_email?: string, from_name?: string, logo_url?: string, header_url?: string, twitter_url?: string, youtube_url?: string, facebook_url?: string, telegram_url?: string, company_name?: string, fullname?: string, ccEmail?: string): Promise<any>;
}
