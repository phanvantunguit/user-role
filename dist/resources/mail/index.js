"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MailResource = void 0;
const common_1 = require("@nestjs/common");
const handlebars = require("handlebars");
const config_1 = require("../../config");
const sendgrid_transport_1 = require("./sendgrid.transport");
const email_forgot_password_1 = require("../../templates/email-forgot-password");
const email_verify_1 = require("../../templates/email-verify");
const email_verify_account_create_1 = require("../../templates/email-verify-account-create");
const email_payment_info_1 = require("../../templates/email-payment-info");
const email_change_1 = require("../../templates/email-change");
const email_common_1 = require("../../templates/email-common");
const email_1 = require("../../const/email");
let MailResource = class MailResource {
    constructor(sendGridTransport) {
        this.sendGridTransport = sendGridTransport;
    }
    async sendEmailConfirmAccount(email, token, from_email, from_name, logo_url, header_url, main_content, twitter_url, youtube_url, facebook_url, telegram_url, company_name, fullname) {
        const logoUrl = logo_url || 'https://static.cextrading.io/images/1/logo.png';
        const headerUrl = header_url || 'https://static.cextrading.io/images/1/header-bg.png';
        let footerContent;
        if (company_name) {
            footerContent = `
        ${company_name.toUpperCase()} is an organization providing trading-related services for professional investors.
        \n 
        ${company_name.toUpperCase()} prioritizes the benefit of high-end technology applied to Trading; we are committed to being top in providing trading-related services and helping our investors optimize profits.
        `;
        }
        else {
            footerContent =
                'Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.';
        }
        const companyName = company_name || 'Coinmap';
        const companyNameUpperCase = company_name || 'COINMAP';
        const mainContent = main_content ||
            `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our CEX and DEX Trading applications will help you trade safety and in comfort.`;
        const twitterUrl = twitter_url || 'https://twitter.com/CoinmapTrading';
        const facebookUrl = facebook_url || 'https://www.facebook.com/CoinmapTrading';
        const youtubeUrl = youtube_url || 'https://www.youtube.com/c/8xTRADING';
        const telegramUrl = telegram_url || 'https://t.me/trading8x';
        const template = handlebars.compile(email_verify_1.emailVerify);
        const replacements = {
            linkVerify: `${config_1.APP_CONFIG.BASE_URL}/api/v1/user/verify-email/${token}`,
            logoUrl,
            headerUrl,
            companyName,
            companyNameUpperCase: companyNameUpperCase.toUpperCase(),
            mainContent,
            footerContent,
            twitterUrl,
            facebookUrl,
            youtubeUrl,
            telegramUrl,
            fullname,
        };
        const html = template(replacements);
        const dataSendEmail = {
            to: email,
            subject: email_1.EMAIL_SUBJECT.verify_email,
            html,
        };
        return this.sendGridTransport.sendGridSendEmail(dataSendEmail, from_email, from_name);
    }
    async sendEmailForgotPassword(email, token, expires_at, resetPasswordLink, from_email, from_name, logo_url, header_url, twitter_url, youtube_url, facebook_url, telegram_url, company_name, fullname) {
        const logoUrl = logo_url || 'https://static.cextrading.io/images/1/logo.png';
        const headerUrl = header_url || 'https://static.cextrading.io/images/1/header-bg.png';
        const companyNameUpperCase = company_name || 'COINMAP';
        let footerContent;
        if (company_name) {
            footerContent = `
        ${company_name.toUpperCase()} is an organization providing trading-related services for professional investors.\n
        ${company_name.toUpperCase()} prioritizes the benefit of high-end technology applied to Trading; we are committed to being top in providing trading-related services and helping our investors optimize profits.
        `;
        }
        else {
            footerContent =
                'Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.';
        }
        const twitterUrl = twitter_url || 'https://twitter.com/CoinmapTrading';
        const facebookUrl = facebook_url || 'https://www.facebook.com/CoinmapTrading';
        const youtubeUrl = youtube_url || 'https://www.youtube.com/c/8xTRADING';
        const telegramUrl = telegram_url || 'https://t.me/trading8x';
        const template = handlebars.compile(email_forgot_password_1.emailForgotPassword);
        const replacements = {
            resetPasswordLink: `${resetPasswordLink || config_1.APP_CONFIG.DASHBOARD_CLIENT_BASE_URL}/reset-password?token=${token}&expires_at=${expires_at}`,
            logoUrl,
            headerUrl,
            fromEmail: from_email || 'coinmaptrading@coinmap.tech',
            companyNameUpperCase: companyNameUpperCase.toUpperCase(),
            twitterUrl,
            facebookUrl,
            youtubeUrl,
            telegramUrl,
            footerContent,
            fullname
        };
        const html = template(replacements);
        const dataSendEmail = {
            to: email,
            subject: email_1.EMAIL_SUBJECT.reset_password,
            html,
        };
        return this.sendGridTransport.sendGridSendEmail(dataSendEmail, from_email, from_name);
    }
    async sendEmailCreateAccount(email, token, password, from_email, from_name, fullname) {
        const txtPassword = `Your password: ${password}`;
        const template = handlebars.compile(email_verify_account_create_1.emailVerifyAccountCreate);
        const replacements = {
            linkVerify: `${config_1.APP_CONFIG.BASE_URL}/api/v1/user/verify-email/${token}`,
            password: txtPassword,
            fullname
        };
        const html = template(replacements);
        const dataSendEmail = {
            to: email,
            subject: email_1.EMAIL_SUBJECT.verify_email,
            html,
        };
        return this.sendGridTransport.sendGridSendEmail(dataSendEmail, from_email, from_name);
    }
    async sendEmailChange(email, token, originEmail, newEmail, callbackUrl, from_email, from_name, logo_url, header_url, twitter_url, youtube_url, facebook_url, telegram_url, company_name, fullname) {
        const logoUrl = logo_url || 'https://static.cextrading.io/images/1/logo.png';
        const headerUrl = header_url || 'https://static.cextrading.io/images/1/header-bg.png';
        const companyNameUpperCase = company_name || 'COINMAP';
        let footerContent;
        if (company_name) {
            footerContent = `
        ${company_name.toUpperCase()} is an organization providing trading-related services for professional investors.
        \n 
        ${company_name.toUpperCase()} prioritizes the benefit of high-end technology applied to Trading; we are committed to being top in providing trading-related services and helping our investors optimize profits.
        `;
        }
        else {
            footerContent =
                'Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.';
        }
        const twitterUrl = twitter_url || 'https://twitter.com/CoinmapTrading';
        const facebookUrl = facebook_url || 'https://www.facebook.com/CoinmapTrading';
        const youtubeUrl = youtube_url || 'https://www.youtube.com/c/8xTRADING';
        const telegramUrl = telegram_url || 'https://t.me/trading8x';
        const template = handlebars.compile(email_change_1.emailChange);
        let linkVerify = callbackUrl;
        if (callbackUrl.includes('?')) {
            linkVerify += `&token=${token}`;
        }
        else {
            linkVerify += `?token=${token}`;
        }
        const replacements = {
            linkVerify,
            originEmail,
            newEmail,
            logoUrl,
            headerUrl,
            companyNameUpperCase: companyNameUpperCase.toUpperCase(),
            footerContent,
            twitterUrl,
            facebookUrl,
            youtubeUrl,
            telegramUrl,
            fullname
        };
        const html = template(replacements);
        const dataSendEmail = {
            to: email,
            subject: email_1.EMAIL_SUBJECT.change_email,
            html,
        };
        return this.sendGridTransport.sendGridSendEmail(dataSendEmail, from_email, from_name);
    }
    async sendEmailPaymentInfo(parmas, from_email, from_name) {
        const { email, role_name, package_name, amount, currency, buy_amount, buy_currency, wallet_address, time, checkout_url, qrcode_url, status_url, sell_amount_received, payment_id, } = parmas;
        let html;
        const template = handlebars.compile(email_payment_info_1.emailPaymentInfo);
        const replacements = {
            role_name,
            package_name,
            buy_amount,
            buy_currency,
            wallet_address,
            time,
            checkout_url,
            qrcode_url,
            status_url,
            payment_id,
        };
        html = template(replacements);
        const dataSendEmail = {
            to: email,
            subject: `${email_1.EMAIL_SUBJECT.transaction_information} ${payment_id}`,
            html,
        };
        return this.sendGridTransport.sendGridSendEmail(dataSendEmail, from_email, from_name);
    }
    async sendEmailInactiveBySystem(email, token, from_email, from_name, logo_url, header_url, main_content, twitter_url, youtube_url, facebook_url, telegram_url, company_name, fullname) {
        const logoUrl = logo_url || 'https://static.cextrading.io/images/1/logo.png';
        const headerUrl = header_url || 'https://static.cextrading.io/images/1/header-bg.png';
        const companyName = company_name || 'Coinmap';
        const companyNameUpperCase = company_name || 'COINMAP';
        const mainContent = main_content ||
            `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our CEX and DEX Trading applications will help you trade safety and in comfort.`;
        let footerContent;
        if (company_name) {
            footerContent = `
          ${company_name.toUpperCase()} is an organization providing trading-related services for professional investors.
          \n 
          ${company_name.toUpperCase()} prioritizes the benefit of high-end technology applied to Trading; we are committed to being top in providing trading-related services and helping our investors optimize profits.
          `;
        }
        else {
            footerContent =
                'Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.';
        }
        const twitterUrl = twitter_url || 'https://twitter.com/CoinmapTrading';
        const facebookUrl = facebook_url || 'https://www.facebook.com/CoinmapTrading';
        const youtubeUrl = youtube_url || 'https://www.youtube.com/c/8xTRADING';
        const telegramUrl = telegram_url || 'https://t.me/trading8x';
        const template = handlebars.compile(email_verify_1.emailVerify);
        const replacements = {
            linkVerify: `${config_1.APP_CONFIG.BASE_URL}/api/v1/user/verify-email/${token}`,
            logoUrl,
            headerUrl,
            companyName,
            companyNameUpperCase: companyNameUpperCase.toUpperCase(),
            mainContent,
            footerContent,
            twitterUrl,
            facebookUrl,
            youtubeUrl,
            telegramUrl,
            fullname
        };
        const html = template(replacements);
        const dataSendEmail = {
            to: email,
            subject: email_1.EMAIL_SUBJECT.verify_email,
            html,
        };
        return this.sendGridTransport.sendGridSendEmail(dataSendEmail, from_email, from_name);
    }
    async sendEmailStandard(subject, title, email, main_content, from_email, from_name, logo_url, header_url, twitter_url, youtube_url, facebook_url, telegram_url, company_name, fullname, ccEmail) {
        const logoUrl = logo_url || 'https://static.cextrading.io/images/1/logo.png';
        const headerUrl = header_url || 'https://static.cextrading.io/images/1/header-bg.png';
        let footerContent;
        if (company_name) {
            footerContent = `
      ${company_name.toUpperCase()} is an organization providing trading-related services for professional investors.
      \n 
      ${company_name.toUpperCase()} prioritizes the benefit of high-end technology applied to Trading; we are committed to being top in providing trading-related services and helping our investors optimize profits.
      `;
        }
        else {
            footerContent =
                'Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.';
        }
        const companyName = company_name || 'Coinmap';
        const companyNameUpperCase = company_name || 'COINMAP';
        const twitterUrl = twitter_url || 'https://twitter.com/CoinmapTrading';
        const facebookUrl = facebook_url || 'https://www.facebook.com/CoinmapTrading';
        const youtubeUrl = youtube_url || 'https://www.youtube.com/c/8xTRADING';
        const telegramUrl = telegram_url || 'https://t.me/trading8x';
        const template = handlebars.compile(email_common_1.emailCommon);
        const replacements = {
            title,
            logoUrl,
            headerUrl,
            companyName,
            companyNameUpperCase: companyNameUpperCase.toUpperCase(),
            mainContent: main_content,
            footerContent,
            twitterUrl,
            facebookUrl,
            youtubeUrl,
            telegramUrl,
            fullname
        };
        const html = template(replacements);
        const dataSendEmail = {
            to: email,
            subject,
            html,
        };
        return this.sendGridTransport.sendGridSendEmail(dataSendEmail, from_email, from_name, ccEmail);
    }
};
MailResource = __decorate([
    __param(0, common_1.Inject(sendgrid_transport_1.SendGridTransport)),
    __metadata("design:paramtypes", [sendgrid_transport_1.SendGridTransport])
], MailResource);
exports.MailResource = MailResource;
//# sourceMappingURL=index.js.map