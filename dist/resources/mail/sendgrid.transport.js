"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendGridTransport = void 0;
const http_transport_util_1 = require("../../utils/http-transport.util");
const config_1 = require("../../config");
class SendGridTransport {
    async sendGridSendEmail(params, email, name, ccEmail) {
        const data = {
            from: {
                email: email || config_1.APP_CONFIG.SENDGRID_SENDER_EMAIL,
                name: name || config_1.APP_CONFIG.SENDGRID_SENDER_NAME,
            },
            personalizations: [],
            subject: params.subject,
            content: [
                {
                    type: 'text/html',
                    value: params.html,
                },
            ],
        };
        const personalization = {
            to: [
                {
                    email: params.to,
                },
            ],
        };
        if (ccEmail) {
            personalization['cc'] = [
                {
                    email: ccEmail
                },
            ];
        }
        data.personalizations.push(personalization);
        const url = `${config_1.APP_CONFIG.SENDGRID_BASE_URL}/v3/mail/send`;
        const headers = {
            Authorization: `Bearer ${config_1.APP_CONFIG.SENDGRID_API_TOKEN}`,
            'Content-Type': 'application/json',
        };
        try {
            console.log('SendGrid request ccEmail:', ccEmail);
            console.log('SendGrid request personalization:', personalization);
            const result = await http_transport_util_1.requestPOST(url, data, headers);
            console.log('SendGrid result:', result);
            return true;
        }
        catch (error) {
            console.log('SendGrid ERROR', error);
            return false;
        }
    }
}
exports.SendGridTransport = SendGridTransport;
//# sourceMappingURL=sendgrid.transport.js.map