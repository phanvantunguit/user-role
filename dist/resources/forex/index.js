"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MetaapiResource = void 0;
const metaapi_cloud_sdk_1 = require("metaapi.cloud-sdk");
const config_1 = require("../../config");
const bot_1 = require("../../const/bot");
class MetaapiResource {
    constructor() {
        this.metaApi = new metaapi_cloud_sdk_1.default(config_1.APP_CONFIG.MT_TOKEN);
        this.copyFactory = new metaapi_cloud_sdk_1.CopyFactory(config_1.APP_CONFIG.MT_TOKEN);
    }
    async createProvisioningProfile(params) {
        const { name, version, brokerTimezone, brokerDSTSwitchTimezone, file, merchant_code, account_id, user_id, } = params;
        const provisioningProfile = await this.metaApi.provisioningProfileApi.createProvisioningProfile({
            name: `${merchant_code}_${account_id}_${user_id}`,
            version,
            brokerTimezone,
            brokerDSTSwitchTimezone,
        });
        let rename = name;
        const type = name.split('.').pop();
        switch (type) {
            case 'ini':
                rename = 'servers.ini';
                break;
            case 'srv':
                rename = 'broker.srv';
                break;
            default:
                break;
        }
        try {
            await provisioningProfile.uploadFile(rename, file);
        }
        catch (error) {
            await provisioningProfile.remove();
            throw error;
        }
        return provisioningProfile;
    }
    async removeProvisioningProfile(profileId) {
        try {
            const provisioningProfile = await this.metaApi.provisioningProfileApi.getProvisioningProfile(profileId);
            await provisioningProfile.remove();
        }
        catch (error) {
            console.log('removeProvisioningProfile error', error);
        }
    }
    async createSubscriber(params) {
        const { login, password, name, server, platform, profile_id } = params;
        const account = await this.metaApi.metatraderAccountApi.createAccount({
            login,
            password,
            name,
            server,
            magic: 0,
            region: 'singapore',
            platform,
            copyFactoryRoles: ['SUBSCRIBER'],
            baseCurrency: 'USD',
            riskManagementApiEnabled: true,
            provisioningProfileId: profile_id,
        });
        return account;
    }
    async getAccount(params) {
        try {
            const { account_id } = params;
            const account = await this.metaApi.metatraderAccountApi.getAccount(account_id);
            return account;
        }
        catch (error) {
            console.log('getAccount error', error);
            if (error.status === 404) {
                return false;
            }
            throw error;
        }
    }
    async updateSubscriber(params) {
        const { subscriber_id, name, strategy_id, multiplier, max_absolute_risk, start_time, broker, } = params;
        let symbolMapping;
        switch (broker) {
            case bot_1.BROKER_CODE.xm:
                symbolMapping = [
                    {
                        from: 'XAUUSD',
                        to: 'GOLD',
                    },
                ];
                break;
        }
        const subscriptionUpdate = await this.copyFactory.configurationApi.updateSubscriber(subscriber_id, {
            name,
            subscriptions: [
                {
                    strategyId: strategy_id,
                    multiplier,
                    riskLimits: [
                        {
                            type: 'lifetime',
                            applyTo: 'balance-minus-equity',
                            maxAbsoluteRisk: max_absolute_risk,
                            closePositions: true,
                            startTime: start_time,
                        },
                    ],
                },
            ],
        });
        return subscriptionUpdate;
    }
    async removeSubscriber(params) {
        const { subscriber_id } = params;
        const subscription = await this.copyFactory.configurationApi.getSubscriber(subscriber_id);
        if (subscription && !subscription.removed) {
            const subscriptionRemove = await this.copyFactory.configurationApi.removeSubscriber(subscriber_id);
            return subscriptionRemove;
        }
        return subscription;
    }
    async removeSubscrition(params) {
        const { subscriber_id, strategy_id } = params;
        try {
            const subscriptionUpdate = await this.copyFactory.configurationApi.removeSubscription(subscriber_id, strategy_id);
            return subscriptionUpdate;
        }
        catch (error) {
            return false;
        }
    }
    async removeAccount(params) {
        const { subscriber_id } = params;
        let account;
        try {
            account = await this.metaApi.metatraderAccountApi.getAccount(subscriber_id);
        }
        catch (error) {
            console.log('getAccount error', error);
            if (error.status === 404) {
                return true;
            }
            throw error;
        }
        if (account) {
            await account.remove();
        }
    }
}
exports.MetaapiResource = MetaapiResource;
//# sourceMappingURL=index.js.map