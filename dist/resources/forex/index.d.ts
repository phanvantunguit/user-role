import MetaApi, { CopyFactory, ProvisioningProfile } from 'metaapi.cloud-sdk';
import { TBOT_PLATFORM } from 'src/const/bot';
export declare class MetaapiResource {
    metaApi: MetaApi;
    copyFactory: CopyFactory;
    constructor();
    createProvisioningProfile(params: {
        name: string;
        version: number;
        brokerTimezone: string;
        brokerDSTSwitchTimezone: string;
        file: any;
        merchant_code: string;
        account_id: string;
        user_id: string;
    }): Promise<ProvisioningProfile>;
    removeProvisioningProfile(profileId: string): Promise<void>;
    createSubscriber(params: {
        login: string;
        password: string;
        name: string;
        server: string;
        platform: TBOT_PLATFORM;
        profile_id?: string;
    }): Promise<import("metaapi.cloud-sdk").MetatraderAccountIdDto>;
    getAccount(params: {
        account_id: string;
    }): Promise<false | import("metaapi.cloud-sdk").MetatraderAccount>;
    updateSubscriber(params: {
        subscriber_id: string;
        name: string;
        strategy_id: string;
        multiplier: number;
        max_absolute_risk: number;
        start_time: Date;
        broker: string;
    }): Promise<any>;
    removeSubscriber(params: {
        subscriber_id: string;
    }): Promise<any>;
    removeSubscrition(params: {
        subscriber_id: string;
        strategy_id: string;
    }): Promise<any>;
    removeAccount(params: {
        subscriber_id: string;
    }): Promise<boolean>;
}
