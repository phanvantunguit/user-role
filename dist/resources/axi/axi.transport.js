"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AxiTransport = void 0;
const http_transport_util_1 = require("../../utils/http-transport.util");
const config_1 = require("../../config");
const format_data_util_1 = require("../../utils/format-data.util");
const common_1 = require("@nestjs/common");
const const_1 = require("../../const");
const handle_error_util_1 = require("../../utils/handle-error.util");
class AxiTransport {
    async requestPOST(params, url) {
        const dataClear = format_data_util_1.cleanObject(params);
        const headers = {
            'Content-Type': 'application/json;',
            SecretKey: config_1.APP_CONFIG.AXI_SECRET_KEY,
            ClientID: config_1.APP_CONFIG.AXI_CLIENT_ID
        };
        try {
            const result = await http_transport_util_1.requestPOST(url, dataClear, headers);
            return result;
        }
        catch (error) {
            console.log('error', error);
            handle_error_util_1.throwError(Object.assign({ status: common_1.HttpStatus.UNPROCESSABLE_ENTITY }, const_1.ERROR_CODE.UNPROCESSABLE));
        }
    }
}
exports.AxiTransport = AxiTransport;
//# sourceMappingURL=axi.transport.js.map