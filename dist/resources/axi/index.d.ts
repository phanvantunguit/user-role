import { AxiTransport } from 'src/resources/axi/axi.transport';
export declare class AxiResource {
    private axiTransport;
    constructor(axiTransport: AxiTransport);
    checkAccountReferal(account: string): Promise<any>;
}
