import { GCloudTransport } from 'src/resources/storage/google.transport';
export declare class StorageResource {
    private gCloudTransport;
    constructor(gCloudTransport: GCloudTransport);
    upload(file: any): Promise<string>;
}
