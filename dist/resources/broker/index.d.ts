import { AxiResource, IAxiDataCheckAccountReferral } from './axi';
import { BrokerDefaultResource, IMetaapiDataCheckAccountReferral } from './metaapi';
import { IXMDataCheckAccountReferral, XMResource } from './xm';
export declare class BrokerResource {
    private xmResource;
    private axiResource;
    private brokerDefaultResource;
    constructor(xmResource: XMResource, axiResource: AxiResource, brokerDefaultResource: BrokerDefaultResource);
    checkAccountReferral(param: IAxiDataCheckAccountReferral | IXMDataCheckAccountReferral | IMetaapiDataCheckAccountReferral): Promise<boolean>;
}
