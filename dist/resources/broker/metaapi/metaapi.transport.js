"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MetaapiTransport = void 0;
const http_transport_util_1 = require("../../../utils/http-transport.util");
class MetaapiTransport {
    async requestGET(url, apiKey) {
        const headers = {
            'Content-Type': 'application/json;',
            'auth-token': apiKey,
        };
        try {
            const result = await http_transport_util_1.requestGET(url, headers);
            return result;
        }
        catch (error) {
            console.log('error', error);
            return false;
        }
    }
}
exports.MetaapiTransport = MetaapiTransport;
//# sourceMappingURL=metaapi.transport.js.map