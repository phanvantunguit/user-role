"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BrokerDefaultResource = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../../config");
const bot_1 = require("../../../const/bot");
const metaapi_transport_1 = require("./metaapi.transport");
let BrokerDefaultResource = class BrokerDefaultResource {
    constructor(metaapiTransport) {
        this.metaapiTransport = metaapiTransport;
    }
    async checkAccountReferral(param) {
        var _a;
        console.log("MetaapiCheckAccountReferral param", param);
        const { account, broker_profile_id, platform } = param;
        if (!broker_profile_id || !account || !platform) {
            return false;
        }
        const url = `${config_1.APP_CONFIG.MT_URL}/users/current/${platform}/provisioning-profiles/${broker_profile_id}/accounts/${account}/account-information`;
        const res = await this.metaapiTransport.requestGET(url, config_1.APP_CONFIG.MT_TOKEN);
        console.log("MetaapiCheckAccountReferral res", res);
        if (((_a = res === null || res === void 0 ? void 0 : res.login) === null || _a === void 0 ? void 0 : _a.toString()) === account.toString()) {
            return true;
        }
        return false;
    }
};
BrokerDefaultResource = __decorate([
    __param(0, common_1.Inject(metaapi_transport_1.MetaapiTransport)),
    __metadata("design:paramtypes", [metaapi_transport_1.MetaapiTransport])
], BrokerDefaultResource);
exports.BrokerDefaultResource = BrokerDefaultResource;
//# sourceMappingURL=index.js.map