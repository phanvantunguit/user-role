export declare class MetaapiTransport {
    requestGET(url: string, apiKey: string): Promise<any>;
}
