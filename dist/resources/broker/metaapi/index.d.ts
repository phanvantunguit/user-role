import { BROKER_CODE, TBOT_PLATFORM } from 'src/const/bot';
import { MetaapiTransport } from 'src/resources/broker/metaapi/metaapi.transport';
export interface IMetaapiDataCheckAccountReferral {
    broker_code: BROKER_CODE.fxlink;
    account: string;
    broker_profile_id: string;
    platform: TBOT_PLATFORM;
}
export declare class BrokerDefaultResource {
    private metaapiTransport;
    constructor(metaapiTransport: MetaapiTransport);
    checkAccountReferral(param: IMetaapiDataCheckAccountReferral): Promise<any>;
}
