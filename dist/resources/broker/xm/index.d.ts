import { BROKER_CODE } from 'src/const/bot';
import { XMTransport } from 'src/resources/broker/xm/xm.transport';
export interface IXMDataCheckAccountReferral {
    broker_code: BROKER_CODE.xm;
    account: string;
    api_key: string;
}
export declare class XMResource {
    private xmTransport;
    constructor(xmTransport: XMTransport);
    checkAccountReferral(param: IXMDataCheckAccountReferral): Promise<any>;
}
