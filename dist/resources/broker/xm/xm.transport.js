"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.XMTransport = void 0;
const http_transport_util_1 = require("../../../utils/http-transport.util");
const const_1 = require("../../../const");
const handle_error_util_1 = require("../../../utils/handle-error.util");
class XMTransport {
    async requestGET(url, apiKey) {
        const headers = {
            'Content-Type': 'application/json;',
            Authorization: `Bearer ${apiKey}`,
        };
        try {
            const result = await http_transport_util_1.requestGET(url, headers);
            return result;
        }
        catch (error) {
            console.log('error', error);
            return false;
        }
    }
}
exports.XMTransport = XMTransport;
//# sourceMappingURL=xm.transport.js.map