"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.XMResource = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../../config");
const bot_1 = require("../../../const/bot");
const xm_transport_1 = require("./xm.transport");
let XMResource = class XMResource {
    constructor(xmTransport) {
        this.xmTransport = xmTransport;
    }
    async checkAccountReferral(param) {
        const { account, api_key } = param;
        if (!api_key || !account) {
            return false;
        }
        const url = `${config_1.APP_CONFIG.XM_BASE_URL}/api/traders/${account}`;
        const res = await this.xmTransport.requestGET(url, api_key);
        if ((res === null || res === void 0 ? void 0 : res.accountType) === 'STANDARD') {
            return true;
        }
        return false;
    }
};
XMResource = __decorate([
    __param(0, common_1.Inject(xm_transport_1.XMTransport)),
    __metadata("design:paramtypes", [xm_transport_1.XMTransport])
], XMResource);
exports.XMResource = XMResource;
//# sourceMappingURL=index.js.map