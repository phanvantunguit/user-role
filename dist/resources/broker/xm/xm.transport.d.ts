export declare class XMTransport {
    requestGET(url: string, apiKey: string): Promise<any>;
}
