"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BrokerResource = void 0;
const common_1 = require("@nestjs/common");
const bot_1 = require("../../const/bot");
const axi_1 = require("./axi");
const metaapi_1 = require("./metaapi");
const xm_1 = require("./xm");
let BrokerResource = class BrokerResource {
    constructor(xmResource, axiResource, brokerDefaultResource) {
        this.xmResource = xmResource;
        this.axiResource = axiResource;
        this.brokerDefaultResource = brokerDefaultResource;
    }
    async checkAccountReferral(param) {
        switch (param.broker_code) {
            case bot_1.BROKER_CODE.axi:
                return this.axiResource.checkAccountReferral(param);
            case bot_1.BROKER_CODE.xm:
                return this.xmResource.checkAccountReferral(param);
            default:
                return this.brokerDefaultResource.checkAccountReferral(param);
        }
    }
};
BrokerResource = __decorate([
    __param(0, common_1.Inject(xm_1.XMResource)),
    __param(1, common_1.Inject(axi_1.AxiResource)),
    __param(2, common_1.Inject(metaapi_1.BrokerDefaultResource)),
    __metadata("design:paramtypes", [xm_1.XMResource,
        axi_1.AxiResource,
        metaapi_1.BrokerDefaultResource])
], BrokerResource);
exports.BrokerResource = BrokerResource;
//# sourceMappingURL=index.js.map