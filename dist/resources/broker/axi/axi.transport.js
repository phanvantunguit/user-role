"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AxiTransport = void 0;
const http_transport_util_1 = require("../../../utils/http-transport.util");
const config_1 = require("../../../config");
const format_data_util_1 = require("../../../utils/format-data.util");
const const_1 = require("../../../const");
const handle_error_util_1 = require("../../../utils/handle-error.util");
class AxiTransport {
    async requestPOST(url, secretKey, clientID) {
        const headers = {
            'Content-Type': 'application/json;',
            SecretKey: secretKey,
            ClientID: clientID
        };
        try {
            const result = await http_transport_util_1.requestPOST(url, {}, headers);
            return result;
        }
        catch (error) {
            console.log('error', error);
            return false;
        }
    }
}
exports.AxiTransport = AxiTransport;
//# sourceMappingURL=axi.transport.js.map