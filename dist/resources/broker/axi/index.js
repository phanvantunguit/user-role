"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AxiResource = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../../config");
const app_setting_1 = require("../../../const/app-setting");
const bot_1 = require("../../../const/bot");
const axi_transport_1 = require("./axi.transport");
let AxiResource = class AxiResource {
    constructor(axiTransport) {
        this.axiTransport = axiTransport;
    }
    async checkAccountReferral(param) {
        console.log('checkAccountReferral param', param);
        const { account, agent_account, client_id, secret_key } = param;
        if (!secret_key || !client_id || !agent_account || !account) {
            return false;
        }
        const url = `${config_1.APP_CONFIG.AXI_BASE_URL}/api/ExternalApi/GetAccountReports?MT4AccountNumber=${account}`;
        const res = await this.axiTransport.requestPOST(url, secret_key, client_id);
        console.log("axiTransport res");
        if (res) {
            return res.Data.some((e) => e.AgentAccount == agent_account);
        }
        return false;
    }
};
AxiResource = __decorate([
    __param(0, common_1.Inject(axi_transport_1.AxiTransport)),
    __metadata("design:paramtypes", [axi_transport_1.AxiTransport])
], AxiResource);
exports.AxiResource = AxiResource;
//# sourceMappingURL=index.js.map