import { BROKER_CODE } from 'src/const/bot';
import { AxiTransport } from 'src/resources/broker/axi/axi.transport';
export interface IAxiDataCheckAccountReferral {
    broker_code: BROKER_CODE.axi;
    account: string;
    agent_account: string;
    client_id: string;
    secret_key: string;
}
export declare class AxiResource {
    private axiTransport;
    constructor(axiTransport: AxiTransport);
    checkAccountReferral(param: IAxiDataCheckAccountReferral): Promise<any>;
}
