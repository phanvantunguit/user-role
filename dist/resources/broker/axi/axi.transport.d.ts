export declare class AxiTransport {
    requestPOST(url: string, secretKey: string, clientID: string): Promise<any>;
}
