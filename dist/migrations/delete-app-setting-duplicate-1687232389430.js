"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteAppSettingDuplicate1687232389430 = void 0;
class DeleteAppSettingDuplicate1687232389430 {
    async up(queryRunner) {
        const deleteString = `
    DELETE FROM app_settings ast
    WHERE user_id is null and (SELECT COUNT (*) FROM app_settings WHERE name = ast.name) > 1
    `;
        await queryRunner.query(deleteString);
        const updateString = `
    UPDATE app_settings
    SET user_id = 'system'
    WHERE user_id is null`;
        await queryRunner.query(updateString);
    }
    down(queryRunner) {
        return;
    }
}
exports.DeleteAppSettingDuplicate1687232389430 = DeleteAppSettingDuplicate1687232389430;
//# sourceMappingURL=delete-app-setting-duplicate-1687232389430.js.map