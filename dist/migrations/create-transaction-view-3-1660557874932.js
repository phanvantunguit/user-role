"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateTransactionView31660557874932 = void 0;
class CreateTransactionView31660557874932 {
    async up(queryRunner) {
        const dropTransactionView = `DROP VIEW IF EXISTS transactions_view`;
        const dropTransactionDetailView = `DROP VIEW IF EXISTS transaction_details_view`;
        const createTransactionDetailView = `
        CREATE OR REPLACE VIEW transaction_details_view AS
        select td.*,
        r.role_name as role_name
        from transaction_details td
        left join roles r on r.id = td.role_id
        `;
        const createTransactionView = `
        CREATE OR REPLACE VIEW transactions_view AS
        select t.*,
        u.email as email,
        u.username as username,
        u.first_name as first_name,
        u.last_name as last_name,
        u.phone as phone,
        COALESCE(json_agg(DISTINCT jsonb_build_object(
          'id', tdv.id,
          'currency', tdv.currency,
          'price', tdv.price,
          'role_id', tdv.role_id,
          'role_name', tdv.role_name,
          'transaction_id', tdv.transaction_id,
          'user_id', tdv.user_id,
          'package_id',tdv.package_id,
          'package_type',tdv.package_type,
          'package_name',tdv.package_name,
          'discount_rate',tdv.discount_rate,
          'discount_amount',tdv.discount_amount,
          'quantity',tdv.quantity,
          'expires_at',tdv.expires_at,
          'created_at',tdv.created_at
          )) FILTER (WHERE tdv.id IS NOT NULL), '[]'::json) AS details
        from transactions t
        left join users u on u.id = t.user_id
        left join transaction_details_view tdv on tdv.transaction_id = t.id 
        GROUP BY t.id, u.id
        `;
        await queryRunner.query(dropTransactionView);
        await queryRunner.query(dropTransactionDetailView);
        await queryRunner.query(createTransactionDetailView);
        await queryRunner.query(createTransactionView);
    }
    down(queryRunner) {
        return;
    }
}
exports.CreateTransactionView31660557874932 = CreateTransactionView31660557874932;
//# sourceMappingURL=create-transaction-view-3-1660557874932.js.map