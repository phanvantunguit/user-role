"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateUserView81659964513727 = void 0;
class CreateUserView81659964513727 {
    async up(queryRunner) {
        const dropUserView = `DROP VIEW IF EXISTS user_view`;
        const dropRolesView = `DROP VIEW IF EXISTS roles_view`;
        const createRolesView = `
        CREATE OR REPLACE VIEW roles_view AS
        select 
        r.id,
        r.role_name,
        r.description,
        r.owner_created,
        r.updated_at,
        r.created_at,
        json_build_object(
        'symbol_settings_roles', COALESCE(json_agg(DISTINCT jsonb_build_object(
            	'id', ssr.id,
                'resolutions', ssr.resolutions,
                'exchanges', ssr.exchanges,
                'symbols', ssr.symbols,
                'description', ssr.description
        )) FILTER (WHERE ssr.id IS NOT NULL), '[]'),
		'features', COALESCE(json_agg(DISTINCT f) FILTER (WHERE f IS NOT NULL), '[]'),
  
        'general_settings', COALESCE(json_agg(DISTINCT jsonb_build_object(
            'general_setting_id', gs.general_setting_id,
            'general_setting_name', gs.general_setting_name,
            'description', gsr.description,
            'val_limit', gsr.val_limit,
            'owner_created', gsr.owner_created,
            'created_at', gsr.created_at,
            'updated_at', gsr.updated_at
        )) FILTER (WHERE gs.general_setting_id IS NOT NULL), '[]')
        ) as root,
        r.status,
        r.type,
        r.price,
        r.currency,
        r.parent_id,
        r.is_best_choice,
        r.order,
        r.description_features,
        r.color
        from roles r
        left join (select 
                    ssr.id, 
                    ssr.role_id,
                    ssr.description,
                    COALESCE(json_agg(DISTINCT rs) FILTER (WHERE rs.id IS NOT NULL), '[]') AS resolutions,
                    COALESCE(json_agg(DISTINCT e) FILTER (WHERE e.exchange_name IS NOT NULL), '[]') AS exchanges,
                    COALESCE(json_agg(DISTINCT s) FILTER (WHERE s.symbol IS NOT NULL), '[]') AS symbols
                    from (select 
                            ssr.id, 
                            ssr.role_id, 
                            ssr.description,
                            jsonb_array_elements(ssr.supported_resolutions) as resolutions_id,
                            jsonb_array_elements(ssr.list_exchanged) as exchanges_name,
                            jsonb_array_elements(ssr.list_symbol) as symbol
                            from symbol_settings_roles ssr) as ssr 
                    left join resolutions rs on ssr.resolutions_id::jsonb::text = concat('"',rs.id,'"')
                    left join symbols s on ssr.symbol::jsonb::text = concat('"',s.symbol,'"')
                    left join exchanges e on ssr.exchanges_name::jsonb::text = concat('"',e.exchange_name,'"')
                    GROUP BY ssr.role_id, ssr.id, ssr.description) as ssr on r.id = ssr.role_id
        left join feature_roles fr on r.id = fr.role_id
        left join features f on fr.feature_id = f.feature_id
        left join general_settings_roles gsr on r.id = gsr.role_id
        left join general_settings gs on gsr.general_setting_id = gs.general_setting_id
        group by r.id
        order by r.created_at   
        `;
        const createUserView = `
    CREATE OR REPLACE VIEW user_view AS
        SELECT u.id,
        u.email,
        u.username,
        u.phone,
        u.first_name,
        u.last_name,
        u.address,
        u.affiliate_code,
        u.link_affiliate,
        u.referral_code,
        u.profile_pic,
        u.active,
        u.email_confirmed,
        u.note_updated,
        u.date_registered,
        u.super_user,
        u.is_admin,
        u.created_at,
        u.updated_at,
        COALESCE(json_agg(DISTINCT jsonb_build_object(
        'id', rv.id, 
        'role_name', rv.role_name, 
        'description', rv.description, 
        'owner_created', rv.owner_created, 
        'updated_at', rv.updated_at, 
        'created_at', rv.created_at, 
        'root', rv.root, 
        'expires_at', ur.expires_at,
        'quantity', ur.quantity,
        'package_name', ur.package_name,
        'package_type', ur.package_type,
        'package_id', ur.package_id,
        'status', rv.status,
        'type', rv.type,
        'price', rv.price,
        'currency', rv.currency,
        'parent_id', rv.parent_id,
        'is_best_choice', rv.is_best_choice,
        'order', rv.order,
        'description_features', rv.description_features,
        'color', rv.color
        )) FILTER (WHERE rv.id IS NOT NULL), '[]'::json) AS roles,
        COALESCE(json_agg(DISTINCT ar) FILTER (WHERE ar.id IS NOT NULL), '[]'::json) AS auth_roles,
        u.country,
        u.year_of_birth,
        u.gender,
        u.phone_code
        FROM users u
         LEFT JOIN user_roles ur ON u.id = ur.user_id
         LEFT JOIN roles_view rv ON rv.id = ur.role_id
         LEFT JOIN auth_user_roles aur ON u.id = aur.user_id
         LEFT JOIN auth_roles ar ON ar.id = aur.auth_role_id
        GROUP BY u.id
        ORDER BY u.created_at DESC;
        `;
        await queryRunner.query(dropUserView);
        await queryRunner.query(dropRolesView);
        await queryRunner.query(createRolesView);
        await queryRunner.query(createUserView);
    }
    down(queryRunner) {
        return;
    }
}
exports.CreateUserView81659964513727 = CreateUserView81659964513727;
//# sourceMappingURL=create-user-view-8-1659964513727.js.map