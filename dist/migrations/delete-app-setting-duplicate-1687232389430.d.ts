import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class DeleteAppSettingDuplicate1687232389430 implements MigrationInterface {
    name: 'DeleteAppSettingDuplicate1687232389430';
    up(queryRunner: QueryRunner): Promise<any>;
    down(queryRunner: QueryRunner): Promise<any>;
}
