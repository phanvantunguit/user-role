"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InsertInitRoleData1653992361403 = void 0;
const permission_1 = require("../const/permission");
class InsertInitRoleData1653992361403 {
    async up(queryRunner) {
        const queryString = `
        INSERT INTO roles (
            id, role_name, root, description, owner_created, created_at, updated_at)
            VALUES ('${permission_1.DEFAULT_ROLE.id}','${permission_1.DEFAULT_ROLE.role_name}', '${JSON.stringify(permission_1.DEFAULT_ROLE.root)}', '${permission_1.DEFAULT_ROLE.description}', '${permission_1.DEFAULT_ROLE.owner_created}', ${permission_1.DEFAULT_ROLE.created_at}, ${permission_1.DEFAULT_ROLE.updated_at})
            ON CONFLICT (id) 
            DO NOTHING;
        `;
        await queryRunner.query(queryString);
    }
    down(queryRunner) {
        return;
    }
}
exports.InsertInitRoleData1653992361403 = InsertInitRoleData1653992361403;
//# sourceMappingURL=insert-init-role-data-1653992361403.js.map