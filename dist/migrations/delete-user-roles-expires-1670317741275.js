"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteUserRolesExpires1670317741275 = void 0;
class DeleteUserRolesExpires1670317741275 {
    async up(queryRunner) {
        const queryString = `
    DELETE FROM user_roles
    where expires_at < ${Date.now()}
    `;
        await queryRunner.query(queryString);
    }
    down(queryRunner) {
        return;
    }
}
exports.DeleteUserRolesExpires1670317741275 = DeleteUserRolesExpires1670317741275;
//# sourceMappingURL=delete-user-roles-expires-1670317741275.js.map