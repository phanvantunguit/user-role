export enum ON_OFF_REGISTER {
  ON = 'ON',
  OFF = 'OFF',
}
export const APP_SETTING = {
  ON_OFF_REGISTER: 'ON_OFF_REGISTER',
  SIGNAL_PLATFORM: 'SIGNAL_PLATFORM',
  MERCHANT_CODE_DEFAULT: 'CM',
  ON_OFF_LOCALSTORAGE: 'ON_OFF_LOCALSTORAGE',
  NUMBER_OF_TBOT: 'NUMBER_OF_TBOT',
  NUMBER_OF_TBOT_USED: 'NUMBER_OF_TBOT_USED',
  TBOT_INACTIVE_BY_SYSTEM: 'TBOT_INACTIVE_BY_SYSTEM',
  BROKER_SERVER: 'BROKER_SERVER',
  CURRENCY_PKG: 'CURRENCY_PKG',
  CURRENCY_SBOT: 'CURRENCY_SBOT',
  CURRENCY_TBOT: 'CURRENCY_TBOT',
  BROKER_SETTING: 'BROKER_SETTING',
}
export const SERVICE_NAME = 'cm-user-roles'

export const CHANELS = {
  WS_CHANEL: `${SERVICE_NAME}ws`,
  SIGNAL_BOT_CHANEL: 'SIGNAL.BOT',
  TBOT_CHANEL_TS: 'TBOT_CHANEL_TS',
  TBOT_CHANEL_UR: 'TBOT_CHANEL_UR',
}

export const ConnectionName = {
  user_role: 'user_role',
  payment_service: 'payment_service',
  dmlcmghrkhmif: 'dmlcmghrkhmif'
}

export type SettingSignalPlatfrom = {
  ON_OFF_PUSH_NOTIFICATION: 'ON' | 'OFF'
  ON_OFF_SOUND_NOTIFICATION: 'ON' | 'OFF'
  OFF_SIGNALS_BOT: string
}
export enum EVENT_STORE_STATE {
  OPEN = 'OPEN',
  PROCESSING = 'PROCESSING',
  ERROR = 'ERROR',
  CLOSED = 'CLOSED',
}
export enum EVENT_STORE_NAME {
  TBOT_INACTIVE_BY_SYSTEM = 'TBOT_INACTIVE_BY_SYSTEM',
  TBOT_STOP_INACTIVE_BY_SYSTEM = 'TBOT_STOP_INACTIVE_BY_SYSTEM',
  TBOT_CONNECTED = 'TBOT_CONNECTED',
  INP_PAYMENT_TBOT = 'INP_PAYMENT_TBOT',
  TBOT_SYNC = 'TBOT_SYNC',
  TBOT_STOP_OUT = 'TBOT_STOP_OUT',
  TBOT_ACTIVE_PROCESSING = 'TBOT_ACTIVE_PROCESSING',
  TBOT_CONNECTING = 'TBOT_CONNECTING',
}