export const Brokers = [
  {
    code: 'axi',
    name: 'AxiCorp Financial Services Pty Ltd',
  },
  {
    code: 'xm',
    name: 'XmCorp Financial Services Pty Ltd',
  },
]
export const BrokerServers = {
  xm: ['XMGlobal-Demo'],
  axi: ['Axi.SVG-US10-Live', 'Axi.SVG-US03-Demo', 'Axi.SVG-US888-Demo'],
}

export enum BROKER_CODE {
  axi = 'axi',
  xm = 'xm',
  fxlink = 'fxlink'
}

export enum BOT_TRADING_EVENT {
  CONNECTING = 'CONNECTING',
  CONNECTED = 'CONNECTED',
  INACTIVE_BY_SYSTEM = 'INACTIVE_BY_SYSTEM',
  DISCONNECTED = 'DISCONNECTED',
  EXPIRED = 'EXPIRED',
}

export enum TRADE_HISTORY_STATUS {
  OPEN = 'OPEN',
  CLOSED = 'CLOSED',
}

export enum TRADE_SIDE {
  LONG = 'LONG',
  SHORT = 'SHORT',
}

export enum TBOT_CHANEL_TS_EVENT {
  trades = 'trades',
  account = 'account',
  connect_stream = 'connect_stream',
  disconnect_stream = 'disconnect_stream',
  strategy_trades = 'strategy_trades',
  add_strategy = 'add_strategy',
  add_subscriber = 'add_subscriber',
}

export enum TBOT_PLATFORM {
  mt4 = 'mt4',
  mt5 = 'mt5'
}