export const EMAIL_SUBJECT = {
  registration: 'Confirm account registration',
  reset_password: 'Reset your password',
  verify_email: 'Verify email',
  transaction_information: 'Transaction information',
  payment_changes: 'Payment changes',
  payment_refund: 'Payment refund',
  upgrade_account_success: 'Upgrade account success',
  change_email: 'Email Change Confirmation',
}
