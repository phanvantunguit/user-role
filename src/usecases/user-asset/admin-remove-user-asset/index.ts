import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE, PACKAGE_TYPE } from 'src/const'
import { ITEM_STATUS, ORDER_CATEGORY } from 'src/const/transaction'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { BotRepository } from 'src/repositories/bot.repository'
import { PermissionRepository } from 'src/repositories/permission.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { throwError } from 'src/utils/handle-error.util'
import { AdminRemoveUserAssetInput } from './validate'

export class AdminRemoveUserAssetHandler {
  constructor(
    @Inject(PermissionRepository)
    private permissionRepository: PermissionRepository,
    @Inject(BotRepository)
    private botRepository: BotRepository,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(UserRepository) private userRepository: UserRepository
  ) {}
  async execute(
    params: AdminRemoveUserAssetInput,
    ownerCreated: string
  ): Promise<any> {
    const { user_id, category, asset_id } = params
    const updateUserAssetLog = {
      user_id: user_id,
      category,
      status: ITEM_STATUS.DELETED,
      owner_created: ownerCreated,
    }
    let asset
    switch (category) {
      case ORDER_CATEGORY.PKG:
        asset = await this.permissionRepository.findOneRoleTable({
          id: asset_id,
        })
        if (asset) {
          await this.userRepository.deleteUserRoles([user_id], [asset_id])
          updateUserAssetLog['name'] = asset.role_name
        }
        break
      case ORDER_CATEGORY.SBOT:
        asset = await this.botRepository.findById(asset_id)
        if (asset) {
          await this.userRepository.deleteUserBot({
            user_id,
            bot_id: asset_id,
          })
          updateUserAssetLog['name'] = asset.name
        }
        break
      case ORDER_CATEGORY.TBOT:
        asset = await this.botTradingRepository.findById(asset_id)
        if (asset) {
          const userBot = await this.userRepository.findUserBotTrading({
            user_id,
            bot_id: asset_id,
          })
          if (userBot.length > 0 && userBot[0].subscriber_id) {
            throwError({
              status: HttpStatus.BAD_REQUEST,
              ...ERROR_CODE.ACCOUNT_CONNECTED,
            })
          }
          await this.userRepository.deleteUserBotTrading({
            user_id,
            bot_id: asset_id,
          })
          updateUserAssetLog['name'] = asset.name
        }
        break
      default:
        break
    }
    if (!asset) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.NOT_FOUND,
      })
    }
    updateUserAssetLog['asset_id'] = asset.id
    await this.userRepository.saveUserAssetLog([updateUserAssetLog])
  }
}
