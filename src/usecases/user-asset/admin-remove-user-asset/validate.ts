import { ApiProperty } from '@nestjs/swagger'
import { IsIn, IsString } from 'class-validator'
import { ORDER_CATEGORY } from 'src/const/transaction'

export class AdminRemoveUserAssetInput {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  user_id: string

  @ApiProperty({
    required: true,
    description: `category of item: ${Object.values(ORDER_CATEGORY)}`,
    example: ORDER_CATEGORY.SBOT,
  })
  @IsIn(Object.values(ORDER_CATEGORY))
  @IsString()
  category: ORDER_CATEGORY

  @ApiProperty({
    required: true,
    description: 'item id',
  })
  @IsString()
  asset_id: string
}
