import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE, PACKAGE_TYPE } from 'src/const'
import { ITEM_STATUS, ORDER_CATEGORY } from 'src/const/transaction'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { BotRepository } from 'src/repositories/bot.repository'
import { PermissionRepository } from 'src/repositories/permission.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { throwError } from 'src/utils/handle-error.util'
import { AdminAddUserAssetInput } from './validate'

export class AdminAddUserAssetHandler {
  constructor(
    @Inject(PermissionRepository)
    private permissionRepository: PermissionRepository,
    @Inject(BotRepository)
    private botRepository: BotRepository,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(UserRepository) private userRepository: UserRepository
  ) {}
  async execute(
    params: AdminAddUserAssetInput,
    ownerCreated: string
  ): Promise<any> {
    const { user_id, category, asset_id, quantity, type } = params
    let expiresAt = null
    if (quantity) {
      const expiredTime = new Date()
      if (type === PACKAGE_TYPE.DAY) {
        expiredTime.setDate(expiredTime.getDate() + quantity)
      } else {
        expiredTime.setMonth(expiredTime.getMonth() + quantity)
      }
      expiresAt = expiredTime.valueOf()
    }
    const dataAdd = {
      user_id,
      expires_at: expiresAt,
      owner_created: ownerCreated,
    }
    const updateUserAssetLog = {
      user_id: user_id,
      category,
      status: ITEM_STATUS.ACTIVE,
      owner_created: ownerCreated,
      expires_at: expiresAt,
      quantity,
      package_type: type,
    }
    let asset
    let user_asset
    switch (category) {
      case ORDER_CATEGORY.PKG:
        asset = await this.permissionRepository.findOneRoleTable({
          id: asset_id,
        })
        user_asset = await this.userRepository.findUserRole({
          user_id,
          role_id: asset_id,
        })
        dataAdd['role_id'] = asset_id
        if (asset) {
          if (user_asset[0]) {
            dataAdd['id'] = user_asset[0].id
          }
          updateUserAssetLog['name'] = asset.role_name
          //@ts-ignore
          await this.userRepository.saveUserRole([dataAdd])
        }
        break
      case ORDER_CATEGORY.SBOT:
        asset = await this.botRepository.findById(asset_id)
        user_asset = await this.userRepository.findUserBot({
          user_id,
          bot_id: asset_id,
        })
        dataAdd['bot_id'] = asset_id
        if (asset) {
          if (user_asset[0]) {
            dataAdd['id'] = user_asset[0].id
          }
          dataAdd['status'] = ITEM_STATUS.ACTIVE
          updateUserAssetLog['name'] = asset.name
          //@ts-ignore
          await this.userRepository.saveUserBot([dataAdd])
        }
        break
      case ORDER_CATEGORY.TBOT:
        asset = await this.botTradingRepository.findById(asset_id)
        user_asset = await this.userRepository.findUserBotTrading({
          user_id,
          bot_id: asset_id,
        })
        dataAdd['bot_id'] = asset_id
        if (asset) {
          if (user_asset[0]) {
            dataAdd['id'] = user_asset[0].id
          }
          dataAdd['status'] = ITEM_STATUS.NOT_CONNECT
          updateUserAssetLog['name'] = asset.name
          //@ts-ignore
          await this.userRepository.saveUserBotTrading([dataAdd])
        }
        break
      default:
        break
    }
    if (!asset) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.NOT_FOUND,
      })
    }
    updateUserAssetLog['asset_id'] = asset.id
    await this.userRepository.saveUserAssetLog([updateUserAssetLog])
  }
}
