import { Inject } from '@nestjs/common'
import { ROLE_STATUS } from 'src/const'
import { PermissionRepository } from 'src/repositories/permission.repository'
import { UserRepository } from 'src/repositories/user.repository'
export class UserListRoleHandler {
  constructor(
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(PermissionRepository) private permissionRepo: PermissionRepository
  ) {}
  async execute(user_id?: string, status?: string[]): Promise<any[]> {
    const roles = await this.permissionRepo.findRolesConditionMultiple({status})
    if (!user_id) {
      return roles
    }
    const userRoles = await this.userRepository.findUserRole({
      user_id,
    })
    const userRolesFilter = []
    userRoles.forEach((ur) => {
      const indexUR = userRolesFilter.findIndex(
        (urf) => urf.role_id === ur.role_id
      )
      if (indexUR > -1) {
        if (
          ur.expires_at === null ||
          Number(ur.expires_at) > Number(userRolesFilter[indexUR].expires_at)
        ) {
          userRolesFilter.slice(indexUR, 1)
          userRolesFilter.push(ur)
        }
      } else {
        userRolesFilter.push(ur)
      }
    })
    const mapRoles = roles.map((r) => {
      const userRoleExit = userRolesFilter.find((ub) => ub.role_id === r.id)
      if (userRoleExit) {
        return {
          ...r,
          expires_at: userRoleExit.expires_at,
        }
      } else {
        return r
      }
    })
    return mapRoles
  }
}
