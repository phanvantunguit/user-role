import { ApiProperty } from '@nestjs/swagger'
import { IsString, IsEmail, IsOptional } from 'class-validator'
import { MERCHANT_STATUS } from 'src/const'

export class MerchantValidate {
  @ApiProperty({
    required: true,
    description: 'name',
  })
  @IsString()
  name?: string
  @ApiProperty({
    required: true,
    description: 'code',
  })
  @IsString()
  code?: string
  @ApiProperty({
    required: true,
    description: 'email',
  })
  @IsEmail()
  email?: string
  @ApiProperty({
    required: true,
    description: `${Object.keys(MERCHANT_STATUS).join(',')}`,
    example: MERCHANT_STATUS.ACTIVE,
  })
  @IsString()
  status?: MERCHANT_STATUS
  @ApiProperty({
    required: false,
    description: 'code',
  })
  @IsString()
  @IsOptional()
  description?: string

  @ApiProperty({
    required: false,
    description: 'domain',
  })
  @IsString()
  @IsOptional()
  domain?: string
}
