import { Inject } from '@nestjs/common'
import { MerchantRepository } from 'src/repositories/merchant.repository'
import { MerchantValidate } from '../validate'

export class AdminListMerchantHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository
  ) {}
  async execute(): Promise<MerchantValidate[]> {
    const merchants = await this.merchantRepository.find({})
    const result = merchants.map((e) => {
      delete e.password
      return e
    })
    return result
  }
}
