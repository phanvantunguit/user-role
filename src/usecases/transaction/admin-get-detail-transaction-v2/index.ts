import { Inject } from '@nestjs/common'
import { TransactionPSRepository } from 'src/repositories/transaction-ps.repository'
import { AdminGetTransactionV2Output } from './validate'

export class AdminGetTransactionV2Handler {
  constructor(
    @Inject(TransactionPSRepository)
    private transactionPSRepository: TransactionPSRepository
  ) {}
  async execute(params: { id: string }): Promise<AdminGetTransactionV2Output> {
    const dataRes = await this.transactionPSRepository.getDetail(params)
    return dataRes
  }
}
