import { ApiProperty } from '@nestjs/swagger'
import { IsObject } from 'class-validator'
import {
  AdminListTransactionV2Validate,
  TransactionV2LogValidate,
  TransactionV2MetadataValidate,
} from '../validate'

export class AdminGetTransactionV2Output {
  transaction: AdminListTransactionV2Validate
  metadatas: TransactionV2MetadataValidate[]
  logs: TransactionV2LogValidate[]
}
export class AdminGetTransactionV2PayloadOutput {
  @ApiProperty({
    required: false,
    type: AdminGetTransactionV2Output,
  })
  @IsObject()
  payload: AdminGetTransactionV2Output
}
