import { Inject } from '@nestjs/common'
import { TransactionPSRepository } from 'src/repositories/transaction-ps.repository'

import {
  AdminListTransactionV2Input,
  AdminListTransactionV2Output,
} from './validate'
export class AdminListTransactionV2Handler {
  constructor(
    @Inject(TransactionPSRepository)
    private transactionPSRepository: TransactionPSRepository
  ) {}
  async execute(
    params: AdminListTransactionV2Input
  ): Promise<AdminListTransactionV2Output> {
    const dataRes = await this.transactionPSRepository.getPaging(params)
    return dataRes
  }
}
