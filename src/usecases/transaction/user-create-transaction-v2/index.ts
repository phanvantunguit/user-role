import { HttpStatus, Inject } from '@nestjs/common'
import { APP_CONFIG } from 'src/config'
import { ERROR_CODE } from 'src/const'
import { SERVICE_NAME } from 'src/const/app-setting'
import { DBContext } from 'src/repositories/db-context'
import { UserRepository } from 'src/repositories/user.repository'
import { CmpaymentResource } from 'src/resources/cmpayment'
import { CheckItemService } from 'src/services/transaction-v2/check-Items'
import { SaveItemService } from 'src/services/transaction-v2/save-items'
import { throwError } from 'src/utils/handle-error.util'
import { CreateTransactionV2Input, CreateTransactionV2Output } from './validate'

export class CreateTransactionV2Handler {
  constructor(
    @Inject(CheckItemService)
    private checkItemService: CheckItemService,
    @Inject(SaveItemService)
    private saveItemService: SaveItemService,
    @Inject(DBContext) private dBContext: DBContext,
    @Inject(CmpaymentResource) private cmpaymentResource: CmpaymentResource,
    @Inject(UserRepository) private userRepository: UserRepository
  ) {}
  async execute(
    param: CreateTransactionV2Input,
    user_id: string,
    email: string,
    merchant_code: string
  ): Promise<CreateTransactionV2Output> {
    const user = await this.userRepository.findOneUser({ id: user_id })
    if (!user) {
      throwError({
        status: HttpStatus.FORBIDDEN,
        ...ERROR_CODE.USER_NOT_FOUND,
      })
    }
    const { items, commission_cash } = await this.checkItemService.execute({
      merchant_code,
      items: param.items,
      amount: param.amount,
    })
    const orderTypes = []
    for (let item of param.items) {
      if (!orderTypes.includes(item.category)) {
        orderTypes.push(item.category)
      }
    }
    const orderType = orderTypes.join('-')
    // save the item
    return await this.dBContext.runInTransaction(async (queryRunner) => {
      const data = {
        merchant_code,
        order_type: orderType,
        amount: Number(param.amount),
        currency: param.currency,
        payment_method: param.payment_method,
        integrate_service: SERVICE_NAME,
        user: {
          id: user_id,
          email,
          fullname: `${user.first_name || ''} ${user.last_name || ''}`.trim(),
        },
        items,
        ipn_url: `${APP_CONFIG.BASE_URL}/api/v2/user/transaction/ipn`,
        commission_cash,
      }
      const dataRes = await this.cmpaymentResource.createTransaction(data)
      if (dataRes.error_code === ERROR_CODE.SUCCESS.error_code) {
        const paymentResult = dataRes.payload
        if (paymentResult.order_id) {
          const userAssetLogs = await this.userRepository.findUserAssetLog({
            order_id: paymentResult.order_id,
          })
          console.log('userAssetLogs', userAssetLogs)
          if (userAssetLogs.length === 0) {
            await this.saveItemService.execute(
              {
                items: items,
                user_id,
                owner_created: user_id,
                order_id: paymentResult.order_id,
              },
              queryRunner
            )
          }
        }
        return {
          transaction_id: paymentResult.id,
          order_id: paymentResult.order_id,
          amount: paymentResult.amount,
          currency: paymentResult.currency,
          payment_method: paymentResult.payment_method,
          status: paymentResult.status,
          payment_id: paymentResult.payment_id,

          wallet_address: paymentResult.wallet_address,
          timeout: paymentResult.timeout?.toString(),
          qrcode_url: paymentResult.qrcode_url,
          status_url: paymentResult.status_url,
          checkout_url: paymentResult.checkout_url,
          updated_at: paymentResult.updated_at,
          created_at: paymentResult.created_at,
        }
      }
      return dataRes
    })
  }
}
