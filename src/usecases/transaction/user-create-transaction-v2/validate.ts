import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsArray,
  IsIn,
  IsNumber,
  IsObject,
  IsOptional,
  IsString,
  ValidateIf,
  ValidateNested,
} from 'class-validator'
import { PACKAGE_TYPE } from 'src/const'
import {
  ORDER_CATEGORY,
  PAYMENT_METHOD,
  TRANSACTION_STATUS,
} from 'src/const/transaction'

export class ItemDataValidate {
  @ApiProperty({
    required: true,
    description: 'item id',
  })
  @IsString()
  id: string

  @ApiProperty({
    required: true,
    description: 'quantity of item',
  })
  @IsNumber()
  quantity: number

  @ApiProperty({
    required: true,
    description: 'type of item',
    example: PACKAGE_TYPE.MONTH,
  })
  @IsIn(Object.values(PACKAGE_TYPE))
  @IsString()
  type: PACKAGE_TYPE

  @ApiProperty({
    required: true,
    description: `category of item: ${Object.values(ORDER_CATEGORY)}`,
    example: ORDER_CATEGORY.SBOT,
  })
  @IsIn(Object.values(ORDER_CATEGORY))
  @IsString()
  category: ORDER_CATEGORY
  
  @ApiProperty({
    required: false,
    description: 'balance',
  })
  @IsOptional()
  @IsNumber()
  @ValidateIf((o) => o.category === ORDER_CATEGORY.TBOT)
  balance?: number;
}

export class CreateTransactionV2Input {
  @ApiProperty({
    required: true,
    description: 'Payment method',
    example: PAYMENT_METHOD.COIN_PAYMENT,
  })
  @IsString()
  @IsIn(Object.values(PAYMENT_METHOD))
  payment_method: PAYMENT_METHOD

  @ApiProperty({
    required: true,
    type: ItemDataValidate,
    isArray: true,
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => ItemDataValidate)
  items: ItemDataValidate[]

  @ApiProperty({
    required: true,
    example: '2',
  })
  @IsString()
  amount: string

  @ApiProperty({
    required: true,
    description: 'Currency for user payment',
    example: 'LTCT',
  })
  @IsString()
  currency: string
}

export class CreateTransactionV2Output {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  transaction_id: string

  @ApiProperty({
    required: true,
    example: 'BOT1802220001',
  })
  @IsString()
  order_id: string

  @ApiProperty({
    required: true,
    example: '2',
  })
  @IsString()
  amount: string

  @ApiProperty({
    required: true,
    description: 'Currency for user payment',
    example: 'LTCT',
  })
  @IsString()
  currency: string

  @ApiProperty({
    required: true,
    description: 'Payment method',
    example: PAYMENT_METHOD.COIN_PAYMENT,
  })
  @IsString()
  @IsIn(Object.values(PAYMENT_METHOD))
  payment_method: PAYMENT_METHOD

  @ApiProperty({
    required: true,
    description: Object.values(TRANSACTION_STATUS).join(','),
    example: TRANSACTION_STATUS.CREATED,
  })
  @IsString()
  status: TRANSACTION_STATUS

  @ApiProperty({
    required: true,
    description: 'Payment id of coinpayment',
    example: 'OAINONOFASF1203412',
  })
  @IsString()
  payment_id: string

  @ApiProperty({
    required: true,
    example: 'wdfghjkloiu1yt2r367893',
  })
  @IsString()
  wallet_address: string

  @ApiProperty({
    required: true,
    description: 'timeout second',
    example: '5000',
  })
  @IsString()
  timeout: string
  @ApiProperty({
    required: true,
    description: 'link qrcode',
  })
  @IsString()
  qrcode_url: string
  @ApiProperty({
    required: true,
    description: 'link status',
  })
  @IsString()
  status_url: string
  @ApiProperty({
    required: true,
    description: 'link checkout',
  })
  @IsString()
  checkout_url: string
  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsString()
  created_at: number

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsString()
  updated_at: number
}

export class CreateTransactionV2PayloadOutput {
  @ApiProperty({
    required: true,
    description: 'Transaction info',
  })
  @IsObject()
  payload: CreateTransactionV2Output
}
