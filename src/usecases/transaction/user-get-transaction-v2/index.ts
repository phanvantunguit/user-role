import { Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { CmpaymentResource } from 'src/resources/cmpayment'
import { DetailTransactionV2Output } from './validate'
export class GetTransactionV2Handler {
  constructor(
    @Inject(CmpaymentResource) private cmpaymentResource: CmpaymentResource
  ) {}
  async execute(id: string): Promise<DetailTransactionV2Output> {
    const dataRes = await this.cmpaymentResource.getTransaction(id)
    if (dataRes.error_code === ERROR_CODE.SUCCESS.error_code) {
      const paymentResult = dataRes.payload
      return {
        transaction_id: paymentResult.id,
        order_id: paymentResult.order_id,
        amount: paymentResult.amount,
        currency: paymentResult.currency,
        payment_method: paymentResult.payment_method,
        status: paymentResult.status,
        payment_id: paymentResult.payment_id,
        items: paymentResult.items,
        wallet_address: paymentResult.wallet_address,
        timeout: paymentResult.timeout?.toString(),
        qrcode_url: paymentResult.qrcode_url,
        status_url: paymentResult.status_url,
        checkout_url: paymentResult.checkout_url,
        updated_at: paymentResult.updated_at,
        created_at: paymentResult.created_at,
      }
    }
    return dataRes
  }
}
