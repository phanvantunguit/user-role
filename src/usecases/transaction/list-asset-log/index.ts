import { Inject } from '@nestjs/common'
import { ITEM_STATUS } from 'src/const/transaction'
import { UserRepository } from 'src/repositories/user.repository'
import { ListAssetLogInput, ListAssetLogOutput } from './validate'
export class ListAssetLogHandler {
  constructor(@Inject(UserRepository) private userRepository: UserRepository) {}
  async execute(
    params: ListAssetLogInput,
    forUser: boolean,
    user_id?: string
  ): Promise<ListAssetLogOutput> {
    const { page, keyword, size, category } = params
    let defaultStatus
    let isOrder
    if (forUser) {
      defaultStatus = [ITEM_STATUS.ACTIVE, ITEM_STATUS.PROCESSING, ITEM_STATUS.NOT_CONNECT, ITEM_STATUS.FAILED]
      isOrder = true
    }
    const status = params.status ? [params.status] : defaultStatus
    const dataRes = await this.userRepository.findUserAssetLogPaging({
      page,
      size,
      keyword,
      status,
      user_id,
      is_order: isOrder,
      category,
    })
    return dataRes
  }
}
