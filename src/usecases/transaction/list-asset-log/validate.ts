import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsOptional,
  IsNumberString,
  IsString,
  IsNumber,
  IsArray,
  IsObject,
} from 'class-validator'
import { ITEM_STATUS, ORDER_CATEGORY } from 'src/const/transaction'
import { UserAssetLogValidate } from '../validate'

export class ListAssetLogInput {
  @ApiProperty({
    required: false,
    description: 'Page',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  page: number

  @ApiProperty({
    required: false,
    description: 'Size',
    example: 50,
  })
  @IsOptional()
  @IsNumberString()
  size: number

  @ApiProperty({
    required: false,
    description: 'The keyword name, order id',
    example: 'john',
  })
  @IsOptional()
  @IsString()
  keyword: string

  @ApiProperty({
    required: false,
    description: `${Object.keys(ITEM_STATUS).join(',')}`,
    example: ITEM_STATUS.ACTIVE,
  })
  @IsOptional()
  @IsString()
  status: ITEM_STATUS

  @ApiProperty({
    required: false,
    description: `${Object.keys(ORDER_CATEGORY).join(',')}`,
    example: ORDER_CATEGORY.SBOT,
  })
  @IsOptional()
  @IsString()
  category: ORDER_CATEGORY
}

export class ListAssetLogOutput {
  @ApiProperty({
    required: true,
    description: 'Page',
    example: 1,
  })
  @IsNumber()
  page: number

  @ApiProperty({
    required: true,
    description: 'Size',
    example: 50,
  })
  @IsNumber()
  size: number

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  count: number

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  total: number

  @ApiProperty({
    required: false,
    isArray: true,
    type: UserAssetLogValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => UserAssetLogValidate)
  rows: UserAssetLogValidate[]
}

export class ListAssetLogPayloadOutput {
  @ApiProperty({
    required: false,
    type: ListAssetLogOutput,
  })
  @IsObject()
  payload: ListAssetLogOutput
}
