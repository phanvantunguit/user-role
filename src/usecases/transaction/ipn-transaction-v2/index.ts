import { HttpStatus, Inject } from '@nestjs/common'
import { APP_CONFIG } from 'src/config'
import { ERROR_CODE } from 'src/const'
import { CHANELS } from 'src/const/app-setting'
import { WS_EVENT } from 'src/const/response'
import {
  ITEM_STATUS,
  ORDER_CATEGORY,
  TBOT_TYPE,
  TRANSACTION_STATUS,
} from 'src/const/transaction'
import { RawUserAssetLog } from 'src/domains/user/user-asset-log.types'
import { CacheRepository } from 'src/repositories/cache.repository'
import { DBContext } from 'src/repositories/db-context'
import { EventStoreRepository } from 'src/repositories/event-store.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { formatRequestDataToString } from 'src/utils/format-data.util'
import { throwError } from 'src/utils/handle-error.util'
import { validateChecksum } from 'src/utils/hash.util'
import { EVENT_STORE_STATE, EVENT_STORE_NAME } from 'src/const/app-setting'
export class IPNTransactionV2Handler {
  constructor(
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(DBContext) private dBContext: DBContext,
    @Inject(CacheRepository) private cacheRepository: CacheRepository,
    @Inject(EventStoreRepository) private eventStoreRepository: EventStoreRepository
  ) {}
  async execute(param: {
    order_id: string
    status: TRANSACTION_STATUS
    order_type: string
    transaction_id: string
    checksum: string
  }): Promise<any> {
    const checksum = param.checksum
    if (!checksum) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.CHECKSUM_INVALID,
      })
    }
    delete param.checksum
    const dataString = formatRequestDataToString(param)
    const check = validateChecksum(dataString, checksum, APP_CONFIG.SECRET_KEY)
    if (!check) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.CHECKSUM_INVALID,
      })
    }
    const userAssetLogs = await this.userRepository.findUserAssetLog({
      order_id: param.order_id,
    })
    if(userAssetLogs.length === 0) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.NOT_FOUND,
      })
    }

    try {
      const openIpnPayment = await this.eventStoreRepository.save({
        event_id: param.order_id,
        event_name: EVENT_STORE_NAME.INP_PAYMENT_TBOT,
        state: EVENT_STORE_STATE.OPEN,
        user_id: userAssetLogs[0].user_id,
        metadata: {
          order_id: param.order_id,
          time: Date.now()
        },
      })
      try {
        return await this.dBContext.runInTransaction(async (queryRunner) => {
          const itemStatus =
            param.status === TRANSACTION_STATUS.COMPLETE
              ? ITEM_STATUS.ACTIVE
              : ITEM_STATUS.FAILED
          const userId = userAssetLogs[0].user_id
          const userAssetBotIds = []
          const userAssetBots: RawUserAssetLog[] = []
    
          const userAssetRoleIds = []
          const userAssetRoles: RawUserAssetLog[] = []
    
          const userAssetBotTradingIds = []
          const userAssetBotTradings: RawUserAssetLog[] = []
    
          const updateUserAssetLogs = userAssetLogs.map((ual) => {
            if (ual.category === ORDER_CATEGORY.SBOT) {
              userAssetBotIds.push(ual.asset_id)
              userAssetBots.push(ual)
            }
            if (ual.category === ORDER_CATEGORY.PKG) {
              userAssetRoleIds.push(ual.asset_id)
              userAssetRoles.push(ual)
            }
            if (ual.category === ORDER_CATEGORY.TBOT) {
              userAssetBotTradingIds.push(ual.asset_id)
              userAssetBotTradings.push(ual)
              return {
                ...ual,
                status:
                  itemStatus === ITEM_STATUS.ACTIVE
                    ? ITEM_STATUS.NOT_CONNECT
                    : ITEM_STATUS.FAILED,
              }
            }
            return {
              ...ual,
              status: itemStatus,
            }
          })
          const promises: any[] = [
            this.userRepository.saveUserAssetLog(updateUserAssetLogs, queryRunner),
          ]
          if (itemStatus === ITEM_STATUS.ACTIVE) {
            console.log('buy userAssetBotIds', userAssetBotIds)
            if (userAssetBotIds.length > 0) {
              const userBots = await this.userRepository.findUserBotSQL({
                user_id: userId,
                bot_ids: userAssetBotIds,
              })
              console.log('buy userBots', userBots)
              const updateUserBots = userAssetBots.map((asset) => {
                console.log('buy userBots asset', asset)
                const assetExited = userBots.find(
                  (b) => b.bot_id === asset.asset_id
                )
                const newAsset = {
                  expires_at: asset.expires_at,
                  bot_id: asset.asset_id,
                  user_id: asset.user_id,
                  owner_created: asset.owner_created,
                  status: itemStatus,
                }
                if (assetExited) {
                  if (assetExited.expires_at > Date.now()) {
                    newAsset.expires_at =
                      Number(assetExited.expires_at) +
                      Number(newAsset.expires_at) -
                      Date.now()
                  }
                  newAsset['id'] = assetExited.id
                }
                console.log('buy userBots newAsset', newAsset)
                return newAsset
              })
              console.log('buy updateUserBots', updateUserBots)
              promises.push(
                this.userRepository.saveUserBot(updateUserBots, queryRunner)
              )
            }
            if (userAssetRoleIds.length > 0) {
              const userRoles =
                await this.userRepository.findUserRoleByUserIdsAndRoleIds(
                  [userId],
                  userAssetRoleIds
                )
              console.log('buy userRoles', userRoles)
              const updateUserRoles = userAssetRoles.map((asset) => {
                console.log('buy userRoles asset', asset)
                const assetExited = userRoles.find(
                  (b) => b.role_id === asset.asset_id
                )
                const newAsset = {
                  role_id: asset.asset_id,
                  user_id: asset.user_id,
                  owner_created: asset.owner_created,
                  quantity: asset.quantity,
                  expires_at: asset.expires_at,
                }
                if (assetExited) {
                  if (assetExited.expires_at > Date.now()) {
                    newAsset.expires_at =
                      Number(assetExited.expires_at) +
                      Number(newAsset.expires_at) -
                      Date.now()
                  }
                  newAsset['id'] = assetExited.id
                }
                console.log('buy userRoles newAsset', newAsset)
                return newAsset
              })
              console.log('buy updateUserRoles', updateUserRoles)
              promises.push(
                this.userRepository.saveUserRole(updateUserRoles, queryRunner)
              )
            }
            if (userAssetBotTradingIds.length > 0) {
              const userBots = await this.userRepository.findUserBotTradingSQL({
                user_id: userId,
                bot_ids: userAssetBotTradingIds,
              })
              console.log('buy userBots', userBots)
              const updateUserBots = userAssetBotTradings.map((asset) => {
                console.log('buy userBots asset', asset)
                const assetExited = userBots.find(
                  (b) => b.bot_id === asset.asset_id
                )
                const newAsset = {
                  expires_at: asset.expires_at,
                  bot_id: asset.asset_id,
                  user_id: asset.user_id,
                  owner_created: asset.owner_created,
                  status: ITEM_STATUS.NOT_CONNECT,
                  type: TBOT_TYPE.BUY,
                  balance: asset.metadata?.balance
                }
                if (assetExited) {
                  if (assetExited.expires_at > Date.now()) {
                    newAsset.expires_at =
                      Number(assetExited.expires_at) +
                      Number(newAsset.expires_at) -
                      Date.now()
                  }
                  newAsset['id'] = assetExited.id
                }
                console.log('buy userBots newAsset', newAsset)
                return newAsset
              })
              console.log('buy updateUserBots', updateUserBots)
              promises.push(
                this.userRepository.saveUserBotTrading(updateUserBots, queryRunner)
              )
            }
          }
          const result = await Promise.all(promises)
          await this.eventStoreRepository.save({
            event_id: param.order_id,
            event_name: EVENT_STORE_NAME.INP_PAYMENT_TBOT,
            state: EVENT_STORE_STATE.CLOSED,
            user_id: userAssetLogs[0].user_id,
            metadata: {
              order_id: param.order_id,
              time: Date.now()
            },
          })
          this.cacheRepository.publish(
            CHANELS.WS_CHANEL,
            WS_EVENT.transaction_update,
            param,
            userId
          )
          return result
        })
      } catch (error) {
        await this.eventStoreRepository.deleteById(openIpnPayment.id)
        throwError({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          ...ERROR_CODE.UNPROCESSABLE,
        })
      }
    } catch (error) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.TRANSACTION_PROCESSING,
      })
    }
  }
}
