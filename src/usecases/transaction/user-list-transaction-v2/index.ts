import { Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { CmpaymentResource } from 'src/resources/cmpayment'
import { ListTransactionV2Validate } from '../validate'
export class ListTransactionV2Handler {
  constructor(
    @Inject(CmpaymentResource) private cmpaymentResource: CmpaymentResource
  ) {}
  async execute(user_id: string): Promise<ListTransactionV2Validate[]> {
    const dataRes = await this.cmpaymentResource.listTransaction({
      user_id,
    })
    if (dataRes.error_code === ERROR_CODE.SUCCESS.error_code) {
      const result = dataRes.payload.map((paymentResult) => ({
        transaction_id: paymentResult.id,
        order_id: paymentResult.order_id,
        amount: paymentResult.amount,
        currency: paymentResult.currency,
        payment_method: paymentResult.payment_method,
        status: paymentResult.status,
        payment_id: paymentResult.payment_id,
        updated_at: paymentResult.updated_at,
        created_at: paymentResult.created_at,
      }))
      return result
    }
    return dataRes
  }
}
