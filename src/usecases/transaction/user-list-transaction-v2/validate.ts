import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import { IsArray, IsOptional } from 'class-validator'
import { UserListTransactionV2Validate } from '../validate'
export class ListTransactionV2PayloadOutput {
  @ApiProperty({
    required: false,
    isArray: true,
    type: UserListTransactionV2Validate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => UserListTransactionV2Validate)
  payload: UserListTransactionV2Validate[]
}
