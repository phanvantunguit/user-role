import { ApiProperty } from '@nestjs/swagger'
import { IsString, IsIn, IsOptional, IsObject, IsNumber } from 'class-validator'
import { PACKAGE_TYPE } from 'src/const'
import {
  ITEM_STATUS,
  ORDER_CATEGORY,
  PAYMENT_METHOD,
  TRANSACTION_STATUS,
} from 'src/const/transaction'

export class ListTransactionV2Validate {
  @ApiProperty({
    required: true,
    example: 'BOT1802220001',
  })
  @IsString()
  order_id?: string

  @ApiProperty({
    required: true,
    example: '2',
  })
  @IsString()
  amount?: string

  @ApiProperty({
    required: true,
    description: 'Currency for user payment',
    example: 'LTCT',
  })
  @IsString()
  currency?: string

  @ApiProperty({
    required: true,
    description: 'Payment method',
    example: PAYMENT_METHOD.COIN_PAYMENT,
  })
  @IsString()
  @IsIn(Object.values(PAYMENT_METHOD))
  payment_method?: PAYMENT_METHOD

  @ApiProperty({
    required: true,
    description: Object.values(TRANSACTION_STATUS).join(','),
    example: TRANSACTION_STATUS.CREATED,
  })
  @IsString()
  status?: TRANSACTION_STATUS

  @ApiProperty({
    required: true,
    description: 'Payment id of coinpayment',
    example: 'OAINONOFASF1203412',
  })
  @IsString()
  payment_id?: string

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsString()
  created_at?: number

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsString()
  updated_at?: number
}

export class UserListTransactionV2Validate extends ListTransactionV2Validate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  transaction_id: string
}
export class AdminListTransactionV2Validate extends ListTransactionV2Validate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id?: string
  @ApiProperty({
    required: false,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e1',
  })
  @IsOptional()
  @IsString()
  user_id?: string

  @ApiProperty({
    required: false,
    example: 'Nguyen Van A',
  })
  @IsOptional()
  @IsString()
  fullname?: string

  @ApiProperty({
    required: false,
    example: 'nguyenvana@gmail.com',
  })
  @IsOptional()
  @IsString()
  email?: string
}

export class TransactionV2MetadataValidate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id?: string

  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  transaction_id?: string

  @ApiProperty({
    required: true,
    example: 'wallet_address',
  })
  @IsString()
  attribute?: string

  @ApiProperty({
    required: true,
    example: 'moBpkWoiMmJJKhBjDXeXPWPaTJFRCdmM1g',
  })
  @IsString()
  value?: string

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsString()
  created_at?: number
}

export class TransactionV2LogValidate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id?: string

  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  transaction_id?: string

  @ApiProperty({
    required: true,
    example: 'PAYMENT_COMPLETE',
  })
  @IsString()
  transaction_event?: string

  @ApiProperty({
    required: true,
    example: 'PROCESSING',
  })
  @IsString()
  transaction_status?: string

  @ApiProperty({
    required: true,
    example: {},
  })
  @IsObject()
  metadata?: any

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsString()
  created_at?: number
}

export class UserAssetLogValidate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id: string

  @ApiProperty({
    required: true,
    example: 'BOT010120230001',
  })
  @IsString()
  order_id: string

  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  asset_id: string

  @ApiProperty({
    required: true,
    example: PACKAGE_TYPE.MONTH,
  })
  @IsString()
  package_type: string

  @ApiProperty({
    required: true,
    example: ORDER_CATEGORY.SBOT,
  })
  @IsString()
  category: ORDER_CATEGORY

  @ApiProperty({
    required: true,
    example: 1,
  })
  @IsNumber()
  quantity: number

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsNumber()
  expires_at: number

  @ApiProperty({
    required: true,
    example: ITEM_STATUS.ACTIVE,
  })
  @IsNumber()
  status: ITEM_STATUS

  @ApiProperty({
    required: true,
    example: ITEM_STATUS.ACTIVE,
  })
  @IsNumber()
  name: string

  @ApiProperty({
    required: true,
    example: '3',
  })
  @IsNumber()
  price: string

  @ApiProperty({
    required: true,
    example: 0.2,
  })
  @IsNumber()
  discount_rate: number

  @ApiProperty({
    required: true,
    example: 10,
  })
  @IsNumber()
  discount_amount: number

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsNumber()
  created_at: number

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsNumber()
  updated_at: number
}
