import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import { IsOptional, IsArray, IsString, IsIn } from 'class-validator'
import { ADDITIONAL_DATA_TYPE } from 'src/domains/merchant/types'

export class BalanceAndFeeDataInput {
  @ApiProperty({
    required: false,
    description: 'name',
  })
  @IsString()
  @IsOptional()
  name?: string
}

export class AdditionalDataValidate {
  @ApiProperty({
    required: false,
    description: `type: ${Object.values(ADDITIONAL_DATA_TYPE)}`,
  })
  @IsIn(Object.values(ADDITIONAL_DATA_TYPE))
  @IsOptional()
  type?: ADDITIONAL_DATA_TYPE

  @ApiProperty({
    required: false,
    description: 'name',
  })
  @IsString()
  @IsOptional()
  name?: string

  @ApiProperty({
    required: false,
    description: 'data',
  })
  @IsOptional()
  data?: any
}

export class BalanceAndFeeDataOutput {
  @ApiProperty({
    required: false,
    type: AdditionalDataValidate,
  })
  @IsOptional()
  @Type(() => AdditionalDataValidate)
  payload: AdditionalDataValidate
}
