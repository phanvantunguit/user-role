import { Inject } from '@nestjs/common'
import { BalanceAndFeeDataInput, BalanceAndFeeDataOutput } from './validate'
import { AdditionalDataRepository } from 'src/repositories/additional-data.repository'
import { ADDITIONAL_DATA_TYPE } from 'src/domains/merchant/types'

export class BalanceAndFeeDataHandler {
  constructor(
    @Inject(AdditionalDataRepository)
    private additionalDataRepository: AdditionalDataRepository
  ) {}
  async execute(): Promise<BalanceAndFeeDataOutput> {
    const data = await this.additionalDataRepository.list(
      'DEFAULT',
      ADDITIONAL_DATA_TYPE.TBOT_FEE
    )
    return data[0]
  }
}
