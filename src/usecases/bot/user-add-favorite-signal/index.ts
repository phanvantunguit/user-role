import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { BotSignalRepository } from 'src/repositories/bot-signal.repository'
import { throwError } from 'src/utils/handle-error.util'
export class UserAddFavoriteSignalHandler {
  constructor(
    @Inject(BotSignalRepository)
    private botSignalRepository: BotSignalRepository
  ) {}
  async execute(user_id: string, bot_signal_id: string): Promise<any> {
    const [signal, favoriteSignal] = await Promise.all([
      this.botSignalRepository.findById(bot_signal_id),
      this.botSignalRepository.findFavoriteSignal({
        user_id,
        bot_signal_id,
      }),
    ])
    if (!signal) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.SIGNAL_NOT_FOUND,
      })
    }
    if (favoriteSignal) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.FAVORITE_SIGNAL_EXISTED,
      })
    }
    return this.botSignalRepository.addFavoriteSignal({
      user_id,
      bot_signal_id,
    })
  }
}
