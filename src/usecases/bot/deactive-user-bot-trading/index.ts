import { Inject } from '@nestjs/common'
import { APP_CONFIG } from 'src/config'
import {
  APP_SETTING,
  EVENT_STORE_NAME,
  EVENT_STORE_STATE,
} from 'src/const/app-setting'
import { ITEM_STATUS } from 'src/const/transaction'
import { EventStoreRepository } from 'src/repositories/event-store.repository'
import { SettingRepository } from 'src/repositories/setting.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { MetaapiResource } from 'src/resources/forex'
import { BotTradingService } from 'src/services/bot-trading'

export class DeactivateUserBotTradingHandler {
  constructor(
    @Inject(EventStoreRepository)
    private eventStoreRepository: EventStoreRepository,
    @Inject(BotTradingService) private botTradingService: BotTradingService,
    @Inject(MetaapiResource) private metaapiResource: MetaapiResource,
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(SettingRepository) private settingRepository: SettingRepository
  ) {}
  async execute(
    user_bot_id: string,
    user_id: string,
    bot_id: string,
    trade: {
      id: string
      symbol: string
      time: number
      broker_account: string
    }
  ): Promise<any> {
    const sendEmailProcessing = await this.eventStoreRepository.save({
      event_id: `${trade.id}`,
      user_id,
      event_name: EVENT_STORE_NAME.TBOT_INACTIVE_BY_SYSTEM,
      state: EVENT_STORE_STATE.PROCESSING,
    })
    try {
      const events = await this.eventStoreRepository.find({
        user_id: user_id,
        event_name: APP_SETTING.TBOT_INACTIVE_BY_SYSTEM,
      })
      const deleteEventIds = []
      let countInactive = 0
      events.forEach((e) => {
        if (e.metadata?.bot_id === bot_id) {
          deleteEventIds.push(e.id)
          if (e.state === EVENT_STORE_STATE.OPEN) {
            countInactive += 1
          }
        }
      })
      if (countInactive > APP_CONFIG.NUMBER_OF_TIMES_ALLOW_USER_ACTIVATE_TBOT) {
        const userBot = await this.userRepository.findUserBotTrading({
          id: user_bot_id,
        })
        if (userBot[0]?.subscriber_id) {
          try {
            await this.metaapiResource.removeSubscriber({
              subscriber_id: userBot[0].subscriber_id,
            })
          } catch (error) {
            console.log('removeSubscriber error', error)
          }
          await this.metaapiResource.removeAccount({
            subscriber_id: userBot[0].subscriber_id,
          })
          await this.userRepository.deleteUserBotTrading({
            user_id,
            bot_id,
          })
          await this.eventStoreRepository.deleteByIds(deleteEventIds)
          const numberOfBot = await this.settingRepository.findAppSetting({
            user_id,
            name: APP_SETTING.NUMBER_OF_TBOT,
          })
          await this.eventStoreRepository.delete({
            event_id: `${userBot[0].user_id}_${userBot[0].bot_id}`,
            state: EVENT_STORE_STATE.OPEN,
            event_name: EVENT_STORE_NAME.TBOT_STOP_INACTIVE_BY_SYSTEM,
          })
          if (numberOfBot[0]?.value) {
            const newNumberOfBot =
              Number(numberOfBot[0].value) > 0
                ? Number(numberOfBot[0].value) - 1
                : 0
            await this.settingRepository.saveAppSetting({
              id: numberOfBot[0].id,
              value: newNumberOfBot.toString(),
            })
          }
        }
      } else {
        try {
          await this.eventStoreRepository.save({
            event_id: `${trade.id}`,
            user_id,
            event_name: EVENT_STORE_NAME.TBOT_INACTIVE_BY_SYSTEM,
            state: EVENT_STORE_STATE.CLOSED,
          })
        } catch (error) {
          console.log('error', error)
        }
      }

      await this.botTradingService.emailBotStatus({
        user_id,
        bot_id,
        status: ITEM_STATUS.INACTIVE_BY_SYSTEM,
        trade,
        count_invalid: countInactive,
      })
    } catch (error) {
      console.log('error', error)
      await this.eventStoreRepository.deleteById(sendEmailProcessing.id)
    }
  }
}
