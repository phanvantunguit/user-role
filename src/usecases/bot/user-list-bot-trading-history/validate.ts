import { Optional } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsString,
  IsNumber,
  IsOptional,
  IsArray,
  IsNumberString,
  IsBooleanString,
  IsObject,
  IsUUID,
} from 'class-validator'
import { BOT_STATUS } from 'src/const'
import { TRADE_HISTORY_STATUS } from 'src/const/bot'
import { ITEM_STATUS } from 'src/const/transaction'

export class UserListTradeHistoryInput {
  @ApiProperty({
    required: false,
    description: 'Page',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  page: number

  @ApiProperty({
    required: false,
    description: 'Size',
    example: 50,
  })
  @IsOptional()
  @IsNumberString()
  size: number

  @ApiProperty({
    required: false,
    example: 'efebdafe-c359-4e88-bd17-82b2a5b1091b',
  })
  @IsOptional()
  @IsUUID()
  bot_id: string

  @ApiProperty({
    required: false,
    description: `${Object.keys(TRADE_HISTORY_STATUS).join(' or ')}`,
    example: TRADE_HISTORY_STATUS.OPEN,
  })
  @IsOptional()
  @IsString()
  status: TRADE_HISTORY_STATUS

  @ApiProperty({
    required: false,
    description: 'true or false',
    example: 'true',
  })
  @IsOptional()
  @IsBooleanString()
  strategy_trade?: string

  @ApiProperty({
    required: false,
    description: 'From',
  })
  @IsOptional()
  @IsNumberString()
  from: number

  @ApiProperty({
    required: false,
    description: 'To',
  })
  @IsOptional()
  @IsNumberString()
  to: number
}
export class TradeHistoryDataValidate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id?: string

  @ApiProperty({
    description: 'Name token frist',
  })
  @Optional()
  @IsString()
  token_first?: string

  @ApiProperty({
    required: true,
    description: 'Name token second',
  })
  @IsString()
  token_second?: string

  @ApiProperty({
    required: true,
    description: Object.values(BOT_STATUS).join(','),
    example: BOT_STATUS.OPEN,
  })
  @IsString()
  status?: string

  @ApiProperty({
    required: true,
    description: 'Side',
    example: 'LONG',
  })
  @IsString()
  side?: string

  @ApiProperty({
    required: false,
    description: 'Price entry',
    example: '0.009',
  })
  @IsNumberString()
  entry_price?: string

  @ApiProperty({
    required: false,
    description: 'Price close',
    example: '0.009',
  })
  @IsNumberString()
  close_price?: string

  @ApiProperty({
    required: true,
    description: 'Profit/loss',
    example: '14',
  })
  @IsString()
  profit?: string

  @ApiProperty({
    required: true,
    description: 'Day start',
    example: '1676000403889',
  })
  @IsNumber()
  day_started?: number

  @ApiProperty({
    required: false,
    description: 'Day completed',
    example: '1676000403889',
  })
  @IsOptional()
  @IsNumber()
  day_completed?: number

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsNumber()
  created_at?: number

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsNumber()
  updated_at?: number

  @ApiProperty({
    required: false,
    description: `${Object.keys(ITEM_STATUS)}`,
    example: ITEM_STATUS.ACTIVE,
  })
  @IsString()
  @IsOptional()
  user_status?: ITEM_STATUS
}

export class UserListTradeHistoryOutput {
  @ApiProperty({
    required: true,
    description: 'Page',
    example: 1,
  })
  @IsNumber()
  page: number

  @ApiProperty({
    required: true,
    description: 'Size',
    example: 50,
  })
  @IsNumber()
  size: number

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  count: number

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  total: number

  @ApiProperty({
    required: false,
    isArray: true,
    type: TradeHistoryDataValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => TradeHistoryDataValidate)
  rows: TradeHistoryDataValidate[]
}
export class UserListTradeHistoryPayloadOutput {
  @ApiProperty({
    required: false,
    type: UserListTradeHistoryOutput,
  })
  @IsObject()
  payload: UserListTradeHistoryOutput
}
