import { Inject } from '@nestjs/common'
import { ITEM_STATUS } from 'src/const/transaction'
import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { UserBotTradingEntity } from 'src/repositories/user.repository/user-bot-trading.entity'
import {
  UserListTradeHistoryOutput,
  UserListTradeHistoryInput,
} from './validate'
export class UserListBotTradingHistoryHandler {
  constructor(
    @Inject(BotTradingHistoryRepository)
    private botTradingHistoryRepository: BotTradingHistoryRepository,
    @Inject(UserRepository)
    private userRepository: UserRepository
  ) {}
  async execute(
    params: UserListTradeHistoryInput,
    user_id?: string
  ): Promise<UserListTradeHistoryOutput> {
    let userBot: UserBotTradingEntity[]
    let userId: string
    let connected_at: number
    if (user_id && params.bot_id) {
      userBot = await this.userRepository.findUserBotTrading({
        user_id,
        bot_id: params.bot_id,
      })
      if (userBot[0]) {        
        connected_at = userBot[0].connected_at
        if(params.strategy_trade !== 'true') {
          userId = user_id
          if(userBot[0].status === ITEM_STATUS.NOT_CONNECT || userBot[0].status === ITEM_STATUS.CONNECTING) {
            connected_at = Date.now()
          }
        }
      }
    }

    const trades = await this.botTradingHistoryRepository.getPaging({
      page: params.page,
      size: params.size,
      status: params.status,
      from: params.from,
      to: params.to,
      bot_id: params.bot_id,
      user_id: userId,
      connected_at
    })
    return trades
  }
}
