import { HttpStatus, Inject } from '@nestjs/common'
import { AdditionalDataRepository } from 'src/repositories/additional-data.repository'
import { ADDITIONAL_DATA_TYPE } from 'src/domains/merchant/types'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { CreateBalanceAndFeeDataInput, CreatelDataValidate } from './validate'
import { throwError } from 'src/utils/handle-error.util'
import { ERROR_CODE } from 'src/const'

export class CreateBalanceAndFeeDataHandler {
  constructor(
    @Inject(AdditionalDataRepository)
    private additionalDataRepository: AdditionalDataRepository,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository
  ) {}
  async execute(params: CreateBalanceAndFeeDataInput): Promise<CreatelDataValidate> {
    const data = await this.additionalDataRepository.list(
      'DEFAULT',
      ADDITIONAL_DATA_TYPE.TBOT_FEE
    )
    const GetTbot = await this.botTradingRepository.findById(params.bot_id)
    let totalPrice
    let baseFee = Number(GetTbot.price)
    if (!GetTbot) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }
    data[0]?.data?.ranges?.map((item, i, array) => {
      baseFee =
        baseFee +
        (Number(item?.from) - Number(array[i - 1]?.from)) *
          Number(array[i - 1]?.percent)
      if (!baseFee) {
        baseFee = Number(GetTbot.price)
      }
      if (params.balance >= item?.from && params.balance <= item?.to) {
        totalPrice =
          Number(baseFee) +
          (Number(params.balance) - Number(item?.from)) * Number(item?.percent)
      } else if (params.balance >= item?.from && !item?.to) {
        totalPrice =
          Number(baseFee) +
          (Number(params.balance) - Number(item?.from)) * Number(item?.percent)
      }
    })
    return {
      total_price: totalPrice,
      price: Number(GetTbot.price),
    }
  }
}
