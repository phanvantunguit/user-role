import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsOptional,
  IsArray,
  IsString,
  IsIn,
  IsUUID,
  IsNumber,
} from 'class-validator'

export class CreateBalanceAndFeeDataInput {
  @ApiProperty({
    required: false,
    description: 'Bot_id',
  })
  @IsUUID()
  @IsOptional()
  bot_id?: string

  @ApiProperty({
    required: false,
    description: 'Balance Bot',
  })
  @IsNumber()
  @IsOptional()
  balance?: number
}

export class CreatelDataValidate {
  @ApiProperty({
    required: false,
    description: 'price',
  })
  @IsNumber()
  @IsOptional()
  price?: Number

  @ApiProperty({
    required: false,
    description: 'total_price',
  })
  @IsNumber()
  @IsOptional()
  total_price?: Number
}

export class CreateBalanceAndFeeDataOutput {
  @ApiProperty({
    required: false,
    type: CreatelDataValidate,
  })
  @IsOptional()
  @Type(() => CreatelDataValidate)
  payload: CreatelDataValidate
}
