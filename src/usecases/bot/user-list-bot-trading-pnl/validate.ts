import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import { IsString, IsNumber, IsOptional, IsArray } from 'class-validator'

export class BotPnlValidate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  bot_id: string

  @ApiProperty({
    required: true,
    example: 20,
  })
  @IsNumber()
  pnl_day_percent: number

  @ApiProperty({
    required: true,
    example: 100,
  })
  @IsNumber()
  pnl_day_init: number

  @ApiProperty({
    required: true,
    example: 120,
  })
  @IsNumber()
  pnl_day_current: number

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsNumber()
  created_at: number

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsNumber()
  updated_at: number
}
export class UserListBotTradingPnlPayloadOutput {
  @ApiProperty({
    required: false,
    isArray: true,
    type: BotPnlValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => BotPnlValidate)
  payload: BotPnlValidate[]
}
