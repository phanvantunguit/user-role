import { HttpStatus, Inject } from '@nestjs/common'
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository'
import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { BotPnlValidate } from './validate'
import { throwError } from 'src/utils/handle-error.util'
import { ERROR_CODE } from 'src/const'
import { ITEM_STATUS } from 'src/const/transaction'
export class UserListBotTradingPNLHandler {
  constructor(
    @Inject(AccountBalanceRepository)
    private accountBalanceRepository: AccountBalanceRepository,
    @Inject(BotTradingHistoryRepository)
    private botTradingHistoryRepository: BotTradingHistoryRepository,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(UserRepository) private userRepository: UserRepository
  ) {}
  async execute(
    user_id: string,
    bot_id: string,
    strategy_trade?: string
  ): Promise<BotPnlValidate[]> {
    let userId
    let userBot
    let connected_at
    if (user_id && bot_id) {
      userBot = await this.userRepository.findUserBotTrading({
        user_id,
        bot_id,
      })
      if (userBot[0]) {        
        connected_at = userBot[0].connected_at
        if(strategy_trade !== 'true') {
          userId = user_id
          if(userBot[0].status === ITEM_STATUS.NOT_CONNECT || userBot[0].status === ITEM_STATUS.CONNECTING) {
            connected_at = Date.now()
          }
        }
      }
    }
    const data = await this.botTradingHistoryRepository.chartPNL({
      bot_id,
      user_id: userId,
      timezone: `UTC-7`,
      time_type: 'DAY',
      connected_at
    })
    const bot = await this.botTradingRepository.findById(bot_id)
    if (!bot) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }
    let initBalance = 0
    const balance = await this.accountBalanceRepository.getBalanceByUser({
      user_id: userId,
      bot_id,
    })
    if (balance[0]) {
      initBalance = balance[0].total
    } else {
      initBalance = Number(bot.balance) || 10000
    }
    const balanceLatest = await this.accountBalanceRepository.getBalance({
      user_id: userId,
      bot_id,
      order: 'desc',
    })

    const realBalance = balanceLatest[0] ? balanceLatest[0].balance : initBalance

    const result = []
    let currentBalance = initBalance
    let cumulativePNL = 0
    for (let i = 0; i < data.length; i++) {
      const e = data[i]
      if (i > 0) {
        const startDate = new Date(data[i - 1].time)
        const secondDate = new Date(data[i - 1].time)
        secondDate.setDate(secondDate.getDate() + 1)
        const endDate = new Date(e.time)
        const timeDiff = Math.abs(endDate.valueOf() - startDate.valueOf())
        const diffDays = Math.floor(timeDiff / (1000 * 3600 * 24))
        if (diffDays > 1) {
          for (let d = secondDate; d < endDate; d.setDate(d.getDate() + 1)) {
            result.push({
              ...result[result.length - 1],
              created_at: d.valueOf(),
              updated_at: d.valueOf(),
            })
          }
        }
      }
      currentBalance += e.profit_cash
      cumulativePNL += e.profit_cash
      result.push({
        bot_id,
        pnl_day_cash: cumulativePNL,
        current_balance: currentBalance,
        balance: realBalance,
        pnl_day_percent: (cumulativePNL / initBalance) * 100,
        created_at: new Date(e.time).valueOf(),
        updated_at: new Date(e.time).valueOf(),
      })
    }
    return result
  }
}
