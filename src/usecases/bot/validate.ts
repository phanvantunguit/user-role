import { ApiProperty } from '@nestjs/swagger'
import { IsString } from 'class-validator'

export class BotSignalValidate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id?: string

  @ApiProperty({
    required: true,
    example: 'BotGifDo',
  })
  @IsString()
  name?: string

  @ApiProperty({
    required: true,
    example: '5m',
  })
  @IsString()
  resolution?: string

  @ApiProperty({
    required: true,
    example: 'fad731ed_1673254427814',
  })
  @IsString()
  signal_id?: string

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsString()
  time?: number

  @ApiProperty({
    required: true,
    example: 'BUY',
  })
  @IsString()
  type?: string

  @ApiProperty({
    required: true,
    example:
      'https://static.cextrading.io/images/Bot_Box/Bot_Box_LTCUSDT_5m_1671527700000.png',
  })
  @IsString()
  image_url?: string

  @ApiProperty({
    required: true,
    example: 'BINANCE',
  })
  @IsString()
  exchange?: string

  @ApiProperty({
    required: true,
    example: 'BTC',
  })
  @IsString()
  symbol?: string

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsString()
  created_at?: number
}
