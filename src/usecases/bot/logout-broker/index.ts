import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { APP_SETTING } from 'src/const/app-setting'
import { ITEM_STATUS, ORDER_CATEGORY, TBOT_TYPE } from 'src/const/transaction'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { MetaapiResource } from 'src/resources/forex'
import { AppSettingService } from 'src/services/app-setting'
import { throwError } from 'src/utils/handle-error.util'
import { LogoutBrokerServerInput } from './validate'
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository'

export class LogoutBrokerServerHandler {
  constructor(
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(MetaapiResource) private metaapiResource: MetaapiResource,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(AppSettingService) private appSettingService: AppSettingService,
    @Inject(AccountBalanceRepository)
    private accountBalanceRepository: AccountBalanceRepository
  ) {}
  async execute(params: LogoutBrokerServerInput, userId: string): Promise<any> {
    const { bot_id } = params
    const [checkUserBot, bot] = await Promise.all([
      this.userRepository.findUserBotTrading({
        bot_id,
        user_id: userId,
      }),
      this.botTradingRepository.findById(bot_id),
    ])
    if (checkUserBot.length === 0 || !bot) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }
    if (
      !checkUserBot[0].subscriber_id ||
      (checkUserBot[0].status !== ITEM_STATUS.ACTIVE &&
        checkUserBot[0].status !== ITEM_STATUS.INACTIVE &&
        checkUserBot[0].status !== ITEM_STATUS.INACTIVE_BY_SYSTEM &&
        checkUserBot[0].status !== ITEM_STATUS.STOP_OUT)
    ) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.ACCOUNT_NOT_CONNECTED,
      })
    }
    const dataLog = {
      asset_id: bot.id,
      user_id: userId,
      category: ORDER_CATEGORY.TBOT,
      status: ITEM_STATUS.LOG_OUT,
      owner_created: userId,
      name: bot.name,
      metadata: {},
    }
    const dataLogSaved = await this.userRepository.saveUserAssetLog([dataLog])
    try {
      await this.metaapiResource.removeSubscriber({
        subscriber_id: checkUserBot[0].subscriber_id,
      })
    } catch (error) {
      dataLog.metadata['errorRemoveSubscriber'] = error
      await this.userRepository.saveUserAssetLog([
        {
          id: dataLogSaved[0].id,
          metadata: dataLog.metadata,
        },
      ])
    }
    try {
      await this.metaapiResource.removeAccount({
        subscriber_id: checkUserBot[0].subscriber_id,
      })
    } catch (error) {
      dataLog.metadata['errorRemoveAccount'] = error
      await this.userRepository.saveUserAssetLog([
        {
          id: dataLogSaved[0].id,
          metadata: dataLog.metadata,
        },
      ])
      throwError({
        status: HttpStatus.UNPROCESSABLE_ENTITY,
        ...ERROR_CODE.UNPROCESSABLE,
      })
    }
    if (checkUserBot[0].profile_id) {
      await this.metaapiResource.removeProvisioningProfile(
        checkUserBot[0].profile_id
      )
    }
    const asNumberOfBot = await this.appSettingService.getAppSetting({
      user_id: userId,
      name: APP_SETTING.NUMBER_OF_TBOT,
    })
    const numberOfBotOfUser = Number(asNumberOfBot?.value) || 0
    if (numberOfBotOfUser > 0 || checkUserBot[0].type === TBOT_TYPE.SELECT) {
      const userBotTradingDeleted =
        await this.userRepository.deleteUserBotTrading({
          user_id: userId,
          bot_id,
        })
      dataLog.metadata['userBotTradingDeleted'] = userBotTradingDeleted
    } else {
      const updateUserBot = {
        id: checkUserBot[0].id,
        status: ITEM_STATUS.NOT_CONNECT,
        subscriber_id: null,
        broker_account: null,
      }
      const userBotTradingSaved = await this.userRepository.saveUserBotTrading([
        updateUserBot,
      ])
      dataLog.metadata['userBotTradingSaved'] = userBotTradingSaved
    }
    await this.accountBalanceRepository.deleteAccountBalance({
      user_id: userId,
      bot_id,
    })
    await this.userRepository.saveUserAssetLog([
      {
        id: dataLogSaved[0].id,
        metadata: dataLog.metadata,
      },
    ])
    delete checkUserBot[0].expires_at
    return checkUserBot[0]
  }
}
