import { ApiProperty } from '@nestjs/swagger'
import { IsString } from 'class-validator'

export class LogoutBrokerServerInput {
  @ApiProperty({
    required: true,
    example: 'efebdafe-c359-4e88-bd17-82b2a5b1091b',
  })
  @IsString()
  bot_id: string
}
