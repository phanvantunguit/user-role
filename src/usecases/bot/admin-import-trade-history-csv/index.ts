import { HttpStatus, Inject, Injectable } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { TRADE_SIDE } from 'src/const/bot'
import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { throwError } from 'src/utils/handle-error.util'
import * as XLSX from 'xlsx'
@Injectable()
export class AdminImportTradeHistoryCSVHandler {
  constructor(
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(BotTradingHistoryRepository)
    private botTradingHistoryRepository: BotTradingHistoryRepository
  ) {}
  async execute(file: any, param: any, admin_id: string): Promise<any> {
    const { bot_id } = param
    const bot = await this.botTradingRepository.findById(bot_id)
    if (!bot) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }
    const workbook = XLSX.read(file.buffer, { type: 'buffer' })
    const rowObjects: any[] = XLSX.utils.sheet_to_json(
      workbook.Sheets[workbook.SheetNames[0]]
    )
    const dataSaves = []
    try {
      for (let row of rowObjects) {
        if (row) {
          const direction = row.side === TRADE_SIDE.LONG ? 1 : -1
          const profitCash =
            Number(row.close) && Number(row.entry)
              ? direction *
                (Number(row.close) - Number(row.entry)) *
                Number(row.volume)
              : null

          const profit =
            Number(row.close) && Number(row.entry)
              ? ((direction * (Number(row.close) - Number(row.entry))) /
                  Number(row.entry)) *
                100
              : null
          dataSaves.push({
            token_first: bot.token_first,
            token_second: bot.token_second,
            bot_id,
            status: row.status,
            side: row.side,
            entry_price: row.entry,
            close_price: row.close,
            volume: Number(row.volume),
            day_started: row.started
              ? new Date(row.started.replace('_', ' ')).valueOf()
              : null,
            day_completed: row.completed
              ? new Date(row.completed.replace('_', ' ')).valueOf()
              : null,
            owner_created: admin_id,
            profit,
            profit_cash: profitCash,
          })
        }
      }
    } catch (error) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.UNPROCESSABLE,
      })
    }
    if (dataSaves.length === 0) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.TRANSACTION_NOT_FOUND,
      })
    }
    await this.botTradingHistoryRepository.deleteDataWithoutUser({ bot_id })
    const saved = await this.botTradingHistoryRepository.saves(dataSaves)
    return saved
  }
}
