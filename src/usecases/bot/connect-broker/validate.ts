import { ApiProperty } from '@nestjs/swagger'
import { IsIn, IsOptional, IsString } from 'class-validator'
import { BROKER_CODE, TBOT_PLATFORM } from 'src/const/bot'

export class ConnectBrokerServerInput {
  @ApiProperty({
    required: true,
    example: 'efebdafe-c359-4e88-bd17-82b2a5b1091b',
  })
  @IsString()
  bot_id: string

  @ApiProperty({
    required: true,
    example: 'axi',
  })
  @IsString()
  broker_code: BROKER_CODE

  @ApiProperty({
    required: true,
    example: 'Axi.SVG-US03-Demo',
  })
  @IsString()
  broker_server: string

  @ApiProperty({
    required: true,
    example: '1235381947',
  })
  @IsString()
  account_id: string

  @ApiProperty({
    required: true,
    example: 'D5M23d7WTY',
  })
  @IsString()
  password: string

  @ApiProperty({
    required: true,
    description: `Status: ${Object.values(TBOT_PLATFORM)}`,
  })
  @IsIn(Object.values(TBOT_PLATFORM))
  @IsString()
  platform: TBOT_PLATFORM

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  profile_id: string
}
