import { HttpStatus, Inject } from '@nestjs/common'
import { EventEmitter2 } from '@nestjs/event-emitter'
import { APP_CONFIG } from 'src/config'
import { ERROR_CODE } from 'src/const'
import {
  APP_SETTING,
  EVENT_STORE_NAME,
  EVENT_STORE_STATE,
} from 'src/const/app-setting'
import { BOT_TRADING_EVENT } from 'src/const/bot'
import { ITEM_STATUS } from 'src/const/transaction'
import {
  BrokerSetting,
  BrokerSettings,
} from 'src/domains/setting/app-setting.types'
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { EventStoreRepository } from 'src/repositories/event-store.repository'
import { MerchantRepository } from 'src/repositories/merchant.repository'
import { SettingRepository } from 'src/repositories/setting.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { BrokerResource } from 'src/resources/broker'
import { MetaapiResource } from 'src/resources/forex'
import { throwError } from 'src/utils/handle-error.util'
import { ConnectBrokerServerInput } from './validate'

export class ConnectBrokerServerHandler {
  constructor(
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(MetaapiResource) private metaapiResource: MetaapiResource,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(EventEmitter2) private eventEmitter: EventEmitter2,
    @Inject(AccountBalanceRepository)
    private accountBalanceRepository: AccountBalanceRepository,
    @Inject(EventStoreRepository)
    private eventStoreRepository: EventStoreRepository,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
    @Inject(BrokerResource) private brokerResource: BrokerResource,
    @Inject(SettingRepository) private settingRepository: SettingRepository
  ) {}
  async execute(
    params: ConnectBrokerServerInput,
    userId: string,
    merchantCode: string
  ): Promise<any> {
    const {
      bot_id,
      account_id,
      password,
      broker_code,
      broker_server,
      platform,
      profile_id,
    } = params
    // number of account_id = number of bot_id
    try {
      const brokerSettings = await this.settingRepository.findAppSetting({
        name: APP_SETTING.BROKER_SETTING,
      })
      let brokerSetting: BrokerSetting
      try {
        if (brokerSettings[0]?.value) {
          const dataPare: BrokerSettings =
            JSON.parse(brokerSettings[0].value) || {}
          brokerSetting = dataPare[broker_code]
        }
      } catch (error) {
        console.log('brokerSetting error', error)
      }
      if (brokerSetting?.required_profile && !profile_id) {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.PROFILE_REQUIRED,
        })
      }
      // account_id connect one bot one time
      const userBotByAccountId = await this.userRepository.findUserBotTrading({
        broker_account: account_id,
      })

      if (
        userBotByAccountId.length > 0 &&
        (userBotByAccountId[0].user_id !== userId ||
          userBotByAccountId[0].bot_id !== bot_id)
      ) {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.ACCOUNT_EXISTED,
        })
      }

      const events = await this.eventStoreRepository.find({
        user_id: userId,
        event_name: EVENT_STORE_NAME.TBOT_INACTIVE_BY_SYSTEM,
        state: EVENT_STORE_STATE.OPEN,
      })
      const numberOfTimeInactive = events.filter(
        (e) => e.metadata?.bot_id === params.bot_id
      ).length

      if (
        numberOfTimeInactive >
        APP_CONFIG.NUMBER_OF_TIMES_ALLOW_USER_ACTIVATE_TBOT
      ) {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.TBOT_BLOCKED,
        })
      }

      const [checkUserBot, bot] = await Promise.all([
        this.userRepository.findUserBotTrading({
          bot_id,
          user_id: userId,
        }),
        this.botTradingRepository.findById(bot_id),
      ])

      if (checkUserBot.length === 0 || !bot) {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.BOT_NOT_FOUND,
        })
      }

      const merchant = await this.merchantRepository.findOne({
        code: merchantCode,
      })
      console.log('merchant.config.brokers', merchant?.config?.brokers)
      if (merchant && brokerSetting?.check_referral_broker) {
        let broker: any = {}
        if (merchant.config?.brokers) {
          broker = merchant.config.brokers.find((br) => br.code === broker_code)
        }
        const checkReferral = await this.brokerResource.checkAccountReferral({
          account: account_id,
          broker_code,
          platform,
          ...broker?.referral_setting,
        })
        if (!checkReferral) {
          throwError({
            status: HttpStatus.BAD_REQUEST,
            ...ERROR_CODE.ACCOUNT_NOT_REFERRED,
          })
        }
      }

      let subscriber = {
        id: userBotByAccountId[0] ? userBotByAccountId[0].subscriber_id : null,
      }
      let account
      if (subscriber.id) {
        account = await this.metaapiResource.getAccount({
          account_id: subscriber.id,
        })
        if (account) {
          try {
            await account.update({
              name: `${merchantCode}_${account_id}_${userId}`,
              // password can be investor password for read-only access
              password: password,
              server: broker_server,
            })
          } catch (error) {
            console.log('error', error)
            if (error.status === HttpStatus.BAD_REQUEST) {
              throwError({
                status: HttpStatus.BAD_REQUEST,
                ...ERROR_CODE.ACCOUNT_INFO_INCORRECT,
              })
            }
            throwError({
              status: HttpStatus.BAD_REQUEST,
              ...ERROR_CODE.CONNECT_ACCOUNT_FAILED,
            })
          }
        }
      }
      if (!account) {
        try {
          subscriber = await this.metaapiResource.createSubscriber({
            login: account_id,
            password: password,
            name: `${merchantCode}_${account_id}_${userId}`,
            server: broker_server,
            platform,
            profile_id,
          })
        } catch (error) {
          console.log('error', error)
          if (error.status === HttpStatus.BAD_REQUEST) {
            throwError({
              status: HttpStatus.BAD_REQUEST,
              ...ERROR_CODE.ACCOUNT_INFO_INCORRECT,
            })
          }
          throwError({
            status: HttpStatus.BAD_REQUEST,
            ...ERROR_CODE.CONNECT_ACCOUNT_FAILED,
          })
        }
      }

      const updateUserBot = {
        id: checkUserBot[0].id,
        status: ITEM_STATUS.CONNECTING,
        broker_server: broker_server,
        broker: broker_code,
        broker_account: account_id,
        connected_at: Date.now(),
      }
      if (profile_id) {
        updateUserBot['profile_id'] = profile_id
      }
      if (subscriber && subscriber.id) {
        updateUserBot['subscriber_id'] = subscriber.id
        const updateBot = await this.userRepository.saveUserBotTrading([
          updateUserBot,
        ])
        delete updateBot[0].expires_at
        if (bot.code) {
          setTimeout(async () => {
            this.eventEmitter.emit(BOT_TRADING_EVENT.CONNECTING, {
              user_bot_id: updateBot[0].id,
              user_id: userId,
              bot_id,
              bot_code: bot.code,
            })
          }, 60 * 1000)
        }
        return updateBot[0]
      }
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.CONNECT_ACCOUNT_FAILED,
      })
    } catch (error) {
      if (profile_id) {
        await this.metaapiResource.removeProvisioningProfile(profile_id)
      }
      throw error
    }
  }
}
