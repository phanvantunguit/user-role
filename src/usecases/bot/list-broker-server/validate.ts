import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import { IsArray, IsOptional, IsString } from 'class-validator'

export class ListBrokerServerInput {
  @ApiProperty({
    required: true,
    example: 'axi',
  })
  @IsString()
  broker_code: string
}

export class ListBrokerServerOutput {
  @ApiProperty({
    required: false,
    isArray: true,
    type: String,
  })
  @IsOptional()
  @IsArray()
  @Type(() => String)
  payload: string[]
}
