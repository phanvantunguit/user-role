import { Inject } from '@nestjs/common'
import { APP_SETTING, SettingSignalPlatfrom } from 'src/const/app-setting'
import { ITEM_STATUS } from 'src/const/transaction'
import { BotSignalRepository } from 'src/repositories/bot-signal.repository'
import { BotRepository } from 'src/repositories/bot.repository'
import { SettingRepository } from 'src/repositories/setting.repository'
import { UserRepository } from 'src/repositories/user.repository'

import { UserListBotSignalInput, UserListSignalOutput } from './validate'
export class UserListSignalHandler {
  constructor(
    @Inject(BotSignalRepository)
    private botSignalRepository: BotSignalRepository,
    @Inject(UserRepository)
    private userRepository: UserRepository,
    @Inject(BotRepository)
    private botRepository: BotRepository,
    @Inject(SettingRepository) private settingRepository: SettingRepository
  ) {}
  async execute(
    params: UserListBotSignalInput,
    user_id: string
  ): Promise<UserListSignalOutput> {
    const favorite = params.favorite === 'true' ? true : false
    const [userBots, userSettingSignalPlatfrom] = await Promise.all([
      this.userRepository.findUserBotSQL({
        user_id,
        status: ITEM_STATUS.ACTIVE,
        expires_at: Date.now(),
      }),
      this.settingRepository.findOneAppSetting({
        name: APP_SETTING.SIGNAL_PLATFORM,
        user_id,
      }),
    ])
    let offBotCodes = []
    if (userSettingSignalPlatfrom && userSettingSignalPlatfrom.value) {
      try {
        const config = JSON.parse(
          userSettingSignalPlatfrom.value
        ) as SettingSignalPlatfrom
        if (config && config.OFF_SIGNALS_BOT) {
          offBotCodes = config.OFF_SIGNALS_BOT.toString().split(',')
        }
      } catch (error) {
        console.log('error', error)
      }
    }
    const queryBotIds = params.bot_ids?.split(',') || []
    let botIds = userBots.map((b) => b.bot_id)
    if (queryBotIds.length > 0) {
      botIds = botIds.filter((bs) => queryBotIds.includes(bs))
    }
    const bots = await this.botRepository.findByIds(botIds)
    const botCodes = []
    for (const b of bots) {
      if (b.code && !offBotCodes.includes(b.code)) {
        botCodes.push(b.code)
      }
    }
    if (botCodes.length === 0) {
      return {
        rows: [],
        page: 1,
        size: Number(params.size) || 50,
        count: 0,
        total: 0,
      }
    }
    const query = {
      page: params.page,
      size: params.size,
      exchanges: params.exchanges?.split(','),
      symbols: params.symbols?.split(','),
      names: botCodes,
      resolutions: params.resolutions?.split(','),
      types: params.types?.split(','),
      from: params.from,
      to: params.to,
      user_id,
    }
    if (favorite) {
      return this.botSignalRepository.getFavoritePaging(query)
    } else {
      return this.botSignalRepository.getPaging(query)
    }
  }
}
