import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsOptional,
  IsNumberString,
  IsString,
  IsNumber,
  IsArray,
  IsObject,
  IsBooleanString,
} from 'class-validator'
import { BotSignalValidate } from '../validate'

export class UserListBotSignalInput {
  @ApiProperty({
    required: false,
    description: 'Page',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  page: number

  @ApiProperty({
    required: false,
    description: 'Size',
    example: 50,
  })
  @IsOptional()
  @IsNumberString()
  size: number

  @ApiProperty({
    required: false,
    description: 'bot_ids',
    example:
      '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2,1c9e1891-b5b0-4949-bcfe-a2e25c1e53e1',
  })
  @IsOptional()
  @IsString()
  bot_ids?: string

  @ApiProperty({
    required: false,
    description: 'exchange name',
    example: 'BINANCE,FTX',
  })
  @IsOptional()
  @IsString()
  exchanges?: string

  @ApiProperty({
    required: false,
    description: 'resolution',
    example: '5m,7m',
  })
  @IsOptional()
  @IsString()
  resolutions?: string

  @ApiProperty({
    required: false,
    description: 'type',
    example: 'BUY,SELL',
  })
  @IsOptional()
  @IsString()
  types: string

  @ApiProperty({
    required: false,
    description: 'symbol',
    example: 'BTC,USD',
  })
  @IsOptional()
  @IsString()
  symbols?: string

  @ApiProperty({
    required: false,
    description: 'favorite',
    example: 'true',
  })
  @IsOptional()
  @IsBooleanString()
  favorite?: string

  @ApiProperty({
    required: false,
    description: 'From',
  })
  @IsOptional()
  @IsNumberString()
  from: number

  @ApiProperty({
    required: false,
    description: 'To',
  })
  @IsOptional()
  @IsNumberString()
  to: number
}

export class UserListSignalOutput {
  @ApiProperty({
    required: true,
    description: 'Page',
    example: 1,
  })
  @IsNumber()
  page: number

  @ApiProperty({
    required: true,
    description: 'Size',
    example: 50,
  })
  @IsNumber()
  size: number

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  count: number

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  total: number

  @ApiProperty({
    required: false,
    isArray: true,
    type: BotSignalValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => BotSignalValidate)
  rows: BotSignalValidate[]
}

export class UserListBotSignalPayloadOutput {
  @ApiProperty({
    required: false,
    type: UserListSignalOutput,
  })
  @IsObject()
  payload: UserListSignalOutput
}
