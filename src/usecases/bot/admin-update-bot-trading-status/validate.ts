import { ApiProperty } from '@nestjs/swagger'
import { IsIn, IsObject, IsString } from 'class-validator'
import { ITEM_STATUS } from 'src/const/transaction'

export class AdminUpdateBotTradingStatusInput {
  @ApiProperty({
    required: true,
    description: `Update status: ${ITEM_STATUS.INACTIVE}`,
    example: ITEM_STATUS.INACTIVE,
  })
  @IsString()
  @IsIn([ITEM_STATUS.INACTIVE])
  status: ITEM_STATUS
}

export class AdminUpdateBotTradingStatusValidate {
  @ApiProperty({
    required: true,
    example: 'efebdafe-c359-4e88-bd17-82b2a5b1091b',
  })
  @IsString()
  id: string

  @ApiProperty({
    required: true,
    description: `Update status: ${Object.values(ITEM_STATUS)}`,
    example: ITEM_STATUS.INACTIVE,
  })
  @IsString()
  status: ITEM_STATUS
}

export class AdminUpdateBotTradingStatusOutput {
  @ApiProperty({
    required: false,
    type: AdminUpdateBotTradingStatusValidate,
  })
  @IsObject()
  payload: AdminUpdateBotTradingStatusValidate
}
