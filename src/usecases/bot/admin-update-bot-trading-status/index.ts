import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { CHANELS } from 'src/const/app-setting'
import { TBOT_CHANEL_TS_EVENT } from 'src/const/bot'
import { ITEM_STATUS } from 'src/const/transaction'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { CacheRepository } from 'src/repositories/cache.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { MetaapiResource } from 'src/resources/forex'
import { throwError } from 'src/utils/handle-error.util'
import {
  AdminUpdateBotTradingStatusInput,
  AdminUpdateBotTradingStatusValidate,
} from './validate'

export class AdminUpdateBotTradingStatusHandler {
  constructor(
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(MetaapiResource) private metaapiResource: MetaapiResource,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(CacheRepository) private cacheRepository: CacheRepository
  ) {}
  async execute(
    params: AdminUpdateBotTradingStatusInput,
    id: string
  ): Promise<AdminUpdateBotTradingStatusValidate> {
    const { status } = params
    const userBot = await this.userRepository.findUserBotTrading({ id })
    if (userBot.length === 0) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }

    const bot = await this.botTradingRepository.findById(userBot[0].bot_id)
    if (!bot) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }

    if (
      !userBot[0].subscriber_id ||
      ![ITEM_STATUS.INACTIVE_BY_SYSTEM].includes(userBot[0].status)
    ) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.STATUS_INVALID,
      })
    }

    // check stopout === true? status : ITEM_STATUS.STOP_OUT
    const updateUserBot = {
      id: userBot[0].id,
      status,
    }
    try {
      const stopouts =
      await this.metaapiResource.copyFactory.tradingApi.getStopouts(
        userBot[0].subscriber_id
      )
      const isStopOut = stopouts.find((e) => e.strategy.id === bot.code)
      if (isStopOut) {
        updateUserBot.status = ITEM_STATUS.STOP_OUT
      }
    } catch (error) {
      
    }
    await this.userRepository.saveUserBotTrading([updateUserBot])
    this.cacheRepository.publisher.publish(
      CHANELS.TBOT_CHANEL_TS,
      JSON.stringify({
        event: TBOT_CHANEL_TS_EVENT.account,
        data: {
          bot_id: bot.id,
          user_id: userBot[0].user_id,
          user_bot_id: updateUserBot.id,
          status: updateUserBot.status,
        },
      })
    )
    return {
      id: updateUserBot.id,
      status,
    }
  }
}
