import { Inject } from '@nestjs/common'
import { BOT_STATUS } from 'src/const'
import { APP_SETTING, EVENT_STORE_STATE } from 'src/const/app-setting'
import { ITEM_STATUS } from 'src/const/transaction'
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { EventStoreRepository } from 'src/repositories/event-store.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { UserGetBotTradingHandler } from '../user-get-bot-trading'
import { BotDataValidate } from '../user-list-bot/validate'
export class UserListBotTradingPlanHandler {
  constructor(
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(UserRepository) private userRepository: UserRepository,
    // @Inject(AccountBalanceRepository)
    // private accountBalanceRepository: AccountBalanceRepository,
    @Inject(UserGetBotTradingHandler)
    private userGetBotTradingHandler: UserGetBotTradingHandler,
    @Inject(EventStoreRepository)
    private eventStoreRepository: EventStoreRepository
  ) {}
  async execute(user_id: string, status?: string): Promise<BotDataValidate[]> {
    let userBots = await this.userRepository.findUserBotTradingSQL({
      user_id,
      //@ts-ignore
      status: status ? status.split(',') : null,
    })
    // this code will be remove
    const userBotIdDuplicate = []
    userBots = userBots.filter((ub) => {
      if (!userBotIdDuplicate.includes(ub.bot_id)) {
        userBotIdDuplicate.push(ub.bot_id)
        return true
      }
      return false
    })
    const bots = await this.botTradingRepository.findByIds(userBotIdDuplicate)

    const events = await this.eventStoreRepository.find({
      user_id: user_id,
      event_name: APP_SETTING.TBOT_INACTIVE_BY_SYSTEM,
      state: EVENT_STORE_STATE.OPEN,
    })
    const eventByBot: { [bot_id: string]: any } = {}

    events.forEach((e) => {
      if (e.metadata?.bot_id) {
        if (!eventByBot[e.metadata.bot_id]) {
          eventByBot[e.metadata.bot_id] = {
            count: 1,
            created_at: e.created_at,
          }
        } else {
          eventByBot[e.metadata.bot_id].count += 1
          if (eventByBot[e.metadata.bot_id].created_at < e.created_at) {
            eventByBot[e.metadata.bot_id].created_at = e.created_at
          }
        }
      }
    })

    const mapBots = []
    for (let b of bots) {
      const userBotExit = userBots.find((ub) => ub.bot_id === b.id)
      if (userBotExit) {
        const ubData = await this.userGetBotTradingHandler.execute(
          userBotExit.bot_id,
          userBotExit.user_id
        )
        mapBots.push({
          ...ubData,
          count_inactive_by_system: eventByBot[b.id]?.count || 0,
          inactive_by_system_at: eventByBot[b.id]?.created_at,
        })
      } else {
        mapBots.push(b)
      }
    }
    return mapBots
  }
}
