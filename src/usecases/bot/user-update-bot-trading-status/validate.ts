import { ApiProperty } from '@nestjs/swagger'
import { IsIn, IsString } from 'class-validator'
import { ITEM_STATUS } from 'src/const/transaction'

export class UserUpdateBotTradingStatusInput {
  @ApiProperty({
    required: true,
    example: 'efebdafe-c359-4e88-bd17-82b2a5b1091b',
  })
  @IsString()
  bot_id: string

  @ApiProperty({
    required: true,
    description: `Update status: ${ITEM_STATUS.ACTIVE}`,
    example: ITEM_STATUS.ACTIVE,
  })
  @IsString()
  @IsIn([ITEM_STATUS.ACTIVE])
  status: ITEM_STATUS
}
