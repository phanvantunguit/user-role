import { HttpStatus, Inject } from '@nestjs/common'
import { EventEmitter2 } from '@nestjs/event-emitter'
import { APP_CONFIG } from 'src/config'
import { ERROR_CODE } from 'src/const'
import {
  APP_SETTING,
  EVENT_STORE_NAME,
  EVENT_STORE_STATE,
} from 'src/const/app-setting'
import { BOT_TRADING_EVENT } from 'src/const/bot'
import { ITEM_STATUS } from 'src/const/transaction'
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { EventStoreRepository } from 'src/repositories/event-store.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { MetaapiResource } from 'src/resources/forex'
import { throwError } from 'src/utils/handle-error.util'
import { UserUpdateBotTradingStatusInput } from './validate'

export class UserUpdateBotTradingStatusHandler {
  constructor(
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(MetaapiResource) private metaapiResource: MetaapiResource,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(EventStoreRepository)
    private eventStoreRepository: EventStoreRepository,
    @Inject(AccountBalanceRepository)
    private accountBalanceRepository: AccountBalanceRepository,
    @Inject(EventEmitter2) private eventEmitter: EventEmitter2
  ) {}
  async execute(
    params: UserUpdateBotTradingStatusInput,
    userId: string,
    merchantCode: string
  ): Promise<any> {
    const { bot_id, status } = params
    const [checkUserBot, bot] = await Promise.all([
      this.userRepository.findUserBotTrading({
        bot_id,
        user_id: userId,
      }),
      this.botTradingRepository.findById(bot_id),
    ])
    if (checkUserBot.length === 0 || !bot) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }
    if (
      !checkUserBot[0].subscriber_id ||
      ![
        ITEM_STATUS.INACTIVE,
        ITEM_STATUS.STOP_OUT,
        ITEM_STATUS.INACTIVE_BY_SYSTEM,
      ].includes(checkUserBot[0].status)
    ) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.ACCOUNT_NOT_CONNECTED,
      })
    }
    if (checkUserBot[0].status === status) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.STATUS_INVALID,
      })
    }
    if (checkUserBot[0].status === ITEM_STATUS.INACTIVE_BY_SYSTEM) {
      const events = await this.eventStoreRepository.find({
        user_id: userId,
        event_name: APP_SETTING.TBOT_INACTIVE_BY_SYSTEM,
        state: EVENT_STORE_STATE.OPEN,
      })
      let inactiveBySystemAt = 0
      let countInactive = 0
      events.forEach((e) => {
        if (e.metadata?.bot_id === bot_id) {
          countInactive += 1
          if (inactiveBySystemAt < e.created_at) {
            inactiveBySystemAt = e.created_at
          }
        }
      })
      if (
        countInactive > 1 &&
        Number(inactiveBySystemAt) + APP_CONFIG.TIME_BLOCKED_TBOT > Date.now()
      ) {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.TBOT_BLOCKED,
        })
      }
    }
    const events = await this.eventStoreRepository.find({
      user_id: userId,
      event_name: EVENT_STORE_NAME.TBOT_INACTIVE_BY_SYSTEM,
      state: EVENT_STORE_STATE.OPEN,
    })
    const numberOfTimeInactive = events.filter(
      (e) => e.metadata?.bot_id === params.bot_id
    ).length

    if (
      numberOfTimeInactive > APP_CONFIG.NUMBER_OF_TIMES_ALLOW_USER_ACTIVATE_TBOT
    ) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.TBOT_BLOCKED,
      })
    }
    let startTime = checkUserBot[0].connected_at
      ? new Date(Number(checkUserBot[0].connected_at))
      : new Date(Number(checkUserBot[0].updated_at))

    const account = await this.metaapiResource.getAccount({
      account_id: checkUserBot[0].subscriber_id,
    })
    if (
      account &&
      account.state === 'DEPLOYED' &&
      account.connectionStatus === 'CONNECTED'
    ) {
      const connection = await account.getRPCConnection()
      await connection.connect()
      await connection.waitSynchronized()
      const accountInfo = await connection.getAccountInformation()
      
      const positions = await connection.getPositions()
      if (positions.length > 0) {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.POSITION_NOT_EMPTY,
        })
      }
      
      let isStopOut 
      try {
        const stopouts =
        await this.metaapiResource.copyFactory.tradingApi.getStopouts(checkUserBot[0].subscriber_id)
        isStopOut = stopouts.find((e) => e.strategy.id === bot.code)
      } catch (error) {
        console.log("getStopouts error", error)
      }

      if (Number(accountInfo.balance) < Number(bot.balance)) {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.ACCOUNT_NOT_ENOUGH_BALANCE,
        })
      }
      const updateUserBot = {
        id: checkUserBot[0].id,
        status: ITEM_STATUS.CONNECTING,
      }
      const updateBot = await this.userRepository.saveUserBotTrading([
        updateUserBot,
      ])
      this.eventEmitter.emit(BOT_TRADING_EVENT.CONNECTING, {
        user_bot_id: updateBot[0].id,
        user_id: userId,
        bot_id,
        bot_code: bot.code,
      })
      delete updateBot[0].expires_at
      return updateBot[0]
    } else {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.ACCOUNT_NOT_CONNECTED,
      })
    }
  }
}
