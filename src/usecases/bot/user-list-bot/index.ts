import { Inject } from '@nestjs/common'
import { BOT_STATUS } from 'src/const'
import { ITEM_STATUS, ORDER_CATEGORY } from 'src/const/transaction'
import { BotRepository } from 'src/repositories/bot.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { BotDataValidate } from './validate'
export class UserListBotHandler {
  constructor(
    @Inject(BotRepository) private botRepository: BotRepository,
    @Inject(UserRepository) private userRepository: UserRepository
  ) {}
  async execute(user_id?: string): Promise<BotDataValidate[]> {
    const bots = await this.botRepository.listConditionMultiple({
      status: [BOT_STATUS.OPEN, BOT_STATUS.COMINGSOON],
    })
    if (!user_id) {
      return bots
    }
    let userBots = await this.userRepository.findUserBot({
      user_id,
      status: ITEM_STATUS.ACTIVE,
    })
    // this code will be remove
    const userBotIdDuplicate = []
    userBots = userBots.filter((ub) => {
      if (!userBotIdDuplicate.includes(ub.bot_id)) {
        userBotIdDuplicate.push(ub.bot_id)
        return true
      }
      return false
    })
    const mapBots = bots.map((b) => {
      const userBotExit = userBots.find((ub) => ub.bot_id === b.id)
      if (userBotExit) {
        return {
          ...b,
          expires_at: userBotExit.expires_at,
        }
      } else {
        return b
      }
    })
    return mapBots
  }
}
