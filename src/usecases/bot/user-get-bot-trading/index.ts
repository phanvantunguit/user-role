import { Inject } from '@nestjs/common'
import { ITEM_STATUS } from 'src/const/transaction'
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository'
import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { UserRepository } from 'src/repositories/user.repository'
export class UserGetBotTradingHandler {
  constructor(
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(AccountBalanceRepository)
    private accountBalanceRepository: AccountBalanceRepository,
    @Inject(BotTradingHistoryRepository)
    private botTradingHistoryRepository: BotTradingHistoryRepository,
    @Inject(UserRepository)
    private userRepository: UserRepository
  ) {}
  async execute(bot_id: string, user_id: string): Promise<any> {
    const bot = await this.botTradingRepository.findById(bot_id)
    if (!user_id) {
      return bot
    }
    const userBot = await this.userRepository.findUserBotTrading({
      user_id,
      bot_id,
    })
    if (userBot.length === 0) {
      return bot
    }

    const userBotExit = userBot[0]
    const minBalance = bot.balance
    if(!userBotExit.subscriber_id || userBotExit.status === ITEM_STATUS.CONNECTING || userBotExit.status === ITEM_STATUS.NOT_CONNECT) {
      return {
        ...bot,
        expires_at: userBotExit.expires_at,
        user_status: userBotExit.status,
        broker: userBotExit.broker,
        broker_server: userBotExit.broker_server,
        broker_account: userBotExit.broker_account,
        min_balance: minBalance,
        balance: 0
      }

    }
    const time7Day = new Date()
    time7Day.setDate(time7Day.getDate() - 7)
    time7Day.setHours(0, 0, 0, 0)

    const time30Day = new Date()
    time30Day.setDate(time30Day.getDate() - 30)
    time30Day.setHours(0, 0, 0, 0)

    const timeToDay = new Date()
    timeToDay.setHours(0, 0, 0, 0)

    const [
      balanceInit,
      profit7Day,
      profit30Day,
      profitToday,
      totalProfit,
      totalTrade,
      balanceLastet,
    ] = await Promise.all([
      this.accountBalanceRepository.getBalanceByUser({
        user_id,
        bot_id,
      }),
      this.botTradingHistoryRepository.getProfit({
        user_id,
        bot_id,
        to: time7Day.valueOf(),
        connected_at: userBotExit.connected_at,
      }),
      this.botTradingHistoryRepository.getProfit({
        user_id,
        bot_id,
        to: time30Day.valueOf(),
        connected_at: userBotExit.connected_at,
      }),
      this.botTradingHistoryRepository.getProfit({
        user_id,
        bot_id,
        to: timeToDay.valueOf(),
        connected_at: userBotExit.connected_at,
      }),
      this.botTradingHistoryRepository.getProfit({
        user_id,
        bot_id,
        connected_at: userBotExit.connected_at,
      }),
      this.botTradingHistoryRepository.count({
        user_id,
        bot_id,
        connected_at: userBotExit.connected_at,
      }),
      this.accountBalanceRepository.getBalance({
        user_id,
        bot_id,
        order: 'desc',
      }),
    ])

    const balance_init = balanceInit[0] ? balanceInit[0].total : 0
    const pnl_30_day_init = (Number(profit30Day[0]?.total) || 0) + balance_init
    const pnl_7_day_init = (Number(profit7Day[0]?.total) || 0) + balance_init
    const pnl_day_init = (Number(profitToday[0]?.total) || 0) + balance_init
    const balance_current = (Number(totalProfit[0]?.total) || 0) + balance_init
    bot.balance = balanceLastet[0] ? balanceLastet[0].balance : 0

    return {
      ...bot,

      pnl_day_current: balance_current,
      pnl_day_init,

      pnl_7_day_current: balance_current,
      pnl_7_day_init,

      pnl_30_day_current: balance_current,
      pnl_30_day_init,

      balance_init,
      balance_current,
      total_trade: totalTrade,

      expires_at: userBotExit.expires_at,
      user_status: userBotExit.status,
      broker: userBotExit.broker,
      broker_server: userBotExit.broker_server,
      broker_account: userBotExit.broker_account,
      min_balance: minBalance
    }
  }
}
