import { Inject } from '@nestjs/common'
import { ITEM_STATUS } from 'src/const/transaction'
import { BotRepository } from 'src/repositories/bot.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { BotDataValidate } from '../user-list-bot/validate'
export class UserListBotPaidHandler {
  constructor(
    @Inject(BotRepository) private botRepository: BotRepository,
    @Inject(UserRepository) private userRepository: UserRepository
  ) {}
  async execute(user_id: string): Promise<BotDataValidate[]> {
    let userBots = await this.userRepository.findUserBotSQL({
      user_id,
      status: ITEM_STATUS.ACTIVE,
      expires_at: Date.now(),
    })
    // this code will be remove
    const userBotIdDuplicate = []
    userBots = userBots.filter((ub) => {
      if (!userBotIdDuplicate.includes(ub.bot_id)) {
        userBotIdDuplicate.push(ub.bot_id)
        return true
      }
      return false
    })
    const bots = await this.botRepository.findByIds(userBotIdDuplicate)
    const mapBots = bots.map((b) => {
      const userBotExit = userBots.find((ub) => ub.bot_id === b.id)
      if (userBotExit) {
        return {
          ...b,
          expires_at: userBotExit.expires_at,
        }
      } else {
        return b
      }
    })
    return mapBots
  }
}
