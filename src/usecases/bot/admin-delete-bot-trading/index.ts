import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { CHANELS } from 'src/const/app-setting'
import { TBOT_CHANEL_TS_EVENT } from 'src/const/bot'
import { ITEM_STATUS } from 'src/const/transaction'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { CacheRepository } from 'src/repositories/cache.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { MetaapiResource } from 'src/resources/forex'
import { throwError } from 'src/utils/handle-error.util'

export class AdminDeleteBotTradingHandler {
  constructor(
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(MetaapiResource) private metaapiResource: MetaapiResource,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(CacheRepository) private cacheRepository: CacheRepository
  ) {}
  async execute(id: string): Promise<boolean> {
    const userBot = await this.userRepository.findUserBotTrading({ id })
    if (userBot.length === 0) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }

    const bot = await this.botTradingRepository.findById(userBot[0].bot_id)
    if (!bot) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }
    if (userBot[0].subscriber_id) {
      try {
        await this.metaapiResource.removeSubscriber({
          subscriber_id: userBot[0].subscriber_id,
        })
      } catch (error) {
        console.log('removeSubscriber error', error)
      }
      await this.metaapiResource.removeAccount({
        subscriber_id: userBot[0].subscriber_id,
      })
    }
    if (userBot[0].profile_id) {
      await this.metaapiResource.removeProvisioningProfile(
        userBot[0].profile_id
      )
    }
    await this.userRepository.deleteUserBotTrading({ id })

    this.cacheRepository.publisher.publish(
      CHANELS.TBOT_CHANEL_TS,
      JSON.stringify({
        event: TBOT_CHANEL_TS_EVENT.account,
        data: {
          bot_id: bot.id,
          user_id: userBot[0].user_id,
          user_bot_id: userBot[0].id,
          status: ITEM_STATUS.DELETED,
        },
      })
    )
    return true
  }
}
