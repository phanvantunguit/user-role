import { ApiProperty } from '@nestjs/swagger'
import { IsObject, IsString } from 'class-validator'

export class UploadBrokerProfileOutput {
  @ApiProperty({
    required: true,
  })
  @IsString()
  profile_id?: string
}
