import { Inject } from '@nestjs/common'
import { TBOT_PLATFORM } from 'src/const/bot'
import { MetaapiResource } from 'src/resources/forex'
import { UploadBrokerProfileOutput } from './validate'
import { SettingRepository } from 'src/repositories/setting.repository'
import { APP_SETTING } from 'src/const/app-setting'
import {
  BrokerSetting,
  BrokerSettings,
} from 'src/domains/setting/app-setting.types'

export class UploadBrokerProfileHandler {
  constructor(
    @Inject(MetaapiResource) private metaapiResource: MetaapiResource,
    @Inject(SettingRepository) private settingRepository: SettingRepository
  ) {}
  async execute(
    broker_code: string,
    file,
    platform: TBOT_PLATFORM,
    merchant_code,
    account_id,
    user_id,
  ): Promise<UploadBrokerProfileOutput> {
    const brokerSettings = await this.settingRepository.findAppSetting({
      name: APP_SETTING.BROKER_SETTING,
    })
    let brokerSetting: BrokerSetting
    try {
      if (brokerSettings[0]?.value) {
        const dataPare: BrokerSettings =
          JSON.parse(brokerSettings[0].value) || {}
        brokerSetting = dataPare[broker_code]
      }
    } catch (error) {
      console.log('brokerSetting error', error)
    }

    const { fieldname, originalname, encoding, mimetype, buffer, size } = file
    const version = platform === TBOT_PLATFORM.mt4 ? 4 : 5
    const profile = await this.metaapiResource.createProvisioningProfile({
      name: originalname,
      version,
      brokerTimezone: brokerSetting?.broker_timezone,
      brokerDSTSwitchTimezone: brokerSetting?.broker_dst_switch_timezone,
      file: buffer,
      merchant_code,
      account_id,
      user_id,
    })
    return { profile_id: profile.id }
  }
}
