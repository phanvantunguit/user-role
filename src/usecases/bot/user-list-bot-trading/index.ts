import { Inject } from '@nestjs/common'
import { BOT_STATUS, MERCHANT_COMMISSION_STATUS } from 'src/const'
import { ITEM_STATUS, ORDER_CATEGORY } from 'src/const/transaction'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { BotTradingDataValidate } from './validate'
import { MerchantCommissionRepository } from 'src/repositories/merchant-commission.repository'
import { EventStoreRepository } from 'src/repositories/event-store.repository'
import { APP_SETTING, EVENT_STORE_STATE } from 'src/const/app-setting'
export class UserListBotTradingHandler {
  constructor(
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(MerchantCommissionRepository)
    private merchantCommissionRepository: MerchantCommissionRepository,
    @Inject(EventStoreRepository)
    private eventStoreRepository: EventStoreRepository
  ) {}
  async execute(
    user_id?: string,
    merchant_id?: string
  ): Promise<BotTradingDataValidate[]> {
    let ids
    if (merchant_id) {
      const merchantBot = await this.merchantCommissionRepository.find({
        merchant_id,
        status: MERCHANT_COMMISSION_STATUS.ACTIVE,
        category: ORDER_CATEGORY.TBOT,
      })
      ids = merchantBot.map((e) => e.asset_id)
    }
    if (ids && ids.length === 0) {
      return []
    }
    const bots = await this.botTradingRepository.listConditionMultiple({
      status: [BOT_STATUS.OPEN, BOT_STATUS.COMINGSOON],
      id: ids,
    })
    if (!user_id) {
      return bots
    }
    let userBots = await this.userRepository.findUserBotTradingSQL({
      user_id,
    })
    // this code will be remove
    const userBotIdDuplicate = []
    userBots = userBots.filter((ub) => {
      if (!userBotIdDuplicate.includes(ub.bot_id)) {
        userBotIdDuplicate.push(ub.bot_id)
        return true
      }
      return false
    })

    const events = await this.eventStoreRepository.find({
      user_id: user_id,
      event_name: APP_SETTING.TBOT_INACTIVE_BY_SYSTEM,
      state: EVENT_STORE_STATE.OPEN,
    })
    const eventByBot: { [bot_id: string]: any } = {}

    events.forEach((e) => {
      if (e.metadata?.bot_id) {
        if (!eventByBot[e.metadata.bot_id]) {
          eventByBot[e.metadata.bot_id] = {
            count: 1,
            created_at: e.created_at,
          }
        } else {
          eventByBot[e.metadata.bot_id].count += 1
          if (eventByBot[e.metadata.bot_id].created_at < e.created_at) {
            eventByBot[e.metadata.bot_id].created_at = e.created_at
          }
        }
      }
    })

    const mapBots = bots.map((b) => {
      const userBotExit = userBots.find((ub) => ub.bot_id === b.id)
      if (userBotExit) {
        return {
          ...b,
          expires_at: userBotExit.expires_at,
          count_inactive_by_system: eventByBot[b.id]?.count || 0,
          inactive_by_system_at: eventByBot[b.id]?.created_at,
        }
      } else {
        return b
      }
    })
    return mapBots
  }
}
