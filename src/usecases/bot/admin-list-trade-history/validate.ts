import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsOptional,
  IsNumberString,
  IsString,
  IsIn,
} from 'class-validator'

export class AdminListTradeHistoryInput {
  @ApiProperty({
    required: false,
    description: 'Page',
  })
  @IsOptional()
  @IsNumberString()
  page: number

  @ApiProperty({
    required: false,
    description: 'Size',
  })
  @IsOptional()
  @IsNumberString()
  size: number

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  keyword: string

  @ApiProperty({
    required: false,
    description: 'type',
  })
  @IsOptional()
  @IsString()
  type?: string
  
  @ApiProperty({
    required: false,
    description: 'symbols',
  })
  @IsOptional()
  @IsString()
  symbols?: string

  @ApiProperty({
    required: false,
    description: 'name_bots',
  })
  @IsOptional()
  @IsString()
  name_bots

  @ApiProperty({
    required: false,
    description: 'resolutions',
  })
  @IsOptional()
  @IsString()
  resolutions?: string

  @ApiProperty({
    required: false,
    description: 'position_side',
  })
  @IsOptional()
  @IsString()
  position_side?: string

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  sort_by: string;

  @ApiProperty({
    required: false,
    description: 'desc or asc',
    example: 'desc',
  })
  @IsOptional()
  @IsIn(['desc', 'asc'])
  order_by: 'desc' | 'asc';

  @ApiProperty({
    required: false,
    description: 'From',
  })
  @IsOptional()
  @IsNumberString()
  from: number

  @ApiProperty({
    required: false,
    description: 'To',
  })
  @IsOptional()
  @IsNumberString()
  to: number
}