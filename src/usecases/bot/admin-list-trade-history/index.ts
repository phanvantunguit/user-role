import { AdminListTradeHistoryInput } from './validate'

export class AdminListTradeHistoryHandler {
  constructor() {}
  async execute(params: AdminListTradeHistoryInput): Promise<any> {
    return {
      rows: [],
      page: 1,
      size: 1,
      count: 1,
      total: 1,
    }
  }
}
