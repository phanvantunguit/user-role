import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { ITEM_STATUS } from 'src/const/transaction'
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { MetaapiResource } from 'src/resources/forex'
import { throwError } from 'src/utils/handle-error.util'

export class AdminResetSubscriptionHandler {
  constructor(
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(MetaapiResource) private metaapiResource: MetaapiResource,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(AccountBalanceRepository)
    private accountBalanceRepository: AccountBalanceRepository
  ) {}
  async execute(): Promise<any> {
    const [userBots, bots] = await Promise.all([
      this.userRepository.findUserBotTrading({
        status: ITEM_STATUS.ACTIVE,
      }),
      this.botTradingRepository.find({}),
    ])
    this.asyncExecute(userBots, bots)
    return userBots.length
  }
  async asyncExecute(userBots, bots) {
    for (let ub of userBots) {
      try {
        const bot = bots.find((b) => b.id === ub.bot_id)
      let startTime = ub.connected_at
        ? new Date(Number(ub.connected_at))
        : new Date(Number(ub.updated_at))
      if (startTime.toString() === 'Invalid Date') {
        startTime = new Date()
      }
      const account = await this.metaapiResource.getAccount({
        account_id: ub.subscriber_id,
      })
      if (
        account &&
        account.state === 'DEPLOYED' &&
        account.connectionStatus === 'CONNECTED'
      ) {
        const connection = await account.getRPCConnection()
        await connection.connect()
        await connection.waitSynchronized()
        const accountInfo = await connection.getAccountInformation()

        const accountBalance = await this.accountBalanceRepository.findOne({
          user_id: ub.user_id,
          bot_id: ub.bot_id,
          change_id: 'init',
        })
        const initBalance = accountBalance
          ? accountBalance.balance
          : accountInfo.balance
        if (Number(accountInfo.balance) < Number(bot.balance)) {
          throwError({
            status: HttpStatus.BAD_REQUEST,
            ...ERROR_CODE.ACCOUNT_NOT_ENOUGH_BALANCE,
          })
        }
        const multiplier = Math.floor(Number(initBalance) / Number(bot.balance))
        console.log('startTime', startTime)
        const maxAbsoluteRisk =
          (Number(bot.max_drawdown) - Number(bot.max_drawdown_change_percent)) *
          multiplier *
          Number(bot.balance)
        const user = await this.userRepository.findOneUser({ id: ub.user_id })
        if (user) {
          await this.metaapiResource.updateSubscriber({
            subscriber_id: ub.subscriber_id,
            name: `${user.merchant_code}_${ub.broker_account}_${user.id}`,
            strategy_id: bot.code,
            multiplier,
            max_absolute_risk: Math.ceil(maxAbsoluteRisk),
            start_time: startTime,
            broker: ub.broker
          })
        }
      }
      } catch (error) {
        
      }
    }
  }
}
