import { Inject } from '@nestjs/common'
import { TBOT_CHANEL_TS_EVENT } from 'src/const/bot'
import { WS_EVENT } from 'src/const/response'
import { BotTradingInitEvent } from './events/bot-trading-init.ws.event'
import { WebSocketMessage, WSClient } from './types'

export class WebSocketEvent {
  constructor(
    @Inject(BotTradingInitEvent)
    private botTradingInitEvent: BotTradingInitEvent
  ) {}
  request(ws: WSClient, message: WebSocketMessage) {
    const _this = this
    const sendMessage = (message: WebSocketMessage, options?: any) => {
      if (ws) {
        if (typeof message === 'object') {
          ws.send(JSON.stringify(message), options)
        } else {
          ws.send(message, options)
        }
      }
    }
    switch (message.event) {
      case WS_EVENT.tbot_init:
        console.log('ws.user_id', ws.user_id)
        this.botTradingInitEvent.execute({
          user_id: ws.user_id,
          ...message.data,
          cb: sendMessage,
        })
        break

      default:
        break
    }
  }

  subscribeWSChanel(message: WebSocketMessage) {
    const _this = this
    switch (message.event) {
      case WS_EVENT.app_message:
        break

      default:
        break
    }
  }
  subscribeTradingBotChanel(message: {
    data: any
    event: TBOT_CHANEL_TS_EVENT
  }) {
    switch (message.event) {
      case TBOT_CHANEL_TS_EVENT.account:
        break
      case TBOT_CHANEL_TS_EVENT.trades:
        break
      default:
        break
    }
  }
}
