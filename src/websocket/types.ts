import { WS_EVENT } from 'src/const'

export type WebSocketMessage = {
  event: WS_EVENT
  data: any
}

export interface IVerify {
  active_client_ids: string[]
  // active_token_ids: string[]
  user_id: string
  token_id: string
  client_id: string
  verify_id: string
}
export interface ICheckBrowser {
  client_id: string
  user_id: string
  token_id: string
  verify_id: string
}
export interface IAppSendMessage {
  user_id?: string
  event: string
  data: any
}
export interface IDataSubscriber {
  event: string
  data: any
}
export interface IMessage {
  event: string
  data: string
}

export interface IBotSignalMessage {
  e: 'signal'
  exchange: 'BINANCE'
  symbol: 'LTCUSDT'
  name: 'botBoxCandle'
  resolution: '5m'
  signal_id: 'fad731ed_1673254427814'
  type: 'BUY'
  time: 1671527700000
  image_url: 'https://static.cextrading.io/images/Bot_Box/Bot_Box_LTCUSDT_5m_1671527700000.png'
  metadata: {
    idSignal: 'fad731ed_1673254427814'
    positionSide: 'LONG'
    side: 'BUY'
  }
  created_at: 1673254434062
}

export type WSClient = {
  id: string
  verify_id: string
  user_id: string
  token_id: string
  send: (data: string, options?: any) => void
}
