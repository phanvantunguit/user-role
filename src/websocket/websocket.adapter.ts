import { INestApplication, Logger } from '@nestjs/common'
import { APP_CONFIG } from 'src/config'
import { IToken } from 'src/const/authorization'
import { SessionService } from 'src/services/session'
import { decodeBase64, verifyToken } from 'src/utils/hash.util'
import { WebSocketServer } from 'ws'
import { v4 } from 'uuid'
import * as redis from 'redis'
import {
  APP_SETTING,
  CHANELS,
  EVENT_STORE_NAME,
  EVENT_STORE_STATE,
  SettingSignalPlatfrom,
} from 'src/const/app-setting'
import { UserRepository } from 'src/repositories/user.repository'
import { CacheRepository } from 'src/repositories/cache.repository'
import { GeneralSettingRoleService } from 'src/services/general-setting-role'
import { BotSignalService } from 'src/services/bot-signal'
import { WS_EVENT } from 'src/const'
import { ITEM_STATUS } from 'src/const/transaction'
import { BotRepository } from 'src/repositories/bot.repository'
import { SettingRepository } from 'src/repositories/setting.repository'
import { WebSocketEvent } from './websocket.event'
import { BOT_TRADING_EVENT, TBOT_CHANEL_TS_EVENT } from 'src/const/bot'
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository'
import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository'
import { EventEmitter2 } from '@nestjs/event-emitter'
import { EventStoreRepository } from 'src/repositories/event-store.repository'
import { BotTradingService } from 'src/services/bot-trading'
import { UserGetBotTradingHandler } from 'src/usecases/bot/user-get-bot-trading'
import { DeactivateUserBotTradingHandler } from 'src/usecases/bot/deactive-user-bot-trading'

interface IVerify {
  active_client_ids: string[]
  // active_token_ids: string[]
  user_id: string
  token_id: string
  client_id: string
  verify_id: string
}
interface ICheckBrowser {
  client_id: string
  user_id: string
  token_id: string
  verify_id: string
}
interface IAppSendMessage {
  user_id?: string
  event: string
  data: any
}
interface IDataSubscriber {
  event: string
  data: any
}
interface IMessage {
  event: string
  data: string
}

interface IBotSignalMessage {
  e: 'signal'
  exchange: 'BINANCE'
  symbol: 'LTCUSDT'
  name: 'botBoxCandle'
  resolution: '5m'
  signal_id: 'fad731ed_1673254427814'
  type: 'BUY'
  time: 1671527700000
  image_url: 'https://static.cextrading.io/images/Bot_Box/Bot_Box_LTCUSDT_5m_1671527700000.png'
  metadata: {
    idSignal: 'fad731ed_1673254427814'
    positionSide: 'LONG'
    side: 'BUY'
  }
  created_at: 1673254434062
}
export class WebSocketAdapter {
  private logger: Logger = new Logger('WSGateway')
  clientsUserId = new Map()
  clientsBot = new Map()
  clientsBotTrading = new Map()
  wss
  sessionService: SessionService
  userRepository: UserRepository
  cacheRepository: CacheRepository
  generalSettingRoleService: GeneralSettingRoleService
  botSignalService: BotSignalService
  botRepository: BotRepository
  settingRepository: SettingRepository
  accountBalanceRepository: AccountBalanceRepository
  botTradingHistoryRepository: BotTradingHistoryRepository
  userGetBotTradingHandler: UserGetBotTradingHandler
  subscriber
  publisher
  isAlive = true
  webSocketEvent: WebSocketEvent
  eventStoreRepository: EventStoreRepository
  botTradingService: BotTradingService
  deactivateUserBotTradingHandler: DeactivateUserBotTradingHandler
  constructor(private app: INestApplication) {
    this.wss = new WebSocketServer({ server: app.getHttpServer() })
    this.sessionService = app.get(SessionService)
    this.userRepository = app.get(UserRepository)
    this.cacheRepository = app.get(CacheRepository)
    this.generalSettingRoleService = app.get(GeneralSettingRoleService)
    this.botSignalService = app.get(BotSignalService)
    this.botRepository = app.get(BotRepository)
    this.settingRepository = app.get(SettingRepository)
    this.webSocketEvent = app.get(WebSocketEvent)
    this.accountBalanceRepository = app.get(AccountBalanceRepository)
    this.botTradingHistoryRepository = app.get(BotTradingHistoryRepository)
    this.botTradingHistoryRepository = app.get(BotTradingHistoryRepository)
    this.eventStoreRepository = app.get(EventStoreRepository)
    this.botTradingService = app.get(BotTradingService)
    this.userGetBotTradingHandler = app.get(UserGetBotTradingHandler)
    this.deactivateUserBotTradingHandler = app.get(
      DeactivateUserBotTradingHandler
    )

    this.subscriber = redis.createClient({
      host: APP_CONFIG.REDIS_HOST,
      port: APP_CONFIG.REDIS_PORT,
      password: APP_CONFIG.REDIS_PASS,
    })
    // this.subscriber = redis.createClient({
    //   tls: {
    //     rejectUnauthorized: false,
    //   },
    //   url: APP_CONFIG.HEROKU_REDIS_CYAN_URL,
    // })

    this.subscriber.on('error', (error) => {
      console.log('WSocket error', error)
    })
    this.subscriber.on('connect', (error) => {
      console.log('WSocket connect')
      this.publisher = this.subscriber.duplicate()
    })
  }

  async forceLogoutClient(params: IVerify, wsServer: any) {
    const _this = this
    const clients = this.clientsUserId.get(params.user_id) || []
    if (clients.length > 0) {
      clients.forEach(function each(ws) {
        if (
          ws.user_id === params.user_id &&
          !params.active_client_ids.includes(ws.id)
        ) {
          if (params.token_id === ws.token_id) {
            ws.send(
              JSON.stringify({
                event: WS_EVENT.deactivate,
              })
            )
            _this.logger.log(`id: ${ws.id} user: ${ws.user_id} Deactivate`)
          } else {
            ws.send(
              JSON.stringify({
                event: WS_EVENT.logout,
              })
            )
            _this.logger.log(`id: ${ws.id} user: ${ws.user_id} Logout`)
          }
        }
      })
    }
  }
  async checkBrowser(params: ICheckBrowser, wsServer) {
    const clients = this.clientsUserId.get(params.user_id) || []
    if (clients.length > 0) {
      clients.forEach(function each(ws) {
        if (ws.token_id === params.token_id && ws.id !== params.client_id) {
          ws.send(
            JSON.stringify({
              event: WS_EVENT.check_device,
              data: params.verify_id,
            })
          )
        } else {
        }
      })
    }
  }
  async appSendMessage(params: IAppSendMessage) {
    const clients = this.clientsUserId.get(params.user_id) || []
    if (clients.length > 0) {
      clients.forEach(function each(ws) {
        if (ws.user_id === params.user_id) {
          ws.send(
            JSON.stringify({
              event: params.event,
              data: params.data,
            })
          )
        }
      })
    }
  }
  async handleStreamBotTrading(params: IAppSendMessage) {
    const clients = this.clientsUserId.get(params.user_id) || []
    if (clients.length > 0) {
      clients.forEach(function each(ws) {
        ws.send(
          JSON.stringify({
            event: params.event,
            data: params.data,
          })
        )
      })
    }
  }
  async handleStreamBotStrategyTrading(params: IAppSendMessage) {
    for (let ws of this.clientsBotTrading.values()) {
      ws.send(
        JSON.stringify({
          event: params.event,
          data: params.data,
        })
      )
    }
  }
  async handleVerify(client: any, token: string): Promise<IVerify> {
    try {
      const decode = await verifyToken(token, APP_CONFIG.TOKEN_PUBLIC_KEY)
      await this.sessionService.verifySession(decode)
      client.send(
        JSON.stringify({
          event: WS_EVENT.verify,
          data: client.verify_id,
        })
      )
      client.user_id = decode.user_id
      client.token_id = decode.token_id
      this.logger.log(`id: ${client.id} user: ${client.user_id} Connection`)

      const userBots = await this.userRepository.findUserBotSQL({
        user_id: decode.user_id,
        status: ITEM_STATUS.ACTIVE,
        expires_at: Date.now(),
      })
      const botIds = userBots.map((b) => b.bot_id)
      const [bots, userSettingSignalPlatfrom] = await Promise.all([
        this.botRepository.findByIds(botIds),
        this.settingRepository.findOneAppSetting({
          name: APP_SETTING.SIGNAL_PLATFORM,
          user_id: decode.user_id,
        }),
      ])
      let offBotCodes = []
      if (userSettingSignalPlatfrom && userSettingSignalPlatfrom.value) {
        try {
          const config = JSON.parse(
            userSettingSignalPlatfrom.value
          ) as SettingSignalPlatfrom
          if (config && config.OFF_SIGNALS_BOT) {
            offBotCodes = config.OFF_SIGNALS_BOT.toString().split(',')
          }
        } catch (error) {}
      }
      const botCodes = []
      bots.forEach((b) => {
        if (b.code && !offBotCodes.includes(b.code)) {
          const userClientBots = this.clientsBot.get(b.code) || []
          botCodes.push(b.code)
          if (!userClientBots.some((ucb) => ucb.id === client.id)) {
            userClientBots.push(client)
            this.clientsBot.set(b.code, userClientBots)
            this.logger.log(`clientsBot size: ${this.clientsBot.size}`)
          }
        }
      })
      client.bot_codes = botCodes
      const userClients = this.clientsUserId.get(decode.user_id) || []
      if (!userClients.some((ucl) => ucl.id === client.id)) {
        userClients.push(client)
        this.clientsUserId.set(decode.user_id, userClients)
        this.logger.log(`clientsUserId size: ${this.clientsUserId.size}`)

        const session = await this.userRepository.findLastSession({
          token_id: decode.token_id,
          enabled: true,
        })
        const dataSession = {
          user_id: session.user_id,
          token: session.token,
          name_device: session.name_device,
          browser: session.browser,
          ip_number: session.ip_number,
          last_login: session.last_login,
          expires_at: session.expires_at,
          enabled: true,
          websocket_id: client.id,
          token_id: session.token_id,
        }
        await this.sessionService.save(dataSession)
        const encodeData = dataSession.token.split('.')[1]
        let limitTab = 1
        if (encodeData) {
          const decode: IToken = JSON.parse(decodeBase64(encodeData))
          limitTab = await this.generalSettingRoleService.getMaxLimitTab(
            decode.roles
          )
        }
        const sessions = await this.userRepository.findSessionWS(
          { token_id: decode.token_id, enabled: true },
          limitTab
        )
        const activeClientIds = sessions
          .map((s) => s.websocket_id)
          .filter((id) => id)
        await this.forceLogoutClient(
          {
            active_client_ids: activeClientIds,
            user_id: decode.user_id,
            token_id: decode.token_id,
            verify_id: client.verify_id,
            client_id: client.id,
          },
          this.wss
        )
        return {
          active_client_ids: activeClientIds,
          user_id: client.user_id,
          token_id: client.token_id,
          verify_id: client.verify_id,
          client_id: client.id,
        }
      }
      return null
    } catch (error) {
      console.log('error', error)
      this.logger.log(
        `id: ${client.id} user: ${client.user_id} Session invalid`
      )
      client.send(
        JSON.stringify({
          event: WS_EVENT.session_invalid,
        })
      )
      return null
    }
  }
  async handleDiconnect(client: any) {
    if (client.bot_codes && client.bot_codes.length > 0) {
      client.bot_codes.forEach((b) => {
        const userClientBots = this.clientsBot.get(b.code) || []
        if (userClientBots.length > 0) {
          const newUserBots = userClientBots.filter(
            (ucb) => ucb.id !== client.id
          )
          this.clientsBot.set(client.user_id, newUserBots)
        }
      })
    }
    if (client.user_id) {
      const userClients = this.clientsUserId.get(client.user_id) || []
      if (userClients.length > 0) {
        const newUserClients = userClients.filter((ucl) => ucl.id !== client.id)
        this.clientsUserId.set(client.user_id, newUserClients)
        const session = await this.userRepository.findLastSession({
          websocket_id: client.id,
          enabled: true,
        })
        if (session) {
          await this.userRepository.saveSession({
            id: session.id,
            enabled: false,
          })
        }
      }
    }
  }
  async handleBotSignal(params: IBotSignalMessage) {
    try {
      const userBots = this.clientsBot.get(params.name) || []
      try {
        await this.botSignalService.saveSignal(params)
      } catch (error) {}
      if (userBots.length > 0) {
        userBots.forEach((client) => {
          client.send(
            JSON.stringify({
              event: WS_EVENT.bot_signal,
              data: {
                exchange: params.exchange,
                symbol: params.symbol,
                name: params.name,
                resolution: params.resolution,
                signal_id: params.signal_id,
                type: params.type,
                time: params.time,
                image_url: params.image_url,
              },
            })
          )
        })
      }
    } catch (error) {
      console.log('handleBotSignal error', error)
    }
  }
  heartbeat() {
    this.isAlive = true
  }
  init() {
    const _this = this
    this.wss.on('connection', (ws, req) => {
      _this.logger.log(`Connection size: ${this.wss.clients.size}`)
      ws.id = v4() // client_id
      ws.isAlive = true
      ws.on('pong', this.heartbeat) // Keep alive the connection
      ws.on('message', async (data, isBinary) => {
        try {
          console.log('isBinary', isBinary)
          const message = JSON.parse(data)
          _this.logger.log(`${ws.id} Event: ${message.event}`)
          // this.webSocketEvent.request(ws, message)
          switch (message.event) {
            case WS_EVENT.verify:
              ws.verify_id = v4()
              const user = await _this.handleVerify(ws, message.data)
              if (user) {
                _this.publisher.publish(
                  CHANELS.WS_CHANEL,
                  JSON.stringify({ event: WS_EVENT.verify, data: user })
                )
              }
              break
            case WS_EVENT.check_device:
              if (ws.verify_id) {
                _this.checkBrowser(
                  {
                    client_id: ws.id,
                    user_id: ws.user_id,
                    token_id: ws.token_id,
                    verify_id: ws.verify_id,
                  },
                  _this.wss
                )
                _this.publisher.publish(
                  CHANELS.WS_CHANEL,
                  JSON.stringify({
                    event: WS_EVENT.check_device,
                    data: {
                      client_id: ws.id,
                      user_id: ws.user_id,
                      token_id: ws.token_id,
                      verify_id: ws.verify_id,
                    },
                  })
                )
              }
              break
            case WS_EVENT.tbot_init:
              _this.logger.log(`${ws.id} verify_id: ${ws.verify_id}`)
              if (_this.publisher) {
                _this.logger.log(`${ws.id} publisher`)
              }
              if (ws.verify_id) {
                ws.tbot_init = true
                _this.publisher.publish(
                  CHANELS.TBOT_CHANEL_UR,
                  JSON.stringify({
                    event: TBOT_CHANEL_TS_EVENT.connect_stream,
                    data: { user_id: ws.user_id },
                  })
                )
              }
              break
            case WS_EVENT.tbot_close:
              if (ws.verify_id) {
                ws.tbot_init = false
                _this.publisher.publish(
                  CHANELS.TBOT_CHANEL_UR,
                  JSON.stringify({
                    event: TBOT_CHANEL_TS_EVENT.disconnect_stream,
                    data: { user_id: ws.user_id },
                  })
                )
              }
              break
            case WS_EVENT.tbot_strategy_init:
              ws.tbot_strategy_init = true
              this.clientsBotTrading.set(ws.id, ws)
              break
            case WS_EVENT.tbot_strategy_close:
              ws.tbot_strategy_init = false
              this.clientsBotTrading.delete(ws.id)
              break
            default:
              break
          }
        } catch (error) {
          console.log('error', error)
        }
      })

      // handling what to do when clients disconnects from server
      ws.on('close', async (e) => {
        this.logger.log(`id: ${ws.id} user: ${ws.user_id} Disconnect`)
        await this.handleDiconnect(ws)
      })
      // handling client connection error
      ws.onerror = function () {
        console.log('Some Error occurred')
      }
    })
    // const _this = this
    const interval = setInterval(function ping() {
      _this.wss.clients.forEach(function each(ws) {
        if (ws.isAlive === false) return ws.terminate()
        ws.isAlive = false
        ws.ping()
      })
    }, 30000)
    this.wss.on('close', function close() {
      clearInterval(interval)
    })
    this.subscriber.on('message', async (chanel, data) => {
      this.logger.log(`Channel: ${chanel} data: ${data}`)
      if (chanel === CHANELS.WS_CHANEL) {
        try {
          const message: IDataSubscriber = JSON.parse(data)
          if (message.event === WS_EVENT.verify) {
            this.forceLogoutClient(message.data, this.wss)
          }
          if (message.event === WS_EVENT.check_device) {
            this.checkBrowser(message.data, this.wss)
          }
          if (message.event === WS_EVENT.app_message) {
            this.appSendMessage({
              event: message.data.sub_event,
              user_id: message.data.user_id,
              data: message.data.sub_data,
            })
          }
        } catch (error) {
          console.log('WS_CHANEL error', error)
        }
      }
      if (chanel === CHANELS.SIGNAL_BOT_CHANEL) {
        try {
          const message: any = JSON.parse(data)
          this.handleBotSignal(message)
        } catch (error) {
          console.log('SIGNAL_BOT_CHANEL error', error)
        }
      }
      if (chanel === CHANELS.TBOT_CHANEL_TS) {
        try {
          const message = JSON.parse(data)
          if (message.event === TBOT_CHANEL_TS_EVENT.account) {
            const {
              user_bot_id,
              user_id,
              bot_id,
              subscriber_id,
              balance,
              time,
              status,
              trade,
              stopout,
              error,
            } = message.data
            const userBot = await this.userGetBotTradingHandler.execute(
              bot_id,
              user_id
            )
            if (userBot) {
              const events = await this.eventStoreRepository.find({
                user_id,
                event_name: APP_SETTING.TBOT_INACTIVE_BY_SYSTEM,
                state: EVENT_STORE_STATE.OPEN,
              })
              let inactiveBySystemAt = 0
              let countInactive = 0
              events.forEach((e) => {
                if (e.metadata?.bot_id === bot_id) {
                  countInactive += 1
                  if (inactiveBySystemAt < e.created_at) {
                    inactiveBySystemAt = e.created_at
                  }
                }
              })
              this.handleStreamBotTrading({
                event: WS_EVENT.tbot_update,
                user_id,
                data: {
                  user_bot_id,
                  id: bot_id,
                  balance_init: userBot.balance_init,
                  pnl_day_init: userBot.pnl_day_init,
                  pnl_day_current: userBot.pnl_day_current,
                  pnl_7_day_current: userBot.pnl_7_day_current,
                  pnl_7_day_init: userBot.pnl_7_day_init,
                  pnl_30_day_current: userBot.pnl_30_day_current,
                  pnl_30_day_init: userBot.pnl_30_day_init,
                  total_trade: userBot.total_trade,
                  balance_current: userBot.balance_current,
                  user_status: status,
                  balance: userBot.balance,
                  count_inactive_by_system: countInactive,
                  inactive_by_system_at: inactiveBySystemAt,
                  error,
                },
              })
            }

            if (status === ITEM_STATUS.INACTIVE_BY_SYSTEM) {
              if (!trade) {
                return
              }
              this.deactivateUserBotTradingHandler.execute(
                user_bot_id,
                user_id,
                bot_id,
                trade
              )
            }

            if (status === ITEM_STATUS.STOP_OUT) {
              if (!stopout) {
                return
              }
              try {
                const sendEmailProcessing =
                  await this.eventStoreRepository.save({
                    event_id: `${stopout.time}`,
                    user_id,
                    event_name: EVENT_STORE_NAME.TBOT_STOP_OUT,
                    state: EVENT_STORE_STATE.PROCESSING,
                  })
                try {
                  await this.botTradingService.emailBotStatus({
                    user_id,
                    bot_id,
                    status: ITEM_STATUS.STOP_OUT,
                    stopout,
                  })
                  try {
                    await this.eventStoreRepository.save({
                      event_id: `${stopout.time}`,
                      user_id,
                      event_name: EVENT_STORE_NAME.TBOT_STOP_OUT,
                      state: EVENT_STORE_STATE.CLOSED,
                    })
                  } catch (error) {
                    console.log('error', error)
                  }
                } catch (error) {
                  console.log('error', error)
                  await this.eventStoreRepository.deleteById(
                    sendEmailProcessing.id
                  )
                }
              } catch (error) {
                console.log('error', error)
              }
            }
          }
          if (message.event === TBOT_CHANEL_TS_EVENT.trades) {
            const { user_bot_id, user_id, bot_id, subscriber_id, trades } =
              message.data
            const userBot = await this.userGetBotTradingHandler.execute(
              bot_id,
              user_id
            )
            if (userBot) {
              const events = await this.eventStoreRepository.find({
                user_id,
                event_name: APP_SETTING.TBOT_INACTIVE_BY_SYSTEM,
                state: EVENT_STORE_STATE.OPEN,
              })
              let inactiveBySystemAt = 0
              let countInactive = 0
              events.forEach((e) => {
                if (e.metadata?.bot_id === bot_id) {
                  countInactive += 1
                  if (inactiveBySystemAt < e.created_at) {
                    inactiveBySystemAt = e.created_at
                  }
                }
              })
              this.handleStreamBotTrading({
                event: WS_EVENT.tbot_update,
                user_id,
                data: {
                  user_bot_id,
                  id: bot_id,
                  balance_init: userBot.balance_init,
                  pnl_day_init: userBot.pnl_day_init,
                  pnl_day_current: userBot.pnl_day_current,
                  pnl_7_day_current: userBot.pnl_7_day_current,
                  pnl_7_day_init: userBot.pnl_7_day_init,
                  pnl_30_day_current: userBot.pnl_30_day_current,
                  pnl_30_day_init: userBot.pnl_30_day_init,
                  total_trade: userBot.total_trade,
                  balance_current: userBot.balance_current,
                  balance: userBot.balance,
                  count_inactive_by_system: countInactive,
                  inactive_by_system_at: inactiveBySystemAt,
                },
              })
            }

            if (trades && trades.length > 0) {
              this.handleStreamBotTrading({
                event: WS_EVENT.tbot_trade_update,
                user_id,
                data: {
                  id: user_bot_id,
                  bot_id,
                  trades,
                },
              })
              this.handleStreamBotTrading({
                event: WS_EVENT.tbot_chart_pnl_update,
                user_id,
                data: {
                  bot_id,
                },
              })
            }
          }
          if (message.event === TBOT_CHANEL_TS_EVENT.strategy_trades) {
            const { bot_id, trades } = message.data
            if (trades && trades.length > 0) {
              this.handleStreamBotStrategyTrading({
                event: WS_EVENT.tbot_strategy_trade_update,
                data: {
                  bot_id,
                  trades,
                },
              })
              this.handleStreamBotStrategyTrading({
                event: WS_EVENT.tbot_strategy_chart_pnl_update,
                data: {
                  bot_id,
                },
              })
            }
          }
        } catch (error) {
          console.log('TBOT_CHANEL_TS error', error)
        }
      }
    })
    this.subscriber.subscribe(CHANELS.WS_CHANEL)
    this.subscriber.subscribe(CHANELS.SIGNAL_BOT_CHANEL)
    this.subscriber.subscribe(CHANELS.TBOT_CHANEL_TS)
  }
}
