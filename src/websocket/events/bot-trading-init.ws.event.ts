import { Inject } from '@nestjs/common'
import { randomInt } from 'crypto'
import { WS_EVENT } from 'src/const'
import { TRADE_HISTORY_STATUS, TRADE_SIDE } from 'src/const/bot'
import { UserListBotTradingPlanHandler } from 'src/usecases/bot/user-list-bot-trading-plan'
import { WebSocketMessage } from '../types'
import { v4 } from 'uuid'
import { ITEM_STATUS } from 'src/const/transaction'
export class BotTradingInitEvent {
  constructor(
    @Inject(UserListBotTradingPlanHandler)
    private userListBotTradingPlanHandler: UserListBotTradingPlanHandler
  ) {}
  async execute(param: {
    user_id: string
    status: string
    cb: (data: WebSocketMessage, options?: any) => void
  }) {
    const { user_id, status, cb } = param
    if (user_id) {
      let userBot = await this.userListBotTradingPlanHandler.execute(
        user_id,
        status
      )
      // connect to all bot
      userBot.map((e) => {
        if (e.user_status === ITEM_STATUS.ACTIVE) {
          setInterval(() => {
            const rate = randomInt(2) === 1 ? 1 : -1
            cb({
              event: WS_EVENT.tbot_update,
              data: {
                id: e.id,
                pnl_day_init: 100,
                pnl_day_current: randomInt(10000),
                pnl_7_day_init: 500,
                pnl_7_day_current: randomInt(10000),
                pnl_30_day_init: 1000,
                pnl_30_day_current: randomInt(10000),
                total_trade: randomInt(10000),
                profit: randomInt(1000) * rate,
                gain: randomInt(100) * rate,
                balance_init: 2000,
                balance_current: randomInt(10000),
              },
            })
            cb({
              event: WS_EVENT.tbot_trade_update,
              data: {
                id: v4(),
                user_id: user_id,
                bot_id: e.id,
                token_first: 'EURUSD',
                status: TRADE_HISTORY_STATUS.OPEN,
                side: rate > 0 ? TRADE_SIDE.LONG : TRADE_SIDE.SHORT,
                entry_price: randomInt(1000),
                profit: randomInt(100),
                day_started: Date.now(),
                created_at: Date.now(),
                updated_at: Date.now(),
              },
            })
            const pnl_day_current = randomInt(10000)
            cb({
              event: WS_EVENT.tbot_chart_pnl_update,
              data: {
                bot_id: e.id,
                pnl_day_init: 1000,
                pnl_day_current,
                pnl_day_percent: ((pnl_day_current - 1000) / 1000) * 100,
                created_at: Date.now(),
                updated_at: Date.now(),
              },
            })
          }, 1000)
        }
      })
    }
  }
}
