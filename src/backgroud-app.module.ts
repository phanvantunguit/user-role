import { Module } from '@nestjs/common'
import { EventEmitterModule } from '@nestjs/event-emitter'
import { SchedulerRegistry } from '@nestjs/schedule'
import { BackgroudJob } from './backgroud-jobs'
import { BotTradingJob } from './backgroud-jobs/bot-trading.job'
import { TransactionJob } from './backgroud-jobs/transaction.job'
import { TransactionEvent } from './events/transaction.event'
import { DatabaseModule } from './repositories'
import { CoinpaymentResource } from './resources/coinpayment'
import { CoinpaymentTransport } from './resources/coinpayment/coinpayment.transport'
import { MetaapiResource } from './resources/forex'
import { MailResource } from './resources/mail'
import { SendGridTransport } from './resources/mail/sendgrid.transport'
import { BotTradingService } from './services/bot-trading'
import { BotTradingHistoryService } from './services/bot-trading-history'
import { PaymentService } from './services/payment'
import { CoinpaymentService } from './services/payment/coinpayment'
import { TransactionService } from './services/transaction'
import { TransactionLogService } from './services/transaction-log'
@Module({
  imports: [DatabaseModule, EventEmitterModule.forRoot()],
  providers: [
    BackgroudJob,
    TransactionJob,
    SchedulerRegistry,
    TransactionService,
    PaymentService,
    TransactionLogService,
    CoinpaymentService,
    CoinpaymentResource,
    CoinpaymentTransport,

    TransactionEvent,
    MailResource,
    SendGridTransport,
    BotTradingJob,
    MetaapiResource,
    BotTradingService,
    BotTradingHistoryService,
  ],
})
export class BackgroudAppModule {}
