import { Injectable } from '@nestjs/common'
import { ElasticsearchService } from '@nestjs/elasticsearch'
import { cleanObject } from 'src/utils/format-data.util'

@Injectable()
export class ElasticSearchRepository {
  constructor(private readonly elasticsearchService: ElasticsearchService) {}
  async createDocument(index: string, id: string, data: any): Promise<string> {
    const result = await this.elasticsearchService.create({
      id,
      index,
      body: data,
    })
    return result._id
  }
  async updateDocument(index: string, id: string, data: any) {
    const result = await this.elasticsearchService.update({
      id,
      index,
      body: {
        doc: data,
      },
    })
    return result
  }
  async deleteDocument(index: string, id: string) {
    const result = await this.elasticsearchService.delete({
      id,
      index,
    })
    return result
  }
  async deleteQueryDocument(index: string, query?: any) {
    const clearQuery = cleanObject(query)
    const must = Object.keys(clearQuery).map((key) => {
      const match = {}
      match[key] = clearQuery[key]
      return { match }
    })
    const result = await this.elasticsearchService.deleteByQuery({
      index,
      query: {
        bool: {
          must: must,
        },
      },
    })
    return result
  }
  async getDocument(
    index: string,
    id?: string,
    query?: any,
    limit?: number,
    offset?: number
  ) {
    if (!limit) limit = 50
    if (!offset) offset = 0
    if (id) {
      const result = await this.elasticsearchService.search({
        index,
        from: offset,
        size: limit,
        query: {
          ids: {
            values: id,
          },
        },
      })
      return result.hits.hits
    } else if (query) {
      const clearQuery = cleanObject(query)
      const must = Object.keys(clearQuery).map((key) => {
        const match = {}
        match[key] = clearQuery[key]
        return { match }
      })
      const result = await this.elasticsearchService.search({
        index,
        from: offset,
        size: limit,
        query: {
          bool: {
            must: must,
          },
        },
      })
      return result.hits.hits
    }
    const result = await this.elasticsearchService.search({
      index,
    })
    return result.hits.hits
  }

  async isExist(index: string, id: string) {
    const result = await this.elasticsearchService.exists({
      id,
      index,
    })
    return result
  }
}
