import { ConnectionName } from 'src/const/app-setting'
import {
  QueryTransaction,
  QueryTransactionPagination,
  RawTransaction,
} from 'src/domains/transaction/transaction.types'
import { Repository, getRepository, QueryRunner } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import {
  TRANSACTIONS_VIEW,
  TransactionViewEntity,
} from './transaction-view.entity'
import { TransactionEntity, TRANSACTIONS_TABLE } from './transaction.entity'

export class TransactionRepository extends BaseRepository<
  QueryTransaction,
  RawTransaction
> {
  repo(): Repository<TransactionEntity> {
    return getRepository(TransactionEntity, ConnectionName.user_role)
  }
  transactionViewRepo() {
    return getRepository(TransactionViewEntity, ConnectionName.user_role)
  }
  async save(params: RawTransaction, queryRunner?: QueryRunner) {
    if (queryRunner) {
      const transaction = await this.repo().create(params)
      return queryRunner.manager.save(transaction)
    } else {
      return this.repo().save(params)
    }
  }

  async viewfindOne(params: QueryTransaction): Promise<TransactionViewEntity> {
    return this.transactionViewRepo().findOne(params)
  }

  async viewFind(params: QueryTransaction): Promise<TransactionViewEntity[]> {
    return this.transactionViewRepo().find({
      where: params,
      order: { created_at: 'DESC' },
    })
  }
  async viewFindByUserMultipleStatus(
    user_id: string,
    status: string[]
  ): Promise<TransactionViewEntity[]> {
    const whereArray = []
    if (status.length > 0) {
      whereArray.push(`status in (${status.map((e) => `'${e}'`)})`)
    }
    whereArray.push(`user_id = '${user_id}'`)
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
      SELECT *
      FROM ${TRANSACTIONS_VIEW}  
      ${where} 
      order by created_at DESC
    `
    return this.transactionViewRepo().query(query)
  }
  async viewFindTransactionPaging(params: QueryTransactionPagination) {
    let { page, size } = params
    const { keyword, status, from, to } = params
    page = Number(page || 1)
    size = Number(size || 50)
    const whereArray = []
    // from = timeUtils.setBeginDate(from)
    // to = timeUtils.setEndDate(to)
    whereArray.push(`created_at >= ${from}`)
    whereArray.push(`created_at <= ${to}`)
    if (status) {
      whereArray.push(`status = '${status}'`)
    }
    if (keyword) {
      whereArray.push(`(email LIKE '%${keyword}%'
        OR username LIKE '%${keyword}%'
        OR phone LIKE '%${keyword}%'
        OR first_name LIKE '%${keyword}%'
        OR last_name LIKE '%${keyword}%'
        OR payment_id LIKE '%${keyword}%')`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const queryTransaction = `
        SELECT  *
        FROM ${TRANSACTIONS_VIEW}  
        ${where}  
        ORDER BY created_at DESC
        OFFSET ${(page - 1) * size}
        LIMIT ${size}
    `
    const queryCount = `
    SELECT COUNT(*)
    FROM ${TRANSACTIONS_VIEW}  
    ${where}
    `
    const [rows, total] = await Promise.all([
      this.transactionViewRepo().query(queryTransaction),
      this.transactionViewRepo().query(queryCount),
    ])
    return {
      rows,
      page,
      size,
      count: rows.length,
      total: Number(total[0].count),
    }
  }
  async findMultipleStatus(params: {
    status: string[]
  }): Promise<TransactionEntity[]> {
    const { status } = params
    const whereArray = []
    if (status.length > 0) {
      whereArray.push(`status in (${status.map((e) => `'${e}'`)})`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
      SELECT *
      FROM ${TRANSACTIONS_TABLE}  
      ${where} 
      order by created_at DESC
    `
    return this.repo().query(query)
  }
}
