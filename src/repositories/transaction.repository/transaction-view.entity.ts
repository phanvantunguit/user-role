import { PAYMENT_METHOD, TRANSACTION_STATUS } from 'src/const/transaction'
import { RawTransactionDetailView } from 'src/domains/transaction'
import { ViewEntity, ViewColumn } from 'typeorm'
export const TRANSACTIONS_VIEW = 'transactions_view'
@ViewEntity(TRANSACTIONS_VIEW, { expression: '', synchronize: false })
export class TransactionViewEntity {
  @ViewColumn()
  id: string

  @ViewColumn()
  user_id: string

  @ViewColumn()
  payment_id: string

  @ViewColumn()
  payment_method: PAYMENT_METHOD

  @ViewColumn()
  description: string

  @ViewColumn()
  status: TRANSACTION_STATUS

  @ViewColumn()
  sell_amount: number

  @ViewColumn()
  sell_currency: string

  @ViewColumn()
  buy_currency: string

  @ViewColumn()
  email: string

  @ViewColumn()
  username: string

  @ViewColumn()
  phone: string

  @ViewColumn()
  first_name: string

  @ViewColumn()
  last_name: string

  @ViewColumn()
  buy_amount: string

  @ViewColumn()
  wallet_address: string

  @ViewColumn()
  details: RawTransactionDetailView[]

  @ViewColumn()
  created_at: number

  @ViewColumn()
  updated_at: number
}
