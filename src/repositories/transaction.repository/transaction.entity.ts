import { PAYMENT_METHOD, TRANSACTION_STATUS } from 'src/const/transaction'
import {
  Entity,
  Column,
  Index,
  PrimaryColumn,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  UpdateEvent,
} from 'typeorm'

export const TRANSACTIONS_TABLE = 'transactions'
@Entity(TRANSACTIONS_TABLE)
export class TransactionEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string

  @Column({ type: 'uuid' })
  @Index()
  user_id: string

  @Column({ type: 'varchar', nullable: true })
  @Index()
  payment_id: string

  @Column({ type: 'varchar' })
  payment_method: PAYMENT_METHOD

  @Column({ type: 'varchar', nullable: true })
  description: string

  @Column({ type: 'varchar' })
  status: TRANSACTION_STATUS

  @Column({ type: 'float4' })
  sell_amount: number

  @Column({ type: 'varchar' })
  sell_currency: string

  @Column({ type: 'varchar' })
  buy_currency: string

  @Column({ type: 'varchar', nullable: true })
  buy_amount: string

  @Column({ type: 'uuid', nullable: true })
  parent_id: string

  @Column({ type: 'varchar', nullable: true })
  wallet_address: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number
}
@EventSubscriber()
export class TransactionEntitySubscriber
  implements EntitySubscriberInterface<TransactionEntity>
{
  async beforeInsert(event: InsertEvent<TransactionEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }
  async beforeUpdate(event: UpdateEvent<TransactionEntity>) {
    event.entity.updated_at = Date.now()
  }
}
