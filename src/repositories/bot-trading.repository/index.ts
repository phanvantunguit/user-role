import { ConnectionName } from 'src/const/app-setting'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import { BotTradingEntity, BOT_TRADING_TABLE } from './bot-trading.entity'

export class BotTradingRepository extends BaseRepository<
  BotTradingEntity,
  BotTradingEntity
> {
  repo(): Repository<BotTradingEntity> {
    return getRepository(BotTradingEntity, ConnectionName.user_role)
  }
  listConditionMultiple(params: {
    status?: string[]
    id?: string[]
  }): Promise<BotTradingEntity[]> {
    const { status, id } = params
    const whereArray = []
    if (status.length > 0) {
      whereArray.push(`bt.status in (${status.map((e) => `'${e}'`)})`)
    }
    if (id && id.length > 0) {
      whereArray.push(`bt.id in (${id.map((e) => `'${e}'`)})`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
      SELECT bt.*, count(ual.id) real_bought
      FROM ${BOT_TRADING_TABLE} bt
      Left Join (
        select * from user_asset_logs ual
          where ual.status in ('ACTIVE', 'NOT_CONNECT')
        ) ual on ual.asset_id = bt.id
      ${where} 
      group by bt.id
      order by "order" ASC
    `
    return this.repo().query(query)
  }
}
