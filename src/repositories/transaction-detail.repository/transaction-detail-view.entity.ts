import { ViewColumn, ViewEntity } from 'typeorm'

export const TRANSACTION_DETAILS_VIEW = 'transaction_details_view'
@ViewEntity(TRANSACTION_DETAILS_VIEW, { expression: '', synchronize: false })
export class TransactionDetailViewEntity {
  @ViewColumn()
  id: string

  @ViewColumn()
  user_id: string

  @ViewColumn()
  transaction_id: string

  @ViewColumn()
  role_id: string

  @ViewColumn()
  role_name: string

  @ViewColumn()
  price: number

  @ViewColumn()
  currency: string

  @ViewColumn()
  package_id: string

  @ViewColumn()
  package_type: string

  @ViewColumn()
  package_name: string

  @ViewColumn()
  discount_rate: number

  @ViewColumn()
  discount_amount: number

  @ViewColumn()
  quantity: number

  @ViewColumn()
  expires_at: number

  @ViewColumn()
  created_at: number
}
