import {
  Entity,
  Column,
  Index,
  PrimaryColumn,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
} from 'typeorm'

export const TRANSACTION_DETAILS_TABLE = 'transaction_details'
@Entity(TRANSACTION_DETAILS_TABLE)
export class TransactionDetailEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string

  @Column({ type: 'uuid' })
  @Index()
  user_id: string

  @Column({ type: 'uuid' })
  @Index()
  transaction_id: string

  @Column({ type: 'uuid' })
  role_id: string

  @Column({ type: 'float4' })
  price: number

  @Column({ type: 'varchar' })
  currency: string

  @Column({ type: 'uuid' })
  package_id: string

  @Column({ type: 'varchar' })
  package_type: string

  @Column({ type: 'varchar' })
  package_name: string

  @Column({ type: 'float4' })
  discount_rate: number

  @Column({ type: 'float4' })
  discount_amount: number

  @Column({ type: 'float4' })
  quantity: number

  @Column({ type: 'varchar', nullable: true })
  expires_at: number

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number
}
@EventSubscriber()
export class TransactionDetailEntitySubscriber
  implements EntitySubscriberInterface<TransactionDetailEntity>
{
  async beforeInsert(event: InsertEvent<TransactionDetailEntity>) {
    event.entity.created_at = Date.now()
  }
}
