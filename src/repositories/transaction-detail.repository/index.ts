import { ConnectionName } from 'src/const/app-setting'
import {
  QueryTransactionDetail,
  RawTransactionDetail,
} from 'src/domains/transaction'
import { Repository, getRepository, QueryRunner } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import { TransactionDetailViewEntity } from './transaction-detail-view.entity'
import { TransactionDetailEntity } from './transaction-detail.entity'

export class TransactionDetailRepository extends BaseRepository<
  QueryTransactionDetail,
  RawTransactionDetail
> {
  repo(): Repository<TransactionDetailEntity> {
    return getRepository(TransactionDetailEntity, ConnectionName.user_role)
  }

  transactionDetailViewRepo() {
    return getRepository(TransactionDetailViewEntity, ConnectionName.user_role)
  }
  async save(params: RawTransactionDetail, queryRunner?: QueryRunner) {
    if (queryRunner) {
      const transactionDetail = await this.repo().create(params)
      return queryRunner.manager.save(transactionDetail)
    } else {
      return this.repo().save(params)
    }
  }
  async findOne(
    params: QueryTransactionDetail
  ): Promise<TransactionDetailViewEntity> {
    return this.transactionDetailViewRepo().findOne(params)
  }
  async find(
    params: QueryTransactionDetail
  ): Promise<TransactionDetailViewEntity[]> {
    return this.transactionDetailViewRepo().find({
      where: params,
      order: { created_at: 'DESC' },
    })
  }
}
