import { ConnectionName } from 'src/const/app-setting'
import { QueryBot, RawBot } from 'src/domains/bot/bot.types'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import { BotEntity, BOT_TABLE } from './bot.entity'

export class BotRepository extends BaseRepository<QueryBot, RawBot> {
  repo(): Repository<BotEntity> {
    return getRepository(BotEntity, ConnectionName.user_role)
  }
  listConditionMultiple(params: { status?: string[] }): Promise<RawBot[]> {
    const { status } = params
    const whereArray = []
    if (status.length > 0) {
      whereArray.push(`status in (${status.map((e) => `'${e}'`)})`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
      SELECT *
      FROM ${BOT_TABLE}  
      ${where} 
      order by "order" ASC
    `
    return this.repo().query(query)
  }
}
