import { ConnectionName } from 'src/const/app-setting'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import {
  AccountBalanceEntity,
  ACCOUNT_BALANCES_TABLE,
} from './account-balance.entity'

export class AccountBalanceRepository extends BaseRepository<
  AccountBalanceEntity,
  AccountBalanceEntity
> {
  repo(): Repository<AccountBalanceEntity> {
    return getRepository(AccountBalanceEntity, ConnectionName.user_role)
  }
  async chartPNL(param: { bot_id: string; user_id: string }) {
    const { user_id, bot_id } = param
    const queryChart = `
    select * 
    FROM account_balances ab
    where created_at = (SELECT MIN(created_at) FROM account_balances where "date" = ab."date")
    and bot_id = '${bot_id}' and user_id = '${user_id}'
    ORDER BY created_at ASC
    `
    return this.repo().query(queryChart)
  }
  async getBalance(param: {
    bot_id: string
    user_id: string
    from?: number
    order?: 'desc' | 'asc'
  }) {
    let { order } = param
    order = order || 'asc'
    const { user_id, bot_id, from } = param
    const whereArray = []
    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`)
    } else {
      whereArray.push(`user_id is null`)
    }
    whereArray.push(`bot_id = '${bot_id}'`)
    if (from) {
      whereArray.push(`created_at >= ${from}`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const queryChart = `
    select * from account_balances
    ${where}
    order by created_at ${order}
    limit 1
    `
    return this.repo().query(queryChart)
  }
  async getBalanceByUser(param: {
    bot_id: string
    user_id?: string
    type?: 'cashout' | 'cashin'
    to?: number
  }) {
    const { user_id, bot_id, to, type } = param
    const whereArray = []
    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`)
    } else {
      whereArray.push(`user_id is null`)
    }
    whereArray.push(`bot_id = '${bot_id}'`)
    whereArray.push('change_by_user = true')
    if (to) {
      whereArray.push(`created_at <= ${to}`)
    }
    let condition
    if (type) {
      if (type === 'cashin') {
        condition = '>'
      } else {
        condition = '<'
      }
    }

    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''

    if (condition) {
      const queryChart = `
      select SUM(case when balance ${condition} 0 then balance else 0 end) as total
      from account_balances
      ${where}
      `
      return this.repo().query(queryChart)
    } else {
      const queryChart = `
      select SUM(balance) as total
      from account_balances
      ${where}
      `
      return this.repo().query(queryChart)
    }
  }

  getAllOfAccounts(param: { user_id?: string }): Promise<{ user_id: string; broker_account: string }[]>  {
    const { user_id } = param
    const whereArray = []
    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
    SELECT user_id, broker_account 
    FROM ${ACCOUNT_BALANCES_TABLE} 
    ${where}
    group by user_id, broker_account
    `
    return this.repo().query(query)
  }

  deleteAccountBalance(param: {
    user_id: string, bot_id: string
  }) {
    const { user_id, bot_id } = param
    const query = `
    DELETE
    FROM ${ACCOUNT_BALANCES_TABLE}  
    WHERE bot_id = '${bot_id}' and user_id = '${user_id}' and change_id = 'init'
  `
    return this.repo().query(query)
  }
}
