import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent,
  Index,
  Unique,
} from 'typeorm'
import { format } from 'date-fns'
import { v4 } from 'uuid'

export const ACCOUNT_BALANCES_TABLE = 'account_balances'
@Entity(ACCOUNT_BALANCES_TABLE)
@Unique('user_id_bot_id_change_id_ab_un', ['user_id', 'bot_id', 'change_id'])
export class AccountBalanceEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string

  @Column({ type: 'uuid', nullable: true })
  @Index()
  user_id?: string

  @Column({ type: 'uuid' })
  @Index()
  bot_id?: string

  @Column({ type: 'varchar' })
  broker_account?: string

  @Column({ type: 'float' })
  balance?: number

  @Column({ type: 'boolean', default: false })
  change_by_user?: boolean

  @Column({ type: 'varchar' })
  change_id?: string

  @Column({ type: 'varchar' })
  date?: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number
}

@EventSubscriber()
export class AccountBalanceEntitySubscriber
  implements EntitySubscriberInterface<AccountBalanceEntity>
{
  async beforeInsert(event: InsertEvent<AccountBalanceEntity>) {
    if (!event.entity.created_at) {
      event.entity.created_at = Date.now()
    }
    if (!event.entity.change_id) {
      event.entity.change_id = v4()
    }
    event.entity.date = format(event.entity.created_at, 'ddMMyy')
  }
}
