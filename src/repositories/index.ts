import { CacheModule, CACHE_MANAGER, Inject, Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ElasticsearchModule } from '@nestjs/elasticsearch'
import { APP_CONFIG } from 'src/config'
import {
  AuthRoleEntity,
  AuthRoleEntitySubscriber,
} from './permission.repository/auth-role.entity'
import {
  FeatureRoleEntity,
  FeatureRoleEntitySubscriber,
} from './permission.repository/feature-role.entity'
import {
  GeneralSettingRoleEntity,
  GeneralSettingRoleEntitySubscriber,
} from './permission.repository/general-setting-role.entity'
import {
  RoleEntity,
  RoleEntitySubscriber,
} from './permission.repository/role.entity'
import {
  SymbolSettingRoleEntity,
  SymbolSettingRoleEntitySubscriber,
} from './permission.repository/symbol-setting-role.entity'
import {
  ExchangeEntity,
  ExchangeEntitySubscriber,
} from './setting.repository/exchange.entity'
import {
  FeatureEntity,
  FeatureEntitySubscriber,
} from './setting.repository/feature.entity'
import {
  GeneralSettingEntity,
  GeneralSettingEntitySubscriber,
} from './setting.repository/general-setting.entity'
import {
  ResolutionEntity,
  ResolutionEntitySubscriber,
} from './setting.repository/resolution.entity'
import {
  SymbolEntity,
  SymbolEntitySubscriber,
} from './setting.repository/symbol.entity'
import { UserViewEntity } from './user.repository/user-view.entity'
import {
  AuthUserRoleEntity,
  AuthUserRoleEntitySubscriber,
} from './user.repository/auth_user_role.entity'
import {
  SessionEntity,
  SessionEntitySubscriber,
} from './user.repository/session.entity'
import {
  UserLogEntity,
  UserLogEntitySubscriber,
} from './user.repository/user-log.entity'
import {
  UserRoleEntity,
  UserRoleEntitySubscriber,
} from './user.repository/user-role.entity'
import { UserEntity, UserEntitySubscriber } from './user.repository/user.entity'
import { VerifyTokenEntity } from './user.repository/verify-token.entity'
import { RoleViewEntity } from './permission.repository/role-view.entity'
import {
  AppSettingEntity,
  AppSettingEntitySubscriber,
} from './setting.repository/app-setting.entity'
import {
  TransactionEntity,
  TransactionEntitySubscriber,
} from './transaction.repository/transaction.entity'
import {
  TransactionLogEntity,
  TransactionLogEntitySubscriber,
} from './transaction-log.repository/transaction-log.entity'
import {
  TransactionDetailEntity,
  TransactionDetailEntitySubscriber,
} from './transaction-detail.repository/transaction-detail.entity'
import { TransactionViewEntity } from './transaction.repository/transaction-view.entity'
import { TransactionDetailViewEntity } from './transaction-detail.repository/transaction-detail-view.entity'
import {
  BotSettingEntity,
  BotSettingEntitySubscriber,
} from './bot-setting.repository/bot-setting.entity'
import {
  PackageEntity,
  PackageEntitySubscriber,
} from './package.ropository/package.entity'
import {
  CurrencyEntity,
  CurrencyEntitySubscriber,
} from './currency.repostitory/currency.entity'
import * as redisStore from 'cache-manager-redis-store'
import { CacheRepository } from './cache.repository'
import { CurrencyRepository } from './currency.repostitory'
import { BotSettingRepository } from './bot-setting.repository'
import { PackageRepository } from './package.ropository'
import { PermissionRepository } from './permission.repository'
import { SettingRepository } from './setting.repository'
import { TransactionDetailRepository } from './transaction-detail.repository'
import { TransactionLogRepository } from './transaction-log.repository'
import { UserRepository } from './user.repository'
import { TransactionRepository } from './transaction.repository'
import { BaseRepository } from './base.repository/base.repository'
// import { ElasticSearchRepository } from './elasticsearch.repository'
import { DBContext } from './db-context'
import { BotRepository } from './bot.repository'
import { BotEntity, BotEntitySubscriber } from './bot.repository/bot.entity'
import {
  UserBotEntity,
  UserBotEntitySubscriber,
} from './user.repository/user-bot.entity'
import {
  UserAssetLogEntity,
  UserAssetLogEntitySubscriber,
} from './user.repository/user-asset-log.entity'
import {
  TransactionPSLogEntity,
  TransactionPSLogEntitySubscriber,
} from './transaction-ps.repository/transaction-ps-log.entity'
import {
  TransactionPSMetadataEntity,
  TransactionPSMetadataEntitySubscriber,
} from './transaction-ps.repository/transaction-ps-metadata.entity'
import {
  TransactionPSEntity,
  TransactionPSEntitySubscriber,
} from './transaction-ps.repository/transaction-ps.entity'
import { TransactionPSRepository } from './transaction-ps.repository'
import { ConnectionName } from 'src/const/app-setting'
import { BotSignalRepository } from './bot-signal.repository'
import {
  BotSignalEntity,
  BotSignalEntitySubscriber,
} from './bot-signal.repository/bot-signal.entity'
import {
  FavoriteSignalEntity,
  FavoriteSignalEntitySubscriber,
} from './bot-signal.repository/favorite-signal.entity'
import { MerchantRepository } from './merchant.repository'
import {
  MerchantEntity,
  MerchantEntitySubscriber,
} from './merchant.repository/merchant.entity'
import { BotTradingRepository } from './bot-trading.repository'
import {
  BotTradingEntity,
  BotTradingEntitySubscriber,
} from './bot-trading.repository/bot-trading.entity'
import {
  UserBotTradingEntity,
  UserBotTradingEntitySubscriber,
} from './user.repository/user-bot-trading.entity'
import {
  BotTradingHistoryEntity,
  BotTradingHistoryEntitySubscriber,
} from './bot-trading-history.repository/bot-trading-history.entity'
import { BotTradingHistoryRepository } from './bot-trading-history.repository'
import { AccountBalanceRepository } from './account-balance.repository'
import {
  AccountBalanceEntity,
  AccountBalanceEntitySubscriber,
} from './account-balance.repository/account-balance.entity'
import { MerchantCommissionRepository } from './merchant-commission.repository'
import {
  MerchantCommissionEntity,
  MerchantCommissionEntitySubscriber,
} from './merchant-commission.repository/merchant-commission.entity'
import { MerchantInvoiceRepository } from './merchant-invoice.repository'
import { MerchantInvoiceEntity } from './merchant-invoice.repository/merchant-invoice.entity'
import { EventStoreRepository } from './event-store.repository'
import { EventStoreEntity, EventStoreEntitySubscriber } from './event-store.repository/event-store.entity'
import { AdditionalDataRepository } from './additional-data.repository'
import { MerchantAdditionalDataRepository } from './merchant-additional-data.repository'
import { AdditionalDataEntity, AdditionalDataEntitySubscriber } from './additional-data.repository/additional-data.entity'
import { MerchantAdditionalDataEntity, MerchantAdditionalDataEntitySubscriber } from './merchant-additional-data.repository/merchant-additional-data.entity'

const repo = [
  BaseRepository,
  UserRepository,
  PermissionRepository,
  SettingRepository,
  TransactionRepository,
  TransactionDetailRepository,
  TransactionLogRepository,
  BotSettingRepository,
  PackageRepository,
  CurrencyRepository,
  CacheRepository,
  // ElasticSearchRepository,
  DBContext,
  BotRepository,
  TransactionPSRepository,
  BotSignalRepository,
  MerchantRepository,
  BotTradingRepository,
  BotTradingHistoryRepository,
  AccountBalanceRepository,
  MerchantCommissionRepository,
  MerchantInvoiceRepository,
  EventStoreRepository,
  AdditionalDataRepository,
  MerchantAdditionalDataRepository,
]
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      name: ConnectionName.user_role,
      host: APP_CONFIG.POSTGRES_HOST,
      port: APP_CONFIG.POSTGRES_PORT,
      username: APP_CONFIG.POSTGRES_USER,
      password: APP_CONFIG.POSTGRES_PASS,
      database: APP_CONFIG.POSTGRES_DB,
      entities: [
        AuthRoleEntity,
        FeatureRoleEntity,
        GeneralSettingEntity,
        RoleEntity,
        SymbolSettingRoleEntity,
        ExchangeEntity,
        FeatureEntity,
        ResolutionEntity,
        SymbolEntity,
        AuthUserRoleEntity,
        UserEntity,
        VerifyTokenEntity,
        SessionEntity,
        UserLogEntity,
        UserRoleEntity,
        UserViewEntity,
        GeneralSettingRoleEntity,
        RoleViewEntity,
        AppSettingEntity,
        TransactionEntity,
        TransactionDetailEntity,
        TransactionLogEntity,
        TransactionViewEntity,
        TransactionDetailViewEntity,
        BotSettingEntity,
        PackageEntity,
        CurrencyEntity,
        BotEntity,
        UserBotEntity,
        UserAssetLogEntity,
        BotSignalEntity,
        FavoriteSignalEntity,
        MerchantEntity,
        BotTradingEntity,
        UserBotTradingEntity,
        BotTradingHistoryEntity,
        AccountBalanceEntity,
        MerchantCommissionEntity,
        MerchantInvoiceEntity,
        EventStoreEntity,
        AdditionalDataEntity,
        MerchantAdditionalDataEntity,
      ],
      subscribers: [
        UserEntitySubscriber,
        AuthUserRoleEntitySubscriber,
        AuthRoleEntitySubscriber,
        FeatureRoleEntitySubscriber,
        GeneralSettingEntitySubscriber,
        RoleEntitySubscriber,
        SymbolSettingRoleEntitySubscriber,
        ExchangeEntitySubscriber,
        FeatureEntitySubscriber,
        ResolutionEntitySubscriber,
        SymbolEntitySubscriber,
        SessionEntitySubscriber,
        UserLogEntitySubscriber,
        UserRoleEntitySubscriber,
        GeneralSettingRoleEntitySubscriber,
        AppSettingEntitySubscriber,
        TransactionEntitySubscriber,
        TransactionDetailEntitySubscriber,
        TransactionLogEntitySubscriber,
        BotSettingEntitySubscriber,
        PackageEntitySubscriber,
        CurrencyEntitySubscriber,
        BotEntitySubscriber,
        UserBotEntitySubscriber,
        UserAssetLogEntitySubscriber,
        BotSignalEntitySubscriber,
        FavoriteSignalEntitySubscriber,
        MerchantEntitySubscriber,
        BotTradingEntitySubscriber,
        UserBotTradingEntitySubscriber,
        BotTradingHistoryEntitySubscriber,
        AccountBalanceEntitySubscriber,
        MerchantCommissionEntitySubscriber,
        MerchantEntitySubscriber,
        EventStoreEntitySubscriber,
        AdditionalDataEntitySubscriber,
        MerchantAdditionalDataEntitySubscriber,
      ],
      synchronize: true,
      migrations: ['dist/migrations/*{.ts,.js}'],
      migrationsTableName: 'migrations',
      migrationsRun: true,
      keepConnectionAlive: true,
    }),
    CacheModule.register({
      store: redisStore,
      host: APP_CONFIG.REDIS_HOST,
      port: APP_CONFIG.REDIS_PORT,
      password: APP_CONFIG.REDIS_PASS,
      ttl: 3600,
    }),
    // ElasticsearchModule.register({
    //   node: APP_CONFIG.ELASTIC_URL,
    //   auth: {
    //     username: APP_CONFIG.ELASTIC_USERNAME,
    //     password: APP_CONFIG.ELASTIC_PASS,
    //   },
    //   tls: {
    //     rejectUnauthorized: false,
    //   },
    // }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      name: ConnectionName.payment_service,
      host: APP_CONFIG.PAYMENT_POSTGRES_HOST,
      port: APP_CONFIG.PAYMENT_POSTGRES_PORT,
      username: APP_CONFIG.PAYMENT_POSTGRES_USER,
      password: APP_CONFIG.PAYMENT_POSTGRES_PASS,
      database: APP_CONFIG.PAYMENT_POSTGRES_DB,
      entities: [
        TransactionPSLogEntity,
        TransactionPSMetadataEntity,
        TransactionPSEntity,
      ],
      subscribers: [
        TransactionPSLogEntitySubscriber,
        TransactionPSEntitySubscriber,
        TransactionPSMetadataEntitySubscriber,
      ],
      synchronize: false,
      // migrations: ['dist/migrations/*{.ts,.js}'],
      // migrationsTableName: 'migrations',
      migrationsRun: false,
      keepConnectionAlive: true,
    }),
  ],
  providers: repo,
  exports: repo,
})
export class DatabaseModule {
  constructor(@Inject(CACHE_MANAGER) cacheManager) {
    const client = cacheManager.store.getClient()

    client.on('error', (error) => {
      console.error(error)
    })
  }
}
