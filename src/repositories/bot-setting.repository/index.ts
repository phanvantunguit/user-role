import { ConnectionName } from 'src/const/app-setting'
import {
  QueryBotSetting,
  RawBotSetting,
} from 'src/domains/setting/bot-setting.types'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import { BotSettingEntity } from './bot-setting.entity'

export class BotSettingRepository extends BaseRepository<
  QueryBotSetting,
  RawBotSetting
> {
  repo(): Repository<BotSettingEntity> {
    return getRepository(BotSettingEntity, ConnectionName.user_role)
  }
}
