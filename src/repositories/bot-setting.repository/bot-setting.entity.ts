import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
} from 'typeorm'

export const BOT_SETTING_TABLE = 'bot_settings'
@Entity(BOT_SETTING_TABLE)
export class BotSettingEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string

  @Column({ type: 'varchar' })
  name: string

  @Column({ type: 'jsonb', default: {} })
  params: any

  @Column({ type: 'varchar' })
  owner_created: string

  @Column({ type: 'boolean' })
  status: boolean

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number
}

@EventSubscriber()
export class BotSettingEntitySubscriber
  implements EntitySubscriberInterface<BotSettingEntity>
{
  async beforeInsert(event: InsertEvent<BotSettingEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }
  async beforeUpdate(event: UpdateEvent<BotSettingEntity>) {
    event.entity.updated_at = Date.now()
  }
}
