import { Repository, getRepository } from 'typeorm'

import { cleanObject } from 'src/utils/format-data.util'
import { FeatureEntity } from './feature.entity'
import {
  QueryExchange,
  QueryFeature,
  QueryGeneralSetting,
  QueryResolution,
  QuerySymbol,
  RawExchange,
  RawFeature,
  RawGeneralSetting,
  RawResolution,
  RawSymbol,
} from 'src/domains/setting'
import { ExchangeEntity } from './exchange.entity'
import { ResolutionEntity } from './resolution.entity'
import { SymbolEntity } from './symbol.entity'
import { GeneralSettingEntity } from './general-setting.entity'
import { AppSettingEntity, APP_SETTING_TABLE } from './app-setting.entity'
import {
  QueryAppSetting,
  RawAppSetting,
} from 'src/domains/setting/app-setting.types'
import { ConnectionName } from 'src/const/app-setting'

export class SettingRepository {
  // feature
  private featureRepo(): Repository<FeatureEntity> {
    return getRepository(FeatureEntity, ConnectionName.user_role)
  }
  async saveFeatures(params: RawFeature[]): Promise<RawFeature[]> {
    return this.featureRepo().save(params)
  }
  async deleteFeatureIds(ids: string[]) {
    return this.featureRepo().delete(ids)
  }
  async findFeature(params: QueryFeature): Promise<RawFeature[]> {
    const query = cleanObject(params)
    return this.featureRepo().find({
      where: query,
      order: { created_at: 'DESC' },
    })
  }
  async findFeatureByIds(ids): Promise<RawFeature[]> {
    return this.featureRepo().findByIds(ids)
  }

  // exchange
  private exchangeRepo(): Repository<ExchangeEntity> {
    return getRepository(ExchangeEntity, ConnectionName.user_role)
  }
  async saveExchanges(params: RawExchange[]): Promise<RawExchange[]> {
    return this.exchangeRepo().save(params)
  }
  async deleteExchangeNames(names: string[]) {
    return this.exchangeRepo().delete(names)
  }
  async findExchange(params: QueryExchange): Promise<RawExchange[]> {
    const query = cleanObject(params)
    return this.exchangeRepo().find({
      where: query,
      order: { created_at: 'DESC' },
    })
  }
  async findExchangeByNames(names: string[]): Promise<RawExchange[]> {
    return this.exchangeRepo().findByIds(names)
  }
  // resolution
  private resolutionRepo(): Repository<ResolutionEntity> {
    return getRepository(ResolutionEntity, ConnectionName.user_role)
  }
  async saveResolution(params: RawResolution[]): Promise<RawResolution[]> {
    return this.resolutionRepo().save(params)
  }
  async deleteResolutionIds(ids: string[]) {
    return this.resolutionRepo().delete(ids)
  }
  async findResolution(params: QueryResolution): Promise<RawResolution[]> {
    const query = cleanObject(params)
    return this.resolutionRepo().find({
      where: query,
      order: { created_at: 'DESC' },
    })
  }
  async findResolutionByIds(ids: string[]): Promise<RawResolution[]> {
    return this.resolutionRepo().findByIds(ids)
  }
  // symbol
  private symbolRepo(): Repository<SymbolEntity> {
    return getRepository(SymbolEntity, ConnectionName.user_role)
  }
  async saveSymbol(params: RawSymbol[]): Promise<RawSymbol[]> {
    return this.symbolRepo().save(params)
  }
  async deleteSymbols(symbols: string[]) {
    return this.symbolRepo().delete(symbols)
  }
  async findSymbol(params: QuerySymbol): Promise<RawSymbol[]> {
    const query = cleanObject(params)
    return this.symbolRepo().find({
      where: query,
      order: { created_at: 'DESC' },
    })
  }
  async findSymbolBySymbols(symbols: string[]): Promise<RawSymbol[]> {
    return this.symbolRepo().findByIds(symbols)
  }
  // general setting
  private generalSettingRepo(): Repository<GeneralSettingEntity> {
    return getRepository(GeneralSettingEntity, ConnectionName.user_role)
  }
  async saveGeneralSetting(
    params: RawGeneralSetting[]
  ): Promise<RawGeneralSetting[]> {
    return this.generalSettingRepo().save(params)
  }
  async deleteGeneralSettingByIds(ids: string[]) {
    return this.generalSettingRepo().delete(ids)
  }
  async findGeneralSetting(
    params: QueryGeneralSetting
  ): Promise<RawGeneralSetting[]> {
    const query = cleanObject(params)
    return this.generalSettingRepo().find({
      where: query,
      order: { created_at: 'DESC' },
    })
  }
  async findGeneralSettingByIds(ids: string[]): Promise<RawGeneralSetting[]> {
    return this.generalSettingRepo().findByIds(ids)
  }

  // app setting
  private appSettingRepo(): Repository<AppSettingEntity> {
    return getRepository(AppSettingEntity, ConnectionName.user_role)
  }
  async saveAppSetting(params: RawAppSetting): Promise<RawAppSetting> {
    return this.appSettingRepo().save(params)
  }
  async findOneAppSetting(params: {
    name?: string
    user_id?: string
  }): Promise<RawAppSetting> {
    return this.appSettingRepo().findOne(params)
  }
  async findAppSetting(params: QueryAppSetting): Promise<RawAppSetting[]> {
    const { id, name, user_id } = params

    const whereArray = []
    if (user_id) {
      whereArray.push(`(user_id = '${user_id}' OR user_id = 'system')`)
    } else {
      whereArray.push(`user_id = 'system'`)
    }
    if (id) {
      whereArray.push(`id = '${id}'`)
    }
    if (name) {
      whereArray.push(`name = '${name}'`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const querySetting = `
    SELECT  *
    FROM ${APP_SETTING_TABLE}  
    ${where}  
    ORDER BY updated_at DESC
  `
    return this.appSettingRepo().query(querySetting)
  }
  async deleteAppSettingById(id: string) {
    return this.appSettingRepo().delete(id)
  }
}
