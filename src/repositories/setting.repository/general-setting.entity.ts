import {
  Entity,
  Column,
  PrimaryColumn,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
} from 'typeorm'

export const GENERAL_SETTING_TABLE = 'general_settings'
@Entity(GENERAL_SETTING_TABLE)
export class GeneralSettingEntity {
  @PrimaryColumn()
  general_setting_id: string

  @Column({ type: 'varchar' })
  general_setting_name: string

  @Column({ type: 'varchar', nullable: true })
  description: string

  @Column({ type: 'varchar' })
  owner_created: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number
}

@EventSubscriber()
export class GeneralSettingEntitySubscriber
  implements EntitySubscriberInterface<GeneralSettingEntity>
{
  async beforeInsert(event: InsertEvent<GeneralSettingEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }
  async beforeUpdate(event: UpdateEvent<GeneralSettingEntity>) {
    event.entity.updated_at = Date.now()
  }
}
