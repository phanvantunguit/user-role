import { SYMBOL_STATUS } from 'src/const'
import {
  Entity,
  Column,
  PrimaryColumn,
  EventSubscriber,
  InsertEvent,
  EntitySubscriberInterface,
} from 'typeorm'

export const SYMBOLS_TABLE = 'symbols'
@Entity(SYMBOLS_TABLE)
export class SymbolEntity {
  @PrimaryColumn()
  symbol: string

  @Column({ type: 'varchar' })
  types: string

  @Column({ type: 'varchar' })
  exchange_name: string

  @Column({ type: 'varchar' })
  base_symbol: string

  @Column({ type: 'varchar' })
  quote_symbol: string

  @Column({ type: 'varchar', nullable: true })
  description: string

  @Column({ type: 'jsonb', default: {} })
  ticks: any

  @Column({ type: 'varchar', default: SYMBOL_STATUS.ON })
  status: SYMBOL_STATUS

  @Column({ type: 'varchar', nullable: true })
  timezone: string

  @Column({ type: 'int4', nullable: true })
  minmov: number

  @Column({ type: 'int4', nullable: true })
  minmov2: number

  @Column({ type: 'int4', nullable: true })
  pointvalue: number

  @Column({ type: 'varchar', nullable: true })
  session: string

  @Column({ type: 'boolean', nullable: true })
  has_intraday: boolean

  @Column({ type: 'boolean', nullable: true })
  has_no_volume: boolean

  @Column({ type: 'int4', nullable: true })
  pricescale: number

  @Column({ type: 'jsonb', nullable: true })
  config_heatmap: any

  @Column({ type: 'int4', nullable: true })
  list_token: number

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number
}
@EventSubscriber()
export class SymbolEntitySubscriber
  implements EntitySubscriberInterface<SymbolEntity>
{
  async beforeInsert(event: InsertEvent<SymbolEntity>) {
    event.entity.created_at = Date.now()
  }
}
