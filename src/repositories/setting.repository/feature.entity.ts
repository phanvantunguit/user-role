import {
  Entity,
  Column,
  PrimaryColumn,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
} from 'typeorm'

export const FEATURES_TABLE = 'features'
@Entity(FEATURES_TABLE)
export class FeatureEntity {
  @PrimaryColumn()
  feature_id: string

  @Column({ type: 'varchar' })
  feature_name: string

  @Column({ type: 'varchar', nullable: true })
  description: string

  @Column({ type: 'varchar', nullable: true })
  action: string

  @Column({ type: 'varchar' })
  owner_created: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number
}

@EventSubscriber()
export class FeatureEntitySubscriber
  implements EntitySubscriberInterface<FeatureEntity>
{
  async beforeInsert(event: InsertEvent<FeatureEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }
  async beforeUpdate(event: UpdateEvent<FeatureEntity>) {
    event.entity.updated_at = Date.now()
  }
}
