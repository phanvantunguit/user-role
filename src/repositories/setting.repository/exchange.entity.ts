import {
  Entity,
  Column,
  PrimaryColumn,
  EventSubscriber,
  InsertEvent,
  EntitySubscriberInterface,
} from 'typeorm'

export const EXCHANGES_TABLE = 'exchanges'
@Entity(EXCHANGES_TABLE)
export class ExchangeEntity {
  @PrimaryColumn()
  exchange_name: string

  @Column({ type: 'varchar' })
  exchange_desc: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number
}
@EventSubscriber()
export class ExchangeEntitySubscriber
  implements EntitySubscriberInterface<ExchangeEntity>
{
  async beforeInsert(event: InsertEvent<ExchangeEntity>) {
    event.entity.created_at = Date.now()
  }
}
