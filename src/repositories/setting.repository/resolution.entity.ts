import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent,
} from 'typeorm'

export const RESOLUTIONS_TABLE = 'resolutions'
@Entity(RESOLUTIONS_TABLE)
export class ResolutionEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string

  @Column({ type: 'varchar' })
  resolutions_name: string

  @Column({ type: 'varchar', nullable: true })
  display_name: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number
}
@EventSubscriber()
export class ResolutionEntitySubscriber
  implements EntitySubscriberInterface<ResolutionEntity>
{
  async beforeInsert(event: InsertEvent<ResolutionEntity>) {
    event.entity.created_at = Date.now()
  }
}
