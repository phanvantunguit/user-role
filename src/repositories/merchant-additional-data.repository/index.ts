import { ConnectionName } from 'src/const/app-setting'
import { ADDITIONAL_DATA_TYPE, MERCHANT_ADDITIONAL_DATA_STATUS } from 'src/domains/merchant/types'
import { Repository, getRepository } from 'typeorm'
import { ADDITIONAL_DATA_TABLE } from '../additional-data.repository/additional-data.entity'
import { BaseRepository } from '../base.repository/base.repository'
import { MerchantAdditionalDataEntity, MERCHANT_ADDITIONAL_DATA_TABLE } from './merchant-additional-data.entity'

export class MerchantAdditionalDataRepository extends BaseRepository<
MerchantAdditionalDataEntity,
  MerchantAdditionalDataEntity
> {
  repo(): Repository<MerchantAdditionalDataEntity> {
    return getRepository(MerchantAdditionalDataEntity, ConnectionName.user_role)
  }
  list(param: {
    merchant_id?: string;
    type?: ADDITIONAL_DATA_TYPE;
    status?: MERCHANT_ADDITIONAL_DATA_STATUS;
  }) {
    const { merchant_id, type, status } = param;
    const whereArray = [];
    if (merchant_id) {
      whereArray.push(`madt.merchant_id = '${merchant_id}'`);
    }
    if (type) {
      whereArray.push(`adt.type = '${type}'`);
    }
    if (status) {
      whereArray.push(`madt.status = '${status}'`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const query = `
    SELECT madt.*,
    adt.type as type,
    adt.name as name,
    adt.data as data
    FROM ${MERCHANT_ADDITIONAL_DATA_TABLE} madt
    LEFT JOIN ${ADDITIONAL_DATA_TABLE} adt on adt.id = madt.additional_data_id
    ${where} 
    order by madt.order ASC
  `;
    return this.repo().query(query);
  }
}
