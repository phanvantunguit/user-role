import { ConnectionName } from 'src/const/app-setting'
import { QueryCurrency, RawCurrency } from 'src/domains/setting/currency.types'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import { CURRENCIES_TABLE, CurrencyEntity } from './currency.entity'

export class CurrencyRepository extends BaseRepository<
  QueryCurrency,
  RawCurrency
> {
  repo(): Repository<CurrencyEntity> {
    return getRepository(CurrencyEntity, ConnectionName.user_role)
  }
  async find(
    params: QueryCurrency & { ids: string[] }
  ): Promise<CurrencyEntity[]> {
    const { id, name, currency, status, ids } = params
    const whereArray = []
    if (status) {
      whereArray.push(`status = ${status}`)
    }
    if (name) {
      whereArray.push(`name = '${name}'`)
    }
    if (id) {
      whereArray.push(`id = '${id}'`)
    }
    if (currency) {
      whereArray.push(`currency = '${currency}'`)
    }
    if (ids && ids.length > 0) {
      whereArray.push(`id in (${ids.map((e) => `'${e}'`)})`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const querySetting = `
    SELECT  *
    FROM ${CURRENCIES_TABLE}  
    ${where}  
    ORDER BY "order" ASC
    `
    return this.repo().query(querySetting)
  }
}
