import {
  Entity,
  Column,
  PrimaryColumn,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
  Generated,
} from 'typeorm'

export const CURRENCIES_TABLE = 'currencies'
@Entity(CURRENCIES_TABLE)
export class CurrencyEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string

  @Column({ type: 'varchar' })
  name: string

  @Column({ type: 'varchar' })
  description: string

  @Column({ type: 'varchar' })
  currency: string

  @Column({ type: 'varchar' })
  image_url: string

  @Column({ type: 'uuid' })
  owner_created: string

  @Column({ type: 'boolean', default: true })
  status: boolean

  @Column({ type: 'int4', nullable: true })
  order: number

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number
}

@EventSubscriber()
export class CurrencyEntitySubscriber
  implements EntitySubscriberInterface<CurrencyEntity>
{
  async beforeInsert(event: InsertEvent<CurrencyEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }
  async beforeUpdate(event: UpdateEvent<CurrencyEntity>) {
    event.entity.updated_at = Date.now()
  }
}
