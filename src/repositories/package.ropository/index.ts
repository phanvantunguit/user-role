import { ConnectionName } from 'src/const/app-setting'
import { QueryPackage, RawPackage } from 'src/domains/setting/package.types'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import { PackageEntity } from './package.entity'

export class PackageRepository extends BaseRepository<
  QueryPackage,
  RawPackage
> {
  repo(): Repository<PackageEntity> {
    return getRepository(PackageEntity, ConnectionName.user_role)
  }
  find(params: QueryPackage): Promise<RawPackage[]> {
    return this.repo().find({ where: params, order: { quantity: 1 } })
  }
}
