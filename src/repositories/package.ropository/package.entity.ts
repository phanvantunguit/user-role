import { PACKAGE_TYPE } from 'src/const'
import {
  Entity,
  Column,
  PrimaryColumn,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
  Generated,
} from 'typeorm'

export const PACKAGES_TABLE = 'packages'
@Entity(PACKAGES_TABLE)
export class PackageEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string

  @Column({ type: 'varchar' })
  name: string

  @Column({ type: 'float4', nullable: true })
  discount_rate: number

  @Column({ type: 'float4', nullable: true })
  discount_amount: number

  @Column({ type: 'boolean', default: true })
  status: boolean

  @Column({ type: 'float4', nullable: true })
  quantity: number

  @Column({ type: 'varchar', nullable: true })
  type: PACKAGE_TYPE

  @Column({ type: 'uuid' })
  owner_created: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number
}

@EventSubscriber()
export class PackageEntitySubscriber
  implements EntitySubscriberInterface<PackageEntity>
{
  async beforeInsert(event: InsertEvent<PackageEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }
  async beforeUpdate(event: UpdateEvent<PackageEntity>) {
    event.entity.updated_at = Date.now()
  }
}
