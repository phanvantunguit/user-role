import { ConnectionName } from 'src/const/app-setting'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import { EventStoreEntity, EVENT_STORE_TABLE } from './event-store.entity'

export class EventStoreRepository extends BaseRepository<
  EventStoreEntity,
  EventStoreEntity
> {
  repo(): Repository<EventStoreEntity> {
    return getRepository(EventStoreEntity, ConnectionName.user_role)
  }
  
  findEventStore(param: {
    event_id?: string
    event_name?: string | string[]
    user_id?: string
    state?: string
    from?: string
    to?: string
  }): Promise<EventStoreEntity[]> {
    const { event_id, event_name, user_id, state, from, to } = param
    const whereArray = []
    if (event_id) {
      whereArray.push(`event_id = '${event_id}'`)
    }
    if (event_name) {
      if(Array.isArray(event_name) && event_name.length > 0) {
        whereArray.push(`event_name in (${event_name.map((e) => `'${e}'`)})`)
      } else {
        whereArray.push(`'event_name' = '${event_name}'`)
      }
    }
    if (user_id) {
      whereArray.push(`'user_id' = '${user_id}'`)
    }
    if (state) {
      whereArray.push(`'state' = '${state}'`)
    }
    if (from) {
      whereArray.push(`created_at >= ${from}`)
    }
    if (to) {
      whereArray.push(`created_at <= ${to}`)
    }
    const where =
    whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''

    const query = `
    SELECT * FROM
    ${EVENT_STORE_TABLE}
    ${where} 
    `
    return this.repo().query(query)
  }
}
