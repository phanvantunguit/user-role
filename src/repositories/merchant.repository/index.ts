import { ConnectionName } from 'src/const/app-setting'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import { MerchantEntity, MERCHANTS_TABLE } from './merchant.entity'

export class MerchantRepository extends BaseRepository<
  MerchantEntity,
  MerchantEntity
> {
  repo(): Repository<MerchantEntity> {
    return getRepository(MerchantEntity, ConnectionName.user_role)
  }
  findMerchant(params: { status?: string; domain_type?: string }): Promise<MerchantEntity[]> {
    const { status, domain_type } = params
    const whereArray = []
    if (status) {
      whereArray.push(`status = '${status}'`)
    }
    if (domain_type) {
      whereArray.push(`config::json->>'domain_type' = '${domain_type}'`)
    }
    const where =
    whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const queryMerchant = `
      SELECT *
      FROM ${MERCHANTS_TABLE}
      ${where}
    `
    return this.repo().query(queryMerchant)
  }
}
