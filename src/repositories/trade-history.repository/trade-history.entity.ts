import {
  Entity,
  Column,
  PrimaryColumn,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
} from 'typeorm'

export const TRADE_HISTORIES_TABLE = 'history_trade'
@Entity(TRADE_HISTORIES_TABLE, { synchronize: false })
export class TradeHistoryEntity {
  @PrimaryColumn({ type: 'varchar' })
  name_bot: string;

  @Column({ type: 'varchar' })
  client_id: string;

  @PrimaryColumn({ type: 'bigint' })
  order_id: number

  @Column({ type: 'bigint' })
  open_time: bigint
  
  @Column({ type: 'bigint', nullable: true })
  close_time: bigint

  @Column({ type: 'varchar' })
  type: string

  @Column({ type: 'numeric' })
  size: number

  @Column({ type: 'varchar' })
  symbol: string

  @Column({ type: 'varchar' })
  resolution: string

  @Column({ type: 'numeric' })
  stop_lost: number

  @Column({ type: 'numeric' })
  take_profit: number

  @Column({ type: 'numeric', nullable: true })
  commission: number

  @Column({ type: 'numeric', nullable: true })
  swap: number

  @Column({ type: 'numeric', nullable: true })
  profit: number
  
  @Column({ type: 'text', nullable: true })
  comment: string

  @Column({ type: 'jsonb', default: {} })
  extradata: any

  @Column({ type: 'numeric', nullable: true })
  open_price: number

  @Column({ type: 'numeric', nullable: true })
  close_price: number

  @Column({ type: 'varchar' })
  position_side: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number
}

@EventSubscriber()
export class TradeHistoryEntitySubscriber
  implements EntitySubscriberInterface<TradeHistoryEntity>
{
  async beforeInsert(event: InsertEvent<TradeHistoryEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }
  async beforeUpdate(event: UpdateEvent<TradeHistoryEntity>) {
    event.entity.updated_at = Date.now()
  }
}
