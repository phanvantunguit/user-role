import { BaseEntity, DeleteResult, Repository, getRepository, QueryRunner } from 'typeorm'
import { EntityId } from 'typeorm/repository/EntityId'
import { IBaseRepository } from './types'

export class BaseRepository<Query, T> implements IBaseRepository<Query, T> {
  repo(): Repository<any> {
    return getRepository(BaseEntity)
  }
  async findOne(params: Query): Promise<T> {
    return this.repo().findOne({ where: params, order: { created_at: 'DESC' } })
  }

  async find(params: Query): Promise<T[]> {
    return this.repo().find({ where: params, order: { created_at: 'DESC' } })
  }

  async findById(id: EntityId): Promise<T> {
    return this.repo().findOne(id)
  }

  async findByIds(ids: any): Promise<T[]> {
    return this.repo().findByIds(ids)
  }

  async save(params: any, queryRunner?: QueryRunner): Promise<T> {
    if (queryRunner) {
      const transaction = await this.repo().create(params);
      return queryRunner.manager.save(transaction);
    } else {
      return this.repo().save(params)
    }
  }
  async saves(params: T[]): Promise<T[]> {
    return this.repo().save(params)
  }

  async deleteById(id: EntityId): Promise<DeleteResult> {
    return this.repo().delete(id)
  }
  async deleteByIds(ids: EntityId[]): Promise<DeleteResult> {
    return this.repo().delete(ids)
  }
  async delete(params: Query, queryRunner?: QueryRunner): Promise<DeleteResult> {
    if (queryRunner) {
      return queryRunner.manager.delete(this.repo().target, params)
    } else {
      return this.repo().delete(params)
    }
  }

  async count(params: T): Promise<number> {
    return this.repo().count(params)
  }
}
