import { DeleteResult } from 'typeorm'
import { EntityId } from 'typeorm/repository/EntityId'

export interface IBaseRepository<Query, T> {
  findOne(params: Query): Promise<T>
  find(params: Query): Promise<T[]>
  findById(id: EntityId): Promise<T>
  findByIds(ids: [EntityId]): Promise<T[]>
  save(params: T): Promise<T>
  delete(params: Query): Promise<DeleteResult>
  deleteById(id: EntityId): Promise<DeleteResult>
}
