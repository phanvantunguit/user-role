import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
} from 'typeorm'

export const FEATURE_ROLES_TABLE = 'feature_roles'
@Entity(FEATURE_ROLES_TABLE)
export class FeatureRoleEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string

  @Column({ type: 'uuid' })
  role_id: string

  @Column({ type: 'varchar' })
  feature_id: string

  @Column({ type: 'varchar', nullable: true })
  description: string

  @Column({ type: 'varchar' })
  owner_created: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number
}

@EventSubscriber()
export class FeatureRoleEntitySubscriber
  implements EntitySubscriberInterface<FeatureRoleEntity>
{
  async beforeInsert(event: InsertEvent<FeatureRoleEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }
  async beforeUpdate(event: UpdateEvent<FeatureRoleEntity>) {
    event.entity.updated_at = Date.now()
  }
}
