import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
} from 'typeorm'

export const GENERAL_SETTINGS_ROLES_TABLE = 'general_settings_roles'
@Entity(GENERAL_SETTINGS_ROLES_TABLE)
export class GeneralSettingRoleEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string

  @Column({ type: 'uuid' })
  role_id: string

  @Column({ type: 'varchar' })
  general_setting_id: string

  @Column({ type: 'varchar', nullable: true })
  description: string

  @Column({ type: 'bigint' })
  val_limit: number

  @Column({ type: 'varchar' })
  owner_created: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number
}

@EventSubscriber()
export class GeneralSettingRoleEntitySubscriber
  implements EntitySubscriberInterface<GeneralSettingRoleEntity>
{
  async beforeInsert(event: InsertEvent<GeneralSettingRoleEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }
  async beforeUpdate(event: UpdateEvent<GeneralSettingRoleEntity>) {
    event.entity.updated_at = Date.now()
  }
}
