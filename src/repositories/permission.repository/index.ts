import { Repository, getRepository } from 'typeorm'
import { AuthRoleEntity } from './auth-role.entity'
import { RoleEntity, ROLES_TABLE } from './role.entity'
import {
  QueryAuthRole,
  QueryFeatureRole,
  QueryGeneralSettingRole,
  QueryRole,
  QuerySymbolSettingRole,
  RawAuthRole,
  RawFeatureRole,
  RawGeneralSettingRole,
  RawRole,
  RawSymbolSettingRole,
} from 'src/domains/permission'
import {
  GeneralSettingRoleEntity,
  GENERAL_SETTINGS_ROLES_TABLE,
} from './general-setting-role.entity'
import { cleanObject } from 'src/utils/format-data.util'
import { SymbolSettingRoleEntity } from './symbol-setting-role.entity'
import { FeatureRoleEntity } from './feature-role.entity'
import { ROLES_VIEW, RoleViewEntity } from './role-view.entity'
import { ConnectionName } from 'src/const/app-setting'

export class PermissionRepository {
  // auth role
  private authRoleRepo(): Repository<AuthRoleEntity> {
    return getRepository(AuthRoleEntity, ConnectionName.user_role)
  }
  async saveAuthRoles(params: RawAuthRole[]): Promise<RawAuthRole[]> {
    return this.authRoleRepo().save(params)
  }
  async findAuthRole(params: QueryAuthRole): Promise<RawAuthRole[]> {
    const query = cleanObject(params)
    return this.authRoleRepo().find({
      where: query,
      order: { created_at: 'DESC' },
    })
  }
  async findAuthRoleByIds(auth_role_ids: string[]): Promise<RawAuthRole[]> {
    return this.authRoleRepo().findByIds(auth_role_ids)
  }
  async deleteAuthRoleByIds(auth_role_ids: string[]): Promise<any> {
    return this.authRoleRepo().delete(auth_role_ids)
  }
  // feature role
  private featureRoleRepo(): Repository<FeatureRoleEntity> {
    return getRepository(FeatureRoleEntity, ConnectionName.user_role)
  }
  async saveFeatureRoles(params: RawFeatureRole[]): Promise<RawFeatureRole[]> {
    return this.featureRoleRepo().save(params)
  }
  async findFeatureRoles(params: QueryFeatureRole): Promise<RawFeatureRole[]> {
    return this.featureRoleRepo().find(params)
  }
  async deleteFeatureRolesByIds(ids: string[]): Promise<any> {
    return this.featureRoleRepo().delete(ids)
  }
  // general setting role
  private generalSettingRoleRepo(): Repository<GeneralSettingRoleEntity> {
    return getRepository(GeneralSettingRoleEntity, ConnectionName.user_role)
  }
  async saveGeneralSettingRoles(
    params: RawGeneralSettingRole[]
  ): Promise<GeneralSettingRoleEntity[]> {
    return this.generalSettingRoleRepo().save(params)
  }
  async findGeneralSettingRoles(
    params: QueryGeneralSettingRole
  ): Promise<RawGeneralSettingRole[]> {
    return this.generalSettingRoleRepo().find(params)
  }
  async findGeneralSettingMultipleRoles(
    roleIds: string[],
    generalSettingId: string
  ): Promise<RawGeneralSettingRole[]> {
    const query = `
      SELECT *
      FROM ${GENERAL_SETTINGS_ROLES_TABLE}  
      WHERE 
      ${
        roleIds.length > 0
          ? `role_id in (${roleIds.map((id) => `'${id}'`)}) and`
          : ''
      } 
	    general_setting_id = '${generalSettingId}'  
    `
    return this.generalSettingRoleRepo().query(query)
  }
  async deleteGeneralSettingRolesByIds(ids: string[]): Promise<any> {
    return this.generalSettingRoleRepo().delete(ids)
  }
  // role
  private roleRepo(): Repository<RoleEntity> {
    return getRepository(RoleEntity, ConnectionName.user_role)
  }
  async saveRoles(params: RawRole[]): Promise<RoleEntity[]> {
    return this.roleRepo().save(params)
  }
  async deleteRoleByIds(ids: string[]): Promise<any> {
    return this.roleRepo().delete(ids)
  }
  async findOneRoleTable(params: QueryRole): Promise<any> {
    return this.roleRepo().findOne(params)
  }
  // role view
  private roleViewRepo(): Repository<RoleViewEntity> {
    return getRepository(RoleViewEntity, ConnectionName.user_role)
  }
  async findRoles(params: QueryRole): Promise<RoleViewEntity[]> {
    const query = cleanObject(params)
    return this.roleViewRepo().find({
      where: query,
      order: { order: 'ASC' },
    })
  }
  async findRolesConditionMultiple(params: {
    status?: string[]
  }): Promise<RoleViewEntity[]> {
    const { status } = params
    const whereArray = []
    if (status && status.length > 0) {
      whereArray.push(`status in (${status.map((e) => `'${e}'`)})`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
      SELECT *
      FROM ${ROLES_TABLE}  
      ${where} 
      order by "order" ASC
    `
    return this.roleRepo().query(query)
  }
  async findRoleByIds(ids: string[]): Promise<RawRole[]> {
    const queryRoles = `
        SELECT *
        FROM ${ROLES_VIEW} rv 
        WHERE id in (${ids.map((e) => `'${e}'`)})
        `
    const data = await this.roleViewRepo().query(queryRoles)
    return data
  }
  // symbol setting role
  private symbolSettingRoleRepo(): Repository<SymbolSettingRoleEntity> {
    return getRepository(SymbolSettingRoleEntity, ConnectionName.user_role)
  }
  async saveSymbolSettingRole(
    params: RawSymbolSettingRole[]
  ): Promise<SymbolSettingRoleEntity[]> {
    return this.symbolSettingRoleRepo().save(params)
  }
  async findSymbolSettingRoleByIds(
    ids: string[]
  ): Promise<SymbolSettingRoleEntity[]> {
    return this.symbolSettingRoleRepo().findByIds(ids)
  }
  async deleteSymbolSettingRoleByIds(ids: string[]): Promise<any> {
    return this.symbolSettingRoleRepo().delete(ids)
  }
  async findSymbolSettingRole(
    params: QuerySymbolSettingRole
  ): Promise<SymbolSettingRoleEntity[]> {
    return this.symbolSettingRoleRepo().find(params)
  }
}
