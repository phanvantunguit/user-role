import { ViewEntity, ViewColumn } from 'typeorm'

export const ROLES_VIEW = 'roles_view'
@ViewEntity(ROLES_VIEW, { expression: '', synchronize: false })
export class RoleViewEntity {
  @ViewColumn()
  id: string

  @ViewColumn()
  role_name: string

  @ViewColumn()
  root: any

  @ViewColumn()
  description: string

  @ViewColumn()
  status: string

  @ViewColumn()
  type: string

  @ViewColumn()
  price: string

  @ViewColumn()
  currency: string

  @ViewColumn()
  owner_created: string

  @ViewColumn()
  parent_id: string

  @ViewColumn()
  is_best_choice: boolean

  @ViewColumn()
  order: number

  @ViewColumn()
  description_features: any

  @ViewColumn()
  color: string

  @ViewColumn()
  created_at: number

  @ViewColumn()
  updated_at: number
}
