import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
} from 'typeorm'

export const SYMBOL_SETTINGS_ROLES_TABLE = 'symbol_settings_roles'
@Entity(SYMBOL_SETTINGS_ROLES_TABLE)
export class SymbolSettingRoleEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string

  @Column({ type: 'uuid' })
  role_id: string

  @Column({ type: 'varchar', nullable: true })
  description: string

  @Column({ type: 'jsonb', nullable: true })
  list_exchanged: any

  @Column({ type: 'jsonb', nullable: true })
  list_symbol: any

  @Column({ type: 'jsonb', nullable: true })
  supported_resolutions: any

  @Column({ type: 'varchar' })
  owner_created: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number
}

@EventSubscriber()
export class SymbolSettingRoleEntitySubscriber
  implements EntitySubscriberInterface<SymbolSettingRoleEntity>
{
  async beforeInsert(event: InsertEvent<SymbolSettingRoleEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }
  async beforeUpdate(event: UpdateEvent<SymbolSettingRoleEntity>) {
    event.entity.updated_at = Date.now()
  }
}
