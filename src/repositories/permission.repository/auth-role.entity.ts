import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
} from 'typeorm'

export const AUTH_ROLES_TABLE = 'auth_roles'
@Entity(AUTH_ROLES_TABLE)
export class AuthRoleEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string

  @Column({ type: 'varchar', unique: true })
  role_name: string

  @Column({ type: 'jsonb', default: {} })
  root: any

  @Column({ type: 'varchar', nullable: true })
  description: string

  @Column({ type: 'varchar' })
  owner_created: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number
}

@EventSubscriber()
export class AuthRoleEntitySubscriber
  implements EntitySubscriberInterface<AuthRoleEntity>
{
  async beforeInsert(event: InsertEvent<AuthRoleEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }
  async beforeUpdate(event: UpdateEvent<AuthRoleEntity>) {
    event.entity.updated_at = Date.now()
  }
}
