import { ConnectionName } from 'src/const/app-setting'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import {
  MERCHANT_INVOICES_TABLE,
  MerchantInvoiceEntity,
} from './merchant-invoice.entity'

export class MerchantInvoiceRepository extends BaseRepository<
  MerchantInvoiceEntity,
  MerchantInvoiceEntity
> {
  repo(): Repository<MerchantInvoiceEntity> {
    return getRepository(MerchantInvoiceEntity, ConnectionName.user_role)
  }
}
