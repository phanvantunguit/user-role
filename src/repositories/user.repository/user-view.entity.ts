import { ViewEntity, ViewColumn } from 'typeorm'
export const USER_VIEW = 'user_view'
@ViewEntity(USER_VIEW, { expression: '', synchronize: false })
export class UserViewEntity {
  @ViewColumn()
  id: string

  @ViewColumn()
  email: string

  @ViewColumn()
  username: string

  @ViewColumn()
  phone: string

  @ViewColumn()
  first_name: string

  @ViewColumn()
  last_name: string

  @ViewColumn()
  address: string

  @ViewColumn()
  affiliate_code: string

  @ViewColumn()
  link_affiliate: string

  @ViewColumn()
  referral_code: string

  @ViewColumn()
  profile_pic: string

  @ViewColumn()
  active: boolean

  @ViewColumn()
  email_confirmed: boolean

  @ViewColumn()
  note_updated: string

  @ViewColumn()
  date_registered: string

  @ViewColumn()
  super_user: boolean

  @ViewColumn()
  is_admin: boolean

  @ViewColumn()
  roles: any

  @ViewColumn()
  auth_roles: any

  @ViewColumn()
  country: string

  @ViewColumn()
  year_of_birth: string

  @ViewColumn()
  gender: string

  @ViewColumn()
  phone_code: string

  @ViewColumn()
  created_at: number

  @ViewColumn()
  updated_at: number
}
