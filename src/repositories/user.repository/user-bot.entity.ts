import { ITEM_STATUS } from 'src/const/transaction'
import {
  Entity,
  Column,
  Index,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
  Unique,
} from 'typeorm'

export const USER_BOT_TABLE = 'user_bots'
@Entity(USER_BOT_TABLE)
@Unique('sbot_id_user_id_un', ['bot_id', 'user_id'])
export class UserBotEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string

  @Column({ type: 'uuid' })
  bot_id?: string

  @Column({ type: 'uuid' })
  @Index()
  user_id?: string

  @Column({ type: 'uuid' })
  owner_created?: string

  // @Column({ type: 'bigint', nullable: true })
  // package_type: string

  // @Column({ type: 'bigint', nullable: true })
  // quantity: number

  @Column({ type: 'bigint', nullable: true })
  expires_at: number

  @Column({ type: 'varchar' })
  status?: ITEM_STATUS

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number
}

@EventSubscriber()
export class UserBotEntitySubscriber
  implements EntitySubscriberInterface<UserBotEntity>
{
  async beforeInsert(event: InsertEvent<UserBotEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }
  async beforeUpdate(event: UpdateEvent<UserBotEntity>) {
    event.entity.updated_at = Date.now()
  }
}
