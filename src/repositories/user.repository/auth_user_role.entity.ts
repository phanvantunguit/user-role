import {
  Entity,
  Column,
  Index,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
} from 'typeorm'

export const AUTH_USER_ROLES_TABLE = 'auth_user_roles'
@Entity(AUTH_USER_ROLES_TABLE)
export class AuthUserRoleEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string

  @Column({ type: 'uuid' })
  auth_role_id: string

  @Column({ type: 'uuid' })
  @Index()
  user_id: string

  @Column({ type: 'varchar', nullable: true })
  description: string

  @Column({ type: 'varchar' })
  owner_created: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number
}

@EventSubscriber()
export class AuthUserRoleEntitySubscriber
  implements EntitySubscriberInterface<AuthUserRoleEntity>
{
  async beforeInsert(event: InsertEvent<AuthUserRoleEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }
  async beforeUpdate(event: UpdateEvent<AuthUserRoleEntity>) {
    event.entity.updated_at = Date.now()
  }
}
