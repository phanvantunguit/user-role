import { Repository, getRepository, QueryRunner } from 'typeorm'
import { UserEntity, USERS_TABLE } from './user.entity'
import { VerifyTokenEntity } from './verify-token.entity'
import {
  DeleteToken,
  QueryAuthUserRole,
  QuerySession,
  QueryToken,
  QueryUser,
  QueryUserLog,
  QueryUserRole,
  RawAuthUserRole,
  RawSession,
  RawUser,
  RawUserLog,
  RawUserRole,
  RawUserView,
  RawVerifyToken,
} from 'src/domains/user'
import { cleanObject, formatQueryArray } from 'src/utils/format-data.util'
import { AuthUserRoleEntity } from './auth_user_role.entity'
import { UserRoleEntity, USER_ROLE_TABLE } from './user-role.entity'
import { SessionEntity, SESSIONS_TABLE } from './session.entity'
import { UserViewEntity, USER_VIEW } from './user-view.entity'
import { RawRole } from 'src/domains/permission'
import { UserBotEntity, USER_BOT_TABLE } from './user-bot.entity'
import { QueryUserBot, RawUserBot } from 'src/domains/user/user-bot.types'
import {
  QueryUserAssetLog,
  RawUserAssetLog,
} from 'src/domains/user/user-asset-log.types'
import { UserLogEntity } from './user-log.entity'
import {
  UserAssetLogEntity,
  USER_ASSET_LOG_TABLE,
} from './user-asset-log.entity'
import { ConnectionName } from 'src/const/app-setting'
import { ITEM_STATUS, ORDER_CATEGORY, TBOT_TYPE } from 'src/const/transaction'
import {
  UserBotTradingEntity,
  USER_BOT_TRADING_TABLE,
} from './user-bot-trading.entity'
import {
  QueryUserBotTrading,
  RawUserBotTrading,
} from 'src/domains/user/user-bot-trading.types'

export class UserRepository {
  // user
  private userRepo(): Repository<UserEntity> {
    return getRepository(UserEntity, ConnectionName.user_role)
  }

  async saveUser(params: RawUser): Promise<UserEntity> {
    if (params.email) {
      params.email = params.email.toLocaleLowerCase()
    }
    return this.userRepo().save(params)
  }

  async findOneUser(params: QueryUser): Promise<UserEntity> {
    const query = cleanObject(params)
    return this.userRepo().findOne(query)
  }
  async findUser(params: {
    email?: string
    keyword?: string
    is_admin?: boolean
    merchant_code?: string[]
  }): Promise<UserEntity[]> {
    let { keyword, is_admin, email, merchant_code } = params
    const whereArray = []
    if (is_admin) {
      whereArray.push(`is_admin = ${is_admin}`)
    }
    if (keyword) {
      keyword = keyword.toLowerCase()
      whereArray.push(`(email LIKE '%${keyword}%'
            OR lower(username) LIKE '%${keyword}%'
            OR phone LIKE '%${keyword}%'
            OR lower(first_name) LIKE '%${keyword}%'
            OR lower(last_name) LIKE '%${keyword}%')
            `)
    }
    if (email) {
      whereArray.push(`email = '${email}'`)
    }
    if (merchant_code && merchant_code.length > 0) {
      whereArray.push(
        `merchant_code in (${merchant_code.map((e) => `'${e}'`)})`
      )
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const queryUser = `
    SELECT  *
    FROM ${USERS_TABLE}  
    ${where}  
    ORDER BY created_at DESC`
    return this.userRepo().query(queryUser)
  }
  // user view
  private userViewRepo(): Repository<UserViewEntity> {
    return getRepository(UserViewEntity, ConnectionName.user_role)
  }
  async findOneUserView(params: QueryUser): Promise<RawUserView> {
    const query = cleanObject(params)
    return this.userViewRepo().findOne(query)
  }
  async findUserViewPaging(params: {
    is_admin?: boolean
    page?: number
    size?: number
    keyword?: string
    roles?: RawRole[]
  }) {
    let { page, size, keyword } = params
    const { is_admin, roles } = params
    page = Number(page || 1)
    size = Number(size || 50)
    const whereArray = []
    if (keyword) {
      keyword = keyword.toLowerCase()
      const keywords = keyword.split(' ')
      if (keywords.length > 1) {
        const last_name = keywords[keywords.length - 1]
        const first_name = keywords
          .filter((e, i) => i < keywords.length - 1)
          .join(' ')
        whereArray.push(`(email LIKE '%${keyword}%'
          OR lower(username) LIKE '%${keyword}%'
          OR phone LIKE '%${keyword}%'
          OR (lower(first_name) LIKE '%${first_name}%'
          AND lower(last_name) LIKE '%${last_name}%'))
          `)
      } else {
        whereArray.push(`(email LIKE '%${keyword}%'
        OR lower(username) LIKE '%${keyword}%'
        OR phone LIKE '%${keyword}%'
        OR lower(first_name) LIKE '%${keyword}%'
        OR lower(last_name) LIKE '%${keyword}%')
        `)
      }
    }
    if (is_admin) {
      whereArray.push(`is_admin = ${is_admin}`)
      const where =
        whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
      const queryUser = `
    SELECT * 
    FROM (
      SELECT 
      u.*,
      COALESCE(json_agg(DISTINCT ar.role_name) FILTER (WHERE ar.role_name IS NOT NULL), '[]') AS auth_roles
      FROM ${USERS_TABLE} u  
      left join auth_user_roles aur on aur.user_id = u.id 
      left join auth_roles ar on ar.id = aur.auth_role_id 
      group by u.id
    ) as utemp
    ${where}
    ORDER BY utemp.created_at DESC
    OFFSET ${(page - 1) * size}
    LIMIT ${size}
    `
      const queryCount = `
    SELECT COUNT(*)
    FROM (
      SELECT 
      u.*,
      COALESCE(json_agg(DISTINCT r.role_name) FILTER (WHERE r.role_name IS NOT NULL), '[]') AS roles
      FROM ${USERS_TABLE} u  
      left join user_roles ur on ur.user_id = u.id 
      left join roles r on r.id = ur.role_id 
      group by u.id
    ) as utemp
    ${where}
    `
      const [rows, total] = await Promise.all([
        this.userRepo().query(queryUser),
        this.userRepo().query(queryCount),
      ])
      return {
        rows,
        page,
        size,
        count: rows.length,
        total: Number(total[0].count),
      }
    } else {
      if (roles && roles.length > 0) {
        const whereRoles = roles.map(
          (r) => `utemp.roles::jsonb ? '${r.role_name}'`
        )
        whereArray.push(`(${whereRoles.join(' OR ')})`)
      }
      const where =
        whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
      const queryUser = `
      SELECT * 
      FROM (
        SELECT 
        u.*,
        COALESCE(json_agg(DISTINCT r.role_name) FILTER (WHERE r.role_name IS NOT NULL), '[]') AS roles
        FROM ${USERS_TABLE} u  
        left join user_roles ur on ur.user_id = u.id 
        left join roles r on r.id = ur.role_id 
        group by u.id
      ) as utemp
      ${where}
      ORDER BY utemp.created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
      `
      const queryCount = `
      SELECT COUNT(*)
      FROM (
        SELECT 
        u.*,
        COALESCE(json_agg(DISTINCT r.role_name) FILTER (WHERE r.role_name IS NOT NULL), '[]') AS roles
        FROM ${USERS_TABLE} u  
        left join user_roles ur on ur.user_id = u.id 
        left join roles r on r.id = ur.role_id 
        group by u.id
      ) as utemp
      ${where}
      `
      const [rows, total] = await Promise.all([
        this.userRepo().query(queryUser),
        this.userRepo().query(queryCount),
      ])
      return {
        rows,
        page,
        size,
        count: rows.length,
        total: Number(total[0].count),
      }
    }
  }
  async findUserPaging(param: {
    page?: number
    size?: number
    keyword?: string
    from?: number
    to?: number
    merchant_code?: string
    roles?: string[]
    bots?: string[]
  }) {
    let { page, size, keyword } = param
    const { from, to, merchant_code, roles, bots } = param
    page = Number(page || 1)
    size = Number(size || 50)
    const whereArray = []
    if (from) {
      whereArray.push(`created_at >= ${from}`)
    }
    if (to) {
      whereArray.push(`created_at <= ${to}`)
    }
    if (merchant_code) {
      whereArray.push(`merchant_code = '${merchant_code}'`)
    }
    if (keyword) {
      keyword = keyword.toLowerCase()
      const keywords = keyword.split(' ')
      if (keywords.length > 1) {
        const last_name = keywords[keywords.length - 1]
        const first_name = keywords
          .filter((e, i) => i < keywords.length - 1)
          .join(' ')
        whereArray.push(`(email LIKE '%${keyword}%'
            OR lower(username) LIKE '%${keyword}%'
            OR phone LIKE '%${keyword}%'
            OR (lower(first_name) LIKE '%${first_name}%'
            AND lower(last_name) LIKE '%${last_name}%'))
            `)
      } else {
        whereArray.push(`(email LIKE '%${keyword}%'
          OR lower(username) LIKE '%${keyword}%'
          OR phone LIKE '%${keyword}%'
          OR lower(first_name) LIKE '%${keyword}%'
          OR lower(last_name) LIKE '%${keyword}%')
          `)
      }
    }
    if (roles.length > 0) {
      const whereRoles = roles.map((name) => `roles::jsonb ? '${name}'`)
      const whereLogRoles = roles.map((name) => `log_roles::jsonb ? '${name}'`)
      whereArray.push(
        `(${whereRoles.join(' OR ')} OR ${whereLogRoles.join(' OR ')})`
      )
    }
    if (bots.length > 0) {
      const whereBots = bots.map((name) => `bots::jsonb ? '${name}'`)
      whereArray.push(`(${whereBots.join(' OR ')})`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const queryUser = `
      SELECT *
      FROM
      (SELECT u.id,
        u.email,
        u.username,
        u.phone,
        u.first_name,
        u.last_name,
        u.address,
        u.affiliate_code,
        u.link_affiliate,
        u.referral_code,
        u.profile_pic,
        u.active,
        u.email_confirmed,
        u.note_updated,
        u.date_registered,
        u.country,
        u.year_of_birth,
        u.gender,
        u.super_user,
        u.is_admin,
        u.phone_code,
        u.merchant_code,
        u.created_at,
        u.updated_at,
        COALESCE(json_agg(DISTINCT r.role_name) FILTER (WHERE r.role_name IS NOT NULL), '[]') AS roles,
        COALESCE(json_agg(DISTINCT ualr.name) FILTER (WHERE ualr.name IS NOT NULL), '[]') AS log_roles,
        COALESCE(json_agg(DISTINCT ualb.name) FILTER (WHERE ualb.name IS NOT NULL), '[]') AS bots
        FROM users as u
        LEFT JOIN user_roles ur on ur.user_id = u.id 
        LEFT JOIN roles r on r.id = ur.role_id
        LEFT JOIN user_asset_logs ualb on ualb.user_id = u.id and ualb.category = 'BOT'
        LEFT JOIN user_asset_logs ualr on ualr.user_id = u.id and ualr.category = 'PKG' and ualr.status != 'PROCESSING'
        GROUP BY u.id) as utemp
      ${where}  
      ORDER BY created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `
    const queryCount = `
    SELECT COUNT(*)
    FROM (SELECT u.*,
      COALESCE(json_agg(DISTINCT r.role_name) FILTER (WHERE r.role_name IS NOT NULL), '[]') AS roles,
      COALESCE(json_agg(DISTINCT ualr.name) FILTER (WHERE ualr.name IS NOT NULL), '[]') AS log_roles,
      COALESCE(json_agg(DISTINCT ualb.name) FILTER (WHERE ualb.name IS NOT NULL), '[]') AS bots
      FROM users as u
      LEFT JOIN user_roles ur on ur.user_id = u.id 
      LEFT JOIN roles r on r.id = ur.role_id 
      LEFT JOIN user_asset_logs ualb on ualb.user_id = u.id and ualb.category = 'BOT'
      LEFT JOIN user_asset_logs ualr on ualr.user_id = u.id and ualr.category = 'PKG' and ualr.status != 'PROCESSING'
      GROUP BY u.id) as utemp
    ${where} 
    `
    const [rows, total] = await Promise.all([
      this.userRepo().query(queryUser),
      this.userRepo().query(queryCount),
    ])
    return {
      rows,
      page,
      size,
      count: rows.length as number,
      total: Number(total[0].count),
    }
  }
  // verify token
  private verifyTokenRepo(): Repository<VerifyTokenEntity> {
    return getRepository(VerifyTokenEntity, ConnectionName.user_role)
  }

  async saveVerifyToken(params: RawVerifyToken): Promise<RawVerifyToken> {
    return this.verifyTokenRepo().save(params)
  }

  async findOneToken(params: QueryToken): Promise<VerifyTokenEntity> {
    const query = cleanObject(params)
    return this.verifyTokenRepo().findOne(query)
  }
  async findToken(params: QueryToken): Promise<VerifyTokenEntity[]> {
    const query = cleanObject(params)
    return this.verifyTokenRepo().find(query)
  }
  async deleteToken(params: DeleteToken) {
    const query = cleanObject(params)
    return this.verifyTokenRepo().delete(query)
  }

  // auth user role
  private authUserRoleRepo(): Repository<AuthUserRoleEntity> {
    return getRepository(AuthUserRoleEntity, ConnectionName.user_role)
  }

  async saveAuthUserRole(
    params: RawAuthUserRole[]
  ): Promise<RawAuthUserRole[]> {
    return this.authUserRoleRepo().save(params)
  }

  async findAuthUserRole(
    params: QueryAuthUserRole
  ): Promise<RawAuthUserRole[]> {
    return this.authUserRoleRepo().find({
      where: params,
      order: { created_at: 'DESC' },
    })
  }

  async deleteAuthUserRoleByIds(ids: string[]) {
    return this.authUserRoleRepo().delete(ids)
  }

  // user role
  private userRoleRepo(): Repository<UserRoleEntity> {
    return getRepository(UserRoleEntity, ConnectionName.user_role)
  }

  async saveUserRole(params: RawUserRole[], queryRunner?: QueryRunner) {
    if (queryRunner) {
      const transaction = await this.userRoleRepo().create(params)
      return queryRunner.manager.save(transaction)
    } else {
      return this.userRoleRepo().save(params)
    }
  }

  async findUserRole(params: QueryUserRole): Promise<RawUserRole[]> {
    return this.userRoleRepo().find({
      where: params,
      order: { created_at: 'DESC' },
    })
  }
  async findUserRoleByUserIdsAndRoleIds(
    userIds: string[],
    roleIds: string[]
  ): Promise<RawUserRole[]> {
    const whereArray = []
    if (userIds.length > 0) {
      whereArray.push(
        `user_id in (${userIds.map((user_id) => `'${user_id}'`)})`
      )
    }
    if (userIds.length > 0) {
      whereArray.push(
        `role_id in (${roleIds.map((role_id) => `'${role_id}'`)})`
      )
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
      SELECT *
      FROM ${USER_ROLE_TABLE}  
      ${where} 
      order by created_at DESC
    `
    return this.userRoleRepo().query(query)
  }
  async deleteUserRoleByIds(ids: string[]) {
    return this.userRoleRepo().delete(ids)
  }
  async deleteUserRoles(userIds: string[], roleIds: string[]) {
    const whereArray = []
    if (userIds.length > 0) {
      whereArray.push(
        `user_id in (${userIds.map((user_id) => `'${user_id}'`)})`
      )
    }
    if (userIds.length > 0) {
      whereArray.push(
        `role_id in (${roleIds.map((role_id) => `'${role_id}'`)})`
      )
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
      DELETE
      FROM ${USER_ROLE_TABLE}  
      ${where}
    `
    return this.userRoleRepo().query(query)
  }
  // session
  private sessionRepo(): Repository<SessionEntity> {
    return getRepository(SessionEntity, ConnectionName.user_role)
  }
  async saveSession(params: RawSession) {
    return this.sessionRepo().save(params)
  }
  async findSessionById(id: string) {
    return this.sessionRepo().findOne(id)
  }
  findLastSession(params: QuerySession) {
    return this.sessionRepo().findOne({
      where: params,
      order: { created_at: -1 },
    })
  }
  findSessionLogin(params: QuerySession, limit: number): Promise<RawSession[]> {
    const paramsClear = cleanObject(params)
    const whereArray = formatQueryArray(paramsClear)
    whereArray.push('websocket_id is null')
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
      SELECT *
      FROM ${SESSIONS_TABLE}  
      ${where} 
      order by created_at DESC
      limit ${limit}
    `
    return this.sessionRepo().query(query)
  }
  findSessionWS(params: QuerySession, limit: number): Promise<RawSession[]> {
    const paramsClear = cleanObject(params)
    const whereArray = formatQueryArray(paramsClear)
    whereArray.push('websocket_id is not null')
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
      SELECT *
      FROM ${SESSIONS_TABLE}  
      ${where} 
      order by created_at DESC
      limit ${limit}
    `
    return this.sessionRepo().query(query)
  }

  // user log
  private userLogRepo(): Repository<UserLogEntity> {
    return getRepository(UserLogEntity, ConnectionName.user_role)
  }
  async saveUserLog(params: RawUserLog) {
    return this.userLogRepo().save(params)
  }
  async findUserLog(params: QueryUserLog) {
    const query = cleanObject(params)
    return this.sessionRepo().find({ where: query, order: { created_at: -1 } })
  }

  // user bot
  private userBotRepo(): Repository<UserBotEntity> {
    return getRepository(UserBotEntity, ConnectionName.user_role)
  }
  async saveUserBot(params: RawUserBot[], queryRunner?: QueryRunner) {
    if (queryRunner) {
      const transaction = await this.userBotRepo().create(params)
      return queryRunner.manager.save(transaction)
    } else {
      return this.userBotRepo().save(params)
    }
  }
  async deleteUserBot(params: { user_id: string; bot_id: string }) {
    const { bot_id, user_id } = params
    return this.userBotRepo().delete({ user_id, bot_id })
  }
  async findUserBot(params: QueryUserBot): Promise<RawUserBot[]> {
    return this.userBotRepo().find({
      where: params,
      order: { created_at: 'DESC' },
    })
  }
  async findUserBotSQL(params: {
    user_id?: string
    bot_ids?: string[]
    status?: ITEM_STATUS
    expires_at?: number
  }): Promise<RawUserBot[]> {
    const { user_id, bot_ids, status, expires_at } = params
    const whereArray = []
    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`)
    }
    if (status) {
      whereArray.push(`status = '${status}'`)
    }
    if (expires_at) {
      whereArray.push(`expires_at > ${expires_at}`)
    }
    if (bot_ids) {
      whereArray.push(`bot_id in (${bot_ids.map((e) => `'${e}'`)})`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
    SELECT *
    FROM ${USER_BOT_TABLE}  
    ${where} 
    order by created_at DESC
  `
    return this.userBotRepo().query(query)
  }
  // user asset log
  private userAssetLogRepo(): Repository<UserAssetLogEntity> {
    return getRepository(UserAssetLogEntity, ConnectionName.user_role)
  }
  async saveUserAssetLog(params: UserAssetLogEntity[], queryRunner?: QueryRunner) {
    if (queryRunner) {
      const transaction = await this.userAssetLogRepo().create(params)
      return queryRunner.manager.save(transaction)
    } else {
      return this.userAssetLogRepo().save(params)
    }
  }

  async findUserAssetLog(
    params: QueryUserAssetLog
  ): Promise<RawUserAssetLog[]> {
    return this.userAssetLogRepo().find({
      where: params,
      order: { created_at: 'DESC' },
    })
  }
  async findUserAssetLogPaging(params: {
    page?: number
    size?: number
    status?: string[]
    category?: ORDER_CATEGORY
    keyword?: string
    user_id?: string
    is_order?: boolean
  }) {
    let { page, size, status, category, keyword, user_id, is_order } = params
    page = Number(page || 1)
    size = Number(size || 50)

    const whereArray = []
    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`)
    }
    if (status && status.length > 0) {
      whereArray.push(`status in (${status.map((e) => `'${e}'`)})`)
    }
    if (category) {
      whereArray.push(`category = '${category}'`)
    }
    if (keyword) {
      whereArray.push(`(name LIKE '%${keyword}%'
        OR order_id LIKE '%${keyword}%')`)
    }
    if (is_order) {
      whereArray.push(`order_id is not null`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const queryAsset = `
      SELECT  *
      FROM ${USER_ASSET_LOG_TABLE}  
      ${where}  
      ORDER BY updated_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `
    const queryCount = `
    SELECT COUNT(*)
    FROM ${USER_ASSET_LOG_TABLE}  
    ${where}
    `
    const [rows, total] = await Promise.all([
      this.userAssetLogRepo().query(queryAsset),
      this.userAssetLogRepo().query(queryCount),
    ])
    return {
      rows,
      page,
      size,
      count: rows.length,
      total: Number(total[0].count),
    }
  }
  async getAllOfUserTradingBots(param: {
    user_id?: string
  }): Promise<{ user_id: string; asset_id: string }[]> {
    const { user_id } = param
    const whereArray = []
    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`)
    }
    whereArray.push(`status = '${ITEM_STATUS.NOT_CONNECT}'`)
    whereArray.push(`category = '${ORDER_CATEGORY.TBOT}'`)
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
    SELECT user_id, asset_id
    FROM ${USER_ASSET_LOG_TABLE} 
    ${where}
    group by user_id, asset_id
    `
    return this.userAssetLogRepo().query(query)
  }

  // user bot trading
  private userBotTradingRepo(): Repository<UserBotTradingEntity> {
    return getRepository(UserBotTradingEntity, ConnectionName.user_role)
  }
  async saveUserBotTrading(
    params: RawUserBotTrading[],
    queryRunner?: QueryRunner
  ) {
    if (queryRunner) {
      const transaction = await this.userBotTradingRepo().create(params)
      return queryRunner.manager.save(transaction)
    } else {
      return this.userBotTradingRepo().save(params)
    }
  }
  async deleteUserBotTrading(params: RawUserBotTrading) {
    return this.userBotTradingRepo().delete(params)
  }
  async findUserBotTrading(
    params: UserBotTradingEntity
  ): Promise<UserBotTradingEntity[]> {
    return this.userBotTradingRepo().find({
      where: params,
      order: { created_at: 'DESC' },
    })
  }
  async findUserBotTradingSQL(params: {
    user_id?: string
    bot_ids?: string[]
    status?: ITEM_STATUS[]
    expires_at?: number
    expired?: number
    type?: TBOT_TYPE
  }): Promise<RawUserBotTrading[]> {
    const { user_id, bot_ids, status, expires_at, expired, type } = params
    const whereArray = []
    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`)
    }
    if (status && status.length > 0) {
      whereArray.push(`status in (${status.map((e) => `'${e}'`)})`)
    }
    if (expires_at) {
      whereArray.push(`expires_at > ${expires_at}`)
    }
    if (expired) {
      whereArray.push(`expires_at < ${expired}`)
      whereArray.push(`status != '${ITEM_STATUS.EXPIRED}'`)
    }
    if (bot_ids && bot_ids.length > 0) {
      whereArray.push(`bot_id in (${bot_ids.map((e) => `'${e}'`)})`)
    }
    if (type) {
      whereArray.push(`type = '${type}'`)
    }

    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
      SELECT *
      FROM ${USER_BOT_TRADING_TABLE}  
      ${where} 
      order by created_at DESC
    `
    return this.userBotTradingRepo().query(query)
  }
}
