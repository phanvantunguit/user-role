import {
  Entity,
  Column,
  Index,
  PrimaryColumn,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
} from 'typeorm'

export const SESSIONS_TABLE = 'sessions'
@Entity(SESSIONS_TABLE)
export class SessionEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string

  @Column({ type: 'uuid' })
  @Index()
  user_id: string

  @Column({ type: 'varchar', nullable: true  })
  token: string

  @Column({ type: 'varchar', nullable: true  })
  name_device: string

  @Column({ type: 'varchar', nullable: true  })
  browser: string

  @Column({ type: 'varchar', nullable: true  })
  ip_number: string

  @Column({ type: 'bigint', nullable: true  })
  last_login: number

  @Column({ type: 'bigint', nullable: true  })
  expires_at: number

  @Column({ type: 'boolean', nullable: true  })
  enabled: boolean

  @Column({ type: 'varchar', nullable: true })
  websocket_id: string

  @Column({ type: 'varchar', nullable: true })
  token_id: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number
}
@EventSubscriber()
export class SessionEntitySubscriber
  implements EntitySubscriberInterface<SessionEntity>
{
  async beforeInsert(event: InsertEvent<SessionEntity>) {
    event.entity.created_at = Date.now()
  }
}
