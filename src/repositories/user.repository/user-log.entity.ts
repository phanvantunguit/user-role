import {
  Entity,
  Column,
  Index,
  PrimaryColumn,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
} from 'typeorm'

export const USER_LOGS_TABLE = 'user_logs'
@Entity(USER_LOGS_TABLE)
export class UserLogEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string

  @Column({ type: 'uuid' })
  @Index()
  user_id: string

  @Column({ type: 'varchar' })
  ip_number: string

  @Column({ type: 'varchar' })
  browser_type: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number
}
@EventSubscriber()
export class UserLogEntitySubscriber
  implements EntitySubscriberInterface<UserLogEntity>
{
  async beforeInsert(event: InsertEvent<UserLogEntity>) {
    event.entity.created_at = Date.now()
  }
}
