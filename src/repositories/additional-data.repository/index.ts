import { ConnectionName } from 'src/const/app-setting'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import {
  ADDITIONAL_DATA_TABLE,
  AdditionalDataEntity,
} from './additional-data.entity'
import { ADDITIONAL_DATA_TYPE } from 'src/domains/merchant/types'

export class AdditionalDataRepository extends BaseRepository<
  AdditionalDataEntity,
  AdditionalDataEntity
> {
  repo(): Repository<AdditionalDataEntity> {
    return getRepository(AdditionalDataEntity, ConnectionName.user_role)
  }
  list(name?: string, type?: ADDITIONAL_DATA_TYPE) {
    const whereArray = []
    if (name) {
      whereArray.push(`(name = '${name}')`)
    }
    if (type) {
      whereArray.push(`type = '${type}'`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
    SELECT *
    FROM ${ADDITIONAL_DATA_TABLE}
    ${where} 
    order by created_at DESC
  `
    return this.repo().query(query)
  }
}
