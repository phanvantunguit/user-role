import {
    Entity,
    Column,
    PrimaryColumn,
    Generated,
    EntitySubscriberInterface,
    EventSubscriber,
    InsertEvent,
    UpdateEvent,
  } from 'typeorm';
  
  export const ADDITIONAL_DATA_TABLE = 'additional_data';
  @Entity(ADDITIONAL_DATA_TABLE, { synchronize: false })
  export class AdditionalDataEntity {
    @PrimaryColumn()
    @Generated('uuid')
    id?: string;
  
    @Column({ type: 'varchar' })
    type?: string;
  
    @Column({ type: 'varchar' })
    name?: string;
  
    @Column({ type: 'jsonb', default: {} })
    data?: any;
  
    @Column({ type: 'bigint', default: Date.now() })
    created_at?: number;
  
    @Column({ type: 'bigint', default: Date.now() })
    updated_at?: number;
  }
  
  @EventSubscriber()
  export class AdditionalDataEntitySubscriber
    implements EntitySubscriberInterface<AdditionalDataEntity>
  {
    async beforeInsert(event: InsertEvent<AdditionalDataEntity>) {
      event.entity.created_at = Date.now();
      event.entity.updated_at = Date.now();
    }
  
    async beforeUpdate(event: UpdateEvent<AdditionalDataEntity>) {
      event.entity.updated_at = Date.now();
    }
  }
  