import { ConnectionName } from 'src/const/app-setting'

import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import { BotSignalEntity, BOT_SIGNAL_TABLE } from './bot-signal.entity'
import { FavoriteSignalEntity } from './favorite-signal.entity'

export class BotSignalRepository extends BaseRepository<
  BotSignalEntity,
  BotSignalEntity
> {
  repo(): Repository<BotSignalEntity> {
    return getRepository(BotSignalEntity, ConnectionName.user_role)
  }
  favoriteSignalRepo(): Repository<FavoriteSignalEntity> {
    return getRepository(FavoriteSignalEntity, ConnectionName.user_role)
  }
  async getPaging(params: {
    user_id: string
    page?: number
    size?: number
    exchanges?: string[]
    symbols?: string[]
    names?: string[]
    resolutions?: string[]
    types?: string[]
    from?: number
    to?: number
  }) {
    let { page, size } = params
    const { exchanges, symbols, from, to, names, resolutions, types, user_id } =
      params
    page = Number(page || 1)
    size = Number(size || 50)
    const whereArray = []
    if (from) {
      whereArray.push(`time >= ${from}`)
    }
    if (to) {
      whereArray.push(`time <= ${to}`)
    }
    if (names && names.length > 0) {
      whereArray.push(`name in (${names.map((e) => `'${e}'`)})`)
    }
    if (exchanges && exchanges.length > 0) {
      whereArray.push(`exchange in (${exchanges.map((e) => `'${e}'`)})`)
    }
    if (symbols && symbols.length > 0) {
      whereArray.push(`symbol in (${symbols.map((e) => `'${e}'`)})`)
    }
    if (resolutions && resolutions.length > 0) {
      whereArray.push(`resolution in (${resolutions.map((e) => `'${e}'`)})`)
    }
    if (types && types.length > 0) {
      whereArray.push(`type in (${types.map((e) => `'${e}'`)})`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const queryTransaction = `
    select ubs.*
    , ufs.favorite
    from (
      SELECT  *
      FROM ${BOT_SIGNAL_TABLE}  
      ${where}  
      ORDER BY created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    ) as ubs
    left join (
    select bot_signal_id, true as favorite from favorite_signal fs2
    where fs2.user_id = '${user_id}'
    ) ufs on ufs.bot_signal_id = ubs.id 
    `
    const queryCount = `
    SELECT COUNT(*)
    FROM ${BOT_SIGNAL_TABLE}  
    ${where}
    `
    const [rows, total] = await Promise.all([
      this.repo().query(queryTransaction),
      this.repo().query(queryCount),
    ])
    return {
      rows,
      page,
      size,
      count: rows.length as number,
      total: Number(total[0].count),
    }
  }
  async getFavoritePaging(params: {
    user_id: string
    page?: number
    size?: number
    exchanges?: string[]
    symbols?: string[]
    names?: string[]
    resolutions?: string[]
    types?: string[]
    from?: number
    to?: number
  }) {
    let { page, size } = params
    const { exchanges, symbols, from, to, names, resolutions, types, user_id } =
      params
    page = Number(page || 1)
    size = Number(size || 50)
    const whereArray = []
    if (from) {
      whereArray.push(`time >= ${from}`)
    }
    if (to) {
      whereArray.push(`time <= ${to}`)
    }
    if (names && names.length > 0) {
      whereArray.push(`name in (${names.map((e) => `'${e}'`)})`)
    }
    if (exchanges && exchanges.length > 0) {
      whereArray.push(`exchange in (${exchanges.map((e) => `'${e}'`)})`)
    }
    if (symbols && symbols.length > 0) {
      whereArray.push(`symbol in (${symbols.map((e) => `'${e}'`)})`)
    }
    if (resolutions && resolutions.length > 0) {
      whereArray.push(`resolution in (${resolutions.map((e) => `'${e}'`)})`)
    }
    if (types && types.length > 0) {
      whereArray.push(`type in (${types.map((e) => `'${e}'`)})`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const queryTransaction = `
    select bs.*
	  ,ufs.favorite
    FROM ${BOT_SIGNAL_TABLE} as bs
    right join (
    select bot_signal_id, true as favorite from favorite_signal fs2
    where fs2.user_id = '${user_id}'
    ) ufs on ufs.bot_signal_id = bs.id 
    ${where}  
      ORDER BY created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `
    const queryCount = `
    SELECT COUNT(*)
    FROM ${BOT_SIGNAL_TABLE} as bs
    right join (
    select bot_signal_id, true as favorite from favorite_signal fs2
    where fs2.user_id = '${user_id}'
    ) ufs on ufs.bot_signal_id = bs.id 
    ${where} 
    `
    const [rows, total] = await Promise.all([
      this.repo().query(queryTransaction),
      this.repo().query(queryCount),
    ])
    return {
      rows,
      page,
      size,
      count: rows.length as number,
      total: Number(total[0].count),
    }
  }
  async addFavoriteSignal(params: { user_id: string; bot_signal_id: string }) {
    return this.favoriteSignalRepo().save({
      user_id: params.user_id,
      bot_signal_id: params.bot_signal_id,
    })
  }
  async removeFavoriteSignal(params: {
    user_id: string
    bot_signal_id: string
  }) {
    return this.favoriteSignalRepo().delete({
      user_id: params.user_id,
      bot_signal_id: params.bot_signal_id,
    })
  }
  async findFavoriteSignal(params: { user_id: string; bot_signal_id: string }) {
    return this.favoriteSignalRepo().findOne({
      user_id: params.user_id,
      bot_signal_id: params.bot_signal_id,
    })
  }
}
