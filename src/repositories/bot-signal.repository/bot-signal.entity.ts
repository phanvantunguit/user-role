import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent,
} from 'typeorm'

export const BOT_SIGNAL_TABLE = 'bot_signal'
@Entity(BOT_SIGNAL_TABLE)
export class BotSignalEntity {
  @PrimaryColumn('varchar')
  id?: string

  @Column({ type: 'varchar', nullable: true })
  name?: string

  @Column({ type: 'varchar', nullable: true })
  resolution?: string

  @Column({ type: 'varchar', unique: true })
  signal_id?: string

  @Column({ type: 'bigint', nullable: true })
  time?: number

  @Column({ type: 'varchar', nullable: true })
  type?: string

  @Column({ type: 'varchar', nullable: true })
  image_url?: string

  @Column({ type: 'varchar', nullable: true })
  exchange?: string

  @Column({ type: 'varchar', nullable: true })
  symbol?: string

  @Column({ type: 'jsonb', default: {} })
  metadata?: any

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number
}

@EventSubscriber()
export class BotSignalEntitySubscriber
  implements EntitySubscriberInterface<BotSignalEntity>
{
  async beforeInsert(event: InsertEvent<BotSignalEntity>) {
    event.entity.created_at = Date.now()
  }
}
