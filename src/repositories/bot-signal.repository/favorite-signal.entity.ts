import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent,
  Unique,
} from 'typeorm'

export const FAVORITE_SIGNAL_TABLE = 'favorite_signal'
@Entity(FAVORITE_SIGNAL_TABLE)
@Unique('bot_signal_id_user_id_un', ['bot_signal_id', 'user_id'])
export class FavoriteSignalEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string

  @Column({ type: 'varchar' })
  bot_signal_id?: string

  @Column({ type: 'uuid' })
  user_id?: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number
}

@EventSubscriber()
export class FavoriteSignalEntitySubscriber
  implements EntitySubscriberInterface<FavoriteSignalEntity>
{
  async beforeInsert(event: InsertEvent<FavoriteSignalEntity>) {
    event.entity.created_at = Date.now()
  }
}
