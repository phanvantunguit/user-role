import { Injectable, Inject, CACHE_MANAGER } from '@nestjs/common'
import { Cache } from 'cache-manager'
import { SERVICE_NAME } from 'src/const/app-setting'
import { WS_EVENT } from 'src/const/response'

@Injectable()
export class CacheRepository {
  client
  publisher
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {
    this.client = cacheManager.store.getClient()
    this.publisher = this.client.duplicate()
    this.publisher.on('error', (error) => {
      console.log('CacheRepository error', error)
    })
  }

  async setKey(key: string, item: string, expiresIn = 3600) {
    if (!this.client.connected) {
      return false
    }
    await this.cacheManager.set(`${SERVICE_NAME}:${key}`, item, {
      ttl: expiresIn,
    })
  }

  async getKey(key: string) {
    if (!this.client.connected) {
      return false
    }
    const value = await this.cacheManager.get(`${SERVICE_NAME}:${key}`)
    return value
  }

  async publish(channel: string, event: string, data: any, user_id?: string) {
    this.publisher.publish(
      channel,
      JSON.stringify({
        event: WS_EVENT.app_message,
        data: {
          sub_event: event,
          sub_data: data,
          user_id,
        },
      })
    )
  }
}
