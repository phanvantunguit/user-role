import { ConnectionName } from 'src/const/app-setting'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import {
  MERCHANT_COMMISSION_TABLE,
  MerchantCommissionEntity,
} from './merchant-commission.entity'

export class MerchantCommissionRepository extends BaseRepository<
  MerchantCommissionEntity,
  MerchantCommissionEntity
> {
  repo(): Repository<MerchantCommissionEntity> {
    return getRepository(MerchantCommissionEntity, ConnectionName.user_role)
  }
}
