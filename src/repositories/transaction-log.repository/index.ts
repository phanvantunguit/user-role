import { ConnectionName } from 'src/const/app-setting'
import {
  QueryTransactionLog,
  RawTransactionLog,
} from 'src/domains/transaction/transaction-log.types'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import { TransactionLogEntity } from './transaction-log.entity'

export class TransactionLogRepository extends BaseRepository<
  QueryTransactionLog,
  RawTransactionLog
> {
  repo(): Repository<TransactionLogEntity> {
    return getRepository(TransactionLogEntity, ConnectionName.user_role)
  }
}
