import { ConnectionName } from 'src/const/app-setting'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import {
  BotTradingHistoryEntity,
  BOT_TRADING_HISTORY_TABLE,
} from './bot-trading-history.entity'

export class BotTradingHistoryRepository extends BaseRepository<
  BotTradingHistoryEntity,
  BotTradingHistoryEntity
> {
  repo(): Repository<BotTradingHistoryEntity> {
    return getRepository(BotTradingHistoryEntity, ConnectionName.user_role)
  }
  listConditionMultiple(params: {
    status?: string[]
    connected_at?: number
  }): Promise<BotTradingHistoryEntity[]> {
    const { status, connected_at } = params
    const whereArray = []
    if (connected_at) {
      whereArray.push(`created_at >= ${connected_at}`)
    }
    if (status.length > 0) {
      whereArray.push(`status in (${status.map((e) => `'${e}'`)})`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
      SELECT *
      FROM ${BOT_TRADING_HISTORY_TABLE}  
      ${where} 
      order by created_at DESC
    `
    return this.repo().query(query)
  }
  async getPaging(params: {
    from?: number
    to?: number
    page?: number
    size?: number
    status?: string
    user_id?: string
    bot_id?: string
    subscriber_id?: string
    connected_at?: number
    bot_code?: string
  }) {
    let {
      page,
      size,
      status,
      user_id,
      from,
      to,
      subscriber_id,
      bot_id,
      bot_code,
      connected_at,
    } = params
    page = Number(page || 1)
    size = Number(size || 50)
    const whereArray = []
    if (from) {
      whereArray.push(`created_at >= ${from}`)
    }
    if (to) {
      whereArray.push(`created_at <= ${to}`)
    }

    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`)
    } else {
      whereArray.push(`user_id is null`)
    }
    if (subscriber_id) {
      whereArray.push(`subscriber_id = '${subscriber_id}'`)
    }
    if (bot_id) {
      whereArray.push(`bot_id = '${bot_id}'`)
    } else {
      whereArray.push(`bot_id is null`)
    }
    if (status) {
      whereArray.push(`status = '${status}'`)
    }
    if (connected_at) {
      whereArray.push(`created_at >= ${connected_at}`)
    }

    if (bot_code) {
      whereArray.push(`bot_code = '${bot_code}'`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const queryTransaction = `
        SELECT  *
        FROM ${BOT_TRADING_HISTORY_TABLE}  
        ${where}  
        ORDER BY day_started DESC
        OFFSET ${(page - 1) * size}
        LIMIT ${size}
    `
    const queryCount = `
    SELECT COUNT(*)
    FROM ${BOT_TRADING_HISTORY_TABLE}  
    ${where}
    `
    const [rows, total] = await Promise.all([
      this.repo().query(queryTransaction),
      this.repo().query(queryCount),
    ])
    return {
      rows,
      page,
      size,
      count: rows.length,
      total: Number(total[0].count),
    }
  }
  async deleteDataWithoutUser(params: { bot_id }) {
    const { bot_id } = params
    const query = `
    DELETE
    FROM ${BOT_TRADING_HISTORY_TABLE}  
    WHERE bot_id = '${bot_id}' and user_id is null
  `
    return this.repo().query(query)
  }

  async chartPNL(param: {
    from?: number
    to?: number
    bot_id: string
    user_id?: string
    timezone: string
    time_type: 'DAY' | 'MONTH'
    subscriber_id?: string
    connected_at?: number
  }) {
    const {
      from,
      to,
      bot_id,
      user_id,
      timezone,
      time_type,
      subscriber_id,
      connected_at,
    } = param
    const whereArray = []
    if (from) {
      whereArray.push(`day_started >= ${from}`)
    }
    if (to) {
      whereArray.push(`day_started <= ${to}`)
    }
    if (bot_id) {
      whereArray.push(`bot_id = '${bot_id}'`)
    }
    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`)
    } else {
      whereArray.push(`user_id is null`)
    }
    if (subscriber_id) {
      whereArray.push(`subscriber_id = '${subscriber_id}'`)
    }
    if (connected_at) {
      whereArray.push(`created_at >= ${connected_at}`)
    }

    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const timeFormat = time_type === 'DAY' ? 'YYYY-MM-DD' : 'YYYY-MM'
    const queryChart = `
    SELECT to_char(to_timestamp(day_started / 1000) AT TIME ZONE '${timezone}', '${timeFormat}') as time, 
    sum(profit_cash::float) as profit_cash
    from ${BOT_TRADING_HISTORY_TABLE} bth 
    ${where}
    GROUP BY time
    ORDER BY time ASC
    `
    return this.repo().query(queryChart)
  }
  async getProfit(param: {
    from?: number
    to?: number
    bot_id: string
    user_id: string
    subscriber_id?: string
    connected_at?: number
  }) {
    const { from, to, bot_id, user_id, subscriber_id, connected_at } = param
    const whereArray = []
    if (from) {
      whereArray.push(`day_started >= ${from}`)
    }
    if (to) {
      whereArray.push(`day_started <= ${to}`)
    }
    if (bot_id) {
      whereArray.push(`bot_id = '${bot_id}'`)
    }
    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`)
    } else {
      whereArray.push(`user_id is null`)
    }
    if (subscriber_id) {
      whereArray.push(`subscriber_id = '${subscriber_id}'`)
    }
    if (connected_at) {
      whereArray.push(`created_at >= ${connected_at}`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const queryProfit = `
    SELECT sum(profit_cash::float) as total
    from bot_tradings_history
    ${where}
    `
    return this.repo().query(queryProfit)
  }

  deleteDataOfUserBot(param: { user_id: string; bot_id: string }) {
    const { user_id, bot_id } = param
    const query = `
    DELETE
    FROM ${BOT_TRADING_HISTORY_TABLE}  
    WHERE bot_id = '${bot_id}' and user_id = '${user_id}'
  `
    return this.repo().query(query)
  }
  async count(param: {
    user_id?: string
    bot_id?: string
    connected_at?: number
  }): Promise<number> {
    const { bot_id, user_id, connected_at } = param
    const whereArray = []
    if (bot_id) {
      whereArray.push(`bot_id = '${bot_id}'`)
    }
    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`)
    } else {
      whereArray.push(`user_id is null`)
    }
    if (connected_at) {
      whereArray.push(`created_at >= ${connected_at}`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const query = `
    SELECT COUNT(*) as count
    FROM ${BOT_TRADING_HISTORY_TABLE}  
    ${where}
    `
    const total = await this.repo().query(query)
    return Number(total[0].count)
  }
}
