import { v4 } from 'uuid'
import {
  Entity,
  Column,
  PrimaryColumn,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
  Index,
} from 'typeorm'

export const BOT_TRADING_HISTORY_TABLE = 'bot_tradings_history'
@Entity(BOT_TRADING_HISTORY_TABLE)
export class BotTradingHistoryEntity {
  @PrimaryColumn({ type: 'varchar' })
  id?: string

  @Column({ type: 'uuid', nullable: true })
  @Index()
  user_id?: string

  @Column({ type: 'varchar', nullable: true })
  subscriber_id?: string

  @Column({ type: 'varchar', nullable: true })
  broker_account?: string

  @Column({ type: 'uuid', nullable: true })
  bot_id?: string

  @Column({ type: 'varchar', nullable: true })
  bot_code?: string

  @Column({ type: 'uuid', nullable: true })
  owner_created?: string

  @Column({ type: 'varchar' })
  token_first?: string
  @Column({ type: 'varchar', nullable: true })
  token_second?: string

  @Column({ type: 'varchar', nullable: true })
  status?: string

  @Column({ type: 'varchar', nullable: true })
  side?: string

  @Column({ type: 'varchar', nullable: true })
  entry_price?: string

  @Column({ type: 'varchar', nullable: true })
  close_price?: string

  @Column({ type: 'varchar', nullable: true })
  profit?: string

  @Column({ type: 'float4', nullable: true })
  profit_cash?: number

  @Column({ type: 'float4', nullable: true })
  commission?: number;

  @Column({ type: 'float4', nullable: true })
  swap?: number;

  @Column({ type: 'float4', nullable: true })
  volume?: number

  @Column({ type: 'bigint', nullable: true })
  day_started?: number;

  @Column({ type: 'bigint', nullable: true })
  day_completed?: number;

  @Column({ type: 'varchar', nullable: true })
  broker_comment?: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number
}

@EventSubscriber()
export class BotTradingHistoryEntitySubscriber
  implements EntitySubscriberInterface<BotTradingHistoryEntity>
{
  async beforeInsert(event: InsertEvent<BotTradingHistoryEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
    if (!event.entity.id) {
      event.entity.id = v4()
    }
  }
  async beforeUpdate(event: UpdateEvent<BotTradingHistoryEntity>) {
    event.entity.updated_at = Date.now()
  }
}
