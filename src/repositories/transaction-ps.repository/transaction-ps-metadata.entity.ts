import {
  Entity,
  Column,
  Index,
  PrimaryColumn,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
} from 'typeorm'

export const TRANSACTION_PS_METADATA_TABLE = 'transaction_metadata'
@Entity(TRANSACTION_PS_METADATA_TABLE)
export class TransactionPSMetadataEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string

  @Column({ type: 'uuid' })
  @Index()
  transaction_id?: string

  @Column({ type: 'varchar' })
  attribute?: string

  @Column({ type: 'varchar' })
  value?: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number
}
@EventSubscriber()
export class TransactionPSMetadataEntitySubscriber
  implements EntitySubscriberInterface<TransactionPSMetadataEntity>
{
  async beforeInsert(event: InsertEvent<TransactionPSMetadataEntity>) {
    event.entity.created_at = Date.now()
  }
}
