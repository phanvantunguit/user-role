import {
  Entity,
  Column,
  Index,
  PrimaryColumn,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
} from 'typeorm'

export const TRANSACTION_PS_LOGS_TABLE = 'transaction_logs'
@Entity(TRANSACTION_PS_LOGS_TABLE)
export class TransactionPSLogEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string

  @Column({ type: 'uuid' })
  @Index()
  transaction_id?: string

  @Column({ type: 'varchar' })
  transaction_event?: string

  @Column({ type: 'varchar' })
  transaction_status?: string

  @Column({ type: 'jsonb', default: {} })
  metadata?: any

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number
}
@EventSubscriber()
export class TransactionPSLogEntitySubscriber
  implements EntitySubscriberInterface<TransactionPSLogEntity>
{
  async beforeInsert(event: InsertEvent<TransactionPSLogEntity>) {
    event.entity.created_at = Date.now()
  }
}
