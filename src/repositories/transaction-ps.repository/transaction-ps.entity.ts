import { PAYMENT_METHOD, TRANSACTION_STATUS } from 'src/const/transaction'
import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  UpdateEvent,
  Unique,
} from 'typeorm'

export const TRANSACTION_PS_TABLE = 'transactions'
@Entity(TRANSACTION_PS_TABLE)
@Unique('service_order_un', ['integrate_service', 'order_id'])
export class TransactionPSEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string

  @Column({ type: 'varchar' })
  integrate_service?: string

  @Column({ type: 'varchar' })
  order_id?: string

  @Column({ type: 'varchar', nullable: true })
  order_type?: string

  @Column({ type: 'varchar', nullable: true })
  user_id?: string

  @Column({ type: 'varchar', nullable: true })
  fullname?: string

  @Column({ type: 'varchar', nullable: true })
  email?: string

  @Column({ type: 'varchar' })
  amount?: string

  @Column({ type: 'float4', default: 0 })
  commission_cash?: string

  @Column({ type: 'varchar' })
  currency?: string

  @Column({ type: 'varchar', nullable: true })
  payment_id?: string

  @Column({ type: 'varchar' })
  payment_method?: PAYMENT_METHOD

  @Column({ type: 'varchar' })
  status?: TRANSACTION_STATUS

  @Column({ type: 'varchar', nullable: true })
  ipn_url?: string

  @Column({ type: 'varchar', nullable: true, default: 'CM' })
  merchant_code?: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number
}

@EventSubscriber()
export class TransactionPSEntitySubscriber
  implements EntitySubscriberInterface<TransactionPSEntity>
{
  async beforeInsert(event: InsertEvent<TransactionPSEntity>) {
    event.entity.created_at = Date.now()
    event.entity.updated_at = Date.now()
  }

  async beforeUpdate(event: UpdateEvent<TransactionPSEntity>) {
    if (!event.entity.deleted_at) {
      event.entity.updated_at = Date.now()
    }
  }
}
