import { ConnectionName } from 'src/const/app-setting'
import { TRANSACTION_STATUS } from 'src/const/transaction'
import { Repository, getRepository } from 'typeorm'
import { BaseRepository } from '../base.repository/base.repository'
import { TransactionPSLogEntity } from './transaction-ps-log.entity'
import { TransactionPSMetadataEntity } from './transaction-ps-metadata.entity'
import {
  TransactionPSEntity,
  TRANSACTION_PS_TABLE,
} from './transaction-ps.entity'

export class TransactionPSRepository extends BaseRepository<
  TransactionPSEntity,
  TransactionPSEntity
> {
  repo(): Repository<TransactionPSEntity> {
    return getRepository(TransactionPSEntity, ConnectionName.payment_service)
  }
  metadataRepo(): Repository<TransactionPSMetadataEntity> {
    return getRepository(
      TransactionPSMetadataEntity,
      ConnectionName.payment_service
    )
  }
  logRepo(): Repository<TransactionPSLogEntity> {
    return getRepository(TransactionPSLogEntity, ConnectionName.payment_service)
  }

  // async getPaging(params: {
  //   page?: number
  //   size?: number
  //   keyword?: string
  //   status?: TRANSACTION_STATUS
  //   from?: number
  //   to?: number
  // }) {
  //   let { page, size } = params
  //   const { keyword, status, from, to } = params
  //   page = Number(page || 1)
  //   size = Number(size || 50)
  //   const whereArray = []
  //   if (from) {
  //     whereArray.push(`created_at >= ${from}`)
  //   }
  //   if (to) {
  //     whereArray.push(`created_at <= ${to}`)
  //   }

  //   if (status) {
  //     whereArray.push(`status = '${status}'`)
  //   }
  //   if (keyword) {
  //     whereArray.push(`(email LIKE '%${keyword}%'
  //         OR fullname LIKE '%${keyword}%'
  //         OR order_id LIKE '%${keyword}%'
  //         OR payment_id LIKE '%${keyword}%')`)
  //   }
  //   const where =
  //     whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
  //   const queryTransaction = `
  //       SELECT  *
  //       FROM ${TRANSACTION_PS_TABLE}
  //       ${where}
  //       ORDER BY created_at DESC
  //       OFFSET ${(page - 1) * size}
  //       LIMIT ${size}
  //   `
  //   const queryCount = `
  //   SELECT COUNT(*)
  //   FROM ${TRANSACTION_PS_TABLE}
  //   ${where}
  //   `
  //   const [rows, total] = await Promise.all([
  //     this.repo().query(queryTransaction),
  //     this.repo().query(queryCount),
  //   ])
  //   return {
  //     rows,
  //     page,
  //     size,
  //     count: rows.length as number,
  //     total: Number(total[0].count),
  //   }
  // }
  async getPaging(params: {
    page?: number
    size?: number
    keyword?: string
    status?: TRANSACTION_STATUS
    from?: number
    to?: number
    merchant_code?: string
    category?: string
    name?: string
  }) {
    let { page, size } = params
    const { keyword, status, from, to, merchant_code, category, name } = params
    page = Number(page || 1)
    size = Number(size || 50)
    const whereArray = []
    if (from) {
      whereArray.push(`created_at >= ${from}`)
    }
    if (to) {
      whereArray.push(`created_at <= ${to}`)
    }

    if (status) {
      whereArray.push(`status = '${status}'`)
    }
    if (merchant_code) {
      whereArray.push(`merchant_code = '${merchant_code}'`)
    }
    if (keyword) {
      whereArray.push(`(email LIKE '%${keyword}%'
          OR fullname LIKE '%${keyword}%'
          OR order_id LIKE '%${keyword}%'
          OR payment_id LIKE '%${keyword}%')`)
    }
    if (category) {
      whereArray.push(`items like '%"category":"${category}"%'`)
    }
    if (name) {
      whereArray.push(`items like '%"name":"${name}"%'`)
    }

    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const queryTransaction = `
      SELECT id,
      integrate_service,
      order_id,
      order_type,
      user_id,
      fullname,
      email,
      amount,
      currency,
      payment_id,
      payment_method,
      status,
      ipn_url,
      merchant_code,
      created_at,
      updated_at,
      CAST(items AS JSONB) AS items    
      FROM
      (SELECT t.*, tm.value as items
      FROM transactions t
      LEFT JOIN transaction_metadata tm on tm.transaction_id = t.id and tm.attribute = 'items') as tmix
      ${where}  
      ORDER BY created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `
    const queryCount = `
    SELECT COUNT(*)
    FROM (SELECT t.*, tm.value as items
      FROM transactions t
      LEFT JOIN transaction_metadata tm on tm.transaction_id = t.id and tm.attribute = 'items') as tmix
    ${where}  
    `
    const [rows, total] = await Promise.all([
      this.repo().query(queryTransaction),
      this.repo().query(queryCount),
    ])
    return {
      rows,
      page,
      size,
      count: rows.length as number,
      total: Number(total[0].count),
    }
  }
  async getDetail(params: { id: string }) {
    const [transaction, metadatas, logs] = await Promise.all([
      this.repo().findOne({ id: params.id }),
      this.getMetadata({ transaction_id: params.id }),
      this.getLog({ transaction_id: params.id }),
    ])
    return { transaction, metadatas, logs }
  }
  async getMetadata(params: { transaction_id: string }) {
    return this.metadataRepo().find({ transaction_id: params.transaction_id })
  }
  async getLog(params: { transaction_id: string }) {
    return this.logRepo().find({ transaction_id: params.transaction_id })
  }
}
