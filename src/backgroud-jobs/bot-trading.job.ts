import { Inject, Injectable, Logger } from '@nestjs/common'
import { CronExpression } from '@nestjs/schedule'
import { EVENT_STORE_NAME, EVENT_STORE_STATE } from 'src/const/app-setting'
import { ITEM_STATUS } from 'src/const/transaction'
import { EventStoreRepository } from 'src/repositories/event-store.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { MetaapiResource } from 'src/resources/forex'
import { BotTradingService } from 'src/services/bot-trading'
import { BackgroudJob } from '.'

@Injectable()
export class BotTradingJob {
  constructor(
    private backgroudJob: BackgroudJob,
    @Inject(BotTradingService) private botTradingService: BotTradingService,
    @Inject(UserRepository)
    private userRepository: UserRepository,
    @Inject(MetaapiResource) private metaapiResource: MetaapiResource,
    @Inject(EventStoreRepository)
    private eventStoreRepository: EventStoreRepository
  ) {}
  run() {
    this.backgroudJob.addNewJob(
      this.checkNotConnected.name,
      this.checkNotConnected.bind(this),
      CronExpression.EVERY_5_MINUTES,
      this.checkNotConnected.bind(this)
    )
    this.backgroudJob.addNewJob(
      this.checkExpired.name,
      this.checkExpired.bind(this),
      CronExpression.EVERY_10_MINUTES,
      this.checkExpired.bind(this)
    )
    this.backgroudJob.addNewJob(
      this.checkBlockEvent.name,
      this.checkBlockEvent.bind(this),
      CronExpression.EVERY_5_MINUTES,
      this.checkBlockEvent.bind(this)
    )
  }
  async checkNotConnected() {
    const userBots = await this.userRepository.findUserBotTradingSQL({
      status: [ITEM_STATUS.CONNECTING],
      expires_at: Date.now(),
    })
    console.log('BotTradingJob checkNotConnected', userBots.length)
    for (let userBot of userBots) {
      try {
        await this.botTradingService.connectBroker({
          user_bot_id: userBot.id,
          user_id: userBot.user_id,
          bot_id: userBot.bot_id,
        })
      } catch (error) {
        console.log('BotTradingJob checkNotConnected error', error)
      }
    }
  }
  async checkExpired() {
    const userBots = await this.userRepository.findUserBotTradingSQL({
      expired: Date.now(),
    })
    console.log('BotTradingJob checkExpired', userBots.length)
    for (let userBot of userBots) {
      let removed = true
      try {
        if (userBot.subscriber_id) {
          removed = await this.metaapiResource.removeAccount({
            subscriber_id: userBot.subscriber_id,
          })
        }
        if (userBot.profile_id) {
          await this.metaapiResource.removeProvisioningProfile(
            userBot.profile_id
          )
        }
        if (removed) {
          await this.userRepository.saveUserBotTrading([
            {
              id: userBot.id,
              status: ITEM_STATUS.EXPIRED,
              subscriber_id: null,
              broker_account: null,
            },
          ])
          await this.botTradingService.emailBotStatus({
            user_id: userBot.user_id,
            bot_id: userBot.bot_id,
            status: ITEM_STATUS.EXPIRED,
          })
        }
      } catch (error) {}
    }
  }
  async checkBlockEvent() {
    const blockEvents = await this.eventStoreRepository.findEventStore({
      state: EVENT_STORE_STATE.OPEN,
      event_name: [
        EVENT_STORE_NAME.TBOT_STOP_INACTIVE_BY_SYSTEM,
        EVENT_STORE_NAME.TBOT_CONNECTING,
      ],
    })
    for (let event of blockEvents) {
      try {
        if (event.event_name === EVENT_STORE_NAME.TBOT_CONNECTING) {
          const userBot = await this.userRepository.findUserBotTrading({
            id: event.event_id,
          })
          if (userBot[0] && userBot[0].status !== ITEM_STATUS.CONNECTING) {
            await this.eventStoreRepository.delete({
              event_id: event.event_id,
              event_name: EVENT_STORE_NAME.TBOT_CONNECTING,
              state: EVENT_STORE_STATE.OPEN,
            })
          }
        } else if (
          event.event_name === EVENT_STORE_NAME.TBOT_STOP_INACTIVE_BY_SYSTEM &&
          event.metadata?.user_bot_id
        ) {
          const userBot = await this.userRepository.findUserBotTrading({
            id: event.metadata.user_bot_id,
          })
          if (userBot[0] && userBot[0].status === ITEM_STATUS.ACTIVE) {
            await this.eventStoreRepository.delete({
              event_id: event.event_id,
              event_name: EVENT_STORE_NAME.TBOT_STOP_INACTIVE_BY_SYSTEM,
              state: EVENT_STORE_STATE.OPEN,
            })
          }
        }
      } catch (error) {}
    }
  }
}
