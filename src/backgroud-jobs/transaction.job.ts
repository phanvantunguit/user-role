import { Inject, Injectable, Logger } from '@nestjs/common'
import { CronExpression } from '@nestjs/schedule'
import { TRANSACTION_STATUS } from 'src/const/transaction'
import { TransactionRepository } from 'src/repositories/transaction.repository'
import { TransactionService } from 'src/services/transaction'
import { BackgroudJob } from '.'

@Injectable()
export class TransactionJob {
  constructor(
    private backgroudJob: BackgroudJob,
    private transactionService: TransactionService,
    @Inject(TransactionRepository)
    private transactionRepository: TransactionRepository
  ) {}
  run() {
    this.backgroudJob.addNewJob(
      this.checkTransaction.name,
      this.checkTransaction.bind(this),
      CronExpression.EVERY_12_HOURS,
      this.checkTransaction.bind(this)
    )
  }
  async checkTransaction() {
    const trans = await this.transactionRepository.findMultipleStatus({
      status: [TRANSACTION_STATUS.PROCESSING, TRANSACTION_STATUS.CREATED],
    })
    let time = 0
    const _this = this
    trans.forEach((tran) => {
      time += 5000
      setTimeout(() => {
        _this.transactionService.checkPartnerTransaction(tran)
      }, time)
    })
  }
}
