import { Injectable, Logger } from '@nestjs/common'
import { CronExpression, SchedulerRegistry } from '@nestjs/schedule'
import { CronJob } from 'cron'

@Injectable()
export class BackgroudJob {
  private readonly logger = new Logger(BackgroudJob.name)
  constructor(private schedulerRegistry: SchedulerRegistry) {}

  stopCronJob(name: string) {
    const job = this.schedulerRegistry.getCronJob(name)
    if (job) {
      job.stop()
    }
    console.log(job.lastDate())
  }

  startCronJob(name: string) {
    const job = this.schedulerRegistry.getCronJob(name)
    if (!job.running) {
      job.start()
    }
    console.log(job.lastDate())
  }

  deleteCronJob(name: string) {
    this.schedulerRegistry.deleteCronJob(name)
  }

  addNewJob(
    name: string,
    taks: Function,
    time: CronExpression,
    preTask?: Function
  ) {
    this.logger.log(`${name} was Scheduled`)
    if (preTask) {
      this.logger.log(`${preTask.name} is runing...`)
      preTask()
    }
    const job = new CronJob(time, () => {
      this.logger.log(`${taks.name} is runing...`)
      taks()
    })
    this.schedulerRegistry.addCronJob(name, job)
    job.start()
  }
}
