import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { ITEM_STATUS } from 'src/const/transaction'
import { QueryBotTradingHistoryDto } from 'src/controller.modules/admin/api.v1.controller/bot-trading-history.controler/dto'
import { QueryBot, RawBot, BotDomain } from 'src/domains/bot/bot.types'
import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { MetaapiResource } from 'src/resources/forex'
import { throwError } from 'src/utils/handle-error.util'
import { BaseService } from '../base'
export class BotTradingHistoryService
  extends BaseService<any, any>
  implements BotDomain
{
  constructor(
    @Inject(BotTradingHistoryRepository)
    private botTradingHistoryRepository: BotTradingHistoryRepository,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
  ) {
    super(botTradingHistoryRepository)
  }

  async listBotTradingHistory(params: QueryBotTradingHistoryDto): Promise<any> {
    const botTrading = await this.botTradingRepository.findById(params.bot_id)
    delete params.bot_id
    const trades = await this.botTradingHistoryRepository.getPaging({...params, bot_code: botTrading.code})
    return trades
  }
  async createBot(params: any) {
    return this.save({ ...params })
  }
}
