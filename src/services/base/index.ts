import { HttpStatus } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { BaseDomain } from 'src/domains/base/types'
import { IBaseRepository } from 'src/repositories/base.repository/types'
import { throwError } from 'src/utils/handle-error.util'

export class BaseService<Query, Raw> implements BaseDomain<Query, Raw> {
  private repository: IBaseRepository<Query, Raw>

  constructor(repository: IBaseRepository<Query, Raw>) {
    this.repository = repository
  }
  domain_name = 'Data'

  async get(params: Query): Promise<Raw> {
    const data = await this.repository.findOne(params)
    return data
  }
  async list(params: Query): Promise<Raw[]> {
    return this.repository.find(params)
  }
  async save(params: Raw): Promise<Raw> {
    return this.repository.save(params)
  }
  async delete(params: Query): Promise<any> {
    await this.checkNotFoundAndThrow(params)
    return this.repository.delete(params)
  }
  async checkNotFoundAndThrow(params: Query): Promise<Raw> {
    const data = await this.repository.findOne(params)
    if (!data) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        message: `${this.domain_name} not found`,
        error_code: ERROR_CODE.NOT_FOUND.error_code,
      })
    }
    return data
  }
  async checkExistAndThrow(params: Query) {
    const data = await this.repository.findOne(params)
    if (data) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        message: `${this.domain_name} was existed`,
        error_code: ERROR_CODE.RESOURCES_EXISTED.error_code,
      })
    }
  }
}
