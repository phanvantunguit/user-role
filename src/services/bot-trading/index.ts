import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import {
  CHANELS,
  EVENT_STORE_NAME,
  EVENT_STORE_STATE,
} from 'src/const/app-setting'
import { BOT_TRADING_EVENT, TBOT_CHANEL_TS_EVENT } from 'src/const/bot'
import { ITEM_STATUS, ORDER_CATEGORY } from 'src/const/transaction'
import { BotDomain } from 'src/domains/bot/bot.types'
import { AccountBalanceRepository } from 'src/repositories/account-balance.repository'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { BotTradingEntity } from 'src/repositories/bot-trading.repository/bot-trading.entity'
import { CacheRepository } from 'src/repositories/cache.repository'
import { MerchantRepository } from 'src/repositories/merchant.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { MetaapiResource } from 'src/resources/forex'
import { MailResource } from 'src/resources/mail'
import { throwError } from 'src/utils/handle-error.util'
import { BaseService } from '../base'
import { formatInTimeZone } from 'date-fns-tz'
import { EventStoreRepository } from 'src/repositories/event-store.repository'
import { BotTradingHistoryRepository } from 'src/repositories/bot-trading-history.repository'
import { APP_CONFIG } from 'src/config'
import { DBContext } from 'src/repositories/db-context'
export class BotTradingService
  extends BaseService<BotTradingEntity, BotTradingEntity>
  implements BotDomain
{
  constructor(
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(MetaapiResource) private metaapiResource: MetaapiResource,
    @Inject(CacheRepository) private cacheRepository: CacheRepository,
    @Inject(AccountBalanceRepository)
    private accountBalanceRepository: AccountBalanceRepository,
    @Inject(MailResource) private mailResource: MailResource,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
    @Inject(EventStoreRepository)
    private eventStoreRepository: EventStoreRepository,
    @Inject(DBContext) private dBContext: DBContext,
    @Inject(BotTradingHistoryRepository)
    private botTradingHistoryRepository: BotTradingHistoryRepository
  ) {
    super(botTradingRepository)
  }
  async listBot(params: { user_id?: string }): Promise<any> {
    const { user_id } = params
    if (user_id) {
      const userBots = await this.userRepository.findUserBot({ user_id })
      const botIds = userBots.map((e) => e.bot_id)
      if (botIds.length === 0) {
        return []
      }
      return await this.botTradingRepository.findByIds(botIds)
    }
    return await this.list({})
  }
  async createBot(params: BotTradingEntity) {
    if (
      Number(params.max_drawdown) <= Number(params.max_drawdown_change_percent)
    ) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.MAX_DRAWDOWN_INVALID,
      })
    }
    if (params.code) {
      this.cacheRepository.publisher.publish(
        CHANELS.TBOT_CHANEL_UR,
        JSON.stringify({
          event: TBOT_CHANEL_TS_EVENT.add_strategy,
          data: {
            bot_code: params.code,
          },
        })
      )
    }
    return this.save({ ...params })
  }
  async updateBot(params: BotTradingEntity) {
    if (
      Number(params.max_drawdown) <= Number(params.max_drawdown_change_percent)
    ) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.MAX_DRAWDOWN_INVALID,
      })
    }
    if (params.code) {
      this.cacheRepository.publisher.publish(
        CHANELS.TBOT_CHANEL_UR,
        JSON.stringify({
          event: TBOT_CHANEL_TS_EVENT.add_strategy,
          data: {
            bot_code: params.code,
          },
        })
      )
    }
    return this.save({ ...params })
  }
  async updateOrderBot(params: {
    bots: {
      id: string
      order: number
    }[]
    owner_created: string
  }): Promise<any[]> {
    const { bots, owner_created } = params
    const dataUpdate = bots.map((e) => {
      return {
        ...e,
        owner_created,
      }
    })
    return this.botTradingRepository.saves(dataUpdate)
  }

  //Handle delete bot trading without userId
  async handleBotWithoutUserId(params: { bot_id?: string }): Promise<any> {
    const { bot_id } = params
    const resultFindUserId = await this.userRepository.findUserBotTrading({
      bot_id,
    })
    if (resultFindUserId.length > 0) {
      throwError({
        status: HttpStatus.FORBIDDEN,
        ...ERROR_CODE.USERS_HAVE_BOTS,
      })
    } else {
      return await this.botTradingRepository.deleteById(bot_id)
    }
  }

  async connectBroker(params: {
    user_bot_id: string
    user_id: string
    bot_id: string
  }) {
    const { user_bot_id, user_id, bot_id } = params
    try {
      await this.eventStoreRepository.save({
        event_id: user_bot_id,
        event_name: EVENT_STORE_NAME.TBOT_CONNECTING,
        state: EVENT_STORE_STATE.OPEN,
        user_id: user_id,
      })
    } catch (error) {
      return
    }

    const [eventStopInactive, userBot, user, bot] = await Promise.all([
      this.eventStoreRepository.findOne({
        event_id: `${user_id}_${bot_id}`,
        state: EVENT_STORE_STATE.OPEN,
        event_name: EVENT_STORE_NAME.TBOT_STOP_INACTIVE_BY_SYSTEM,
      }),
      this.userRepository.findUserBotTrading({
        user_id,
        bot_id,
      }),
      this.userRepository.findOneUser({
        id: user_id,
      }),
      this.get({ id: bot_id }),
    ])
    try {
      // connect lan dau: xet config dua vao balance hien tai, status = ACTIVE, cap nhat initBalance, that bai => status = INACTIVE
      // vi pham: xet config dua vao balance lan dau, that bai => status = INACTIVE_BY_SYSTEM
      // dinh stop out: xet config dua vao balance hien tai, reset stop out , status = ACTIVE, cap nhat initBalance,, that bai => status = STOP_OUT
      if (!userBot[0] || !user || !bot) {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.BOT_INVALID,
        })
      }
      if (
        userBot[0]?.status !== ITEM_STATUS.CONNECTING ||
        !userBot[0]?.subscriber_id
      ) {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.CONNECT_ACCOUNT_FAILED,
        })
      }
      const subscriberId = userBot[0].subscriber_id

      const account = await this.metaapiResource.getAccount({
        account_id: userBot[0].subscriber_id,
      })
      if (
        !account ||
        account.state !== 'DEPLOYED' ||
        account.connectionStatus !== 'CONNECTED'
      ) {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.ACCOUNT_NOT_CONNECTED,
        })
        return
      }
      const connection = await account.getRPCConnection()
      await connection.connect()
      await connection.waitSynchronized()
      // connect lan dau: xet config dua vao balance hien tai, status = ACTIVE, cap nhat initBalance, that bai => status = INACTIVE
      // vi pham: xet config dua vao balance lan dau, that bai => status = INACTIVE_BY_SYSTEM
      // check stop out: xet config dua vao balance hien tai, reset stop out , status = ACTIVE, cap nhat initBalance,, that bai => status = INACTIVE
      // check positions
      const positions = await connection.getPositions()
      if (positions.length > 0) {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.POSITION_NOT_EMPTY,
        })
      }

      const accountInfo = await connection.getAccountInformation()
      let initBalanceAmount = Number(accountInfo.balance)
      if(userBot[0].balance && Number(userBot[0].balance) < initBalanceAmount) {
        initBalanceAmount = Number(userBot[0].balance)
      }
      const startTime = new Date()
      let connectedTime = new Date()
      const botBalanceAmount = Number(bot.balance)

      // check vi pham: xet config dua vao balance lan dau
      const accountBalanceInit = await this.accountBalanceRepository.findOne({
        user_id,
        bot_id,
        change_id: 'init',
      })

      let isStopOut
      try {
        const stopouts =
          await this.metaapiResource.copyFactory.tradingApi.getStopouts(
            subscriberId
          )
        isStopOut = stopouts.find((e) => e.strategy.id === bot.code)
      } catch (error) {
        console.log('getStopouts error', error)
      }

      // check vi pham: xet config dua vao balance lan dau
      // check stop out: xet config dua vao balance hien tai (uu tien)

      if (eventStopInactive && !isStopOut) {
        // if(accountBalanceInit) {
        //   initBalanceAmount = accountBalanceInit.balance
        // }
        connectedTime = userBot[0].connected_at
          ? new Date(Number(userBot[0].connected_at))
          : new Date(Number(userBot[0].updated_at))
      }

      // check balance
      if (botBalanceAmount > Number(accountInfo.balance)) {
        // Connect lần đầu không đủ tiền
        // check accountBalanceInit not found
        // or (accountBalanceInit.balance < botBalanceAmount && trade history not found)
        // delete account metaapi, delete accountBalanceInit
        // if bot select delete user bot
        // if bot purchase remove account metaapi info
        const [trade, accountBalance] = await Promise.all([
          this.botTradingHistoryRepository.findOne({
            broker_account: `${accountInfo.login}`,
          }),
          this.accountBalanceRepository.findOne({
            user_id,
            broker_account: `${accountInfo.login}`,
          }),
        ])
        if (
          !accountBalance ||
          accountBalanceInit.broker_account !== `${accountInfo.login}` ||
          (accountBalanceInit?.balance < botBalanceAmount && !trade)
        ) {
          try {
            try {
              await this.metaapiResource.removeSubscriber({
                subscriber_id: userBot[0].subscriber_id,
              })
            } catch (error) {
              console.log('removeSubscriber error', error)
            }
            await this.metaapiResource.removeAccount({
              subscriber_id: userBot[0].subscriber_id,
            })
            await this.accountBalanceRepository.delete({
              broker_account: `${accountInfo.login}`,
            })
            const logConnect = await this.userRepository.findUserAssetLog({
              status: ITEM_STATUS.NOT_CONNECT,
              asset_id: bot_id,
            })
            if (logConnect[0]?.order_id) {
              // if bot purchase remove account metaapi info
              const updateUserBot = {
                id: user_bot_id,
                broker: null,
                broker_server: null,
                broker_account: null,
                subscriber_id: null,
                status: ITEM_STATUS.NOT_CONNECT,
              }
              await this.userRepository.saveUserBotTrading([updateUserBot])
            } else {
              // if bot select delete user bot
              await this.userRepository.deleteUserBotTrading({
                user_id,
                bot_id,
              })
            }
          } catch (error) {}
          this.cacheRepository.publisher.publish(
            CHANELS.TBOT_CHANEL_TS,
            JSON.stringify({
              event: TBOT_CHANEL_TS_EVENT.account,
              data: {
                status: ITEM_STATUS.NOT_CONNECT,
                user_id,
                bot_id,
                error: ERROR_CODE.ACCOUNT_NOT_ENOUGH_BALANCE,
              },
            })
          )
          return 
        }

        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.ACCOUNT_NOT_ENOUGH_BALANCE,
        })
      }
      const multiplier = Math.floor(initBalanceAmount / botBalanceAmount)
      const maxAbsoluteRisk =
        (Number(bot.max_drawdown) - Number(bot.max_drawdown_change_percent)) *
        multiplier *
        botBalanceAmount

      // use transaction
      // active user bot
      // save user asset log
      // delete eventStore TBOT_STOP_INACTIVE_BY_SYSTEM
      // update init balance
      // save eventStore TBOT_CONNECTED
      // updateSubscriber
      // reset stop out
      // publish ws

      return await this.dBContext.runInTransaction(async (queryRunner) => {
        const updateUserBot = {
          id: userBot[0].id,
          status: ITEM_STATUS.ACTIVE,
          connected_at: connectedTime.valueOf(),
        }
        const dataLog = {
          asset_id: bot.id,
          user_id: user_id,
          category: ORDER_CATEGORY.TBOT,
          status: updateUserBot.status,
          owner_created: user_id,
          name: bot.name,
          metadata: {
            balance: initBalanceAmount,
            broker: userBot[0].broker,
            broker_account: userBot[0].broker_account,
            broker_server: userBot[0].broker_server,
            subscriber_id: userBot[0].subscriber_id,
            expires_at: userBot[0].expires_at,
            bot_name: bot.name
          }
        }
        await Promise.all([
          this.userRepository.saveUserBotTrading([updateUserBot], queryRunner),
          this.userRepository.saveUserAssetLog([dataLog], queryRunner),
          this.eventStoreRepository.delete({
            event_id: `${userBot[0].user_id}_${userBot[0].bot_id}`,
            state: EVENT_STORE_STATE.OPEN,
            event_name: EVENT_STORE_NAME.TBOT_STOP_INACTIVE_BY_SYSTEM,
          }),
        ])

        if (!eventStopInactive) {
          const dataInitBalance = {
            user_id,
            bot_id,
            broker_account: `${accountInfo.login}`,
            balance: accountInfo.balance,
            change_by_user: true,
            change_id: 'init',
            created_at: updateUserBot.connected_at,
          }
          if (accountBalanceInit) {
            dataInitBalance['id'] = accountBalanceInit.id
          }
          await Promise.all([
            this.accountBalanceRepository.save(dataInitBalance, queryRunner),
            this.eventStoreRepository.save(
              {
                event_id: `${updateUserBot.connected_at}_${userBot[0].subscriber_id}`,
                event_name: EVENT_STORE_NAME.TBOT_CONNECTED,
                state: EVENT_STORE_STATE.OPEN,
                user_id,
                metadata: {
                  account_id: userBot[0].subscriber_id,
                  user_id: userBot[0].user_id,
                  connected_at: updateUserBot.connected_at,
                },
              },
              queryRunner
            ),
          ])
        }
        await this.metaapiResource.updateSubscriber({
          subscriber_id: userBot[0].subscriber_id,
          name: `${user.merchant_code}_${userBot[0].broker_account}_${user_id}`,
          strategy_id: bot.code,
          multiplier: multiplier,
          max_absolute_risk: Math.ceil(maxAbsoluteRisk),
          start_time: startTime,
          broker: userBot[0].broker,
        })
        if (isStopOut) {
          await this.metaapiResource.copyFactory.tradingApi.resetStopouts(
            subscriberId,
            bot.code,
            'lifetime-balance-minus-equity'
          )
        }
        this.cacheRepository.publisher.publish(
          CHANELS.TBOT_CHANEL_TS,
          JSON.stringify({
            event: TBOT_CHANEL_TS_EVENT.account,
            data: {
              user_id,
              bot_id,
              balance: accountInfo.balance,
              status: updateUserBot.status,
              user_bot_id: updateUserBot.id,
            },
          })
        )

        if (!eventStopInactive && !isStopOut) {
          this.cacheRepository.publisher.publish(
            CHANELS.TBOT_CHANEL_UR,
            JSON.stringify({
              event: TBOT_CHANEL_TS_EVENT.add_subscriber,
              data: {
                subscriber_id: userBot[0].subscriber_id,
                user_id,
              },
            })
          )
        }
        console.log('connectBroker finish')
        return true
      })
    } catch (error) {
      console.log('connectBroker error1:', error)
      try {
        const updateUserBot = {
          id: user_bot_id,
          status: eventStopInactive
            ? ITEM_STATUS.INACTIVE_BY_SYSTEM
            : ITEM_STATUS.INACTIVE,
        }
        await this.userRepository.saveUserBotTrading([updateUserBot])
        const dataLog = {
          asset_id: bot_id,
          user_id: user_id,
          category: ORDER_CATEGORY.TBOT,
          status: updateUserBot.status,
          owner_created: user_id,
          name: bot.name,
          metadata: {
            event: BOT_TRADING_EVENT.CONNECTING,
            error,
          },
        }
        await this.userRepository.saveUserAssetLog([dataLog])
        this.cacheRepository.publisher.publish(
          CHANELS.TBOT_CHANEL_TS,
          JSON.stringify({
            event: TBOT_CHANEL_TS_EVENT.account,
            data: {
              status: updateUserBot.status,
              user_id,
              bot_id,
              error,
            },
          })
        )
      } catch (error) {
        console.log('connectBroker error2:', error)
      }
    } 
    finally {
      await this.eventStoreRepository.delete({event_id: user_bot_id, event_name: EVENT_STORE_NAME.TBOT_CONNECTING, state: EVENT_STORE_STATE.OPEN})
    }
  }

  async emailBotStatus(params: {
    user_id: string
    bot_id: string
    status: ITEM_STATUS
    trade?: {
      id: string
      symbol: string
      time: number
      broker_account: string
    }
    count_invalid?: number
    stopout?: {
      balance_init: number
      balance_current: number
      max_drawdown: number
      max_drawdown_change_percent: number
      time: number
      broker_account: string
    }
  }) {
    const { user_id, bot_id, status, trade, stopout, count_invalid } = params
    const [user, bot] = await Promise.all([
      this.userRepository.findOneUser({
        id: user_id,
      }),
      this.botTradingRepository.findById(bot_id),
    ])
    if (!user || !bot) {
      return
    }
    const merchant = await this.merchantRepository.findOne({
      code: user.merchant_code,
    })
    let subject
    let title
    let from_email
    let from_name
    let logo_url
    let header_url
    let main_content = []
    let twitter_url
    let youtube_url
    let facebook_url
    let telegram_url
    let company_name
    if (merchant && merchant.config) {
      if (
        merchant.config['verified_sender'] &&
        merchant.config['email_sender']
      ) {
        from_email = merchant.config['email_sender']['from_email']
        from_name = merchant.config['email_sender']['from_name']
      }
      if (merchant.config['social_media']) {
        twitter_url = merchant.config['social_media']['twitter_url']
        youtube_url = merchant.config['social_media']['youtube_url']
        facebook_url = merchant.config['social_media']['facebook_url']
        telegram_url = merchant.config['social_media']['telegram_url']
      }
      logo_url = merchant.config['email_logo_url']
      header_url = merchant.config['email_banner_url']
      company_name = merchant.name
      from_email = from_email || APP_CONFIG.SENDGRID_SENDER_ALGO_EMAIL
      from_name = from_name || APP_CONFIG.SENDGRID_SENDER_ALGO_NAME
      logo_url = logo_url || APP_CONFIG.ALGO_LOGO_EMAIL
      header_url = header_url || APP_CONFIG.ALGO_BANNER_EMAIL
    }
    switch (status) {
      case ITEM_STATUS.INACTIVE_BY_SYSTEM:
        if (!trade) {
          return
        }
        subject = 'Vi Phạm Trading Bot'
        title = 'Vi Phạm Trading Bot'
        main_content.push(title)
        main_content.push(
          'Bạn đã vi phạm chính sách tham gia. Chúng tôi đã phát hiện bạn có dấu hiệu vào lệnh bằng tay.'
        )
        main_content.push(`Tên bot: ${bot.name}`)
        main_content.push(`Lần vi phạm thứ: ${count_invalid}`)
        main_content.push(`Tài khoản MT4: ${trade.broker_account}`)
        main_content.push(
          `Thời gian vào lệnh: ${formatInTimeZone(
            Number(trade.time),
            'Asia/Ho_Chi_Minh',
            'HH:mm dd/MM/yyyy'
          )} (GMT+7)`
        )
        main_content.push(`Mã lệnh: ${trade.id}`)
        main_content.push(`Symbol: ${trade.symbol}`)
        main_content.push(`Quy định xử lý:`)
        main_content.push(`-  Lần 1: Tạm dừng trade.`)
        main_content.push(
          `-  Lần 2: Tạm dừng trade. Được phép kích hoạt lại sau 3 ngày.`
        )
        main_content.push(`-  Lần 3: Xoá Bot Trading.`)

        break
      case ITEM_STATUS.EXPIRED:
        subject = 'Trading Bot Đã Hết Hạn'
        title = 'Trading Bot Đã Hết Hạn'
        main_content.push(`Bot của bạn đã hết hạn.`)
        main_content.push(`Tên bot: ${bot.name}`)
        break
      case ITEM_STATUS.STOP_OUT:
        if (!stopout) {
          return
        }
        subject = 'Trading Bot Của Bạn Đã Bị Chạm Mốc Max Drawdown'
        title = 'Trading Bot của bạn đã bị chạm mốc Max Drawdown'
        main_content.push(title)
        main_content.push(
          `Bot Trading của bạn sẽ tạm dừng trong thời gian này, vui lòng kích hoạt lại Bot Trading để tiếp trade và đảm bảo tiền lớn hơn số dư tối thiểu.`
        )
        main_content.push(`Tên bot: ${bot.name}`)
        main_content.push(`Tài khoản MT4: ${stopout.broker_account}`)
        main_content.push(
          `Thời gian tạm dừng: ${formatInTimeZone(
            Number(stopout.time),
            'Asia/Ho_Chi_Minh',
            'HH:mm dd/MM/yyyy'
          )} (GMT+7)`
        )
        break
    }
    await this.mailResource.sendEmailStandard(
      subject,
      title,
      user.email,
      main_content,
      from_email,
      from_name,
      logo_url,
      header_url,
      twitter_url,
      youtube_url,
      facebook_url,
      telegram_url,
      company_name,
      `${user.first_name || ''} ${user.last_name || ''}`,
      APP_CONFIG.SENDGRID_CC_EMAIL
    )
  }
}
