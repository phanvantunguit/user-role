import { Inject } from '@nestjs/common'
import {
  PackageDomain,
  QueryPackage,
  RawPackage,
} from 'src/domains/setting/package.types'
import { PackageRepository } from 'src/repositories/package.ropository'

import { BaseService } from '../base'
export class PackageService
  extends BaseService<QueryPackage, RawPackage>
  implements PackageDomain
{
  constructor(
    @Inject(PackageRepository)
    private packageRepository: PackageRepository
  ) {
    super(packageRepository)
  }
}
