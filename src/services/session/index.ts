import { HttpStatus, Inject } from '@nestjs/common'
import { RawSession, Session, SessionDomain } from 'src/domains/user'
import { UserRepository } from 'src/repositories/user.repository'
import { IToken } from 'src/const/authorization'
import { CacheRepository } from 'src/repositories/cache.repository'
import { APP_CONFIG } from 'src/config'
import { throwError } from 'src/utils/handle-error.util'
import { ERROR_CODE } from 'src/const'
import { GeneralSettingRoleService } from '../general-setting-role'
import { decodeBase64 } from 'src/utils/hash.util'
import * as hashUtil from 'src/utils/hash.util'
import { v4 } from 'uuid'
import * as uaParser from 'ua-parser-js'

export class SessionService implements SessionDomain {
  constructor(
    @Inject(UserRepository)
    private userRepository: UserRepository,
    @Inject(CacheRepository) private cacheRepository: CacheRepository,
    @Inject(GeneralSettingRoleService)
    private generalSettingRoleService: GeneralSettingRoleService
  ) {}

  async save(params: Session): Promise<RawSession> {
    const session = await this.userRepository.saveSession(params)
    const encodeData = params.token.split('.')[1]
    let limitTab = 1
    if (encodeData) {
      const decode: IToken = JSON.parse(decodeBase64(encodeData))
      limitTab = await this.generalSettingRoleService.getMaxLimitTab(
        decode.roles
      )
    }
    console.log("limitTab", limitTab)
    const sessions = await this.userRepository.findSessionLogin(
      { user_id: params.user_id, enabled: true },
      limitTab
    )
    const activeTokens = sessions.map((s) => s.token_id)
    await this.cacheRepository.setKey(
      params.user_id,
      JSON.stringify(activeTokens),
      Number(APP_CONFIG.TIME_EXPIRED_LOGIN)
    )
    return session
  }
  async verifySession(token: IToken): Promise<boolean> {
    const cacheString = await this.cacheRepository.getKey(token.user_id)
    const cacheSessions = JSON.parse(cacheString) as string[]
    if (!cacheSessions || cacheSessions.length === 0) {
      const limitTab = await this.generalSettingRoleService.getMaxLimitTab(
        token.roles
      )
      const sessions = await this.userRepository.findSessionLogin(
        { user_id: token.user_id, enabled: true },
        limitTab
      )
      const session = sessions.find((s) => s.token_id === token.token_id)
      if (session) {
        return true
      }
    } else {
      const session = cacheSessions.find((t) => t === token.token_id)
      if (session) {
        return true
      }
    }
    throwError({
      status: HttpStatus.UNAUTHORIZED,
      ...ERROR_CODE.TOKEN_INVALID,
    })
  }
}
