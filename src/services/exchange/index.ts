import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { Exchange, ExchangeDomain, QueryExchange } from 'src/domains/setting'
import { SettingRepository } from 'src/repositories/setting.repository'
import { throwError } from 'src/utils/handle-error.util'

export class ExchangeService implements ExchangeDomain {
  constructor(
    @Inject(SettingRepository) private settingRepo: SettingRepository
  ) {}
  async getExchange(params: QueryExchange): Promise<Exchange> {
    const exchanges = await this.settingRepo.findExchange(params)
    if (exchanges.length === 0) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.EXCHANGE_NOT_FOUND,
      })
    }
    return exchanges[0]
  }
  async listExchange(): Promise<Exchange[]> {
    const exchanges = await this.settingRepo.findExchange({})
    return exchanges
  }
  async createExchange(params: Exchange[]): Promise<any> {
    const create = await this.settingRepo.saveExchanges(params)
    return create
  }
  async updateExchange(params: Exchange): Promise<any> {
    const { exchange_name } = params
    await this.checkExchangeNotFoundAndThrow({ exchange_name })
    const updated = await this.settingRepo.saveExchanges([params])
    return updated
  }
  async deleteExchange(names: string[]): Promise<any> {
    const deleted = await this.settingRepo.deleteExchangeNames(names)
    return deleted
  }
  private async checkExchangeNotFoundAndThrow(params: QueryExchange) {
    const exchanges = await this.settingRepo.findExchange(params)
    if (exchanges.length === 0) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.EXCHANGE_NOT_FOUND,
      })
    }
  }
}
