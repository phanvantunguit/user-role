import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { QueryBot, RawBot, BotDomain } from 'src/domains/bot/bot.types'
import { BotSettingRepository } from 'src/repositories/bot-setting.repository'
import { BotRepository } from 'src/repositories/bot.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { throwError } from 'src/utils/handle-error.util'
import { BaseService } from '../base'
export class BotService
  extends BaseService<QueryBot, RawBot>
  implements BotDomain
{
  constructor(
    @Inject(BotRepository)
    private botRepository: BotRepository,
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(BotSettingRepository)
    private botSettingRepository: BotSettingRepository
  ) {
    super(botRepository)
  }
  async listBot(params: { user_id?: string }): Promise<any> {
    const { user_id } = params
    if (user_id) {
      const userBots = await this.userRepository.findUserBot({ user_id })
      const botIds = userBots.map((e) => e.bot_id)
      if (botIds.length === 0) {
        return []
      }
      return await this.botRepository.findByIds(botIds)
    }
    return await this.list({})
  }
  async updateOrderBot(params: {
    bots: {
      id: string
      order: number
    }[]
    owner_created: string
  }): Promise<any[]> {
    const { bots, owner_created } = params
    const dataUpdate = bots.map((e) => {
      return {
        ...e,
        owner_created,
      }
    })
    return this.botRepository.saves(dataUpdate)
  }

  async createBot(params: RawBot) {
    const botSeting = await this.botSettingRepository.findById(
      params.bot_setting_id
    )
    if (!botSeting) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }
    const code =
      (botSeting.params &&
        botSeting.params.type &&
        botSeting.params.type.default) ||
      botSeting.name.replace(/ /g, '')

    return this.save({ ...params, code })
  }

  //Handle delete bot trading without userId
  async handleBotWithoutUserId(params: { bot_id?: string }): Promise<any> {
    const { bot_id } = params
    const resultFindUserId = await this.userRepository.findUserBot({
      bot_id,
    })
    if (resultFindUserId.length > 0) {
      throwError({
        status: HttpStatus.FORBIDDEN,
        ...ERROR_CODE.USERS_HAVE_BOTS,
      })
    } else {
      return await this.botRepository.deleteById(bot_id)
    }
  }
}
