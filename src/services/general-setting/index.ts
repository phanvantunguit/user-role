import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import {
  GeneralSettingCreate,
  GeneralSettingDomain,
  GeneralSettingUpdate,
  QueryGeneralSetting,
  RawGeneralSetting,
} from 'src/domains/setting'
import { SettingRepository } from 'src/repositories/setting.repository'
import { throwError } from 'src/utils/handle-error.util'

export class GeneralSettingService implements GeneralSettingDomain {
  constructor(
    @Inject(SettingRepository) private settingRepo: SettingRepository
  ) {}
  async getGeneralSetting(
    params: QueryGeneralSetting
  ): Promise<RawGeneralSetting> {
    const generalSettings = await this.settingRepo.findGeneralSetting(params)
    if (generalSettings.length === 0) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.GENERAL_SETTING_NOT_FOUND,
      })
    }
    return generalSettings[0]
  }
  async listGeneralSetting(): Promise<RawGeneralSetting[]> {
    const generalSettings = await this.settingRepo.findGeneralSetting({})
    return generalSettings
  }
  async createGeneralSetting(params: GeneralSettingCreate): Promise<any> {
    const { owner_created, general_settings } = params
    const dataCreate = general_settings.map((gs) => {
      return {
        ...gs,
        owner_created,
      }
    })
    const create = await this.settingRepo.saveGeneralSetting(dataCreate)
    return create
  }
  async updateGeneralSetting(params: GeneralSettingUpdate): Promise<any> {
    const { general_setting_id, general_setting_name } = params
    await this.getGeneralSetting({ general_setting_id })
    await this.checkGSExistAndThrow({
      general_setting_id,
      general_setting_name,
    })
    const update = await this.settingRepo.saveGeneralSetting([params])
    return update
  }
  async deleteGeneralSetting(general_setting_ids: string[]): Promise<any> {
    const deleted = await this.settingRepo.deleteGeneralSettingByIds(
      general_setting_ids
    )
    return deleted
  }
  private async checkGSExistAndThrow(params: QueryGeneralSetting) {
    const resolutions = await this.settingRepo.findGeneralSetting(params)
    if (
      resolutions.length > 0 &&
      resolutions[0].general_setting_id !== params.general_setting_id
    ) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.GENERAL_SETTING_EXISTED,
      })
    }
  }
}
