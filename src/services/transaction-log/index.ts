import { Inject } from '@nestjs/common'
import { EventEmitter2 } from '@nestjs/event-emitter'
import {
  QueryTransactionLog,
  RawTransactionLog,
  TransactionLogDomain,
} from 'src/domains/transaction/transaction-log.types'
import { TransactionLogRepository } from 'src/repositories/transaction-log.repository'
import { BaseService } from '../base'
export class TransactionLogService
  extends BaseService<QueryTransactionLog, RawTransactionLog>
  implements TransactionLogDomain
{
  constructor(
    @Inject(TransactionLogRepository)
    private transactionLogRepository: TransactionLogRepository,
    private eventEmitter: EventEmitter2
  ) {
    super(transactionLogRepository)
  }

  async save(params: RawTransactionLog): Promise<RawTransactionLog> {
    const transLog = await this.transactionLogRepository.save(params)
    this.eventEmitter.emit(transLog.transaction_event, transLog)
    return transLog
  }
  async saveNotSendEvent(
    params: RawTransactionLog
  ): Promise<RawTransactionLog> {
    const transLog = await this.transactionLogRepository.save(params)
    return transLog
  }
}
