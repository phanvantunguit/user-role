import { HttpStatus, Inject } from '@nestjs/common'
import {
  DEFAULT_ROLE,
  ERROR_CODE,
  ROLE_STATUS,
  USER_ROLE_STATUS,
} from 'src/const'
import { TRANSACTION_STATUS } from 'src/const/transaction'
import {
  ListRole,
  QueryRole,
  RoleCreate,
  RoleDomain,
  RoleOrderUpdate,
  RoleUpdate,
} from 'src/domains/permission'
import { RawRoleView } from 'src/domains/user'
import { PermissionRepository } from 'src/repositories/permission.repository'
import { TransactionRepository } from 'src/repositories/transaction.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { throwError } from 'src/utils/handle-error.util'

export class RoleService implements RoleDomain {
  constructor(
    @Inject(PermissionRepository) private permissionRepo: PermissionRepository,
    @Inject(UserRepository) private userRepo: UserRepository,
    @Inject(TransactionRepository)
    private transactionRepository: TransactionRepository
  ) {}
  async updateOrderRole(params: RoleOrderUpdate): Promise<any[]> {
    const { roles, owner_created } = params
    const dataUpdate = roles.map((e) => {
      return {
        ...e,
        owner_created,
      }
    })
    return this.permissionRepo.saveRoles(dataUpdate)
  }
  checkRoleDisable(parentId: string, roles: RawRoleView[]) {
    if (!parentId) {
      return false
    }
    const roleParent = roles.find((ur) => ur.id === parentId)
    if (!roleParent) {
      return false
    }
    if (!roleParent.expires_at || Number(roleParent.expires_at) > Date.now()) {
      return true
    }
    return this.checkRoleDisable(roleParent.parent_id, roles)
  }
  async listRoleForUser(user_id: string, package_id?: string): Promise<any> {
    const rolesForUser = []
    const [roles, user, trans] = await Promise.all([
      this.permissionRepo.findRoles({}),
      this.userRepo.findOneUserView({ id: user_id }),
      this.transactionRepository.viewFindByUserMultipleStatus(user_id, [
        TRANSACTION_STATUS.PROCESSING,
        TRANSACTION_STATUS.CREATED,
      ]),
    ])

    // get all role, map with role of user and status for role
    // USER_ROLE_STATUS.DISABLED: user is using role parent
    // USER_ROLE_STATUS.PROCESSING: payment is processing
    roles
      // .sort((a, b) => (Number(a.price) || 0) - (Number(b.price) || 0))
      .forEach((r) => {
        // get role to buy, check CLOSE and return
        if (package_id) {
          if (r.status === ROLE_STATUS.CLOSE) {
            return
          }
        }
        let userStatus = ''
        // check CURRENT_PLAN, expires_at > now push to rolesForUser and return
        const roleUser = user.roles.find((ur) => ur.id === r.id)
        if (roleUser) {
          const description_features =
            roleUser.description_features?.features || []
          userStatus = USER_ROLE_STATUS.CURRENT_PLAN
          if (
            !roleUser.expires_at ||
            Number(roleUser.expires_at) > Date.now()
          ) {
            userStatus = USER_ROLE_STATUS.CURRENT_PLAN
            rolesForUser.push({
              ...roleUser,
              user_status: userStatus,
              description_features,
            })
            return
          }
        }
        // check CLOSE and return
        if (r.status === ROLE_STATUS.CLOSE) {
          return
        }

        const description_features = r.description_features?.features || []
        r.description_features = description_features
        // check DISABLED and push
        const isDisable = this.checkRoleDisable(r.parent_id, user.roles)
        if (isDisable) {
          userStatus = USER_ROLE_STATUS.DISABLED
          rolesForUser.push({
            ...r,
            user_status: userStatus,
          })
          return
        }
        // check PROCESSING
        // !package_id: push all role processing
        // package_id: push role processing where package_id = trans.package_id and return
        let checkProcessing = false
        for (const t of trans) {
          if (t.details.length > 0 && t.details[0].role_id === r.id) {
            if (package_id) {
              if (package_id === t.details[0].package_id) {
                checkProcessing = true
                rolesForUser.push({
                  ...r,
                  user_status: USER_ROLE_STATUS.PROCESSING,
                  transaction_id: t.id,
                })
                return
              }
            } else {
              checkProcessing = true
              rolesForUser.push({
                ...r,
                user_status: USER_ROLE_STATUS.PROCESSING,
                transaction_id: t.id,
              })
            }
          }
        }
        if (!checkProcessing) {
          if (userStatus === USER_ROLE_STATUS.CURRENT_PLAN) {
            const price = Number(roleUser.price) || 0
            if (price === 0 && package_id) {
              return
            }
            rolesForUser.push({
              ...roleUser,
              user_status: userStatus,
              description_features,
            })
          } else {
            rolesForUser.push(r)
          }
        }
      })

    // const data = roles
    //   .sort((a, b) => (Number(a.price) || 0) - (Number(b.price) || 0))
    //   .map((r, index) => {
    //     const description_features = r.description_features?.features || []
    //     r.description_features = description_features
    //     r.order = index
    //     let userStatus = ''
    //     const isDisable = this.checkRoleDisable(r.parent_id, user.roles)
    //     if (isDisable) {
    //       userStatus = USER_ROLE_STATUS.DISABLED
    //     }
    //     const roleUser = user.roles.find((ur) => ur.id === r.id)
    //     if (roleUser) {
    //       const description_features =
    //         roleUser.description_features?.features || []
    //       roleUser.order = index
    //       if (
    //         roleUser.expires_at &&
    //         Number(roleUser.expires_at) <= Date.now()
    //       ) {
    //         userStatus = userStatus || USER_ROLE_STATUS.CURRENT_PLAN
    //       } else {
    //         userStatus = USER_ROLE_STATUS.CURRENT_PLAN
    //       }
    //       return {
    //         ...roleUser,
    //         user_status: userStatus,
    //         description_features,
    //       }
    //     }
    //     const roleTrans = trans.find((t) => t.details[0].role_id === r.id)
    //     if (roleTrans) {
    //       userStatus = USER_ROLE_STATUS.PROCESSING
    //       return {
    //         ...r,
    //         user_status: userStatus,
    //         transaction_id: roleTrans.id,
    //       }
    //     }
    //     if (userStatus) {
    //       return {
    //         ...r,
    //         user_status: userStatus,
    //       }
    //     }
    //     return r
    //   })
    return rolesForUser
  }
  async listRole(params: ListRole): Promise<any> {
    const { user_id } = params
    if (user_id) {
      const userRoles = await this.userRepo.findUserRole({ user_id })
      const roleIds = userRoles.map((e) => e.role_id)
      if (roleIds.length === 0) {
        return []
      }
      return await this.permissionRepo.findRoleByIds(roleIds)
    }
    return await this.permissionRepo.findRoles({})
  }
  async getRoleDetail(id: string): Promise<any> {
    const roles = await this.permissionRepo.findRoles({ id })
    if (roles.length === 0) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.ROLE_NOT_FOUND,
      })
    }
    return roles[0]
  }
  async createRole(params: RoleCreate): Promise<any> {
    await this.checkRoleExistAndThrow({ role_name: params.role_name })
    const dataCreate = {
      ...params,
      description_features: {
        features: params.description_features,
      },
    }
    dataCreate.parent_id = dataCreate.parent_id ? dataCreate.parent_id : null
    const create = await this.permissionRepo.saveRoles([dataCreate])
    return create
  }
  async updateRole(params: RoleUpdate): Promise<any> {
    await this.checkRoleNotFoundAndThrow({ id: params.id })
    const dataUpdate = {
      ...params,
      description_features: {
        features: params.description_features,
      },
    }
    if (!params.description_features) {
      delete dataUpdate.description_features
    }
    if (!params.parent_id) {
      delete dataUpdate.parent_id
    }
    const updated = await this.permissionRepo.saveRoles([dataUpdate])
    return updated
  }
  async deleteRole(id: string): Promise<any> {
    if (id === DEFAULT_ROLE.id) {
      throwError({
        status: HttpStatus.FORBIDDEN,
        ...ERROR_CODE.NO_PERMISSION,
      })
    }

    const resultFindUserId = await this.userRepo.findUserRole({
      role_id: id,
    })
    if (resultFindUserId.length > 0) {
      throwError({
        status: HttpStatus.FORBIDDEN,
        ...ERROR_CODE.USERS_HAVE_ROLES,
      })
    }
    const deleted = await this.permissionRepo.deleteRoleByIds([id])
    return deleted
  }
  async checkRoleNotFoundAndThrow(params: QueryRole) {
    const roles = await this.permissionRepo.findRoles(params)
    if (roles.length === 0) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.ROLE_NOT_FOUND,
      })
    }
    return roles
  }
  private async checkRoleExistAndThrow(params: QueryRole) {
    const roles = await this.permissionRepo.findRoles(params)
    if (roles.length > 0) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.ROLE_EXISTED,
      })
    }
  }
}
