import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { AuthUserRoleDomain, AuthUserRoleModify } from 'src/domains/user'
import { PermissionRepository } from 'src/repositories/permission.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { throwError } from 'src/utils/handle-error.util'

export class AuthUserRoleService implements AuthUserRoleDomain {
  constructor(
    private userRepo: UserRepository,
    @Inject(PermissionRepository) private permissionRepo: PermissionRepository
  ) {}
  async modifyAuthUserRole(params: AuthUserRoleModify): Promise<any> {
    const { user_id, owner_created, auth_roles } = params
    const authRoleIds = auth_roles.map((ar) => ar.auth_role_id)
    const authRoles = await this.permissionRepo.findAuthRoleByIds(authRoleIds)
    if (authRoles.length !== authRoleIds.length) {
      const authRoleIdsNotFound = []
      for (const ar of authRoles) {
        if (!authRoleIds.includes(ar.id)) {
          authRoleIdsNotFound.push(ar.id)
        }
      }
      throwError({
        status: HttpStatus.NOT_FOUND,
        error_code: ERROR_CODE.ROLE_NOT_FOUND.error_code,
        message: authRoleIdsNotFound,
      })
    }
    const currentAuthUserRoles = await this.userRepo.findAuthUserRole({
      user_id,
    })
    const dataModify = []
    for (const arReq of auth_roles) {
      const aurExistIndex = currentAuthUserRoles.findIndex(
        (aur) => aur.auth_role_id === arReq.auth_role_id
      )
      if (aurExistIndex > -1) {
        if (
          currentAuthUserRoles[aurExistIndex].description !== arReq.description
        ) {
          dataModify.push({
            ...currentAuthUserRoles[aurExistIndex],
            description: arReq.description,
            owner_created,
          })
        }
        currentAuthUserRoles.splice(aurExistIndex, 1)
      } else {
        dataModify.push({
          ...arReq,
          user_id,
          owner_created,
        })
      }
    }
    const deleteAuthUserRoles = currentAuthUserRoles.map((aur) => aur.id)
    if (deleteAuthUserRoles.length > 0) {
      await this.userRepo.deleteAuthUserRoleByIds(deleteAuthUserRoles)
    }
    const modify = await this.userRepo.saveAuthUserRole(dataModify)
    return modify
  }
}
