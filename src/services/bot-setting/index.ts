import { Inject } from '@nestjs/common'
import {
  BotSettingDomain,
  QueryBotSetting,
  RawBotSetting,
} from 'src/domains/setting/bot-setting.types'
import { BotSettingRepository } from 'src/repositories/bot-setting.repository'
import { BaseService } from '../base'
export class BotSettingService
  extends BaseService<QueryBotSetting, RawBotSetting>
  implements BotSettingDomain
{
  constructor(
    @Inject(BotSettingRepository)
    private botSettingRepository: BotSettingRepository
  ) {
    super(botSettingRepository)
  }
}
