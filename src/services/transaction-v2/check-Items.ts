import { HttpStatus, Inject } from '@nestjs/common'
import e from 'express'
import { BOT_STATUS, ERROR_CODE, PACKAGE_TYPE, ROLE_STATUS } from 'src/const'
import { MERCHANT_TYPE } from 'src/const/authorization'
import { ORDER_CATEGORY } from 'src/const/transaction'
import {
  ADDITIONAL_DATA_TYPE,
  MERCHANT_ADDITIONAL_DATA_STATUS,
} from 'src/domains/merchant/types'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { BotRepository } from 'src/repositories/bot.repository'
import { MerchantAdditionalDataRepository } from 'src/repositories/merchant-additional-data.repository'
import { MerchantCommissionRepository } from 'src/repositories/merchant-commission.repository'
import { MerchantRepository } from 'src/repositories/merchant.repository'
import { PackageRepository } from 'src/repositories/package.ropository'
import { PermissionRepository } from 'src/repositories/permission.repository'
import { CreateBalanceAndFeeDataHandler } from 'src/usecases/bot/user-create-balance-and-fee'
import { throwError } from 'src/utils/handle-error.util'

export class CheckItemService {
  constructor(
    @Inject(PermissionRepository)
    private permissionRepository: PermissionRepository,
    @Inject(BotRepository)
    private botRepository: BotRepository,
    @Inject(PackageRepository) private packageRepository: PackageRepository,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(MerchantCommissionRepository)
    private merchantCommissionRepository: MerchantCommissionRepository,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
    @Inject(MerchantAdditionalDataRepository)
    private merchantAdditionalDataRepository: MerchantAdditionalDataRepository,
    @Inject(CreateBalanceAndFeeDataHandler)
    private createBalanceAndFeeDataHandler: CreateBalanceAndFeeDataHandler
  ) {}
  async execute(param: {
    merchant_code: string
    items: {
      id: string
      quantity: number
      type: PACKAGE_TYPE
      category: ORDER_CATEGORY
      balance?: number
    }[]
    amount: string
  }): Promise<{
    items: {
      id: string
      name: string
      price: string
      quantity: number
      type: PACKAGE_TYPE
      category: ORDER_CATEGORY
      discount_rate: number
      discount_amount: number
      commission_rate: number
      commission_cash: number
      balance?: number
    }[]
    commission_cash: number
  }> {
    const merchant = await this.merchantRepository.findOne({
      code: param.merchant_code,
    })
    const sbotIds: string[] = []
    const roleIds: string[] = []
    const tbotIds: string[] = []
    const items = []
    for (let item of param.items) {
      switch (item.category) {
        case ORDER_CATEGORY.SBOT:
          sbotIds.push(item.id)
          break
        case ORDER_CATEGORY.PKG:
          roleIds.push(item.id)
          break
        case ORDER_CATEGORY.TBOT:
          tbotIds.push(item.id)
          break
      }
    }
    let totalCommission = 0
    let totalAmount = 0
    const roles =
      roleIds.length === 0
        ? []
        : await this.permissionRepository.findRoleByIds(roleIds)
    const sbots =
      sbotIds.length === 0 ? [] : await this.botRepository.findByIds(sbotIds)
    const tbots =
      tbotIds.length === 0
        ? []
        : await this.botTradingRepository.findByIds(tbotIds)

    let packages: {
      discount_rate?: number
      discount_amount?: number
      status: boolean
      quantity: number
      type: PACKAGE_TYPE
      owner_created: string
    }[]
    // check merchant MERCHANT_TYPE === OTHERS
    // get package from additional data
    if (merchant && merchant.config.domain_type === MERCHANT_TYPE.OTHERS) {
    } else {
      // else
      // get package from packageRepository
      packages = await this.packageRepository.find({
        status: true,
      })
    }
    for (let r of roles) {
      if (merchant && merchant.config.domain_type === MERCHANT_TYPE.OTHERS) {
        const additional = await this.merchantAdditionalDataRepository.list({
          merchant_id: merchant.id,
          type: ADDITIONAL_DATA_TYPE.PKG_PERIOD,
          status: MERCHANT_ADDITIONAL_DATA_STATUS.ON,
        })
        packages = additional.map((e) => {
          if (e?.data?.translation?.en) {
            return e.data.translation.en
          } else {
            return {}
          }
        })
      }
      const roleOrder = param.items.find((i) => i.id === r.id)
      if (r.status === ROLE_STATUS.OPEN && roleOrder) {
        let commissionRate = 0
        if (merchant) {
          const merchantCommission =
            await this.merchantCommissionRepository.findOne({
              merchant_id: merchant.id,
              asset_id: r.id,
            })
          if (merchantCommission && merchantCommission.commission) {
            commissionRate = Number(merchantCommission.commission)
          }
        }
        const packageTime = packages.find(
          (p) => p.type === roleOrder.type && p.quantity == roleOrder.quantity
        )
        let discountRate = 0
        let discountAmount = 0
        if (packageTime) {
          discountRate = Number(packageTime.discount_rate) || 0
          discountAmount = Number(packageTime.discount_amount) || 0
        }
        const price = Number(r.price) || 0
        const quantity = roleOrder.quantity || 0

        const sellAmount =
          price * quantity - price * quantity * discountRate - discountAmount

        totalAmount += sellAmount
        totalCommission += sellAmount * commissionRate
        items.push({
          id: r.id,
          name: r.role_name,
          price: r.price,
          quantity: roleOrder.quantity,
          type: roleOrder.type,
          category: ORDER_CATEGORY.PKG,
          discount_rate: discountRate,
          discount_amount: discountAmount,
          commission_rate: commissionRate,
          commission_cash: sellAmount * commissionRate,
        })
      } else {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.ROLE_INVALID,
        })
      }
    }
    for (let b of sbots) {
      if (merchant && merchant.config.domain_type === MERCHANT_TYPE.OTHERS) {
        const additional = await this.merchantAdditionalDataRepository.list({
          merchant_id: merchant.id,
          type: ADDITIONAL_DATA_TYPE.SBOT_PERIOD,
          status: MERCHANT_ADDITIONAL_DATA_STATUS.ON,
        })
        packages = additional.map((e) => {
          if (e?.data?.translation?.en) {
            return e.data.translation.en
          } else {
            return {}
          }
        })
      }
      const botOrder = param.items.find((i) => i.id === b.id)
      if (b.status === BOT_STATUS.OPEN && botOrder) {
        let commissionRate = 0
        if (merchant) {
          const merchantCommission =
            await this.merchantCommissionRepository.findOne({
              merchant_id: merchant.id,
              asset_id: b.id,
            })
          if (merchantCommission && merchantCommission.commission) {
            commissionRate = Number(merchantCommission.commission)
          }
        }
        const packageTime = packages.find(
          (p) => p.type === botOrder.type && p.quantity == botOrder.quantity
        )
        let discountRate = 0
        let discountAmount = 0
        if (packageTime) {
          discountRate = Number(packageTime.discount_rate) || 0
          discountAmount = Number(packageTime.discount_amount) || 0
        }
        const price = Number(b.price) || 0
        const quantity = botOrder.quantity || 0
        const sellAmount =
          price * quantity - price * quantity * discountRate - discountAmount
        totalAmount += sellAmount
        totalCommission += sellAmount * commissionRate
        items.push({
          id: b.id,
          name: b.name,
          price: b.price,
          quantity: botOrder.quantity,
          type: botOrder.type,
          category: ORDER_CATEGORY.SBOT,
          discount_rate: discountRate,
          discount_amount: discountAmount,
          commission_rate: commissionRate,
          commission_cash: sellAmount * commissionRate,
        })
      } else {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.BOT_INVALID,
        })
      }
    }
    for (let b of tbots) {
      if (merchant && merchant.config.domain_type === MERCHANT_TYPE.OTHERS) {
        const additional = await this.merchantAdditionalDataRepository.list({
          merchant_id: merchant.id,
          type: ADDITIONAL_DATA_TYPE.TBOT_PERIOD,
          status: MERCHANT_ADDITIONAL_DATA_STATUS.ON,
        })
        packages = additional.map((e) => {
          if (e?.data?.translation?.en) {
            return e.data.translation.en
          } else {
            return {}
          }
        })
      }
      const botOrder = param.items.find((i) => i.id === b.id)
      if (b.status === BOT_STATUS.OPEN && botOrder) {
        let commissionRate = 0
        if (merchant) {
          const merchantCommission =
            await this.merchantCommissionRepository.findOne({
              merchant_id: merchant.id,
              asset_id: b.id,
            })
          if (merchantCommission && merchantCommission.commission) {
            commissionRate = Number(merchantCommission.commission)
          }
        }
        const packageTime = packages.find(
          (p) => p.type === botOrder.type && p.quantity == botOrder.quantity
        )
        let discountRate = 0
        let discountAmount = 0
        if (packageTime) {
          discountRate = Number(packageTime.discount_rate) || 0
          discountAmount = Number(packageTime.discount_amount) || 0
        }
        // const price = Number(b.price) || 0 // TODO get fee by balance
        const getFee = await this.createBalanceAndFeeDataHandler.execute({ balance: Number(botOrder.balance), bot_id: b.id})
        const price = Number(getFee.total_price)
        const quantity = botOrder.quantity || 0
        const sellAmount =
          price * quantity - price * quantity * discountRate - discountAmount
        totalAmount += sellAmount
        totalCommission += sellAmount * commissionRate
        items.push({
          id: b.id,
          name: b.name,
          price: price,
          quantity: botOrder.quantity,
          type: botOrder.type,
          category: ORDER_CATEGORY.TBOT,
          discount_rate: discountRate,
          discount_amount: discountAmount,
          commission_rate: commissionRate,
          commission_cash: sellAmount * commissionRate,
          balance: botOrder.balance
        })
      } else {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.BOT_INVALID,
        })
      }
    }
    if (Number(param.amount) !== Number(totalAmount.toFixed(2))) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.AMOUNT_INVALID,
      })
    }
    return { items, commission_cash: totalCommission }
  }
}
