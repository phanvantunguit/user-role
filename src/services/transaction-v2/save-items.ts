import { HttpStatus, Inject } from '@nestjs/common'
import { BOT_STATUS, ERROR_CODE, PACKAGE_TYPE, ROLE_STATUS } from 'src/const'
import { ITEM_STATUS, ORDER_CATEGORY, TBOT_TYPE } from 'src/const/transaction'
import { BotRepository } from 'src/repositories/bot.repository'
import { PermissionRepository } from 'src/repositories/permission.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { throwError } from 'src/utils/handle-error.util'
export class SaveItemService {
  constructor(
    @Inject(PermissionRepository)
    private permissionRepository: PermissionRepository,
    @Inject(UserRepository)
    private userRepository: UserRepository
  ) {}
  async execute(
    param: {
      items: {
        id: string
        name: string
        price: string
        quantity: number
        type: PACKAGE_TYPE
        category: ORDER_CATEGORY
        discount_rate: number
        discount_amount: number
        balance?: number
      }[]
      order_id: string
      user_id: string
      owner_created: string
    },
    queryRunner
  ) {
    const userAssetLogs = param.items.map((item) => {
      const time = new Date()
      if (item.type === PACKAGE_TYPE.MONTH) {
        time.setMonth(time.getMonth() + item.quantity)
      } else {
        time.setDate(time.getDate() + item.quantity)
      }
      const data = {
        order_id: param.order_id,
        user_id: param.user_id,
        owner_created: param.owner_created,
        expires_at: time.valueOf(),
        status: ITEM_STATUS.PROCESSING,
        asset_id: item.id,
        category: item.category,
        package_type: item.type,
        quantity: item.quantity,
        name: item.name,
        price: item.price,
        discount_rate: item.discount_rate,
        discount_amount: item.discount_amount,
        type: TBOT_TYPE.BUY,
      }
      if (item.balance) {
        data['metadata'] = { balance: item.balance }
      }
      return data
    })
    return this.userRepository.saveUserAssetLog(userAssetLogs, queryRunner)
  }
}
