import { Inject } from '@nestjs/common'
import { PAYMENT_METHOD } from 'src/const/transaction'
import { PaymentDomain } from 'src/domains/payment/payment.types'
import { CoinpaymentService } from './coinpayment'
export class PaymentService {
  constructor(
    @Inject(CoinpaymentService) private coinpaymentService: CoinpaymentService
  ) {}
  getService(method: PAYMENT_METHOD): PaymentDomain {
    switch (method) {
      case PAYMENT_METHOD.COIN_PAYMENT:
        return this.coinpaymentService
      default:
        throw new Error(`Payment method ${method} not implemented`)
    }
  }
}
