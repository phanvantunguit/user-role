import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import {
  COIN_PAYMENT_IPN_TYPE,
  TRANSACTION_EVENT,
  TRANSACTION_STATUS,
} from 'src/const/transaction'
import { PaymentDomain } from 'src/domains/payment/payment.types'
import { RawTransaction, RawTransactionLog } from 'src/domains/transaction'
import { TransactionRepository } from 'src/repositories/transaction.repository'
import { CoinpaymentResource } from 'src/resources/coinpayment'
import { throwError } from 'src/utils/handle-error.util'
import { TransactionLogService } from '../transaction-log'
export class CoinpaymentService implements PaymentDomain {
  constructor(
    @Inject(CoinpaymentResource)
    private coinpaymentResource: CoinpaymentResource,
    private transactionRepository: TransactionRepository,
    private transactionLogService: TransactionLogService
  ) {}
  async ipnTransaction(ipnData: any) {
    let checktrans
    let transaction
    switch (ipnData.ipn_type) {
      case COIN_PAYMENT_IPN_TYPE.API: {
        checktrans = await this.coinpaymentResource.checkTransaction(
          ipnData.txn_id
        )
        transaction = await this.transactionRepository.findOne({
          wallet_address: checktrans.result.payment_address,
        })
        break
      }
      case COIN_PAYMENT_IPN_TYPE.WITHDRAWAL: {
        checktrans = await this.coinpaymentResource.checkWithdrawal(ipnData.id)
        checktrans.result['id'] = ipnData.id
        transaction = await this.transactionRepository.findOne({
          wallet_address: checktrans.result.send_address,
        })
        break
      }
    }
    if (checktrans.error !== 'ok') {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BAD_REQUEST,
      })
    }
    let transaction_event = TRANSACTION_EVENT.PAYMENT_PROCESSING
    if (Number(checktrans.result.status) >= 100) {
      transaction_event = TRANSACTION_EVENT.PAYMENT_COMPLETE
    } else if (Number(checktrans.result.status) < 0) {
      transaction_event = TRANSACTION_EVENT.PAYMENT_FAILED
    } else if (checktrans.result.status === 0) {
      transaction_event = TRANSACTION_EVENT.PAYMENT_CREATED
    }
    return {
      transaction,
      transaction_event,
      metadata: checktrans,
    }
  }
  async createTransaction(
    transaction: RawTransaction,
    email: string
  ): Promise<any> {
    const resTrans = await this.coinpaymentResource.createTransaction({
      sell_amount: Number(transaction.sell_amount),
      sell_currency: transaction.sell_currency,
      buy_currency: transaction.buy_currency,
      buyer_email: email,
    })
    if (resTrans.error !== 'ok') {
      throw resTrans
    }
    // await this.transactionRepository.save({
    //   ...transaction,
    //   payment_id: resTrans.result.txn_id,
    //   buy_amount: resTrans.result.amount,
    //   wallet_address: resTrans.result.address,
    // })
    // await this.transactionLogService.save({
    //   transaction_id: transaction.id,
    //   transaction_event: TRANSACTION_EVENT.PAYMENT_CREATED,
    //   transaction_status: TRANSACTION_STATUS.CREATED,
    //   metadata: resTrans,
    // })
    return {
      buy_amount: resTrans.result.amount,
      txn_id: resTrans.result.txn_id,
      wallet_address: resTrans.result.address,
      timeout: resTrans.result.timeout,
      qrcode_url: resTrans.result.qrcode_url,
      status_url: resTrans.result.status_url,
      checkout_url: resTrans.result.checkout_url,
      metadata: resTrans,
    }
  }
  async extraDataPaymentInfo(payload: RawTransactionLog) {
    const time = `${Number(payload.metadata.result.timeout) / 60} minutes`
    return {
      wallet_address: payload.metadata.result.address,
      time,
      checkout_url: payload.metadata.result.checkout_url,
      qrcode_url: payload.metadata.result.qrcode_url,
      status_url: payload.metadata.result.status_url,
    }
  }
  async checkTransaction(payment_id: string) {
    const checktrans = await this.coinpaymentResource.checkTransaction(
      payment_id
    )
    let transaction_event = TRANSACTION_EVENT.PAYMENT_PROCESSING
    if (checktrans.error !== 'ok') {
      transaction_event = TRANSACTION_EVENT.PAYMENT_FAILED
      return {
        transaction_event,
        metadata: checktrans,
      }
    }
    if (Number(checktrans.result.status) >= 100) {
      transaction_event = TRANSACTION_EVENT.PAYMENT_COMPLETE
    } else if (Number(checktrans.result.status) < 0) {
      transaction_event = TRANSACTION_EVENT.PAYMENT_FAILED
    } else if (checktrans.result.status === 0) {
      transaction_event = TRANSACTION_EVENT.PAYMENT_CREATED
    }
    return {
      transaction_event,
      metadata: checktrans,
    }
  }
}
