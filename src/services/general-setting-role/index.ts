import { PermissionRepository } from 'src/repositories/permission.repository'
import { HttpStatus, Inject } from '@nestjs/common'
import {
  GeneralSettingRoleDomain,
  GeneralSettingRoleCreate,
} from 'src/domains/permission'
import { ERROR_CODE, GENERAL_SETTING_LIMIT_TAB } from 'src/const'
import { throwError } from 'src/utils/handle-error.util'
import { SettingRepository } from 'src/repositories/setting.repository'
import { RoleService } from '../role'
export class GeneralSettingRoleService implements GeneralSettingRoleDomain {
  constructor(
    private roleService: RoleService,
    @Inject(SettingRepository) private settingRepo: SettingRepository,
    @Inject(PermissionRepository) private permissionRepo: PermissionRepository
  ) {}
  async getMaxLimitTab(roleIds: any): Promise<number> {
    const currentGSRoles =
      await this.permissionRepo.findGeneralSettingMultipleRoles(
        roleIds,
        GENERAL_SETTING_LIMIT_TAB
      )
    if (currentGSRoles.length > 0) {
      return Math.max(...currentGSRoles.map((st) => st.val_limit))
    }
    return 1
  }
  async modifyGeneralSettingRole(
    params: GeneralSettingRoleCreate
  ): Promise<any> {
    const { owner_created, general_settings, role_id } = params
    await this.roleService.checkRoleNotFoundAndThrow({
      id: role_id,
    })
    // const general_setting_ids = general_settings.map(e => e.general_setting_id)
    // let dataGeneralSettings = await this.getGeneralSettingOrThrow(general_setting_ids)
    // dataGeneralSettings = dataGeneralSettings.map(e => {
    //     const findFReq = general_settings.find(g => g.general_setting_id === e.general_setting_id)
    //     return  {
    //         ...e,
    //         general_setting_role_description: findFReq.description,
    //         val_limit: findFReq.val_limit
    //     }
    // })
    const currentGSRoles = await this.permissionRepo.findGeneralSettingRoles({
      role_id,
    })
    const dataModify = []
    for (const gReq of general_settings) {
      if (gReq.val_limit) {
        const gExistIndex = currentGSRoles.findIndex(
          (cgsr) => cgsr.general_setting_id === gReq.general_setting_id
        )
        if (gExistIndex > -1) {
          if (
            currentGSRoles[gExistIndex].description !== gReq.description ||
            Number(currentGSRoles[gExistIndex].val_limit) !==
              Number(gReq.val_limit)
          ) {
            dataModify.push({
              ...currentGSRoles[gExistIndex],
              description: gReq.description,
              val_limit: gReq.val_limit,
              owner_created,
            })
          }
          currentGSRoles.splice(gExistIndex, 1)
        } else {
          dataModify.push({
            ...gReq,
            role_id,
            owner_created,
          })
        }
      }
    }
    const deleteGSR = currentGSRoles.map((aur) => aur.id)
    if (deleteGSR.length > 0) {
      await this.permissionRepo.deleteGeneralSettingRolesByIds(deleteGSR)
    }
    await this.permissionRepo.saveGeneralSettingRoles(dataModify)
    // role[0].root.general_settings = dataGeneralSettings
    // const updateRole = await this.permissionRepo.saveRoles(role)
    const roles = await this.permissionRepo.findRoles({ id: role_id })
    return roles[0]
  }
  private async getGeneralSettingOrThrow(general_setting_ids: string[]) {
    const generalSettings = await this.settingRepo.findGeneralSettingByIds(
      general_setting_ids
    )
    if (generalSettings.length !== general_setting_ids.length) {
      const gsIdsNotFound = []
      for (const f of generalSettings) {
        if (!general_setting_ids.includes(f.general_setting_id)) {
          gsIdsNotFound.push(f.general_setting_id)
        }
      }
      throwError({
        status: HttpStatus.NOT_FOUND,
        error_code: ERROR_CODE.FEATURE_NOT_FOUND.error_code,
        message: gsIdsNotFound,
      })
    }
    return generalSettings
  }
}
