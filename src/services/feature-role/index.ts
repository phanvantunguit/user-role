import { PermissionRepository } from 'src/repositories/permission.repository'
import { HttpStatus, Inject } from '@nestjs/common'
import { FeatureRoleDomain, FeatureRoleCreate } from 'src/domains/permission'
import { ERROR_CODE } from 'src/const'
import { throwError } from 'src/utils/handle-error.util'
import { SettingRepository } from 'src/repositories/setting.repository'
import { RoleService } from '../role'
export class FeatureRoleService implements FeatureRoleDomain {
  constructor(
    private roleService: RoleService,
    @Inject(SettingRepository) private settingRepo: SettingRepository,
    @Inject(PermissionRepository) private permissionRepo: PermissionRepository
  ) {}
  async modifyFeatureRole(params: FeatureRoleCreate): Promise<any> {
    const { owner_created, features, role_id } = params
    await this.roleService.checkRoleNotFoundAndThrow({
      id: role_id,
    })
    // const feature_ids = features.map(e => e.feature_id)
    // let dataFeature = await this.getFeatureOrThrow(feature_ids)
    // dataFeature = dataFeature.map(e => {
    //     const findFReq = features.find(f => f.feature_id === e.feature_id)
    //     return  {
    //         ...e,
    //         feature_role_description: findFReq.description
    //     }
    // })
    const currentFeatureRoles = await this.permissionRepo.findFeatureRoles({
      role_id,
    })
    const dataModify = []
    for (const fReq of features) {
      const frExistIndex = currentFeatureRoles.findIndex(
        (cfr) => cfr.feature_id === fReq.feature_id
      )
      if (frExistIndex > -1) {
        if (
          currentFeatureRoles[frExistIndex].description !== fReq.description
        ) {
          dataModify.push({
            ...currentFeatureRoles[frExistIndex],
            description: fReq.description,
            owner_created,
          })
        }
        currentFeatureRoles.splice(frExistIndex, 1)
      } else {
        dataModify.push({
          ...fReq,
          role_id,
          owner_created,
        })
      }
    }
    const deleteFeatureRoles = currentFeatureRoles.map((aur) => aur.id)
    if (deleteFeatureRoles.length > 0) {
      await this.permissionRepo.deleteFeatureRolesByIds(deleteFeatureRoles)
    }
    await this.permissionRepo.saveFeatureRoles(dataModify)
    // role[0].root.features = dataFeature
    // const updateRole = await this.permissionRepo.saveRoles(role)
    const roles = await this.permissionRepo.findRoles({ id: role_id })
    return roles[0]
  }
  private async getFeatureOrThrow(feature_ids: string[]) {
    const features = await this.settingRepo.findFeatureByIds(feature_ids)
    if (features.length !== feature_ids.length) {
      const featureIdsNotFound = []
      for (const f of features) {
        if (!feature_ids.includes(f.feature_id)) {
          featureIdsNotFound.push(f.feature_id)
        }
      }
      throwError({
        status: HttpStatus.NOT_FOUND,
        error_code: ERROR_CODE.FEATURE_NOT_FOUND.error_code,
        message: featureIdsNotFound,
      })
    }
    return features
  }
}
