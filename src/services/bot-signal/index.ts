import { Inject } from '@nestjs/common'
import {
  BotSignalDomain,
  QueryBotSignal,
  RawBotSignal,
} from 'src/domains/bot/bot-signal.types'
import { BotSignalRepository } from 'src/repositories/bot-signal.repository'
import { BaseService } from '../base'
export class BotSignalService
  extends BaseService<QueryBotSignal, RawBotSignal>
  implements BotSignalDomain
{
  constructor(
    @Inject(BotSignalRepository)
    private botSignalRepository: BotSignalRepository
  ) {
    super(botSignalRepository)
  }
  async saveSignal(params: {
    exchange: 'BINANCE'
    symbol: 'LTCUSDT'
    name: 'botBoxCandle'
    resolution: '5m'
    signal_id: 'fad731ed_1673254427814'
    type: 'BUY'
    time: 1671527700000
    image_url: 'https://static.cextrading.io/images/Bot_Box/Bot_Box_LTCUSDT_5m_1671527700000.png'
    metadata: {
      idSignal: 'fad731ed_1673254427814'
      positionSide: 'LONG'
      side: 'BUY'
    }
    created_at: 1673254434062
  }): Promise<any> {
    const signal = await this.botSignalRepository.findOne({
      signal_id: params.signal_id,
    })
    if (signal) {
      return false
    }
    const newSignal = {
      id: params.signal_id,
      exchange: params.exchange,
      symbol: params.symbol,
      name: params.name,
      resolution: params.resolution,
      signal_id: params.signal_id,
      type: params.type,
      time: params.time,
      image_url: params.image_url,
      metadata: params.metadata,
    }
    const saved = await this.botSignalRepository.save(newSignal)
    return saved
  }
}
