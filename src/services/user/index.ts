import { HttpStatus, Inject } from '@nestjs/common'
import {
  DEFAULT_ROLE,
  ERROR_CODE,
  MERCHANT_STATUS,
  SECOND,
  USER_TYPE,
  VERIFY_TOKEN_TYPE,
} from 'src/const'
import {
  UserRegister,
  UserDomain,
  UserLogin,
  UserLoginResponse,
  EmailSend,
  UserResetPassword,
  UserUpdateInfo,
  UserCreate,
  RawUser,
  QueryUser,
  QueryUserPagination,
  RawUserView,
  ChangeEmail,
  ChangeEmailVerify,
} from 'src/domains/user'
import { MailResource } from 'src/resources/mail'
import { APP_CONFIG } from 'src/config'
import { UserRepository } from 'src/repositories/user.repository'
import * as hashUtil from 'src/utils/hash.util'
import { throwError } from 'src/utils/handle-error.util'
import { VerifyTokenService } from 'src/services/verify-token'
import { generatePassword } from 'src/utils/format-data.util'
import * as uaParser from 'ua-parser-js'
import { SessionService } from '../session'
import { v4 } from 'uuid'
import { MerchantRepository } from 'src/repositories/merchant.repository'
import { MERCHANT_TYPE } from 'src/const/authorization'
import { APP_SETTING } from 'src/const/app-setting'
export class UserService implements UserDomain {
  constructor(
    @Inject(MailResource) private mailResource: MailResource,
    private verifyTokenService: VerifyTokenService,
    private userRepo: UserRepository,
    private sessionService: SessionService,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository
  ) {}
  async getProfile(params: QueryUser, merchantCode?: string): Promise<any> {
    // get one user and remove role expired
    const findUser = await this.userRepo.findOneUser({ id: params.id })
    if (merchantCode) {
      if (findUser.merchant_code !== merchantCode) {
        throwError({
          status: HttpStatus.FORBIDDEN,
          ...ERROR_CODE.ACCOUNT_INVALID,
        })
      }
    } else {
      const merchant = await this.merchantRepository.findOne({
        code: findUser.merchant_code,
      })
      if (merchant && merchant.config.domain_type === MERCHANT_TYPE.OTHERS) {
        throwError({
          status: HttpStatus.FORBIDDEN,
          ...ERROR_CODE.ACCOUNT_INVALID,
        })
      }
    }
    const user = await this.userRepo.findOneUserView(params)
    const userMerchant = await this.userRepo.findOneUser(params)
    const roles = user.roles.filter((r) => {
      if (r.expires_at) {
        if (Number(r.expires_at) <= Date.now()) {
          return false
        }
      }
      return true
    })
    return {
      ...user,
      roles,
      merchant_code: userMerchant.merchant_code,
    }
  }
  async getUserPagination(params: QueryUserPagination): Promise<any> {
    const {
      page,
      size,
      keyword,
      is_admin,
      role_ids,
      roles,
      bots,
      merchant_code,
    } = params
    // let roles = []

    // if (role_ids) {
    //   const roleIds = role_ids.split(',')
    //   roles = await this.permissionRepository.findRoleByIds(roleIds)
    // }
    if (is_admin) {
      return await this.userRepo.findUserViewPaging({
        page,
        size,
        keyword,
        is_admin,
        // roles,
      })
    } else {
      return await this.userRepo.findUserPaging({
        page,
        size,
        keyword,
        roles: roles ? roles.split(',') : [],
        bots: bots ? bots.split(',') : [],
        merchant_code,
      })
    }
  }
  async getUser(params: QueryUser): Promise<RawUserView> {
    const user = await this.userRepo.findOneUserView(params)
    return user
  }
  async listUser(params: {
    keyword?: string
    is_admin?: boolean
  }): Promise<RawUser[]> {
    const user = await this.userRepo.findUser(params)
    return user
  }
  async updateInfo(params: UserUpdateInfo, isAdmin: boolean): Promise<any> {
    const findUser = await this.userRepo.findOneUser({ id: params.id })
    if (!findUser) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.USER_NOT_FOUND,
      })
    }
    // if (findUser.is_admin !== isAdmin) {
    //   throwError({
    //     status: HttpStatus.FORBIDDEN,
    //     ...ERROR_CODE.NO_PERMISSION,
    //   })
    // }
    if (params.password) {
      const passIsCorrect = await hashUtil.comparePassword(
        params.old_password,
        findUser.password
      )
      if (!passIsCorrect) {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.PASSWORD_INCORRECT,
        })
      }
    }
    delete params.old_password
    if (params.username) {
      const check = await this.checkUsername(params.username, findUser.id)
      if (!check) {
        throwError({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          ...ERROR_CODE.USERNAME_EXISTED,
        })
      }
    }
    const userUpdated = await this.userRepo.saveUser(params)
    delete userUpdated.password
    return userUpdated
  }
  async sendEmailForgotPassword(params: EmailSend): Promise<any> {
    const email = params.email.toLocaleLowerCase()
    const findUser = await this.userRepo.findOneUser({
      email,
      merchant_code: params.merchant_code,
    })
    if (!findUser) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.EMAIL_NOT_FOUND,
      })
    }
    await this.userRepo.deleteToken({
      user_id: findUser.id,
      type: VERIFY_TOKEN_TYPE.RESET_PASSWORD,
    })
    const verifyToken = await this.verifyTokenService.createTokenUUID({
      user_id: findUser.id,
      expires_at:
        Date.now() + Number(APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * SECOND,
      type: VERIFY_TOKEN_TYPE.RESET_PASSWORD,
    })
    if (verifyToken) {
      const merchant = await this.merchantRepository.findOne({
        code: findUser.merchant_code,
      })
      let link = merchant ? merchant.domain : ''
      let from_email
      let from_name
      let logo_url
      let header_url
      let twitter_url
      let youtube_url
      let facebook_url
      let telegram_url
      let company_name
      if (merchant && merchant.config) {
        if (
          merchant.config['verified_sender'] &&
          merchant.config['email_sender']
        ) {
          from_email = merchant.config['email_sender']['from_email']
          from_name = merchant.config['email_sender']['from_name']
        }
        if (merchant.config['social_media']) {
          twitter_url = merchant.config['social_media']['twitter_url']
          youtube_url = merchant.config['social_media']['youtube_url']
          facebook_url = merchant.config['social_media']['facebook_url']
          telegram_url = merchant.config['social_media']['telegram_url']
        }
        logo_url = merchant.config['email_logo_url']
        header_url = merchant.config['email_banner_url']
        if (merchant.config.domain_type === MERCHANT_TYPE.OTHERS) {
          company_name = merchant.name
          from_email = from_email || APP_CONFIG.SENDGRID_SENDER_ALGO_EMAIL
          from_name = from_name || APP_CONFIG.SENDGRID_SENDER_ALGO_NAME
          logo_url = logo_url || APP_CONFIG.ALGO_LOGO_EMAIL
          header_url = header_url || APP_CONFIG.ALGO_BANNER_EMAIL
        }
      }
      const resultSendMail = await this.mailResource.sendEmailForgotPassword(
        params.email,
        verifyToken.token,
        verifyToken.expires_at,
        link,
        from_email,
        from_name,
        logo_url,
        header_url,
        twitter_url,
        youtube_url,
        facebook_url,
        telegram_url,
        company_name,
        `${findUser.first_name || ''} ${findUser.last_name || ''}`
      )
      if (resultSendMail) {
        return resultSendMail
      }
    }
    throwError({
      status: HttpStatus.UNPROCESSABLE_ENTITY,
      ...ERROR_CODE.SEND_EMAIL_FAILED,
    })
  }
  async resetPassword(params: UserResetPassword): Promise<any> {
    const { token, password } = params
    const verifyToken = await this.userRepo.findOneToken({
      token,
    })
    if (verifyToken) {
      if (verifyToken.expires_at < Date.now()) {
        throwError({
          status: HttpStatus.GONE,
          ...ERROR_CODE.SESSION_VERIFY_EMAIL_EXPIRED,
        })
      }
      const findUser = await this.userRepo.findOneUser({
        id: verifyToken.user_id,
      })
      if (!findUser) {
        throwError({
          status: HttpStatus.NOT_FOUND,
          ...ERROR_CODE.USER_NOT_FOUND,
        })
      }
      await this.userRepo.saveUser({
        id: findUser.id,
        password,
      })
      await this.userRepo.deleteToken({
        token: verifyToken.token,
      })
      return 'Reset password successfully'
    }
    throwError({
      status: HttpStatus.UNAUTHORIZED,
      ...ERROR_CODE.SESSION_INVALID,
    })
  }
  async loginUser(
    params: UserLogin,
    userAgent: string,
    ipAddress: string,
    merchantCode?: string
  ): Promise<UserLoginResponse> {
    const email = params.email.toLocaleLowerCase()
    const queryUser = {
      email,
    }
    if (merchantCode) {
      queryUser['merchant_code'] = merchantCode
    }
    const findUser = await this.userRepo.findOneUser(queryUser)
    if (!findUser) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.EMAIL_NOT_FOUND,
      })
    }
    if (!findUser.email_confirmed) {
      throwError({
        status: HttpStatus.FORBIDDEN,
        ...ERROR_CODE.EMAIL_NOT_VERIFIED,
      })
    }
    if (merchantCode) {
      if (findUser.merchant_code !== merchantCode) {
        throwError({
          status: HttpStatus.FORBIDDEN,
          ...ERROR_CODE.ACCOUNT_INVALID,
        })
      }
    } else {
      const merchant = await this.merchantRepository.findOne({
        code: findUser.merchant_code,
      })
      if (merchant && merchant.config.domain_type === MERCHANT_TYPE.OTHERS) {
        throwError({
          status: HttpStatus.FORBIDDEN,
          ...ERROR_CODE.ACCOUNT_INVALID,
        })
      }
    }
    const passIsCorrect = await hashUtil.comparePassword(
      params.password,
      findUser.password
    )
    if (!passIsCorrect) {
      throwError({
        status: HttpStatus.UNAUTHORIZED,
        ...ERROR_CODE.LOGIN_FAILED,
      })
    }
    if (!findUser.active) {
      throwError({
        status: HttpStatus.FORBIDDEN,
        ...ERROR_CODE.DEACTIVE_ACCOUNT,
      })
    }
    let roles = []
    if (params.user_type === USER_TYPE.USER) {
      const userRoles = await this.userRepo.findUserRole({
        user_id: findUser.id,
      })
      roles = userRoles.map((r) => r.role_id)
    } else {
      const authRoles = await this.userRepo.findAuthUserRole({
        user_id: findUser.id,
      })
      roles = authRoles.map((r) => r.auth_role_id)
    }
    if (params.user_type === USER_TYPE.ADMIN && !findUser.is_admin) {
      throwError({
        status: HttpStatus.FORBIDDEN,
        ...ERROR_CODE.NO_PERMISSION,
      })
    }
    const expiresIn =
      params.user_type === USER_TYPE.USER
        ? APP_CONFIG.TIME_EXPIRED_LOGIN
        : 30 * 60
    const expiresAt = Date.now() + expiresIn * 1000
    const tokenId = v4()
    const token = hashUtil.generateToken(
      {
        user_id: findUser.id,
        email: findUser.email,
        merchant_code: findUser.merchant_code,
        iss: 'cextrading',
        roles,
        super_user: findUser.super_user,
        token_id: tokenId,
      },
      APP_CONFIG.TOKEN_PRIVATE_KEY,
      expiresIn
    )
    const dataUA = uaParser(userAgent)
    const dataSession = {
      user_id: findUser.id,
      token: token,
      name_device: dataUA.os.name,
      browser: dataUA.browser.name,
      ip_number: ipAddress,
      last_login: Date.now(),
      expires_at: expiresAt,
      enabled: true,
      token_id: tokenId,
    }
    await this.sessionService.save(dataSession)
    return {
      token,
    }
  }
  async verifyEmailUser(token: string): Promise<RawUser> {
    const verifyToken = await this.userRepo.findOneToken({
      token,
    })
    if (verifyToken) {
      // if (verifyToken.expires_at < Date.now()) {
      //   throwError({
      //     status: HttpStatus.GONE,
      //     ...ERROR_CODE.SESSION_VERIFY_EMAIL_EXPIRED,
      //   })
      // }
      const findUser = await this.userRepo.findOneUser({
        id: verifyToken.user_id,
      })
      if (!findUser) {
        throwError({
          status: HttpStatus.NOT_FOUND,
          ...ERROR_CODE.USER_NOT_FOUND,
        })
      }
      if (findUser.email_confirmed) {
        return findUser
      }
      await this.userRepo.saveUser({
        id: findUser.id,
        active: true,
        email_confirmed: true,
      })
      return findUser
    }
    throwError({
      status: HttpStatus.UNAUTHORIZED,
      ...ERROR_CODE.SESSION_INVALID,
    })
  }
  async userConfirmEmail(token: string) {
    const verifyToken = await this.userRepo.findOneToken({
      token,
    })
    if (verifyToken) {
      if (verifyToken.expires_at < Date.now()) {
        throwError({
          status: HttpStatus.OK,
          ...ERROR_CODE.SESSION_VERIFY_EMAIL_EXPIRED,
        })
      }
      const findUser = await this.userRepo.findOneUser({
        id: verifyToken.user_id,
      })
      if (!findUser) {
        throwError({
          status: HttpStatus.NOT_FOUND,
          ...ERROR_CODE.USER_NOT_FOUND,
        })
      }
      if (verifyToken.type === VERIFY_TOKEN_TYPE.CHANGE_EMAIL) {
        if (!verifyToken.metadata.verified) {
          await this.userRepo.saveVerifyToken({
            id: verifyToken.id,
            metadata: {
              ...verifyToken.metadata,
              verified: true,
            },
          })
        }
        const changeEmailTokens = await this.userRepo.findToken({
          user_id: findUser.id,
          type: VERIFY_TOKEN_TYPE.CHANGE_EMAIL,
        })
        const checkConfirm = changeEmailTokens.every(
          (vt) => vt.metadata.verified
        )
        if (checkConfirm) {
          await this.userRepo.saveUser({
            id: findUser.id,
            email: verifyToken.metadata.email,
          })
          return true
        }
        throwError({
          status: HttpStatus.OK,
          ...ERROR_CODE.EMAIL_VERIFIED_NOT_ENOUGH,
        })
      }
    }
    throwError({
      status: HttpStatus.OK,
      ...ERROR_CODE.SESSION_INVALID,
    })
  }
  async createUser(params: UserCreate): Promise<any> {
    const email = params.email.toLocaleLowerCase()
    const findUser = await this.userRepo.findOneUser({ email })
    if (findUser) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.EMAIL_EXISTED,
      })
    }
    const password = generatePassword()
    const userCreated = await this.userRepo.saveUser({
      created_at: Date.now(),
      ...params,
      password,
      email,
    })
    const verifyToken = await this.verifyTokenService.createTokenUUID({
      user_id: userCreated.id,
      expires_at:
        Date.now() + Number(APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * SECOND,
      type: VERIFY_TOKEN_TYPE.VERIFY_EMAIL,
    })

    if (verifyToken) {
      await this.userRepo.saveUserRole([
        {
          user_id: userCreated.id,
          role_id: DEFAULT_ROLE.id,
          description: 'Role default',
          owner_created: 'system',
        },
      ])
      const merchant = await this.merchantRepository.findOne({
        code: params.merchant_code,
      })
      let from_email
      let from_name
      if (
        merchant &&
        merchant.config &&
        merchant.config['verified_sender'] &&
        merchant.config['email_sender']
      ) {
        from_email = merchant.config['email_sender']['from_email']
        from_name = merchant.config['email_sender']['from_name']
      }

      const resultSendMail = await this.mailResource.sendEmailCreateAccount(
        params.email,
        verifyToken.token,
        password,
        from_email,
        from_name,
        `${userCreated.first_name || ''} ${userCreated.last_name || ''}`
      )
      if (resultSendMail) {
        await this.userRepo.saveUser({
          id: userCreated.id,
          note_updated: 'Send mail verify success',
        })
      } else {
        await this.userRepo.saveUser({
          id: userCreated.id,
          note_updated: 'Send mail verify failed',
        })
      }
      delete userCreated.password
      return userCreated
    }
    throwError({
      status: HttpStatus.UNPROCESSABLE_ENTITY,
      ...ERROR_CODE.SEND_EMAIL_FAILED,
    })
  }
  async registerUser(params: UserRegister): Promise<any> {
    const email = params.email.toLocaleLowerCase()
    let merchantCode = APP_SETTING.MERCHANT_CODE_DEFAULT
    const queryUser = {
      email,
    }
    if (params.merchant_code) {
      const merchant = await this.merchantRepository.findOne({
        code: params.merchant_code,
        status: MERCHANT_STATUS.ACTIVE,
      })
      if (merchant) {
        if (merchant.config.domain_type === MERCHANT_TYPE.OTHERS) {
          queryUser['merchant_code'] = [params.merchant_code]
        }
      } else {
        params.merchant_code = merchantCode
      }
    } else {
      params.merchant_code = merchantCode
    }
    if (!queryUser['merchant_code']) {
      const merchants = await this.merchantRepository.findMerchant({
        domain_type: MERCHANT_TYPE.COINMAP,
      })
      queryUser['merchant_code'] = merchants.map((e) => e.code)
    }

    if (queryUser['merchant_code'] && queryUser['merchant_code'].length > 0) {
      const findUser = await this.userRepo.findUser(queryUser)
      if (findUser.length > 0) {
        throwError({
          status: HttpStatus.BAD_REQUEST,
          ...ERROR_CODE.EMAIL_EXISTED,
        })
      }
    }

    const userCreated = await this.userRepo.saveUser({
      ...params,
      created_at: Date.now(),
      email,
    })
    const verifyToken = await this.verifyTokenService.createTokenUUID({
      user_id: userCreated.id,
      expires_at:
        Date.now() + Number(APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * SECOND,
      type: VERIFY_TOKEN_TYPE.VERIFY_EMAIL,
    })

    if (verifyToken) {
      await this.userRepo.saveUserRole([
        {
          user_id: userCreated.id,
          role_id: DEFAULT_ROLE.id,
          description: 'Role default',
          owner_created: 'system',
        },
      ])
      const merchant = await this.merchantRepository.findOne({
        code: params.merchant_code,
      })
      let from_email
      let from_name
      let logo_url
      let header_url
      let main_content
      let twitter_url
      let youtube_url
      let facebook_url
      let telegram_url
      let company_name
      if (merchant && merchant.config) {
        if (
          merchant.config['verified_sender'] &&
          merchant.config['email_sender']
        ) {
          from_email = merchant.config['email_sender']['from_email']
          from_name = merchant.config['email_sender']['from_name']
        }
        if (merchant.config['social_media']) {
          twitter_url = merchant.config['social_media']['twitter_url']
          youtube_url = merchant.config['social_media']['youtube_url']
          facebook_url = merchant.config['social_media']['facebook_url']
          telegram_url = merchant.config['social_media']['telegram_url']
        }
        logo_url = merchant.config['email_logo_url']
        header_url = merchant.config['email_banner_url']
        main_content = `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our Trading applications will help you trade safety and in comfort.`
        if (merchant.config.domain_type === MERCHANT_TYPE.OTHERS) {
          company_name = merchant.name
          from_email = from_email || APP_CONFIG.SENDGRID_SENDER_ALGO_EMAIL
          from_name = from_name || APP_CONFIG.SENDGRID_SENDER_ALGO_NAME
          logo_url = logo_url || APP_CONFIG.ALGO_LOGO_EMAIL
          header_url = header_url || APP_CONFIG.ALGO_BANNER_EMAIL
        }
      }
      const resultSendMail = await this.mailResource.sendEmailConfirmAccount(
        params.email,
        verifyToken.token,
        from_email,
        from_name,
        logo_url,
        header_url,
        main_content,
        twitter_url,
        youtube_url,
        facebook_url,
        telegram_url,
        company_name,
        `${userCreated.first_name || ''} ${userCreated.last_name || ''}`
      )
      if (resultSendMail) {
        await this.userRepo.saveUser({
          id: userCreated.id,
          note_updated: 'Send mail verify success',
        })
      } else {
        await this.userRepo.saveUser({
          id: userCreated.id,
          note_updated: 'Send mail verify failed',
        })
      }
      return resultSendMail
    }
    throwError({
      status: HttpStatus.UNPROCESSABLE_ENTITY,
      ...ERROR_CODE.SEND_EMAIL_FAILED,
    })
  }
  async resendEmailVerify(params: EmailSend): Promise<any> {
    const email = params.email.toLocaleLowerCase()
    const findUser = await this.userRepo.findOneUser({
      email,
      merchant_code: params.merchant_code,
    })
    if (!findUser) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.EMAIL_NOT_FOUND,
      })
    }
    if (findUser.email_confirmed) {
      throwError({
        status: HttpStatus.FORBIDDEN,
        ...ERROR_CODE.EMAIL_EXISTED,
      })
    }
    // await this.userRepo.deleteToken({
    //   user_id: findUser.id,
    //   type: VERIFY_TOKEN_TYPE.VERIFY_EMAIL,
    // })
    const verifyToken = await this.verifyTokenService.createTokenUUID({
      user_id: findUser.id,
      expires_at:
        Date.now() + Number(APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * SECOND,
      type: VERIFY_TOKEN_TYPE.VERIFY_EMAIL,
    })
    if (verifyToken) {
      const merchant = await this.merchantRepository.findOne({
        code: findUser.merchant_code,
      })
      let from_email
      let from_name
      let logo_url
      let header_url
      let main_content
      let twitter_url
      let youtube_url
      let facebook_url
      let telegram_url
      let company_name
      if (merchant && merchant.config) {
        if (
          merchant.config['verified_sender'] &&
          merchant.config['email_sender']
        ) {
          from_email = merchant.config['email_sender']['from_email']
          from_name = merchant.config['email_sender']['from_name']
        }
        if (merchant.config['social_media']) {
          twitter_url = merchant.config['social_media']['twitter_url']
          youtube_url = merchant.config['social_media']['youtube_url']
          facebook_url = merchant.config['social_media']['facebook_url']
          telegram_url = merchant.config['social_media']['telegram_url']
        }
        logo_url = merchant.config['email_logo_url']
        header_url = merchant.config['email_banner_url']
        main_content = `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our Trading applications will help you trade safety and in comfort.`
        if (merchant.config.domain_type === MERCHANT_TYPE.OTHERS) {
          company_name = merchant.name
          from_email = from_email || APP_CONFIG.SENDGRID_SENDER_ALGO_EMAIL
          from_name = from_name || APP_CONFIG.SENDGRID_SENDER_ALGO_NAME
          logo_url = logo_url || APP_CONFIG.ALGO_LOGO_EMAIL
          header_url = header_url || APP_CONFIG.ALGO_BANNER_EMAIL
        }
      }

      const resultSendMail = await this.mailResource.sendEmailConfirmAccount(
        params.email,
        verifyToken.token,
        from_email,
        from_name,
        logo_url,
        header_url,
        main_content,
        twitter_url,
        youtube_url,
        facebook_url,
        telegram_url,
        company_name,
        `${findUser.first_name || ''} ${findUser.last_name || ''}`
      )
      if (resultSendMail) {
        await this.userRepo.saveUser({
          id: findUser.id,
          note_updated: 'Send mail verify success',
        })
        return resultSendMail
      } else {
        await this.userRepo.saveUser({
          id: findUser.id,
          note_updated: 'Send mail verify failed',
        })
      }
    }
    throwError({
      status: HttpStatus.UNPROCESSABLE_ENTITY,
      ...ERROR_CODE.SEND_EMAIL_FAILED,
    })
  }
  async checkUsername(username: string, user_id?: string): Promise<boolean> {
    const user = await this.userRepo.findOneUser({ username })
    if (!user || user.id === user_id) {
      return true
    }
    return false
  }
  async checkEmail(email: string): Promise<boolean> {
    const user = await this.userRepo.findOneUser({ email })
    if (user) {
      return false
    }
    return true
  }
  async changeEmail(params: ChangeEmail): Promise<boolean> {
    const { email, user_id, callback_url } = params
    const [user, checkEmail] = await Promise.all([
      this.userRepo.findOneUser({ id: user_id }),
      this.userRepo.findOneUser({ email }),
    ])
    if (!user) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.USER_NOT_FOUND,
      })
    }
    if (checkEmail) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.EMAIL_EXISTED,
      })
    }
    await this.userRepo.deleteToken({
      user_id: user.id,
      type: VERIFY_TOKEN_TYPE.CHANGE_EMAIL,
    })
    const verifyToken1 = await this.verifyTokenService.createTokenUUID(
      {
        user_id: user.id,
        expires_at:
          Date.now() + Number(APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * SECOND,
        type: VERIFY_TOKEN_TYPE.CHANGE_EMAIL,
      },
      { email, verified: false }
    )
    const verifyToken2 = await this.verifyTokenService.createTokenUUID(
      {
        user_id: user.id,
        expires_at:
          Date.now() + Number(APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * SECOND,
        type: VERIFY_TOKEN_TYPE.CHANGE_EMAIL,
      },
      { email, verified: false }
    )
    const merchant = await this.merchantRepository.findOne({
      code: user.merchant_code,
    })
    let from_email
    let from_name
    let logo_url
    let header_url
    let main_content
    let twitter_url
    let youtube_url
    let facebook_url
    let telegram_url
    let company_name
    if (merchant && merchant.config) {
      if (
        merchant.config['verified_sender'] &&
        merchant.config['email_sender']
      ) {
        from_email = merchant.config['email_sender']['from_email']
        from_name = merchant.config['email_sender']['from_name']
      }
      if (merchant.config['social_media']) {
        twitter_url = merchant.config['social_media']['twitter_url']
        youtube_url = merchant.config['social_media']['youtube_url']
        facebook_url = merchant.config['social_media']['facebook_url']
        telegram_url = merchant.config['social_media']['telegram_url']
      }
      logo_url = merchant.config['email_logo_url']
      header_url = merchant.config['email_banner_url']
      main_content = `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our Trading applications will help you trade safety and in comfort.`
      if (merchant.config.domain_type === MERCHANT_TYPE.OTHERS) {
        company_name = merchant.name
        from_email = from_email || APP_CONFIG.SENDGRID_SENDER_ALGO_EMAIL
        from_name = from_name || APP_CONFIG.SENDGRID_SENDER_ALGO_NAME
        logo_url = logo_url || APP_CONFIG.ALGO_LOGO_EMAIL
        header_url = header_url || APP_CONFIG.ALGO_BANNER_EMAIL
      }
    }
    const [resultSendMail1, resultSendMail2] = await Promise.all([
      this.mailResource.sendEmailChange(
        user.email,
        verifyToken1.token,
        user.email,
        email,
        callback_url,
        from_email,
        from_name,
        logo_url,
        header_url,
        twitter_url,
        youtube_url,
        facebook_url,
        telegram_url,
        company_name,
        `${user.first_name || ''} ${user.last_name || ''}`
      ),
      this.mailResource.sendEmailChange(
        email,
        verifyToken2.token,
        user.email,
        email,
        callback_url,
        from_email,
        from_name,
        logo_url,
        header_url,
        twitter_url,
        youtube_url,
        facebook_url,
        telegram_url,
        company_name,
        `${user.first_name || ''} ${user.last_name || ''}`
      ),
    ])
    if (resultSendMail1 && resultSendMail2) {
      await this.userRepo.saveUser({
        id: user.id,
        note_updated: 'Send mail change email success',
      })
      return true
    } else {
      await this.userRepo.saveUser({
        id: user.id,
        note_updated: 'Send mail change email failed',
      })
    }
    throwError({
      status: HttpStatus.UNPROCESSABLE_ENTITY,
      ...ERROR_CODE.SEND_EMAIL_FAILED,
    })
  }
  async changeEmailVerifyCode(params: ChangeEmailVerify): Promise<boolean> {
    const { user_id, code } = params
    const verifyToken = await this.userRepo.findOneToken({
      user_id,
      type: VERIFY_TOKEN_TYPE.CHANGE_EMAIL,
    })
    if (verifyToken.type === VERIFY_TOKEN_TYPE.CHANGE_EMAIL) {
      if (code === verifyToken.metadata.code) {
        await this.userRepo.saveUser({
          id: user_id,
          email: verifyToken.metadata.email,
        })
        return true
      } else {
        return false
      }
    }
    throwError({
      status: HttpStatus.UNAUTHORIZED,
      ...ERROR_CODE.SESSION_INVALID,
    })
  }
  async checkPassword(user_id: string, password: string): Promise<boolean> {
    const user = await this.userRepo.findOneUser({ id: user_id })
    if (!user) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.USER_NOT_FOUND,
      })
    }
    const passIsCorrect = await hashUtil.comparePassword(
      password,
      user.password
    )
    return passIsCorrect
  }
}
