import { Inject } from '@nestjs/common'
import { ORDER_CATEGORY } from 'src/const/transaction'
import {
  CurrencyDomain,
  CurrencyOrderUpdate,
  QueryCurrency,
  RawCurrency,
} from 'src/domains/setting/currency.types'
import { CurrencyRepository } from 'src/repositories/currency.repostitory'
import { CURRENCIES_TABLE } from 'src/repositories/currency.repostitory/currency.entity'
import { SettingRepository } from 'src/repositories/setting.repository'
import { throwError } from 'src/utils/handle-error.util'

import { BaseService } from '../base'
export class CurrencyService
  extends BaseService<QueryCurrency, RawCurrency>
  implements CurrencyDomain
{
  constructor(
    @Inject(CurrencyRepository)
    private currencyRepository: CurrencyRepository,
    @Inject(SettingRepository)
    private settingRepository: SettingRepository
  ) {
    super(currencyRepository)
  }

  async updateOrderCurrency(params: CurrencyOrderUpdate): Promise<any[]> {
    const { currencies, owner_created } = params
    const dataUpdate = currencies.map((e) => {
      return {
        ...e,
        owner_created,
      }
    })
    return this.currencyRepository.saves(dataUpdate)
  }
  async findCurrencies(params: {
    category: ORDER_CATEGORY
  }): Promise<RawCurrency[]> {
    const { category } = params
    const keyName = `CURRENCY_${category}`
    const setting = await this.settingRepository.findOneAppSetting({
      name: keyName,
    })
    if (!setting?.value?.trim()) {
      return []
    }
    const ids = setting.value.split(',').filter(e => e.trim())
    return this.currencyRepository.find({ status: true, ids })
  }
}
