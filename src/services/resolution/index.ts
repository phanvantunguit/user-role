import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import {
  QueryResolution,
  RawResolution,
  Resolution,
  ResolutionDomain,
  ResolutionUpdate,
} from 'src/domains/setting'
import { SettingRepository } from 'src/repositories/setting.repository'
import { throwError } from 'src/utils/handle-error.util'

export class ResolutionService implements ResolutionDomain {
  constructor(
    @Inject(SettingRepository) private settingRepo: SettingRepository
  ) {}
  async updateResolution(params: ResolutionUpdate): Promise<RawResolution> {
    await this.getResolution({ id: params.id })
    const update = await this.settingRepo.saveResolution([params])
    return update[0]
  }
  async getResolution(params: QueryResolution): Promise<RawResolution> {
    const resolutions = await this.settingRepo.findResolution(params)
    if (resolutions.length === 0) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.RESOLUTION_NOT_FOUND,
      })
    }
    return resolutions[0]
  }
  async listResolution(): Promise<RawResolution[]> {
    const resolutions = await this.settingRepo.findResolution({})
    return resolutions
  }
  async createResolution(params: Resolution[]): Promise<RawResolution[]> {
    const create = await this.settingRepo.saveResolution(params)
    return create
  }
  async deleteResolution(ids: string[]): Promise<any> {
    const deleted = await this.settingRepo.deleteResolutionIds(ids)
    return deleted
  }
}
