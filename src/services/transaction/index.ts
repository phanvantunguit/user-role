import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE, PACKAGE_TYPE, ROLE_STATUS } from 'src/const'
import {
  PAYMENT_METHOD,
  TRANSACTION_EVENT,
  TRANSACTION_STATUS,
} from 'src/const/transaction'
import {
  CreateTransaction,
  QueryTransaction,
  QueryTransactionPagination,
  RawTransaction,
  RawTransactionView,
  ResCheckTransaction,
  ResCreateTransaction,
  TransactionDomain,
} from 'src/domains/transaction/transaction.types'
import { PackageRepository } from 'src/repositories/package.ropository'
import { PermissionRepository } from 'src/repositories/permission.repository'
import { TransactionDetailRepository } from 'src/repositories/transaction-detail.repository'
import { TransactionRepository } from 'src/repositories/transaction.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { throwError } from 'src/utils/handle-error.util'
import { TransactionLogService } from '../transaction-log'
import { PaymentService } from '../payment'
import { RawUserRole } from 'src/domains/user'
import { EventEmitter2 } from '@nestjs/event-emitter'
import { DBContext } from 'src/repositories/db-context'
export class TransactionService implements TransactionDomain {
  constructor(
    @Inject(PaymentService)
    private paymentService: PaymentService,
    @Inject(TransactionRepository)
    private transactionRepository: TransactionRepository,
    @Inject(PermissionRepository)
    private permissionRepository: PermissionRepository,
    @Inject(TransactionLogService)
    private transactionLogService: TransactionLogService,
    @Inject(TransactionDetailRepository)
    private transactionDetailRepository: TransactionDetailRepository,
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(PackageRepository) private packageRepository: PackageRepository,
    @Inject(EventEmitter2) private eventEmitter: EventEmitter2,
    @Inject(DBContext) private dBContext: DBContext
  ) {}
  async getTransactionPagination(
    params: QueryTransactionPagination
  ): Promise<any> {
    return await this.transactionRepository.viewFindTransactionPaging(params)
  }
  async ipnTransaction(method: PAYMENT_METHOD, pramas: any) {
    const { transaction, transaction_event, metadata } =
      await this.paymentService.getService(method).ipnTransaction(pramas)
    if (!transaction) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BAD_REQUEST,
      })
    }
    let transaction_status = TRANSACTION_STATUS.PROCESSING
    switch (transaction_event) {
      case TRANSACTION_EVENT.PAYMENT_FAILED:
        transaction_status = TRANSACTION_STATUS.FAILED
        break
      case TRANSACTION_EVENT.PAYMENT_CREATED:
        transaction_status = TRANSACTION_STATUS.CREATED
        break
      default:
        transaction_status = TRANSACTION_STATUS.PROCESSING
    }
    const transLog = await this.transactionLogService.get({
      transaction_id: transaction.id,
      transaction_event,
    })
    if (!transLog) {
      await this.transactionLogService.save({
        transaction_id: transaction.id,
        transaction_event,
        transaction_status,
        metadata: metadata,
      })
    } else {
      this.eventEmitter.emit(transLog.transaction_event, transLog)
    }
  }
  async listTransaction(
    parmas: QueryTransaction
  ): Promise<RawTransactionView[]> {
    const transaction = await this.transactionRepository.viewFind(parmas)
    return transaction
  }
  async getTransaction(parmas: QueryTransaction): Promise<RawTransactionView> {
    const transaction = await this.transactionRepository.viewfindOne(parmas)
    if (!transaction) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.TRANSACTION_NOT_FOUND,
      })
    }
    const createdInfo = await this.transactionLogService.get({
      transaction_id: parmas.id,
      transaction_event: TRANSACTION_EVENT.PAYMENT_CREATED,
    })
    let paymentInfo = {}
    if (
      transaction.payment_method === PAYMENT_METHOD.COIN_PAYMENT &&
      createdInfo.metadata.error === 'ok'
    ) {
      paymentInfo = createdInfo.metadata.result
    }
    // const details = await this.transactionDetailRepository.find({
    //   transaction_id: parmas.id,
    // })
    return { ...paymentInfo, ...transaction }
  }
  async createTransaction(
    parmas: CreateTransaction
  ): Promise<ResCreateTransaction> {
    const {
      user_id,
      payment_method,
      description,
      role_id,
      buy_currency,
      buyer_email,
      package_id,
    } = parmas
    const roles = await this.permissionRepository.findRoleByIds([role_id])
    const packageTime = await this.packageRepository.findById(package_id)
    if (roles.length === 0) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.ROLE_NOT_FOUND,
      })
    }
    if (roles[0].status !== ROLE_STATUS.OPEN) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.ROLE_INVALID,
      })
    }
    if (!packageTime) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.PACKAGE_NOT_FOUND,
      })
    }
    const sellCurrency = buy_currency //roles[0].currency
    const price = Number(roles[0].price) || 0
    const quantity = packageTime.quantity || 0
    const discountRate = packageTime.discount_rate || 0
    const discountAmount = packageTime.discount_amount || 0
    const sellAmount =
      price * quantity - price * quantity * discountRate - discountAmount
    try {
      return await this.dBContext.runInTransaction(async (queryRunner) => {
        const newTrans = await this.transactionRepository.save(
          {
            user_id,
            payment_method,
            description,
            status: TRANSACTION_STATUS.CREATED,
            sell_amount: sellAmount,
            sell_currency: sellCurrency,
            buy_currency,
          },
          queryRunner
        )
        const expiredTime = new Date()
        const transDetail = await this.transactionDetailRepository.save(
          {
            transaction_id: newTrans.id,
            user_id: newTrans.user_id,
            role_id: roles[0].id,
            price: Number(roles[0].price),
            currency: sellCurrency,
            package_id,
            package_type: packageTime.type,
            package_name: packageTime.name,
            discount_rate: Number(packageTime.discount_rate),
            discount_amount: Number(packageTime.discount_amount),
            quantity: Number(packageTime.quantity),
            expires_at: expiredTime.setMonth(expiredTime.getMonth() + quantity),
          },
          queryRunner
        )
        const paymentResult = await this.paymentService
          .getService(payment_method)
          .createTransaction(newTrans, buyer_email)
        await this.transactionRepository.save(
          {
            ...newTrans,
            payment_id: paymentResult.txn_id,
            buy_amount: paymentResult.buy_amount,
            wallet_address: paymentResult.wallet_address,
          },
          queryRunner
        )
        await this.transactionLogService.save({
          transaction_id: newTrans.id,
          transaction_event: TRANSACTION_EVENT.PAYMENT_CREATED,
          transaction_status: TRANSACTION_STATUS.CREATED,
          metadata: paymentResult.metadata,
        })
        delete paymentResult.metadata
        return {
          ...paymentResult,
          transaction_id: newTrans.id,
          buy_currency,
          sell_amount: sellAmount,
          sell_currency: sellCurrency,
          details: [transDetail],
          created_at: newTrans.created_at,
        }
        // try {
        //   const expiredTime = new Date()
        //   const transDetail = await this.transactionDetailRepository.save(
        //     {
        //       transaction_id: newTrans.id,
        //       user_id: newTrans.user_id,
        //       role_id: roles[0].id,
        //       price: Number(roles[0].price),
        //       currency: sellCurrency,
        //       package_id,
        //       package_type: packageTime.type,
        //       package_name: packageTime.name,
        //       discount_rate: Number(packageTime.discount_rate),
        //       discount_amount: Number(packageTime.discount_amount),
        //       quantity: Number(packageTime.quantity),
        //       expires_at: expiredTime.setMonth(expiredTime.getMonth() + quantity),
        //     },
        //   )
        //   const paymentResult = await this.paymentService
        //     .getService(payment_method)
        //     .createTransaction(newTrans, buyer_email)
        //   return {
        //     ...paymentResult,
        //     transaction_id: newTrans.id,
        //     buy_currency,
        //     sell_amount: sellAmount,
        //     sell_currency: sellCurrency,
        //     details: [transDetail],
        //     created_at: newTrans.created_at,
        //   }
        // } catch (error) {
        //   await this.transactionLogService.save({
        //     transaction_id: newTrans.id,
        //     transaction_event: TRANSACTION_EVENT.PAYMENT_CREATED,
        //     transaction_status: TRANSACTION_STATUS.FAILED,
        //     metadata: error,
        //   })
        //   throwError({
        //     status: HttpStatus.UNPROCESSABLE_ENTITY,
        //     ...ERROR_CODE.CREATE_TRANSACTION_FAILED,
        //   })
        // }
      })
    } catch (error) {
      let message = ERROR_CODE.CREATE_TRANSACTION_FAILED.message
      if (error && error.error && error.error.includes('Invalid currency')) {
        message = 'Currency is not support'
      }
      throwError({
        status: HttpStatus.UNPROCESSABLE_ENTITY,
        error_code: ERROR_CODE.CREATE_TRANSACTION_FAILED.error_code,
        message,
      })
    }
  }
  async handlePaymentComplete(transaction: RawTransaction) {
    const [transDetail] = await Promise.all([
      this.transactionDetailRepository.find({ transaction_id: transaction.id }),
    ])

    // const updateRole = transDetail.map((detail) => {
    //   return {
    //     role_id: detail.role_id,
    //     description: 'User upgrade',
    //     user_id: transaction.user_id,
    //     owner_created: transaction.user_id,
    //     quantity: detail.quantity,
    //     package_name: detail.package_name,
    //     expires_at: detail.expires_at,
    //   }
    // })
    const detail = transDetail[0]
    const userRole = await this.userRepository.findUserRole({
      role_id: detail.role_id,
      user_id: detail.user_id,
    })
    const dataUpdateRole = {
      role_id: detail.role_id,
      description: 'User upgrade',
      user_id: detail.user_id,
      owner_created: detail.user_id,
      quantity: detail.quantity,
      package_name: detail.package_name,
      expires_at: detail.expires_at,
      package_id: detail.package_id,
      package_type: detail.package_type,
    }
    if (userRole.length > 0) {
      if (userRole[0].expires_at > Date.now()) {
        dataUpdateRole.expires_at =
          Number(userRole[0].expires_at) +
          Number(dataUpdateRole.expires_at) -
          Date.now()
      }
      dataUpdateRole['id'] = userRole[0].id
    }
    await this.userRepository.saveUserRole([dataUpdateRole])
    const dataUpdate = {
      ...transaction,
      status: TRANSACTION_STATUS.COMPLETE,
    }
    await this.transactionRepository.save(dataUpdate)
    await this.transactionLogService.saveNotSendEvent({
      transaction_id: transaction.id,
      transaction_event: TRANSACTION_EVENT.DELIVERED,
      transaction_status: TRANSACTION_STATUS.COMPLETE,
      metadata: dataUpdateRole,
    })
  }
  async handlePaymentFailed(transaction: RawTransaction) {
    const dataUpdate = {
      ...transaction,
      status: TRANSACTION_STATUS.FAILED,
    }
    this.transactionRepository.save(dataUpdate)
  }
  async checkCreateTransactionValid(
    user_id: string,
    role_id: string,
    package_id: string,
    buy_currency: string
  ): Promise<any> {
    const trans = await this.transactionRepository.viewFindByUserMultipleStatus(
      user_id,
      [TRANSACTION_STATUS.PROCESSING, TRANSACTION_STATUS.CREATED]
    )
    const transaction = trans.find(
      (t) =>
        t.details.length > 0 &&
        t.details[0].role_id === role_id &&
        t.details[0].package_id === package_id &&
        t.buy_currency === buy_currency
    )
    if (transaction && transaction.payment_id) {
      const createdInfo = await this.transactionLogService.get({
        transaction_id: transaction.id,
        transaction_event: TRANSACTION_EVENT.PAYMENT_CREATED,
      })
      let paymentInfo = {}
      if (
        transaction.payment_method === PAYMENT_METHOD.COIN_PAYMENT &&
        createdInfo.metadata.error === 'ok'
      ) {
        paymentInfo = createdInfo.metadata.result
      }
      const transactionId = transaction.id
      delete transaction.id
      return {
        ...transaction,
        ...paymentInfo,
        transaction_id: transactionId,
      }
    }
    return null

    // const invalid = roles.some((r) => {
    //   if (!r.expires_at || Number(r.expires_at) <= Date.now()) {
    //     return true
    //   }
    //   return false
    // })
    // if (invalid) {
    //   throwError({
    //     status: HttpStatus.BAD_REQUEST,
    //     ...ERROR_CODE.PACKAGE_UPGRADED,
    //   })
    // }
  }
  async upgradeFreeTrial(
    user_id: string,
    role_id: string
  ): Promise<RawUserRole> {
    const roles = await this.permissionRepository.findRoleByIds([role_id])
    if (roles.length === 0) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.ROLE_NOT_FOUND,
      })
    }
    const price = Number(roles[0].price) || 0
    if (price !== 0) {
      throwError({
        status: HttpStatus.BAD_GATEWAY,
        ...ERROR_CODE.ROLE_INVALID,
      })
    }
    const userRole = await this.userRepository.findUserRole({
      role_id: role_id,
      user_id: user_id,
    })
    if (userRole.length > 0) {
      throwError({
        status: HttpStatus.BAD_GATEWAY,
        ...ERROR_CODE.PACKAGE_UPGRADED,
      })
    }
    const quantity = 14
    const expiredTime = new Date()
    const dataUpdateRole = {
      role_id,
      description: 'User upgrade',
      user_id,
      owner_created: user_id,
      quantity,
      package_name: '14 Days',
      expires_at: expiredTime.setDate(expiredTime.getDate() + quantity),
      // package_id: 'package_id',
      package_type: PACKAGE_TYPE.DAY,
    }
    const data = await this.userRepository.saveUserRole([dataUpdateRole])
    return data[0]
  }
  async checkPartnerTransaction(
    transaction: RawTransaction
  ): Promise<ResCheckTransaction> {
    const { transaction_event, metadata } = await this.paymentService
      .getService(transaction.payment_method)
      .checkTransaction(transaction.payment_id)
    let transaction_status = TRANSACTION_STATUS.PROCESSING
    switch (transaction_event) {
      case TRANSACTION_EVENT.PAYMENT_FAILED:
        transaction_status = TRANSACTION_STATUS.FAILED
        break
      case TRANSACTION_EVENT.PAYMENT_CREATED:
        transaction_status = TRANSACTION_STATUS.CREATED
        break
      default:
        transaction_status = TRANSACTION_STATUS.PROCESSING
    }
    const transLog = await this.transactionLogService.get({
      transaction_id: transaction.id,
      transaction_event,
    })
    if (!transLog) {
      await this.transactionLogService.save({
        transaction_id: transaction.id,
        transaction_event,
        transaction_status,
        metadata: metadata,
      })
    } else {
      this.eventEmitter.emit(transLog.transaction_event, transLog)
    }
    return { transaction_status, transaction_event, metadata }
  }
}
