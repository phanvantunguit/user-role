import { PermissionRepository } from 'src/repositories/permission.repository'
import { HttpStatus, Inject } from '@nestjs/common'
import {
  SymbolSettingRoleDomain,
  SymbolSettingRoleCreate,
  SymbolSettingRoleUpdate,
  SymbolSettingRoleDelete,
} from 'src/domains/permission'
import { ERROR_CODE } from 'src/const'
import { throwError } from 'src/utils/handle-error.util'
import { SettingRepository } from 'src/repositories/setting.repository'
import { RoleService } from '../role'
export class SymbolSettingRoleService implements SymbolSettingRoleDomain {
  constructor(
    private roleService: RoleService,
    @Inject(SettingRepository) private settingRepo: SettingRepository,
    @Inject(PermissionRepository) private permissionRepo: PermissionRepository
  ) {}
  async deleteSymbolSettingRole(params: SymbolSettingRoleDelete): Promise<any> {
    const { id, role_id } = params
    const role = await this.roleService.checkRoleNotFoundAndThrow({
      id: role_id,
    })
    await this.permissionRepo.deleteSymbolSettingRoleByIds([id])
    const ssrIndex = role[0].root.symbol_settings_roles.findIndex(
      (e) => e.id === id
    )
    if (ssrIndex > -1) {
      role[0].root.symbol_settings_roles.splice(ssrIndex, 1)
      const updateRole = await this.permissionRepo.saveRoles(role)
      return updateRole
    }
    return role
  }
  async modifySymbolSettingRole(params: SymbolSettingRoleCreate): Promise<any> {
    const { role_id } = params
    await this.roleService.checkRoleNotFoundAndThrow({
      id: role_id,
    })
    const currentSSR = await this.permissionRepo.findSymbolSettingRole({
      role_id,
    })
    await Promise.all(
      currentSSR.map((e) => this.deleteSymbolSettingRole({ id: e.id, role_id }))
    )
    // const [exchanges, symbols, resolutions] = await Promise.all([
    //     this.getExchangeSettingOrThrow(list_exchanged),
    //     this.getSymbolSettingOrThrow(list_symbol),
    //     this.getResolutionOrThrow(supported_resolutions)])

    await this.permissionRepo.saveSymbolSettingRole([params])
    // const dataSymbolSettingRole = {
    //     id: createSymbolSettingRole[0].id,
    //     description,
    //     exchanges,
    //     symbols,
    //     resolutions
    // }
    // if(!role[0].root.symbol_settings_roles) {
    //     role[0].root.symbol_settings_roles = []
    // }
    // role[0].root.symbol_settings_roles = [dataSymbolSettingRole]
    // const updateRole = await this.permissionRepo.saveRoles(role)
    const roles = await this.permissionRepo.findRoles({ id: role_id })
    return roles[0]
  }
  async createSymbolSettingRole(params: SymbolSettingRoleCreate): Promise<any> {
    const {
      role_id,
      description,
      list_exchanged,
      list_symbol,
      supported_resolutions,
    } = params
    const role = await this.roleService.checkRoleNotFoundAndThrow({
      id: role_id,
    })
    const [exchanges, symbols, resolutions] = await Promise.all([
      this.getExchangeSettingOrThrow(list_exchanged),
      this.getSymbolSettingOrThrow(list_symbol),
      this.getResolutionOrThrow(supported_resolutions),
    ])
    const createSymbolSettingRole =
      await this.permissionRepo.saveSymbolSettingRole([params])
    const dataSymbolSettingRole = {
      id: createSymbolSettingRole[0].id,
      description,
      exchanges,
      symbols,
      resolutions,
    }
    if (!role[0].root.symbol_settings_roles) {
      role[0].root.symbol_settings_roles = []
    }
    role[0].root.symbol_settings_roles.push(dataSymbolSettingRole)
    const updateRole = await this.permissionRepo.saveRoles(role)
    return updateRole
  }
  async updateSymbolSettingRole(params: SymbolSettingRoleUpdate): Promise<any> {
    const {
      id,
      role_id,
      description,
      list_exchanged,
      list_symbol,
      supported_resolutions,
    } = params
    const role = await this.roleService.checkRoleNotFoundAndThrow({
      id: role_id,
    })
    await this.checkSymbolSettingRolesNotFoundAndThrow([id])
    const [exchanges, symbols, resolutions] = await Promise.all([
      this.getExchangeSettingOrThrow(list_exchanged),
      this.getSymbolSettingOrThrow(list_symbol),
      this.getResolutionOrThrow(supported_resolutions),
    ])
    await this.permissionRepo.saveSymbolSettingRole([params])
    const dataSymbolSettingRole = {
      id,
      description,
      exchanges,
      symbols,
      resolutions,
    }
    const ssrIndex = role[0].root.symbol_settings_roles.findIndex(
      (e) => e.id === id
    )
    if (ssrIndex > -1) {
      role[0].root.symbol_settings_roles[ssrIndex] = dataSymbolSettingRole
      const updateRole = await this.permissionRepo.saveRoles(role)
      return updateRole
    }
    return role
  }
  async checkSymbolSettingRolesNotFoundAndThrow(ids: string[]) {
    const roles = await this.permissionRepo.findSymbolSettingRoleByIds(ids)
    if (roles.length === 0) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.SYMBOL_SETTING_ROLE_NOT_FOUND,
      })
    }
    return roles
  }
  private async getResolutionOrThrow(ids: string[]) {
    const resolutions = await this.settingRepo.findResolutionByIds(ids)
    if (resolutions.length !== ids.length) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.RESOLUTION_NOT_FOUND,
      })
    }
    return resolutions
  }
  private async getSymbolSettingOrThrow(symbols: string[]) {
    const listSymbols = await this.settingRepo.findSymbolBySymbols(symbols)
    if (listSymbols.length !== symbols.length) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.SYMBOL_NOT_FOUND,
      })
    }
    return listSymbols
  }
  private async getExchangeSettingOrThrow(names: string[]) {
    const exchanges = await this.settingRepo.findExchangeByNames(names)
    if (exchanges.length !== names.length) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.EXCHANGE_NOT_FOUND,
      })
    }
    return exchanges
  }
}
