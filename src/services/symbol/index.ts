import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE, SYMBOL_STATUS } from 'src/const'
import {
  CreateSymbol,
  QuerySymbol,
  RawSymbol,
  SymbolDomain,
  UpdateSymbol,
} from 'src/domains/setting'
// import { ElasticSearchRepository } from 'src/repositories/elasticsearch.repository'
import { SettingRepository } from 'src/repositories/setting.repository'
import { throwError } from 'src/utils/handle-error.util'

export class SymbolService implements SymbolDomain {
  constructor(
    @Inject(SettingRepository) private settingRepo: SettingRepository // @Inject(ElasticSearchRepository)
  ) // private elasticSearchRepository: ElasticSearchRepository
  {}
  async listSymbol(): Promise<RawSymbol[]> {
    const symbols = await this.settingRepo.findSymbol({})
    return symbols
  }
  async getSymbol(params: QuerySymbol): Promise<RawSymbol> {
    const symbols = await this.settingRepo.findSymbol(params)
    if (symbols.length === 0) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.SYMBOL_NOT_FOUND,
      })
    }
    return symbols[0]
  }
  // private async saveElasticSearch(params: UpdateSymbol) {
  //   if (params.status === SYMBOL_STATUS.ON) {
  //     const resolutions = await this.settingRepo.findResolution({})
  //     const supportedResolutions = resolutions.map((sr) => sr.resolutions_name)
  //     const tradingviewIndex = 'info_symbol_tradingview'
  //     const tradingviewId = `${params.exchange_name.toLowerCase()}_${params.types.toLowerCase()}_${params.symbol.toLowerCase()}` // "binance_crypto_ftmusdt"
  //     const tradingviewData = {
  //       name: params.symbol,
  //       'exchange-traded': params.exchange_name,
  //       'exchange-listed': params.exchange_name,
  //       timezone: 'UTC',
  //       minmov: params.minmov || 0,
  //       minmov2: params.minmov2 || 0,
  //       pointvalue: params.pointvalue || 0,
  //       session: params.session || '24x7',
  //       has_intraday: params.has_intraday || false,
  //       has_no_volume: params.has_no_volume || false,
  //       description: params.description,
  //       type: params.types,
  //       pricescale: params.pricescale || 10000,
  //       supported_resolutions: supportedResolutions,
  //       ticker: params.symbol,
  //       ticks: params.ticks,
  //     }

  //     const listsymbolIndex = 'listsymbol'
  //     const listsymbolId = `${params.exchange_name.toLowerCase()}_${params.types.toLowerCase()}_${params.symbol.toLowerCase()}` // "binance_crypto_ftmusdt"
  //     const listsymbolData = {
  //       symbol: params.symbol,
  //       full_name: params.symbol,
  //       description: params.description,
  //       exchange: params.exchange_name,
  //       ticker: params.symbol,
  //       type: params.types,
  //       supported_resolutions: supportedResolutions,
  //       ticks: params.ticks,
  //     }

  //     const tradingviewExistId = await this.elasticSearchRepository.isExist(
  //       tradingviewIndex,
  //       tradingviewId
  //     )
  //     if (tradingviewExistId) {
  //       await this.elasticSearchRepository.updateDocument(
  //         tradingviewIndex,
  //         tradingviewId,
  //         tradingviewData
  //       )
  //     } else {
  //       await this.elasticSearchRepository.createDocument(
  //         tradingviewIndex,
  //         tradingviewId,
  //         tradingviewData
  //       )
  //     }
  //     const listsymbolExistId = await this.elasticSearchRepository.isExist(
  //       listsymbolIndex,
  //       tradingviewId
  //     )
  //     if (listsymbolExistId) {
  //       await this.elasticSearchRepository.updateDocument(
  //         listsymbolIndex,
  //         listsymbolId,
  //         listsymbolData
  //       )
  //     } else {
  //       await this.elasticSearchRepository.createDocument(
  //         listsymbolIndex,
  //         listsymbolId,
  //         listsymbolData
  //       )
  //     }
  //   }
  // }
  async createSymbol(params: CreateSymbol[]): Promise<any> {
    const create = await this.settingRepo.saveSymbol(params)
    // await Promise.all(params.map((p) => this.saveElasticSearch(p)))
    return create
  }
  async updateSymbol(params: UpdateSymbol): Promise<any> {
    const symbol = await this.getSymbol({ symbol: params.symbol })
    const updated = await this.settingRepo.saveSymbol([params])
    // await this.saveElasticSearch({ ...symbol, ...params })
    return updated
  }
  async deleteSymbol(symbol: string[]): Promise<any> {
    const deleted = await this.settingRepo.deleteSymbols(symbol)
    return deleted
  }
}
