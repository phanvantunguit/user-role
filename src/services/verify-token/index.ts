import { v4 } from 'uuid'

import {
  CreateTokenUUID,
  RawVerifyToken,
  VerifyTokenDomain,
} from 'src/domains/user'
import { UserRepository } from 'src/repositories/user.repository'
import { Inject } from '@nestjs/common'
export class VerifyTokenService implements VerifyTokenDomain {
  constructor(@Inject(UserRepository) private userRepo: UserRepository) {}

  createTokenUUID(
    params: CreateTokenUUID,
    metadata?: any
  ): Promise<RawVerifyToken> {
    const { user_id, expires_at, type } = params
    const verifyToken = {
      user_id,
      expires_at,
      type,
      token: v4(),
      metadata: metadata,
    }
    return this.userRepo.saveVerifyToken(verifyToken)
  }
}
