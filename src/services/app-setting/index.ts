import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import {
  APP_SETTING,
} from 'src/const/app-setting'
import { ITEM_STATUS, TBOT_TYPE } from 'src/const/transaction'
import {
  AppSetting,
  AppSettingDomain,
  AppSettingUpdate,
  QueryAppSetting,
  RawAppSetting,
} from 'src/domains/setting/app-setting.types'
import { SettingRepository } from 'src/repositories/setting.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { throwError } from 'src/utils/handle-error.util'

export class AppSettingService implements AppSettingDomain {
  constructor(
    @Inject(SettingRepository) private settingRepo: SettingRepository,
    @Inject(UserRepository) private userRepository: UserRepository,
  ) {}
  async listAppSetting(params: { user_id?: string }): Promise<RawAppSetting[]> {
    const data = await this.settingRepo.findAppSetting(params)
    if (params.user_id) {
      const numberOfBotConnected =
        await this.userRepository.findUserBotTradingSQL({
          user_id: params.user_id,
          type: TBOT_TYPE.SELECT
        })
      data.push({
        id: null,
        name: APP_SETTING.NUMBER_OF_TBOT_USED,
        value: numberOfBotConnected.length.toString(),
        description: null,
        owner_created: null,
        created_at: null,
        updated_at: null,
        user_id: params.user_id,
      })
    }
    return data
  }
  async getAppSetting(params: QueryAppSetting): Promise<RawAppSetting> {
    if (!Object.values(APP_SETTING).includes(params.name)) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.APP_SETTING_NOT_FOUND,
      })
    }
    switch (params.name) {
      case APP_SETTING.NUMBER_OF_TBOT_USED:
        const numberOfBotConnected =
          await this.userRepository.findUserBotTradingSQL({
            user_id: params.user_id,
            type: TBOT_TYPE.SELECT
          })
        return {
          id: null,
          name: params.name,
          value: numberOfBotConnected.length.toString(),
          description: null,
          owner_created: null,
          created_at: null,
          updated_at: null,
          user_id: params.user_id,
        }
      default:
        const data = await this.settingRepo.findAppSetting(params)
        if (data.length === 0) {
          return {
            id: null,
            name: params.name,
            value: null,
            description: null,
            owner_created: null,
            created_at: null,
            updated_at: null,
            user_id: null,
          }
        }
        return data[0]
    }
  }
  async createAppSetting(params: AppSetting): Promise<any> {
    await this.checkAppSettingExistedAndThrow({
      id: undefined,
      name: params.name,
    })
    return this.settingRepo.saveAppSetting(params)
  }
  async userCreateAppSetting(params: {
    name: string
    value: string
    description?: string
    owner_created: string
  }): Promise<any> {
    const setting = await this.settingRepo.findOneAppSetting({
      name: params.name,
      user_id: params.owner_created,
    })
    if (setting) {
      return this.settingRepo.saveAppSetting({
        ...params,
        id: setting.id,
        user_id: params.owner_created,
      })
    } else {
      return this.settingRepo.saveAppSetting({
        ...params,
        user_id: params.owner_created,
      })
    }
  }

  async updateAppSetting(params: AppSettingUpdate): Promise<any> {
    await this.checkAppSettingExistedAndThrow({
      id: params.id,
      name: params.name,
    })
    return this.settingRepo.saveAppSetting(params)
  }
  async deleteAppSetting(id: string): Promise<any> {
    await this.checkAppSettingNotFoundAndThrow({ id })
    return this.settingRepo.deleteAppSettingById(id)
  }

  private async checkAppSettingNotFoundAndThrow(params: QueryAppSetting) {
    const data = await this.settingRepo.findAppSetting(params)
    if (data.length === 0) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.APP_SETTING_NOT_FOUND,
      })
    }
  }
  private async checkAppSettingExistedAndThrow(params: QueryAppSetting) {
    const data = await this.settingRepo.findAppSetting({ name: params.name })
    if (data.length > 0 && data[0].id !== params.id) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.APP_SETTING_EXISTED,
      })
    }
  }
}
