import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import {
  FeatureCreate,
  FeatureDomain,
  FeatureUpdate,
  QueryFeature,
  RawFeature,
} from 'src/domains/setting'
import { SettingRepository } from 'src/repositories/setting.repository'
import { throwError } from 'src/utils/handle-error.util'

export class FeatureService implements FeatureDomain {
  constructor(
    @Inject(SettingRepository) private settingRepo: SettingRepository
  ) {}
  async getFeatureDetail(params: QueryFeature): Promise<RawFeature> {
    const features = await this.settingRepo.findFeature(params)
    return features[0]
  }
  async getFeature(): Promise<RawFeature[]> {
    const features = await this.settingRepo.findFeature({})
    return features
  }
  async createFeature(params: FeatureCreate): Promise<any> {
    const { owner_created, features } = params
    const dataCreate = features.map((f) => {
      return {
        ...f,
        owner_created,
      }
    })
    const create = await this.settingRepo.saveFeatures(dataCreate)
    return create
  }
  async updateFeature(params: FeatureUpdate): Promise<any> {
    const { feature_id, feature_name, description, action, owner_created } =
      params
    await this.checkFeatureNotFoundAndThrow({ feature_id })
    await this.checkExistedAndThrow({ feature_id, feature_name })
    const dataUpdate = {
      feature_id,
      feature_name,
      description,
      action,
      owner_created,
    }
    const update = await this.settingRepo.saveFeatures([dataUpdate])
    return update
  }

  async deleteFeature(feature_ids: string[]): Promise<any> {
    const deleted = await this.settingRepo.deleteFeatureIds(feature_ids)
    return deleted
  }

  private async checkFeatureNotFoundAndThrow(params: QueryFeature) {
    const features = await this.settingRepo.findFeature(params)
    if (features.length === 0) {
      throwError({
        status: HttpStatus.NOT_FOUND,
        ...ERROR_CODE.FEATURE_NOT_FOUND,
      })
    }
  }
  private async checkExistedAndThrow(params: QueryFeature) {
    const features = await this.settingRepo.findFeature({
      feature_name: params.feature_name,
    })
    if (features.length > 0 && features[0].feature_id !== params.feature_id) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.FEATURE_EXISTED,
      })
    }
  }
}
