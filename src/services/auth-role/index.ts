import { PermissionRepository } from 'src/repositories/permission.repository'
import { HttpStatus, Inject } from '@nestjs/common'
import {
  AuthRoleCreate,
  AuthRoleDomain,
  Permission,
  AuthRoleUpdate,
  RawAuthRole,
  QueryAuthRole,
  ListAuthRole,
} from 'src/domains/permission'
import { ADMIN_PERMISSION, ADMIN_PERMISSION_DATA, ERROR_CODE } from 'src/const'
import { throwError } from 'src/utils/handle-error.util'
import { UserRepository } from 'src/repositories/user.repository'
export class AuthRoleService implements AuthRoleDomain {
  constructor(
    @Inject(PermissionRepository) private permissionRepo: PermissionRepository,
    @Inject(UserRepository) private userRepo: UserRepository
  ) {}
  async getAuthUserRole(params: QueryAuthRole): Promise<RawAuthRole> {
    const authUserRoles = await this.permissionRepo.findAuthRole(params)
    return authUserRoles[0]
  }
  async getListAuthUserRole(params: ListAuthRole): Promise<RawAuthRole[]> {
    const { user_id } = params
    if (user_id) {
      const userRoles = await this.userRepo.findAuthUserRole({ user_id })
      const roleIds = userRoles.map((e) => e.auth_role_id)
      return await this.permissionRepo.findAuthRoleByIds(roleIds)
    }
    return await this.permissionRepo.findAuthRole({})
  }

  getListPermission(): Permission[] {
    return ADMIN_PERMISSION_DATA
  }
  private async checkRoleNameExistAndThrow(params: QueryAuthRole) {
    const authRoles = await this.permissionRepo.findAuthRole({
      role_name: params.role_name,
    })
    if (
      (authRoles.length > 0 && !params.id) ||
      (authRoles.length > 0 && params.id && params.id !== authRoles[0].id)
    ) {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.ROLE_EXISTED,
      })
    }
  }
  async createAuthRole(params: AuthRoleCreate): Promise<RawAuthRole> {
    const { permission_ids, role_name, owner_created, description } = params
    await this.checkRoleNameExistAndThrow({ role_name })
    const permissions: Permission[] = permission_ids.map((id) => {
      return ADMIN_PERMISSION[id]
    })
    const dataCreate: RawAuthRole = {
      role_name,
      owner_created,
      root: {
        permissions,
      },
      description,
    }
    const create = await this.permissionRepo.saveAuthRoles([dataCreate])
    return create[0]
  }
  async updateAuthRole(params: AuthRoleUpdate): Promise<RawAuthRole> {
    const { id, permission_ids, role_name, owner_created, description } = params
    await this.checkRoleNameExistAndThrow({ id, role_name })
    let root
    if (permission_ids) {
      const permissions: Permission[] = permission_ids.map((id) => {
        return ADMIN_PERMISSION[id]
      })
      root = {
        permissions,
      }
    }
    const dataUpdate: RawAuthRole = {
      id,
      role_name,
      owner_created,
      description,
    }
    if (root) {
      dataUpdate.root = root
    }
    const update = await this.permissionRepo.saveAuthRoles([dataUpdate])
    return update[0]
  }
  deleteAuthRole(ids: string[]): Promise<any> {
    return this.permissionRepo.deleteAuthRoleByIds(ids)
  }
}
