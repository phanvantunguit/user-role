import { HttpStatus, Inject } from '@nestjs/common'
import { ERROR_CODE, PACKAGE_TYPE } from 'src/const'
import { ITEM_STATUS, ORDER_CATEGORY } from 'src/const/transaction'
import {
  AddUserRoles,
  RemoveUserRoles,
  UserRoleDomain,
  UserRoleModify,
} from 'src/domains/user'
import { PermissionRepository } from 'src/repositories/permission.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { throwError } from 'src/utils/handle-error.util'

export class UserRoleService implements UserRoleDomain {
  constructor(
    private userRepo: UserRepository,
    @Inject(PermissionRepository) private permissionRepo: PermissionRepository
  ) {}

  async modifyUserRole(params: UserRoleModify): Promise<any> {
    const { user_id, owner_created, roles } = params
    const roleIds = roles.map((r) => r.role_id)
    const dataRoles = await this.permissionRepo.findRoleByIds(roleIds)
    const updateUserAssetLogs = []
    if (dataRoles.length !== roleIds.length) {
      const roleIdsNotFound = []
      for (const dr of dataRoles) {
        if (!roleIds.includes(dr.id)) {
          roleIdsNotFound.push(dr.id)
        }
      }
      throwError({
        status: HttpStatus.NOT_FOUND,
        error_code: ERROR_CODE.ROLE_NOT_FOUND.error_code,
        message: roleIdsNotFound,
      })
    }
    const currentUserRoles = await this.userRepo.findUserRole({ user_id })
    const dataModify = []
    for (const rReq of roles) {
      const urExistIndex = currentUserRoles.findIndex(
        (ur) => ur.role_id === rReq.role_id
      )
      const dataRole = dataRoles.find((r) => r.id === rReq.role_id)
      updateUserAssetLogs.push({
        asset_id: rReq.role_id,
        user_id: user_id,
        category: ORDER_CATEGORY.PKG,
        status: ITEM_STATUS.ACTIVE,
        owner_created,
        expires_at: rReq.expires_at,
        name: dataRole.role_name,
        quantity: rReq.quantity,
        package_type: rReq.package_type,
      })
      if (urExistIndex > -1) {
        if (currentUserRoles[urExistIndex].description !== rReq.description) {
          dataModify.push({
            ...currentUserRoles[urExistIndex],
            description: rReq.description,
            owner_created,
          })
        }
        currentUserRoles.splice(urExistIndex, 1)
      } else {
        dataModify.push({
          ...rReq,
          user_id,
          owner_created,
        })
      }
    }
    const deleteUserRoleIds = currentUserRoles.map((ur) => {
      return ur.id
    })
    if (deleteUserRoleIds.length > 0) {
      const deleteUserRoles = await this.permissionRepo.findRoleByIds(
        deleteUserRoleIds
      )
      deleteUserRoles.forEach((dr) => {
        updateUserAssetLogs.push({
          asset_id: dr.id,
          user_id: user_id,
          category: ORDER_CATEGORY.PKG,
          status: ITEM_STATUS.DELETED,
          owner_created,
          name: dr.role_name,
        })
      })

      await this.userRepo.deleteUserRoleByIds(deleteUserRoleIds)
    }
    const modify = await this.userRepo.saveUserRole(dataModify)
    // save log
    await this.userRepo.saveUserAssetLog(updateUserAssetLogs)
    return modify
  }

  async addUserRoles(params: AddUserRoles): Promise<any> {
    const { user_ids, owner_created, role_ids, quantity, package_type } = params
    let expiresAt = null
    if (quantity) {
      const expiredTime = new Date()
      if (package_type === PACKAGE_TYPE.DAY) {
        expiredTime.setDate(expiredTime.getDate() + quantity)
      } else {
        expiredTime.setMonth(expiredTime.getMonth() + quantity)
      }
      expiresAt = expiredTime.valueOf()
    }
    const userRoles = await this.userRepo.findUserRoleByUserIdsAndRoleIds(
      user_ids,
      role_ids
    )
    const dataDoles = await this.permissionRepo.findRoleByIds(role_ids)
    const dataAdd = []
    const updateUserAssetLogs = []
    for (const user_id of user_ids) {
      for (let role of dataDoles) {
        if (role) {
          const role_id = role.id
          const userRoleExisted = userRoles.find(
            (u) => u.user_id === user_id && u.role_id === role_id
          )
          updateUserAssetLogs.push({
            asset_id: role_id,
            user_id: user_id,
            category: ORDER_CATEGORY.PKG,
            status: ITEM_STATUS.ACTIVE,
            owner_created,
            expires_at: expiresAt,
            name: role.role_name,
            quantity,
            package_type,
          })
          if (userRoleExisted) {
            if (
              expiresAt &&
              userRoleExisted.expires_at &&
              userRoleExisted.expires_at > Date.now()
            ) {
              expiresAt =
                Number(userRoleExisted.expires_at) + expiresAt - Date.now()
            }
            dataAdd.push({
              id: userRoleExisted.id,
              user_id,
              role_id,
              expires_at: expiresAt,
              owner_created,
            })
          } else {
            dataAdd.push({
              user_id,
              role_id,
              expires_at: expiresAt,
              owner_created,
            })
          }
        }
      }
    }
    if (dataAdd.length > 0) {
      await this.userRepo.saveUserRole(dataAdd)
      // save log
      await this.userRepo.saveUserAssetLog(updateUserAssetLogs)
    }
    return true
  }
  async removeUserRoles(
    owner_created: string,
    params: RemoveUserRoles
  ): Promise<any> {
    const { user_ids, role_ids } = params
    await this.userRepo.deleteUserRoles(user_ids, role_ids)
    const dataDoles = await this.permissionRepo.findRoleByIds(role_ids)
    // save log
    const updateUserAssetLogs = []
    for (let userId of user_ids) {
      for (let role of dataDoles) {
        if (role) {
          const roleId = role.id
          updateUserAssetLogs.push({
            asset_id: roleId,
            user_id: userId,
            category: ORDER_CATEGORY.PKG,
            status: ITEM_STATUS.DELETED,
            owner_created,
            name: role.role_name,
          })
        }
      }
    }
    await this.userRepo.saveUserAssetLog(updateUserAssetLogs)
    return true
  }
}
