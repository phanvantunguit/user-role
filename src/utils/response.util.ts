import { HttpStatus } from '@nestjs/common'
import { ERROR_CODE, IFailed, ISuccess } from '../const'

export function resSuccess(params: ISuccess): any {
  const { payload, res, message, status } = params
  const result = {
    ...ERROR_CODE.SUCCESS,
    payload,
  }
  if (message) {
    result.message = message
  }
  res.status(status || HttpStatus.OK).send(result)
}
export function resFailed(params: IFailed): any {
  const { payload, res, reason } = params
  const result = {
    error_code: reason.error_code,
    message: reason.message,
    payload,
  }
  const status = reason.status || HttpStatus.UNPROCESSABLE_ENTITY
  res.status(status).send(result)
}
