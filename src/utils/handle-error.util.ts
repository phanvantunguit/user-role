import { IReason } from 'src/const/response'

export function throwError(reason: IReason) {
  throw reason
}
