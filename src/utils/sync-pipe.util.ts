import { EventEmitter } from 'events'
import { v4 } from 'uuid'
const eventEmitter = new EventEmitter()

export class SyncPipe {
  private pipe = []
  async promise(callback, ...input) {
    const id = v4()
    return new Promise(async (resolve) => {
      if (this.pipe.length === 0) {
        this.pipe.push(id)
        const result = await callback(...input)
        this.pipe.splice(0, 1)
        eventEmitter.emit(id)
        return resolve(result)
      } else {
        const previousId = this.pipe[this.pipe.length - 1]
        this.pipe.push(id)
        eventEmitter.on(previousId, async () => {
          const result = await callback(...input)
          this.pipe.splice(0, 1)
          eventEmitter.emit(id, result)
          return resolve(result)
        })
      }
    })
  }
}
export const SyncPipeRoleModify = new SyncPipe()
