import {
  Controller,
  Get,
  Inject,
  Param,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger'
import { Response } from 'express'
import { ADMIN_PERMISSION } from 'src/const'
import { AdminGetTransactionV2Handler } from 'src/usecases/transaction/admin-get-detail-transaction-v2'
import { AdminGetTransactionV2PayloadOutput } from 'src/usecases/transaction/admin-get-detail-transaction-v2/validate'
import { AdminListTransactionV2Handler } from 'src/usecases/transaction/admin-list-transaction-v2'
import {
  AdminListTransactionV2Input,
  AdminListTransactionV2PayloadOutput,
} from 'src/usecases/transaction/admin-list-transaction-v2/validate'
import { resSuccess } from 'src/utils/response.util'
import { AdminAuthGuard, AdminBaseApiOutput, AdminPermission } from '../const'
@ApiBearerAuth()
@ApiTags('admin/transaction/v2')
@Controller('transaction')
@UseGuards(AdminAuthGuard)
export class AdminTransactionV2Controller {
  constructor(
    @Inject(AdminListTransactionV2Handler)
    private adminListTransactionV2Handler: AdminListTransactionV2Handler,
    @Inject(AdminGetTransactionV2Handler)
    private adminGetTransactionV2Handler: AdminGetTransactionV2Handler
  ) {}

  @AdminPermission(ADMIN_PERMISSION.GET_TRANSACTION.permission_id)
  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class AdminListTransactionV2PayloadOutputMap extends IntersectionType(
      AdminListTransactionV2PayloadOutput,
      AdminBaseApiOutput
    ) {},
    description: 'List transaction',
  })
  async listTransaction(
    @Query() query: AdminListTransactionV2Input,
    @Res() res: Response
  ) {
    const data = await this.adminListTransactionV2Handler.execute(query)
    resSuccess({
      payload: data,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.GET_TRANSACTION.permission_id)
  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class AdminGetTransactionV2PayloadOutputMap extends IntersectionType(
      AdminGetTransactionV2PayloadOutput,
      AdminBaseApiOutput
    ) {},
    description: 'Get detail transaction',
  })
  async getTransaction(@Param('id') id: string, @Res() res: Response) {
    const data = await this.adminGetTransactionV2Handler.execute({ id })
    resSuccess({
      payload: data,
      res,
    })
  }
}
