import { Controller, Get, Inject, Res, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger'
import { Response } from 'express'
import { ADMIN_PERMISSION } from 'src/const'
import { AdminListMerchantHandler } from 'src/usecases/merchant/admin-list-merchant'
import { AdminListMerchantOutput } from 'src/usecases/merchant/admin-list-merchant/validate'
import { resSuccess } from 'src/utils/response.util'
import { AdminAuthGuard, AdminBaseApiOutput, AdminPermission } from '../const'
@ApiBearerAuth()
@ApiTags('admin/merchant/v2')
@Controller('merchant')
@UseGuards(AdminAuthGuard)
export class AdminMerchantV2Controller {
  constructor(
    @Inject(AdminListMerchantHandler)
    private adminListMerchantHandler: AdminListMerchantHandler
  ) {}

  @AdminPermission(ADMIN_PERMISSION.GET_TRANSACTION.permission_id)
  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class AdminListMerchantOutputMap extends IntersectionType(
      AdminListMerchantOutput,
      AdminBaseApiOutput
    ) {},
    description: 'List merchant',
  })
  async listTransaction(@Res() res: Response) {
    const data = await this.adminListMerchantHandler.execute()
    resSuccess({
      payload: data,
      res,
    })
  }
}
