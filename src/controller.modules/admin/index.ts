import { Module } from '@nestjs/common'
import { RouterModule, Routes } from '@nestjs/core'
import { DatabaseModule } from 'src/repositories'
import { CoinpaymentResource } from 'src/resources/coinpayment'
import { CoinpaymentTransport } from 'src/resources/coinpayment/coinpayment.transport'
import { MetaapiResource } from 'src/resources/forex'
import { MailResource } from 'src/resources/mail'
import { SendGridTransport } from 'src/resources/mail/sendgrid.transport'
import { StorageResource } from 'src/resources/storage'
import { GCloudTransport } from 'src/resources/storage/google.transport'
import { AppSettingService } from 'src/services/app-setting'
import { AuthRoleService } from 'src/services/auth-role'
import { AuthUserRoleService } from 'src/services/auth-user-role'
import { BotService } from 'src/services/bot'
import { BotSettingService } from 'src/services/bot-setting'
import { BotSignalService } from 'src/services/bot-signal'
import { BotTradingService } from 'src/services/bot-trading'
import { BotTradingHistoryService } from 'src/services/bot-trading-history'
import { CurrencyService } from 'src/services/currency'
import { ExchangeService } from 'src/services/exchange'
import { FeatureService } from 'src/services/feature'
import { FeatureRoleService } from 'src/services/feature-role'
import { GeneralSettingService } from 'src/services/general-setting'
import { GeneralSettingRoleService } from 'src/services/general-setting-role'
import { PackageService } from 'src/services/package'
import { PaymentService } from 'src/services/payment'
import { CoinpaymentService } from 'src/services/payment/coinpayment'
import { ResolutionService } from 'src/services/resolution'
import { RoleService } from 'src/services/role'
import { SessionService } from 'src/services/session'
import { SymbolService } from 'src/services/symbol'
import { SymbolSettingRoleService } from 'src/services/symbol-setting-role'
import { TransactionService } from 'src/services/transaction'
import { TransactionLogService } from 'src/services/transaction-log'
import { UserService } from 'src/services/user'
import { UserRoleService } from 'src/services/user-role'
import { VerifyTokenService } from 'src/services/verify-token'
import { AdminDeleteBotTradingHandler } from 'src/usecases/bot/admin-delete-bot-trading'
import { AdminImportTradeHistoryCSVHandler } from 'src/usecases/bot/admin-import-trade-history-csv'
import { AdminListTradeHistoryHandler } from 'src/usecases/bot/admin-list-trade-history'
import { AdminUpdateBotTradingStatusHandler } from 'src/usecases/bot/admin-update-bot-trading-status'
import { AdminResetSubscriptionHandler } from 'src/usecases/bot/reset-subscriptions'
import { UserListBotHandler } from 'src/usecases/bot/user-list-bot'
import { UserListBotTradingHandler } from 'src/usecases/bot/user-list-bot-trading'
import { UserListBotTradingHistoryHandler } from 'src/usecases/bot/user-list-bot-trading-history'
import { AdminListMerchantHandler } from 'src/usecases/merchant/admin-list-merchant'
import { UserListRoleHandler } from 'src/usecases/role/user-list-role'
import { AdminGetTransactionV2Handler } from 'src/usecases/transaction/admin-get-detail-transaction-v2'
import { AdminListTransactionV2Handler } from 'src/usecases/transaction/admin-list-transaction-v2'
import { ListAssetLogHandler } from 'src/usecases/transaction/list-asset-log'
import { AdminAddUserAssetHandler } from 'src/usecases/user-asset/admin-add-user-asset'
import { AdminRemoveUserAssetHandler } from 'src/usecases/user-asset/admin-remove-user-asset'

import { AdminUserController } from './api.v1.controller/admin-user.controller'
import { AuthRoleController } from './api.v1.controller/auth-role.controller'
import { AdminBotTradingHistoryController } from './api.v1.controller/bot-trading-history.controler'
import { AdminBotTradingController } from './api.v1.controller/bot-trading.controler'
import { AdminBotController } from './api.v1.controller/bot.controller'
import { AdminPermissionController } from './api.v1.controller/permission.controller'
import { RoleController } from './api.v1.controller/role.controller'
import { AdminSettingController } from './api.v1.controller/setting.controller'
import { AdminTransactionController } from './api.v1.controller/transaction.controller'
import { AdminMerchantV2Controller } from './api.v2.controller/merchant.controller'
import { AdminTransactionV2Controller } from './api.v2.controller/transaction.controller'

@Module({
  imports: [DatabaseModule],
  controllers: [
    AdminUserController,
    AdminPermissionController,
    AuthRoleController,
    AdminSettingController,
    RoleController,
    AdminTransactionController,
    AdminBotController,
    AdminBotTradingController,
    AdminBotTradingHistoryController,
  ],
  providers: [
    AuthRoleService,
    UserService,
    MailResource,
    VerifyTokenService,
    SendGridTransport,
    AuthUserRoleService,
    FeatureService,
    ExchangeService,
    ResolutionService,
    SymbolService,
    GeneralSettingService,
    UserRoleService,
    RoleService,
    FeatureRoleService,
    GeneralSettingRoleService,
    SymbolSettingRoleService,
    AppSettingService,
    TransactionService,
    CoinpaymentResource,
    TransactionLogService,
    CoinpaymentTransport,
    BotSettingService,
    PackageService,
    PaymentService,
    CoinpaymentService,
    CurrencyService,
    GCloudTransport,
    StorageResource,
    SessionService,
    BotService,
    BotTradingService,
    BotSignalService,
    ListAssetLogHandler,
    AdminAddUserAssetHandler,
    AdminRemoveUserAssetHandler,
    UserListRoleHandler,
    UserListBotHandler,
    UserListBotTradingHandler,
    MetaapiResource,
    BotTradingHistoryService,
    UserListBotTradingHistoryHandler,
    AdminImportTradeHistoryCSVHandler,
    AdminListTradeHistoryHandler,
    AdminUpdateBotTradingStatusHandler,
    AdminResetSubscriptionHandler,
    AdminDeleteBotTradingHandler
  ],
})
class APIV1Controller {}

@Module({
  imports: [DatabaseModule],
  controllers: [AdminTransactionV2Controller, AdminMerchantV2Controller],
  providers: [
    SessionService,
    GeneralSettingRoleService,
    RoleService,
    AdminListTransactionV2Handler,
    AdminGetTransactionV2Handler,
    AdminListMerchantHandler,
  ],
})
class APIV2Controller {}

const routes: Routes = [
  { path: 'api/v1/admin', module: APIV1Controller },
  { path: 'api/v2/admin', module: APIV2Controller },
]

@Module({
  imports: [RouterModule.register(routes), APIV1Controller, APIV2Controller],
})
export class ApiV1AdminControllerModule {}
