import { ApiProperty } from '@nestjs/swagger'
import { IsOptional, IsNumberString, IsString } from 'class-validator'
import { TRANSACTION_STATUS } from 'src/const/transaction'

export class GetTransactionDto {
  @ApiProperty({
    required: false,
    description: 'Page',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  page: number

  @ApiProperty({
    required: false,
    description: 'Size',
    example: 50,
  })
  @IsOptional()
  @IsNumberString()
  size: number

  @ApiProperty({
    required: false,
    description: 'The keyword to find user ex: phone number, name, email',
    example: 'john',
  })
  @IsOptional()
  @IsString()
  keyword: string

  @ApiProperty({
    required: false,
    description: 'Status of transaction',
    example: 'COMPLETE',
  })
  @IsOptional()
  @IsString()
  status: TRANSACTION_STATUS

  @ApiProperty({
    required: false,
    description: 'From',
  })
  @IsOptional()
  @IsNumberString()
  from: number

  @ApiProperty({
    required: false,
    description: 'To',
  })
  @IsOptional()
  @IsNumberString()
  to: number
}
