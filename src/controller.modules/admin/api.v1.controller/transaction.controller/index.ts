import {
  Controller,
  Get,
  Inject,
  Param,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { ADMIN_PERMISSION } from 'src/const'
import { TransactionRepository } from 'src/repositories/transaction.repository'
import { TransactionService } from 'src/services/transaction'
import { TransactionLogService } from 'src/services/transaction-log'
import { resSuccess } from 'src/utils/response.util'
import { AdminAuthGuard, AdminPermission } from '../../const'
import { GetTransactionDto } from './dto'
@ApiBearerAuth()
@ApiTags('admin/transaction')
@Controller('transaction')
@UseGuards(AdminAuthGuard)
export class AdminTransactionController {
  constructor(
    @Inject(TransactionService) private transactionService: TransactionService,
    @Inject(TransactionLogService)
    private transactionLogService: TransactionLogService,
    @Inject(TransactionRepository)
    private transactionRepository: TransactionRepository
  ) {}

  @AdminPermission(ADMIN_PERMISSION.GET_TRANSACTION.permission_id)
  @Get('/list')
  async listTransaction(
    @Query() query: GetTransactionDto,
    @Res() res: Response
  ) {
    const data = await this.transactionService.getTransactionPagination(query)
    resSuccess({
      payload: data,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.GET_TRANSACTION.permission_id)
  @Get('/:id/check')
  async checkOrigin(@Param('id') id: string, @Res() res: Response) {
    const transaction = await this.transactionRepository.findById(id)
    const data = await this.transactionService.checkPartnerTransaction(
      transaction
    )
    resSuccess({
      payload: data,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.GET_TRANSACTION.permission_id)
  @Get('/:id/logs')
  async transactionLogs(@Param('id') id: string, @Res() res: Response) {
    const data = await this.transactionLogService.list({ transaction_id: id })
    resSuccess({
      payload: data,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.GET_TRANSACTION.permission_id)
  @Get('/:id')
  async getTransaction(@Param('id') id: string, @Res() res: Response) {
    const data = await this.transactionService.getTransaction({ id })
    resSuccess({
      payload: data,
      res,
    })
  }
}
