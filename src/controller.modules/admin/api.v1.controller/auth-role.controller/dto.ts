import { IsString, IsArray, IsOptional } from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'
export class PostAuthRoleDto {
  @ApiProperty({
    required: true,
    description: 'List permission_id',
    example: ['CREATE_AUTH_ROLE', 'UPDATE_AUTH_ROLE'],
  })
  @IsArray()
  @IsString({ each: true })
  permission_ids: string[]

  @ApiProperty({
    required: true,
    description: 'Name of role',
    example: 'Customer Support',
  })
  @IsString()
  role_name: string

  @ApiProperty({
    required: true,
    description: 'Describe detail about role',
    example: `Allow CS view feature and package of customer`,
  })
  @IsOptional()
  @IsString()
  description: string
}
export class PutAuthRoleDto {
  @ApiProperty({
    description: 'List permission_id',
    example: ['CREATE_AUTH_ROLE', 'UPDATE_AUTH_ROLE'],
  })
  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  permission_ids: string[]

  @ApiProperty({
    description: 'Name of role',
    example: 'Customer Support',
  })
  @IsOptional()
  @IsString()
  role_name: string

  @ApiProperty({
    description: 'Describe detail about role',
    example: `Allow CS view feature and package of customer`,
  })
  @IsOptional()
  @IsString()
  description: string
}
export class QueryAuthRoleDto {
  @ApiProperty({
    required: false,
    description: 'id of user',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsOptional()
  @IsString()
  user_id: string
}
