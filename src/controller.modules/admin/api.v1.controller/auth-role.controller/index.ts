import {
  Param,
  Body,
  Controller,
  Get,
  Post,
  Put,
  UseGuards,
  Res,
  Req,
  Delete,
  Query,
} from '@nestjs/common'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { ADMIN_PERMISSION } from 'src/const'
import { AuthRoleService } from 'src/services/auth-role'

import { resSuccess } from 'src/utils/response.util'
import { AdminAuthGuard, AdminPermission } from '../../const'
import { PostAuthRoleDto, PutAuthRoleDto, QueryAuthRoleDto } from './dto'

@ApiBearerAuth()
@ApiTags('admin/auth-role')
@Controller('auth-role')
@UseGuards(AdminAuthGuard)
export class AuthRoleController {
  constructor(private authRoleService: AuthRoleService) {}

  @Get('list')
  @AdminPermission(ADMIN_PERMISSION.GET_AUTH_ROLE.permission_id)
  async listAuthRole(@Query() query: QueryAuthRoleDto, @Res() res: Response) {
    const result = await this.authRoleService.getListAuthUserRole(query)

    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('/:id')
  @AdminPermission(ADMIN_PERMISSION.GET_AUTH_ROLE.permission_id)
  async getAuthRole(@Param('id') id: string, @Res() res: Response) {
    const result = await this.authRoleService.getAuthUserRole({ id })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Post()
  @AdminPermission(ADMIN_PERMISSION.CREATE_AUTH_ROLE.permission_id)
  async createAuthRole(
    @Body() body: PostAuthRoleDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.authRoleService.createAuthRole({
      ...body,
      owner_created,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('/:id')
  @AdminPermission(ADMIN_PERMISSION.UPDATE_AUTH_ROLE.permission_id)
  async updateAuthRole(
    @Param('id') id: string,
    @Body() body: PutAuthRoleDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.authRoleService.updateAuthRole({
      ...body,
      owner_created,
      id,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Delete('/:id')
  @AdminPermission(ADMIN_PERMISSION.DELETE_AUTH_ROLE.permission_id)
  async deletAuthRole(@Param('id') id: string, @Res() res: Response) {
    const result = await this.authRoleService.deleteAuthRole([id])
    resSuccess({
      payload: result,
      res,
    })
  }
}
