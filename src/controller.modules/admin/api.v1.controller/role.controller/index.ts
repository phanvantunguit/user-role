import {
  Param,
  Body,
  Controller,
  Get,
  Post,
  Put,
  UseGuards,
  Res,
  Req,
  Delete,
  Query,
} from '@nestjs/common'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { ADMIN_PERMISSION } from 'src/const'
import { FeatureRoleService } from 'src/services/feature-role'
import { GeneralSettingRoleService } from 'src/services/general-setting-role'
import { RoleService } from 'src/services/role'
import { SymbolSettingRoleService } from 'src/services/symbol-setting-role'

import { resSuccess } from 'src/utils/response.util'
import { AdminAuthGuard, AdminPermission } from '../../const'
import {
  PostRoleDto,
  PutRoleDto,
  PutFeatureRoleDto,
  PutGeneralSettingRoleDto,
  PutSymbolSettingRoleDto,
  QueryRoleDto,
  PutRoleOrderDto,
} from './dto'
@ApiBearerAuth()
@ApiTags('admin/role')
@Controller('role')
@UseGuards(AdminAuthGuard)
export class RoleController {
  constructor(
    private roleService: RoleService,
    private featureRoleService: FeatureRoleService,
    private generalSettingRoleService: GeneralSettingRoleService,
    private symbolSettingRoleService: SymbolSettingRoleService
  ) {}

  @AdminPermission(ADMIN_PERMISSION.GET_ROLE.permission_id)
  @Get('/list')
  async listRole(@Query() query: QueryRoleDto, @Res() res: Response) {
    const { user_id } = query
    const result = await this.roleService.listRole({ user_id })
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.GET_ROLE.permission_id)
  @Get('/:id')
  async getRoleDetail(@Param('id') id: string, @Res() res: Response) {
    const result = await this.roleService.getRoleDetail(id)
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.CREATE_ROLE.permission_id)
  @Post('')
  async createRole(
    @Body() body: PostRoleDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.roleService.createRole({ ...body, owner_created })
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.UPDATE_ROLE.permission_id)
  @Put('/order')
  async updateOrderRole(
    @Body() body: PutRoleOrderDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.roleService.updateOrderRole({
      roles: body.roles,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.UPDATE_ROLE.permission_id)
  @Put('/:id')
  async updateRole(
    @Param('id') id: string,
    @Body() body: PutRoleDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.roleService.updateRole({
      ...body,
      owner_created,
      id,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.DELETE_ROLE.permission_id)
  @Delete('/:id')
  async deleteRole(@Param('id') id: string, @Res() res: Response) {
    const result = await this.roleService.deleteRole(id)
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.UPDATE_ROLE.permission_id)
  @Put('/feature/:role_id')
  async modifyFeatureRole(
    @Body() body: PutFeatureRoleDto,
    @Param('role_id') role_id: string,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    // const result = await SyncPipeRoleModify.promise(this.featureRoleService.modifyFeatureRole.bind(this.featureRoleService), {...body, owner_created, role_id})
    const result = await this.featureRoleService.modifyFeatureRole({
      ...body,
      owner_created,
      role_id,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.UPDATE_ROLE.permission_id)
  @Put('/general-setting/:role_id')
  async modifyGSRole(
    @Body() body: PutGeneralSettingRoleDto,
    @Param('role_id') role_id: string,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    // const result = await SyncPipeRoleModify.promise(this.generalSettingRoleService.modifyGeneralSettingRole.bind(this.generalSettingRoleService),{...body, owner_created, role_id})
    const result =
      await this.generalSettingRoleService.modifyGeneralSettingRole({
        ...body,
        owner_created,
        role_id,
      })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.UPDATE_ROLE.permission_id)
  @Put('/symbol-setting/:role_id')
  async createSSRole(
    @Body() body: PutSymbolSettingRoleDto,
    @Param('role_id') role_id: string,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    // const result = await SyncPipeRoleModify.promise(this.symbolSettingRoleService.modifySymbolSettingRole.bind(this.symbolSettingRoleService), {...body, owner_created, role_id})
    const result = await this.symbolSettingRoleService.modifySymbolSettingRole({
      ...body,
      owner_created,
      role_id,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
}
