import {
  IsString,
  IsArray,
  IsOptional,
  ValidateNested,
  IsNumber,
  IsBoolean,
  IsIn,
  IsNumberString,
} from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import { ROLE_STATUS, ROLE_TYPE } from 'src/const'

class GeneralSettingRole {
  @ApiProperty({
    required: true,
    description: 'Id of general setting',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  general_setting_id: string

  @ApiProperty({
    required: false,
    example: '',
  })
  @IsOptional()
  @IsString()
  description: string

  @ApiProperty({
    required: false,
    example: 10,
  })
  @IsOptional()
  @IsNumber()
  val_limit: number
}
class FeatureRole {
  @ApiProperty({
    required: true,
    description: 'Id of feature',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  feature_id: string

  @ApiProperty({
    required: false,
    example: '',
  })
  @IsOptional()
  @IsString()
  description: string
}
export class PutSymbolSettingRoleDto {
  @ApiProperty({
    required: true,
    description: 'List of symbol',
    example: ['BTC', 'ETH'],
  })
  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  list_symbol: string[]

  @ApiProperty({
    required: true,
    description: 'List of exchange',
    example: ['exchange_1', 'exchange_2'],
  })
  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  list_exchanged: string[]
  @ApiProperty({
    required: true,
    description: 'List of resolution',
    example: ['resolution_1', 'resolution_2'],
  })
  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  supported_resolutions: string[]
  @ApiProperty({
    required: false,
    example: '',
  })
  @IsOptional()
  @IsString()
  description: string
}
export class PostRoleDto {
  @ApiProperty({
    required: true,
    description: 'Name of role',
    example: 'Customer Support',
  })
  @IsString()
  role_name: string

  @ApiProperty({
    required: false,
    description: 'Describe detail about role',
    example: `Allow CS view feature and package of customer`,
  })
  @IsOptional()
  @IsString()
  description: string

  @ApiProperty({
    required: true,
    description: `Status: ${Object.values(ROLE_STATUS)}`,
    example: ROLE_STATUS.OPEN,
  })
  @IsString()
  @IsIn(Object.values(ROLE_STATUS))
  status: string

  @ApiProperty({
    required: true,
    description: 'Type of role Pack',
    example: ROLE_TYPE.PACKAGE,
  })
  @IsString()
  @IsIn(Object.values(ROLE_TYPE))
  type: string

  @ApiProperty({
    required: false,
    description: 'Price upgrade',
    example: '0.001',
  })
  @IsNumberString()
  price: string

  @ApiProperty({
    required: false,
    description: 'currency',
    example: 'TLCT',
  })
  @IsString()
  currency: string
  @ApiProperty({
    required: false,
    description: 'parent role id',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsOptional()
  @IsString()
  parent_id: string

  @ApiProperty({
    required: false,
    description: 'Set highlight for role',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  is_best_choice: boolean

  @ApiProperty({
    required: false,
    description: 'Sort role',
    example: 1,
  })
  @IsOptional()
  @IsString()
  order: number

  @ApiProperty({
    required: false,
    description: 'List feature display for user',
    example: [
      'Crypto Data',
      'Footprint chat',
      'Volume profile',
      'Delta & cumulative delta',
      'Multiple panels',
    ],
  })
  @IsArray()
  @IsString({ each: true })
  description_features: string[]

  @ApiProperty({
    required: false,
    description: 'Color role card on UI',
    example: '0000FF',
  })
  @IsOptional()
  @IsString()
  color: string
}
export class PutRoleDto {
  @ApiProperty({
    required: true,
    description: 'Name of role',
    example: 'Customer Support',
  })
  @IsOptional()
  @IsString()
  role_name: string

  @ApiProperty({
    required: false,
    description: 'Describe detail about role',
    example: `Allow CS view feature and package of customer`,
  })
  @IsOptional()
  @IsString()
  description: string

  @ApiProperty({
    required: true,
    description: `Status: ${Object.values(ROLE_STATUS)}`,
    example: ROLE_STATUS.OPEN,
  })
  @IsOptional()
  @IsString()
  @IsIn(Object.values(ROLE_STATUS))
  status: string

  @ApiProperty({
    required: true,
    description: 'Type of role Pack',
    example: ROLE_TYPE.PACKAGE,
  })
  @IsOptional()
  @IsString()
  @IsIn(Object.values(ROLE_TYPE))
  type: string

  @ApiProperty({
    required: false,
    description: 'Price upgrade',
    example: '0.001',
  })
  @IsOptional()
  @IsNumberString()
  price: string

  @ApiProperty({
    required: false,
    description: 'currency',
    example: 'TLCT',
  })
  @IsOptional()
  @IsString()
  currency: string

  @ApiProperty({
    required: false,
    description: 'parent role id',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsOptional()
  @IsString()
  parent_id: string

  @ApiProperty({
    required: false,
    description: 'Set highlight for role',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  is_best_choice: boolean

  @ApiProperty({
    required: false,
    description: 'Sort role',
    example: 1,
  })
  @IsOptional()
  @IsNumber()
  order: number

  @ApiProperty({
    required: false,
    description: 'List feature display for user',
    example: [
      'Crypto Data',
      'Footprint chat',
      'Volume profile',
      'Delta & cumulative delta',
      'Multiple panels',
    ],
  })
  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  description_features: string[]

  @ApiProperty({
    required: false,
    description: 'Color role card on UI',
    example: '0000FF',
  })
  @IsOptional()
  @IsString()
  color: string
}
export class PutFeatureRoleDto {
  @ApiProperty({
    required: false,
    description: 'List of feature',
    example: [
      {
        feature_id: 'fd97c746-72c7-4eac-98b2-e5a9ea4278fb',
        description: '',
      },
    ],
  })
  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => FeatureRole)
  features: FeatureRole[]
}
export class PutGeneralSettingRoleDto {
  @ApiProperty({
    required: false,
    description: 'List of general setting',
    example: [
      {
        general_setting_id: 'fd97c746-72c7-4eac-98b2-e5a9ea4278fb',
        description: '',
        val_limit: 10,
      },
    ],
  })
  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => GeneralSettingRole)
  general_settings: GeneralSettingRole[]
}

export class QueryRoleDto {
  @ApiProperty({
    required: false,
    description: 'id of user',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsOptional()
  @IsString()
  user_id: string
}

class RoleOrderDto {
  @ApiProperty({
    required: true,
    description: 'id of role',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id: string
  @ApiProperty({
    required: true,
    description: 'order of role',
    example: 1,
  })
  @IsString()
  order: number
}
export class PutRoleOrderDto {
  @ApiProperty({
    required: true,
    description: 'list roles',
    example: [
      { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2', order: 1 },
      { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e3', order: 2 },
    ],
  })
  @IsArray()
  roles: RoleOrderDto[]
}
