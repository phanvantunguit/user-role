import { ApiProperty } from '@nestjs/swagger'
import {
  IsArray,
  IsIn,
  IsNumber,
  IsNumberString,
  IsObject,
  IsOptional,
  IsString,
} from 'class-validator'
import { BOT_STATUS } from 'src/const'

export class QueryBotTradingDto {
  @ApiProperty({
    required: false,
    description: 'id of user',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsOptional()
  @IsString()
  user_id: string
}
export class PostBotTradingDto {
  @ApiProperty({
    required: true,
    description: 'Name of bot',
  })
  @IsString()
  name: string

  @ApiProperty({
    required: false,
    description: 'Clone Name of bot',
  })
  @IsOptional()
  @IsString()
  clone_name: string

  @ApiProperty({
    required: true,
    description: 'Bot code',
  })
  @IsString()
  code: string

  @ApiProperty({
    required: true,
    description: 'Bot type',
  })
  @IsString()
  type: string

  @ApiProperty({
    required: true,
    description: `Status: ${Object.values(BOT_STATUS)}`,
    example: BOT_STATUS.OPEN,
  })
  @IsString()
  @IsIn(Object.values(BOT_STATUS))
  status: BOT_STATUS

  @ApiProperty({
    required: false,
    description: 'Price upgrade',
    example: '0.001',
  })
  @IsNumberString()
  price: string

  @ApiProperty({
    required: false,
    description: 'Display price',
    example: '0.001',
  })
  @IsOptional()
  @IsNumberString()
  display_price: string

  @ApiProperty({
    required: false,
    description: 'Balance default',
    example: '500000',
  })
  @IsOptional()
  @IsNumberString()
  balance: string

  @ApiProperty({
    required: false,
    description: 'Pnl upgrade',
    example: '999.9',
  })
  @IsOptional()
  @IsNumberString()
  pnl: string

  @ApiProperty({
    required: false,
    description: 'Max Drawdown',
    example: '30.31',
  })
  @IsOptional()
  @IsNumberString()
  max_drawdown: string

  @ApiProperty({
    required: false,
    description: 'Max drawdown change percent',
  })
  @IsOptional()
  @IsNumberString()
  max_drawdown_change_percent: string

  @ApiProperty({
    required: true,
    description: 'currency',
    example: 'TLCT',
  })
  @IsString()
  currency: string

  @ApiProperty({
    required: false,
    description: 'Describe detail about bot',
  })
  @IsOptional()
  @IsString()
  description?: string

  @ApiProperty({
    required: false,
    description: 'Sort role',
    example: 1,
  })
  @IsOptional()
  @IsNumber()
  order?: number

  @ApiProperty({
    required: false,
    description: 'List feature display for user',
    example: [
      'Crypto Data',
      'Footprint chat',
      'Volume profile',
      'Delta & cumulative delta',
      'Multiple panels',
    ],
  })
  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  work_based_on?: string[]

  @ApiProperty({
    required: false,
    description: 'translation',
  })
  @IsOptional()
  translation: any

  @ApiProperty({
    required: true,
    description: 'Url image',
  })
  @IsString()
  image_url: string

  @ApiProperty({
    required: false,
    description: 'Data back test',
  })
  @IsOptional()
  @IsString()
  back_test: string

  @ApiProperty({
    required: false,
    description: 'Number of user bought',
  })
  @IsOptional()
  @IsNumber()
  bought: number
}

export class PutBotTradingDto {
  @ApiProperty({
    required: false,
    description: 'Name of bot',
  })
  @IsOptional()
  @IsString()
  name?: string

  @ApiProperty({
    required: false,
    description: 'Clone Name of bot',
  })
  @IsOptional()
  @IsString()
  clone_name: string

  @ApiProperty({
    required: false,
    description: 'Bot code',
  })
  @IsOptional()
  @IsString()
  code?: string

  @ApiProperty({
    required: false,
    description: 'Bot type',
    example: 'FUTURE',
  })
  @IsOptional()
  @IsString()
  type?: string

  @ApiProperty({
    required: false,
    description: `Status: ${Object.values(BOT_STATUS)}`,
    example: BOT_STATUS.OPEN,
  })
  @IsOptional()
  @IsString()
  @IsIn(Object.values(BOT_STATUS))
  status?: BOT_STATUS

  @ApiProperty({
    required: false,
    description: 'Price upgrade',
    example: '0.001',
  })
  @IsOptional()
  @IsNumberString()
  price?: string

  @ApiProperty({
    required: false,
    description: 'Display price',
    example: '0.001',
  })
  @IsOptional()
  @IsNumberString()
  display_price: string

  @ApiProperty({
    required: false,
    description: 'Balance default',
    example: '500000',
  })
  @IsOptional()
  @IsNumberString()
  balance: string

  @ApiProperty({
    required: false,
    description: 'Pnl upgrade',
    example: '999.9',
  })
  @IsOptional()
  @IsNumberString()
  pnl: string

  @ApiProperty({
    required: false,
    description: 'Max Drawdown',
    example: '30.31',
  })
  @IsOptional()
  @IsNumberString()
  max_drawdown: string

  @ApiProperty({
    required: false,
    description: 'Max drawdown change percent',
  })
  @IsOptional()
  @IsNumberString()
  max_drawdown_change_percent: string

  @ApiProperty({
    required: false,
    description: 'currency',
    example: 'TLCT',
  })
  @IsOptional()
  @IsString()
  currency?: string

  @ApiProperty({
    required: false,
    description: 'Describe detail about bot',
  })
  @IsOptional()
  @IsString()
  description?: string

  @ApiProperty({
    required: false,
    description: 'Sort role',
    example: 1,
  })
  @IsOptional()
  @IsNumber()
  order?: number

  @ApiProperty({
    required: false,
    description: 'List feature display for user',
    example: [
      'Crypto Data',
      'Footprint chat',
      'Volume profile',
      'Delta & cumulative delta',
      'Multiple panels',
    ],
  })
  @IsArray()
  @IsString({ each: true })
  work_based_on?: string[]

  @ApiProperty({
    required: false,
    description: 'translation',
  })
  @IsOptional()
  translation: any

  @ApiProperty({
    required: false,
    description: 'Url image',
  })
  @IsString()
  image_url?: string

  @ApiProperty({
    required: false,
    description: 'Data back test',
  })
  @IsOptional()
  @IsString()
  back_test: string

  @ApiProperty({
    required: false,
    description: 'Number of user bought',
  })
  @IsOptional()
  @IsNumber()
  bought: number
}

class BotTradingOrderDto {
  @ApiProperty({
    required: true,
    description: 'id of bot',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id: string
  @ApiProperty({
    required: true,
    description: 'order of bot',
    example: 1,
  })
  @IsNumber()
  order: number
}

export class PutBotTradingOrderDto {
  @ApiProperty({
    required: true,
    description: 'list bot',
    example: [
      { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2', order: 1 },
      { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e3', order: 2 },
    ],
  })
  @IsArray()
  bots: BotTradingOrderDto[]
}
