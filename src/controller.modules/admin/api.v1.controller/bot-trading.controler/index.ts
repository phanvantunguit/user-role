import {
  Param,
  Body,
  Controller,
  Get,
  Post,
  Put,
  UseGuards,
  Res,
  Req,
  Query,
  Delete,
} from '@nestjs/common'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { ADMIN_PERMISSION } from 'src/const'
import { BotTradingService } from 'src/services/bot-trading'

import { resSuccess } from 'src/utils/response.util'
import { AdminAuthGuard, AdminPermission } from '../../const'
import {
  PostBotTradingDto,
  QueryBotTradingDto,
  PutBotTradingDto,
  PutBotTradingOrderDto,
} from './dto'
@ApiBearerAuth()
@ApiTags('admin/bot-trading')
@Controller('bot-trading')
@UseGuards(AdminAuthGuard)
export class AdminBotTradingController {
  constructor(private botTradingService: BotTradingService) {}

  @AdminPermission(ADMIN_PERMISSION.GET_BOT_TRADING.permission_id)
  @Get('/list')
  async listBot(@Query() query: QueryBotTradingDto, @Res() res: Response) {
    const { user_id } = query
    const result = await this.botTradingService.listBot({ user_id })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.GET_BOT_TRADING.permission_id)
  @Get('/:id')
  async getBotDetail(@Param('id') id: string, @Res() res: Response) {
    const result = await this.botTradingService.get({ id })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.CREATE_BOT_TRADING.permission_id)
  @Post('')
  async createBotTrading(
    @Body() body: PostBotTradingDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.botTradingService.createBot({
      ...body,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.UPDATE_BOT_TRADING.permission_id)
  @Put('/:id')
  async updateBot(
    @Param('id') id: string,
    @Body() body: PutBotTradingDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.botTradingService.updateBot({
      id,
      ...body,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.UPDATE_BOT_TRADING.permission_id)
  @Put('/order')
  async updateOrderBot(
    @Body() body: PutBotTradingOrderDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.botTradingService.updateOrderBot({
      bots: body.bots,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.DELETE_BOT_TRADING.permission_id)
  @Delete('/:id')
  async deleteBot(
    @Param('id') id: string,
    @Req() req: any,
    @Res() res: Response
  ) {
    const result = await this.botTradingService.handleBotWithoutUserId({
      bot_id: id,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
}
