import {
  Param,
  Body,
  Controller,
  Get,
  Post,
  Put,
  UseGuards,
  Res,
  Req,
  Delete,
  UseInterceptors,
  UploadedFile,
  HttpStatus,
} from '@nestjs/common'
import { FileInterceptor } from '@nestjs/platform-express'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { ADMIN_PERMISSION, ERROR_CODE } from 'src/const'
import { AppSettingService } from 'src/services/app-setting'
import { BotSettingService } from 'src/services/bot-setting'
import { CurrencyService } from 'src/services/currency'
import { ExchangeService } from 'src/services/exchange'
import { FeatureService } from 'src/services/feature'
import { GeneralSettingService } from 'src/services/general-setting'
import { PackageService } from 'src/services/package'
import { ResolutionService } from 'src/services/resolution'
import { SymbolService } from 'src/services/symbol'
import { StorageResource } from 'src/resources/storage'

import { resSuccess } from 'src/utils/response.util'
import { AdminAuthGuard, AdminPermission } from '../../const'
import {
  PostAppSettingDto,
  PostBotSettingDto,
  PostCurrencyDto,
  PostExchangeDto,
  PostFeatureDto,
  PostGeneralSettingDto,
  PostPackageDto,
  PostResolutionDto,
  PostSymbolDto,
  PutAppSettingDto,
  PutBotSettingDto,
  PutCurrencyDto,
  PutCurrencyOrderDto,
  PutExchangeDto,
  PutFeatureDto,
  PutGeneralSettingDto,
  PutPackageDto,
  PutResolutionDto,
  PutSymbolDto,
} from './dto'
import { throwError } from 'src/utils/handle-error.util'
@ApiBearerAuth()
@ApiTags('admin/setting')
@Controller('setting')
@UseGuards(AdminAuthGuard)
export class AdminSettingController {
  constructor(
    private featureService: FeatureService,
    private exchangeService: ExchangeService,
    private generalSettingService: GeneralSettingService,
    private resolutionService: ResolutionService,
    private symbolService: SymbolService,
    private appSettingService: AppSettingService,
    private botSettingService: BotSettingService,
    private packageService: PackageService,
    private currencyService: CurrencyService,
    private storageResource: StorageResource
  ) {}
  @Get('feature/list')
  @AdminPermission(ADMIN_PERMISSION.GET_FEATURE.permission_id)
  async getFeatures(@Res() res: Response) {
    const result = await this.featureService.getFeature()

    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('feature/:id')
  @AdminPermission(ADMIN_PERMISSION.GET_FEATURE.permission_id)
  async getFeatureDetail(@Param('id') id: string, @Res() res: Response) {
    const result = await this.featureService.getFeatureDetail({
      feature_id: id,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @Post('feature')
  @AdminPermission(ADMIN_PERMISSION.CREATE_FEATURE.permission_id)
  async createFeatures(
    @Body() body: PostFeatureDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.featureService.createFeature({
      ...body,
      owner_created,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('feature')
  @AdminPermission(ADMIN_PERMISSION.UPDATE_FEATURE.permission_id)
  async updateFeature(
    @Body() body: PutFeatureDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.featureService.updateFeature({
      ...body,
      owner_created,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Delete('feature/:id')
  @AdminPermission(ADMIN_PERMISSION.DELETE_FEATURE.permission_id)
  async deleteFeature(@Param('id') id: string, @Res() res: Response) {
    const result = await this.featureService.deleteFeature([id])
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('exchange/list')
  @AdminPermission(ADMIN_PERMISSION.GET_EXCHANGE.permission_id)
  async listExchange(@Res() res: Response) {
    const result = await this.exchangeService.listExchange()

    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('exchange/:name')
  @AdminPermission(ADMIN_PERMISSION.GET_EXCHANGE.permission_id)
  async getExchange(@Param('name') name: string, @Res() res: Response) {
    const result = await this.exchangeService.getExchange({
      exchange_name: name,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Post('exchange')
  @AdminPermission(ADMIN_PERMISSION.CREATE_EXCHANGE.permission_id)
  async createExchanges(
    @Body() body: PostExchangeDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const result = await this.exchangeService.createExchange(body.exchanges)

    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('exchange')
  @AdminPermission(ADMIN_PERMISSION.UPDATE_EXCHANGE.permission_id)
  async updateExchange(
    @Body() body: PutExchangeDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const result = await this.exchangeService.updateExchange({ ...body })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Delete('exchange/:name')
  @AdminPermission(ADMIN_PERMISSION.DELETE_EXCHANGE.permission_id)
  async deleteExchange(@Param('name') name: string, @Res() res: Response) {
    const result = await this.exchangeService.deleteExchange([name])
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('general-setting/list')
  @AdminPermission(ADMIN_PERMISSION.GET_GENERAL_SETTING.permission_id)
  async listGeneralSetting(@Res() res: Response) {
    const result = await this.generalSettingService.listGeneralSetting()

    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('general-setting/:id')
  @AdminPermission(ADMIN_PERMISSION.GET_GENERAL_SETTING.permission_id)
  async getGeneralSetting(@Param('id') id: string, @Res() res: Response) {
    const result = await this.generalSettingService.getGeneralSetting({
      general_setting_id: id,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Post('general-setting')
  @AdminPermission(ADMIN_PERMISSION.CREATE_GENERAL_SETTING.permission_id)
  async createGeneralSetting(
    @Body() body: PostGeneralSettingDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.generalSettingService.createGeneralSetting({
      ...body,
      owner_created,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('general-setting')
  @AdminPermission(ADMIN_PERMISSION.UPDATE_EXCHANGE.permission_id)
  async updateGeneralSetting(
    @Body() body: PutGeneralSettingDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.generalSettingService.updateGeneralSetting({
      ...body,
      owner_created,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Delete('general-setting/:id')
  @AdminPermission(ADMIN_PERMISSION.DELETE_GENERAL_SETTING.permission_id)
  async deleteGeneralSetting(@Param('id') id: string, @Res() res: Response) {
    const result = await this.generalSettingService.deleteGeneralSetting([id])
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('resolution/list')
  @AdminPermission(ADMIN_PERMISSION.GET_RESOLUTION.permission_id)
  async listResolution(@Res() res: Response) {
    const result = await this.resolutionService.listResolution()

    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('resolution/:id')
  @AdminPermission(ADMIN_PERMISSION.GET_RESOLUTION.permission_id)
  async getResolution(@Param('id') id: string, @Res() res: Response) {
    const result = await this.resolutionService.getResolution({ id })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Post('resolution')
  @AdminPermission(ADMIN_PERMISSION.CREATE_RESOLUTION.permission_id)
  async createResolution(
    @Body() body: PostResolutionDto,
    @Res() res: Response
  ) {
    const result = await this.resolutionService.createResolution(
      body.resolutions
    )

    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('resolution/:id')
  @AdminPermission(ADMIN_PERMISSION.UPDATE_RESOLUTION.permission_id)
  async updateResolution(
    @Param('id') id: string,
    @Body() body: PutResolutionDto,
    @Res() res: Response
  ) {
    const result = await this.resolutionService.updateResolution({
      ...body,
      id,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Delete('resolution/:id')
  @AdminPermission(ADMIN_PERMISSION.DELETE_RESOLUTION.permission_id)
  async deleteResolution(@Param('id') id: string, @Res() res: Response) {
    const result = await this.resolutionService.deleteResolution([id])
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('symbol/list')
  @AdminPermission(ADMIN_PERMISSION.GET_SYMBOL.permission_id)
  async listSymbol(@Res() res: Response) {
    const result = await this.symbolService.listSymbol()

    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('symbol/:symbol')
  @AdminPermission(ADMIN_PERMISSION.GET_SYMBOL.permission_id)
  async getSymbol(@Param('symbol') symbol: string, @Res() res: Response) {
    const result = await this.symbolService.getSymbol({ symbol })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Post('symbol')
  @AdminPermission(ADMIN_PERMISSION.CREATE_SYMBOL.permission_id)
  async createSymbol(@Body() body: PostSymbolDto, @Res() res: Response) {
    const result = await this.symbolService.createSymbol(body.symbols)

    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('symbol/:symbol')
  @AdminPermission(ADMIN_PERMISSION.UPDATE_SYMBOL.permission_id)
  async updateSymbol(
    @Param('symbol') symbol: string,
    @Body() body: PutSymbolDto,
    @Res() res: Response
  ) {
    const result = await this.symbolService.updateSymbol({ ...body, symbol })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Delete('symbol/:symbol')
  @AdminPermission(ADMIN_PERMISSION.DELETE_SYMBOL.permission_id)
  async deleteSymbol(@Param('symbol') symbol: string, @Res() res: Response) {
    const result = await this.symbolService.deleteSymbol([symbol])
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('app-setting/list')
  @AdminPermission(ADMIN_PERMISSION.GET_APP_SETTING.permission_id)
  async listAppSetting(@Res() res: Response) {
    const result = await this.appSettingService.listAppSetting({})
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('app-setting/:id')
  @AdminPermission(ADMIN_PERMISSION.GET_APP_SETTING.permission_id)
  async getAppSetting(@Param('id') id: string, @Res() res: Response) {
    const result = await this.appSettingService.getAppSetting({ id })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Post('app-setting')
  @AdminPermission(ADMIN_PERMISSION.CREATE_APP_SETTING.permission_id)
  async createAppSetting(
    @Body() body: PostAppSettingDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.appSettingService.createAppSetting({
      ...body,
      owner_created,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('app-setting/:id')
  @AdminPermission(ADMIN_PERMISSION.UPDATE_SYMBOL.permission_id)
  async updateAppSetting(
    @Param('id') id: string,
    @Body() body: PutAppSettingDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.appSettingService.updateAppSetting({
      ...body,
      id,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @Delete('app-setting/:id')
  @AdminPermission(ADMIN_PERMISSION.DELETE_SYMBOL.permission_id)
  async deleteAppSetting(@Param('id') id: string, @Res() res: Response) {
    const result = await this.appSettingService.deleteAppSetting(id)
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('bot/list')
  @AdminPermission(ADMIN_PERMISSION.GET_BOT_SETTING.permission_id)
  async listBotSetting(@Res() res: Response) {
    const result = await this.botSettingService.list({})
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('bot/:id')
  @AdminPermission(ADMIN_PERMISSION.GET_BOT_SETTING.permission_id)
  async getBotSetting(@Param('id') id: string, @Res() res: Response) {
    const result = await this.botSettingService.get({ id })
    resSuccess({
      payload: result,
      res,
    })
  }
  @Post('bot')
  @AdminPermission(ADMIN_PERMISSION.CREATE_APP_SETTING.permission_id)
  async creatrBotSetting(
    @Body() body: PostBotSettingDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.botSettingService.save({
      ...body,
      owner_created,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('bot/:id')
  @AdminPermission(ADMIN_PERMISSION.UPDATE_SYMBOL.permission_id)
  async updateBotSetting(
    @Param('id') id: string,
    @Body() body: PutBotSettingDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.botSettingService.save({
      ...body,
      id,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @Delete('bot/:id')
  @AdminPermission(ADMIN_PERMISSION.DELETE_SYMBOL.permission_id)
  async deleteBotSetting(@Param('id') id: string, @Res() res: Response) {
    const result = await this.botSettingService.delete({ id })
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('package/list')
  @AdminPermission(ADMIN_PERMISSION.GET_PACKAGE.permission_id)
  async getList(@Res() res: Response) {
    const result = await this.packageService.list({})
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('package/:id')
  @AdminPermission(ADMIN_PERMISSION.GET_PACKAGE.permission_id)
  async getPackage(@Param('id') id: string, @Res() res: Response) {
    const result = await this.packageService.checkNotFoundAndThrow({ id })
    resSuccess({
      payload: result,
      res,
    })
  }
  @Post('package')
  @AdminPermission(ADMIN_PERMISSION.CREATE_PACKAGE.permission_id)
  async createPackage(
    @Body() body: PostPackageDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.packageService.save({
      ...body,
      owner_created,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('package/:id')
  @AdminPermission(ADMIN_PERMISSION.UPDATE_PACKAGE.permission_id)
  async updatePackage(
    @Param('id') id: string,
    @Body() body: PutPackageDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.packageService.save({
      ...body,
      id,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @Delete('package/:id')
  @AdminPermission(ADMIN_PERMISSION.DELETE_PACKAGE.permission_id)
  async deletePackage(@Param('id') id: string, @Res() res: Response) {
    const result = await this.packageService.delete({ id })
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('currency/list')
  @AdminPermission(ADMIN_PERMISSION.GET_CURRENCY.permission_id)
  async getCurrencyList(@Res() res: Response) {
    const result = await this.currencyService.list({})
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.UPDATE_ROLE.permission_id)
  @Put('currency/order')
  async updateOrderRole(
    @Body() body: PutCurrencyOrderDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.currencyService.updateOrderCurrency({
      currencies: body.currencies,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('currency/:id')
  @AdminPermission(ADMIN_PERMISSION.GET_CURRENCY.permission_id)
  async getCurrency(@Param('id') id: string, @Res() res: Response) {
    const result = await this.currencyService.get({ id })
    resSuccess({
      payload: result,
      res,
    })
  }
  @Post('currency')
  @AdminPermission(ADMIN_PERMISSION.CREATE_CURRENCY.permission_id)
  async createCurrency(
    @Body() body: PostCurrencyDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.currencyService.save({
      ...body,
      owner_created,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('currency/:id')
  @AdminPermission(ADMIN_PERMISSION.UPDATE_CURRENCY.permission_id)
  async updateCurrency(
    @Param('id') id: string,
    @Body() body: PutCurrencyDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.currencyService.save({
      ...body,
      id,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @Delete('currency/:id')
  @AdminPermission(ADMIN_PERMISSION.DELETE_CURRENCY.permission_id)
  async deleteCurrency(@Param('id') id: string, @Res() res: Response) {
    const result = await this.currencyService.delete({ id })
    resSuccess({
      payload: result,
      res,
    })
  }

  @Post('/upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file, @Res() res: Response) {
    if (file) {
      const file_url = await this.storageResource.upload(file)
      resSuccess({
        payload: { file_url },
        res,
      })
    } else {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BAD_REQUEST,
      })
    }
  }
}
