import {
  IsString,
  IsOptional,
  IsArray,
  IsObject,
  IsBoolean,
  IsNumber,
  IsIn,
} from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'
import { PACKAGE_TYPE, SYMBOL_STATUS } from 'src/const'

export class PutFeatureDto {
  @ApiProperty({
    required: true,
    description: 'Id unique define specific feature',
    example: 'GET_CHART',
  })
  @IsString()
  feature_id: string
  @ApiProperty({
    required: true,
    description: 'Name of feature',
    example: 'View chart',
  })
  @IsString()
  feature_name: string
  @ApiProperty({
    required: false,
    description: 'Describe detail about feature',
    example: 'View chart 1',
  })
  @IsOptional()
  @IsString()
  description: string
  @ApiProperty({
    required: false,
    description: 'Action of feature (create, update, view)',
    example: 'view',
  })
  @IsOptional()
  @IsString()
  action: string
}
export class PostFeatureDto {
  @ApiProperty({
    required: true,
    description: 'Features',
    example: [
      {
        feature_id: 'GET_CHART',
        feature_name: 'View chart',
        description: 'View chart 1',
        action: 'view',
      },
    ],
  })
  @IsArray()
  features: PutFeatureDto[]
}

export class PutExchangeDto {
  @ApiProperty({
    required: true,
    description: 'Name of exchange',
    example: 'exchange',
  })
  @IsString()
  exchange_name: string

  @ApiProperty({
    required: true,
    description: 'Describe detail about exchange',
    example: 'describe',
  })
  @IsString()
  exchange_desc: string
}
export class PostExchangeDto {
  @ApiProperty({
    required: false,
    description: 'Exchanges',
    example: [
      {
        exchange_name: 'exchange',
        exchange_desc: 'describe',
      },
    ],
  })
  @IsArray()
  exchanges: PutExchangeDto[]
}

export class PutGeneralSettingDto {
  @ApiProperty({
    required: true,
    description: 'Id of general setting',
    example: 'template_qty',
  })
  @IsString()
  general_setting_id: string

  @ApiProperty({
    required: true,
    description: 'Name of general setting',
    example: 'Template quantity',
  })
  @IsString()
  general_setting_name: string

  @ApiProperty({
    required: false,
    description: 'Describe detail about setting',
    example: "Number of customer's template",
  })
  @IsOptional()
  @IsString()
  description: string
}
export class PostGeneralSettingDto {
  @ApiProperty({
    required: false,
    description: 'General settings',
    example: [
      {
        general_setting_id: 'template_qty',
        general_setting_name: 'Template quantity',
        description: "Number of customer's template",
      },
    ],
  })
  @IsArray()
  general_settings: PutGeneralSettingDto[]
}

export class PutResolutionDto {
  @ApiProperty({
    required: true,
    description: 'resolutions',
    example: '720',
  })
  @IsString()
  resolutions_name: string

  @ApiProperty({
    required: true,
    description: 'Display name of resolutions',
    example: '12h',
  })
  @IsString()
  display_name: string
}
export class PostResolutionDto {
  @ApiProperty({
    required: true,
    description: 'resolutions',
    example: [
      {
        resolutions_name: '720',
        display_name: '12h',
      },
    ],
  })
  @IsArray()
  resolutions: PutResolutionDto[]
}

export class PutSymbolDto {
  @ApiProperty({
    required: false,
    description: 'Type of symbol',
    example: 'type 1',
  })
  @IsOptional()
  @IsString()
  types: string

  @ApiProperty({
    required: false,
    description: 'Name of exchange',
    example: 'exchange',
  })
  @IsOptional()
  @IsString()
  exchange_name: string

  @ApiProperty({
    required: true,
    description: 'base symbol',
    example: 'base_symbol',
  })
  @IsOptional()
  @IsString()
  base_symbol: string

  @ApiProperty({
    required: false,
    description: 'quote symbol',
    example: 'quote_symbol',
  })
  @IsOptional()
  @IsString()
  quote_symbol: string

  @ApiProperty({
    required: false,
    description: '',
    example: 'Bitcoin / TetherUS',
  })
  @IsOptional()
  @IsString()
  description: string

  @ApiProperty({
    required: false,
    description: '',
    example: {
      tick: 10,
      stepbase: 3,
      stepquote: 5,
      tickvalue: 10,
      muldecimal: 0,
      numdecimal: 1,
      ticksizemin: 0.000001,
    },
  })
  @IsOptional()
  @IsObject()
  ticks: object

  @ApiProperty({
    required: true,
    description: 'Status of symbol',
    example: SYMBOL_STATUS.ON,
  })
  @IsOptional()
  @IsIn(Object.values(SYMBOL_STATUS))
  status: SYMBOL_STATUS

  @ApiProperty({
    required: false,
    example: 'UTC',
  })
  @IsOptional()
  @IsString()
  timezone: string

  @ApiProperty({
    required: false,
    example: 1,
  })
  @IsOptional()
  @IsNumber()
  minmov: number

  @ApiProperty({
    required: false,
    example: 0,
  })
  @IsOptional()
  @IsNumber()
  minmov2: number

  @ApiProperty({
    required: false,
    example: 1,
  })
  @IsOptional()
  @IsNumber()
  pointvalue: number

  @ApiProperty({
    required: false,
    example: '24x7',
  })
  @IsOptional()
  @IsString()
  session: string

  @ApiProperty({
    required: false,
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  has_intraday: boolean

  @ApiProperty({
    required: false,
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  has_no_volume: boolean

  @ApiProperty({
    required: false,
    example: 10000,
  })
  @IsOptional()
  @IsNumber()
  pricescale: number
}
class SymbolDto {
  @ApiProperty({
    required: true,
    description: 'Name of symbol',
    example: 'BTC',
  })
  @IsString()
  symbol: string
  @ApiProperty({
    required: true,
    description: 'Type of symbol',
    example: 'type 1',
  })
  @IsString()
  types: string

  @ApiProperty({
    required: true,
    description: 'Name of exchange',
    example: 'exchange',
  })
  @IsString()
  exchange_name: string

  @ApiProperty({
    required: true,
    description: 'base symbol',
    example: 'base_symbol',
  })
  @IsString()
  base_symbol: string

  @ApiProperty({
    required: true,
    description: 'quote symbol',
    example: 'quote_symbol',
  })
  @IsString()
  quote_symbol: string

  @ApiProperty({
    required: false,
    description: '',
    example: 'Bitcoin / TetherUS',
  })
  @IsOptional()
  @IsString()
  description: string

  @ApiProperty({
    required: true,
    description: '',
    example: {
      tickvalue: 10,
      tickvalueHeatmap: 10,
    },
  })
  @IsOptional()
  @IsObject()
  ticks: object

  @ApiProperty({
    required: true,
    description: 'Status of symbol',
    example: SYMBOL_STATUS.ON,
  })
  @IsIn(Object.values(SYMBOL_STATUS))
  status: SYMBOL_STATUS

  @ApiProperty({
    required: false,
    example: 'UTC',
  })
  @IsOptional()
  @IsString()
  timezone: string

  @ApiProperty({
    required: false,
    example: 1,
  })
  @IsOptional()
  @IsNumber()
  minmov: number

  @ApiProperty({
    required: false,
    example: 0,
  })
  @IsOptional()
  @IsNumber()
  minmov2: number

  @ApiProperty({
    required: false,
    example: 1,
  })
  @IsOptional()
  @IsNumber()
  pointvalue: number

  @ApiProperty({
    required: false,
    example: '24x7',
  })
  @IsOptional()
  @IsString()
  session: string

  @ApiProperty({
    required: false,
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  has_intraday: boolean

  @ApiProperty({
    required: false,
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  has_no_volume: boolean

  @ApiProperty({
    required: false,
    example: 10000,
  })
  @IsOptional()
  @IsNumber()
  pricescale: number
}
export class PostSymbolDto {
  @ApiProperty({
    required: false,
    description: 'Symbols',
    example: [
      {
        symbol: 'BTC',
        types: 'type 1',
        exchange_name: 'exchange',
        base_symbol: 'base_symbol',
        quote_symbol: 'base_symbol',
        description: 'Bitcoin / TetherUS',
        ticks: {
          tick: 10,
          stepbase: 3,
          stepquote: 5,
          tickvalue: 10,
          muldecimal: 0,
          numdecimal: 1,
          ticksizemin: 0.000001,
        },
      },
    ],
  })
  @IsArray()
  symbols: SymbolDto[]
}

export class PostAppSettingDto {
  @ApiProperty({
    required: true,
    description: 'Name of app setting',
    example: 'ON_OFF_REGISTER',
  })
  @IsString()
  name: string

  @ApiProperty({
    required: true,
    description: 'Value of app setting',
    example: 'ON',
  })
  @IsString()
  value: string

  @ApiProperty({
    required: false,
    description: 'Description of app setting',
    example: '',
  })
  @IsOptional()
  @IsString()
  description: string
}
export class PutAppSettingDto {
  @ApiProperty({
    required: false,
    description: 'Name of app setting',
    example: 'ON_OFF_REGISTER',
  })
  @IsOptional()
  @IsString()
  name: string

  @ApiProperty({
    required: false,
    description: 'Value of app setting',
    example: 'ON',
  })
  @IsOptional()
  @IsString()
  value: string

  @ApiProperty({
    required: false,
    description: 'Description of app setting',
    example: '',
  })
  @IsOptional()
  @IsString()
  description: string
}

export class PostBotSettingDto {
  @ApiProperty({
    required: true,
    description: 'Name of bot setting',
    example: 'Violency Pinbar',
  })
  @IsString()
  name: string

  @ApiProperty({
    required: true,
    description: 'Params of bot setting',
    example: {},
  })
  @IsObject()
  params: any

  @ApiProperty({
    required: true,
    description: 'Status of bot setting',
    example: true,
  })
  @IsBoolean()
  status: boolean
}
export class PutBotSettingDto {
  @ApiProperty({
    required: false,
    description: 'Name of bot setting',
    example: 'Violency Pinbar',
  })
  @IsOptional()
  @IsString()
  name: string

  @ApiProperty({
    required: false,
    description: 'Params of bot setting',
    example: {},
  })
  @IsOptional()
  @IsObject()
  params: any

  @ApiProperty({
    required: false,
    description: 'Status of bot setting',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  status: boolean
}

export class PostPackageDto {
  @ApiProperty({
    required: true,
    description: 'Name of package',
    example: '3 months',
  })
  @IsString()
  name: string

  @ApiProperty({
    required: false,
    description: 'Type of package MONTH or DAY',
    example: PACKAGE_TYPE.MONTH,
  })
  @IsOptional()
  @IsString()
  type: PACKAGE_TYPE

  @ApiProperty({
    required: false,
    description: 'Discount rate',
    example: 0.1,
  })
  @IsOptional()
  @IsNumber()
  discount_rate: number

  @ApiProperty({
    required: false,
    description: 'Discount amount',
    example: 10,
  })
  @IsOptional()
  @IsNumber()
  discount_amount: number

  @ApiProperty({
    required: true,
    description: 'Status of package',
    example: true,
  })
  @IsBoolean()
  status: boolean

  @ApiProperty({
    required: true,
    description: 'Quantity of months',
    example: 1,
  })
  @IsNumber()
  quantity: number

  @ApiProperty({
    required: false,
    description: 'Expires for package',
    example: 1660186416132,
  })
  @IsOptional()
  @IsNumber()
  expires_at: number
}
export class PutPackageDto {
  @ApiProperty({
    required: true,
    description: 'Name of package',
    example: '3 months',
  })
  @IsOptional()
  @IsString()
  name: string

  @ApiProperty({
    required: true,
    description: 'Type of package MONTH or DAY',
    example: PACKAGE_TYPE.MONTH,
  })
  @IsOptional()
  @IsString()
  type: PACKAGE_TYPE

  @ApiProperty({
    required: false,
    description: 'Discount rate',
    example: 0.1,
  })
  @IsOptional()
  @IsNumber()
  discount_rate: number

  @ApiProperty({
    required: false,
    description: 'Discount amount',
    example: '10',
  })
  @IsOptional()
  @IsNumber()
  discount_amount: number

  @ApiProperty({
    required: true,
    description: 'Status of package',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  status: boolean

  @ApiProperty({
    required: true,
    description: 'Quantity of months',
    example: '1',
  })
  @IsOptional()
  @IsNumber()
  quantity: number

  @ApiProperty({
    required: false,
    description: 'Expires for package',
    example: 1660186416132,
  })
  @IsOptional()
  @IsNumber()
  expires_at: number
}

export class PostCurrencyDto {
  @ApiProperty({
    required: true,
    description: 'Name of currency',
    example: 'Litecoin Testnet2',
  })
  @IsString()
  name: string

  @ApiProperty({
    required: true,
    description: 'Code display',
    example: 'LTCT',
  })
  @IsString()
  description: string

  @ApiProperty({
    required: true,
    description: 'Currency',
    example: 'LTCT',
  })
  @IsString()
  currency: string

  @ApiProperty({
    required: true,
    description: 'Link of image currency',
    example: 'link',
  })
  @IsString()
  image_url: string
}
export class PutCurrencyDto {
  @ApiProperty({
    required: false,
    description: 'Name of currency',
    example: 'Litecoin Testnet2',
  })
  @IsOptional()
  @IsString()
  name: string

  @ApiProperty({
    required: false,
    description: 'Code display',
    example: 'LTCT',
  })
  @IsOptional()
  @IsString()
  description: string

  @ApiProperty({
    required: false,
    description: 'Currency',
    example: 'LTCT',
  })
  @IsOptional()
  @IsString()
  currency: string

  @ApiProperty({
    required: false,
    description: 'Link of image currency',
    example: 'link',
  })
  @IsOptional()
  @IsString()
  image_url: string

  @ApiProperty({
    required: false,
    description: 'On off currency',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  status: boolean
}
class CurrencyOrderDto {
  @ApiProperty({
    required: true,
    description: 'id of currency',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id: string
  @ApiProperty({
    required: true,
    description: 'order of role',
    example: 1,
  })
  @IsString()
  order: number
}
export class PutCurrencyOrderDto {
  @ApiProperty({
    required: true,
    description: 'list currency',
    example: [
      { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2', order: 1 },
      { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e3', order: 2 },
    ],
  })
  @IsArray()
  currencies: CurrencyOrderDto[]
}
