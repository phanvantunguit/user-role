import {
  Param,
  Body,
  Controller,
  Get,
  Put,
  UseGuards,
  Res,
  Req,
  Query,
  Delete,
  Post,
  Inject,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common'
import { FileInterceptor } from '@nestjs/platform-express'
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { ADMIN_PERMISSION } from 'src/const'
import { BotTradingHistoryService } from 'src/services/bot-trading-history'
import { AdminImportTradeHistoryCSVHandler } from 'src/usecases/bot/admin-import-trade-history-csv'
import { AdminListTradeHistoryHandler } from 'src/usecases/bot/admin-list-trade-history'
import { AdminListTradeHistoryInput } from 'src/usecases/bot/admin-list-trade-history/validate'

import { resSuccess } from 'src/utils/response.util'
import { AdminAuthGuard, AdminPermission } from '../../const'
import { PostBotTradingHistoryDto, QueryBotTradingHistoryDto } from './dto'
@ApiBearerAuth()
@ApiTags('admin/bot-trading-history')
@Controller('bot-trading-history')
@UseGuards(AdminAuthGuard)
export class AdminBotTradingHistoryController {
  constructor(
    private botTradingHistoryService: BotTradingHistoryService,
    @Inject(AdminImportTradeHistoryCSVHandler)
    private adminImportTradeHistoryCSVHandler: AdminImportTradeHistoryCSVHandler,
    @Inject(AdminListTradeHistoryHandler) private adminListTradeHistoryHandler: AdminListTradeHistoryHandler
  ) {}

  @AdminPermission(ADMIN_PERMISSION.GET_SYSTEM_TRADE_HISTORY.permission_id)
  @Get('/system-trade-history')
  async tradeHistory(
    @Query() query: AdminListTradeHistoryInput,
    @Res() res: Response
  ) {
    const result = await this.adminListTradeHistoryHandler.execute(
      query
    )
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.GET_BOT_TRADING_HISTORY.permission_id)
  @Get('/list')
  async listBot(
    @Query() query: QueryBotTradingHistoryDto,
    @Res() res: Response
  ) {
    const result = await this.botTradingHistoryService.listBotTradingHistory(
      query
    )
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.CREATE_BOT_TRADING_HISTORY.permission_id)
  @Post('')
  async createBotTradingHistory(
    @Body() body: PostBotTradingHistoryDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.botTradingHistoryService.createBot({
      ...body,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.CREATE_BOT_TRADING_HISTORY.permission_id)
  @Post('/import-csv')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        bot_id: { type: 'string' },
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(FileInterceptor('file'))
  async importBotTradingHistory(
    @UploadedFile() file,
    @Body() body: any,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.adminImportTradeHistoryCSVHandler.execute(
      file,
      body,
      owner_created
    )
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.UPDATE_BOT_TRADING_HISTORY.permission_id)
  @Put('/:id')
  async updateBot(
    @Param('id') id: string,
    @Body() body: PostBotTradingHistoryDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.botTradingHistoryService.save({
      id,
      ...body,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.DELETE_BOT_TRADING_HISTORY.permission_id)
  @Delete('/:id')
  async deleteBot(
    @Param('id') id: string,
    @Req() req: any,
    @Res() res: Response
  ) {
    const result = await this.botTradingHistoryService.delete({
      id,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
}
