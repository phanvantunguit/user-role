import { ApiProperty } from '@nestjs/swagger'
import {
  IsArray,
  IsIn,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator'
import { TRADE_HISTORY_STATUS, TRADE_SIDE } from 'src/const/bot'

export class QueryBotTradingHistoryDto {
  @ApiProperty({
    required: false,
    description: 'Page',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  page: number

  @ApiProperty({
    required: false,
    description: 'Size',
    example: 50,
  })
  @IsOptional()
  @IsNumberString()
  size: number

  @ApiProperty({
    required: false,
    example: 'efebdafe-c359-4e88-bd17-82b2a5b1091b',
  })
  @IsOptional()
  @IsUUID()
  bot_id: string

  @ApiProperty({
    required: false,
    description: `${Object.keys(TRADE_HISTORY_STATUS).join(' or ')}`,
    example: TRADE_HISTORY_STATUS.OPEN,
  })
  @IsOptional()
  @IsString()
  status: TRADE_HISTORY_STATUS

  @ApiProperty({
    required: false,
    description: 'id of bot trading history',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsOptional()
  @IsString()
  user_id: string

  @ApiProperty({
    required: false,
    description: 'From',
  })
  @IsOptional()
  @IsNumberString()
  from: number

  @ApiProperty({
    required: false,
    description: 'To',
  })
  @IsOptional()
  @IsNumberString()
  to: number
}

export class PostBotTradingHistoryDto {
  @ApiProperty({
    required: true,
    example: 'efebdafe-c359-4e88-bd17-82b2a5b1091b',
  })
  @IsUUID()
  bot_id: string

  @ApiProperty({
    required: true,
    description: 'Name token frist',
  })
  @IsString()
  token_first: string

  @ApiProperty({
    required: false,
    description: 'Name token second',
  })
  @IsOptional()
  @IsString()
  token_second: string

  @ApiProperty({
    required: true,
    description: `Status: ${Object.values(TRADE_HISTORY_STATUS)}`,
    example: TRADE_HISTORY_STATUS.OPEN,
  })
  @IsString()
  @IsIn(Object.values(TRADE_HISTORY_STATUS))
  status: TRADE_HISTORY_STATUS

  @ApiProperty({
    required: true,
    description: 'Side',
    example: 'LONG',
  })
  @IsString()
  @IsIn(Object.keys(TRADE_SIDE))
  side: TRADE_SIDE

  @ApiProperty({
    required: true,
    description: 'Price entry',
    example: '0.009',
  })
  @IsNumberString()
  entry_price: string

  @ApiProperty({
    required: false,
    description: 'Price close',
    example: '0.009',
  })
  @IsOptional()
  @IsNumberString()
  close_price: string

  @ApiProperty({
    required: false,
    description: 'Profit/loss',
    example: '14',
  })
  @IsOptional()
  @IsString()
  profit: string

  @ApiProperty({
    required: true,
    description: 'Day start',
    example: '1676000403889',
  })
  @IsNumber()
  day_started: number

  @ApiProperty({
    required: false,
    description: 'Day completed',
    example: '1676000403889',
  })
  @IsOptional()
  @IsNumber()
  day_completed: number

  @ApiProperty({
    required: false,
    description: 'user of bot trading history',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsOptional()
  @IsString()
  user_id: string
}
