import {
  Param,
  Body,
  Controller,
  Get,
  Post,
  Put,
  UseGuards,
  Res,
  Req,
  Query,
  Delete,
} from '@nestjs/common'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { ADMIN_PERMISSION } from 'src/const'
import { BotService } from 'src/services/bot'

import { resSuccess } from 'src/utils/response.util'
import { AdminAuthGuard, AdminPermission } from '../../const'
import { PostBotDto, PutBotDto, PutBotOrderDto, QueryBotDto } from './dto'
@ApiBearerAuth()
@ApiTags('admin/bot')
@Controller('bot')
@UseGuards(AdminAuthGuard)
export class AdminBotController {
  constructor(private botService: BotService) {}

  @AdminPermission(ADMIN_PERMISSION.GET_BOT.permission_id)
  @Get('/list')
  async listBot(@Query() query: QueryBotDto, @Res() res: Response) {
    const { user_id } = query
    const result = await this.botService.listBot({ user_id })
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.GET_BOT.permission_id)
  @Get('/:id')
  async getBotDetail(@Param('id') id: string, @Res() res: Response) {
    const result = await this.botService.get({ id })
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.CREATE_BOT.permission_id)
  @Post('')
  async createBot(
    @Body() body: PostBotDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.botService.createBot({ ...body, owner_created })
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.UPDATE_BOT.permission_id)
  @Put('/:id')
  async updateBot(
    @Param('id') id: string,
    @Body() body: PutBotDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.botService.save({
      id,
      ...body,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.UPDATE_BOT.permission_id)
  @Put('/order')
  async updateOrderBot(
    @Body() body: PutBotOrderDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.botService.updateOrderBot({
      bots: body.bots,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.DELETE_BOT.permission_id)
  @Delete('/:id')
  async deleteBot(
    @Param('id') id: string,
    @Req() req: any,
    @Res() res: Response
  ) {
    const result = await this.botService.handleBotWithoutUserId({
      bot_id: id,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
}
