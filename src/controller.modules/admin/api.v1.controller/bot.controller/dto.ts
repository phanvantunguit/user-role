import {
  IsString,
  IsArray,
  IsOptional,
  IsIn,
  IsNumberString,
  IsNumber,
} from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'
import { BOT_STATUS } from 'src/const'

export class PostBotDto {
  @ApiProperty({
    required: true,
    description: 'Name of bot',
  })
  @IsString()
  name: string

  @ApiProperty({
    required: true,
    description: 'Bot setting id',
  })
  @IsString()
  bot_setting_id: string

  @ApiProperty({
    required: true,
    description: 'Bot type',
  })
  @IsString()
  type: string

  @ApiProperty({
    required: true,
    description: `Status: ${Object.values(BOT_STATUS)}`,
    example: BOT_STATUS.OPEN,
  })
  @IsString()
  @IsIn(Object.values(BOT_STATUS))
  status: BOT_STATUS

  @ApiProperty({
    required: false,
    description: 'Price upgrade',
    example: '0.001',
  })
  @IsNumberString()
  price: string

  @ApiProperty({
    required: true,
    description: 'currency',
    example: 'TLCT',
  })
  @IsString()
  currency: string

  @ApiProperty({
    required: false,
    description: 'Describe detail about bot',
  })
  @IsOptional()
  @IsString()
  description?: string

  @ApiProperty({
    required: false,
    description: 'Sort role',
    example: 1,
  })
  @IsOptional()
  @IsNumber()
  order?: number

  @ApiProperty({
    required: false,
    description: 'List feature display for user',
    example: [
      'Crypto Data',
      'Footprint chat',
      'Volume profile',
      'Delta & cumulative delta',
      'Multiple panels',
    ],
  })
  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  work_based_on?: string[]

  @ApiProperty({
    required: true,
    description: 'Url image',
  })
  @IsString()
  image_url: string
}
export class PutBotDto {
  @ApiProperty({
    required: false,
    description: 'Name of bot',
  })
  @IsOptional()
  @IsString()
  name?: string

  @ApiProperty({
    required: false,
    description: 'Bot setting id',
  })
  @IsOptional()
  @IsString()
  bot_setting_id?: string

  @ApiProperty({
    required: false,
    description: 'Bot type',
    example: 'FUTURE',
  })
  @IsOptional()
  @IsString()
  type?: string

  @ApiProperty({
    required: false,
    description: `Status: ${Object.values(BOT_STATUS)}`,
    example: BOT_STATUS.OPEN,
  })
  @IsOptional()
  @IsString()
  @IsIn(Object.values(BOT_STATUS))
  status?: BOT_STATUS

  @ApiProperty({
    required: false,
    description: 'Price upgrade',
    example: '0.001',
  })
  @IsOptional()
  @IsNumberString()
  price?: string

  @ApiProperty({
    required: false,
    description: 'currency',
    example: 'TLCT',
  })
  @IsOptional()
  @IsString()
  currency?: string

  @ApiProperty({
    required: false,
    description: 'Describe detail about bot',
  })
  @IsOptional()
  @IsString()
  description?: string

  @ApiProperty({
    required: false,
    description: 'Sort role',
    example: 1,
  })
  @IsOptional()
  @IsNumber()
  order?: number

  @ApiProperty({
    required: false,
    description: 'List feature display for user',
    example: [
      'Crypto Data',
      'Footprint chat',
      'Volume profile',
      'Delta & cumulative delta',
      'Multiple panels',
    ],
  })
  @IsArray()
  @IsString({ each: true })
  work_based_on?: string[]

  @ApiProperty({
    required: false,
    description: 'Url image',
  })
  @IsString()
  image_url?: string
}
export class QueryBotDto {
  @ApiProperty({
    required: false,
    description: 'id of user',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsOptional()
  @IsString()
  user_id: string
}

class BotOrderDto {
  @ApiProperty({
    required: true,
    description: 'id of bot',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id: string
  @ApiProperty({
    required: true,
    description: 'order of bot',
    example: 1,
  })
  @IsNumber()
  order: number
}
export class PutBotOrderDto {
  @ApiProperty({
    required: true,
    description: 'list bot',
    example: [
      { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2', order: 1 },
      { id: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e3', order: 2 },
    ],
  })
  @IsArray()
  bots: BotOrderDto[]
}
