import {
  IsString,
  IsEmail,
  Matches,
  IsOptional,
  ValidateNested,
  IsNumberString,
  IsBoolean,
  ValidateIf,
  IsArray,
  IsNumber,
} from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'
import { PACKAGE_TYPE, PasswordRegex } from 'src/const'
import { Type } from 'class-transformer'

export class PostLoginDto {
  @ApiProperty({
    required: true,
    description: 'Email to login Coinmap',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string

  @ApiProperty({
    required: true,
    description:
      'password must be at least 8 characters with upper case letter, lower case letter, and number',
    example: '123456Aa',
  })
  @Matches(PasswordRegex)
  password: string
}
export class AuthUserRoleDto {
  @ApiProperty({
    required: true,
    description: 'Id of auth role',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  auth_role_id: string

  @ApiProperty({
    example: '',
  })
  @IsOptional()
  @IsString()
  description?: string
}
export class UserRoleDto {
  @ApiProperty({
    required: true,
    description: 'Id of role',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  role_id: string

  @ApiProperty({
    description: 'description',
  })
  @IsOptional()
  @IsString()
  description?: string

  @ApiProperty({
    description: 'expires_at',
  })
  @IsOptional()
  @IsNumber()
  expires_at?: number
}
export class PutAdminUserProfileDto {
  @ApiProperty({
    required: false,
    description: 'Phone number',
    example: '0987654321',
  })
  @IsOptional()
  @IsString()
  phone: string

  @ApiProperty({
    required: false,
    description: 'First name',
    example: 'John',
  })
  @IsOptional()
  @IsString()
  first_name: string

  @ApiProperty({
    required: false,
    description: 'Last name',
    example: 'Nguyen',
  })
  @IsOptional()
  @IsString()
  last_name: string

  @ApiProperty({
    required: false,
    description: 'Full address',
    example: '123 To Hien Thanh, phuong 10, quan 10, tp HCM',
  })
  @IsOptional()
  @IsString()
  address: string

  @ApiProperty({
    required: false,
    description: 'Affiliate code',
    example: 'affiliate_code',
  })
  @IsOptional()
  @IsString()
  affiliate_code: string

  @ApiProperty({
    required: false,
    description: 'Link affiliate',
    example: '',
  })
  @IsOptional()
  @IsString()
  link_affiliate: string

  @ApiProperty({
    required: false,
    description: 'Referral_code',
    example: '',
  })
  @IsOptional()
  @IsString()
  referral_code: string

  @ApiProperty({
    required: false,
    description: 'Profile pic',
    example: '',
  })
  @IsOptional()
  @IsString()
  profile_pic: string

  @ApiProperty({
    required: false,
    description: 'Note updated',
    example: '',
  })
  @IsOptional()
  @IsString()
  note_updated: string

  @ApiProperty({
    required: false,
    description:
      'Password must be at least 8 characters with upper case letter, lower case letter, and number',
    example: '123456Aa',
  })
  @IsOptional()
  @Matches(new RegExp(PasswordRegex), {
    message:
      'Password must be at least 8 characters with upper case letter, lower case letter, and number',
  })
  password: string
  @ApiProperty({
    required: false,
    description:
      'Password must be at least 8 characters with upper case letter, lower case letter, and number',
    example: '123456Aa',
  })
  @ValidateIf((o) => o.password)
  @Matches(new RegExp(PasswordRegex), {
    message:
      'Old password must be at least 8 characters with upper case letter, lower case letter, and number',
  })
  old_password: string
}
export class PutUserDto extends PutAdminUserProfileDto {
  @ApiProperty({
    required: false,
    description: 'Active of inactive',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  active: boolean
  @ApiProperty({
    required: false,
    description: 'Set user is admin or not',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  is_admin: boolean
}
export class PostCreateUserAdminDto extends PutUserDto {
  @ApiProperty({
    required: true,
    description: 'Email to register account Coinmap',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string

  // @ApiProperty({
  //   required: false,
  //   description: 'List of auth role',
  //   example: [
  //     {
  //       auth_role_id: 'fd97c746-72c7-4eac-98b2-e5a9ea4278fb',
  //       description: 'Base role'
  //     },
  //     {
  //       auth_role_id: 'f4381e7d-a7df-45ac-b7b4-986e19919454',
  //       description: 'Plus role'
  //     }
  //   ]
  // })
  // @IsOptional()
  // @ValidateNested({ each: true })
  // @Type(() => AuthUserRoleDto)
  // auth_roles: AuthUserRoleDto[]

  // @ApiProperty({
  //   required: false,
  //   description: 'List of auth role',
  //   example: [
  //     {
  //       role_id: 'fd97c746-72c7-4eac-98b2-e5a9ea4278fb',
  //       description: 'Base role'
  //     },
  //     {
  //       role_id: 'f4381e7d-a7df-45ac-b7b4-986e19919454',
  //       description: 'Plus role'
  //     }
  //   ]
  // })
  // @IsOptional()
  // @ValidateNested({ each: true })
  // @Type(() => UserRoleDto)
  // roles: UserRoleDto[]
}
export class PutAuthUserRoleDto {
  @ApiProperty({
    required: true,
    description: 'Id of user',
    example: '8c9e1891-b5b0-4949-bcfe-a2e25c1e53e6',
  })
  @IsString()
  user_id: string

  @ApiProperty({
    required: true,
    description: 'List of auth role',
    example: [
      {
        auth_role_id: 'fd97c746-72c7-4eac-98b2-e5a9ea4278fb',
        description: 'Base role',
      },
      {
        auth_role_id: 'f4381e7d-a7df-45ac-b7b4-986e19919454',
        description: 'Plus role',
      },
    ],
  })
  @ValidateNested({ each: true })
  @Type(() => AuthUserRoleDto)
  auth_roles: AuthUserRoleDto[]
}

export class PutUserRoleDto {
  @ApiProperty({
    required: true,
    description: 'Id of user',
    example: '8c9e1891-b5b0-4949-bcfe-a2e25c1e53e6',
  })
  @IsString()
  user_id: string

  @ApiProperty({
    required: true,
    description: 'List of auth role',
    example: [
      {
        role_id: 'fd97c746-72c7-4eac-98b2-e5a9ea4278fb',
        description: 'Base role',
      },
      {
        role_id: 'f4381e7d-a7df-45ac-b7b4-986e19919454',
        description: 'Plus role',
      },
    ],
  })
  @ValidateNested({ each: true })
  @Type(() => UserRoleDto)
  roles: UserRoleDto[]
}

export class AddMultipleUserRoleDto {
  @ApiProperty({
    required: true,
    description: 'Id of user',
    example: ['8c9e1891-b5b0-4949-bcfe-a2e25c1e53e6'],
  })
  @IsArray()
  user_ids: string[]

  @ApiProperty({
    required: true,
    description: 'Id of roles',
    example: ['8c9e1891-b5b0-4949-bcfe-a2e25c1e53e6'],
  })
  @IsArray()
  role_ids: string[]

  @ApiProperty({
    description: 'quantity',
  })
  @IsOptional()
  @IsNumber()
  quantity: number

  @ApiProperty({
    required: false,
    description: 'Type of package MONTH or DAY',
    example: PACKAGE_TYPE.MONTH,
  })
  @ValidateIf((o) => o.quantity)
  @IsOptional()
  @IsString()
  type: PACKAGE_TYPE
}
export class RemoveMultipleUserRoleDto {
  @ApiProperty({
    required: true,
    description: 'Id of user',
    example: ['8c9e1891-b5b0-4949-bcfe-a2e25c1e53e6'],
  })
  @IsArray()
  user_ids: string[]

  @ApiProperty({
    required: true,
    description: 'Id of role',
    example: ['8c9e1891-b5b0-4949-bcfe-a2e25c1e53e6'],
  })
  @IsArray()
  role_ids: string[]
}
export class GetUserDto {
  @ApiProperty({
    required: false,
    description: 'Page',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  page: number

  @ApiProperty({
    required: false,
    description: 'Size',
    example: 50,
  })
  @IsOptional()
  @IsNumberString()
  size: number

  @ApiProperty({
    required: false,
    description: 'The keyword to find user ex: phone number, name, email',
    example: 'john',
  })
  @IsOptional()
  @IsString()
  keyword: string

  @ApiProperty({
    required: false,
    description: 'List role filter',
    example: 'Role 1, role 2',
  })
  @IsOptional()
  @IsString()
  roles?: string

  @ApiProperty({
    required: false,
    description: 'List bot filter',
    example: 'Bot 1, bot 2',
  })
  @IsOptional()
  @IsString()
  bots?: string

  @ApiProperty({
    required: false,
    description: 'Merchant code',
    example: 'CM',
  })
  @IsOptional()
  @IsString()
  merchant_code?: string
}

export class GetAllUserDto {
  @ApiProperty({
    required: false,
    description: 'The keyword to find user ex: phone number, name, email',
    example: 'john',
  })
  @IsOptional()
  @IsString()
  keyword: string
}
