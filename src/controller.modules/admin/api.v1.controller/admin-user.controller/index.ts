import {
  Controller,
  Body,
  Post,
  Put,
  Get,
  Res,
  Param,
  Inject,
  Req,
  UseGuards,
  Query,
  HttpStatus,
  Delete,
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger'
import { Response } from 'express'

import { ADMIN_PERMISSION, WITHOUT_AUTHORIZATION, USER_TYPE, ERROR_CODE } from 'src/const'
import { APP_SETTING } from 'src/const/app-setting'
import { BaseApiOutput } from 'src/controller.modules/user/const'
import { MerchantRepository } from 'src/repositories/merchant.repository'
import { AuthUserRoleService } from 'src/services/auth-user-role'
import { UserService } from 'src/services/user'
import { UserRoleService } from 'src/services/user-role'
import { AdminDeleteBotTradingHandler } from 'src/usecases/bot/admin-delete-bot-trading'
import { AdminUpdateBotTradingStatusHandler } from 'src/usecases/bot/admin-update-bot-trading-status'
import {
  AdminUpdateBotTradingStatusInput,
  AdminUpdateBotTradingStatusOutput,
} from 'src/usecases/bot/admin-update-bot-trading-status/validate'
import { AdminResetSubscriptionHandler } from 'src/usecases/bot/reset-subscriptions'
import { UserListBotHandler } from 'src/usecases/bot/user-list-bot'
import { UserListBotTradingHandler } from 'src/usecases/bot/user-list-bot-trading'
import { UserListBotTradingOutput } from 'src/usecases/bot/user-list-bot-trading/validate'
import { UserListBotOutput } from 'src/usecases/bot/user-list-bot/validate'
import { UserListRoleHandler } from 'src/usecases/role/user-list-role'
import { ListAssetLogHandler } from 'src/usecases/transaction/list-asset-log'
import {
  ListAssetLogPayloadOutput,
  ListAssetLogInput,
} from 'src/usecases/transaction/list-asset-log/validate'
import { AdminAddUserAssetHandler } from 'src/usecases/user-asset/admin-add-user-asset'
import { AdminAddUserAssetInput } from 'src/usecases/user-asset/admin-add-user-asset/validate'
import { AdminRemoveUserAssetHandler } from 'src/usecases/user-asset/admin-remove-user-asset'
import { AdminRemoveUserAssetInput } from 'src/usecases/user-asset/admin-remove-user-asset/validate'
import { throwError } from 'src/utils/handle-error.util'
import { decodeBase64 } from 'src/utils/hash.util'
import { resSuccess } from 'src/utils/response.util'
import { AdminAuthGuard, AdminPermission } from '../../const'
import {
  PutAuthUserRoleDto,
  PostLoginDto,
  GetUserDto,
  PostCreateUserAdminDto,
  PutUserDto,
  PutUserRoleDto,
  PutAdminUserProfileDto,
  AddMultipleUserRoleDto,
  RemoveMultipleUserRoleDto,
  GetAllUserDto,
} from './dto'

@ApiBearerAuth()
@UseGuards(AdminAuthGuard)
@ApiTags('admin')
@Controller('')
export class AdminUserController {
  constructor(
    private userService: UserService,
    @Inject(AuthUserRoleService)
    private authUserRoleService: AuthUserRoleService,
    @Inject(UserRoleService) private userRoleService: UserRoleService,
    @Inject(ListAssetLogHandler)
    private listAssetLogHandler: ListAssetLogHandler,
    @Inject(UserListRoleHandler)
    private userListRoleHandler: UserListRoleHandler,
    @Inject(UserListBotHandler) private userListBotHandler: UserListBotHandler,
    @Inject(UserListBotTradingHandler)
    private userListBotTradingHandler: UserListBotTradingHandler,
    @Inject(AdminAddUserAssetHandler)
    private adminAddUserAssetHandler: AdminAddUserAssetHandler,
    @Inject(AdminRemoveUserAssetHandler)
    private adminRemoveUserAssetHandler: AdminRemoveUserAssetHandler,
    @Inject(AdminUpdateBotTradingStatusHandler)
    private adminUpdateBotTradingStatusHandler: AdminUpdateBotTradingStatusHandler,
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
    @Inject(AdminResetSubscriptionHandler) private adminResetSubscriptionHandler: AdminResetSubscriptionHandler,
    @Inject(AdminDeleteBotTradingHandler) private adminDeleteBotTradingHandler: AdminDeleteBotTradingHandler
  ) {}
  @AdminPermission(WITHOUT_AUTHORIZATION)
  @Post('login')
  async login(
    @Req() req: any,
    @Body() body: PostLoginDto,
    @Res() res: Response
  ) {
    const userAgent = req.get('user-agent')
    const ipAddress =
      req.headers['x-forwarded-for'] || req.socket.remoteAddress || null
    const result = await this.userService.loginUser(
      {
        ...body,
        user_type: USER_TYPE.ADMIN,
      },
      userAgent,
      ipAddress
    )
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.CREATE_USER.permission_id)
  @Post('/user')
  async createUser(
    @Body() body: PostCreateUserAdminDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.userService.createUser({
      ...body,
      owner_created,
      is_admin: false,
      merchant_code: APP_SETTING.MERCHANT_CODE_DEFAULT,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.CREATE_ADMIN.permission_id)
  @Post('/admin')
  async createAdmin(
    @Body() body: PostCreateUserAdminDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.userService.createUser({
      ...body,
      owner_created,
      is_admin: true,
      merchant_code: APP_SETTING.MERCHANT_CODE_DEFAULT,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('/admin/profile')
  async updateUserProfile(
    @Body() body: PutAdminUserProfileDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const user_id = req.user.user_id
    const result = await this.userService.updateInfo(
      { ...body, id: user_id },
      true
    )
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.UPDATE_USER.permission_id)
  @Put('/user/:id')
  async updateUser(
    @Body() body: PutUserDto,
    @Param('id') id: string,
    @Res() res: Response
  ) {
    const result = await this.userService.updateInfo({ ...body, id }, false)
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.UPDATE_ADMIN.permission_id)
  @Put('/admin/:id')
  async updateAdmin(
    @Body() body: PutUserDto,
    @Param('id') id: string,
    @Res() res: Response
  ) {
    const result = await this.userService.updateInfo({ ...body, id }, true)
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.UPDATE_AUTH_ROLE.permission_id)
  @Put('auth-user-role')
  async createAuthUserRole(
    @Body() body: PutAuthUserRoleDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.authUserRoleService.modifyAuthUserRole({
      ...body,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.UPDATE_USER_ROLE.permission_id)
  @Put('user-role')
  async createUserRole(
    @Body() body: PutUserRoleDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.userRoleService.modifyUserRole({
      ...body,
      owner_created,
    })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.UPDATE_USER_ROLE.permission_id)
  @Put('add-multiple-user-role')
  async addUserRole(
    @Body() body: AddMultipleUserRoleDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const owner_created = req.user.user_id
    const result = await this.userRoleService.addUserRoles({
      user_ids: body.user_ids,
      owner_created: owner_created,
      role_ids: body.role_ids,
      quantity: body.quantity,
      package_type: body.type,
    })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.UPDATE_USER_ROLE.permission_id)
  @Put('remove-multiple-user-role')
  async removeUserRole(
    @Body() body: RemoveMultipleUserRoleDto,
    @Res() res: Response,
    @Req() req: any
  ) {
    const owner_created = req.user.user_id
    const result = await this.userRoleService.removeUserRoles(owner_created, {
      ...body,
    })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.UPDATE_USER.permission_id)
  @Put('add-user-asset')
  async AddUserAsset(
    @Body() body: AdminAddUserAssetInput,
    @Res() res: Response,
    @Req() req: any
  ) {
    const owner_created = req.user.user_id
    const result = await this.adminAddUserAssetHandler.execute(
      body,
      owner_created
    )
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.UPDATE_USER.permission_id)
  @Put('remove-user-asset')
  async RemoveUserAsset(
    @Body() body: AdminRemoveUserAssetInput,
    @Res() res: Response,
    @Req() req: any
  ) {
    const owner_created = req.user.user_id
    const result = await this.adminRemoveUserAssetHandler.execute(
      body,
      owner_created
    )
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.GET_USER.permission_id)
  @Get('/user/list')
  async getUser(@Query() query: GetUserDto, @Res() res: Response) {
    const result = await this.userService.getUserPagination({
      ...query,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.GET_USER.permission_id)
  @Get('/user/all')
  async getAllUser(@Query() query: GetAllUserDto, @Res() res: Response) {
    const result = await this.userService.listUser({
      ...query,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @AdminPermission(ADMIN_PERMISSION.GET_ADMIN.permission_id)
  @Get('/admin/list')
  async getAdmin(@Query() query: GetUserDto, @Res() res: Response) {
    const result = await this.userService.getUserPagination({
      ...query,
      is_admin: true,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('/admin/profile')
  async getUserProfile(@Req() req: any, @Res() res: Response) {
    const user_id = req.user.user_id
    const result = await this.userService.getUser({ id: user_id })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.GET_USER.permission_id)
  @Get('/user/:id/asset')
  @ApiResponse({
    status: 200,
    type: class ListAssetLogPayloadOutputMap extends IntersectionType(
      ListAssetLogPayloadOutput,
      BaseApiOutput
    ) {},
    description: 'List asset for user',
  })
  async getAssetList(
    @Res() res: Response,
    @Query() query: ListAssetLogInput,
    @Param('id') id: string
  ) {
    const data = await this.listAssetLogHandler.execute(query, false, id)
    resSuccess({
      payload: data,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.GET_USER.permission_id)
  @Get('/user/:id/role')
  async getRoleList(@Res() res: Response, @Param('id') id: string) {
    const data = await this.userListRoleHandler.execute(id)
    resSuccess({
      payload: data,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.GET_USER.permission_id)
  @Get('/user/:id/sbot')
  @ApiResponse({
    status: 200,
    type: class UserListBotOutputMap extends IntersectionType(
      UserListBotOutput,
      BaseApiOutput
    ) {},
    description: 'List asset for user',
  })
  async getBotList(@Res() res: Response, @Param('id') id: string) {
    const data = await this.userListBotHandler.execute(id)
    resSuccess({
      payload: data,
      res,
    })
  }


  @AdminPermission(ADMIN_PERMISSION.UPDATE_USER.permission_id)
  @Put('/user/tbot/reset-subscriptions')
  async resetSubscriptions(
    @Res() res: Response,
  ) {
    const data = await this.adminResetSubscriptionHandler.execute()
    resSuccess({
      payload: data,
      res,
    })
  }

  @AdminPermission(WITHOUT_AUTHORIZATION)
  @Put('/user/tbot/:id')
  @ApiResponse({
    status: 200,
    type: class AdminUpdateBotTradingStatusOutputMap extends IntersectionType(
      AdminUpdateBotTradingStatusOutput,
      BaseApiOutput
    ) {},
    description: 'Update User Bot Trading Status',
  })
  async updateUserBotTradingStatus(
    @Req() req: any,
    @Res() res: Response,
    @Param('id') id: string,
    @Body() body: AdminUpdateBotTradingStatusInput
  ) {
    const { authorization } = req.headers
    if(!authorization) {
      throwError({
        status: HttpStatus.UNAUTHORIZED,
        ...ERROR_CODE.AUTHORIZATION_REQUIRED,
      })
    }
    const [type, encode] = authorization.split(' ')
    if (type.toLowerCase() === 'basic' && encode) { 
      const decode = decodeBase64(encode)
      const [code, password] = decode.split(':')
      const merchant = await this.merchantRepository.findOne({code})
      if(!merchant || password !== merchant.password) {
        throwError({
          status: HttpStatus.FORBIDDEN,
          ...ERROR_CODE.ACCOUNT_INVALID,
        }) 
      }
    } else {
      throwError({
        status: HttpStatus.UNAUTHORIZED,
        ...ERROR_CODE.AUTHORIZATION_REQUIRED,
      })
    }
    const data = await this.adminUpdateBotTradingStatusHandler.execute(body, id)
    resSuccess({
      payload: data,
      res,
    })
  }

  @AdminPermission(WITHOUT_AUTHORIZATION)
  @Delete('/user/tbot/:id')
  async deleteBotTrading(
    @Req() req: any,
    @Res() res: Response,
    @Param('id') id: string
  ) {
    const { authorization } = req.headers
    if(!authorization) {
      throwError({
        status: HttpStatus.UNAUTHORIZED,
        ...ERROR_CODE.AUTHORIZATION_REQUIRED,
      })
    }
    const [type, encode] = authorization.split(' ')
    if (type.toLowerCase() === 'basic' && encode) { 
      const decode = decodeBase64(encode)
      const [code, password] = decode.split(':')
      const merchant = await this.merchantRepository.findOne({code})
      if(!merchant || password !== merchant.password) {
        throwError({
          status: HttpStatus.FORBIDDEN,
          ...ERROR_CODE.ACCOUNT_INVALID,
        }) 
      }
    } else {
      throwError({
        status: HttpStatus.UNAUTHORIZED,
        ...ERROR_CODE.AUTHORIZATION_REQUIRED,
      })
    }
    const data = await this.adminDeleteBotTradingHandler.execute(id)
    resSuccess({
      payload: data,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.GET_USER.permission_id)
  @Get('/user/:id/tbot')
  @ApiResponse({
    status: 200,
    type: class UserListBotTradingOutputMap extends IntersectionType(
      UserListBotTradingOutput,
      BaseApiOutput
    ) {},
    description: 'List asset for user',
  })
  async getBotTradingList(@Res() res: Response, @Param('id') id: string) {
    const data = await this.userListBotTradingHandler.execute(id)
    resSuccess({
      payload: data,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.GET_USER.permission_id)
  @Get('/user/:id')
  async getUserDetail(@Param('id') id: string, @Res() res: Response) {
    const result = await this.userService.getUser({ id, is_admin: false })
    resSuccess({
      payload: result,
      res,
    })
  }

  @AdminPermission(ADMIN_PERMISSION.GET_ADMIN.permission_id)
  @Get('/admin/:id')
  async getAdminDetail(@Param('id') id: string, @Res() res: Response) {
    const result = await this.userService.getUser({ id, is_admin: true })
    resSuccess({
      payload: result,
      res,
    })
  }
}
