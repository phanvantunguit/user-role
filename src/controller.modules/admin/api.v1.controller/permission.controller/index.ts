import { Controller, Get, UseGuards, Res } from '@nestjs/common'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { AuthRoleService } from 'src/services/auth-role'

import { resSuccess } from 'src/utils/response.util'
import { AdminAuthGuard } from '../../const'
@ApiBearerAuth()
@ApiTags('admin/permission')
@Controller('permission')
@UseGuards(AdminAuthGuard)
export class AdminPermissionController {
  constructor(private authRoleService: AuthRoleService) {}
  @Get('/list')
  getPermissions(@Res() res: Response) {
    const result = this.authRoleService.getListPermission()
    resSuccess({
      payload: result,
      res,
    })
  }
}
