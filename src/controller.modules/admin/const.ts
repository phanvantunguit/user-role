import {
  ExecutionContext,
  Injectable,
  CanActivate,
  HttpStatus,
} from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { APP_CONFIG } from 'src/config'
import { WITHOUT_AUTHORIZATION, ERROR_CODE } from 'src/const'
import { throwError } from 'src/utils/handle-error.util'
import { verifyToken } from 'src/utils/hash.util'
import { SetMetadata } from '@nestjs/common'
import { PermissionRepository } from 'src/repositories/permission.repository'
import { UserRepository } from 'src/repositories/user.repository'
import { SessionService } from 'src/services/session'
import { ApiProperty } from '@nestjs/swagger'
import { IsString } from 'class-validator'

export const AdminPermission = (permission: string) =>
  SetMetadata('permission', permission)

@Injectable()
export class AdminAuthGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private permissionRepo: PermissionRepository,
    private userRepo: UserRepository,
    private sessionService: SessionService
  ) {}
  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const permission = this.reflector.get<string>(
      'permission',
      context.getHandler()
    )
    if (permission === WITHOUT_AUTHORIZATION) {
      return true
    }
    const request = context.switchToHttp().getRequest()

    const authHeader: string | undefined = request.get('Authorization')
    if (authHeader) {
      const [type, token] = authHeader.split(' ')
      if (type.toLowerCase() === 'bearer' && token) {
        let decode
        try {
          decode = await verifyToken(token, APP_CONFIG.TOKEN_PUBLIC_KEY)
        } catch (error) {
          throwError({
            status: HttpStatus.UNAUTHORIZED,
            ...ERROR_CODE.TOKEN_EXPIRED,
          })
        }
        // const ipAddress =
        //   request.headers['x-forwarded-for'] ||
        //   request.socket.remoteAddress ||
        //   null
        await this.sessionService.verifySession(decode)
        if (decode.super_user || !permission) {
          request.user = decode
          return true
        }
        if (decode.user_id && decode.roles && decode.roles.length > 0) {
          const userRoles = await this.userRepo.findAuthUserRole({
            user_id: decode.user_id,
          })
          const roleIds = userRoles.map((e) => e.auth_role_id)
          const roles = await this.permissionRepo.findAuthRoleByIds(roleIds)
          const isValid = roles.some((r) =>
            r.root.permissions.some((p) => p.permission_id === permission)
          )
          if (isValid) {
            request.user = decode
            return true
          }
        }
        throwError({
          status: HttpStatus.FORBIDDEN,
          ...ERROR_CODE.NO_PERMISSION,
        })
      }
    }
    throwError({
      status: HttpStatus.UNAUTHORIZED,
      ...ERROR_CODE.AUTHORIZATION_REQUIRED,
    })
  }
}

export class AdminBaseApiOutput {
  @ApiProperty({
    required: true,
    description: 'Error code of request',
    example: 'SUCCESS',
  })
  @IsString()
  error_code?: string

  @ApiProperty({
    required: true,
    description: 'Message describe error code',
    example: 'Success',
  })
  @IsString()
  message?: string
}
