import {
  ExecutionContext,
  Injectable,
  CanActivate,
  HttpStatus,
} from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { WITHOUT_AUTHORIZATION, ERROR_CODE } from 'src/const'
import { throwError } from 'src/utils/handle-error.util'
import { verifyToken } from 'src/utils/hash.util'
import { SetMetadata } from '@nestjs/common'
import { IToken } from 'src/const/authorization'
import { APP_CONFIG } from 'src/config'
import { SessionService } from 'src/services/session'
import { ApiProperty } from '@nestjs/swagger'
import { IsString } from 'class-validator'

export const UserPermission = (permission: string) =>
  SetMetadata('permission', permission)

@Injectable()
export class UserAuthGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private sessionService: SessionService
  ) {}
  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const permission = this.reflector.get<string>(
      'permission',
      context.getHandler()
    )
    const request = context.switchToHttp().getRequest()
    const authHeader: string | undefined = request.get('Authorization')
    if (authHeader) {
      const [type, token] = authHeader.split(' ')
      if (type.toLowerCase() === 'bearer' && token) {
        let decode: IToken
        try {
          decode = await verifyToken(token, APP_CONFIG.TOKEN_PUBLIC_KEY)
        } catch (error) {
          throwError({
            status: HttpStatus.UNAUTHORIZED,
            ...ERROR_CODE.TOKEN_EXPIRED,
          })
        }
        // const ipAddress =
        //   request.headers['x-forwarded-for'] ||
        //   request.socket.remoteAddress ||
        //   null
        request.user = decode
        await this.sessionService.verifySession(decode)
        return true
      }
    }
    if (permission === WITHOUT_AUTHORIZATION) {
      return true
    }
    throwError({
      status: HttpStatus.UNAUTHORIZED,
      ...ERROR_CODE.AUTHORIZATION_REQUIRED,
    })
  }
}

export class BaseApiOutput {
  @ApiProperty({
    required: true,
    description: 'Error code of request',
    example: 'SUCCESS',
  })
  @IsString()
  error_code?: string

  @ApiProperty({
    required: true,
    description: 'Message describe error code',
    example: 'Success',
  })
  @IsString()
  message?: string
}