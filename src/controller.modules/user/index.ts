import { Module } from '@nestjs/common'
import { RouterModule, Routes } from '@nestjs/core'
import { MailResource } from 'src/resources/mail'
import { SendGridTransport } from 'src/resources/mail/sendgrid.transport'
import { UserService } from 'src/services/user'

import { UserController } from './api.v1.controller/user.controller'
import { VerifyTokenService } from 'src/services/verify-token'
import { UserRoleService } from 'src/services/user-role'
import { AuthUserRoleService } from 'src/services/auth-user-role'
import { UserRoleController } from './api.v1.controller/role.controller'
import { RoleService } from 'src/services/role'
import { PublicController } from './api.v1.controller/public.controller'
import { TransactionController } from './api.v1.controller/transaction.controller'
import { TransactionService } from 'src/services/transaction'
import { CoinpaymentTransport } from 'src/resources/coinpayment/coinpayment.transport'
import { CoinpaymentResource } from 'src/resources/coinpayment'
import { AppSettingService } from 'src/services/app-setting'
import { TransactionLogService } from 'src/services/transaction-log'
import { TransactionEvent } from 'src/events/transaction.event'
import { BotSettingService } from 'src/services/bot-setting'
import { EventEmitterModule } from '@nestjs/event-emitter'
import { UserPackageController } from './api.v1.controller/package.controller'
import { PackageService } from 'src/services/package'
import { StorageResource } from 'src/resources/storage'
import { GCloudTransport } from 'src/resources/storage/google.transport'
import { CoinpaymentService } from 'src/services/payment/coinpayment'
import { PaymentService } from 'src/services/payment'
import { ResolutionService } from 'src/services/resolution'
import { CurrencyService } from 'src/services/currency'
import { SessionService } from 'src/services/session'
import { DatabaseModule } from 'src/repositories'
import { GeneralSettingRoleService } from 'src/services/general-setting-role'
import { UserBotController } from './api.v1.controller/bot.controller'
import { TransactionV2Controller } from './api.v2.controller/transaction.controller'
import { BotService } from 'src/services/bot'
import { CreateTransactionV2Handler } from 'src/usecases/transaction/user-create-transaction-v2'
import { CheckItemService } from 'src/services/transaction-v2/check-Items'
import { SaveItemService } from 'src/services/transaction-v2/save-items'
import { CmpaymentResource } from 'src/resources/cmpayment'
import { CmpaymentTransport } from 'src/resources/cmpayment/cmpayment.transport'
import { ListTransactionV2Handler } from 'src/usecases/transaction/user-list-transaction-v2'
import { GetTransactionV2Handler } from 'src/usecases/transaction/user-get-transaction-v2'
import { IPNTransactionV2Handler } from 'src/usecases/transaction/ipn-transaction-v2'
import { UserListBotHandler } from 'src/usecases/bot/user-list-bot'
import { UserRoleV2Controller } from './api.v2.controller/role.controller'
import { UserListRoleHandler } from 'src/usecases/role/user-list-role'
import { UserListSignalHandler } from 'src/usecases/bot/user-list-bot-signal'
import { UserListBotPaidHandler } from 'src/usecases/bot/user-list-bot-paid'
import { UserAddFavoriteSignalHandler } from 'src/usecases/bot/user-add-favorite-signal'
import { UserRemoveFavoriteSignalHandler } from 'src/usecases/bot/user-remove-favorite-signal'
import { ListAssetLogHandler } from 'src/usecases/transaction/list-asset-log'
import { UserListBotPlanHandler } from 'src/usecases/bot/user-list-bot-plan'
import { UserListRolePlanHandler } from 'src/usecases/role/user-list-role-plan'
import { UserBotTradingController } from './api.v1.controller/bot-trading.controller'
import { UserListBotTradingHandler } from 'src/usecases/bot/user-list-bot-trading'
import { BotTradingService } from 'src/services/bot-trading'
import { UserListBotTradingPlanHandler } from 'src/usecases/bot/user-list-bot-trading-plan'
import { ConnectBrokerServerHandler } from 'src/usecases/bot/connect-broker'
import { MetaapiResource } from 'src/resources/forex'
import { LogoutBrokerServerHandler } from 'src/usecases/bot/logout-broker'
import { UserUpdateBotTradingStatusHandler } from 'src/usecases/bot/user-update-bot-trading-status'
import { BotTradingEvent } from 'src/events/bot-trading.event'
import { UserListBotTradingHistoryHandler } from 'src/usecases/bot/user-list-bot-trading-history'
import { UserListBotTradingPNLHandler } from 'src/usecases/bot/user-list-bot-trading-pnl'
import { UserGetBotTradingHandler } from 'src/usecases/bot/user-get-bot-trading'
import { SelectAndConnectBrokerServerHandler } from 'src/usecases/bot/select-and-connect-broker'
import { BrokerResource } from 'src/resources/broker'
import { AxiResource } from 'src/resources/broker/axi'
import { XMResource } from 'src/resources/broker/xm'
import { XMTransport } from 'src/resources/broker/xm/xm.transport'
import { AxiTransport } from 'src/resources/broker/axi/axi.transport'
import { DeactivateUserBotTradingHandler } from 'src/usecases/bot/deactive-user-bot-trading'
import { BalanceAndFeeDataHandler } from 'src/usecases/bot/user-balance-and-fee'
import { CreateBalanceAndFeeDataHandler } from 'src/usecases/bot/user-create-balance-and-fee'
import { UploadBrokerProfileHandler } from 'src/usecases/bot/upload-broker-profile'
import { BrokerDefaultResource } from 'src/resources/broker/metaapi'
import { MetaapiTransport } from 'src/resources/broker/metaapi/metaapi.transport'

@Module({
  imports: [DatabaseModule],
  controllers: [
    UserController,
    UserRoleController,
    TransactionController,
    UserPackageController,
    UserBotController,
    UserBotTradingController,
  ],
  providers: [
    UserService,
    VerifyTokenService,
    MailResource,
    SendGridTransport,
    UserRoleService,
    AuthUserRoleService,
    RoleService,
    TransactionService,
    CoinpaymentResource,
    CoinpaymentTransport,
    AppSettingService,
    TransactionService,
    TransactionLogService,
    BotSettingService,
    TransactionEvent,
    PackageService,
    StorageResource,
    GCloudTransport,
    CoinpaymentService,
    PaymentService,
    ResolutionService,
    CurrencyService,
    SessionService,
    GeneralSettingRoleService,
    UserListBotHandler,
    BotService,
    BotTradingService,
    UserListSignalHandler,
    UserListBotPaidHandler,
    UserAddFavoriteSignalHandler,
    UserRemoveFavoriteSignalHandler,
    ListAssetLogHandler,
    UserListBotPlanHandler,
    UserListBotTradingHandler,
    UserGetBotTradingHandler,
    UserListBotTradingPlanHandler,
    ConnectBrokerServerHandler,
    MetaapiResource,
    LogoutBrokerServerHandler,
    UserUpdateBotTradingStatusHandler,
    BotTradingEvent,
    UserListBotTradingHistoryHandler,
    UserListBotTradingPNLHandler,
    SelectAndConnectBrokerServerHandler,
    DeactivateUserBotTradingHandler,
    BrokerResource,
    XMResource,
    AxiResource,
    XMTransport,
    AxiTransport,
    BalanceAndFeeDataHandler,
    CreateBalanceAndFeeDataHandler,
    UploadBrokerProfileHandler,
    BrokerDefaultResource,
    MetaapiTransport
  ],
})
class APIV1Controller {}

@Module({
  imports: [DatabaseModule],
  controllers: [TransactionV2Controller, UserRoleV2Controller],
  providers: [
    SessionService,
    GeneralSettingRoleService,
    RoleService,
    CreateTransactionV2Handler,
    CheckItemService,
    SaveItemService,
    CmpaymentResource,
    CmpaymentTransport,
    GetTransactionV2Handler,
    ListTransactionV2Handler,
    IPNTransactionV2Handler,
    UserListRoleHandler,
    UserListRolePlanHandler,
    CreateBalanceAndFeeDataHandler,
  ],
})
class APIV2Controller {}
@Module({
  imports: [DatabaseModule],
  controllers: [PublicController],
  providers: [
    SessionService,
    GeneralSettingRoleService,
    RoleService,
    BotSettingService,
  ],
})
class APIPublicController {}

const routes: Routes = [
  { path: '', module: APIPublicController },
  { path: 'api/v1/user', module: APIV1Controller },
  { path: 'api/v2/user', module: APIV2Controller },
]
@Module({
  imports: [
    RouterModule.register(routes),
    APIV1Controller,
    APIV2Controller,
    APIPublicController,
    EventEmitterModule.forRoot(),
  ],
  providers: [],
})
export class ApiV1UserControllerModule {}
