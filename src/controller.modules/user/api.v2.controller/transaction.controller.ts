import {
  Controller,
  UseGuards,
  Res,
  Post,
  Body,
  Req,
  Inject,
  Put,
  Get,
  Param,
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiResponse,
  ApiSecurity,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger'
import { Response } from 'express'
import { WITHOUT_AUTHORIZATION } from 'src/const'
import { APP_SETTING } from 'src/const/app-setting'
import { IPNTransactionV2Handler } from 'src/usecases/transaction/ipn-transaction-v2'
import { CreateTransactionV2Handler } from 'src/usecases/transaction/user-create-transaction-v2'
import {
  CreateTransactionV2Input,
  CreateTransactionV2PayloadOutput,
} from 'src/usecases/transaction/user-create-transaction-v2/validate'
import { GetTransactionV2Handler } from 'src/usecases/transaction/user-get-transaction-v2'
import { DetailTransactionV2PayloadOutput } from 'src/usecases/transaction/user-get-transaction-v2/validate'
import { ListTransactionV2Handler } from 'src/usecases/transaction/user-list-transaction-v2'
import { ListTransactionV2PayloadOutput } from 'src/usecases/transaction/user-list-transaction-v2/validate'
import { resSuccess } from 'src/utils/response.util'
import { BaseApiOutput, UserAuthGuard, UserPermission } from '../const'

@ApiSecurity('Refresh-Token')
@ApiBearerAuth()
@UseGuards(UserAuthGuard)
@ApiTags('user/transaction/v2')
@Controller('transaction')
export class TransactionV2Controller {
  constructor(
    @Inject(CreateTransactionV2Handler)
    private createTransactionV2Handler: CreateTransactionV2Handler,
    @Inject(GetTransactionV2Handler)
    private getTransactionV2Handler: GetTransactionV2Handler,
    @Inject(ListTransactionV2Handler)
    private listTransactionV2Handler: ListTransactionV2Handler,
    @Inject(IPNTransactionV2Handler)
    private ipnTransactionV2Handler: IPNTransactionV2Handler
  ) {}
  @Post('')
  @ApiResponse({
    status: 200,
    type: class CreateTransactionOutputMap extends IntersectionType(
      CreateTransactionV2PayloadOutput,
      BaseApiOutput
    ) {},
    description: 'Create transaction',
  })
  async createTransaction(
    @Req() req: any,
    @Body() body: CreateTransactionV2Input,
    @Res() res: Response
  ) {
    const user_id = req.user.user_id
    const email = req.user.email
    let merchantCode =
      req.user.merchant_code || APP_SETTING.MERCHANT_CODE_DEFAULT
    const result = await this.createTransactionV2Handler.execute(
      {
        ...body,
      },
      user_id,
      email,
      merchantCode
    )
    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('/ipn')
  @UserPermission(WITHOUT_AUTHORIZATION)
  async ipnTransaction(@Body() body: any, @Res() res: Response) {
    const result = await this.ipnTransactionV2Handler.execute(body)
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class ListTransactionV2PayloadOutputMap extends IntersectionType(
      ListTransactionV2PayloadOutput,
      BaseApiOutput
    ) {},
    description: 'List transaction',
  })
  async listTransaction(@Req() req: any, @Res() res: Response) {
    const user_id = req.user.user_id
    const result = await this.listTransactionV2Handler.execute(user_id)
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class DetailTransactionV2PayloadOutputMap extends IntersectionType(
      DetailTransactionV2PayloadOutput,
      BaseApiOutput
    ) {},
    description: 'Get detail transaction',
  })
  async getTransaction(@Param('id') id: string, @Res() res: Response) {
    const result = await this.getTransactionV2Handler.execute(id)
    resSuccess({
      payload: result,
      res,
    })
  }
}
