import {
  Param,
  Controller,
  Get,
  UseGuards,
  Res,
  Req,
  Query,
  Inject,
} from '@nestjs/common'
import { ApiBearerAuth, ApiSecurity, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { ROLE_STATUS, WITHOUT_AUTHORIZATION } from 'src/const'
import { UserListRoleHandler } from 'src/usecases/role/user-list-role'
import { UserListRolePlanHandler } from 'src/usecases/role/user-list-role-plan'
import { resSuccess } from 'src/utils/response.util'
import { UserAuthGuard, UserPermission } from '../const'

@ApiSecurity('Refresh-Token')
@ApiBearerAuth()
@UseGuards(UserAuthGuard)
@ApiTags('user/role/v2')
@Controller('role')
export class UserRoleV2Controller {
  constructor(
    @Inject(UserListRoleHandler)
    private userListRoleHandler: UserListRoleHandler,
    @Inject(UserListRolePlanHandler)
    private userListRolePlanHandler: UserListRolePlanHandler
  ) {}

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('/list')
  async listRole(@Req() req: any, @Res() res: Response) {
    const user_id = req.user?.user_id
    const result = await this.userListRoleHandler.execute(user_id, [ROLE_STATUS.OPEN, ROLE_STATUS.COMINGSOON])
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('/plans')
  async listPlan(@Req() req: any, @Res() res: Response) {
    const user_id = req.user.user_id
    const result = await this.userListRolePlanHandler.execute(user_id)
    resSuccess({
      payload: result,
      res,
    })
  }
}
