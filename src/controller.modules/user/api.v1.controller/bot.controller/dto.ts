import {
  IsString,
  IsOptional,
  IsObject,
  IsNumber,
  IsArray,
} from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'
import { BOT_STATUS } from 'src/const'
import { Type } from 'class-transformer'
import { ORDER_CATEGORY } from 'src/const/transaction'

export class DetailBotDto {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id: string

  @ApiProperty({
    required: true,
    example: 'MBC',
  })
  @IsString()
  name: string

  @ApiProperty({
    required: true,
    example: 'FUTURE',
  })
  @IsString()
  type: string

  @ApiProperty({
    required: true,
    example: 'Detect strong pin bar candlestick patterns with delta divergence',
  })
  @IsString()
  description: string

  @ApiProperty({
    required: true,
    description: Object.values(BOT_STATUS).join(','),
    example: BOT_STATUS.COMINGSOON,
  })
  @IsString()
  status: BOT_STATUS
  @ApiProperty({
    required: true,
    example: '96',
  })
  @IsString()
  price: string
  @ApiProperty({
    required: true,
    example: 'USD',
  })
  @IsString()
  currency: string

  @ApiProperty({
    required: true,
    example: [
      "Candle's wick length.",
      'Delta parameter.',
      'Stacked Imbalance parameter.',
      'Footprint parameter.',
    ],
    isArray: true,
  })
  @Type(() => String)
  work_based_on: string[]
  @ApiProperty({
    required: true,
    example:
      'https://static-dev.cextrading.io/images/cm-user-roles/1672212992415.png',
  })
  @IsString()
  image_url: string

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsNumber()
  created_at: number
  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsNumber()
  updated_at: number

  @ApiProperty({
    required: true,
    example: 1,
  })
  @IsNumber()
  order: number

  @ApiProperty({
    required: true,
    example: ORDER_CATEGORY.SBOT,
  })
  @IsString()
  category: string
}

export class DetailBotPayloadDto {
  @ApiProperty({
    required: true,
    description: 'Bot package info',
  })
  @IsObject()
  payload: DetailBotDto
}
