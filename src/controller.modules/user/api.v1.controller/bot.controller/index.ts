import {
  Param,
  Controller,
  Get,
  UseGuards,
  Res,
  HttpStatus,
  Inject,
  Req,
  Query,
  Post,
  Delete,
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiResponse,
  ApiSecurity,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger'
import { Response } from 'express'
import { ERROR_CODE, WITHOUT_AUTHORIZATION } from 'src/const'
import { ORDER_CATEGORY } from 'src/const/transaction'
import { BotSignalRepository } from 'src/repositories/bot-signal.repository'
import { BotRepository } from 'src/repositories/bot.repository'
import { UserAddFavoriteSignalHandler } from 'src/usecases/bot/user-add-favorite-signal'
import { UserListBotHandler } from 'src/usecases/bot/user-list-bot'
import { UserListBotPaidHandler } from 'src/usecases/bot/user-list-bot-paid'
import { UserListBotPlanHandler } from 'src/usecases/bot/user-list-bot-plan'
import { UserListSignalHandler } from 'src/usecases/bot/user-list-bot-signal'
import {
  UserListBotSignalInput,
  UserListBotSignalPayloadOutput,
} from 'src/usecases/bot/user-list-bot-signal/validate'
import { UserListBotOutput } from 'src/usecases/bot/user-list-bot/validate'
import { UserRemoveFavoriteSignalHandler } from 'src/usecases/bot/user-remove-favorite-signal'
import { throwError } from 'src/utils/handle-error.util'

import { resSuccess } from 'src/utils/response.util'
import { BaseApiOutput, UserAuthGuard, UserPermission } from '../../const'
import { DetailBotPayloadDto } from './dto'
@ApiSecurity('Refresh-Token')
@ApiBearerAuth()
@UseGuards(UserAuthGuard)
@ApiTags('user/bot')
@Controller('bot')
export class UserBotController {
  constructor(
    @Inject(BotRepository)
    private botRepository: BotRepository,
    @Inject(UserListBotHandler) private userListBotHandler: UserListBotHandler,
    @Inject(UserListSignalHandler)
    private userListSignalHandler: UserListSignalHandler,
    @Inject(UserListBotPaidHandler)
    private userListBotPaidHandler: UserListBotPaidHandler,
    @Inject(UserAddFavoriteSignalHandler)
    private userAddFavoriteSignalHandler: UserAddFavoriteSignalHandler,
    @Inject(UserRemoveFavoriteSignalHandler)
    private userRemoveFavoriteSignalHandler: UserRemoveFavoriteSignalHandler,
    @Inject(UserListBotPlanHandler)
    private userListBotPlanHandler: UserListBotPlanHandler
  ) {}

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class UserListBotOutputMap extends IntersectionType(
      UserListBotOutput,
      BaseApiOutput
    ) {},
    description: 'List bot for user',
  })
  async listBot(@Res() res: Response, @Req() req: any) {
    const user_id = req.user?.user_id
    const result = await this.userListBotHandler.execute(user_id)
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('/paid')
  @ApiResponse({
    status: 200,
    type: class UserListBotOutputMap extends IntersectionType(
      UserListBotOutput,
      BaseApiOutput
    ) {},
    description: 'List bot for user',
  })
  async listBotPaid(@Res() res: Response, @Req() req: any) {
    const user_id = req.user.user_id
    const result = await this.userListBotPaidHandler.execute(user_id)
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('/plans')
  @ApiResponse({
    status: 200,
    type: class UserListBotOutputMap extends IntersectionType(
      UserListBotOutput,
      BaseApiOutput
    ) {},
    description: 'List bot for user',
  })
  async listBotPlan(@Res() res: Response, @Req() req: any) {
    const user_id = req.user.user_id
    const result = await this.userListBotPlanHandler.execute(user_id)
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('/signals')
  @ApiResponse({
    status: 200,
    type: class UserListBotSignalPayloadOutputMap extends IntersectionType(
      UserListBotSignalPayloadOutput,
      BaseApiOutput
    ) {},
    description: 'List bot signal for user',
  })
  async listBotSignal(
    @Res() res: Response,
    @Query() query: UserListBotSignalInput,
    @Req() req: any
  ) {
    const user_id = req.user.user_id
    const result = await this.userListSignalHandler.execute(query, user_id)
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class DetailBotPayloadDtoMap extends IntersectionType(
      DetailBotPayloadDto,
      BaseApiOutput
    ) {},
    description: 'Get bot detail',
  })
  async getRoleDetail(@Param('id') id: string, @Res() res: Response) {
    const result = await this.botRepository.findById(id)
    if (result) {
      result['category'] = ORDER_CATEGORY.SBOT
      resSuccess({
        payload: result,
        res,
      })
    } else {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }
  }

  @Post('/favorite-signal/:id')
  @ApiResponse({
    status: 200,
    description: 'Add favorite signal',
  })
  async addFavoriteSignal(
    @Param('id') id: string,
    @Res() res: Response,
    @Req() req: any
  ) {
    const user_id = req.user.user_id
    const result = await this.userAddFavoriteSignalHandler.execute(user_id, id)
    if (result) {
      resSuccess({
        payload: result,
        res,
      })
    } else {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }
  }
  @Delete('/favorite-signal/:id')
  @ApiResponse({
    status: 200,
    description: 'Add remove signal',
  })
  async removeFavoriteSignal(
    @Param('id') id: string,
    @Res() res: Response,
    @Req() req: any
  ) {
    const user_id = req.user.user_id
    const result = await this.userRemoveFavoriteSignalHandler.execute(
      user_id,
      id
    )
    if (result) {
      resSuccess({
        payload: result,
        res,
      })
    } else {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }
  }
}
