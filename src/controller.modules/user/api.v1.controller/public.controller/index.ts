import {
  Controller,
  Get,
  Inject,
  Param,
  Query,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common'
import { ApiBearerAuth, ApiSecurity } from '@nestjs/swagger'
import { Response } from 'express'
import { APP_CONFIG } from 'src/config'
import { MERCHANT_STATUS, WITHOUT_AUTHORIZATION } from 'src/const'
import { APP_SETTING } from 'src/const/app-setting'
import {
  ADDITIONAL_DATA_TYPE,
  ADDITIONAL_DEFAULT_NAME,
  MERCHANT_ADDITIONAL_DATA_STATUS,
  STANDALONE_ADDITIONAL,
} from 'src/domains/merchant/types'
import { AdditionalDataRepository } from 'src/repositories/additional-data.repository'
import { EventStoreRepository } from 'src/repositories/event-store.repository'
import { MerchantAdditionalDataRepository } from 'src/repositories/merchant-additional-data.repository'
import { MerchantRepository } from 'src/repositories/merchant.repository'
import { BotSettingService } from 'src/services/bot-setting'
import { SessionService } from 'src/services/session'
import { resSuccess } from 'src/utils/response.util'
import { UserAuthGuard, UserPermission } from '../../const'

@ApiSecurity('Refresh-Token')
@ApiBearerAuth()
@UseGuards(UserAuthGuard)
@Controller('')
export class PublicController {
  constructor(
    @Inject(SessionService) private sessionService: SessionService,
    private botSettingService: BotSettingService,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
    @Inject(EventStoreRepository)
    private eventStoreRepository: EventStoreRepository,
    @Inject(MerchantAdditionalDataRepository)
    private merchantAdditionalDataRepository: MerchantAdditionalDataRepository,
    @Inject(AdditionalDataRepository)
    private additionalDataRepository: AdditionalDataRepository
  ) {}

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('/health_check')
  async listRole(@Req() req: any, @Res() res: Response) {
    const data = {
      version: APP_CONFIG.VERSION,
    }

    resSuccess({
      payload: data,
      res,
    })
  }

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('/merchant')
  async getMerchant(@Req() req: any, @Res() res: Response) {
    const { m_affiliate, origin } = req.headers
    console.log('merchant req.headers', req.headers)
    const queryMerchant = {
      status: MERCHANT_STATUS.ACTIVE,
    }
    if (m_affiliate) {
      queryMerchant['code'] = m_affiliate
    } else if (origin) {
      queryMerchant['domain'] = origin
    }

    const merchant = await this.merchantRepository.findOne(queryMerchant)

    let result
    if (merchant) {
      delete merchant.config.wallet
      delete merchant.config.wallet_active_history
      const brokers = merchant.config.brokers?.map((e) => {
        delete e?.referral_setting?.secret_key
        return e
      })
      merchant.config.brokers = brokers
      const additional = await this.merchantAdditionalDataRepository.list({
        merchant_id: merchant.id,
        type: ADDITIONAL_DATA_TYPE.FAQ,
        status: MERCHANT_ADDITIONAL_DATA_STATUS.ON,
      })
      result = {
        name: merchant.name,
        code: merchant.code,
        domain: merchant.domain,
        config: merchant.config,
        faq: additional,
      }
    } else {
      result = {
        code: APP_SETTING.MERCHANT_CODE_DEFAULT,
      }
    }
    resSuccess({
      payload: result,
      res,
    })
  }

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('/merchant/additional-data/list')
  async getMerchantAdditionalData(
    @Req() req: any,
    @Res() res: Response,
    @Query() query: any
  ) {
    const { m_affiliate } = req.headers
    console.log('req.headers', req.headers)
    const { type } = query

    let result = []
    if (m_affiliate) {
      const queryMerchant = {
        status: MERCHANT_STATUS.ACTIVE,
        code: m_affiliate,
      }
      const merchant = await this.merchantRepository.findOne(queryMerchant)
      console.log('merchant', merchant)
      if (merchant && m_affiliate) {
        result = await this.merchantAdditionalDataRepository.list({
          merchant_id: merchant.id,
          type,
          status: MERCHANT_ADDITIONAL_DATA_STATUS.ON,
        })
      }
    } 
    if(result.length === 0){
      let name
      if (STANDALONE_ADDITIONAL.includes(type)) {
        name = ADDITIONAL_DEFAULT_NAME
      }
      result = await this.additionalDataRepository.list(name, type)
    }

    resSuccess({
      payload: result,
      res,
    })
  }

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('/additional-data/list')
  async getAdditionalData(
    @Req() req: any,
    @Res() res: Response,
    @Query() query: any
  ) {
    const { type } = query
    let name
    if (STANDALONE_ADDITIONAL.includes(type)) {
      name = ADDITIONAL_DEFAULT_NAME
    }
    let result = await this.additionalDataRepository.list(name, type)
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('/check_session')
  async checkSession(@Req() req: any, @Res() res: Response) {
    // const ipAddress =
    //   req.headers['x-forwarded-for'] || req.socket.remoteAddress || null
    const result = await this.sessionService.verifySession(req.user)
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('bot/list')
  @UserPermission(WITHOUT_AUTHORIZATION)
  async listBotSetting(@Res() res: Response) {
    const result = await this.botSettingService.list({})
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('bot/:id')
  @UserPermission(WITHOUT_AUTHORIZATION)
  async getBotSetting(@Param('id') id: string, @Res() res: Response) {
    const result = await this.botSettingService.get({ id })
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('event-stores')
  @UserPermission(WITHOUT_AUTHORIZATION)
  async getEventStores(@Res() res: Response, @Query() query: any) {
    const result = await this.eventStoreRepository.findEventStore(query)
    resSuccess({
      payload: result,
      res,
    })
  }
}
