import {
  Controller,
  Get,
  UseGuards,
  Res,
  Inject,
  Req,
  Param,
  HttpStatus,
  Put,
  Body,
  Query,
  Post,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiResponse,
  ApiSecurity,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger'
import { Response } from 'express'
import { ERROR_CODE, MERCHANT_STATUS, WITHOUT_AUTHORIZATION } from 'src/const'
import { APP_SETTING } from 'src/const/app-setting'
import { Brokers, BrokerServers } from 'src/const/bot'
import { ORDER_CATEGORY } from 'src/const/transaction'
import { BotTradingRepository } from 'src/repositories/bot-trading.repository'
import { MerchantRepository } from 'src/repositories/merchant.repository'
import { AppSettingService } from 'src/services/app-setting'
import { ConnectBrokerServerHandler } from 'src/usecases/bot/connect-broker'
import { ConnectBrokerServerInput } from 'src/usecases/bot/connect-broker/validate'
import { LogoutBrokerServerHandler } from 'src/usecases/bot/logout-broker'
import { LogoutBrokerServerInput } from 'src/usecases/bot/logout-broker/validate'
import { SelectAndConnectBrokerServerHandler } from 'src/usecases/bot/select-and-connect-broker'
import { UserGetBotTradingHandler } from 'src/usecases/bot/user-get-bot-trading'
import { UserListBotTradingHandler } from 'src/usecases/bot/user-list-bot-trading'
import { UserListBotTradingHistoryHandler } from 'src/usecases/bot/user-list-bot-trading-history'
import {
  UserListTradeHistoryInput,
  UserListTradeHistoryPayloadOutput,
} from 'src/usecases/bot/user-list-bot-trading-history/validate'
import { UserListBotTradingPlanHandler } from 'src/usecases/bot/user-list-bot-trading-plan'
import { UserListBotTradingPNLHandler } from 'src/usecases/bot/user-list-bot-trading-pnl'
import { UserListBotTradingPnlPayloadOutput } from 'src/usecases/bot/user-list-bot-trading-pnl/validate'
import { UserListBotTradingOutput } from 'src/usecases/bot/user-list-bot-trading/validate'
import { UserListBotOutput } from 'src/usecases/bot/user-list-bot/validate'
import { UserUpdateBotTradingStatusHandler } from 'src/usecases/bot/user-update-bot-trading-status'
import { UserUpdateBotTradingStatusInput } from 'src/usecases/bot/user-update-bot-trading-status/validate'
import { throwError } from 'src/utils/handle-error.util'

import { resSuccess } from 'src/utils/response.util'
import { BaseApiOutput, UserAuthGuard, UserPermission } from '../../const'
import { DetailBotTradingPayloadDto } from './dto'
import {
  BalanceAndFeeDataInput,
  BalanceAndFeeDataOutput,
} from 'src/usecases/bot/user-balance-and-fee/validate'
import { BalanceAndFeeDataHandler } from 'src/usecases/bot/user-balance-and-fee'
import {
  CreateBalanceAndFeeDataInput,
  CreateBalanceAndFeeDataOutput,
} from 'src/usecases/bot/user-create-balance-and-fee/validate'
import { CreateBalanceAndFeeDataHandler } from 'src/usecases/bot/user-create-balance-and-fee'
import { FileInterceptor } from '@nestjs/platform-express'
import { UploadBrokerProfileHandler } from 'src/usecases/bot/upload-broker-profile'
import { UploadBrokerProfileOutput } from 'src/usecases/bot/upload-broker-profile/validate'

@ApiSecurity('Refresh-Token')
@ApiBearerAuth()
@UseGuards(UserAuthGuard)
@ApiTags('user/bot-trading')
@Controller('bot-trading')
export class UserBotTradingController {
  constructor(
    @Inject(UserListBotTradingHandler)
    private userListBotTradingHandler: UserListBotTradingHandler,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(UserListBotTradingPlanHandler)
    private userListBotTradingPlanHandler: UserListBotTradingPlanHandler,
    @Inject(ConnectBrokerServerHandler)
    private connectBrokerServerHandler: ConnectBrokerServerHandler,
    @Inject(LogoutBrokerServerHandler)
    private logoutBrokerServerHandler: LogoutBrokerServerHandler,
    @Inject(UserUpdateBotTradingStatusHandler)
    private userUpdateBotTradingStatusHandler: UserUpdateBotTradingStatusHandler,
    @Inject(UserListBotTradingHistoryHandler)
    private userListBotTradingHistoryHandler: UserListBotTradingHistoryHandler,
    @Inject(UserListBotTradingPNLHandler)
    private userListBotTradingPNLHandler: UserListBotTradingPNLHandler,
    @Inject(UserGetBotTradingHandler)
    private userGetBotTradingHandler: UserGetBotTradingHandler,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
    @Inject(SelectAndConnectBrokerServerHandler)
    private selectAndConnectBrokerServerHandler: SelectAndConnectBrokerServerHandler,
    @Inject(AppSettingService) private appSettingService: AppSettingService,
    @Inject(BalanceAndFeeDataHandler)
    private balanceAndFeeDataHandler: BalanceAndFeeDataHandler,
    @Inject(CreateBalanceAndFeeDataHandler)
    private createBalanceAndFeeDataHandler: CreateBalanceAndFeeDataHandler,
    @Inject(UploadBrokerProfileHandler)
    private uploadBrokerProfileHandler: UploadBrokerProfileHandler
  ) {}

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class UserListBotTradingOutputMap extends IntersectionType(
      UserListBotTradingOutput,
      BaseApiOutput
    ) {},
    description: 'List bot trading for user',
  })
  async listBot(@Res() res: Response, @Req() req: any) {
    const { m_affiliate } = req.headers
    let merchantId
    if (m_affiliate) {
      const merchant = await this.merchantRepository.findOne({
        code: m_affiliate,
        status: MERCHANT_STATUS.ACTIVE,
      })
      if (merchant) {
        merchantId = merchant.id
      }
    }

    const user_id = req.user?.user_id
    const result = await this.userListBotTradingHandler.execute(
      user_id,
      merchantId
    )
    resSuccess({
      payload: result,
      res,
    })
  }

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('/balance-and-fee')
  @ApiResponse({
    status: 200,
    type: class BalanceAndFeeDataOutputMap extends IntersectionType(
      BalanceAndFeeDataOutput,
      BaseApiOutput
    ) {},
    description: 'Balance And Fee Name Default',
  })
  async BalanceAndFee(@Res() res: Response) {
    const result = await this.balanceAndFeeDataHandler.execute()
    resSuccess({
      payload: result,
      res,
    })
  }

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('/trade-history')
  @ApiResponse({
    status: 200,
    type: class UserListTradeHistoryPayloadOutputMap extends IntersectionType(
      UserListTradeHistoryPayloadOutput,
      BaseApiOutput
    ) {},
    description: 'List bot trading history for user',
  })
  async listBotTradingHistory(
    @Res() res: Response,
    @Req() req: any,
    @Query() query: UserListTradeHistoryInput
  ) {
    const user_id = req.user?.user_id
    const result = await this.userListBotTradingHistoryHandler.execute(
      query,
      user_id
    )
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('/plans')
  @ApiResponse({
    status: 200,
    type: class UserListBotOutputMap extends IntersectionType(
      UserListBotOutput,
      BaseApiOutput
    ) {},
    description: 'List bot trading for user',
  })
  async listBotPlan(
    @Res() res: Response,
    @Req() req: any,
    @Query() query: any
  ) {
    const { status } = query
    const user_id = req.user.user_id
    const result = await this.userListBotTradingPlanHandler.execute(
      user_id,
      status
    )
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('/brokers')
  async listBroker(@Res() res: Response, @Req() req: any) {
    resSuccess({
      payload: Brokers,
      res,
    })
  }

  @Get('/broker-servers')
  async listBrokerServer(@Res() res: Response) {
    const data = await this.appSettingService.getAppSetting({
      name: APP_SETTING.BROKER_SERVER,
    })
    let result = BrokerServers
    if (data.value) {
      try {
        result = JSON.parse(data.value)
      } catch (error) {
        console.log('error', error)
      }
    }
    resSuccess({
      payload: result,
      res,
    })
  }

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class DetailBotTradingPayloadDtoMap extends IntersectionType(
      DetailBotTradingPayloadDto,
      BaseApiOutput
    ) {},
    description: 'Get bot trading detail',
  })
  async getRoleDetail(
    @Param('id') id: string,
    @Res() res: Response,
    @Req() req: any
  ) {
    const user_id = req.user?.user_id
    const result = await this.userGetBotTradingHandler.execute(id, user_id)

    if (result) {
      result['category'] = ORDER_CATEGORY.TBOT
      resSuccess({
        payload: result,
        res,
      })
    } else {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }
  }

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('/:id/pnl')
  @ApiResponse({
    status: 200,
    type: class UserListBotTradingPnlPayloadOutputMap extends IntersectionType(
      UserListBotTradingPnlPayloadOutput,
      BaseApiOutput
    ) {},
    description: 'Get bot trading detail',
  })
  async getBotPNL(
    @Param('id') id: string,
    @Res() res: Response,
    @Req() req: any,
    @Query() query: any
  ) {
    const user_id = req.user?.user_id
    const { strategy_trade } = query
    const result = await this.userListBotTradingPNLHandler.execute(
      user_id,
      id,
      strategy_trade
    )
    resSuccess({
      payload: result,
      res,
    })
  }

  @Put('/connect')
  async connectBroker(
    @Res() res: Response,
    @Req() req: any,
    @Body() body: ConnectBrokerServerInput
  ) {
    const user_id = req.user.user_id
    const merchant_code = req.user.merchant_code
    const result = await this.connectBrokerServerHandler.execute(
      body,
      user_id,
      merchant_code
    )
    resSuccess({
      payload: result,
      res,
    })
  }

  @Put('/select-and-connect')
  async selectAndConnectBroker(
    @Res() res: Response,
    @Req() req: any,
    @Body() body: ConnectBrokerServerInput
  ) {
    const user_id = req.user.user_id
    const merchant_code = req.user.merchant_code
    const result = await this.selectAndConnectBrokerServerHandler.execute(
      body,
      user_id,
      merchant_code
    )
    resSuccess({
      payload: result,
      res,
    })
  }

  @Put('/logout')
  async logoutBroker(
    @Res() res: Response,
    @Req() req: any,
    @Body() body: LogoutBrokerServerInput
  ) {
    const user_id = req.user.user_id
    const result = await this.logoutBrokerServerHandler.execute(body, user_id)
    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('/update-status')
  async updateStatus(
    @Res() res: Response,
    @Req() req: any,
    @Body() body: UserUpdateBotTradingStatusInput
  ) {
    const user_id = req.user.user_id
    const merchant_code = req.user.merchant_code
    const result = await this.userUpdateBotTradingStatusHandler.execute(
      body,
      user_id,
      merchant_code
    )
    resSuccess({
      payload: result,
      res,
    })
  }

  @Post('/balance-and-fee')
  @ApiResponse({
    status: 200,
    type: class UserCreateBalanceAndFeeDataOutputMap extends IntersectionType(
      CreateBalanceAndFeeDataOutput,
      BaseApiOutput
    ) {},
    description: 'User create balance and fee',
  })
  async CreateBalanceAndFee(
    @Res() res: Response,
    @Req() req: any,
    @Body() body: CreateBalanceAndFeeDataInput
  ) {
    const result = await this.createBalanceAndFeeDataHandler.execute(body)
    resSuccess({
      payload: result,
      res,
    })
  }

  @Post('/upload-profile')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        broker_code: { type: 'string' },
        platform: { type: 'string' },
        account_id: { type: 'string' },
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(FileInterceptor('file'))
  @ApiResponse({
    status: 200,
    type: class UploadBrokerProfileOutputMap extends IntersectionType(
      UploadBrokerProfileOutput,
      BaseApiOutput
    ) {},
    description: 'User create balance and fee',
  })
  async uploadFile(
    @UploadedFile() file,
    @Body() body,
    @Res() res: Response,
    @Req() req: any
  ) {
    const { platform, broker_code, account_id } = body
    if (file) {
      const user_id = req.user.user_id
      const merchant_code = req.user.merchant_code
      const result = await this.uploadBrokerProfileHandler.execute(
        broker_code,
        file,
        platform,
        merchant_code,
        account_id,
        user_id
      )
      resSuccess({
        payload: result,
        res,
      })
    } else {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BAD_REQUEST,
      })
    }
  }
}
