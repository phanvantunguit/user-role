import { ApiProperty } from '@nestjs/swagger'
import { IsIn, IsString } from 'class-validator'
import { PAYMENT_METHOD } from 'src/const/transaction'

export class PostTransactionDto {
  @ApiProperty({
    required: true,
    description: 'Payment method',
    example: PAYMENT_METHOD.COIN_PAYMENT,
  })
  @IsString()
  @IsIn(Object.values(PAYMENT_METHOD))
  payment_method: PAYMENT_METHOD

  @ApiProperty({
    required: true,
    description: 'Role id',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  role_id: string

  @ApiProperty({
    required: true,
    description: 'package role id',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  package_id: string

  @ApiProperty({
    required: true,
    description: 'Currency for user payment',
    example: 'LTCT',
  })
  @IsString()
  buy_currency: string
}

export class PostUpgradeFreeTrialDto {
  @ApiProperty({
    required: true,
    description: 'Role id',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  role_id: string
}
