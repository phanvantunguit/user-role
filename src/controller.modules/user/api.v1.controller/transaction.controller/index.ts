import {
  Body,
  Controller,
  Get,
  Headers,
  Inject,
  Param,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common'
import { ApiBearerAuth, ApiSecurity, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { WITHOUT_AUTHORIZATION } from 'src/const'
import { TransactionService } from 'src/services/transaction'
import { resSuccess } from 'src/utils/response.util'
import { UserAuthGuard, UserPermission } from '../../const'
import { PostTransactionDto, PostUpgradeFreeTrialDto } from './dto'
import { PAYMENT_METHOD } from 'src/const/transaction'

@ApiSecurity('Refresh-Token')
@ApiBearerAuth()
@UseGuards(UserAuthGuard)
@ApiTags('user/transaction')
@Controller('transaction')
export class TransactionController {
  constructor(
    @Inject(TransactionService) private transactionService: TransactionService
  ) {}

  @Post('')
  async createTransaction(
    @Req() req: any,
    @Body() body: PostTransactionDto,
    @Res() res: Response
  ) {
    const user_id = req.user.user_id
    const email = req.user.email
    const transaction =
      await this.transactionService.checkCreateTransactionValid(
        user_id,
        body.role_id,
        body.package_id,
        body.buy_currency
      )
    if (transaction) {
      transaction['sell_amount'] = Number(transaction.sell_amount.toFixed(2))
      resSuccess({
        payload: transaction,
        res,
      })
    } else {
      const result = await this.transactionService.createTransaction({
        ...body,
        user_id,
        buyer_email: email,
      })
      result['address'] = result.wallet_address
      result['sell_amount'] = Number(result.sell_amount.toFixed(2))
      resSuccess({
        payload: result,
        res,
      })
    }
  }

  @Post('/upgrade-trial')
  async upgradeFreeTrial(
    @Req() req: any,
    @Body() body: PostUpgradeFreeTrialDto,
    @Res() res: Response
  ) {
    const user_id = req.user.user_id
    const userRole = await this.transactionService.upgradeFreeTrial(
      user_id,
      body.role_id
    )
    resSuccess({
      payload: userRole,
      res,
    })
  }

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Post('ipn/coinpayment')
  async ipnTransaction(
    @Headers() headers: any,
    @Body() body: any,
    @Res() res: Response
  ) {
    // const bodyString = formatRequestDataToString(body)
    // const checksum = createChecksumSHA512HMAC(
    //   bodyString,
    //   APP_CONFIG.SECRET_KEY,
    //   'hex'
    // )
    // const hmac = headers.HMAC || headers.hmac
    // if (checksum !== hmac) {
    //   throwError({
    //     status: HttpStatus.FORBIDDEN,
    //     ...ERROR_CODE.SIGNATURE_INVALID,
    //   })
    // }
    await this.transactionService.ipnTransaction(
      PAYMENT_METHOD.COIN_PAYMENT,
      body
    )
    resSuccess({
      payload: 'ok',
      res,
    })
  }

  @Get('/list')
  async listTransaction(@Req() req: any, @Res() res: Response) {
    const user_id = req.user.user_id
    const data = await this.transactionService.listTransaction({ user_id })
    resSuccess({
      payload: data,
      res,
    })
  }
  @Get('/:id')
  async checkTransaction(@Param('id') id: string, @Res() res: Response) {
    const data = await this.transactionService.getTransaction({ id })
    data['sell_amount'] = Number(data.sell_amount.toFixed(2))
    resSuccess({
      payload: data,
      res,
    })
  }
}
