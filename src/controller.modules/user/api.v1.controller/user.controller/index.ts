import {
  Body,
  Controller,
  Post,
  Res,
  Get,
  Param,
  Query,
  Put,
  Req,
  UseGuards,
  HttpStatus,
  UseInterceptors,
  UploadedFile,
  Inject,
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiResponse,
  ApiSecurity,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger'
import { Response } from 'express'
import { APP_CONFIG } from 'src/config'
import {
  ERROR_CODE,
  MERCHANT_STATUS,
  USER_TYPE,
  WITHOUT_AUTHORIZATION,
} from 'src/const'
import { APP_SETTING, ON_OFF_REGISTER } from 'src/const/app-setting'
import { AppSettingService } from 'src/services/app-setting'
import { BotSettingService } from 'src/services/bot-setting'
import { UserService } from 'src/services/user'
import { throwError } from 'src/utils/handle-error.util'
import { resSuccess } from 'src/utils/response.util'
import { UserPermission, UserAuthGuard, BaseApiOutput } from '../../const'
import {
  GetBotSettingDto,
  PostLoginDto,
  PostRegisterDto,
  PostResendEmailVerifyDto,
  PostResetPasswordDto,
  PostSendEmailForgotPasswordDto,
  PutCheckPasswordDto,
  PutUserProfileDto,
  PostChangeEmailDto,
  PostEmailConfirmDto,
} from './dto'
import { FileInterceptor } from '@nestjs/platform-express'
import { StorageResource } from 'src/resources/storage'
import { ResolutionService } from 'src/services/resolution'
import { CurrencyService } from 'src/services/currency'
import { SettingRepository } from 'src/repositories/setting.repository'
import { ListAssetLogHandler } from 'src/usecases/transaction/list-asset-log'
import {
  ListAssetLogInput,
  ListAssetLogPayloadOutput,
} from 'src/usecases/transaction/list-asset-log/validate'
import { PostAppSettingDto } from 'src/controller.modules/admin/api.v1.controller/setting.controller/dto'
import { MerchantRepository } from 'src/repositories/merchant.repository'

@ApiSecurity('Refresh-Token')
@ApiBearerAuth()
@UseGuards(UserAuthGuard)
@ApiTags('user')
@Controller('')
export class UserController {
  constructor(
    private userService: UserService,
    private appSettingService: AppSettingService,
    private botSettingService: BotSettingService,
    private storageResource: StorageResource,
    private resolutionService: ResolutionService,
    private currencyService: CurrencyService,
    @Inject(SettingRepository) private settingRepo: SettingRepository,
    @Inject(ListAssetLogHandler)
    private listAssetLogHandler: ListAssetLogHandler,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository
  ) {}

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Post('register')
  async register(
    @Body() body: PostRegisterDto,
    @Res() res: Response,
    @Req() req: any
  ) {
    const { m_affiliate } = req.headers
    const { email, first_name, last_name, password } = body
    const settingRegister = await this.settingRepo.findAppSetting({
      name: APP_SETTING.ON_OFF_REGISTER,
    })
    if (
      settingRegister.length === 0 ||
      settingRegister[0].value === ON_OFF_REGISTER.ON
    ) {
      const result = await this.userService.registerUser({
        email,
        first_name,
        last_name,
        password,
        merchant_code: m_affiliate,
      })
      resSuccess({
        payload: result,
        res,
      })
    } else {
      throwError({
        status: HttpStatus.FORBIDDEN,
        ...ERROR_CODE.NO_PERMISSION,
      })
    }
  }
  @UserPermission(WITHOUT_AUTHORIZATION)
  @Post('resend-email-verify')
  async resend(
    @Body() body: PostResendEmailVerifyDto,
    @Res() res: Response,
    @Req() req: any
  ) {
    const { m_affiliate } = req.headers
    const result = await this.userService.resendEmailVerify({
      email: body.email,
      merchant_code: m_affiliate || APP_SETTING.MERCHANT_CODE_DEFAULT,
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @UserPermission(WITHOUT_AUTHORIZATION)
  @Post('login')
  async login(
    @Req() req: any,
    @Body() body: PostLoginDto,
    @Res() res: Response
  ) {
    const { m_affiliate } = req.headers
    const { email, password } = body
    const userAgent = req.get('user-agent')
    const ipAddress =
      req.headers['x-forwarded-for'] || req.socket.remoteAddress || null
    const result = await this.userService.loginUser(
      {
        email,
        password,
        user_type: USER_TYPE.USER,
      },
      userAgent,
      ipAddress,
      m_affiliate
    )
    resSuccess({
      payload: result,
      res,
    })
  }
  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('verify-email/:token')
  async verifyEmail(@Param('token') token: string, @Res() res: Response) {
    try {
      const result = await this.userService.verifyEmailUser(token)
      let urlReplace = ''
      if (result.is_admin) {
        urlReplace = `${APP_CONFIG.DASHBOARD_ADMIN_BASE_URL}/login`
      } else {
        // to do get domain
        const merchant = await this.merchantRepository.findOne({
          code: result.merchant_code,
        })
        urlReplace = merchant
          ? `${merchant.domain}/login`
          : `${APP_CONFIG.DASHBOARD_CLIENT_BASE_URL}/login`
      }
      res.redirect(urlReplace)
    } catch (error) {
      let message = 'Verify email failed'
      if (error && error.message) {
        message = error.message
      }
      const htmlFailed = `
      <div class="heading"
      style="background: #942626;display:flex; align-items: center;justify-content: space-between;margin-left:-20px;margin-right: -20px;box-shadow: 0 5px 10px -5px green;padding-left: 30px;padding-right:30px;">
      <h1 style="margin: auto; line-height: 100px;padding-left: 10px;color:white">${message}</h1>
  </div>
      `
      res.send(htmlFailed)
    }
  }

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Post('confirm-email')
  async userConfirmEmail(
    @Body() body: PostEmailConfirmDto,
    @Res() res: Response
  ) {
    const result = await this.userService.userConfirmEmail(body.token)
    resSuccess({
      payload: result,
      res,
    })
  }

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Post('forgot-password')
  async forgotPassword(
    @Body() body: PostSendEmailForgotPasswordDto,
    @Res() res: Response,
    @Req() req: any
  ) {
    const { m_affiliate } = req.headers
    const result = await this.userService.sendEmailForgotPassword({
      email: body.email,
      merchant_code: m_affiliate || APP_SETTING.MERCHANT_CODE_DEFAULT
    })
    resSuccess({
      payload: result,
      res,
    })
  }
  @UserPermission(WITHOUT_AUTHORIZATION)
  @Post('reset-password')
  async resetPassword(
    @Body() body: PostResetPasswordDto,
    @Res() res: Response
  ) {
    const result = await this.userService.resetPassword(body)
    resSuccess({
      payload: result,
      res,
    })
  }
  @Put('/profile')
  async updateUserProfile(
    @Body() body: PutUserProfileDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const user_id = req.user.user_id
    const dataUpdate = { ...body, id: user_id }
    const result = await this.userService.updateInfo(dataUpdate, false)
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('/profile')
  async getUserProfile(@Req() req: any, @Res() res: Response) {
    const { m_affiliate } = req.headers
    const user_id = req.user.user_id
    const result = await this.userService.getProfile(
      {
        id: user_id,
      },
      m_affiliate
    )
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('/check-username/:username')
  async checkUsername(
    @Req() req: any,
    @Param('username') username: string,
    @Res() res: Response
  ) {
    const user_id = req.user.user_id
    const result = await this.userService.checkUsername(username, user_id)
    if (result) {
      res.status(HttpStatus.OK).send({
        error_code: ERROR_CODE.SUCCESS.error_code,
        message: 'Username is valid',
      })
    } else {
      res.status(HttpStatus.OK).send({
        ...ERROR_CODE.USERNAME_EXISTED,
      })
    }
  }
  @Put('app-setting')
  async createAppSetting(
    @Body() body: PostAppSettingDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const user_id = req.user.user_id
    const result = await this.appSettingService.userCreateAppSetting({
      ...body,
      owner_created: user_id,
    })

    resSuccess({
      payload: result,
      res,
    })
  }
  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('app-setting/list')
  async listAppSetting(@Res() res: Response, @Req() req: any) {
    const user_id = req.user?.user_id
    const result = await this.appSettingService.listAppSetting({ user_id })
    resSuccess({
      payload: result,
      res,
    })
  }
  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('app-setting/:name')
  async getAppSetting(
    @Param('name') name: string,
    @Res() res: Response,
    @Req() req: any
  ) {
    const user_id = req.user?.user_id
    const result = await this.appSettingService.getAppSetting({ name, user_id })
    resSuccess({
      payload: result,
      res,
    })
  }
  @Get('bot-setting/list')
  async listBotSetting(@Res() res: Response, @Query() query: GetBotSettingDto) {
    const result = await this.botSettingService.list({ ...query, status: true })
    resSuccess({
      payload: result,
      res,
    })
  }
  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('bot-setting/:id')
  async getBotSetting(@Param('id') id: string, @Res() res: Response) {
    const result = await this.botSettingService.get({ id })
    resSuccess({
      payload: result,
      res,
    })
  }
  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('resolution/list')
  async getResolution(@Res() res: Response) {
    const result = await this.resolutionService.listResolution()
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('asset/list')
  @ApiResponse({
    status: 200,
    type: class ListAssetLogPayloadOutputMap extends IntersectionType(
      ListAssetLogPayloadOutput,
      BaseApiOutput
    ) {},
    description: 'List asset for user',
  })
  async getAssetList(
    @Res() res: Response,
    @Req() req: any,
    @Query() query: ListAssetLogInput
  ) {
    const user_id = req.user.user_id
    const data = await this.listAssetLogHandler.execute(query, true, user_id)
    resSuccess({
      payload: data,
      res,
    })
  }

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('currency/list')
  async getPaymentMethod(@Res() res: Response, @Query() query: any) {
    const { all, category } = query
    let data
    if (all === true || all === 'true') {
      data = await this.currencyService.list({})
    } else {
      data = await this.currencyService.findCurrencies({category})
    }
    const result = data.map((c) => {
      return {
        ...c,
        type: c.currency,
      }
    })
    resSuccess({
      payload: result,
      res,
    })
  }

  @Post('change-email')
  async changeEmail(
    @Req() req: any,
    @Body() body: PostChangeEmailDto,
    @Res() res: Response
  ) {
    const user_id = req.user.user_id
    const result = await this.userService.changeEmail({ ...body, user_id })
    resSuccess({
      payload: result,
      res,
    })
  }
  // @Put('change-email')
  // async verifyCodeEmail(
  //   @Req() req: any,
  //   @Body() body: PutChangeEmailDto,
  //   @Res() res: Response
  // ) {
  //   const user_id = req.user.user_id
  //   const result = await this.userService.changeEmailVerifyCode({
  //     code: body.code,
  //     user_id,
  //   })
  //   if (result) {
  //     res.status(HttpStatus.OK).send({
  //       error_code: ERROR_CODE.SUCCESS.error_code,
  //       message: 'Change email successfully',
  //     })
  //   } else {
  //     res.status(HttpStatus.OK).send({
  //       ...ERROR_CODE.VERIFY_CODE_INVALID,
  //     })
  //   }
  // }

  @Post('/upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file, @Res() res: Response) {
    if (file) {
      const file_url = await this.storageResource.upload(file)
      resSuccess({
        payload: { file_url },
        res,
      })
    } else {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BAD_REQUEST,
      })
    }
  }

  @Put('/check-password')
  async checkPassword(
    @Req() req: any,
    @Body() body: PutCheckPasswordDto,
    @Res() res: Response
  ) {
    const user_id = req.user.user_id
    const result = await this.userService.checkPassword(user_id, body.password)
    if (result) {
      res.status(HttpStatus.OK).send({
        error_code: ERROR_CODE.SUCCESS.error_code,
        message: 'Password is correct',
      })
    } else {
      res.status(HttpStatus.OK).send({
        ...ERROR_CODE.PASSWORD_INCORRECT,
      })
    }
  }
}
