import {
  IsString,
  IsEmail,
  Matches,
  IsOptional,
  IsIn,
  ValidateIf,
} from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'
import { GENDER, PasswordRegex } from 'src/const'

export class PostRegisterDto {
  @ApiProperty({
    required: true,
    description: 'Email to register account Coinmap',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string

  @ApiProperty({
    required: true,
    description:
      'Password must be at least 8 characters with upper case letter, lower case letter, and number',
    example: '123456Aa',
  })
  @Matches(new RegExp(PasswordRegex), {
    message:
      'Password must be at least 8 characters with upper case letter, lower case letter, and number',
  })
  password: string

  @ApiProperty({
    required: true,
    description: 'First name',
    example: 'John',
  })
  @IsString()
  first_name: string

  @ApiProperty({
    required: true,
    description: 'Last name',
    example: 'Nguyen',
  })
  @IsString()
  last_name: string
}

export class PostLoginDto {
  @ApiProperty({
    required: true,
    description: 'Email to login Coinmap',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string

  @ApiProperty({
    required: true,
    description:
      'password must be at least 8 characters with upper case letter, lower case letter, and number',
    example: '123456Aa',
  })
  @Matches(new RegExp(PasswordRegex), {
    message:
      'Password must be at least 8 characters with upper case letter, lower case letter, and number',
  })
  password: string
}

export class PostResendEmailVerifyDto {
  @ApiProperty({
    required: true,
    description: 'Email to login Coinmap',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string
}
export class PostChangeEmailDto {
  @ApiProperty({
    required: true,
    description: 'Email to login Coinmap',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string

  @ApiProperty({
    required: true,
    description: 'Url return',
    example: 'http://localhost:3000/confirm-change-email',
  })
  @IsString()
  callback_url: string
}
export class PostEmailConfirmDto {
  @ApiProperty({
    required: true,
    description: 'Token to verify',
    example: '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d',
  })
  @IsString()
  token: string
}

export class PostSendEmailForgotPasswordDto {
  @ApiProperty({
    required: true,
    description: 'Email to reset password',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string
}

export class PostResetPasswordDto {
  @ApiProperty({
    required: true,
    description: 'Token to reset password',
    example: '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d',
  })
  @IsString()
  token: string

  @ApiProperty({
    required: true,
    description:
      'password must be at least 8 characters with upper case letter, lower case letter, and number',
    example: '123456Aa',
  })
  @Matches(PasswordRegex)
  password: string
}

export class PutUserProfileDto {
  @ApiProperty({
    required: false,
    description: 'Phone number',
    example: '0987654321',
  })
  @IsOptional()
  @IsString()
  phone: string

  @ApiProperty({
    required: false,
    description: 'First name',
    example: 'John',
  })
  @IsOptional()
  @IsString()
  first_name: string

  @ApiProperty({
    required: false,
    description: 'Last name',
    example: 'Nguyen',
  })
  @IsOptional()
  @IsString()
  last_name: string

  @ApiProperty({
    required: false,
    description: 'Full address',
    example: '123 To Hien Thanh, phuong 10, quan 10, tp HCM',
  })
  @IsOptional()
  @IsString()
  address: string

  @ApiProperty({
    required: false,
    description: 'Affiliate code',
    example: 'affiliate_code',
  })
  @IsOptional()
  @IsString()
  affiliate_code: string

  @ApiProperty({
    required: false,
    description: 'Link image',
    example: 'https://static-dev.cextrading.io/1660898281635Untitled.png',
  })
  @IsOptional()
  @IsString()
  profile_pic: string

  @ApiProperty({
    required: false,
    description: 'Link affiliate',
    example: '',
  })
  @IsOptional()
  @IsString()
  link_affiliate: string

  @ApiProperty({
    required: false,
    description: 'Referral_code',
    example: '',
  })
  @IsOptional()
  @IsString()
  referral_code: string

  @ApiProperty({
    required: false,
    description: 'Note updated',
    example: '',
  })
  @IsOptional()
  @IsString()
  note_updated: string

  @ApiProperty({
    required: false,
    description:
      'Password must be at least 8 characters with upper case letter, lower case letter, and number',
    example: '123456Aa',
  })
  @IsOptional()
  @Matches(new RegExp(PasswordRegex), {
    message:
      'Password must be at least 8 characters with upper case letter, lower case letter, and number',
  })
  password: string
  @ApiProperty({
    required: false,
    description:
      'Password must be at least 8 characters with upper case letter, lower case letter, and number',
    example: '123456Aa',
  })
  @ValidateIf((o) => o.password)
  @Matches(new RegExp(PasswordRegex), {
    message:
      'Old password must be at least 8 characters with upper case letter, lower case letter, and number',
  })
  old_password: string

  @ApiProperty({
    required: false,
    description: 'Username is unique',
    example: 'John',
  })
  @IsOptional()
  @IsString()
  username: string

  @ApiProperty({
    required: false,
    description: 'Country',
    example: 'Viet Nam',
  })
  @IsOptional()
  @IsString()
  country: string

  @ApiProperty({
    required: false,
    description: 'gender: male, female or other',
    example: 'male',
  })
  @IsOptional()
  @IsString()
  @IsIn(Object.values(GENDER))
  gender: string

  @ApiProperty({
    required: false,
    description: 'Year of birth',
    example: '2000',
  })
  @IsOptional()
  @IsString()
  year_of_birth: string

  @ApiProperty({
    required: false,
    description: 'Phone code',
    example: '+84',
  })
  @IsOptional()
  @IsString()
  phone_code: string
}

export class GetBotSettingDto {
  @ApiProperty({
    required: false,
    example: 'Violency Pinbar',
  })
  @IsOptional()
  @IsString()
  name: string
}
export class PutCheckPasswordDto {
  @ApiProperty({
    required: true,
    description: 'Check password',
    example: '143a4vAAA',
  })
  @IsString()
  password: string
}
function DoesMatch() {
  throw new Error('Function not implemented.')
}
