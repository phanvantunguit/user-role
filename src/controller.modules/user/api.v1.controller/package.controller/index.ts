import {
  Param,
  Controller,
  Get,
  UseGuards,
  Res,
  Inject,
  Req,
  Query,
} from '@nestjs/common'
import { ApiBearerAuth, ApiSecurity, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { WITHOUT_AUTHORIZATION } from 'src/const'
import { MERCHANT_TYPE } from 'src/const/authorization'
import { MERCHANT_ADDITIONAL_DATA_STATUS } from 'src/domains/merchant/types'
import { AdditionalDataRepository } from 'src/repositories/additional-data.repository'
import { MerchantAdditionalDataRepository } from 'src/repositories/merchant-additional-data.repository'
import { MerchantRepository } from 'src/repositories/merchant.repository'
import { PackageService } from 'src/services/package'

import { resSuccess } from 'src/utils/response.util'
import { UserAuthGuard, UserPermission } from '../../const'

@ApiSecurity('Refresh-Token')
@ApiBearerAuth()
@UseGuards(UserAuthGuard)
@ApiTags('user/package')
@Controller('package')
export class UserPackageController {
  constructor(
    @Inject(MerchantAdditionalDataRepository)
    private merchantAdditionalDataRepository: MerchantAdditionalDataRepository,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
    @Inject(AdditionalDataRepository)
    private additionalDataRepository: AdditionalDataRepository,
    @Inject(PackageService) private packageService: PackageService
  ) {}

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('/list')
  async listRole(@Req() req: any, @Res() res: Response, @Query() query: any) {
    const { m_affiliate } = req.headers
    let result = []
    // check MERCHANT_TYPE === OTHERS
    if (m_affiliate) {
      const merchant = await this.merchantRepository.findOne({
        code: m_affiliate,
      })
      if (merchant && merchant.config.domain_type === MERCHANT_TYPE.OTHERS) {
        if (query.type) {
          const additional = await this.merchantAdditionalDataRepository.list({
            merchant_id: merchant.id,
            type: query.type,
            status: MERCHANT_ADDITIONAL_DATA_STATUS.ON,
          })
          result = additional
        }
        return resSuccess({
          payload: result,
          res,
        })
      }
    }
    // check MERCHANT_TYPE === COINMAP
    result = await this.packageService.list({ status: true })
    resSuccess({
      payload: result,
      res,
    })
  }

  @UserPermission(WITHOUT_AUTHORIZATION)
  @Get('/:id')
  async getRoleDetail(@Param('id') id: string, @Res() res: Response) {
    const defaultPackage = await this.packageService.get({ id })
    if (defaultPackage) {
      resSuccess({
        payload: defaultPackage,
        res,
      })
    } else {
      const result = await this.additionalDataRepository.findById(id)
      resSuccess({
        payload: result,
        res,
      })
    }
  }
}
