import { IsString, IsOptional } from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'

export class QueryUserRoleDto {
  @ApiProperty({
    required: false,
    description: 'Package time id',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsOptional()
  @IsString()
  package_id: string
}
