import {
  Param,
  Controller,
  Get,
  UseGuards,
  Res,
  Req,
  Query,
  HttpStatus,
} from '@nestjs/common'
import { ApiBearerAuth, ApiSecurity, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { ERROR_CODE } from 'src/const'
import { ORDER_CATEGORY } from 'src/const/transaction'
import { RoleService } from 'src/services/role'
import { throwError } from 'src/utils/handle-error.util'

import { resSuccess } from 'src/utils/response.util'
import { UserAuthGuard } from '../../const'
import { QueryUserRoleDto } from './dto'

@ApiSecurity('Refresh-Token')
@ApiBearerAuth()
@UseGuards(UserAuthGuard)
@ApiTags('user/role')
@Controller('role')
export class UserRoleController {
  constructor(private roleService: RoleService) {}

  @Get('/list')
  async listRole(
    @Query() query: QueryUserRoleDto,
    @Req() req: any,
    @Res() res: Response
  ) {
    const user_id = req.user.user_id
    const result = await this.roleService.listRoleForUser(
      user_id,
      query.package_id
    )
    resSuccess({
      payload: result,
      res,
    })
  }

  @Get('/:id')
  async getRoleDetail(@Param('id') id: string, @Res() res: Response) {
    const result = await this.roleService.getRoleDetail(id)
    if (result) {
      result['category'] = ORDER_CATEGORY.PKG
      resSuccess({
        payload: result,
        res,
      })
    } else {
      throwError({
        status: HttpStatus.BAD_REQUEST,
        ...ERROR_CODE.BOT_NOT_FOUND,
      })
    }
  }
}
