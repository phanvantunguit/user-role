import * as nodemailer from 'nodemailer'
import { ISendEmail } from './types'
const option = {
  service: 'gmail',
  auth: {
    user: 'sender.coinmap@gmail.com', // email hoặc username
    pass: 'Sender@coinmap', // password
  },
}
export class NodeMailerTransport {
  transporter

  connect() {
    this.transporter = nodemailer.createTransport(option)
  }

  async nodemailerSendEmail(params: ISendEmail): Promise<boolean> {
    return new Promise((resolve) => {
      const _this = this
      if (!this.transporter) {
        this.connect()
      }
      this.transporter.verify(function (error) {
        if (error) {
          throw error
        } else {
          const mail = {
            from: option.auth.user,
            to: params.to,
            subject: params.subject,
            html: params.html,
          }
          _this.transporter.sendMail(mail, function (error, info) {
            if (error) {
              console.log(error)
              return resolve(false)
            } else {
              console.log('Email sent: ' + info.response)
              return resolve(true)
            }
          })
        }
      })
    })
  }
}
