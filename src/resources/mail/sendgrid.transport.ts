import { ISendEmail } from './types'
import { requestPOST } from 'src/utils/http-transport.util'
import { APP_CONFIG } from 'src/config'
export class SendGridTransport {
  async sendGridSendEmail(
    params: ISendEmail,
    email?: string,
    name?: string,
    ccEmail?: string
  ): Promise<any> {
    const data = {
      from: {
        email: email || APP_CONFIG.SENDGRID_SENDER_EMAIL,
        name: name || APP_CONFIG.SENDGRID_SENDER_NAME,
      },
      personalizations: [],
      subject: params.subject,
      content: [
        {
          type: 'text/html',
          value: params.html,
        },
      ],
    }
    const personalization = {
      to: [
        {
          email: params.to,
        },
      ],
    }
    if (ccEmail) {
      personalization['cc'] = [
        {
          email: ccEmail
        },
      ]
    }
    data.personalizations.push(personalization)
    const url = `${APP_CONFIG.SENDGRID_BASE_URL}/v3/mail/send`
    const headers = {
      Authorization: `Bearer ${APP_CONFIG.SENDGRID_API_TOKEN}`,
      'Content-Type': 'application/json',
    }
    try {
      console.log('SendGrid request ccEmail:', ccEmail)
      console.log('SendGrid request personalization:', personalization)
      const result = await requestPOST(url, data, headers)
      console.log('SendGrid result:', result)
      return true
    } catch (error) {
      console.log('SendGrid ERROR', error)
      return false
    }
  }
}
