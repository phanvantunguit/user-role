import { Inject } from '@nestjs/common'
import * as handlebars from 'handlebars'
import { APP_CONFIG } from 'src/config'
import { SendGridTransport } from './sendgrid.transport'
import { emailForgotPassword } from 'src/templates/email-forgot-password'
import { emailVerify } from 'src/templates/email-verify'
import { emailVerifyAccountCreate } from 'src/templates/email-verify-account-create'
import { emailPaymentInfo } from 'src/templates/email-payment-info'
// import { emailPaymentWarning } from 'src/templates/email-payment-warning'
import { emailChange } from 'src/templates/email-change'
// import { emailPaymentChild } from 'src/templates/email-payment-child'
import { emailCommon } from 'src/templates/email-common'
import {
  IEmailPaymentInfo,
  IEmailPaymentRefund,
  IEmailPaymentWarning,
  IEmailUpgradeSuccess,
} from './types'
import { EMAIL_SUBJECT } from 'src/const/email'
// import { emailUpgradeSuccess } from 'src/templates/email-upgrade-success'

export class MailResource {
  constructor(
    @Inject(SendGridTransport) private sendGridTransport: SendGridTransport
  ) {}
  async sendEmailConfirmAccount(
    email: string,
    token: string,
    from_email?: string,
    from_name?: string,
    logo_url?: string,
    header_url?: string,
    main_content?: string,
    twitter_url?: string,
    youtube_url?: string,
    facebook_url?: string,
    telegram_url?: string,
    company_name?: string,
    fullname?: string
  ): Promise<any> {
    const logoUrl = logo_url || 'https://static.cextrading.io/images/1/logo.png'
    const headerUrl =
      header_url || 'https://static.cextrading.io/images/1/header-bg.png'
    let footerContent
    if (company_name) {
      footerContent = `
        ${company_name.toUpperCase()} is an organization providing trading-related services for professional investors.
        \n 
        ${company_name.toUpperCase()} prioritizes the benefit of high-end technology applied to Trading; we are committed to being top in providing trading-related services and helping our investors optimize profits.
        `
    } else {
      footerContent =
        'Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.'
    }

    const companyName = company_name || 'Coinmap'
    const companyNameUpperCase = company_name || 'COINMAP'
    const mainContent =
      main_content ||
      `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our CEX and DEX Trading applications will help you trade safety and in comfort.`
    const twitterUrl = twitter_url || 'https://twitter.com/CoinmapTrading'
    const facebookUrl =
      facebook_url || 'https://www.facebook.com/CoinmapTrading'
    const youtubeUrl = youtube_url || 'https://www.youtube.com/c/8xTRADING'
    const telegramUrl = telegram_url || 'https://t.me/trading8x'

    const template = handlebars.compile(emailVerify)
    const replacements = {
      linkVerify: `${APP_CONFIG.BASE_URL}/api/v1/user/verify-email/${token}`,
      logoUrl,
      headerUrl,
      companyName,
      companyNameUpperCase: companyNameUpperCase.toUpperCase(),
      mainContent,
      footerContent,
      twitterUrl,
      facebookUrl,
      youtubeUrl,
      telegramUrl,
      fullname,
    }
    const html = template(replacements)
    const dataSendEmail = {
      to: email,
      subject: EMAIL_SUBJECT.verify_email,
      html,
    }
    return this.sendGridTransport.sendGridSendEmail(
      dataSendEmail,
      from_email,
      from_name,
    )
  }
  async sendEmailForgotPassword(
    email: string,
    token: string,
    expires_at: number,
    resetPasswordLink?: string,
    from_email?: string,
    from_name?: string,
    logo_url?: string,
    header_url?: string,
    twitter_url?: string,
    youtube_url?: string,
    facebook_url?: string,
    telegram_url?: string,
    company_name?: string,
    fullname?: string
  ): Promise<any> {
    const logoUrl = logo_url || 'https://static.cextrading.io/images/1/logo.png'
    const headerUrl =
      header_url || 'https://static.cextrading.io/images/1/header-bg.png'
    const companyNameUpperCase = company_name || 'COINMAP'
    let footerContent
    if (company_name) {
      footerContent = `
        ${company_name.toUpperCase()} is an organization providing trading-related services for professional investors.\n
        ${company_name.toUpperCase()} prioritizes the benefit of high-end technology applied to Trading; we are committed to being top in providing trading-related services and helping our investors optimize profits.
        `
    } else {
      footerContent =
        'Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.'
    }
    const twitterUrl = twitter_url || 'https://twitter.com/CoinmapTrading'
    const facebookUrl =
      facebook_url || 'https://www.facebook.com/CoinmapTrading'
    const youtubeUrl = youtube_url || 'https://www.youtube.com/c/8xTRADING'
    const telegramUrl = telegram_url || 'https://t.me/trading8x'
    const template = handlebars.compile(emailForgotPassword)
    const replacements = {
      resetPasswordLink: `${
        resetPasswordLink || APP_CONFIG.DASHBOARD_CLIENT_BASE_URL
      }/reset-password?token=${token}&expires_at=${expires_at}`,
      logoUrl,
      headerUrl,
      fromEmail: from_email || 'coinmaptrading@coinmap.tech',
      companyNameUpperCase: companyNameUpperCase.toUpperCase(),
      twitterUrl,
      facebookUrl,
      youtubeUrl,
      telegramUrl,
      footerContent,
      fullname
    }
    const html = template(replacements)
    const dataSendEmail = {
      to: email,
      subject: EMAIL_SUBJECT.reset_password,
      html,
    }
    return this.sendGridTransport.sendGridSendEmail(
      dataSendEmail,
      from_email,
      from_name,
    )
  }
  async sendEmailCreateAccount(
    email: string,
    token: string,
    password: string,
    from_email?: string,
    from_name?: string,
    fullname?: string
  ): Promise<any> {
    const txtPassword = `Your password: ${password}`
    const template = handlebars.compile(emailVerifyAccountCreate)
    const replacements = {
      linkVerify: `${APP_CONFIG.BASE_URL}/api/v1/user/verify-email/${token}`,
      password: txtPassword,
      fullname
    }
    const html = template(replacements)
    const dataSendEmail = {
      to: email,
      subject: EMAIL_SUBJECT.verify_email,
      html,
    }
    return this.sendGridTransport.sendGridSendEmail(
      dataSendEmail,
      from_email,
      from_name
    )
  }
  async sendEmailChange(
    email: string,
    token: string,
    originEmail: string,
    newEmail: string,
    callbackUrl: string,
    from_email?: string,
    from_name?: string,
    logo_url?: string,
    header_url?: string,
    twitter_url?: string,
    youtube_url?: string,
    facebook_url?: string,
    telegram_url?: string,
    company_name?: string,
    fullname?: string
  ): Promise<any> {
    const logoUrl = logo_url || 'https://static.cextrading.io/images/1/logo.png'
    const headerUrl =
      header_url || 'https://static.cextrading.io/images/1/header-bg.png'
    const companyNameUpperCase = company_name || 'COINMAP'
    let footerContent
    if (company_name) {
      footerContent = `
        ${company_name.toUpperCase()} is an organization providing trading-related services for professional investors.
        \n 
        ${company_name.toUpperCase()} prioritizes the benefit of high-end technology applied to Trading; we are committed to being top in providing trading-related services and helping our investors optimize profits.
        `
    } else {
      footerContent =
        'Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.'
    }
    const twitterUrl = twitter_url || 'https://twitter.com/CoinmapTrading'
    const facebookUrl =
      facebook_url || 'https://www.facebook.com/CoinmapTrading'
    const youtubeUrl = youtube_url || 'https://www.youtube.com/c/8xTRADING'
    const telegramUrl = telegram_url || 'https://t.me/trading8x'

    const template = handlebars.compile(emailChange)
    let linkVerify = callbackUrl
    if (callbackUrl.includes('?')) {
      linkVerify += `&token=${token}`
    } else {
      linkVerify += `?token=${token}`
    }
    const replacements = {
      linkVerify,
      originEmail,
      newEmail,
      logoUrl,
      headerUrl,
      companyNameUpperCase: companyNameUpperCase.toUpperCase(),
      footerContent,
      twitterUrl,
      facebookUrl,
      youtubeUrl,
      telegramUrl,
      fullname
    }
    const html = template(replacements)
    const dataSendEmail = {
      to: email,
      subject: EMAIL_SUBJECT.change_email,
      html,
    }
    return this.sendGridTransport.sendGridSendEmail(
      dataSendEmail,
      from_email,
      from_name,
    )
  }
  async sendEmailPaymentInfo(
    parmas: IEmailPaymentInfo,
    from_email?: string,
    from_name?: string
  ): Promise<any> {
    const {
      email,
      role_name,
      package_name,
      amount,
      currency,
      buy_amount,
      buy_currency,
      wallet_address,
      time,
      checkout_url,
      qrcode_url,
      status_url,
      sell_amount_received,
      payment_id,
    } = parmas
    let html
    const template = handlebars.compile(emailPaymentInfo)
    const replacements = {
      role_name,
      package_name,
      buy_amount,
      buy_currency,
      wallet_address,
      time,
      checkout_url,
      qrcode_url,
      status_url,
      payment_id,
    }
    html = template(replacements)

    const dataSendEmail = {
      to: email,
      subject: `${EMAIL_SUBJECT.transaction_information} ${payment_id}`,
      html,
    }
    return this.sendGridTransport.sendGridSendEmail(
      dataSendEmail,
      from_email,
      from_name,
    )
  }
  async sendEmailInactiveBySystem(
    email: string,
    token: string,
    from_email?: string,
    from_name?: string,
    logo_url?: string,
    header_url?: string,
    main_content?: string,
    twitter_url?: string,
    youtube_url?: string,
    facebook_url?: string,
    telegram_url?: string,
    company_name?: string,
    fullname?: string
  ): Promise<any> {
    const logoUrl = logo_url || 'https://static.cextrading.io/images/1/logo.png'
    const headerUrl =
      header_url || 'https://static.cextrading.io/images/1/header-bg.png'
    const companyName = company_name || 'Coinmap'
    const companyNameUpperCase = company_name || 'COINMAP'
    const mainContent =
      main_content ||
      `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our CEX and DEX Trading applications will help you trade safety and in comfort.`
      let footerContent
      if (company_name) {
        footerContent = `
          ${company_name.toUpperCase()} is an organization providing trading-related services for professional investors.
          \n 
          ${company_name.toUpperCase()} prioritizes the benefit of high-end technology applied to Trading; we are committed to being top in providing trading-related services and helping our investors optimize profits.
          `
      } else {
        footerContent =
          'Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.'
      }
    const twitterUrl = twitter_url || 'https://twitter.com/CoinmapTrading'
    const facebookUrl =
      facebook_url || 'https://www.facebook.com/CoinmapTrading'
    const youtubeUrl = youtube_url || 'https://www.youtube.com/c/8xTRADING'
    const telegramUrl = telegram_url || 'https://t.me/trading8x'

    const template = handlebars.compile(emailVerify)
    const replacements = {
      linkVerify: `${APP_CONFIG.BASE_URL}/api/v1/user/verify-email/${token}`,
      logoUrl,
      headerUrl,
      companyName,
      companyNameUpperCase: companyNameUpperCase.toUpperCase(),
      mainContent,
      footerContent,
      twitterUrl,
      facebookUrl,
      youtubeUrl,
      telegramUrl,
      fullname
    }
    const html = template(replacements)
    const dataSendEmail = {
      to: email,
      subject: EMAIL_SUBJECT.verify_email,
      html,
    }
    return this.sendGridTransport.sendGridSendEmail(
      dataSendEmail,
      from_email,
      from_name,
    )
  }
  async sendEmailStandard(
    subject: string,
    title: string,
    email: string,
    main_content: string[],
    from_email?: string,
    from_name?: string,
    logo_url?: string,
    header_url?: string,
    twitter_url?: string,
    youtube_url?: string,
    facebook_url?: string,
    telegram_url?: string,
    company_name?: string,
    fullname?: string,
    ccEmail?: string,
  ): Promise<any> {
    const logoUrl = logo_url || 'https://static.cextrading.io/images/1/logo.png'
    const headerUrl =
      header_url || 'https://static.cextrading.io/images/1/header-bg.png'
    let footerContent
    if (company_name) {
      footerContent = `
      ${company_name.toUpperCase()} is an organization providing trading-related services for professional investors.
      \n 
      ${company_name.toUpperCase()} prioritizes the benefit of high-end technology applied to Trading; we are committed to being top in providing trading-related services and helping our investors optimize profits.
      `
    } else {
      footerContent =
        'Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.'
    }
    const companyName = company_name || 'Coinmap'
    const companyNameUpperCase = company_name || 'COINMAP'
    const twitterUrl = twitter_url || 'https://twitter.com/CoinmapTrading'
    const facebookUrl =
      facebook_url || 'https://www.facebook.com/CoinmapTrading'
    const youtubeUrl = youtube_url || 'https://www.youtube.com/c/8xTRADING'
    const telegramUrl = telegram_url || 'https://t.me/trading8x'

    const template = handlebars.compile(emailCommon)
    const replacements = {
      title,
      logoUrl,
      headerUrl,
      companyName,
      companyNameUpperCase: companyNameUpperCase.toUpperCase(),
      mainContent: main_content,
      footerContent,
      twitterUrl,
      facebookUrl,
      youtubeUrl,
      telegramUrl,
      fullname
    }
    const html = template(replacements)
    const dataSendEmail = {
      to: email,
      subject,
      html,
    }
    return this.sendGridTransport.sendGridSendEmail(
      dataSendEmail,
      from_email,
      from_name,
      ccEmail
    )
  }
}
