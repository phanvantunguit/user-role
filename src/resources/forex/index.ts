import MetaApi, { CopyFactory, ProvisioningProfile } from 'metaapi.cloud-sdk'
import { APP_CONFIG } from 'src/config'
import { BROKER_CODE, TBOT_PLATFORM } from 'src/const/bot'

export class MetaapiResource {
  metaApi: MetaApi
  copyFactory: CopyFactory
  constructor() {
    this.metaApi = new MetaApi(APP_CONFIG.MT_TOKEN)
    this.copyFactory = new CopyFactory(APP_CONFIG.MT_TOKEN)
  }

  async createProvisioningProfile(params: {
    name: string
    version: number
    brokerTimezone: string
    brokerDSTSwitchTimezone: string
    file: any
    merchant_code: string
    account_id: string
    user_id: string
  }): Promise<ProvisioningProfile> {
    const {
      name,
      version,
      brokerTimezone,
      brokerDSTSwitchTimezone,
      file,
      merchant_code,
      account_id,
      user_id,
    } = params
    const provisioningProfile =
      await this.metaApi.provisioningProfileApi.createProvisioningProfile({
        name: `${merchant_code}_${account_id}_${user_id}`,
        version,
        brokerTimezone,
        brokerDSTSwitchTimezone,
      })
    let rename = name
    const type = name.split('.').pop()
    switch (type) {
      case 'ini':
        rename = 'servers.ini'
        break
      case 'srv':
        rename = 'broker.srv'
        break

      default:
        break
    }
    try {
      await provisioningProfile.uploadFile(rename, file)
    } catch (error) {
      await provisioningProfile.remove()
      throw error
    }

    return provisioningProfile
  }

  async removeProvisioningProfile(profileId: string): Promise<void> {
    try {
      const provisioningProfile =
        await this.metaApi.provisioningProfileApi.getProvisioningProfile(
          profileId
        )
      await provisioningProfile.remove()
    } catch (error) {
      console.log('removeProvisioningProfile error', error)
    }
  }

  async createSubscriber(params: {
    login: string
    password: string
    name: string
    server: string
    platform: TBOT_PLATFORM
    profile_id?: string
  }) {
    const { login, password, name, server, platform, profile_id } = params
    // const subscriber = {
    //   id: 'b158d9f5-046c-4fac-a301-10ebc8ba518e',
    // }
    // return subscriber
    const account = await this.metaApi.metatraderAccountApi.createAccount({
      login,
      password,
      name,
      server,
      magic: 0,
      region: 'singapore',
      platform,
      copyFactoryRoles: ['SUBSCRIBER'],
      baseCurrency: 'USD',
      riskManagementApiEnabled: true,
      provisioningProfileId: profile_id,
    })
    return account
  }
  async getAccount(params: { account_id: string }) {
    try {
      const { account_id } = params
      const account = await this.metaApi.metatraderAccountApi.getAccount(
        account_id
      )
      return account
    } catch (error) {
      console.log('getAccount error', error)
      if (error.status === 404) {
        return false
      }
      throw error
    }
  }
  async updateSubscriber(params: {
    subscriber_id: string
    name: string
    strategy_id: string
    multiplier: number
    max_absolute_risk: number
    start_time: Date
    broker: string
  }) {
    const {
      subscriber_id,
      name,
      strategy_id,
      multiplier,
      max_absolute_risk,
      start_time,
      broker,
    } = params
    let symbolMapping
    switch (broker) {
      case BROKER_CODE.xm:
        symbolMapping = [
          {
            from: 'XAUUSD',
            to: 'GOLD',
          },
        ]
        break
    }
    const subscriptionUpdate =
      await this.copyFactory.configurationApi.updateSubscriber(subscriber_id, {
        name,
        subscriptions: [
          {
            strategyId: strategy_id,
            multiplier,
            riskLimits: [
              {
                type: 'lifetime',
                applyTo: 'balance-minus-equity',
                maxAbsoluteRisk: max_absolute_risk,
                closePositions: true,
                startTime: start_time,
              },
            ],
            // symbolMapping: symbolMapping, update later
          },
        ],
      })
    return subscriptionUpdate
  }
  async removeSubscriber(params: { subscriber_id: string }) {
    const { subscriber_id } = params
    const subscription = await this.copyFactory.configurationApi.getSubscriber(
      subscriber_id
    )
    //@ts-ignore
    if (subscription && !subscription.removed) {
      const subscriptionRemove =
        await this.copyFactory.configurationApi.removeSubscriber(subscriber_id)
      return subscriptionRemove
    }
    return subscription
  }
  async removeSubscrition(params: {
    subscriber_id: string
    strategy_id: string
  }) {
    const { subscriber_id, strategy_id } = params
    try {
      // const { subscriber_id, name, strategy_id } = params
      const subscriptionUpdate =
        await this.copyFactory.configurationApi.removeSubscription(
          subscriber_id,
          strategy_id
        )
      return subscriptionUpdate
    } catch (error) {
      return false
    }
  }
  async removeAccount(params: { subscriber_id: string }) {
    const { subscriber_id } = params
    let account
    try {
      account = await this.metaApi.metatraderAccountApi.getAccount(
        subscriber_id
      )
    } catch (error) {
      console.log('getAccount error', error)
      if (error.status === 404) {
        return true
      }
      throw error
    }
    if (account) {
      await account.remove()
    }
  }
}
