import { APP_CONFIG } from 'src/config'
import { Storage } from '@google-cloud/storage'
import { SERVICE_NAME } from 'src/const/app-setting'

export class GCloudTransport {
  storage = new Storage({
    projectId: APP_CONFIG.GOOGLE_CLOUD_PROJECT_ID,
    keyFilename: APP_CONFIG.GOOGLE_CLOUD_KEYFILE,
  })
  //   bucket = storage.bucket(process.env.GCLOUD_STORAGE_BUCKET)
  bucket = this.storage.bucket(APP_CONFIG.GOOGLE_CLOUD_STORAGE_BUCKET)
  async upload(file): Promise<string> {
    return new Promise((resolve, reject) => {
      const mimetype = file.originalname.split('.').pop()
      const fileName = `images/${SERVICE_NAME}/${Date.now()}.${mimetype}`
      const blob = this.bucket.file(fileName)
      const blobStream = blob.createWriteStream()
      blobStream.on('error', (err) => {
        console.log(err)
        reject(err)
      })
      blobStream.on('finish', () => {
        resolve(`https://${this.bucket.name}/${blob.name}`)
      })

      blobStream.end(file.buffer)
    })
  }
}
