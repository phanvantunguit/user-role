import { Inject } from '@nestjs/common'
import { GCloudTransport } from 'src/resources/storage/google.transport'

export class StorageResource {
  constructor(
    @Inject(GCloudTransport)
    private gCloudTransport: GCloudTransport
  ) {}
  async upload(file): Promise<string> {
    return this.gCloudTransport.upload(file)
  }
}
