import { Inject } from '@nestjs/common'
import { APP_CONFIG } from 'src/config'
import { BROKER_CODE, TBOT_PLATFORM } from 'src/const/bot'
import { MetaapiTransport } from 'src/resources/broker/metaapi/metaapi.transport'

export interface IMetaapiDataCheckAccountReferral {
  broker_code: BROKER_CODE.fxlink
  account: string
  broker_profile_id: string
  platform: TBOT_PLATFORM
}
export class BrokerDefaultResource {
  constructor(
    @Inject(MetaapiTransport)
    private metaapiTransport: MetaapiTransport
  ) {}

  async checkAccountReferral(
    param: IMetaapiDataCheckAccountReferral
  ): Promise<any> {
    console.log("MetaapiCheckAccountReferral param", param)
    const { account, broker_profile_id, platform } = param
    if (!broker_profile_id || !account || !platform) {
      return false
    }
    const url = `${APP_CONFIG.MT_URL}/users/current/${platform}/provisioning-profiles/${broker_profile_id}/accounts/${account}/account-information`
    const res = await this.metaapiTransport.requestGET(url, APP_CONFIG.MT_TOKEN)
    console.log("MetaapiCheckAccountReferral res", res)
    if (res?.login?.toString() === account.toString()) {
      return true
    }
    return false
  }
}
