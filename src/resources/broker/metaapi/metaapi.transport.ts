import { requestGET } from 'src/utils/http-transport.util'
export class MetaapiTransport {
  async requestGET(url: string, apiKey: string): Promise<any> {
    const headers = {
      'Content-Type': 'application/json;',
      'auth-token': apiKey,
    }
    try {
      const result = await requestGET(url, headers)
      return result
    } catch (error) {
      console.log('error', error)
      return false
    }
  }
}
