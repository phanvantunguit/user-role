import { requestPOST } from 'src/utils/http-transport.util'
import { APP_CONFIG } from 'src/config'
import {
  cleanObject,
} from 'src/utils/format-data.util'
import { HttpStatus } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { throwError } from 'src/utils/handle-error.util'
export class AxiTransport {
  async requestPOST(url: string, secretKey: string, clientID: string): Promise<any> {
    const headers = {
      'Content-Type': 'application/json;',
      SecretKey: secretKey,
      ClientID: clientID
    }
    try {
      const result = await requestPOST(url, {}, headers)
      return result
    } catch (error) {
      console.log('error', error)
      // throwError({
      //   status: HttpStatus.UNPROCESSABLE_ENTITY,
      //   ...ERROR_CODE.UNPROCESSABLE,
      // })
      return false
    }
  }
}
