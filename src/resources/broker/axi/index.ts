import { Inject } from '@nestjs/common'
import { APP_CONFIG } from 'src/config'
import { SERVICE_NAME } from 'src/const/app-setting'
import { BROKER_CODE } from 'src/const/bot'
import { AxiTransport } from 'src/resources/broker/axi/axi.transport'

export interface IAxiDataCheckAccountReferral {
  broker_code: BROKER_CODE.axi
  account: string
  agent_account: string
  client_id: string
  secret_key: string
}
export class AxiResource {
  constructor(
    @Inject(AxiTransport)
    private axiTransport: AxiTransport
  ) {}
  async checkAccountReferral(param: IAxiDataCheckAccountReferral): Promise<any> {
    console.log('checkAccountReferral param', param)
    const { account, agent_account, client_id, secret_key } = param
    if(!secret_key || !client_id || !agent_account || !account ) {
      return false
    }
    const url = `${APP_CONFIG.AXI_BASE_URL}/api/ExternalApi/GetAccountReports?MT4AccountNumber=${account}`
    const res = await this.axiTransport.requestPOST(url, secret_key, client_id)
    console.log("axiTransport res")
    if (res) {
      return res.Data.some((e) => e.AgentAccount == agent_account)
    }
    return false
  }
}
