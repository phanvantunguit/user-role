import { Inject } from '@nestjs/common'
import { BROKER_CODE } from 'src/const/bot'
import { AxiResource, IAxiDataCheckAccountReferral } from './axi'
import { BrokerDefaultResource, IMetaapiDataCheckAccountReferral } from './metaapi'
import { IXMDataCheckAccountReferral, XMResource } from './xm'

export class BrokerResource {
  constructor(
    @Inject(XMResource) private xmResource: XMResource,
    @Inject(AxiResource) private axiResource: AxiResource,
    @Inject(BrokerDefaultResource) private brokerDefaultResource: BrokerDefaultResource
  ) {}

  async checkAccountReferral(
    param: IAxiDataCheckAccountReferral | IXMDataCheckAccountReferral | IMetaapiDataCheckAccountReferral
  ): Promise<boolean> {
    switch (param.broker_code) {
      case BROKER_CODE.axi:
        return this.axiResource.checkAccountReferral(param)
      case BROKER_CODE.xm:
        return this.xmResource.checkAccountReferral(param)
      default:
        return this.brokerDefaultResource.checkAccountReferral(param)
    }
  }
}
