import { requestGET } from 'src/utils/http-transport.util'
import { HttpStatus } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { throwError } from 'src/utils/handle-error.util'
export class XMTransport {
  async requestGET(url: string, apiKey: string): Promise<any> {
    const headers = {
      'Content-Type': 'application/json;',
      Authorization: `Bearer ${apiKey}`,
    }
    try {
      const result = await requestGET(url, headers)
      return result
    } catch (error) {
      console.log('error', error)
      //   throwError({
      //     status: HttpStatus.UNPROCESSABLE_ENTITY,
      //     ...ERROR_CODE.UNPROCESSABLE,
      //   })
      return false
    }
  }
}
