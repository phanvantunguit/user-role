import { Inject } from '@nestjs/common'
import { APP_CONFIG } from 'src/config'
import { BROKER_CODE } from 'src/const/bot'
import { XMTransport } from 'src/resources/broker/xm/xm.transport'

export interface IXMDataCheckAccountReferral {
  broker_code: BROKER_CODE.xm
  account: string
  api_key: string
}
export class XMResource {
  constructor(
    @Inject(XMTransport)
    private xmTransport: XMTransport
  ) {}

  async checkAccountReferral(param: IXMDataCheckAccountReferral): Promise<any> {
    const { account, api_key } = param
    if(!api_key || !account) {
      return false
    }
    const url = `${APP_CONFIG.XM_BASE_URL}/api/traders/${account}`
    const res = await this.xmTransport.requestGET(url, api_key)
    if (res?.accountType === 'STANDARD') {
      return true
    }
    return false
  }
}
