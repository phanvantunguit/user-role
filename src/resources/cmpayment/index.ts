import { Inject } from '@nestjs/common'
import { APP_CONFIG } from 'src/config'
import { SERVICE_NAME } from 'src/const/app-setting'
import { CmpaymentTransport } from 'src/resources/cmpayment/cmpayment.transport'

export class CmpaymentResource {
  constructor(
    @Inject(CmpaymentTransport)
    private cmpaymentTransport: CmpaymentTransport
  ) {}
  async createTransaction(params: any): Promise<any> {
    const url = `${APP_CONFIG.CMPAYMENT_BASE_URL}/api/v1/transaction`
    return this.cmpaymentTransport.requestPOST(params, url)
  }
  async getTransaction(id: string): Promise<any> {
    const url = `${APP_CONFIG.CMPAYMENT_BASE_URL}/api/v1/transaction/${id}`
    return this.cmpaymentTransport.requestGET({}, url)
  }
  async listTransaction(param: {
    user_id?: string
    email?: string
  }): Promise<any> {
    const url = `${APP_CONFIG.CMPAYMENT_BASE_URL}/api/v1/transaction/list`
    return this.cmpaymentTransport.requestGET(
      { integrate_service: SERVICE_NAME, ...param },
      url
    )
  }
}
