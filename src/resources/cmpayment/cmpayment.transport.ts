// import { ISendEmail } from './types';
import { requestPOST, requestGET } from 'src/utils/http-transport.util'
import { APP_CONFIG } from 'src/config'
import { createChecksum } from 'src/utils/hash.util'
import {
  cleanObject,
  formatRequestDataToStringV2,
} from 'src/utils/format-data.util'
import { HttpStatus } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { throwError } from 'src/utils/handle-error.util'
export class CmpaymentTransport {
  async requestPOST(params: any, url: string): Promise<any> {
    const dataClear = cleanObject(params)
    const dataString = formatRequestDataToStringV2(dataClear)
    const checksum = createChecksum(dataString, APP_CONFIG.SECRET_KEY)
    dataClear['checksum'] = checksum
    const headers = {
      'Content-Type': 'application/json',
    }
    try {
      const result = await requestPOST(url, dataClear, headers)
      return result
    } catch (error) {
      console.log('error', error)
      throwError({
        status: HttpStatus.UNPROCESSABLE_ENTITY,
        ...ERROR_CODE.UNPROCESSABLE,
      })
    }
  }
  async requestGET(params: any, url: string): Promise<any> {
    const dataClear = cleanObject(params)
    const dataString = formatRequestDataToStringV2(dataClear)
    const checksum = createChecksum(dataString, APP_CONFIG.SECRET_KEY)
    dataClear['checksum'] = checksum
    const headers = {
      'Content-Type': 'application/json',
    }
    try {
      url = `${url}?${dataString}`
      const result = await requestGET(url, headers)
      return result
    } catch (error) {
      console.log('error', error)
      throwError({
        status: HttpStatus.UNPROCESSABLE_ENTITY,
        ...ERROR_CODE.UNPROCESSABLE,
      })
    }
  }
}
