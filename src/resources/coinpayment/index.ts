import { Inject } from '@nestjs/common'
import { CoinpaymentTransport } from 'src/resources/coinpayment/coinpayment.transport'
import { IConvertCoin, ICreateTransaction } from './types'
export class CoinpaymentResource {
  constructor(
    @Inject(CoinpaymentTransport)
    private coinpaymentTransport: CoinpaymentTransport
  ) {}
  async createTransaction(params: ICreateTransaction): Promise<any> {
    const data = {
      cmd: 'create_transaction',
      amount: params.sell_amount,
      currency1: params.sell_currency,
      currency2: params.buy_currency,
      buyer_email: params.buyer_email,
    }
    return this.coinpaymentTransport.requestPOST(data)
  }
  async checkTransaction(payment_id: string): Promise<any> {
    const data = {
      cmd: 'get_tx_info',
      txid: payment_id,
    }
    return this.coinpaymentTransport.requestPOST(data)
  }
  async checkWithdrawal(withdrawal_id: string): Promise<any> {
    const data = {
      cmd: 'get_withdrawal_info',
      id: withdrawal_id,
    }
    return this.coinpaymentTransport.requestPOST(data)
  }
  async convertCoin(params: IConvertCoin): Promise<any> {
    const data = {
      cmd: 'convert',
      amount: params.amount,
      from: params.from,
      to: params.to,
    }
    return this.coinpaymentTransport.requestPOST(data)
  }
}
