// import { ISendEmail } from './types';
import { requestPOST } from 'src/utils/http-transport.util'
import { APP_CONFIG } from 'src/config'
import { createChecksumSHA512HMAC } from 'src/utils/hash.util'
import {
  cleanObject,
  formatRequestDataToString,
} from 'src/utils/format-data.util'
import { HttpStatus } from '@nestjs/common'
import { ERROR_CODE } from 'src/const'
import { throwError } from 'src/utils/handle-error.util'
export class CoinpaymentTransport {
  async requestPOST(params: any): Promise<any> {
    const url =
      APP_CONFIG.COINPAYMENT_BASE_URL || 'https://www.coinpayments.net/api.php'
    const dataClear = {
      ...cleanObject(params),
      version: '1',
      key: APP_CONFIG.COINPAYMENT_PUBLIC_KEY,
      format: 'json',
    }
    const dataString = formatRequestDataToString(dataClear)
    const checksum = createChecksumSHA512HMAC(
      dataString,
      APP_CONFIG.COINPAYMENT_PRIVATE_KEY,
      'hex'
    )
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      HMAC: checksum,
    }
    // return dataClear
    try {
      const result = await requestPOST(url, dataString, headers)
      return result
    } catch (error) {
      console.log('error', error)
      throwError({
        status: HttpStatus.UNPROCESSABLE_ENTITY,
        ...ERROR_CODE.UNPROCESSABLE,
      })
    }
  }
}
