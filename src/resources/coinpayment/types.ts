export interface ICreateTransaction {
  sell_amount: number
  sell_currency: string
  buy_currency: string
  buyer_email: string
}
export interface IConvertCoin {
  amount: number
  from: string
  to: string
}
