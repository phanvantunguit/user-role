import { MigrationInterface, QueryRunner } from 'typeorm'
import { DEFAULT_ROLE } from 'src/const/permission'
export class InsertInitRoleData1653992361403 implements MigrationInterface {
  name: 'InsertInitRoleData1653992361403'
  async up(queryRunner: QueryRunner): Promise<any> {
    const queryString = `
        INSERT INTO roles (
            id, role_name, root, description, owner_created, created_at, updated_at)
            VALUES ('${DEFAULT_ROLE.id}','${
      DEFAULT_ROLE.role_name
    }', '${JSON.stringify(DEFAULT_ROLE.root)}', '${
      DEFAULT_ROLE.description
    }', '${DEFAULT_ROLE.owner_created}', ${DEFAULT_ROLE.created_at}, ${
      DEFAULT_ROLE.updated_at
    })
            ON CONFLICT (id) 
            DO NOTHING;
        `
    await queryRunner.query(queryString)
  }
  down(queryRunner: QueryRunner): Promise<any> {
    return
  }
}
