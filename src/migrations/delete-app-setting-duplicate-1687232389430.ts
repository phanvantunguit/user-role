import { MigrationInterface, QueryRunner } from 'typeorm'
export class DeleteAppSettingDuplicate1687232389430 implements MigrationInterface {
  name: 'DeleteAppSettingDuplicate1687232389430'
  async up(queryRunner: QueryRunner): Promise<any> {
    const deleteString = `
    DELETE FROM app_settings ast
    WHERE user_id is null and (SELECT COUNT (*) FROM app_settings WHERE name = ast.name) > 1
    `
    await queryRunner.query(deleteString)

    const updateString =`
    UPDATE app_settings
    SET user_id = 'system'
    WHERE user_id is null`
    await queryRunner.query(updateString)
  }
  down(queryRunner: QueryRunner): Promise<any> {
    return
  }
}
