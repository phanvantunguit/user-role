import { MigrationInterface, QueryRunner } from 'typeorm'
export class DeleteUserRolesExpires1670317741275 implements MigrationInterface {
  name: 'DeleteUserRolesExpires1670317741275'
  async up(queryRunner: QueryRunner): Promise<any> {
    const queryString = `
    DELETE FROM user_roles
    where expires_at < ${Date.now()}
    `
    await queryRunner.query(queryString)
  }
  down(queryRunner: QueryRunner): Promise<any> {
    return
  }
}
