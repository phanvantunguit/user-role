import { Inject } from '@nestjs/common'
import { OnEvent } from '@nestjs/event-emitter'

import { BOT_TRADING_EVENT } from 'src/const/bot'
import { BotTradingService } from 'src/services/bot-trading'

export class BotTradingEvent {
  constructor(
    @Inject(BotTradingService) private botTradingService: BotTradingService
  ) {}
  @OnEvent(BOT_TRADING_EVENT.CONNECTING)
  async handleConnectingEvent(payload: {
    user_bot_id: string,
    user_id: string
    bot_id: string
    bot_code: string
  }) {
    console.log('BOT_TRADING_EVENT.CONNECTING', payload.bot_code)
    await this.botTradingService.connectBroker(payload)
  }
}
