import { Inject } from '@nestjs/common'
import { OnEvent } from '@nestjs/event-emitter'
import { TRANSACTION_EVENT, TRANSACTION_STATUS } from 'src/const/transaction'
import { RawTransactionLog } from 'src/domains/transaction'
import { TransactionRepository } from 'src/repositories/transaction.repository'
import { MailResource } from 'src/resources/mail'
import { TransactionService } from 'src/services/transaction'
import { PaymentService } from 'src/services/payment'
import { TransactionLogService } from 'src/services/transaction-log'
import { EMAIL_SUBJECT } from 'src/const/email'

export class TransactionEvent {
  constructor(
    @Inject(TransactionRepository)
    private transactionRepository: TransactionRepository,
    private mailResource: MailResource,
    private transactionService: TransactionService,
    private paymentService: PaymentService,
    private transactionLogService: TransactionLogService
  ) {}

  // send email to customer
  // check log send email
  @OnEvent(TRANSACTION_EVENT.PAYMENT_CREATED)
  async handleCreatedEvent(payload: RawTransactionLog) {
    const transLog = await this.transactionLogService.get({
      transaction_id: payload.transaction_id,
      transaction_event: TRANSACTION_EVENT.SEND_EMAIL,
    })
    if (payload.transaction_status !== TRANSACTION_STATUS.FAILED && !transLog) {
      const transaction = await this.transactionRepository.viewfindOne({
        id: payload.transaction_id,
      })
      const extraDataPaymentInfo = await this.paymentService
        .getService(transaction.payment_method)
        .extraDataPaymentInfo(payload)
      const dataSend = {
        email: transaction.email,
        role_name: transaction.details[0].role_name,
        package_name: transaction.details[0].package_name,
        amount: transaction.sell_amount.toString(),
        currency: transaction.sell_currency,
        buy_amount: transaction.buy_amount,
        buy_currency: transaction.buy_currency,
        payment_id: transaction.payment_id,
        ...extraDataPaymentInfo,
      }
      const resEmail = await this.mailResource.sendEmailPaymentInfo(dataSend)
      this.transactionLogService.saveNotSendEvent({
        transaction_id: transaction.id,
        transaction_status: payload.transaction_status,
        transaction_event: TRANSACTION_EVENT.SEND_EMAIL,
        metadata: {
          is_send: resEmail,
          subject: EMAIL_SUBJECT.transaction_information,
          ...dataSend,
        },
      })
    } else {
      const transaction = await this.transactionRepository.findOne({
        id: payload.transaction_id,
      })
      await this.transactionRepository.save({
        ...transaction,
        status: payload.transaction_status,
      })
    }
  }

  // event first
  @OnEvent(TRANSACTION_EVENT.PAYMENT_PROCESSING)
  async handleProcessingEvent(payload: RawTransactionLog) {
    const transaction = await this.transactionRepository.findOne({
      id: payload.transaction_id,
    })
    if (transaction.status === TRANSACTION_STATUS.CREATED) {
      await this.transactionRepository.save({
        ...transaction,
        status: payload.transaction_status,
      })
    }
  }

  // event first
  @OnEvent(TRANSACTION_EVENT.PAYMENT_COMPLETE)
  async handleCompleteEvent(payload: RawTransactionLog) {
    const transaction = await this.transactionRepository.findOne({
      id: payload.transaction_id,
    })
    if (
      transaction.status === TRANSACTION_STATUS.PROCESSING ||
      transaction.status === TRANSACTION_STATUS.CREATED
    ) {
      this.transactionService.handlePaymentComplete(transaction)
    }
  }
  // event first
  @OnEvent(TRANSACTION_EVENT.PAYMENT_FAILED)
  async handleFailedEvent(payload: RawTransactionLog) {
    const transaction = await this.transactionRepository.findOne({
      id: payload.transaction_id,
    })
    if (
      transaction.status === TRANSACTION_STATUS.PROCESSING ||
      transaction.status === TRANSACTION_STATUS.CREATED
    ) {
      this.transactionService.handlePaymentFailed(transaction)
    }
  }

  // update user role first
  @OnEvent(TRANSACTION_EVENT.DELIVERED)
  async handleDeliveredEvent(payload: RawTransactionLog) {
    // const transaction = await this.transactionRepository.findOne({
    //   id: payload.transaction_id,
    // })
    // const dataSend = {
    //   email: transaction.email,
    //   role_name: transaction.details[0].role_name,
    //   package_name: transaction.details[0].package_name,
    //   amount: transaction.sell_amount.toFixed(2),
    //   currency: transaction.sell_currency,
    // }
    // const resEmail = await this.mailResource.sendEmailUpgradeSuccess(dataSend)
    // this.transactionLogService.saveNotSendEvent({
    //   transaction_id: transaction.id,
    //   transaction_status: payload.transaction_status,
    //   transaction_event: TRANSACTION_EVENT.SEND_EMAIL,
    //   metadata: {
    //     is_send: resEmail,
    //     subject: EMAIL_SUBJECT.upgrade_account_success,
    //     ...dataSend,
    //   },
    // })
  }
}
