import { BaseDomain } from '../base/types'

export type BotSignalDomain = BaseDomain<QueryBotSignal, RawBotSignal>

export type QueryBotSignal = {
  id?: string

  name?: string

  signal_id?: string

  time?: number

  type?: string

  exchange?: string

  symbol?: string
}
export type RawBotSignal = {
  id?: string

  name?: string

  resolution?: string

  signal_id?: string

  time?: number

  type?: string

  image_url?: string

  exchange?: string

  symbol?: string

  metadata?: any

  created_at?: number
}
