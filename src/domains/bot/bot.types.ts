import { BOT_STATUS } from 'src/const'
import { BaseDomain } from '../base/types'

export type BotDomain = BaseDomain<QueryBot, RawBot>

export type QueryBot = {
  id?: string
  name?: string
  code?: string
  status?: BOT_STATUS
}
export type RawBot = {
  id?: string
  name?: string
  code?: string
  bot_setting_id?: string
  type?: string
  description?: string
  work_based_on?: any
  status?: BOT_STATUS
  price?: string
  currency?: string
  image_url?: string
  owner_created?: string
  order?: number
  created_at?: number
  updated_at?: number
}
