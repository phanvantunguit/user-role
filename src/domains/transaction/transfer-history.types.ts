import { BaseDomain } from '../base/types'

export type TransferHistoryDomain = BaseDomain<
  QueryTransferHistory,
  RawTransferHistory
>

export type QueryTransferHistory = {
  transaction_id?: string
  transfer_id?: string
  id?: string
}
export type RawTransferHistory = {
  id?: string
  transaction_id: string
  transfer_id: string
  buy_amount: string
  buy_currency: string
  sell_amount: number
  sell_currency: string
  created_at?: number
}
