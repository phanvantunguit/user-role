import { TRANSACTION_EVENT, TRANSACTION_STATUS } from 'src/const/transaction'
import { BaseDomain } from '../base/types'

export type TransactionLogDomain = BaseDomain<
  QueryTransactionLog,
  RawTransactionLog
>

export type QueryTransactionLog = {
  transaction_id?: string
  id?: string
  transaction_status?: TRANSACTION_STATUS
  transaction_event?: TRANSACTION_EVENT
}
type TransactionLog = {
  id?: string
  transaction_id: string
  transaction_event: TRANSACTION_EVENT
  transaction_status: TRANSACTION_STATUS
  created_at?: number
}
export type RawTransactionLog = TransactionLog & {
  metadata: any
}
