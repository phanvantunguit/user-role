import { BaseDomain } from '../base/types'

export type TransactionDetailDomain = BaseDomain<
  QueryTransactionDetail,
  RawTransactionDetail
>

export type RawTransactionDetailView = {
  id: string
  user_id: string
  transaction_id: string
  role_id: string
  role_name: string
  price: number
  currency: string
  package_id: string
  package_name: string
  discount_rate: number
  discount_amount: number
  quantity: number
  expires_at: number
  created_at: number
}

export type QueryTransactionDetail = {
  transaction_id?: string
  user_id?: string
  id?: string
}
export type RawTransactionDetail = {
  id?: string
  user_id: string
  transaction_id: string
  role_id: string
  price: number
  currency: string
  package_id: string
  package_type: string
  package_name: string
  discount_rate: number
  discount_amount: number
  quantity: number
  expires_at: number
  created_at?: number
}
