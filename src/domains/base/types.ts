export interface BaseDomain<Query, Raw> {
  domain_name: string
  get(params: Query): Promise<Raw>
  list(params: Query): Promise<Raw[]>
  save(params: Raw): Promise<Raw>
  delete(params: Query): Promise<any>
  checkNotFoundAndThrow(params: Query): Promise<Raw>
  checkExistAndThrow(params: Query)
}
