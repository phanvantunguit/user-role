export interface AppSettingDomain {
  // read
  listAppSetting(params: QueryAppSetting): Promise<RawAppSetting[]>
  getAppSetting(params: QueryAppSetting): Promise<RawAppSetting>
  // write
  createAppSetting(params: AppSetting): Promise<any>
  updateAppSetting(params: AppSettingUpdate): Promise<any>
  deleteAppSetting(id: string): Promise<any>
}

export type AppSetting = {
  name?: string
  value?: string
  description?: string
  owner_created?: string
}

export type AppSettingUpdate = AppSetting & {
  id: string
}

export type QueryAppSetting = {
  id?: string
  name?: string
  user_id?: string
}

export type RawAppSetting = AppSetting & {
  id?: string
  user_id?: string
  created_at?: number
  updated_at?: number
}

export type BrokerSettings = {
  [code: string]: BrokerSetting
}

export type BrokerSetting = {
  name: string
  code: string
  broker_timezone: string
  broker_dst_switch_timezone: string
  check_referral_broker: boolean
  required_profile: boolean
  servers: string[]
  referral_setting: { key: string; name: string; type: string }[]
}
