export interface ExchangeDomain {
  // read
  listExchange(): Promise<Exchange[]>
  getExchange(params: QueryExchange): Promise<Exchange>
  // write
  createExchange(params: Exchange[]): Promise<any>
  updateExchange(params: Exchange): Promise<any>
  deleteExchange(ids: string[]): Promise<any>
}

export type Exchange = {
  exchange_name: string
  exchange_desc: string
}
export type QueryExchange = {
  exchange_name?: string
}
export type RawExchange = Exchange & {
  created_at?: number
}
