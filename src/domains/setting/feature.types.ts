export interface FeatureDomain {
  // read
  getFeature(): Promise<RawFeature[]>
  getFeatureDetail(params: QueryFeature): Promise<RawFeature>
  // write
  createFeature(params: FeatureCreate): Promise<any>
  updateFeature(params: FeatureUpdate): Promise<any>
  deleteFeature(feature_ids: string[]): Promise<any>
}

export type Feature = {
  feature_id: string
  feature_name: string
  description?: string
  action?: string
}

export type FeatureCreate = {
  owner_created: string
  features: Feature[]
}

export type FeatureUpdate = Feature & {
  owner_created: string
}

export type RawFeature = Feature & {
  owner_created: string
  created_at?: number
  updated_at?: number
}
export type QueryFeature = {
  feature_id?: string
  feature_name?: string
}
