export interface GeneralSettingDomain {
  // read
  listGeneralSetting(): Promise<RawGeneralSetting[]>
  getGeneralSetting(params: QueryGeneralSetting): Promise<RawGeneralSetting>
  // write
  createGeneralSetting(params: GeneralSettingCreate): Promise<any>
  updateGeneralSetting(params: GeneralSettingUpdate): Promise<any>
  deleteGeneralSetting(general_setting_ids: string[]): Promise<any>
}

export type GeneralSetting = {
  general_setting_id: string
  general_setting_name: string
  description?: string
}
export type GeneralSettingCreate = {
  general_settings: GeneralSetting[]
  owner_created: string
}
export type GeneralSettingUpdate = GeneralSetting & {
  owner_created: string
}
export type QueryGeneralSetting = {
  general_setting_id?: string
  general_setting_name?: string
}
export type RawGeneralSetting = GeneralSetting & {
  owner_created: string
  created_at?: number
  updated_at?: number
}
