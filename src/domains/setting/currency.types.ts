import { BaseDomain } from '../base/types'

export interface CurrencyDomain extends BaseDomain<QueryCurrency, RawCurrency> {
  updateOrderCurrency(params: CurrencyOrderUpdate): Promise<any[]>
}

export type CurrencyOrderUpdate = {
  currencies: {
    id: string
    order: number
  }[]
  owner_created: string
}

export type QueryCurrency = {
  id?: string
  name?: string
  currency?: string
  status?: boolean
}
export type RawCurrency = {
  id?: string
  name?: string
  description?: string
  currency?: string
  image_url?: string
  owner_created?: string
  status?: boolean
  order?: number
  created_at?: number
  updated_at?: number
}
