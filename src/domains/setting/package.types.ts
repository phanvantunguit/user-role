import { PACKAGE_TYPE } from 'src/const'
import { BaseDomain } from '../base/types'

export type PackageDomain = BaseDomain<QueryPackage, RawPackage>

export type QueryPackage = {
  id?: string
  name?: string
  status?: boolean
  type?: PACKAGE_TYPE
}
export type RawPackage = {
  id?: string
  name: string
  discount_rate?: number
  discount_amount?: number
  status: boolean
  quantity: number
  type: PACKAGE_TYPE
  owner_created: string
  created_at?: number
  updated_at?: number
}
