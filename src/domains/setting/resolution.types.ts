export interface ResolutionDomain {
  // read
  listResolution(): Promise<RawResolution[]>
  getResolution(params: QueryResolution): Promise<RawResolution>
  // write
  createResolution(params: Resolution[]): Promise<RawResolution[]>
  deleteResolution(ids: string[]): Promise<any>
  updateResolution(params: ResolutionUpdate): Promise<RawResolution>
}

export type Resolution = {
  resolutions_name: string
  display_name: string
}
export type ResolutionUpdate = Resolution & {
  id
}
export type QueryResolution = {
  id?: string
  resolutions_name?: string
  display_name?: string
}
export type RawResolution = Resolution & {
  id?: string
  created_at?: number
}
