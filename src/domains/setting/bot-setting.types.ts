import { BaseDomain } from '../base/types'

export type BotSettingDomain = BaseDomain<QueryBotSetting, RawBotSetting>

export type QueryBotSetting = {
  id?: string
  name?: string
  status?: boolean
}
export type RawBotSetting = {
  id?: string
  name?: string
  params?: any
  owner_created?: string
  status?: boolean
  created_at?: number
  updated_at?: number
}
