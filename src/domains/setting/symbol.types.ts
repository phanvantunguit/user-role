import { SYMBOL_STATUS } from 'src/const'

export interface SymbolDomain {
  // read
  listSymbol(): Promise<RawSymbol[]>
  getSymbol(params: QuerySymbol): Promise<RawSymbol>
  // write
  createSymbol(params: CreateSymbol[]): Promise<RawSymbol[]>
  updateSymbol(params: UpdateSymbol): Promise<RawSymbol>
  deleteSymbol(symbol: string[]): Promise<any>
}
export type CreateSymbol = {
  symbol: string
  types: string
  exchange_name: string
  base_symbol: string
  quote_symbol: string
  description?: string
  ticks: any
  status: SYMBOL_STATUS
  timezone?: string
  minmov?: number
  minmov2?: number
  pointvalue?: number
  session?: string
  has_intraday?: boolean
  has_no_volume?: boolean
  pricescale?: number
}

export type UpdateSymbol = {
  symbol: string
  types?: string
  exchange_name?: string
  base_symbol?: string
  quote_symbol: string
  description?: string
  ticks?: any
  status?: SYMBOL_STATUS
  timezone?: string
  minmov?: number
  minmov2?: number
  pointvalue?: number
  session?: string
  has_intraday?: boolean
  has_no_volume?: boolean
  pricescale?: number
}

export type SymbolData = {
  symbol: string
  types?: string
  exchange_name?: string
  base_symbol?: string
  quote_symbol: string
  description?: string
  ticks?: any
  status?: SYMBOL_STATUS
  timezone?: string
  minmov?: number
  minmov2?: number
  pointvalue?: number
  session?: string
  has_intraday?: boolean
  has_no_volume?: boolean
  pricescale?: number
}
export type QuerySymbol = {
  symbol?: string
  status?: SYMBOL_STATUS
}
export type RawSymbol = SymbolData & {
  created_at?: number
}
