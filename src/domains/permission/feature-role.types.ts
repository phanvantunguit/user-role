export interface FeatureRoleDomain {
  // write
  // modify
  modifyFeatureRole(params: FeatureRoleCreate): Promise<any>
}

type Feature = {
  feature_id: string
  description?: string
}

export type FeatureRoleCreate = {
  role_id: string
  features: Feature[]
  owner_created: string
}

export type FeatureRole = {
  role_id: string
  feature_id: string
  description?: string
  owner_created: string
}

export type FeatureRoleUpdate = FeatureRole & {
  id: string
}

export type RawFeatureRole = FeatureRole & {
  id?: string
  created_at?: number
  updated_at?: number
}
export type QueryFeatureRole = {
  role_id?: string
}
