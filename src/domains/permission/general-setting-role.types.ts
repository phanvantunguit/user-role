export interface GeneralSettingRoleDomain {
  // write
  // modify
  modifyGeneralSettingRole(params: GeneralSettingRoleCreate): Promise<any>
  getMaxLimitTab(roleIds: string[]): Promise<number>
}

export type GeneralSettingRole = {
  general_setting_id: string
  description?: string
  val_limit: number
}

export type GeneralSettingRoleCreate = {
  role_id: string
  owner_created: string
  general_settings: GeneralSettingRole[]
}
export type GeneralSettingRoleUpdate = GeneralSettingRole & {
  id: string
  role_id: string
  owner_created: string
}
export type RawGeneralSettingRole = GeneralSettingRole & {
  id?: string
  role_id: string
  owner_created: string
  created_at?: number
  updated_at?: number
}
export type QueryGeneralSettingRole = {
  role_id?: string
}
