export interface SymbolSettingRoleDomain {
  // write
  modifySymbolSettingRole(params: SymbolSettingRoleCreate): Promise<any>
  createSymbolSettingRole(params: SymbolSettingRoleCreate): Promise<any>
  updateSymbolSettingRole(params: SymbolSettingRoleUpdate): Promise<any>
  deleteSymbolSettingRole(params: SymbolSettingRoleDelete): Promise<any>
}
export type SymbolSettingRole = {
  description?: string
  list_exchanged?: string[]
  list_symbol?: string[]
  supported_resolutions?: string[]
  role_id: string
}
export type SymbolSettingRoleCreate = SymbolSettingRole & {
  owner_created: string
}
export type SymbolSettingRoleUpdate = SymbolSettingRole & {
  id: string
  owner_created: string
}
export type SymbolSettingRoleDelete = {
  id: string
  role_id: string
}
export type QuerySymbolSettingRole = {
  id?: string
  role_id?: string
}
export type RawSymbolSettingRole = SymbolSettingRole & {
  id?: string
  created_at?: number
  updated_at?: number
}
