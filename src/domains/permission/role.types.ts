import {
  RawFeature,
  RawGeneralSetting,
  RawSymbol,
  RawExchange,
  RawResolution,
} from '../setting/'
export interface RoleDomain {
  // write
  // create new role
  createRole(params: RoleCreate): Promise<any>
  // update role
  updateRole(params: RoleUpdate): Promise<any>
  // delete role
  deleteRole(id: string): Promise<any>

  updateOrderRole(params: RoleOrderUpdate): Promise<any[]>

  //read
  listRoleForUser(userId: string, package_id?: string): Promise<any>
  listRole(params: ListRole): Promise<any>
  getRoleDetail(id: string): Promise<any>
}

type SymbolSettingRole = {
  id: string
  description?: string
  symbols?: RawSymbol[]
  exchanges?: RawExchange[]
  resolutions?: RawResolution[]
}
type Root = {
  features?: RawFeature[]
  general_settings?: RawGeneralSetting[]
  symbol_settings_roles?: SymbolSettingRole[]
}

export type Role = {
  role_name?: string
  root?: Root
  description?: string
  status?: string
  type?: string
  price?: string
  currency?: string

  parent_id?: string
  is_best_choice?: boolean
  order?: number
  description_features?: {
    features: string[]
  }
  color?: string
}
export type RoleCreate = {
  role_name: string
  owner_created: string
  description?: string
  status: string
  type: string
  price: string
  currency: string

  parent_id?: string
  is_best_choice?: boolean
  order?: number
  description_features?: string[]
  color?: string
}
export type RoleUpdate = RoleCreate & {
  id: string
}
export type RoleOrderUpdate = {
  roles: {
    id: string
    order: number
  }[]
  owner_created: string
}
export type QueryRole = {
  id?: string
  role_name?: string
}
export type ListRole = {
  user_id?: string
}
export type RawRole = Role & {
  id?: string
  owner_created: string
  created_at?: number
  updated_at?: number
}
