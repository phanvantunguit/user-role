export interface AuthRoleDomain {
  // write
  // create new roles
  createAuthRole(params: AuthRoleCreate): Promise<RawAuthRole>
  // update role
  updateAuthRole(params: AuthRoleUpdate): Promise<RawAuthRole>
  // delete role
  deleteAuthRole(ids: string[]): Promise<any>
  // get permission
  getListPermission(): Permission[]
  // get auth user role
  getListAuthUserRole(params: ListAuthRole): Promise<RawAuthRole[]>
  // get auth user role
  getAuthUserRole(params: QueryAuthRole): Promise<RawAuthRole>
}

export type Permission = {
  permission_id: string
  permission_name: string
  description: string
}
type Root = {
  permissions: Permission[]
}

export type AuthRole = {
  role_name?: string
  root?: Root
  description?: string
  owner_created?: string
}

export type AuthRoleUpdate = {
  id: string
  permission_ids: string[]
  role_name: string
  owner_created: string
  description?: string
}
export type AuthRoleCreate = {
  permission_ids: string[]
  role_name: string
  owner_created: string
  description?: string
}
export type QueryAuthRole = {
  id?: string
  role_name?: string
}
export type ListAuthRole = {
  user_id?: string
}
export type RawAuthRole = AuthRole & {
  id?: string
  created_at?: number
  updated_at?: number
}
