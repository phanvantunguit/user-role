export interface AuthUserRoleDomain {
  // write
  // modify role
  modifyAuthUserRole(params: AuthUserRoleModify): Promise<any>
}

export type AuthUserRole = {
  auth_role_id?: string
  description?: string
}
export type AuthUserRoleModify = {
  user_id: string
  owner_created: string
  auth_roles: AuthUserRole[]
}
export type QueryAuthUserRole = {
  user_id?: string
}
export type RawAuthUserRole = AuthUserRole & {
  id?: string
  owner_created?: string
  created_at?: number
  updated_at?: number
}
