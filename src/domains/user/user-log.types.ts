export type UserLog = {
  user_id: string
  ip_number: string
  browser_type: string
}
export type QueryUserLog = {
  user_id?: string
}
export type RawUserLog = UserLog & {
  id?: string
  created_at?: number
}
