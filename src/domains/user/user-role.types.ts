export interface UserRoleDomain {
  // write
  // modify role
  modifyUserRole(params: UserRoleModify): Promise<any>
  addUserRoles(params: AddUserRoles): Promise<any>
  removeUserRoles(owner_created: string, params: RemoveUserRoles): Promise<any>
}

export type UserRole = {
  role_id: string
  description?: string
  expires_at?: number
  package_name?: string
  package_id?: string
  package_type?: string
  quantity?: number
}
export type AddUserRoles = {
  user_ids: string[]
  owner_created: string
  role_ids: string[]
  quantity: number
  package_type: string
}
export type RemoveUserRoles = {
  user_ids: string[]
  role_ids: string[]
}
export type UserRoleModify = {
  user_id: string
  owner_created: string
  roles: UserRole[]
}

export type QueryUserRole = {
  user_id?: string
  role_id?: string
  order_id?: string
}

export type RawUserRole = UserRole & {
  id?: string
  order_id?: string
  user_id: string
  owner_created: string
  created_at?: number
  updated_at?: number
}
