import { ITEM_STATUS } from 'src/const/transaction'

export type QueryUserBot = {
  id?: string
  bot_id?: string
  user_id?: string
  status?: ITEM_STATUS
}

export type RawUserBot = {
  id?: string
  bot_id?: string
  user_id?: string
  owner_created?: string
  expires_at?: number
  status?: ITEM_STATUS
  created_at?: number
  updated_at?: number
}
