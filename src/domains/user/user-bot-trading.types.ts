import { ITEM_STATUS } from 'src/const/transaction'

export type QueryUserBotTrading = {
  id?: string
  order_id?: string
  bot_id?: string
  user_id?: string
  status?: ITEM_STATUS
  broker_server?: string
  broker_account?: string
  subscriber_id?: string
}

export type RawUserBotTrading = {
  id?: string
  order_id?: string
  bot_id?: string
  user_id?: string
  owner_created?: string
  expires_at?: number
  status?: ITEM_STATUS
  broker?: string
  broker_server?: string
  broker_account?: string
  subscriber_id?: string
  profile_id?: string
  created_at?: number
  updated_at?: number
}
