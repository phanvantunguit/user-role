import { TRANSACTION_EVENT } from 'src/const/transaction'
import { RawTransaction, RawTransactionLog } from '../transaction'

export interface PaymentDomain {
  ipnTransaction(params: any): Promise<IPNTransaction>
  createTransaction(
    transaction: RawTransaction,
    email: string
  ): Promise<PartnerResCreateTransaction>
  extraDataPaymentInfo(payload: RawTransactionLog): any
  checkTransaction(payment_id: string): Promise<CheckTransaction>
}
export type CheckTransaction = {
  transaction_event: TRANSACTION_EVENT
  metadata: any
}
export type IPNTransaction = {
  transaction: RawTransaction
  transaction_event: TRANSACTION_EVENT
  metadata: any
}
export type PartnerResCreateTransaction = {
  metadata: any
  buy_amount: string
  txn_id: string
  wallet_address: string
  timeout: number
  qrcode_url?: string
  status_url?: string
  checkout_url?: string
}
