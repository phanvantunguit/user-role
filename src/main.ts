import { NestFactory } from '@nestjs/core'
import { ValidationPipe } from '@nestjs/common'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'
import * as bodyParser from 'body-parser';

import { UserRolesAppModule } from './user-roles-app.module'
import { ExceptionsUtil } from 'src/utils/exception.util'
import { APP_CONFIG } from './config'
import { ENVIRONMENT } from './config/types'
// import { BackgroudAppModule } from './backgroud-app.module'
// import { TransactionJob } from './backgroud-jobs/transaction.job'
// import { WsAdapter } from '@nestjs/platform-ws'
import { WebSocketAdapter } from './websocket/websocket.adapter'
import { BackgroudAppModule } from './backgroud-app.module'
import { BotTradingJob } from './backgroud-jobs/bot-trading.job'
async function bootstrap() {
  const userRolesApp = await NestFactory.create(UserRolesAppModule)
  userRolesApp.use(bodyParser.json({ limit: '50mb' }));
  userRolesApp.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  userRolesApp.enableCors({
    exposedHeaders: ['Authorization', 'Refresh-Token']
  })
  userRolesApp.useGlobalPipes(
    new ValidationPipe({ whitelist: false, forbidNonWhitelisted: true })
  )
  userRolesApp.useGlobalFilters(new ExceptionsUtil())
  if (APP_CONFIG.ENVIRONMENT !== ENVIRONMENT.PRODUCTION) {
    const merchantSwaggerConfig = new DocumentBuilder()
      .setTitle('User roles Document')
      .setDescription('This is user roles API document')
      .setVersion('1.0')
      .addBearerAuth()
      .addApiKey({type: 'apiKey', name: 'Refresh-Token', in: 'header'}, 'Refresh-Token')
      .addServer(APP_CONFIG.BASE_URL)
      .build()
    const userRolesAppDocument = SwaggerModule.createDocument(
      userRolesApp,
      merchantSwaggerConfig
    )
    SwaggerModule.setup('api/v1/swagger', userRolesApp, userRolesAppDocument)
  }
  // userRolesApp.useWebSocketAdapter(new WsAdapter(userRolesApp))

  const wss = new WebSocketAdapter(userRolesApp)
  wss.init()
  await userRolesApp.listen(APP_CONFIG.PORT || 3000)
  // if (APP_CONFIG.ENVIRONMENT !== ENVIRONMENT.DEVELOPMENT) {
  const backgroudApp = await NestFactory.createApplicationContext(
    BackgroudAppModule
  )
  const botTradingJob = backgroudApp.get(BotTradingJob)
  botTradingJob.run()
  // const transactionJob = backgroudApp.get(TransactionJob)
  // transactionJob.run()
  // }
}
bootstrap()
