import { Module } from '@nestjs/common'

import {
  ApiV1AdminControllerModule,
  ApiV1UserControllerModule,
} from './controller.modules'
import { DatabaseModule } from './repositories'
import { DeactivateUserBotTradingHandler } from './usecases/bot/deactive-user-bot-trading'
import { UserGetBotTradingHandler } from './usecases/bot/user-get-bot-trading'
import { UserListBotTradingPlanHandler } from './usecases/bot/user-list-bot-trading-plan'
import * as websocketEvents from './websocket'
@Module({
  imports: [
    DatabaseModule,
    ApiV1AdminControllerModule,
    ApiV1UserControllerModule,
  ],
  providers: [
    ...Object.values(websocketEvents),
    UserListBotTradingPlanHandler,
    UserGetBotTradingHandler,
  ],
})
export class UserRolesAppModule {}
